<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/admin', 'Admin\AdminDashboardController@welcomeTest'); 
Route::get('/admin/profileView', 'Admin\AdminDashboardController@profileView'); 
Route::get('/admin/View', 'CustomerCareController@viewCustomerCare');
Route::get('/order/get-customer-details', 'CustomerCareController@getCustomerDetails');


Route::get('/admin/customer-care/orders/view-all', 'Admin\AdminDashboardController@viewAllOrders');
Route::post('/admin/customer-care/orders/view-all', 'Admin\AdminDashboardController@viewAllOrders');



Route::get('/admin/customer-care/order/view-details/{orderno}', 'Admin\AdminDashboardController@viewOrderDetails');
 


 
Route::get('/admin/customer-care/orders/view-completed', 'Admin\AdminDashboardController@viewCompletedOrders');
Route::post('/admin/customer-care/orders/view-completed', 'Admin\AdminDashboardController@viewCompletedOrders');





//Manager product category form
Route::get('/admin/products/category/manage-categories', 'Admin\AdminDashboardController@manageProductCategory');
Route::post('/admin/products/category/save-category', 'Admin\AdminDashboardController@saveProductCategory'); 



//Update product category

//Destroy/Delete category
Route::get('/admin/product-category/delete/{cat_id}', 'Admin\AdminDashboardController@destroyCategory'); 




//portal
Route::get('/privacy-policy', 'PublicPortalController@viewPrivacyPolicy'); 
Route::get('/about-us', 'PublicPortalController@viewAboutUs');
Route::get('/terms-and-conditions', 'PublicPortalController@viewTermsConditions');