<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Development Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/appointment/business/prepare-booking-cab/{bin}/{srv_code?}', 'Appointment\AppointmentControllerWeb@bookCabNewService')->middleware('preloadcmsconfig' );

 Route::match(array('GET', 'POST'),'/booking/customer/crs/place-booking','Booking\BookingController@makeCrsBooking')->middleware('auth' ,'preloadcmsconfig' );

Route::post('/booking/customer/services/place-appointment','StoreFront\StoreFrontController@makeServiceAppointment')->middleware('preloadcmsconfig' );
Route::get('/admin/customer-care/car-rental-services-view-all', 'Admin\CarRentalServiceController@viewCarRentalServices')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/car-rental-services-service-all', 'Admin\CarRentalServiceController@viewCarRentalServicesAll')->middleware('auth' , 'checkifadmin');

Route::get('/admin/customer-care/view-car-service/{bin}', 'Admin\CarRentalServiceController@viewCarServices')->middleware('auth' , 'checkifadmin');
Route::match(array('GET', 'POST'), '/admin/business/add-car-rental-services',
'Admin\CarRentalServiceController@addCarRentalServices')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/customer-care/business/add-promotion', 'Admin\CarRentalServiceController@savePromotion')->middleware('auth' , 'checkifadmin');
Route::match(array('GET', 'POST'), '/admin/customer-care/edit-car-rental-service',
'Admin\CarRentalServiceController@editCarRentalService')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/customer-care/delete-car-rental-service', 'Admin\CarRentalServiceController@deleteCarRentalServices')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/view-more-details-crs/{srv_prid}','Admin\CarRentalServiceController@viewMoreDetailsCRS')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/add_service_package_spec', 'Admin\CarRentalServiceController@addServicePackageSpec')->middleware('auth', 'checkifadmin' );
Route::post('/admin/customer-care/business/edit_service_package_spec', 'Admin\CarRentalServiceController@editServicePackageSpec')->middleware('auth', 'checkifadmin' );
Route::post('/admin/customer-care/business/delete_service_package_spec', 'Admin\CarRentalServiceController@deleteServicePackageSpec')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer-care/all-car-rental-service', 'Admin\CarRentalServiceController@allCarRentalServices')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/add-sponsored-package', 'Admin\CarRentalServiceController@addSponsoredPackage')->middleware('auth' , 'checkifadmin');
Route::match(array('GET', 'POST'), '/admin/customer-care/edit-all-car-rental-service',
'Admin\CarRentalServiceController@editAllCarRentalService')->middleware('auth' , 'checkifadmin' );

Route::post('/admin/customer-care/delete-all-car-rental-service', 'Admin\CarRentalServiceController@deleteAllCarRentalServices')->middleware('auth' , 'checkifadmin');

Route::get('/pos/orders/view-bookings-bill', 'Erp\PDFController@downloadReceiptBookings') ;

Route::get('/admin/manage-landing-section-contents', 'Admin\CarRentalServiceController@landingSectionContents')->middleware('auth' , 'checkifadmin');
Route::get('/admin/manage-landing-section-contents-business', 'Admin\CarRentalServiceController@landingSectionContentsBusiness')->middleware('auth' , 'checkifadmin');

Route::get('/admin/link-landing-section-with-package-business/{id}', 'Admin\CarRentalServiceController@landingSectionPackageBusiness')->middleware('auth' , 'checkifadmin');
Route::post('/admin/link-landing-section-with-package-business-save', 'Admin\CarRentalServiceController@landingSectionPackageBusinessSave')->middleware('auth' , 'checkifadmin');
Route::match(array('GET', 'POST'), '/admin/add-landing_screen_section_content', 'Admin\CarRentalServiceController@addLandingScreenSectionContent')->middleware('auth' , 'checkifadmin');

Route::post('/admin/delete-landing-sreen-section', 'Admin\CarRentalServiceController@deleteLandingScreensection')->middleware('auth' , 'checkifadmin');
Route::match(array('GET', 'POST'), '/admin/edit-landing-sreen-section', 'Admin\CarRentalServiceController@editLandingScreensection')->middleware('auth' , 'checkifadmin');

Route::match(array('GET', 'POST'),'/admin/landing-screen-sections-image', 'Admin\CarRentalServiceController@editLandingScreenSectionsImage')->middleware('auth' , 'checkifadmin');

Route::get('/admin/booking/order/view-more-details/{orderno}', 'Admin\CarRentalServiceController@viewMoreOrderDetails')->middleware('auth', 'checkifadmin' );
Route::get('/admin/booking', 'Admin\BookingAppointmentController@dashboardBooking')->middleware('auth', 'checkifadmin' );

Route::get('/admin/booking/orders-view-all', 'Admin\CarRentalServiceController@viewAllOrders')->middleware('auth' , 'checkifadmin');
Route::post('/admin/booking/orders-view-all', 'Admin\CarRentalServiceController@viewAllOrders')->middleware('auth' , 'checkifadmin');

Route::get('/admin/booking/orders/view-completed', 'Admin\CarRentalServiceController@viewCompletedOrdersBooking')->middleware('auth' , 'checkifadmin');
Route::post('/admin/booking/orders/view-completed', 'Admin\CarRentalServiceController@viewCompletedOrdersBooking')->middleware('auth' , 'checkifadmin');
Route::post('/admin/order/update-booking-remarks', 'Admin\CarRentalServiceController@updateNormalOrderRemarks')->middleware('auth' , 'checkifadmin');
Route::post('/admin/update-discount', 'Admin\CarRentalServiceController@updateDiscount')->middleware('auth' , 'checkifadmin' );
Route::match(array('GET', 'POST'),'/admin/manage-landing-screen-sections-editor', 'Admin\CarRentalServiceController@LandingScreenSectionsEditor')->middleware('auth' , 'checkifadmin' );

Route::get('/admin/manage-landing-screen-sections', 'Admin\CarRentalServiceController@manageLandingScreenSections')->middleware('auth' , 'checkifadmin');
Route::post('/admin/reorder-landing-screen-sections', 'Admin\CarRentalServiceController@reorderLandingScreenSections')->middleware('auth' , 'checkifadmin' );

Route::get('/admin/app-landing-screen-sections', 'Admin\CarRentalServiceController@appLandingScreenSections')->middleware('auth' , 'checkifadmin');
Route::post('/admin/add-landing-screen-sections', 'Admin\CarRentalServiceController@addLandingScreenSections')->middleware('auth' , 'checkifadmin');
Route::post('/admin/edit-landing-screen-sections', 'Admin\CarRentalServiceController@editLandingScreenSections')->middleware('auth' , 'checkifadmin');
Route::match(array('GET', 'POST'),'/admin/edit-landing-screen-sections-icon', 'Admin\CarRentalServiceController@editLandingScreenSectionsIcon')->middleware('auth' , 'checkifadmin');
Route::post('/admin/delete-landing-screen-sections', 'Admin\CarRentalServiceController@deleteLandingScreenSections')->middleware('auth' , 'checkifadmin');
Route::post('/admin/systems/add-meta-key', 'Admin\CarRentalServiceController@addMetaKey')->middleware('auth' , 'checkifadmin');
Route::get('/admin/systems/listing-meta-key', 'Admin\CarRentalServiceController@listingMetaKey')->middleware('auth' , 'checkifadmin');
Route::post('/admin/systems/edit-meta-key', 'Admin\CarRentalServiceController@editMetaKey')->middleware('auth' , 'checkifadmin');
Route::post('/admin/systems/delete-meta-key', 'Admin\CarRentalServiceController@deleteMetaKey')->middleware('auth' , 'checkifadmin');
Route::get('/admin/business/time-slot-listing/{bin}', 'Admin\AdminDashboardController@businessTimeSlotListing')->middleware('auth' , 'checkifadmin');
Route::get('/admin/staffs/active-attendance', 'Admin\AdminDashboardController@activeAttendance')->middleware('auth', 'checkifadmin' );
Route::post('/admin/business/save-time-slot', 'Admin\ServiceBookingController@saveTimeSlot');
Route::match(array('GET', 'POST'),'/admin/business/time-slot-listing-search', 'Admin\AdminDashboardController@businessTimeSlotListing')->middleware('auth' , 'checkifadmin');

Route::match(array('GET', 'POST'),'/admin/agents/active-attendance', 'Admin\StaffsAndAgentsController@activeAttendance')->middleware('auth', 'checkifadmin' );

Route::post('/unit-test/auto-assign-order',  'TestController@autoAssignOrder');
Route::match(array('GET', 'POST'),'/services/view-picnic-bookings', 'Admin\ServiceBookingController@viewPicnicBooking')->middleware('auth' , 'checkifadmin');
Route::post('/services/update-picnic-bookings-status','Admin\ServiceBookingController@updatePicnicBookingStatus');
Route::get('/services/download-picnic-receipt', 'Admin\ServiceBookingController@downloadPicnicBookingReceipt')->middleware('auth' );


Route::match(array('GET', 'POST'),'/services/view-all-active-appointment', 'Admin\ServiceBookingController@viewAllActiveAppointment')->middleware('auth' , 'checkifadmin');
Route::match(array('GET', 'POST'),'/services/view-all-offers', 'Admin\ServiceBookingController@viewAllOffers')->middleware('auth' , 'checkifadmin');
Route::match(array('GET', 'POST'),'/services/view-all-water-orders', 'Admin\ServiceBookingController@viewAllWaterOrders')->middleware('auth' , 'checkifadmin');



//chanchan
Route::post('/admin/systems/apply-voucher-code-booking', 'Admin\BookingAppointmentController@applyVoucherCodeBooking')->middleware('auth' , 'checkifadmin' );

Route::post('/admin/systems/order/remove-coupon-code-booking', 'Admin\BookingAppointmentController@removeCouponCodeBooking')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/order/update-remarks-booking', 'Admin\BookingAppointmentController@updateRemarksBooking')->middleware('auth' , 'checkifadmin');
Route::post('/admin/order/update-booking-order-status', 'Admin\BookingAppointmentController@updateBookingOrderStatus')->middleware('auth' , 'checkifadmin');
Route::post('/admin/business/update-pnd-comission','Admin\AdminDashboardController@updatePndComission')->middleware('auth', 'checkifadmin' );
Route::match(array('GET', 'POST'),'/admin/billing-and-clearance/merchant-clearance-billing-reports', 'Admin\AccountsAndBillingController@merchantClearanceBillingReports')->middleware('auth' , 'checkifadmin' );
Route::match( array('get', 'post'), '/admin/billing-and-clearance/generate-sales-and-merchant-clearance-report', 'Admin\AccountsAndBillingController@generateSalesAndMerchantClearanceReport')->middleware('auth' , 'checkifadmin' );


//Accounts GST/ITR

Route::match(array('GET', 'POST'),'admin/accounts/gst-itr-search', 'Admin\AccountsAndBillingController@searchGstAndItrController')->middleware('auth' , 'checkifadmin');

Route::post('/admin/business/split-pay-setup','Admin\AdminDashboardController@splitPaySetup')->middleware('auth', 'checkifadmin' );