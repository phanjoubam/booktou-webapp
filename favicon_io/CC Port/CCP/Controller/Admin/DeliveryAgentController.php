<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Business;  
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel;

use App\CustomerProfileModel;
use App\PaymentDealingModel;

use App\Traits\Utilities;
use App\User;
class DeliveryAgentController extends Controller
{


      protected function viewAllDeliveryAgent(Request $request)
    {
     
       // $business= array();
      if( isset($request->btnSearch) )
      {

      	$agents= DB::table("ba_users")
      	->join("ba_profile","ba_profile.id","=","ba_users.profile_id")
      	->select("ba_profile.*")
        ->where("category",100)
        ->where("ba_profile.fullname",'like', '' .$request->search_key. '%')
        ->orderBy("ba_profile.fullname","asc")
        ->get(); 

       }
       else
       {

       	$agents= DB::table("ba_users")
        ->join("ba_profile","ba_profile.id","=","ba_users.profile_id")
        ->select("ba_profile.*")
        ->where("category",100)
        ->orderBy("ba_profile.fullname","asc")
        ->get(); 

       }
       $data = array('results' => $agents);

         return view("admin.all_agent")->with('data',$data);
    
  }


    protected function viewAgentProfile(Request $request)
  {
      $id=$request->a_id;
 
	   $agents= CustomerProfileModel::find($id)
        ->first(); 

      
       $data = array('results' => $agents);

         return view("admin.agent_profile")->with('data',$data);
    
  }

  

     



     




}
