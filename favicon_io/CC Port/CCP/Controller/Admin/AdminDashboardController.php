<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
 use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;


use App\Business;
use App\PremiumBusiness;



class AdminDashboardController  extends Controller
{
   
  

    protected function searchBusinessByLocation(Request $request)
    {
     
        $business= array();
      if( isset($request->btnSearch) )
      {

      $business= DB::table("ba_business")
      ->where("category","VARIETY STORE")
      ->where("name",'like', '' .$request->search_key. '%')
      ->orWhere("city",'like', '' .$request->search_key. '%')
      ->get(); 

       }
       $data = array('results' => $business);

         return view("admin.business_list")->with('data',$data);
    
  }

  protected function viewBusinessProfile(Request $request)
  {

 

      $id = $request->busi_id;
      $businessInfo= DB::table("ba_business")
      ->where("id", $id ) 
      ->first(); 

 

      $productCategory= DB::table("ba_product_category")
      ->get();

 

      $data = array('business' => $businessInfo,
                  'category' => $productCategory, 'bin' => $id);

 

         return view("admin.business_profile")->with('data',$data);
    
  }


    protected function viewBusinessProducts(Request $request)
    {
       $id = $request->busi_id;
      $businessInfo= DB::table("ba_business")
      ->where("id", $id ) 
      ->first(); 

        $productCategory= DB::table("ba_product_category")
      ->get();
       

       $products= array();
      if( isset($request->btnSearch) )
      {

        $products= DB::table("ba_products")
      ->where("bin",$id)
      ->where("category",$request->search_key)
      ->get(); 
    }
    else
    {
      $products= DB::table("ba_products")
      ->where("bin",$id)
      ->get();
    }

    $data = array('items' => $products,
                  'category' => $productCategory, 'bin' => $id);

         return view("admin.business_products")->with('data',$data);
    
  }


     // Get Business Earning Report for daily
     protected function viewBusinessDailyEarning(Request $request)
     {
        
       $id = $request->busi_id;
       $businessInfo= DB::table("ba_business")
      ->where("id", $id ) 
      ->first(); 

       if($request->reportDate == "" )
        {

            $today =   date('Y-m-d');

        }
        else
        {
            $today = date('Y-m-d', strtotime( $request->reportDate ));
        }
       
      $totalEarning = 0; 
     
      if( isset($request->btnSearch) )
      {


        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->join("ba_delivery_order", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->join("ba_order_status_log", "ba_service_booking.id", "=", "ba_order_status_log.order_no")
        ->where("ba_service_booking.bin",$id)
        ->where("ba_service_booking.book_status","delivered")
        ->whereDate('ba_order_status_log.log_date', $today)  
        ->select("ba_service_booking.id as orderNo", 
                 "ba_profile.fullname as orderBy",  
                 'book_status as orderStatus',
                 "total_cost as totalCost",
                 'delivery_charge as deliveryCharge',
                 "member_id as deliverBy")
        ->get();

        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {
            $totalEarning += $item->totalCost;
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->orderNo;

        }

         $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                }

 

            }   
         }
       } 
       else{



        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->join("ba_delivery_order", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->join("ba_order_status_log", "ba_service_booking.id", "=", "ba_order_status_log.order_no")
        ->where("ba_service_booking.bin",$id)
        ->where("ba_service_booking.book_status","delivered")
        ->whereDate('ba_order_status_log.log_date', $today)  
        ->select("ba_service_booking.id as orderNo", 
                 "ba_profile.fullname as orderBy",  
                 'book_status as orderStatus',
                 "total_cost as totalCost",
                 'delivery_charge as deliveryCharge',
                 "member_id as deliverBy")
        ->get();

        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {
            $totalEarning += $item->totalCost;
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->orderNo;

        }

         $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                }

 

            }   
         } 

        }   

          $data = array("totalEarning" => $totalEarning,  'results' =>  $booking_list, 'bin' => $id );


      return view("admin.business_daily_earning")->with('data',$data);

     }

     // Get Business Earning Report for monthly
     protected function viewBusinessMonthlyEarning(Request $request)
     {

        $id = $request->busi_id;
        $businessInfo= DB::table("ba_business")
        ->where("id", $id ) 
        ->first(); 

       // return json_encode($request->month);

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }

        if($request->month !="" )
        {

            $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $totalEarning = 0; 

       // $products= array();
      if( isset($request->btnSearch) )
      {

        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->join("ba_delivery_order", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->join("ba_order_status_log", "ba_service_booking.id", "=", "ba_order_status_log.order_no")
        ->where("ba_service_booking.bin",$id)
        ->where("ba_service_booking.book_status","delivered")
        ->whereMonth('ba_order_status_log.log_date', $month )
        ->whereYear('ba_order_status_log.log_date', $year )  
        ->select("ba_service_booking.id as orderNo", 
                 "ba_profile.fullname as orderBy",  
                 'book_status as orderStatus',
                 "total_cost as totalCost",
                 'delivery_charge as deliveryCharge',
                 "member_id as deliverBy")
        ->get();

 
       // return json_encode($booking_list);

        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {
            $totalEarning += $item->totalCost;
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->orderNo;

           // $item->totalEarning=$totalEarning;
        }

         $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                }

 

            }   
         }
       }   
       else
       {
         $booking_list  = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->join("ba_delivery_order", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->join("ba_order_status_log", "ba_service_booking.id", "=", "ba_order_status_log.order_no")
        ->where("ba_service_booking.bin",$id)
        ->where("ba_service_booking.book_status","delivered")
        ->whereMonth('ba_order_status_log.log_date', $month )
        ->whereYear('ba_order_status_log.log_date', $year )  
        ->select("ba_service_booking.id as orderNo", 
                 "ba_profile.fullname as orderBy",  
                 'book_status as orderStatus',
                 "total_cost as totalCost",
                 'delivery_charge as deliveryCharge',
                 "member_id as deliverBy")
        ->get();

 
       // return json_encode($booking_list);

        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {
            $totalEarning += $item->totalCost;
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->orderNo;

           // $item->totalEarning=$totalEarning;
        }

         $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                }

 

            }   
       }
       }

          $data = array("totalEarning" => $totalEarning,  'results' =>  $booking_list, 'bin' => $id );


      return view("admin.business_monthly_earning")->with('data',$data);

     }



 
}
