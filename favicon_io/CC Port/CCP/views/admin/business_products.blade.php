@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 



<div class="cardbodyy margin-top-bg"> 


  <div class="col-md-12">

 <div class="card">
  <div class="card-body">
     <div class="card-header"> 
       <h3 class="card-title text-center">Item Details</h3>
     </div>

   <form class="form-inline" method="post" action="{{  action('Admin\AdminDashboardController@viewBusinessProducts') }}">

            {{ csrf_field() }}
              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Item Category:</label> 
              <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='search_key'>
                 @foreach ($data['category'] as $item)
                    <option value='{{$item->category_name}}'>{{$item->category_name}}</option> 
                 @endforeach
                </select>
              
                  <input type="hidden" value="{{ $data['bin'] }}" name="busi_id">
                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form> 

           
<hr class="my-4" />

 <table class="table">
      

  <thead class=" text-primary">
    <tr class="text-center">
    <th scope="col">Item Image</th>
    <th scope="col">Item Name</th>
    <th scope="col">Initial Stock</th>
    <th scope="col">Stock in Hand</th>
    <th scope="col">Max Order</th>
    <th scope="col">Unit Name</th>
    <th scope="col">Unit Price</th>
    <th scope="col">Action</th>
    </tr>
    </thead>

    <tbody>

    @foreach ($data['items'] as $itemP)

     <tr class="text-center">
     <td><img src='http://booktou.in/app/<?php 
      if($itemP->photos=="")
      { 
        echo "public/assets/image/no-image.jpg"; 
       }
  else{
        echo $itemP->photos ;
       }?>'
       alt="..." height="80px" width="100px"></td>
     <td>{{$itemP->pr_name}}</td>
      <td>{{$itemP->initial_stock}}</td>
      <td>{{$itemP->stock_inhand}}</td>
      <td>{{$itemP->max_order}}</td>
      <td>{{$itemP->unit_name}}</td>
      <td>{{$itemP->unit_price}}</td>

      <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                           <a class="dropdown-item" href="">Edit/Update</a> 
                            <a class="dropdown-item" href="">Delete</a> 
                               
                        </div>
                      </div>
                  
        </td>
 
    </tr>
    @endforeach
   </tbody>

  </table>


  </div>

   
</div>    
  
   </div> 
 </div>


     

@endsection

 
  
 