@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 


<div class="cardbodyy margin-top-bg"> 

        <?php 

$business = $data['business'];
 
?>

          <div class="card-body">
            <div class="row">

          <div class="col-lg-8">
            <div class="card card-chart">
              <div class="card-header"> 
                <h4 class="card-title text-center">Shop/Business Profile</h4>
                 
              </div>
             <div class="card-body">
          <div class="row">
            <div class="col-md-6">
         <div><strong>Store Name :</strong> {{$business->name}}</div>
         <div><strong>Shop No.:</strong> {{$business->shop_number}}</div>
         <div><strong>Store Address :</strong> {{$business->locality}}, {{$business->landmark}}, {{$business->city}}, {{$business->state}}-{{$business->pin}}</div>
         <div><strong>Primary Phone :</strong> {{$business->phone_pri}}</div>
         <div><strong>Alternate Phone :</strong> {{$business->phone_alt}}</div>
         <div><strong>Is Open :</strong> {{$business->is_open}}</div>
 
              </div> 
               <div class="col-md-6">
                  <img src='http://booktou.in/app/<?php 
      if($business->profile_image=="")
      { 
        echo "public/assets/image/no-image.jpg"; 
       }
  else{
        echo $business->profile_image ;
       }?>'
       alt="..." height="130px" width="190px">
              </div> 
  
            </div>
          </div>


        </div>
      </div>
       

   </div>
     
  </div>
</div>


@endsection


 
  
 