@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 



<div class="cardbodyy margin-top-bg"> 


  <div class="col-md-12">

 <div class="card">
 	<div class="card-body">
        <div class="card-header"> 
 		 <h3 class="card-title text-center">Agent Lists</h3>
 		</div>



        <form class="form-inline" method="get" action="" enctype="multipart/form-data">
              {{  csrf_field() }}
      
     <div class="form-group">
     	<label class="my-1 mr-2" for="inlineFormCustomSelectPref">Enter Agent Name:</label> 
    <input type="search" class="form-control form-control-sm" name="search_key" id="input-search" placeholder="Please type here...">
    </div>

     <button type="submit" class="btn btn-primary" value='search' name='btnSearch'>Search</button>
      </form>


       <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
		<th scope="col">Sl.No.</th>
		<th scope="col">Agent Name</th>
		<th scope="col">Address</th>
		<th scope="col">Phone</th>
		<th scope="col">Action</th>
		</tr>
	</thead>
	
		<tbody>
        	 <?php $i = 0 ?>
		@foreach ($data['results'] as $item)
		<?php $i++ ?>
		 <tr class="text-center">
		 <td>{{$i}}</td>
		 <td>{{$item->fullname}}</td>
		 <td>{{$item->locality}}, {{$item->landmark}}, {{$item->city}}, {{$item->state}}-{{$item->pin_code}}</td>
		 <td>{{$item->phone}}</td>
		 <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                           <a class="dropdown-item" href="{{ URL::to('/admin/customer-care/delivery-agent/view-profile') }}/{{$item->id}}">Profile</a> 
                             <a class="dropdown-item" href="{{ URL::to('/admin/customer-care/delivery-agent/get-daily-earning') }}/{{$item->id}}">Daily Earning Report</a> 
                             <a class="dropdown-item" href="{{ URL::to('/admin/customer-care/delivery-agent/get-monthly-earning') }}/{{$item->id}}">Monthly Earning Report</a> 
                           
                        </div>
                      </div>
                  
        </td>
		 </tr>
@endforeach
	</tbody>
		</table>
	
		
	</div>

	
	</div>


</div>

</div>


 
@endsection