@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 


<div class="cardbodyy margin-top-bg"> 

        <?php 

$agents = $data['results'];
 
?>

          <div class="card-body">
            <div class="row">

          <div class="col-lg-8">
            <div class="card card-chart">
              <div class="card-header"> 
                <h4 class="card-title text-center">Agent Profile</h4>
                 
              </div>
             <div class="card-body">
          <div class="row">
            <div class="col-md-6">
         <div><strong>Agent Name :</strong> {{$agents->fullname}}</div>
         <div><strong>Date Of Birth :</strong> {{date('d-m-Y', strtotime($agents->dob))}}</div>
         <div><strong>Agent Address :</strong> {{$agents->locality}}, {{$agents->landmark}}, {{$agents->city}}, {{$agents->state}}-{{$agents->pin_code}}</div>
         <div><strong>Phone :</strong> {{$agents->phone}}</div>
         <div><strong>Email :</strong> {{$agents->email}}</div>
         <div><strong>Aadhaar No. :</strong> {{$agents->aadhaar}}</div>
         <div><strong>Pan No. :</strong> {{$agents->pan}}</div>
 
              </div> 
               <div class="col-md-6">
                  <img src='http://booktou.in/app/<?php 
      if($agents->profile_photo=="")
      { 
        echo "public/assets/image/no-image.jpg"; 
       }
  else{
        echo $agents->profile_photo ;
       }?>'
       alt="..." height="130px" width="190px">
              </div> 
  
            </div>
          </div>


        </div>
      </div>
       

   </div>
     
  </div>
</div>


@endsection


 
  
 