<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/admin/customer-care/business/list-down', 'Admin\AdminDashboardController@searchBusinessByLocation');

Route::get('/admin/customer-care/business/view-profile/{busi_id}','Admin\AdminDashboardController@viewBusinessProfile');


Route::get('/admin/customer-care/business/view-products/{busi_id}', 'Admin\AdminDashboardController@viewBusinessProducts');
Route::post('/admin/customer-care/business/view-products', 'Admin\AdminDashboardController@viewBusinessProducts');

Route::get('/admin/customer-care/business/get-daily-earning/{busi_id}', 'Admin\AdminDashboardController@viewBusinessDailyEarning');
Route::post('/admin/customer-care/business/get-daily-earning', 'Admin\AdminDashboardController@viewBusinessDailyEarning');

Route::get('/admin/customer-care/business/get-monthly-earning/{busi_id}', 'Admin\AdminDashboardController@viewBusinessMonthlyEarning');

Route::post('/admin/customer-care/business/get-monthly-earning', 'Admin\AdminDashboardController@viewBusinessMonthlyEarning');

Route::get('/admin/customer-care/delivery-agent/get-all', 'Admin\DeliveryAgentController@viewAllDeliveryAgent');

Route::get('/admin/customer-care/delivery-agent/view-profile/{a_id}', 'Admin\DeliveryAgentController@viewAgentProfile');


Route::get('/admin/customer-care/delivery-agent/get-daily-earning/{a_id}', 'Admin\DeliveryAgentController@viewDailyEarningReport');

Route::post('/admin/customer-care/delivery-agent/get-daily-earning', 'Admin\DeliveryAgentController@viewDailyEarningReport');


Route::get('/admin/customer-care/delivery-agent/get-monthly-earning/{a_id}', 'Admin\DeliveryAgentController@viewMonthlyEarningReport');

Route::post('/admin/customer-care/delivery-agent/get-monthly-earning', 'Admin\DeliveryAgentController@viewMonthlyEarningReport');



