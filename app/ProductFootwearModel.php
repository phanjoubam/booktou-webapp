<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFootwearModel extends Model
{
    protected $table = 'ba_product_footwear';
    public $timestamps = false;

}
