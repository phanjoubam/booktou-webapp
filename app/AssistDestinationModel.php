<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistDestinationModel extends Model
{
    protected $table = 'ba_assist_destination';
    public $timestamps = false;
}
