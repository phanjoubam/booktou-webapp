<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTicketPurchaseDetails extends Model
{
    protected  $table='event_ticket_purchase_details';
    public $timestamps = false;

}
