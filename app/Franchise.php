<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Franchise extends Model
{
	protected $table = 'ba_franchise';
	public $timestamps = false;
}
