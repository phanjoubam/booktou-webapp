<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaterServiceModel extends Model
{
	protected $table = 'ba_water_services';
	public $timestamps = false;
}
