<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberWishlistModel extends Model
{
	protected $table = 'ba_member_wishlist';
	public $timestamps = false;
}
