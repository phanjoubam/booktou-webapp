<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopularcategoryModel extends Model
{
   protected $table = 'app_popular_category';
   
    public $timestamps = false;
}
