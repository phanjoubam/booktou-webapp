<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsProductGroupsModel extends Model
{
    protected $table = 'cms_product_groups'; 
 	public $timestamps = false;     
}
