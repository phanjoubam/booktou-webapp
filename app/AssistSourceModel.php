<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistSourceModel extends Model
{
    protected $table = 'ba_assist_source';
    public $timestamps = false;
}
