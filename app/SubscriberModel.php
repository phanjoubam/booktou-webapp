<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriberModel  extends Model
{
	protected $table = 'ba_subscriber';
	public $timestamps = false; 
	
}
