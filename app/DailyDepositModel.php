<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyDepositModel extends Model
{
	protected $table = 'ba_daily_deposit';
	public $timestamps = false;
}
