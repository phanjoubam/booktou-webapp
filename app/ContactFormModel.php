<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactFormModel extends Model
{
	protected $table = 'ba_contact_form';
}
