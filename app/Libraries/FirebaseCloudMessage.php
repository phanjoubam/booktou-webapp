<?php 

namespace App\Libraries;


class FirebaseCloudMessage 
{

    //push notify single user using firebase token  ID
   public function sendToIndividual( $to, $message ) {

      $fields = array(
         'to'   => $to,
         'notification' => $message,
         'data' => $message,
      );

      return $this->sendPushNotification( $fields );
   }


   // public function sendToIndividual( $to, $message ) {



   //    $fields = array (
   //       "message" => array( 
   //          "topic" => "BOOKTOU_GENERAL_TOPIC",
   //          "notification" => array(
   //             "body" => $message['message'],
   //             "title" =>$message['title'],
   //             "image" =>$message['image']
               
   //          ),
               
   //         // "data" => array("type" => $message['type'])
   //    )
   //    ); 

   //    return $this->sendPushNotification( $fields );
   // }



   // Sending push message to multiple users by firebase registration ids  
   public function sendToMultiple( $registration_ids, $message ) {
      $fields = array(
         'to'   => $registration_ids,
         'data' => $message,
      );

      return $this->sendPushNotification( $fields );
   }



  /* 
    // Sending message to a topic by topic name
    * @param $to
    * @param $message 
    */
   public function sendToTopic( $to, $message ) {
      $fields = array(
         'to'   => '/topics/' . $to,
         'data' => $message,
      );

      return $this->sendPushNotification( $fields );
   }


   
   // request to firebase servers 
   private function sendPushNotification( $fields ) 
   {

   		$fcm_key = 'AAAA627AzvU:APA91bE3DMR0mlNCrHDLA_KUYXEpgaYROqIV9WOtC5zRPtCWNzGHaaWlQFvf5amNFMVF7RkbNBrySlRkYMuBvUpLSKrdreohnUQgV5B8giJwJQftsAPDKLpUi8dvCJts2v8O6CB4Skv8';


      // Set POST variables
      $url = 'https://fcm.googleapis.com/fcm/send';

      $headers = array(
         'Authorization: key=' . $fcm_key ,
         'Content-Type: application/json'
      );

 		
 		//curl code  
      $ch = curl_init(); 
      curl_setopt( $ch, CURLOPT_URL, $url ); 
      curl_setopt( $ch, CURLOPT_POST, true );
      curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true ); 
      // Disabling SSL Certificate support temporarly
      curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false ); 
      curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) ); 
      // Execute post
      $result = curl_exec( $ch );
      curl_close( $ch );  
      return  $result ;
   }

   /*Send Push Notification for Business App*/

   public function sendToIndividualBiz( $to, $message ) {

      $fields = array(
         'to'   => $to,
         'notification' => $message,
         'data' => $message,
      );

      return $this->sendBizAppPushNotification( $fields );
   }



   public function sendToIndividualBizOwner( $to, $message ) {

      $fields = array(
         'to'   => $to,
         'notification' => $message,
         'data' => $message,
      );

      return $this->sendBizAppPushNotification( $fields );
   }


   private function sendBizAppPushNotification( $fields ) 
   {

         $fcm_key = 'AAAAbs1wu2E:APA91bF4PRbTBgy6PH3LIJdZ1nNsgpo3gnZyQGR0nxDaJFoAzCUiyQ_7WA2opj7Xo9gqNF7mMKRTa9Ms9V4UYBk87705EAZIymIKOOlBFPT7D_67sxPb03FNO3uvYODkayYus7MTaeUY';


      // Set POST variables
      $url = 'https://fcm.googleapis.com/fcm/send';

      $headers = array(
         'Authorization: key=' . $fcm_key ,
         'Content-Type: application/json'
      );

      
      //curl code  
      $ch = curl_init(); 
      curl_setopt( $ch, CURLOPT_URL, $url ); 
      curl_setopt( $ch, CURLOPT_POST, true );
      curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true ); 
      // Disabling SSL Certificate support temporarly
      curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false ); 
      curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) ); 
      // Execute post
      $result = curl_exec( $ch );
      curl_close( $ch );  
      return  $result ;
   }

}

 
?>

