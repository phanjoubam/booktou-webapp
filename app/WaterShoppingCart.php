<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaterShoppingCart extends Model
{
    protected $table = 'ba_water_booking_basket'; 
    public $timestamps=false;
}
