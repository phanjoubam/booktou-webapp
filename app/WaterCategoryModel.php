<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaterCategoryModel extends Model
{
	protected $table = 'ba_water_category';
	public $timestamps = false;
}
