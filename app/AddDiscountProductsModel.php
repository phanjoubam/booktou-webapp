<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddDiscountProductsModel extends Model
{
     protected $table = 'app_discounted_products';
    public $timestamps = false;
}
