<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantMenu extends Model
{
    protected $table = 'ba_restaurant_menu';
    public $timestamps = false;
}
