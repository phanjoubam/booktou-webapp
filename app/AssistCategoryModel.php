<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistCategoryModel extends Model
{
    protected $table = 'ba_assist_source';
    public $timestamps = false;
}
