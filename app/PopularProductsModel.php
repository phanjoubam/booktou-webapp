<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopularProductsModel extends Model
{
   
    protected $table = 'app_popular_products';
    
    public $timestamps = false;
}

