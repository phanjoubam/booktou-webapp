<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardLogModel  extends Model
{
	protected $table = 'ba_reward_log';
    public $timestamps = false;
}
