<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRating extends Model
{
	protected $table = 'ba_service_ratings';
	public $timestamps = false;
}
