<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingOfferBasket extends Model
{
    protected $table = 'ba_shopping_offer_basket';
	public $timestamps = false;
	
}
