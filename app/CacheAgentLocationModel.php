<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CacheAgentLocationModel extends Model
{
    protected $table = 'ba_cache_agent_location';
    public $timestamps = false;
}
