<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPndStatusLogModel extends Model
{
    protected $table = 'ba_pnd_order_status_log';
	public $timestamps = false;
}
