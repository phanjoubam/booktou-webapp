<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationItemsModel extends Model
{
    protected $table = 'ba_donation_items';
    public $timestamps = false;
}
