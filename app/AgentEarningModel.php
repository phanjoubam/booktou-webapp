<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentEarningModel  extends Model
{
    protected $table = 'ba_agent_earning';
	public $timestamps = false;

}
