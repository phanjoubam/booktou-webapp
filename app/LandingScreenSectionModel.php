<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingScreenSectionModel extends Model
{
    protected $table = 'app_landing_screen_section';
    
    public $timestamps = false;
}
