<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessCommission  extends Model
{
	protected $table = 'ba_business_commission';
	public $timestamps = false;
}
