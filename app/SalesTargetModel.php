<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesTargetModel extends Model
{
	protected $table = 'ba_sales_target';
	public $timestamps = false; 
	
}
