<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackerCategoryVisit extends Model
{
    protected $table = 'ba_tracker_category_visit';
	public $timestamps = false;

}
