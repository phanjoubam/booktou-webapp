<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingScreenSectionContenModel extends Model
{
    protected $table = 'app_landing_screen_section_content';
    
    public $timestamps = false;
}
