<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringMasterDishGroup extends Model
{
    protected $table = 'ba_master_dish_group';
	public $timestamps = false;
}
