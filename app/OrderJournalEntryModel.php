<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderJournalEntryModel extends Model
{
	protected $table = 'bta_journal_entry';
    public $timestamps = false;  
}
