<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentLocationModel  extends Model
{
	protected $table = 'ba_agent_locations'; 
}
