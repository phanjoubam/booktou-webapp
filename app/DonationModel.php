<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationModel extends Model
{
    protected $table = 'ba_donations';
    public $timestamps = false;
}
