<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingBasketServiceProductModel extends Model
{
    protected $table = 'ba_shopping_basket_srv_products';
    public $timestamps = false;
}
