<?php

namespace App\Traits;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Libraries\FirebaseCloudMessage;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;


use App\Business;
use App\User;
use App\ServiceBooking;
use App\DeliveryOrderModel;
use App\CustomerProfileModel;
use App\OrderStatusLogModel;

//razorpay
use Razorpay\Api\Api;


trait Utilities
{

    public function getUserInfo($uid)
    {
		 
        $data = array( "user_id" => 'na', 'profileIsComplete' => 'no',  'name' => 'na' ,  "bin" => "na", 
          "member_id" => "na" , "business"=> "na",  
          'category' => '-1' , 'pinCode' => 'na'); 
        $user = User::find($uid); 
 
        if( isset($user) )
        {


          //0 - customer, 1 - salon owner, 10 - staff, 100-delivery executive 

          if(  $user->category == 1 || $user->category == 10  )
          {
              
              $business_info = DB::table("ba_business") 
              ->select( "id", "name", "phone_pri as phone" )
              ->where("id", $user->bin ) 
              ->first();
              
              if(isset($business_info ))
              {
                $data['name'] = $business_info->name ;
                $data['bin'] = $business_info->id  ;
                $data['name'] = $business_info->phone ;
                 $data['member_id'] = $user->profile_id   ;
                $data['profileIsComplete'] = 'yes'; 
              }

          }
          else  if(  $user->category == 0 || $user->category == 100  )
          {
              $profile_info  = DB::table("ba_profile") 
              ->select( "id", "fullname as name", "phone"  )
              ->where("id", $user->profile_id ) 
              ->first();
              
              if(isset($profile_info ))
              {
                $data['name'] = $profile_info->name ;
                $data['bin'] = 0   ;
                $data['member_id'] = $profile_info->id  ;
                $data['phone'] = $profile_info->phone ;
                 $data['pinCode'] =  $user->pin_code ;
              }
          }
          else 
          {
              
          }
 
            $data['category'] = $user->category ;
      			$data['user_id'] = $user->id ;
      			$data['name'] = $user->fname ;
            $data['userAddress'] = $user->locality ;

			 
        }
        return json_encode($data);

    }
 
	 
 public function sendSmsAlert( $phones, $message  )
 { 


    $api_key = '35EE230E8E71EA';
    $contacts = implode(",", $phones) ;
    $from = 'SOFTSS';
    $sms_text =  $message   ;
    //Submit to server
    $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, "http://sms.acceptsms.com/app/smsapi/index.php");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
      $response = curl_exec($ch);
      curl_close($ch);
       

      /*
    foreach($phones as $phone )
    {
         $ch = curl_init('https://www.txtguru.in/imobile/api.php?');
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, "username=ezrasorokhaiba&password=Mayol@2019&source=INFSMS&dmobile=". $phone .
         "&message=" . $message );
         curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
         $data = curl_exec($ch); 
     } 
*/
   

}




public function sendFCMNotification( $tokens , $title, $message )
{

    $notification = array(
     'title' => $title ,
     'body' => $message ,
     'sound' => 'default',
     'badge' => '1',
     'image' => "https://booktou.in/app/public/assets/image/logo.png"  );


   $api_key = 'AAAATlUiw28:APA91bGnNJEV_wqD-ROY22yxNGNVPUP12frmjiWaDPKT_vnGcUQ9ufvW14tXV5PnswkaL_v8b0urqgTrjr0_ibK40NtNGyO7oIxFfcIikNXsKIS2rphxPFh4gtUA_ID7NbQ3FuF6IIGI';
   $headers = array(
     'Content-Type:application/json',
     'Authorization:key='. $api_key
   );

   $ch = curl_init('https://fcm.googleapis.com/fcm/send');
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
   curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

   foreach($tokens as $token)
   {
     $arrayToSend = array('to' => $token, 'data' => $notification,'priority'=>'high');
     $json = json_encode($arrayToSend);
     curl_setopt($ch, CURLOPT_POSTFIELDS,   $json   );
     $data = curl_exec($ch); 
   }

 }



public function sendFCMNotificationByTitle( $topic , $title, $message )
{

    $notification = array(
     'title' => $title ,
     'body' => $message ,
     'sound' => 'default',
     'badge' => '1',
     'image' => "https://booktou.in/app/public/assets/image/logo.png"  );


   $api_key = 'AAAATlUiw28:APA91bGnNJEV_wqD-ROY22yxNGNVPUP12frmjiWaDPKT_vnGcUQ9ufvW14tXV5PnswkaL_v8b0urqgTrjr0_ibK40NtNGyO7oIxFfcIikNXsKIS2rphxPFh4gtUA_ID7NbQ3FuF6IIGI';
   $headers = array(
     'Content-Type:application/json',
     'Authorization:key='. $api_key
   );

   $ch = curl_init('https://fcm.googleapis.com/fcm/send');
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
   curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

   foreach($tokens as $token)
   {
     $arrayToSend = array('to' => $token, 'data' => $notification,'priority'=>'high');
     $json = json_encode($arrayToSend);
     curl_setopt($ch, CURLOPT_POSTFIELDS,   $json   );
     $data = curl_exec($ch); 
   }

 }




 public function calcGeoDistance( $latitude1, $longitude1, $latitude2, $longitude2)
 {


    $theta = $longitude1 - $longitude2; 
    $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta))); 
    $distance = acos($distance); 
    $distance = rad2deg($distance); 
    $distance = $distance * 60 * 1.1515 * 1.609344;
    return $distance ;  
}




  public function chunkImage($source , $target_folder , $destination )
  {

    //compress
    $info = getimagesize($source );

    if ($info['mime'] == 'image/jpeg')

      $image = imagecreatefromjpeg($source);

    elseif ($info['mime'] == 'image/gif')

      $image = imagecreatefromgif($source);

    elseif ($info['mime'] == 'image/png')
      $image = imagecreatefrompng($source);


    $original_w = $info[0];
    $original_h = $info[1];
    $thumb_w =  ceil( $original_w / 2  );
    $thumb_h =  ceil( $original_h / 2  );

    $thumb_img = imagecreatetruecolor($thumb_w, $thumb_h);
    
    imagecopyresampled($thumb_img, $image, 0, 0, 0, 0, $thumb_w, $thumb_h, $original_w, $original_h);

    imagejpeg($thumb_img, $destination); 
                            

  }



  public function sendSmsFromTemplate(  $dlt_entity_id,  $dlt_header_id,  $dlt_template_id , $phones, $message )
  {
    
      $allphones = array(); 
      for($i=0; $i < sizeof($phones) ; $i++ )
      {
        $allphones[] .= "91" .  $phones[$i] ;
      }
      
      $contacts = implode(",", $allphones) ;

      $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, "https://www.txtguru.in/imobile/api.php?");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  "username=ezrasorokhaiba&password=bookTou@2020&source=BOOKTU&dmobile=". $contacts . 
        "&message=" . $message .  "&dltentityid=". $dlt_entity_id . "&dltheaderid=". $dlt_header_id ."&dlttempid=". $dlt_template_id . "&mt=u" );
        $response = curl_exec($ch);
        curl_close($ch);

      

}


  //assigning normal order to agent
  public function autoAssignNormalOrderToAgent(  $orderno,  $agentid )
  {
        
        $order_info = ServiceBooking::find( $orderno );
        if( !isset( $order_info)  )
        {
          return json_encode ( array( "status" =>  "failure"  ) );
        }

        $customer_info  = CustomerProfileModel::find($order_info->book_by);
        if(   !isset( $customer_info)  )
        {
          return json_encode ( array( "status" =>  "failure"  ) );
        }

        
        $delivery_order = DeliveryOrderModel::where("member_id", $agentid )
        ->where("order_type", "client order" )
        ->where("order_no", $orderno )
        ->first();
        
        
        if( !isset($delivery_order))
        {
          $delivery_order = new DeliveryOrderModel; 
        } 
        $delivery_order->member_id = $agentid ;
        $delivery_order->order_no = $orderno ;
        $delivery_order->accepted_date = date('Y-m-d H:i:s');
        $delivery_order->order_type =  "client order" ;
        $save = $delivery_order->save(); 
        
        
        if($save)
        { 
          $order_info->is_confirmed = 'yes'; 
          $order_info->book_status = "delivery_scheduled";
          $order_info->save();

          //update agent log
          DB::table('ba_agent_locations')
          ->where('staff_id', $agentid  )
          ->where("agent_status", "ready-for-task")
          ->whereDate("created_at", date("Y-m-d") )
          ->update( array('agent_status' => "engaged" ) );



          DB::table('ba_order_status_log')
          ->where('order_no', $orderno  )
          ->increment('seq_no', 1);

          $orderStatus = new OrderStatusLogModel;
          $orderStatus->order_no = $orderno  ; 
          $orderStatus->order_status = "delivery_scheduled";
          $orderStatus->log_date = date('Y-m-d H:i:s');
          $orderStatus->save();  
          
          //send cloud SMS 
          $response = array();
          $response['title']     = "New Order Assigned";
          $response['message']   = "Order # " . $orderno  . " is assigned to you."; 
          $response['imageUrl'] = config('app.app_cdn')  . "/assets/image/delivery-task.jpg"; 
          
          $receipent = User::where("profile_id", $agentid )->first()  ; 
          if(  isset($receipent ) )
          {
            //$firebase = new FirebaseCloudMessage();
            //$regId    = $receipent->firebase_token;
            //$response = $firebase->sendToSingleMember( $regId, $response, "agent" ); 
          } 
          
          return json_encode ( array( "status" =>  "success"  ) );
        }
        else
        {
          return json_encode ( array( "status" =>  "failure"  ) );
        }
      }


    

    //razorpay create order
  public function createRazorPayOrder( $orderno , $amount, $order_notes, $gateway_mode )
  {
    if(  $gateway_mode  != "" && strcasecmp(  $gateway_mode  , "development") == 0 )
    {
            $key_id  = 'rzp_test_CKkfnJst0MbRyR';
            $key_secret = 'iKB6FyDkOHW3Jdg1Izh7GbpK';
        }
      else if(  $gateway_mode  != "" && strcasecmp(  $gateway_mode  , "production") == 0 )
        {
            $key_id  = 'rzp_live_C7fzsFySjZQxIt';
            $key_secret = 'Ow4CFJkbi9oa6zVFhhPnPMY3';
        }
    else
    {
      $data= [ 
      "message" => "failure", 
            "status_code" =>  998 ,
            'detailed_msg' => "Failed to create RazaorPay Order!", 
            'cftoken' => "" ] ;
            return json_encode($data);
    }
     
    $api = new Api($key_id, $key_secret);
    $response = $api->order->create( 
      array(
      'receipt' => "rcp-" . $orderno  , 
      'amount' => $amount,
      'currency' => 'INR', 
      'notes'=> $order_notes ));

    return  $response ;
    
  }
  
   public function fetchRazorPaySecureKey( $gateway_mode )
    {

    if(  $gateway_mode  != "" && strcasecmp(  $gateway_mode  , "production") == 0 )
    {
      $key_id  = 'rzp_live_C7fzsFySjZQxIt';
      $key_secret = 'Ow4CFJkbi9oa6zVFhhPnPMY3';     
    }
    else
    {
      $key_id  = 'rzp_test_CKkfnJst0MbRyR';
      $key_secret = 'iKB6FyDkOHW3Jdg1Izh7GbpK';
    }

      $secure_key  = [ "key_id" => $key_id, "key_secret" => $key_secret  ] ;
      return json_encode($secure_key);
    }

//check cashfree status and update order 
  public function checkRazorPayOrderStatus( $order_id , $gateway_mode )
  {

      if(  $gateway_mode  != "" && strcasecmp(  $gateway_mode  , "production") == 0 )
      {
        $key_id  = 'rzp_live_C7fzsFySjZQxIt';
        $key_secret = 'Ow4CFJkbi9oa6zVFhhPnPMY3';     
      }
      else
      {
        $key_id  = 'rzp_test_CKkfnJst0MbRyR';
        $key_secret = 'iKB6FyDkOHW3Jdg1Izh7GbpK';
      }

      $api = new Api($key_id, $key_secret); 
      $response = $api->order->fetch($order_id)->payments();
      return   $response;

  }


    public function fetchRazorPaymentLinkDetails( $payment_link_id , $gateway_mode )
  {

      if(  $gateway_mode  != "" && strcasecmp(  $gateway_mode  , "production") == 0 )
      {
        $key_id  = 'rzp_live_C7fzsFySjZQxIt';
        $key_secret = 'Ow4CFJkbi9oa6zVFhhPnPMY3';     
      }
      else
      {
        $key_id  = 'rzp_test_CKkfnJst0MbRyR';
        $key_secret = 'iKB6FyDkOHW3Jdg1Izh7GbpK';
      }


      $api = new Api($key_id, $key_secret);
      $response = $api->paymentLink->fetch( $payment_link_id ); 

      return   $response;
  }



      public function paginate($items, $perPage = 50, $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $total = count($items);
        $currentpage = $page;
        $offset = ($currentpage * $perPage) - $perPage ;
        $itemstoshow = array_slice($items , $offset , $perPage);
        return new LengthAwarePaginator($itemstoshow ,$total   ,$perPage);
    }
    
  public function pendingPayments($orderno)
  {
    
    $order_info = ServiceBooking::find( $orderno );

    if(!isset( $order_info))
    {
      return json_encode ( array( "status" =>  "failure"  ) );
    }

    $order_key  = ["order_info" => $order_info] ;
      return json_encode($order_key);
    
  }




  public function sendWhatsAppMessageToRegisteredPhone( $phone )
  {

      $doc_url = "https://staging.booktoux.com/mega-event/public/online_file.pdf";
      
      /*
      $data_array = array(
          "messaging_product" => "whatsapp", 
          "to" =>  $phone ,
          "type" => "template",  
          "template" => array(
              "name" => "fest_btx_ticket",
              "language" => array("code" => "en_US") ,
             
          "components" => array(
            array(
            "type" => "header",
            "parameters" => array( array(
                    "type" => "document",
                    "document" => array(
                      "link" => $doc_url ,
                      "filename" => "Ticket" ) 
                ),
            ),
          ),
            array( "type" => "body" ),
          ),
        ),
      ); 


      */

$data_array = 
array(
          "messaging_product" => "whatsapp", 
          "to" =>  $phone ,
          "type" => "template",  
          "template" => array(
              "name" => "mega_event_promo",
              "language" => array("code" => "en_US") ,
             
          "components" => array(
            array(
            "type" => "header",
            "parameters" => array( array(
                    "type" => "image",
                    "image" => array(
                      "link" =>  'https://booktoux.com/btxfest/images/btx_fest.jpg' ) 
                ),
            ),
          ),
            array( "type" => "body" ),
          ),
        ),
      ); 



      $json = json_encode($data_array);
      $token = "EAAKBW1q7UQkBALrjYfGYqXsDrfZCHRWc0Eb4uqolRfwphZB2yBkZBFKSZCDFKz99JOpKdAW2EW6TGngwiGU46ZBXrxYQJI2QiuzuTA2bzOiIiMwzHvlWUtMjfZC2dCdZCKFPKfIxy90vDsYY07vH9jlRSLAM04GKZAQnPOCxELgdLfZCazqrrNpUyCuYCiCJO9le7pTgPPTXqXgZDZD";
 
    
    echo $json;
      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, "https://graph.facebook.com/v15.0/100112509662787/messages?");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Authorization: Bearer ' . $token
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json );
        $response = curl_exec($ch);
        curl_close($ch);

}


public function sendWhatsAppEventInvitation( $phone )
{


      $data_array = 
        array(
          "messaging_product" => "whatsapp", 
          "to" =>  $phone ,
          "type" => "template",  
          "template" => array(
              "name" => "mega_event_promo",
              "language" => array("code" => "en_US") ,
             
          "components" => array(
            array(
            "type" => "header",
            "parameters" => array( array(
                    "type" => "image",
                    "image" => array(
                      "link" =>  'https://booktoux.com/btxfest/images/btx_fest.jpg' ) 
                ),
            ),
          ),
            array( "type" => "body" ),
          ),
        ),
      ); 



      $json = json_encode($data_array);
      $token = "EAAKBW1q7UQkBALrjYfGYqXsDrfZCHRWc0Eb4uqolRfwphZB2yBkZBFKSZCDFKz99JOpKdAW2EW6TGngwiGU46ZBXrxYQJI2QiuzuTA2bzOiIiMwzHvlWUtMjfZC2dCdZCKFPKfIxy90vDsYY07vH9jlRSLAM04GKZAQnPOCxELgdLfZCazqrrNpUyCuYCiCJO9le7pTgPPTXqXgZDZD";
 

      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, "https://graph.facebook.com/v15.0/100112509662787/messages?");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Authorization: Bearer ' . $token
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json );
        $response = curl_exec($ch);
        curl_close($ch);

}



}
