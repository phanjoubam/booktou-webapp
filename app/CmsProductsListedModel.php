<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsProductsListedModel extends Model
{
    protected $table = 'cms_products_listed'; 
    public $timestamps = false; 
     
}
