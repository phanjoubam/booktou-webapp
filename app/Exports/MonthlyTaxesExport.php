<?php

namespace App\Exports;

use App\MonthlyTaxes;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class MonthlyTaxesExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

      use Exportable;

      protected $bin ;
      protected $month ;
      protected $year ;

      public function __construct(String $bin ,String $month, String $year) {

          $this->bin = $bin;
          $this->month = $month;
          $this->year = $year;
           
      }

    public function view(): View
    
    { 
    	  
             
              $bin   =  $this->bin;
              $month = $this->month;
              $monthname = date('F', mktime(0, 0, 0, $month, 10));
              $year = $this->year;
        
              $business = DB::table('ba_business')
              ->select()
              ->where('is_block','no')
              ->orderBy('name','asc')
              ->get();


        // $merchantTotalAmount = DB::table('ba_service_booking')
        // ->where('bin',$bin)
        // ->select(DB::Raw('sum(total_cost) as taxableAmount'),"bin")
        // ->whereYear("ba_service_booking.service_date",$year)
        // ->where('book_status','delivered')
        // ->groupBy('bin')
        // ->get();

              $merchantTotalAmount = DB::select('select sum(taxableAmount) as taxableAmount,bin 
          from
          (
            select sum(total_cost)as taxableAmount, bin 
            from ba_service_booking
            where book_status="delivered" and 
            year(service_date)='.$year.' 
            and bin='.$bin.'  
            group by bin
    
            UNION
    
            select sum(total_amount)as taxableAmount,request_by as bin 
            from ba_pick_and_drop_order
            where book_status="delivered" and 
            year(service_date) = '.$year.' and 
            source="business"
            and request_from="business" 
            and pay_mode="online"
            and request_by = '.$bin.' 

            group by bin
            
            )npnd group by bin' );

        

       $normal_orders= DB::table("ba_service_booking")
        ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
        ->where("ba_service_booking.bin",$bin)
        ->whereMonth("ba_service_booking.service_date",$month)
        ->whereYear("ba_service_booking.service_date",$year)
        ->select("ba_service_booking.id as orderNo",
                  "ba_service_booking.bin as bin",
                  "ba_service_booking.book_date as orderDate",
                  "ba_service_booking.total_cost as total_amount",
                  "ba_service_booking.delivery_charge",
                  "ba_service_booking.payment_type",
                  "ba_service_booking.source",
                  "ba_service_booking.service_date as receiptDate",
                  "ba_service_booking.service_date as payDate",
                  "ba_service_booking.book_status as orderStatus", 
                  "ba_business.name","ba_business.category",
                  "ba_business.gstin as gst",
                  "ba_business.commission",
                  DB::Raw("'Normal' type")); 

        $pnd_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_pick_and_drop_order.id")
        ->where("ba_pick_and_drop_order.request_by",$bin)
        ->where("ba_pick_and_drop_order.request_from",'business')
        ->whereMonth("ba_pick_and_drop_order.service_date",$month)
        ->whereYear("ba_pick_and_drop_order.service_date",$year)
        ->unionAll($normal_orders)
        ->select("ba_pick_and_drop_order.id as orderNo",
                "ba_pick_and_drop_order.request_by as bin",
                "ba_pick_and_drop_order.book_date as orderDate",
                "ba_pick_and_drop_order.total_amount as total_amount",
                "ba_pick_and_drop_order.service_fee as delivery_charge",
                "ba_pick_and_drop_order.pay_mode as payment_type",
                "ba_pick_and_drop_order.source",
                "ba_pick_and_drop_order.service_date as receiptDate",
                "ba_pick_and_drop_order.service_date as payDate",
                "ba_pick_and_drop_order.book_status as orderStatus",
                DB::Raw("'NA' name ,
                         'Service' category,
                         'NA' gst,
                         '0.00' commission 
                         "), 
                "ba_order_sequencer.type as type")

        ->orderBy('orderNo','asc')
        ->get();
        //ends here
         
       $day_bookings = $pnd_orders; 

        
        
         $data = array('orderDetails'=>$day_bookings,'merchant'=>$business,
                       'binTotalAmount'=>$merchantTotalAmount,'month'=>$monthname,'year'=>$year);

       
        
        


         
        return view('admin.exports.merchant_taxes')->with($data);

      
      
    }
}
