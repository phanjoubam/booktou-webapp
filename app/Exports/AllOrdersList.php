<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AllOrdersList implements FromView
{

    use Exportable;

    protected $sorted_orders;

    public function __construct($sorted_orders)
    {
        $this->sorted_orders = $sorted_orders;
    }

    public function view(): View
    {

        return view('admin.accounts.view_gst_itr_download', [
            'orders' => $this->sorted_orders
        ]);
    }
}