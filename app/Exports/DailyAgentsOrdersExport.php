<?php

namespace App\Exports;

use App\DailyAgentsOrders;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class DailyAgentsOrdersExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

      use Exportable;
      protected $memberid ;
      protected $month ;
      protected $year ;

      public function __construct(String $memberid,String $month, String $year) {

          $this->memberid = $memberid;
          $this->month = $month;
          $this->year = $year;
           
      }

    public function view(): View
    
    { 
        
             
              
              $month = $this->month;
              $monthname = date('F', mktime(0, 0, 0, $month, 10));
              $year = $this->year;
              $memberid = $this->memberid;
               

         
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

         $all_agents = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->where("ba_users.category", 100)
        ->where("ba_users.franchise",0)
        ->where("ba_users.status","<>",100)
        ->where("ba_profile.fullname", "<>", "" )
        ->where("ba_profile.id",$memberid)
        ->select("ba_profile.*")
        ->first();

        
         $normal_orders = DB::table("ba_delivery_order")
         ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
         ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
         //->where("ba_service_booking.book_status","delivered")
         ->where('ba_delivery_order.member_id',$memberid)
         ->whereMonth("ba_service_booking.service_date", $month )
         ->whereYear("ba_service_booking.service_date", $year )        
         ->select(
          DB::Raw("date(service_date) as serviceDate") , 
          DB::Raw(" 'normal' as type"),
          DB::Raw("count(ba_service_booking.id) as orderCount"),
          DB::Raw("sum(ba_service_booking.delivery_charge) as total_earned")
         )
         ->groupBy("serviceDate")
          
         
         ->get();



         $pnd_orders = DB::table("ba_delivery_order")
         ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
         ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
         ->whereIn("request_from", array ('business', 'merchant') )
         //->where("ba_pick_and_drop_order.book_status","delivered")
         ->where('ba_delivery_order.member_id',$memberid)
         ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
         ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
         ->select(
           DB::Raw("date(service_date) as serviceDate"),
           DB::Raw(" 'pnd' as type"),
           DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
           DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned"),
          )
         ->groupBy("serviceDate")
          
          ->get();

        $assist_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
       ->whereIn("request_from", array ('customer' ) )
       //->where("ba_pick_and_drop_order.book_status","delivered")
       ->where('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
         DB::Raw("date(service_date) as serviceDate"),
         DB::Raw(" 'assist' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned"),
        )
       ->groupBy("serviceDate")
        
        ->get();

         

        $merged = $normal_orders->merge($pnd_orders);
         
        $_orders = $merged->merge($assist_orders);

        $all_orders = $_orders->all();

         
        $data = array(
          "agents" =>$all_agents, 
          'orders'=>$all_orders,
          'month'=>$monthname,
          'year'=>$year
        );


        return view('admin.exports.daily_orders_agents')->with('data',$data); 

      
      
    }
}
