<?php

namespace App\Exports;

use App\AdminDashboard;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class AdminDashboardExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    protected $key ;

    public function __construct(String $key ) {

        $this->key = $key;
         
    }

    public function view(): View
    { 
    	  
    	 

      
     $business= DB::table("ba_business")
          ->Where("id",$this->key)
          ->first(); 


     $bin = $business->id;
     $name = $business->name;

     $month =date('m');
     $monthName = date('F', mktime(0, 0, 0, $month, 10));
     
     $normal_orders = DB::table('ba_service_booking')
          ->join('ba_order_sequencer','ba_service_booking.id','=', 'ba_order_sequencer.id')
          ->whereMonth("ba_service_booking.service_date",$month)
          ->where('bin',$bin)
          ->select("type","ba_order_sequencer.id as orderNo","ba_service_booking.total_cost as amount",
            "book_status","source as source","payment_type as ptype",
            "delivery_charge as deliverycharge",DB::Raw("'bookTou'  target_pay"));


     

     $pnd_orders = DB::table("ba_pick_and_drop_order")
          ->join('ba_order_sequencer','ba_pick_and_drop_order.id','=', 'ba_order_sequencer.id')
          ->whereMonth("ba_pick_and_drop_order.service_date",$month)
          ->where('request_by', $bin)
          ->unionAll($normal_orders)
           ->select("type","ba_order_sequencer.id as orderNo","ba_pick_and_drop_order.total_amount as amount", "book_status","source as source","pay_mode as ptype","service_fee as deliverycharge",
            "payment_target as target_pay"
          ) 
    ->get();

          $data = array('allresult'=>$pnd_orders,'name'=>$name,'monthname'=>$monthName);

        //exporting excel from view    
        return view('admin.exports.monthly_orders')->with('data',$data);
    }
}
