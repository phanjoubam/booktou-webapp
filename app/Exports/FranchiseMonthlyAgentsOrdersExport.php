<?php

namespace App\Exports;

use App\FranchiseMonthlyAgentsOrders;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class FranchiseMonthlyAgentsOrdersExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

      use Exportable;
      protected $frno ;
      protected $month ;
      protected $year ;

      public function __construct(String $frno,String $month, String $year) {

          $this->frno = $frno;
          $this->month = $month;
          $this->year = $year;
           
      }

    public function view(): View
    
    { 
        
             
              
       $month = $this->month;
       $monthname = date('F', mktime(0, 0, 0, $month, 10));
       $year = $this->year;
 
       $frno = $this->frno;
      
      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->where("ba_users.category", 100)
      ->where("ba_users.franchise",$frno)
      ->where("ba_users.status","<>",100)
      ->where("ba_profile.fullname", "<>", "" )
      ->select("ba_profile.*")
      ->orderBy("id", "asc")
      ->get(); 



      //dd($all_agents);

      $memberid = array();
      foreach ($all_agents as $profile) {
         $memberid[] = $profile->id;
      }



       $normal_orders = DB::table("ba_delivery_order")
       ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_service_booking.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_service_booking.service_date", $month )
       ->whereYear("ba_service_booking.service_date", $year )        
       ->select(
        "ba_delivery_order.member_id as member_id", 

        DB::Raw(" 'normal' as type"),
        DB::Raw("count(ba_service_booking.id) as orderCount"),
        DB::Raw("sum(ba_service_booking.delivery_charge) as total_earned")

       )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
       ->get();

        

       $pnd_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereIn("request_from", array ('business', 'merchant') )
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
        "ba_delivery_order.member_id as member_id",
         
         DB::Raw(" 'pnd' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned")


        )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
        ->get();
        
       $assist_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereIn("request_from", array ('customer' ) )
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
        "ba_delivery_order.member_id as member_id",
         
         DB::Raw(" 'assist' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned")


        )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
        ->get();


        $merged = $normal_orders->merge($pnd_orders,$assist_orders);
        
        $all_orders = $merged->all();
        
        //dd($all_orders);
         
        $data = array(
          "agents" =>$all_agents, 
          'orders'=>$all_orders ,
          'month'=>$monthname,
          'year'=>$year,
          'mont'=>$month 
        );
        return view('franchise.exports.franchise_monthly_orders_agents')->with('data',$data);








        
              
      
      
    }
}
