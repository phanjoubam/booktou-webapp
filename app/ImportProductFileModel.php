<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportProductFileModel extends Model
{
    protected $table = 'ba_import_file';
    public $timestamps = false;
}
