<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveUserLog extends Model
{
	protected $table = 'ba_active_user_log';
    public $timestamps = false;
}
