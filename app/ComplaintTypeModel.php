<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintTypeModel extends Model
{
    protected $table = 'ba_complaint_type';
    public $timestamps = false;

}
