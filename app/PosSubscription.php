<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosSubscription  extends Model
{
	protected $table = 'ba_pos_subscription';
	public $timestamps = false; 
	
}
