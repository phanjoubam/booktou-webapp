<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentSalaryAdvanceModel  extends Model
{
	protected $table = 'ba_salary_and_advance';
    public $timestamps = false;
}
