<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempBookingCartModel extends Model
{
    protected $table = 'ba_temp_booking_cart';
	public $timestamps = false;

}
