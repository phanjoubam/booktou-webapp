<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffSalaryModel extends Model
{
	protected $table = 'ba_staff_salary';
	public $timestamps = false;
}
