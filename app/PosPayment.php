<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosPayment  extends Model
{
	protected $table = 'ba_pos_payment';
	public $timestamps = false; 
	
}
