<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessMenu extends Model
{
    protected $table = 'ba_business_menu';
    public $timestamps = false;
}
