<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCacheModel  extends Model
{
	protected $table = 'ba_cache_agent_location';
	public $timestamps = false;
}
