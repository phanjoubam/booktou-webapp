<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPhotosModel extends Model
{
    protected $table = 'ba_product_photos';
    public $timestamps = false;

}
