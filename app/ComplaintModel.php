<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintModel extends Model
{
	protected $table = 'ba_complaint';
	public $timestamps = false;
}
