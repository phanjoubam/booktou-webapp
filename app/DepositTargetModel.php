<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepositTargetModel  extends Model
{
	protected $table = 'ba_deposit_target';
	public $timestamps = false;
}
