<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogsProductBrowsedModel extends Model
{
    protected $table = 'ba_logs_product_browsed';
	public $timestamps = false;

}
