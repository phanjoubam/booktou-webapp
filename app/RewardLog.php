<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardLog  extends Model
{
    protected $table = 'ba_reward_log';
    public $timestamps = false;
}
