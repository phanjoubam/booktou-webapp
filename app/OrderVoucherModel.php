<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderVoucherModel extends Model
{
	protected $primaryKey = 'vno';  
	protected $table = 'bta_order_journal';
    public $timestamps = false;  
}
