<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImportedModel extends Model
{
	protected $fillable = [ 'store', 'pr_name', 'description', 'unit_value','unit_name','unit_price','stock','category' ];
    protected $table = 'ba_products_imported';
    public $timestamps = false;

}
