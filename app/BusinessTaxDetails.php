<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessTaxDetails extends Model
{
	protected $table = 'ba_business_tax_details';
	public $timestamps = false;
}
