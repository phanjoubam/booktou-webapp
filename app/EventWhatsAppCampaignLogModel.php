<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventWhatsAppCampaignLogModel   extends Model
{
    //Event Whatsapp Campaign Log
    protected  $table='event_whatsapp_cpn_log';
    public $timestamps = false;

}
