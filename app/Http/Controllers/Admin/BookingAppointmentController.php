<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

use App\User; 
use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;
use App\BusinessPaymentModel;
use App\Franchise;
use App\BusinessCommissionModel;
use App\DailyDepositModel;
use App\FeedbackMerchant;
use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\MonthlyAgentsOrdersExport;
use App\Exports\DailyAgentsOrdersExport;
use App\Exports\DailySalesOrderExport;
use App\Exports\MonthlyCommissionExport;
use App\Traits\Utilities; 
use PDF;
use App\DepositTargetModel;
use App\ProductVariantModel;
use App\ImportProductFileModel;
use App\SectionTypeModel;
use App\SectionDetailsModel;
use App\AppSectionModel;
use App\AppSectionDetailsModel;
use App\PosSubscription;
use App\SelfPickUpModel;
use App\PosPayment;
use App\BrandModel;
use App\ServiceCategoryModel;
use App\AgentEarningModel;
use App\AgentSalaryAdvanceModel;
use App\PopularProductsModel;
use App\AddDiscountProductsModel;
use App\ServiceProductModel;
use App\PackageMetaModel;
use App\ServicePackageSpecModel;
use App\SponsoredPackageModel;
use App\LandingScreenSectionModel;
use App\LandingScreenSectionContenModel;
use App\MetaKeyModel;



class BookingAppointmentController extends Controller
{
    use Utilities;
    public function dashboardBooking(Request $request)
    {
      $status = array('new', 'confirmed','completed', ); 

       // if($request->filter_date=="")
       //  {
       //      $today = date('Y-m-d');
       //  }else{
       //      $today = date('Y-m-d',strtotime($request->filter_date));
       //  }

      $range_date = date('Y-m-d' , strtotime("10 days ago"));
      
      $picnic = DB::table('ba_service_booking')
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereRaw("date(ba_service_booking.book_date)>='$range_date'") 
      ->whereIn("ba_order_sequencer.type", array('booking'))   
      ->where("book_category", "PICNIC AND ACTIVITIES")
      ->where("ba_service_booking.book_status", "new")
      ->count();

       $completed_picnic = DB::table("ba_service_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("book_date",'>=',$range_date )
      ->where("ba_service_booking.book_status","confirmed")           
      ->where("book_category", "PICNIC AND ACTIVITIES")
      ->count();      

      $appointment_orders = DB::table("ba_service_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("book_date",'>=',$range_date )
      ->where("ba_service_booking.book_status","new") 
       ->where("ba_order_sequencer.type","appointment")          
      ->where("book_category", "Beauty")
      ->count();

      $completed_appointment_orders = DB::table("ba_service_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("book_date",'>=', $range_date )
      ->where("ba_service_booking.book_status","confirmed")
      ->where("ba_order_sequencer.type","appointment")             
      ->where("book_category", "Beauty")
      ->count();

      $offer_appointment_orders = DB::table("ba_service_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("book_date",'>=',$range_date )
      ->where("ba_service_booking.book_status","new") 
      ->where("ba_order_sequencer.type","offers")          
      ->count();

      $completed_offer_appointment_orders = DB::table("ba_service_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("book_date",'>=',$range_date )
      ->where("ba_service_booking.book_status","confirmed") 
      ->where("ba_order_sequencer.type","offers")          
      ->count();

      $water_orders = DB::table("ba_water_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_water_booking.id")
      ->whereDate("book_date",'>=',$range_date )
      ->where("ba_water_booking.book_status","new") 
       ->where("ba_order_sequencer.type","booking")          
      ->where("ba_water_booking.book_category", "WATER")
      ->count();   

      $completed_water_orders = DB::table("ba_water_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_water_booking.id")
      ->whereDate("book_date",'>=',$range_date )
      ->where("ba_water_booking.book_status","confirmed") 
       ->where("ba_order_sequencer.type","booking")          
      ->where("ba_water_booking.book_category", "WATER")
      ->count();

     
 
        return view("admin.bookings.dashboard_admin")->with("data",
        array(       
         "picnic" => $picnic,
         "completed_picnic" => $completed_picnic,
         "appointment_orders" => $appointment_orders,
         "completed_appointment_orders" => $completed_appointment_orders,
         "offer_appointment_orders"=>$offer_appointment_orders,
         "completed_offer_appointment_orders"=>$completed_offer_appointment_orders,
         "water_orders"=>$water_orders,
         "completed_water_orders"=>$completed_water_orders
       
      ));       
    } 


    protected function applyVoucherCodeBooking(Request $request)
    {

      $orderno = $request->order_no;
      $code = $request->coupon;

      $order_info = ServiceBooking::find($orderno);   
      if( !isset($order_info))
      {
        return redirect("services/booking/view-details/$orderno?bin=$order_info->bin" )->with("err_msg", 'No order found to apply coupon.'  );  
      }
 

      $order_costing =  DB::table("ba_service_booking_details")
      ->where("book_id", $orderno  ) 
      ->get();
      //dd( $order_costing);

      if( !isset($order_costing))
      {
        return redirect("services/booking/view-details/$orderno?bin=$order_info->bin" )->with("err_msg", 'No order found to apply coupon.'  );  
      }

      $book_price = 0.00;
      foreach($order_costing as $item)
      {
        $book_price  += $item->service_charge;  
    
      }


      $code_info= DB::table("ba_coupons")
      ->where("code", $code)
      ->where("bin", $order_info->bin )
      ->first();


  
      if(  !isset($code_info))
      {
        
              return redirect("services/booking/view-details/$orderno?bin=$order_info->bin" )->with("err_msg", 'Coupon is invalid!'  );  
      }


          $total_discount =  0;
          if(strcasecmp(trim($order_info->book_category),trim( $code_info->biz_category)) == 0 )
          {
            $total_discount  = ( $code_info->product_discount_pc / 100 ) *  $book_price ;  

          }           

            // $bookin_order = DB::table("ba_service_booking")
            // ->where('id',$orderno)
            // ->get();
 
          $order_info->coupon  = $code; 
          $order_info->discount =$order_info->discount + $total_discount;
          $order_info->save();

          return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", 'Coupon discount applied.',); 
  
      
 
    }


     protected function removeCouponCodeBooking(Request $request)
    {

      if($request->btnsave =='save')
      {
        $orderno = $request->key;
        $order_info = ServiceBooking::find($orderno);

         $code_info= DB::table("ba_coupons")
      ->where("bin", $order_info->bin )
      ->first();
        if( !isset($order_info))
        {
            return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", 'No order found to remove coupon.'  );  
        }
        else 
        {
          $order_costing =  DB::table("ba_service_booking_details")
          ->where("book_id", $orderno  ) 
          ->get();

          if( !isset($order_costing))
          {
            return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", 'No order found to remove coupon.'  );  
          }

          $book_price = 0.00;
          $total_discount=0.00;
          foreach($order_costing as $item)
          {
            $book_price  += $item->service_charge;  
          }
          $total_discount  = ( $code_info->product_discount_pc / 100 ) *  $book_price ; 

          $order_info->coupon  = null ; 
          $order_info->discount =$order_info->discount-$total_discount ;
          $order_info->save(); 
          
          return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", 'Coupon discount removed.'  ); 
        }
      } 
      
 
    }

    protected function updateRemarksBooking(Request $request)
     {
        $orderno = $request->orderno;
         $order_info   = ServiceBooking::find( $orderno) ;
        if( !isset($order_info))
        {
          return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", "No matching order found!"); 
        }
    
        if( $request->remarks == "" || $request->type == "" )
        {
          if($request->turl == "list")
          {
            return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", "Order remarks fields missing!"); 
          }
          else 
          {
            return redirect("services/booking/view-details/$orderno?bin=$order_info->bin" )->with("err_msg", "Order remarks fields missing!"); 
          }
          
        }

       

        $remark = new OrderRemarksModel();
        $remark->order_no = $orderno;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = $request->type ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();   

         if($request->turl == "list")
          {
            return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", "Order remarks updated!");   
          }
          else 
          {
            return redirect("services/booking/view-details/$orderno?bin=$order_info->bin" )->with("err_msg", "Order remarks updated!");   
          }

     
     }


    protected function updateBookingOrderStatus(Request $request)
    {

        if( $request->status == "" || $request->orderno == ""    )
        {
          return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", "Order status missing!"); 
        }


        $orderno = $request->orderno;

        $ordersequence = OrderSequencerModel::find( $orderno );
        if(!isset($ordersequence ))
        {
           return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", "Order not found!");
        }

        $ordertype = $ordersequence->type;

        if ( strcasecmp($ordersequence->type,  "booking" )  != 0 )  
        {
          return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", "Order type mismatched!");
        }


        $order_info   = ServiceBooking::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", "No matching order found!"); 
        }

        if( !in_array($request->status, array('new', 'confirmed', 'cancelled') ) )
        {
            return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")->with("err_msg", "Status selected is invalid!"); 
        }

        $order_info->book_status = $request->status;
        $order_info->cc_remarks = $request->remarks;
        $order_info->save();

        //updating service days status
        if( strcasecmp( $request->status, "cancelled") == 0)
        {
          $new_status = "open";
        }
        else
        {
           $new_status = "booked";
        }

        $booking_items = DB::table("ba_service_booking_details")
        ->where("book_id", $orderno)
        ->select("service_product_id")
        ->get();
        $package_ids = $booking_items->pluck("service_product_id")->toArray();

        if(count($package_ids) >  0)
        {
          $bin = $order_info->bin;
          $service_date = date("Y-m-d", strtotime( $order_info->service_date));

          $timeslots = DB::table("ba_service_days")
          ->where("bin",  $bin)
          ->whereDate("service_date",  $service_date )
          ->whereIn("package_id", $package_ids)
          ->update([ 'status' =>  $new_status ]); 

        }

        return redirect("services/booking/view-details/$orderno?bin=$order_info->bin")
        ->with("err_msg", "Order status updated!");
        
     }

    //
}
