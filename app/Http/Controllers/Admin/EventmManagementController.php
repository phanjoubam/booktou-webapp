<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

use App\Libraries\FirebaseCloudMessage;


use App\Business;
use App\EventTicketModel;



use App\Traits\Utilities; 

class EventmManagementController  extends Controller
{

    use Utilities;
 

    public function prepareTicketGeneration(Request $request)
    {
      return view("admin.eventmanagement.prepare_tickets_form");
    }   




    
    public function generateTicketQrcodes(Request $request)
    {

      $prefix = $request->prefix;
      $startfrom = $request->startfrom;
      $endno = $startfrom  + 500;

      $event_id = 1; //for the time being it is bookTouX Film Festival

      $cdn_path =  config('app.app_cdn_path')  ; 

      $folder_path =  $cdn_path  . '/assets/events/tickets/';
      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0777, true, true);
      }


      for($i = $startfrom; $i < $endno;  $i++ )
      {

        if($i < 10)
        {
          $slno = "0000".$i;
        }
        else if($i < 100)
        {
          $slno = "000".$i;
        }
        else if($i < 1000)
        {
          $slno = "00".$i;
        }
        else if($i < 10000)
        {
          $slno = "0".$i;
        }

        $ticket_no = $prefix . $slno;

        //generate qr codes
        $qr_url =   config('app.app_cdn') . "/assets/events/tickets/" . $ticket_no . ".png" ;
        $qr_path =  config('app.app_cdn_path') . "/assets/events/tickets/" . $ticket_no . ".png" ;
        QrCode::format('png')->size(500)->generate( $ticket_no , $qr_path );
        $new_ticket = new EventTicketModel;
        $new_ticket->ticket_no  = $ticket_no;
        $new_ticket->valid_date = date('Y-m-d', strtotime( $request->validdate));
        $new_ticket->price =  $request->cost;
        $new_ticket->visitor_name  = "not-filled";
        $new_ticket->visitor_phone  = "not-filled";
        $new_ticket->qrcode_url  = $qr_url;
        $new_ticket->ticket_format  = "OFL";
        $new_ticket->current_status  =  0;
        $new_ticket->event_id  = $event_id;
        $new_ticket->save();
      }

      return redirect("/admin/business/event-management/prepare-ticket-generation")->with("err_msg", "Tickets generated!");
    }

    public function urlToQrGenerator(Request $request)
    {

      return view("admin.qr_image");
 

    }

 
}
