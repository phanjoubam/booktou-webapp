<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;
use Maatwebsite\Excel\Concerns\Exportable;
 



use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;

use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\AllOrdersList;
use App\AccountsDailyLogModel;

use App\OrderVoucherModel; 
use App\OrderJournalEntryModel;




use App\Traits\Utilities; 
use PDF;

class AccountsAndBillingController  extends Controller
{
    use Utilities;


    protected function dailyAccountLedgerEntry(Request $request)
    {

    	if($request->btnsearch != "")
    	{
    		$date = date('Y-m-d', strtotime($request->datefilter));
    	}
    	else 
    	{
    		$date = date('Y-m-d');
    	}
    	

    	$ledgers = DB::table("ba_accounts_daily_log")
    	->whereDate("account_date", $date  )
    	->get();
 

    	if(isset($request->btnsave))
    	{
    		$dailylog = AccountsDailyLogModel::where("account_name", $request->type)
    		->whereDate("account_date", date('Y-m-d', strtotime( $request->date) ))
    		->first();

    		if( !isset($dailylog))
    		{
    			$dailylog = new AccountsDailyLogModel;
    		} 

    		$dailylog->account_name = $request->type;
    		$dailylog->amount = $request->amount;
    		$dailylog->details = $request->details;
    		$dailylog->entered_by = Session::get('__user_id_') ;
    		$dailylog->account_date = date('Y-m-d', strtotime( $request->date) );
    		$dailylog->entry_date = date('Y-m-d');
    		$dailylog->save();
 
    		return redirect('/admin/accounts/account-ledger-entry')->with("err_msg", "Ledger entry saved!");
    	}


    	$data = array( 
        'title'=>'Account ledger entry' ,  
        'date' => $date,
        'ledgers' =>$ledgers );

         return view("admin.accounts.add_daily_closing")->with( $data);

    }
    


    protected function weeklyPaymentDueMerchants(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }


         $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 2;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                return redirect("/admin/billing-and-clearance/payment-due-merchants" )->with("err_msg", "Selected month has only 4 weeks of payment."); 
            } 
        }
 

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "request_by", DB::Raw("sum(total_amount) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "bin", DB::Raw("sum(total_cost) as totalAmount") )
          ->groupBy("bin")
          ->get();   


        $data = array(  'businesses' => $all_businesses , 'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales, 'endDay' => $endDay , 'month' =>  $month ,  'year' => $year ); 
        return view("admin.billing.payment_due_report")->with(  $data);


    }

 

    protected function weeklySalesAndServiceReport(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }


         $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 2;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                return redirect("/admin/billing-and-clearance/payment-due-merchants" )->with("err_msg", "Selected month has only 4 weeks of payment."); 
            } 
        }
 

        $pnd_businesses = DB::table('ba_business')  
        ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
        ->whereIn("ba_business.id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->where("ba_business_commission.month", $month ) 
        ->where("ba_business_commission.year", $year ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" , "ba_business.phone_pri", "ba_business_commission.commission") 
        ->orderBy("ba_business.name",'asc');  

        
        $all_businesses = DB::table('ba_business')  
        ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
        ->whereIn("ba_business.id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
         ->where("ba_business_commission.month", $month ) 
        ->where("ba_business_commission.year", $year ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "ba_business.phone_pri" , "ba_business_commission.commission") 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->where("clerance_status", "<>", "paid")
          ->select( "request_by", DB::Raw("sum(total_amount-refunded) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->where("clerance_status", "<>", "paid")
          ->select( "bin", DB::Raw("sum(total_cost -refunded) as totalAmount") )
          ->groupBy("bin")
          ->get();   


        $data = array(  'businesses' => $all_businesses ,
         'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales, 'endDay' => $endDay , 'month' =>  $month ,  'year' => $year ); 
        return view("admin.billing.weekly_sales_service_report")->with(  $data);


    }


    protected function merchantClearanceBillingReports(Request $request )
    {   
//     if($request->year !=="" && $request->month !=="" && $request->cycle!=="" && $request->from_date==null && $request->to_date==null )
//     {     
//         if( $request->month  == "" &&  $request->year   == "")
//         { 
//             $month   =  date('m');
//             $year = date('Y');
//         }
//         else 
//         {
//           $month   =  $request->month ; 
//           $year   =  $request->year;
//         }  
//           $cycle = 1;
//         if($request->cycle == "" ||  $request->cycle  == 1 )
//         {
//             $startDay   =  1 ;  
//             $endDay   =    7;
//             $cycle = 1;
//         }
//         else if(  $request->cycle  == 2 )
//         {
//             $startDay   = 8 ;  
//             $endDay   =    14;
//             $cycle = 2;
//         }else if(  $request->cycle  == 3)
//         {
//             $startDay   = 15 ;  
//             $endDay   =    21;
//             $cycle = 2;
//         }else if(  $request->cycle  == 4)
//         {
//             $startDay   = 22 ;  
//             $endDay   =    28;
//             $cycle = 4;
//         }
//         else  
//         {
//             $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

//             if( $totalDays > 28)
//             {
//                 $startDay   = 29;  
//                 $endDay   =    $totalDays;   
//                 $cycle = 5; 
//             }
//             else 
//             {
//                 return redirect("/admin/billing-and-clearance/payment-due-merchants" )->with("err_msg", "Selected month has only 4 weeks of payment."); 
//             } 
//         }
 

//         $pnd_businesses = DB::table('ba_business')  
//         ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
//         ->whereIn("ba_business.id", function($query) use (   $month, $year ) { 

//           $query->select('request_by as bin' )
//           ->from('ba_pick_and_drop_order') 
//           ->where("request_from", "business") 
//           ->whereMonth("service_date", $month )
//           ->whereYear("service_date", $year )
//           ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

//         })
//         ->where("frno", 0 ) 
//         ->where("ba_business_commission.month", $month ) 
//         ->where("ba_business_commission.year", $year ) 
//         ->select('ba_business.id', "ba_business.name", "ba_business.category" , "ba_business.phone_pri", "ba_business_commission.commission") 
//         ->orderBy("ba_business.name",'asc');  

        

//         $all_businesses = DB::table('ba_business')  
//         ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
//         ->whereIn("ba_business.id", function($query) use (    $month, $year ) { 

//           $query->select('bin' )
//           ->from('ba_service_booking')  
//           ->whereMonth("service_date", $month )
//           ->whereYear("service_date", $year )
//           ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

//         })
//         ->where("frno", 0 ) 
//          ->where("ba_business_commission.month", $month ) 
//         ->where("ba_business_commission.year", $year ) 
//         ->select('ba_business.id', "ba_business.name", "ba_business.category", "ba_business.phone_pri" , "ba_business_commission.commission") 
//         ->orderBy("ba_business.name",'asc')
//         ->union($pnd_businesses)
//         ->get();  

  
//         $pnd_sales = DB::table("ba_pick_and_drop_order")
//         ->where("request_from", "business") 
//           ->whereMonth("service_date", $month )
//           ->whereYear("service_date", $year )
//           ->whereIn("book_status",  array ( 'completed', 'delivered') )
//           ->where("clerance_status", "<>", "paid")
//           ->select( "request_by", DB::Raw("sum(total_amount-refunded) as totalAmount") )
//           ->groupBy("request_by")
//           ->get(); 

//         $normal_sales = DB::table("ba_service_booking") 
//           ->whereMonth("service_date", $month )
//           ->whereYear("service_date", $year )
//           ->whereIn("book_status",  array ( 'completed', 'delivered') )
//           ->where("clerance_status", "<>", "paid")
//           ->select( "bin", DB::Raw("sum(total_cost -refunded) as totalAmount") )
//           ->groupBy("bin")
//           ->get();   


//         $data = array(  'businesses' => $all_businesses ,
//          'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales, 'endDay' => $endDay , 'month' =>  $month ,  'year' => $year ); 
//         return view("admin.billing.view_merchant_clearance_billing")->with(  $data);
// dd($request);

// }
// else{

    // if( $request->month  == "" &&  $request->year   == "")
    //     { 
    //         $month   =  date('m');
    //         $year = date('Y');
    //     }
    //     else 
    //     {
    //       $month   =  $request->month ; 
    //       $year   =  $request->year;
    //     }  

    //     if($request->cycle == "" ||  $request->cycle  == 1 )
    //     {
    //         $startDay   =  1 ;  
    //         $endDay   =    7;
    //         $cycle = 1;
    //     }
    //     else if(  $request->cycle  == 2 )
    //     {
    //         $startDay   = 8 ;  
    //         $endDay   =    14;
    //         $cycle = 2;
    //     }else if(  $request->cycle  == 3)
    //     {
    //         $startDay   = 15 ;  
    //         $endDay   =    21;
    //         $cycle = 2;
    //     }else if(  $request->cycle  == 4)
    //     {
    //         $startDay   = 22 ;  
    //         $endDay   =    28;
    //         $cycle = 4;
    //     }
    //     else  
    //     {
    //         $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

    //         if( $totalDays > 28)
    //         {
    //             $startDay   = 29;  
    //             $endDay   =    $totalDays;   
    //             $cycle = 5; 
    //         }
    //         else 
    //         {
    //             return redirect("/admin/billing-and-clearance/payment-due-merchants" )->with("err_msg", "Selected month has only 4 weeks of payment."); 
    //         } 
    //     }

         $from = date('Y-m-d' , strtotime("30 days ago"));
        $to =  date('Y-m-d');
        $up_to = date('Y-m-d', strtotime("+1 day",strtotime($to)));      
        if($request->from_date != '' && $request->to_date !='' )
        {
            $from = date('Y-m-d', strtotime($request->from_date));
            $to = date('Y-m-d', strtotime($request->to_date));
            $up_to = date('Y-m-d', strtotime("+1 day",strtotime($request->to_date)));
        }
        
        $page = isset($request->page) ? $request->page : 1;

        $orders = DB::table("ba_order_sequencer")
        ->whereBetween('entry_date',[$from, $up_to])
        ->get();       
         $non_pnd = $pnd_or_assist = array();

        foreach($orders as $order)
        {
            if($order->type == 'pnd' || $order->type == 'normal')
            {
                $pnd_or_assist[] = $order->id;
            }

            else
                $non_pnd[] = $order->id;
        }

        
        $pnd_businesses = DB::table('ba_business')  
        ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
        ->whereIn("ba_business.id", function($query) use (   $from , $up_to ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business")
          ->whereBetween('service_date',[$from, $up_to]) 
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->whereBetween('ba_business_commission.year',[$from, $up_to]) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" , "ba_business.phone_pri", "ba_business_commission.commission") 
        ->orderBy("ba_business.name",'asc');
        

        $all_businesses = DB::table('ba_business')  
        ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
        ->whereIn("ba_business.id", function($query) use (    $from, $up_to ) { 

          $query->select('bin' )
          ->from('ba_service_booking') 
          ->whereBetween('service_date',[$from, $up_to]) 
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->whereBetween('ba_business_commission.year',[$from, $up_to]) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "ba_business.phone_pri" , "ba_business_commission.commission") 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  
// dd( $all_businesses);
  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business")
        ->whereBetween('service_date',[$from, $up_to]) 
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->where("clerance_status", "<>", "paid")
          ->select( "request_by", DB::Raw("sum(total_amount-refunded) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking")
        ->whereBetween('service_date',[$from, $up_to]) 
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->where("clerance_status", "<>", "paid")
          ->select( "bin", DB::Raw("sum(total_cost -refunded) as totalAmount") )
          ->groupBy("bin")
          ->get();   

        $data = array(  'businesses' => $all_businesses ,
         'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales,'from_date' => $from, 'upto_date' => $to); 
        return view("admin.billing.view_merchant_clearance_billing")->with(  $data);
    } 





    // protected function dateRangeMerchantClearance(Request $request )
    // {  

    //     $from = date('Y-m-d' , strtotime("30 days ago"));
    //     $to =  date('Y-m-d');
    //     $up_to = date('Y-m-d', strtotime("+1 day",strtotime($to)));  
        
    //     if($request->from_date != '' && $request->to_date !='' )
    //     {
    //         $from = date('Y-m-d', strtotime($request->from_date));
    //         $to = date('Y-m-d', strtotime($request->to_date));
    //         $up_to = date('Y-m-d', strtotime("+1 day",strtotime($request->to_date)));

    //     }
        
    //     $page = isset($request->page) ? $request->page : 1;

    //     $orders = DB::table("ba_order_sequencer")
    //     ->whereBetween('entry_date',[$from, $up_to])
    //     ->get();       
    //      $non_pnd = $pnd_or_assist = array();

    //     foreach($orders as $order)
    //     {
    //         if($order->type == 'pnd' || $order->type == 'normal')
    //         {
    //             $pnd_or_assist[] = $order->id;
    //         }

    //         else
    //             $non_pnd[] = $order->id;
    //     }

        
    //     $pnd_businesses = DB::table('ba_business')  
    //     ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
    //     ->whereIn("ba_business.id", function($query) use (   $from , $up_to ) { 

    //       $query->select('request_by as bin' )
    //       ->from('ba_pick_and_drop_order') 
    //       ->where("request_from", "business")
    //       ->whereBetween('service_date',[$from, $up_to]) 
    //       ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

    //     })
    //     ->where("frno", 0 ) 
    //     ->whereBetween('ba_business_commission.year',[$from, $up_to]) 
    //     ->select('ba_business.id', "ba_business.name", "ba_business.category" , "ba_business.phone_pri", "ba_business_commission.commission") 
    //     ->orderBy("ba_business.name",'asc');
        

    //     $all_businesses = DB::table('ba_business')  
    //     ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
    //     ->whereIn("ba_business.id", function($query) use (    $from, $up_to ) { 

    //       $query->select('bin' )
    //       ->from('ba_service_booking') 
    //       ->whereBetween('service_date',[$from, $up_to]) 
    //       ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

    //     })
    //     ->where("frno", 0 ) 
    //     ->whereBetween('ba_business_commission.year',[$from, $up_to]) 
    //     ->select('ba_business.id', "ba_business.name", "ba_business.category", "ba_business.phone_pri" , "ba_business_commission.commission") 
    //     ->orderBy("ba_business.name",'asc')
    //     ->union($pnd_businesses)
    //     ->get();  

  
    //     $pnd_sales = DB::table("ba_pick_and_drop_order")
    //     ->where("request_from", "business")
    //     ->whereBetween('service_date',[$from, $up_to]) 
    //       ->whereIn("book_status",  array ( 'completed', 'delivered') )
    //       ->where("clerance_status", "<>", "paid")
    //       ->select( "request_by", DB::Raw("sum(total_amount-refunded) as totalAmount") )
    //       ->groupBy("request_by")
    //       ->get(); 

    //     $normal_sales = DB::table("ba_service_booking")
    //     ->whereBetween('service_date',[$from, $up_to]) 
    //       ->whereIn("book_status",  array ( 'completed', 'delivered') )
    //       ->where("clerance_status", "<>", "paid")
    //       ->select( "bin", DB::Raw("sum(total_cost -refunded) as totalAmount") )
    //       ->groupBy("bin")
    //       ->get();   


    //     $data = array(  'businesses' => $all_businesses ,
    //      'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales, 'endDay' => $endDay , 'month' =>  $month ,  'year' => $year ); 
    //     return view("admin.billing.view_merchant_clearance_billing")->with(  $data);
    // }


    protected function monthlyServiceReport(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }
  

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "request_by", DB::Raw("sum(total_amount) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "bin", DB::Raw("sum(total_cost) as totalAmount") )
          ->groupBy("bin")
          ->get();   


        $data = array(  'businesses' => $all_businesses , 
            'pnd_sales' =>$pnd_sales, 
         'normal_sales' => $normal_sales, 
         'month' =>  $month ,  'year' => $year ); 
        return view("admin.billing.monthly_service_report")->with(  $data);


    }


    // View Business Normal and PnD Sales Report for monthly
     protected function salesAndClearanceReport(Request $request )
     {           
        $cycles =  array(1);
        if( isset($request->cycle))
        {
            $cycles =  $request->cycle;
        }
        $bin = $request->bin;  

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        } 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

        $allowed_cycles = true;
        if(count($cycles) > 0)
        {
            //check if adjoining cycles are selected
            
            for($i=0; $i<count($cycles)-1; $i++)
            {
                for($j= $i+1; $j <count($cycles) ; $j++)
                {
                    if( $cycles[$i] + 1 != $cycles[$j]  )
                    {
                        $allowed_cycles = false; 
                    }
                    break;

                } 
            }

            if( count($cycles)  == 1 )
            {
                if($cycles[0]  == 1 )
                {
                    $startDay   =  1 ;  
                    $endDay   =    7;
                    $cycle = 1;
                }
                else if($cycles[0]  == 2 )
                {
                    $startDay   = 8 ;  
                    $endDay   =    14;
                    $cycle = 2;
                }else if($cycles[0]  == 3 )
                {
                    $startDay   = 15 ;  
                    $endDay   =    21;
                    $cycle = 3;
                }else if($cycles[0]  == 4 )
                {
                    $startDay   = 22 ;  
                    $endDay   =    28;
                    $cycle = 4;
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $startDay   = 29;  
                        $endDay   =  $totalDays; 
                    }
                }

            }
            else  //count($cycles)  > 1
            {
                $size = count($cycles)-1;
                
                if($cycles[0]  == 1 )
                {
                    $startDay   =  1 ;  
                }
                else if($cycles[0]  == 2 )
                {
                    $startDay   = 8 ;  
                }else if($cycles[0]  == 3 )
                {
                    $startDay   = 15 ;  
                }else if($cycles[0]  == 4 )
                {
                    $startDay   = 22 ; 
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $startDay   = 29;   
                    }
                    else 
                    {
                        $startDay   = 0;   
                    }
                }
 

                if($cycles[$size]  == 1 )
                { 
                    $endDay   =    7; 
                }
                else if($cycles[$size]  == 2 )
                { 
                    $endDay   =    14; 
                }else if($cycles[$size]  == 3 )
                { 
                    $endDay   =    21; 
                }else if($cycles[$size]  == 4 )
                {  
                    $endDay   =    28; 
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $endDay   =  $totalDays; 
                    }
                    else 
                    {
                        $endDay   = 0;   
                    }
                }
            }
        }

        if(  !$allowed_cycles  )
        {
            return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
            ->with("err_msg", "Billing cycles selected are not adjacent. Report generation blocked."); 
        }

        $business  =  DB::table("ba_business")
        ->where("id", $bin )
        ->select("id", "name", "commission" ) 
        ->first(); 

        if( !isset($business)  )
        {
            return redirect("/admin/billing-and-clearance/weekly-sales-and-services-report"  )
            ->with("err_msg", "Business not selected. Report generation blocked."); 
        }


        $business_commission  =  DB::table("ba_business_commission")
        ->where("month", $month )
        ->where("year", $year ) 
        ->where("bin", $bin ) 
        ->select("bin", "commission") 
        ->first(); 

        if(isset($business_commission))
        {
            $business->commission = $business_commission->commission;
        }
        

        // return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
        // ->with("err_msg", "Selected month has only 4 weeks of payment.");  

        $first = DB::table('ba_service_booking')  
        ->where("ba_service_booking.bin", $bin )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereIn("clerance_status", array( 'un-paid' ) )
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          DB::Raw("(total_cost - refunded) as orderCost"),   
          DB::Raw("'0.00' packingCost"), "seller_payable",
          'delivery_charge as deliveryCharge','service_fee', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("clerance_status", array( 'un-paid' ) )
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
          "source",
         DB::Raw("(total_amount - refunded) as orderCost"),  
          'packaging_cost as packingCost', "seller_payable",
          'delivery_charge as deliveryCharge','service_fee',
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 

       $all_business  = DB::table("ba_business") 
       ->get(); 

       $data =  array(  
        'business' => $business, 
        'businesses' => $all_business,
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart,
        'bin' => $bin,   
        'cycles' => $cycles,
        'start' =>  $startDay , 
        'end' =>  $endDay , 
        'month' =>  $month , 
        'year' => $year);

       return view("admin.billing.sales_report_per_cycle_for_merchant")->with( $data );


    }

    protected function generateSalesAndMerchantClearanceReport(Request $request )
     { 
        $bin=$request->bin;
        $from_date = $request->from;
        $up_to = $request->to;
        

        $business  =  DB::table("ba_business")
        ->where("id", $bin )
        ->select("id", "name", "commission" ) 
        ->first(); 

        if( !isset($business)  )
        {
            return redirect("/admin/billing-and-clearance/weekly-sales-and-services-report"  )
            ->with("err_msg", "Business not selected. Report generation blocked."); 
        }


        $business_commission  =  DB::table("ba_business_commission")
        ->whereBetween('month',[$from_date, $up_to])
        ->whereBetween('year',[$from_date, $up_to]) 
        ->where("bin", $bin ) 
        ->select("bin", "commission") 
        ->first(); 

        if(isset($business_commission))
        {
            $business->commission = $business_commission->commission;
        }
        

        // return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
        // ->with("err_msg", "Selected month has only 4 weeks of payment.");  

        $first = DB::table('ba_service_booking')

        ->where("ba_service_booking.bin", $bin )
        ->whereBetween('service_date',[$from_date, $up_to])
        ->whereIn("clerance_status", array( 'un-paid' ) )
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          DB::Raw("(total_cost - refunded) as orderCost"),   
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereBetween('service_date',[$from_date, $up_to])
          ->whereIn("clerance_status", array( 'un-paid' ) )
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
          "source",
         DB::Raw("(total_amount - refunded) as orderCost"),  
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 

       $all_business  = DB::table("ba_business") 
       ->get(); 

       $data =  array(  
        'business' => $business, 
        'businesses' => $all_business,
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart,
        'bin' => $bin, 
        'from_date'=>$from_date,
        'up_to'=>$up_to);
       return view("admin.billing.view_details_merchant_sales")->with( $data );
}


    function saveClearanceInformation(Request $request )
    {

        if( $request->bin == "" || $request->amount == "" ||$request->paymode == "" || $request->clearedon == ""  || 
            $request->month == ""|| $request->weekno == ""  )
        {
            return redirect('/')->with("err_msg", "No data provided for clearance.");
        }

       
        
        $bin = $request->bin; 

        $payment = new PaymentDealingModel;
        $payment->bin = $bin  ;
        $payment->amount =  $request->amount ;
        $payment->cleared_on = date('Y-m-d', strtotime($request->clearedon))  ;
        $payment->transact_no =  ( $request->refno != "" ) ? $request->refno  : "not provided"  ;
        $payment->payment_mode = $request->paymode;
        $payment->remarks =   ( $request->remarks != "" ) ? $request->remarks  : "not provided"  ;
       

        $payment->amount =  $request->weekno ;
        $payment->month =  $request->month ;
        $payment->year  =  $request->year ;
        $payment->save();




        //update selected orders in the week as paid 
        /*
        DB::table("ba_pick_and_drop_order")  
        ->whereIn('id', $orders )   
        ->update( ['clerance_status' => 'paid'] ) ; 

        DB::table("ba_service_booking")  
        ->whereIn('id', $orders )   
        ->update( ['clerance_status' => 'paid'] ) ; 

        */ 

         return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin  )
        ->with('err_msg', "Payment clerance information saved." );

    }


 

    protected function dailySalesAndServiceReport(Request $request )
    {

        if($request->reportDate == "")
        {
               $date   =  date('Y-m-d');  
        }
        else
        {
            $date   =  date('Y-m-d', strtotime($request->reportDate)); 
        }
         

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (  $date   ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business")
          ->whereDate("service_date", "=",   $date  )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use ( $date  ) { 

          $query->select('bin' )
          ->from('ba_service_booking') 
          ->whereDate("service_date", "=",   $date  )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business")
        ->whereDate("service_date", "=",   $date  )
        ->whereIn("book_status",  array ( 'completed', 'delivered') )
        ->select( "ba_pick_and_drop_order.*" ) 
        ->get(); 
    

         $normal_sales = DB::table('ba_service_booking')   
        ->whereDate("service_date",   $date  )
        ->whereIn("book_status",  array ( 'completed', 'delivered') )
        ->get();  

        $data = array(  'businesses' => $all_businesses , 'pnd_sales' =>$pnd_sales,  
            'normal_sales' => $normal_sales,  
            'reportDate' => $date ); 
        return view("admin.billing.daily_sales_and_service")->with(  $data);


    }




    // View Business Normal and PnD Sales Report for monthly
     protected function dailySalesAndServiceMerchantReport(Request $request )
     {

        $bin = $request->bin; 
        
        if($request->reportDate == "")
        {
            $date   =  date('Y-m-d');  
        }
        else
        {
            $date   =  date('Y-m-d', strtotime($request->reportDate)); 
        }
 


        if( $bin == 0 || $bin == "")
        {
            $bin == 0;
            $normals = DB::table('ba_service_booking')   
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $assists = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "customer") 
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id")  
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'assist' orderType"))   ;


            $result = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "business") 
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->union($normals)  
            ->union($assists)  
            ->select('ba_pick_and_drop_order.id', 
            "request_by as requestBy",
            'ba_pick_and_drop_order.service_date',
            "source",
            'total_amount as orderCost', 
            'service_fee as deliveryCharge', 
            DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
            'book_status  as bookingStatus',             
            "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get(); 
        }
        else 
        {
            $first = DB::table('ba_service_booking')  
            ->where("ba_service_booking.bin", $bin )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $result = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "business")
            ->where("request_by", $bin )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->union($first)  
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();

        } 
     
        
        $memberids = $bins = $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;
                $bins[] = $item->requestBy ;

            }
            if($item->orderType == "pnd")
            {
                $bins[] = $item->requestBy ;
            }
            if($item->orderType == "assist")
            {
                $memberids[] = $item->requestBy ;
            }
            
            
        }
        $oids[]= $bins[]= $memberids[]=0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 


        $businesses  =  DB::table("ba_business") 
        ->whereIn("id",  $bins  )  
        ->get();

        $profiles  =  DB::table("ba_profile") 
        ->whereIn("id",  $memberids  )  
        ->get();  

       

       $data =  array(  'businesses' =>$businesses, 
        'profiles' => $profiles,
        'results' =>  $result, 
        'cart_items' => $normal_sales_cart,
        'bin' => $bin ,'reportDate' => $date );
       return view("admin.billing.detailed_sales_report_for_day")->with( $data ); 
     }





    public function generateClearanceBill( Request $request)
    {
        $bin = $request->bin; 
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }


        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        if( $request->start !=  "" )
        {
            $startDay   =  $request->start; 
        }

        if( $request->end !=  "" )
        {
            $endDay   =  $request->end; 
        }

        if(   $startDay <=  0 || $endDay <   $startDay  )
        {
            return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )->with("err_msg", 
                    "Selected date range is invalid.");

        }
  
        


        $first = DB::table('ba_service_booking')  
        ->where("ba_service_booking.bin", $bin )
         ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
            'ba_service_booking.service_date',
          DB::Raw("'customer' source"),  
          DB::Raw("(total_cost - refunded) as orderCost"),   
          "refunded",
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 'service_fee', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ; 

        $orders = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
          "source", 
           DB::Raw("(total_amount - refunded) as orderCost"),   
          "refunded",
          "packaging_cost as packingCost" , 
          'delivery_charge as deliveryCharge','service_fee', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();

        $oids = array();
        foreach($orders as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 
 

       $business  = Business::find(  $bin)  ;
       $data =  array(  'business' =>$business,   
        'orders' =>  $orders, 
       'cart_items' =>$normal_sales_cart,
       'bin' => $bin,  
       'startDate' => $startDay,   
       'endDate' => $endDay, 
       'month' => $month, 'year' => $year ); 

       $pdf_file =  "booktou_mcbill_" .time(). '_' . $bin . ".pdf";
       $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ;

         

        if($request->format == "roll")
        {
            $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('admin.billing.clearance_bill_roll_paper' ,  $data  )   ; 
            return view('admin.billing.clearance_bill_roll_paper')->with( $data );
        }
        else  
        {
            if( strcasecmp($request->download  , "yes" ) == 0)
            {
                $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('admin.billing.clearance_bill_a4' ,  $data  )   ;
               return $pdf->download( $pdf_file  ); 
               
            }
            else 
            { 
                $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('admin.billing.clearance_bill_a4' ,  $data  )   ;
                return view('admin.billing.clearance_bill_a4')->with( $data );
            }
        }
    }
    

    protected function monthlySalesServiceEarningReport(Request $request )
    {
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }



        if($request->franchise !="" )
        {
          $franchise = $request->franchise ;
        }
        else
        {
            $franchise =  0;
        }


       
        $first = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->where("route_to_frno",  $franchise)
        ->whereIn("clerance_status",  array('un-paid')) 
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          "bin",
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->where("route_to_frno",  $franchise)
        ->whereIn("clerance_status",  array('un-paid')) 
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
            "request_by as bin",
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 

       $data =  array(   
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart, 
        'businesses' => $all_business, 
        'month' =>  $month , 
        'year' => $year  );
       return view("admin.billing.sales_report_per_month_for_merchant")->with( $data ); 
     }


    public function dailySalesReport(Request $request )
    {

        $franchise =  0;
        if($request->todayDate != "")
        {
            $todayDate  =  date('Y-m-d' , strtotime($request->todayDate)) ; 
        }
        else
        {
            $todayDate  = date('Y-m-d' );
        }
         $totalEarning = 0;

        if( strcasecmp($request->payTarget, "merchant")  == 0   )
        {
            $paytarget_where_pnd  = "payment_target  in ('merchant', 'business')";  

        }
        elseif( strcasecmp($request->payTarget, "booktou")  == 0   )
        {
            $paytarget_where_pnd  = "payment_target  in ('booktou' , 'Cash using Agent UPI', 'Cash to Agent')";  

        }
        else   
            {
               $paytarget_where_pnd  = "payment_target  in  ('merchant', 'business', 'booktou' , 'Cash using Agent UPI', 'Cash to Agent')";  
            }
        
        if( strcasecmp($request->payType, "online")  == 0)
        {
            $paytype_where_normal  = "payment_type in ('Pay online (UPI)', 'online')"; 
            $paytype_where_pnd = "pay_mode = 'online'"; 

        }
        else if( strcasecmp($request->payType, "cash")  == 0  )
            {
               $paytype_where_normal  = "payment_type in ('Cash on delivery', 'OTC', 'POD', 'OTC')"; 
               $paytype_where_pnd = "pay_mode = 'cash'"; 
            }
            else 
            {
                $paytype_where_normal  = "payment_type in ('Pay online (UPI)', 'online', 'Cash on delivery', 'OTC', 'COD', 'POD')"; 
                $paytype_where_pnd = "pay_mode in ('online', 'cash', 'Cash on delivery', 'OTC', 'COD', 'POD' )"; 
            }
            
                $first = DB::table('ba_service_booking')
                ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
                ->wheredate('ba_service_booking.service_date', $todayDate)
                ->whereIn("book_status", array('completed', 'delivered') )  
                 ->where("ba_service_booking.route_to_frno",  $franchise)
                ->whereRaw($paytype_where_normal )
                ->select('ba_service_booking.id', 
                  DB::Raw("'customer' source"), 
                  "ba_business.id as bin" ,   
                  'total_cost as orderCost', 
                  DB::Raw("'0.00' packingCost"), 
                  'delivery_charge as deliveryCharge', 
                  'discount',  'payment_type  as paymentType',
                  'book_status as bookingStatus', 
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",
                  DB::Raw("'booktou' paymentTarget"),
                  DB::Raw("'normal' orderType")) ;

                $second = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
                ->whereIn("book_status", array('completed', 'delivered') ) 
                ->where("request_from", "customer")
                ->where("route_to_frno",  $franchise)
                ->whereRaw($paytype_where_pnd )
                ->whereRaw($paytarget_where_pnd )
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"))
                ->orderBy("id", "asc")   ;
         

                $result = DB::table('ba_pick_and_drop_order')
                ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
                ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
                ->whereIn("book_status", array('completed', 'delivered') ) 
                ->where("request_from", "business")
                ->where("route_to_frno",  $franchise)
                ->whereRaw($paytype_where_pnd )
                ->whereRaw($paytarget_where_pnd )
                ->union($first) 
                ->union($second) 
                ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"))
                ->orderBy("id", "asc")  
                ->get();


 

        


        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 
  



        $data = array(  'sales' =>  $result , 
            'cart_items' =>$normal_sales_cart,  
            'date' =>  $todayDate,
            'businesses' => $all_business );  

        return view("admin.billing.sales_report_per_day")->with(  $data);  
    }



  public function monthlyTaxableSalesReport(Request $request) 
  {
        if( $request->month  == "")
        { 
            $month   =  date('m');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
        else 
        {
          $month   =  $request->month ;
          $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
 

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }
 


        $first = DB::table('ba_service_booking')
        ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
        ->whereMonth('ba_service_booking.service_date', $month)
        ->whereYear('ba_service_booking.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("ba_business.frno", "0") 
        ->select('ba_service_booking.id', 
          DB::Raw("'customer' source"), 
          "ba_business.id as bin" ,   
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', 
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",
          DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType"), "book_date", "service_date") ;

        $second = DB::table('ba_pick_and_drop_order')
        ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
        ->whereMonth('ba_pick_and_drop_order.service_date', $month)
        ->whereYear('ba_pick_and_drop_order.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "customer")
        ->where("ba_users.franchise", "0") 
        ->select('ba_pick_and_drop_order.id',  
          "source",
          DB::Raw("'0' bin"),
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  
          'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'pickup_name as name', 
          DB::Raw("'customer' category"), 
          DB::Raw("'0' commission"),       
          "payment_target as paymentTarget",
          DB::Raw("'assist' orderType"), "book_date", "service_date")
        ->orderBy("id", "asc")   ;



        $result = DB::table('ba_pick_and_drop_order')
        ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
        ->whereMonth('ba_pick_and_drop_order.service_date', $month)
        ->whereYear('ba_pick_and_drop_order.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "business")
        ->where("ba_business.frno", "0") 
        ->union($first) 
        ->union($second) 
        ->select('ba_pick_and_drop_order.id',  
          "source","ba_business.id as bin",
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",          
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"), "book_date", "service_date")
        ->orderBy("id", "asc")  
        ->get();


        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 
   


        $data = array(  'sales' =>  $result , 
            'cart_items' =>$normal_sales_cart,  
            'monthname' =>  $monthname,
            'businesses' => $all_business );  

        return view("admin.reports.monthly_tax_report")->with(  $data);  
 
  }


  public function orderTaxDetails(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("//admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }
 
        
        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
         

 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  


        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business );  

        return view("admin.reports.order_tax_report")->with(  $data);  
 
  }


  public function viewVoucherDetails(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("//admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }
 
        
        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
         

 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  


        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business );  

        return view("admin.reports.order_tax_report")->with(  $data); 
 
  }



  public function prepareVoucherEntry(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("//admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }

        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
          
 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  

        $month = date('m', strtotime($order_info->book_date));
        $year  = date('Y', strtotime($order_info->book_date));

        $account_types = DB::table('bta_accounts')
        ->get() ;


        $balance_sheet = DB::table('bta_order_journal')
        ->join('bta_journal_entry', 'bta_journal_entry.vno', '=', 'bta_order_journal.vno') 
        ->whereMonth("voucher_date", $month  )
        ->whereYear("voucher_date", $year )
        ->where("bta_order_journal.vno", "<=",  $orderno )
        ->select( "bta_journal_entry.ac_key_name" , "bta_journal_entry.rule",
            DB::Raw(" sum(debit) as debit") , 
            DB::Raw(" sum(credit) as credit")  ) 
        ->groupBy("bta_journal_entry.ac_key_name" ) 
        ->groupBy("bta_journal_entry.rule" ) 
        ->get() ;
         
 

        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business,
            'account_types' =>$account_types ,
            'balance_sheet' => $balance_sheet);  
 

        return view("admin.reports.order_to_voucher_entry")->with(  $data); 
 
  }


 

  public function updateVoucherEntry(Request $request) 
  {
    if( $request->o  == "")
    {
        return redirect("//admin/accounting/monthly-taxable-sales-report")
        ->with("err_msg", "No order selected!"); 
    }
    else 
    {
        $orderno   =  $request->o ; 
    }
    
    if($orderno < 13876     )
    {
        return redirect("//admin/accounting/monthly-taxable-sales-report")
        ->with("err_msg", "No order selected!");  
    }

    //finding order
     $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"),  
                      "customer_name as customer",
                      "address",
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source",
                   "drop_name as customer",
                   "drop_address as address",
                  "ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                   "drop_name as customer",
                   "drop_address as address",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
    if( !isset($order_info) )
    {
        return redirect("/admin/accounting/monthly-taxable-sales-report")->with("err_msg", "No order selected!"); 
    }

    //saving new voucher or updating existing one
    $order_journal = OrderVoucherModel::find( $orderno) ;
    if( !isset($order_journal))
    {
        $order_journal = new OrderVoucherModel ;
    }
    $order_journal->vno  = $orderno ;
    $order_journal->voucher_date = $order_info->book_date;
    $order_journal->cust_name = $order_info->customer;
    $order_journal->cust_address = $order_info->address;
    $order_journal->related_order =$orderno;
    $order_journal->save();


    $cash_dr  = $request->tbcasset;
    $merchant_cr  = $request->tbmerlbt;
    $comm_cr  = $request->tbcommin; 
    $delivery_cr  = $request->tbdelin; 
    $cgst_cr  = $request->tbcgstlbt;
    $sgst_cr  = $request->tbsgstlbt;

    //making voucher entries
    //finding existing entry for cash_asset
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "cash_asset")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno =  $orderno ;
    $voucher_entry->ac_title = "Cash A/c";
    $voucher_entry->ac_key_name = "cash_asset";
    $voucher_entry->ac_type = "asset";
    $voucher_entry->debit = $cash_dr;  
    $voucher_entry->rule= "dr"; 
    $voucher_entry->save();

    //finding existing entry for merchant_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "merchant_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Merchant A/c";
    $voucher_entry->ac_key_name = "merchant_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $merchant_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();


    //finding existing entry for comm_income
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "comm_income")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Commission Income A/c";
    $voucher_entry->ac_key_name = "comm_income";
    $voucher_entry->ac_type = "income"; 
    $voucher_entry->credit= $comm_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();


    //finding existing entry for delivery_income
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "delivery_income")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Delivery Charge A/c";
    $voucher_entry->ac_key_name = "delivery_income";
    $voucher_entry->ac_type = "income"; 
    $voucher_entry->credit= $delivery_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    //finding existing entry for sgst_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "sgst_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Ouput SGST";
    $voucher_entry->ac_key_name = "sgst_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $sgst_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    
    //finding existing entry for cgst_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "cgst_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Output CGST";
    $voucher_entry->ac_key_name = "cgst_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $cgst_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    return redirect("/admin/accounting/prepare-voucher-entry?o=" . $orderno )
    ->with("err_msg", "Voucher entry updated!");

  }

    protected function cyclewiseSalesServiceReport(Request $request )
    {
         $bin = $request->bin; 
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 3;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                $startDay   =  1 ;  
                $endDay   =    7;
                $cycle = 1;
            } 
        }

         $first = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          "bin",
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
         ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
            "request_by as bin",
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get();  
 

       $data =  array(   
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart, 
        'businesses' => $all_business, 
        'cycle' => $cycle,
        'month' =>  $month , 
        'year' => $year  );  

       return view("admin.billing.sales_report_per_cycle")->with( $data ); 
     }



     // View Business Normal and PnD Sales Report for daily
     protected function dailySalesAndServiceMerchantWiseReport(Request $request )
     {

        $bin = $request->bin; 
        $frno = $request->frno; 
        
        if($request->reportDate == "")
        {
            $date   =  date('Y-m-d');  
        }
        else
        {
            $date   =  date('Y-m-d', strtotime($request->reportDate)); 
        }
 

        if($frno==0)//headquarter merchant clearance record
        {
            $first = DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("ba_business.frno", $frno )
            ->where("ba_service_booking.route_to_frno", $frno )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $result = DB::table('ba_pick_and_drop_order') 
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
            ->where("request_from", "business")
            ->where("request_by", $bin )
            ->where("ba_business.frno", $frno )
            ->where("ba_pick_and_drop_order.route_to_frno", $frno )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->union($first)  
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();

        }else{ //franchise clearance report for particular merchant

          $first = DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("ba_business.frno", $frno )
            ->whereDate("service_date", "=",   $date)
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $normal_franchise_route_to = DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("ba_service_booking.route_to_frno", $frno )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $pnd_franchise_route_to = DB::table('ba_pick_and_drop_order') 
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
            ->where("request_from", "business")
            ->where("request_by", $bin ) 
            ->where("ba_pick_and_drop_order.route_to_frno", $frno )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"));

            $result = DB::table('ba_pick_and_drop_order') 
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
            ->where("request_from", "business")
            ->where("request_by", $bin )
            ->where("ba_business.frno", $frno )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->union($first)
            ->union($normal_franchise_route_to) 
            ->union($pnd_franchise_route_to) 
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();

        } 
     
        
        $memberids = $bins = $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;
                $bins[] = $item->requestBy ;

            }
            if($item->orderType == "pnd")
            {
                $bins[] = $item->requestBy ;
            }
            if($item->orderType == "assist")
            {
                $memberids[] = $item->requestBy ;
            }
            
            
        }
        $oids[]= $bins[]= $memberids[]=0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 


        $businesses  =  DB::table("ba_business") 
        ->whereIn("id",  $bins  )  
        ->get();

        $profiles  =  DB::table("ba_profile") 
        ->whereIn("id",  $memberids  )  
        ->get();  

       

       $data =  array(  'businesses' =>$businesses, 
        'profiles' => $profiles,
        'results' =>  $result, 
        'cart_items' => $normal_sales_cart,
        'bin' => $bin ,'reportDate' => $date );
       return view("admin.billing.detailed_daily_sales_report_merchantwise")->with( $data ); 
     }


     public function generateDailyClearanceBill( Request $request)
    {
        $bin = $request->bin;  
        $frno = $request->frno;
        if($request->reportDate !="" )
        {
          $today = $request->reportDate ;
        }
        else
        {
            $today = date('Y-m-d' );
        }
 
        $business  = Business::find($bin);


        
        if ($frno==0) { //headquarter query
            
            $first = DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("service_date", $today ) 
            ->where("ba_business.frno",$frno)
            ->where("ba_service_booking.route_to_frno",$frno)   
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_service_booking.service_date")
            ->select('ba_service_booking.id', 
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              DB::Raw("'0.00' packingCost"), 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $orders = DB::table('ba_pick_and_drop_order')
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
            ->where("ba_pick_and_drop_order.request_by", $bin )  
            ->where("request_from", "business")
            ->where("service_date", $today )
            ->where("ba_business.frno",$frno)
            ->where("ba_pick_and_drop_order.route_to_frno",$frno)  
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_pick_and_drop_order.service_date")
            ->union($first)  
            ->select('ba_pick_and_drop_order.id', 
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              "packaging_cost as packingCost" , 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();
        }else{  //franchise query with route to franchise orders for normal and pnd
             
            $first = DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("service_date", $today ) 
            ->where("ba_business.frno",$frno) 
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_service_booking.service_date")
            ->select('ba_service_booking.id', 
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              DB::Raw("'0.00' packingCost"), 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ;

            $normal_route_to =  DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("service_date", $today ) 
            ->where("ba_service_booking.route_to_frno",$frno) 
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_service_booking.service_date")
            ->select('ba_service_booking.id', 
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              DB::Raw("'0.00' packingCost"), 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $pnd_route_to = DB::table('ba_pick_and_drop_order')
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
            ->where("ba_pick_and_drop_order.request_by", $bin )  
            ->where("request_from", "business")
            ->where("service_date", $today )
            ->where("ba_pick_and_drop_order.route_to_frno",$frno) 
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_pick_and_drop_order.service_date")
            ->select('ba_pick_and_drop_order.id', 
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              "packaging_cost as packingCost" , 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"));

            $orders = DB::table('ba_pick_and_drop_order')
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
            ->where("ba_pick_and_drop_order.request_by", $bin )  
            ->where("request_from", "business")
            ->where("service_date", $today )
            ->where("ba_business.frno",$frno) 
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_pick_and_drop_order.service_date")
            ->union($first)  
            ->union($normal_route_to)
            ->union($pnd_route_to)
            ->select('ba_pick_and_drop_order.id', 
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              "packaging_cost as packingCost" , 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();

        }

        

         

        $oids = array();
        foreach($orders as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 
 

       

       $franchise = DB::table("ba_franchise")
       ->where("frno",$frno)
       ->first();

       $data =  array(  'business' =>$business,   
        'orders' =>  $orders, 
       'cart_items' =>$normal_sales_cart,
       'bin' => $bin,  
       'reportDate' => $today ,
       'franchise'=>$franchise
       ); 

       $pdf_file =  "booktou_mcbill_" .time(). '_' . $bin . ".pdf";
       $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ;

        
        if ($frno==0) {
          
                if($request->format == "roll")
                {
                    $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('admin.billing.daily_clearance_bill_roll_paper' ,  $data  )   ; 
                    return view('admin.billing.daily_clearance_bill_roll_paper')->with( $data );
                }
                else  
                {


                    if( strcasecmp($request->download  , "yes" ) == 0)
                    { $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('admin.billing.daily_clearance_bill_a4' ,  $data  )   ;
                       return $pdf->download( $pdf_file  ); 
                       
                    }
                    else 
                    { 
                         $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('admin.billing.daily_clearance_bill_a4' ,  $data  )   ;
                        return view('admin.billing.daily_clearance_bill_a4')->with( $data );
                    }

                    
                }

        }else
        {
              if($request->format == "roll")
                {
                    $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('admin.billing.daily_clearance_bill_roll_paper' ,  $data  )   ; 
                    return view('admin.billing.daily_franchise_clearance_bill_roll_paper')->with( $data );
                }
                else  
                {


                    if( strcasecmp($request->download  , "yes" ) == 0)
                    { $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('admin.billing.daily_franchise_clearance_bill_a4' ,  $data  )   ;
                       return $pdf->download( $pdf_file  ); 
                       
                    }
                    else 
                    { 
                         $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('admin.billing.daily_clearance_bill_a4' ,  $data  )   ;
                        return view('admin.billing.daily_franchise_clearance_bill_a4')->with( $data );
                    }

                    
                }

        } 

    }



    protected function salesAndClearanceHistory(Request $request )
    {
 
        if($request->year != "")
        {
          $year  = $request->year ; 
        }
        else
        {
          $year  = date('Y' );
        }

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $payment_history =  DB::table("ba_payment_dealings") 
        ->whereMonth('cleared_on', $month )
        ->whereYear('cleared_on', $year )   
        ->orderBy("id", "desc")
        ->paginate(20);

        $bins = array();
        $order_no_str = "";
        foreach($payment_history as $item)
        {
          $order_no_str .=  $item->order_nos . ",";

          $bins[] = $item->bin;
        }

        $order_nos = explode(",", $order_no_str);

        $order_nos = array_unique(array_filter($order_nos));


        $booking_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->whereIn("ba_pick_and_drop_order.id", $order_nos )  
        ->select("ba_pick_and_drop_order.*",  "ba_business.name as orderBy" , DB::Raw("'na'  deliverBy") )
        ->get();
 


        $totalEarning= 0;
        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {

           if( $item->book_status != "cancel_by_client" && $item->book_status != "cancel_by_owner" && 
            $item->book_status != "canceled"  && $item->book_status != "returned")
           {
              $totalEarning += $item->total_amount;
           } 
            
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->id; 
        }

        $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                } 
            }   
       }
       $businesses  = DB::table("ba_business")
        ->whereIn("id", $bins ) 
        ->get();


        $data = array("totalEarning" => $totalEarning,  
        'results' =>  $booking_list, 'businesss' =>   $businesses , 'payment_history' => $payment_history);
       return view("admin.earning_clearance.merchant_clearance_report")->with('data',$data);

     }




     // View Business Normal and PnD Sales Report for monthly
     protected function migratedOrderSalesAndClearanceReport(Request $request )
     {
        $frno = 0;
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        } 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }
        $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

        

       $startDay   = 1;   
       if( $totalDays > 28)
       {
          $endDay   =  $totalDays; 
       } 
                     
        $first = DB::table('ba_service_booking') 
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->where("route_to_frno",  $frno  )
        ->where("original_franchise",  0)  
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
            'ba_service_booking.bin', 
          'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business") 
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->where("route_to_frno",  $frno  )
         ->where("original_franchise",  0) 
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select( 'ba_pick_and_drop_order.id',  
            'ba_pick_and_drop_order.request_by as bin', 
            'ba_pick_and_drop_order.service_date',
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

 


        $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 

       $all_business  = DB::table("ba_business") 
       ->join("ba_franchise", "ba_franchise.frno", "=", "ba_business.frno" )
       ->select("ba_business.*", "ba_franchise.zone" )
       ->get(); 

       

       $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

       $data =  array('businesses' => $all_business,
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart,  
        'start' =>  $startDay , 
        'end' =>  $endDay , 
        'month' =>  $month , 
        'year' => $year ,
        'theme'=>$theme );
       return view("admin.billing.migrated_sales_report_per_cycle_for_merchant")->with( $data ); 
     }



      protected function searchGstAndItrController(Request $request )
    {

        $from = date('Y-m-d' , strtotime("30 days ago"));
        $to =  date('Y-m-d');
        $up_to = date('Y-m-d', strtotime("+1 day",strtotime($to)));  
        
        if($request->from_date != '' && $request->to_date !='' )
        {
            $from = date('Y-m-d', strtotime($request->from_date));
            $to = date('Y-m-d', strtotime($request->to_date));
            $up_to = date('Y-m-d', strtotime("+1 day",strtotime($request->to_date)));

        }
        
        $page = isset($request->page) ? $request->page : 1;

        $orders = DB::table("ba_order_sequencer")
        ->whereBetween('entry_date',[$from, $up_to])
        ->get();

         $non_pnd = $pnd_or_assist = array();

        foreach($orders as $order)
        {
            if($order->type == 'pnd' || $order->type == 'assist')
            {
                $pnd_or_assist[] = $order->id;
            }

            else
                $non_pnd[] = $order->id;
        }

        
        $non_pnd_orders = DB::table('ba_service_booking')
        ->whereIn('id',$non_pnd)
        ->orderBy("ba_service_booking.book_date")
        ->select('id as orderNo','bin',
        'book_date as orderDate',
         DB::Raw("'NA' order_type"),
         'source', 
         'book_status as status',
         'payment_type as paymentMode',
         DB::Raw("'booktou' target"),
         'total_cost as totalCost',
         'packing_charge as packingCharge',
         DB::Raw("'0' vendorDiscount"),
         'discount as bookTouDiscount',
         'refunded',
         'delivery_charge',
         DB::Raw("'0' comissionRate"),
         'seller_payable',
         DB::Raw("'0' dueOnMerchant"),
         'clerance_status',
         'payment_status')
        ->get();

        foreach($orders as $order)
        {
            foreach($non_pnd_orders as $non_pnd_order )
            {
                if($non_pnd_order->orderNo == $order->id)
                    $non_pnd_order->order_type = $order->type;
            }
        }
        

        $pnd_orders = DB::table('ba_pick_and_drop_order')  
        ->whereIn('id',$pnd_or_assist)
        ->orderBy("ba_pick_and_drop_order.service_date")  
        ->select('id as orderNo','request_by as bin',
        'book_date as orderDate',
         DB::Raw("'NA' order_type"),
         'request_from as source', 
         'book_status as status',
         'pay_mode as paymentMode',
         'payment_target as target',
         'total_amount as totalCost',
         'packaging_cost as packingCharge',
         DB::Raw("'0' vendorDiscount"),
         DB::Raw("'0' bookTouDiscount"),
         'refunded',
         'service_fee as delivery_charge',
         DB::Raw("'0' comissionRate"),
         'seller_payable',
         'due_to_merchant as dueOnMerchant',
         'clerance_status',
         DB::Raw("'NA' payment_status"))  
        ->get(); 




        foreach($orders as $order)
        {
            foreach($pnd_orders as $pnd_order )
            {
                if($pnd_order->orderNo == $order->id)
                    $pnd_order->order_type = $order->type;
            }
        }

       
        $all_orders = $pnd_orders->merge($non_pnd_orders);
        $sorted_orders = $all_orders->sortBy('orderNo');


       $bins =$sorted_orders->pluck('bin');

 
        $commision_rate  = DB::table("ba_business_commission")  
        ->whereIn("bin", $bins ) 
        ->select('bin','commission', 'month', 'year')
        ->get(); 

        foreach($sorted_orders as $order_info)
        {

            if(strcasecmp($order_info->paymentMode,'Cash on delivery') == 0)
            {
                $order_info->paymentMode = 'COD';
            }

            $month= date('n',strtotime($order_info->orderDate));
            $year= date('Y',strtotime($order_info->orderDate));


             foreach ($commision_rate as $rate)
              {
                 if($order_info->bin == $rate->bin && $rate->month == $month && $rate->year == $year )
                  $order_info->comissionRate = $rate->commission;  
             }
        }

        if(strcasecmp($request->download, "export_excel")==0)
        {

            $order_collection = collect($sorted_orders);
            return (new AllOrdersList($order_collection))->download('order-list.xlsx');
        }
        

        $combine_orders = $sorted_orders->toArray();

        $combine_orders = $this->paginate($combine_orders, 50, $page);
        $combine_orders->withPath('');

       $data =  array('orders' => $combine_orders, 'from_date' => $from, 'upto_date' => $to);

         return view("admin.accounts.view_gst_itr")->with($data);
    }


     

}

