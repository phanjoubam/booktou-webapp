<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

use App\Libraries\FirebaseCloudMessage;



use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;


use App\Imports\ProductImport;


use App\Traits\Utilities; 

class CMSController  extends Controller
{
	 
 use Utilities;
 

    public function cmsDashboard(Request $request)
    {
      $status = array( 'delivered','completed' ) ; 
      $today = date('Y-m-d');

      $cms_main_menu = DB::table("cms_main_menu")->get();
      $cms_sub_menu = DB::table("cms_sub_menu")->get();

          

      return view("admin.cms.dashboard_cms")->with("data",  array(
          "main_menu_items" => $cms_main_menu,
          "sub_menu_items" => $cms_sub_menu,  
          'breadcrumb_menu' => 'cms_sub_menu'
    ) ); 
    }   




    
    public function viewExistingCMSMenu(Request $request)
    {
      

      $status = array( 'delivered','completed' ) ; 
      $today = date('Y-m-d'); 

      $sales_summary = DB::table("ba_service_booking") 
      ->whereRaw("date(service_date) >= '$today' " )  
      ->whereIn("book_status", $status)   
      ->select( DB::Raw("sum(total_cost) as totalSold"),  DB::Raw("sum(delivery_charge) as conveyance"), 
        DB::Raw("sum(agent_payable) as agentPayable"), DB::Raw("sum(service_fee) as serviceFee")   )
      ->first();
 
     $target_achieved = DB::table("ba_service_booking") 
      ->whereRaw("date(service_date) >= '$today' " )  
      ->whereIn("book_status", $status)   
      ->select( DB::Raw("count(*) as totalSold"))
      ->first();
    
    
    

      $new_signup = DB::table("ba_profile") 
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->whereRaw("date(ba_users.created_at) = '$today' " ) 
      ->select("ba_profile.*", "ba_users.id", "ba_users.created_at", "ba_users.otp")
      ->get(); 


       $new_business = DB::table("ba_business") 
      ->join("ba_users", "ba_users.bin", "=", "ba_business.id")
      ->whereRaw("date(ba_users.created_at) = '$today' " ) 
      ->select("ba_business.*", "ba_users.id", "ba_users.created_at")
      ->get(); 
        
        
        
      $active_users =  DB::table("ba_profile") 
      ->join("ba_active_user_log", "ba_active_user_log.member_id", "=", "ba_profile.id")
      ->whereRaw("date(ba_active_user_log.log_date) = '$today'" ) 
      ->select("ba_profile.id", "ba_profile.fullname", DB::Raw("count(*) as openCount") )
      ->groupBy("ba_profile.id")
      ->get();
         


      return view("cms.dashboard_cms")->with("data",  array(
          "active_users" => $active_users,
          "sales" => $sales_summary, 
          "target_achieved" =>$target_achieved,
          'new_customers' => $new_signup, 
          'new_business' => $new_business, 
          'breadcrumb_menu' => 'cms_sub_menu'
    ) ); 
    }

 
}
