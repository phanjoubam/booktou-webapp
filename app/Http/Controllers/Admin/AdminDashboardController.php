<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;
use App\BusinessBankAccountModel;
use App\User; 
use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;
use App\BusinessPaymentModel;
use App\Franchise;
use App\BusinessCommissionModel;
use App\DailyDepositModel;
use App\FeedbackMerchant;
use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\MonthlyAgentsOrdersExport;
use App\Exports\DailyAgentsOrdersExport;
use App\Exports\DailySalesOrderExport;
use App\Exports\MonthlyCommissionExport;
use App\Traits\Utilities; 
use PDF;
use App\DepositTargetModel;
use App\ProductVariantModel;
use App\ImportProductFileModel;
use App\SectionTypeModel;
use App\SectionDetailsModel;
use App\AppSectionModel;
use App\AppSectionDetailsModel;
use App\PosSubscription;
use App\SelfPickUpModel;
use App\PosPayment;
use App\BrandModel;
use App\ServiceCategoryModel;
use App\AgentEarningModel;
use App\AgentSalaryAdvanceModel;
use App\PopularProductsModel;
use App\AddDiscountProductsModel;
use App\WaterBookingModel;


class AdminDashboardController  extends Controller
{
    use Utilities;
  
    public function dashboard(Request $request)
    {

      try
      {
       $fcm_token =  $request->token ;

       echo $fcm_token ;

      }catch(\Exception $e)
      {
        report($e);

      }


      $status = array('new', 'confirmed','in_queue',  'order_packed', 'in_route', 'delivered', 'completed',  'package_picked_up', 'delivery_scheduled' ); 
      $today = date('Y-m-d');

      $sales_summary = DB::table("ba_service_booking") 
      ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
      ->whereRaw("date(ba_service_booking.service_date) = '$today' " )  
      ->whereIn("ba_service_booking.book_status",  array( 'delivered', 'completed' )  )   
      ->where("ba_business.frno", 0  ) 
      ->where("book_category", "<>", "BEAUTY")
      ->select( DB::Raw("sum(ba_service_booking.total_cost) as totalSold"),  
        DB::Raw("sum(ba_service_booking.delivery_charge) as conveyance"), 
        DB::Raw("sum(ba_service_booking.agent_payable) as agentPayable"), 
        DB::Raw("sum(ba_service_booking.service_fee) as serviceFee")   )
      ->first();

      $pnd_assists_revenue = DB::table("ba_pick_and_drop_order") 
      ->whereRaw("date(service_date) = '$today' " )  
      ->whereIn("book_status",  array( 'delivered', 'completed' )  ) 
      ->select(  DB::Raw("sum(service_fee) as serviceFee")   )
      ->first();

      $target_achieved = DB::table("ba_service_booking") 
      ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
      ->whereRaw("date(service_date) >= '$today' " )  
      ->whereIn("book_status", $status)   
      ->where("ba_business.frno", 0  ) 
      ->where("book_category", "<>", "BEAUTY")
      ->select( DB::Raw("count(*) as totalSold"))
      ->first();


      $active_status = array('new', 'confirmed','in_queue',  'order_packed', 'in_route', 'package_picked_up', 'delivery_scheduled' );

      $normal_orders = DB::table("ba_service_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("ba_service_booking.service_date", ">=",$today )    
      ->where("ba_order_sequencer.type", "normal") 
      ->confirmed("ba_service_booking.book_status",$active_status)
      ->count();
      $normal_completed_orders = DB::table("ba_service_booking") 
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("ba_service_booking.service_date", ">=",$today )    
      ->where("ba_order_sequencer.type", "normal") 
      ->whereIn("ba_service_booking.book_status",array(  'completed', 'delivered' ))
      ->count();

      $total_pick_drop = DB::table("ba_pick_and_drop_order")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_pick_and_drop_order.id") 
      ->whereDate("ba_pick_and_drop_order.service_date", '>=', $today )
      ->where("ba_order_sequencer.type", "pnd")  
      ->whereIn("ba_pick_and_drop_order.book_status", $active_status)  
      ->count();
      $total_completed_pick_drop = DB::table("ba_pick_and_drop_order")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_pick_and_drop_order.id") 
      ->whereDate("ba_pick_and_drop_order.service_date", '>=', $today )
      ->where("ba_order_sequencer.type", "pnd")  
      ->whereIn("ba_pick_and_drop_order.book_status", array(  'completed', 'delivered' ) )  
      ->count();

      $total_assist_order = DB::table("ba_pick_and_drop_order")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_pick_and_drop_order.id") 
      ->whereDate("ba_pick_and_drop_order.service_date", '>=', $today )
      ->where("ba_order_sequencer.type", "assist")  
      ->whereIn("ba_pick_and_drop_order.book_status", $active_status)  
      ->count();
      $total_completed_assist_order = DB::table("ba_pick_and_drop_order")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_pick_and_drop_order.id") 
      ->whereDate("ba_pick_and_drop_order.service_date", '>=', $today )
      ->where("ba_order_sequencer.type", "assist")  
      ->whereIn("ba_pick_and_drop_order.book_status", array(  'completed', 'delivered' ))  
      ->count();
 
      $new_signup = DB::table("ba_profile") 
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->whereRaw("date(ba_users.created_at) = '$today' " ) 
      ->select("ba_profile.*", "ba_users.id as userId", "ba_users.created_at", "ba_users.otp")
      ->get(); 

      $new_business = DB::table("ba_business") 
      ->join("ba_users", "ba_users.bin", "=", "ba_business.id")
      ->whereRaw("date(ba_users.created_at) = '$today' " ) 
      ->select("ba_business.*", "ba_users.id", "ba_users.created_at")
      ->get();


      $active_users =  DB::table("ba_profile") 
      ->join("ba_active_user_log", "ba_active_user_log.member_id", "=", "ba_profile.id")
      ->whereRaw("date(ba_active_user_log.log_date) = '$today'" ) 
      ->select("ba_profile.id", "ba_profile.fullname", DB::Raw("count(*) as openCount") )
      ->groupBy("ba_profile.id")
      ->get();


      $active_agents  = DB::table("ba_business") 
      ->join("ba_users", "ba_users.bin", "=", "ba_business.id")
      ->whereRaw("date(ba_users.created_at) = '$today' " ) 
      ->select("ba_business.*", "ba_users.id", "ba_users.created_at")
      ->get();
      
 
      $report_date =  date('Y-m-d', strtotime("-10 days")); 

      $pnd_sales_chart_data = DB::table("ba_pick_and_drop_order")
      ->whereDate("service_date", ">=", $report_date )
      ->where("book_status", "delivered" ) 
      ->select( DB::Raw("date( service_date ) as serviceDate"), DB::Raw("count(*) as pndOrders")  )
      ->groupBy("serviceDate")
      ->get();

      $normal_sales_chart_data = DB::table("ba_service_booking")
      ->whereDate("service_date", ">=", $report_date ) 
      ->where("book_status", "delivered" ) 
      ->where("book_category", "<>", "BEAUTY")
      ->select( DB::Raw("date( service_date ) as serviceDate"), DB::Raw("count(*) as normalOrder")  )
      ->groupBy("serviceDate")
      ->get(); 


      $pnd_sales_earning_data = DB::table("ba_pick_and_drop_order")
      ->whereDate("service_date", ">=", $report_date )
      ->where("book_status", "delivered" ) 
      ->select( DB::Raw("date( service_date ) as serviceDate"), DB::Raw("sum(service_fee) as totalRevenue")  )
      ->groupBy("serviceDate")
      ->get();


    
      /*
      $migrated_users   = DB::table("ba_profile")
      ->whereIn("phone", function($query) use ($today){
        $query->select("drop_phone")
        ->from("ba_pick_and_drop_order")
         ->whereDate("created_at", $today  ); 
      })   
      ->select("ba_profile.*" )
      ->orderBy("id", "desc")
      ->paginate(10);
      */



      $migrated_users   = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("ba_users.category", 0)
      ->whereDate("ba_profile.created_at", $today  )
      ->whereIn("ba_profile.phone", function($query) use ($today){
        $query->select("drop_phone")
        ->from("ba_pick_and_drop_order")
        ->whereDate("ba_pick_and_drop_order.book_date", $today  ) ; 
      })   
      ->select("ba_profile.*" )
      ->orderBy("ba_profile.id", "desc")
      ->paginate(10);



      $sales_target   = DB::table("ba_sales_target")   
      ->where("month", date('m'))
      ->where("year", date('Y'))
      ->where("frno",  0 )
      ->first();


      $pnd_income_achieved = DB::table("ba_pick_and_drop_order")   
      ->whereMonth("service_date", date('m') )  
      ->whereYear("service_date", date('Y') )   
      ->where("book_status", "delivered") 
      ->select(   DB::Raw("sum(service_fee) as deliveryCommission")  ) 
      ->first();

      $total_remark_count = DB::table("ba_order_remarks")   
      ->whereDate("created_at", $today  )  
      ->distinct()
      ->count('order_no');

      //payment cycle count for
      $msg_payment="";
      $month = date('m');
      $year = date('Y');
      $current_month = date('Y-m-d');
      $no_of_days_for_current_month = date('t');
      $day = date("j");  
      
      
      switch ($day) {
        case '7':
           $filter_date = date('Y-m-d',strtotime($year."-".$month."-".$day));
           
          break;
        case '14':
           $filter_date = date('Y-m-d',strtotime($year."-".$month."-".$day));
           
          break;
        case '21':
           $filter_date = date('Y-m-d',strtotime($year."-".$month."-".$day)); 
           
          break;
        case '28':
           $filter_date = date('Y-m-d',strtotime($year."-".$month."-".$day)); 
          
          break;
      }

      if ($no_of_days_for_current_month==$day) {
        $filter_date = date('Y-m-d',strtotime($year."-".$month."-".$day)); 
        
      }

     

          
      //cycle payment check ends here
      if (isset($filter_date)) {

        $pnd_list  = DB::table("ba_pick_and_drop_order")
        ->whereRaw("date(ba_pick_and_drop_order.book_date)<= '$filter_date' ")
        ->whereYear('ba_pick_and_drop_order.book_date', $year )  
        ->where('clerance_status','un-paid')
        ->where('book_status','delivered')
        ->selectRaw( "count(*) as totalCount");

        $booking_list  = DB::table("ba_service_booking")
        ->whereRaw("date(ba_service_booking.book_date)<= '$filter_date' ")
        ->whereYear('ba_service_booking.book_date', $year ) 
        ->where('clerance_status','un-paid')
        ->where('book_status','delivered') 
        ->selectRaw( "count(*) as totalCount")
        ->union($pnd_list)
        ->get();

        if (count($booking_list) > 0) {
          
           $msg_payment="Upcoming Payment Reminder";
          } 
        }

    $order_cancel_first_query = DB::table("ba_service_booking")
    ->whereDate("service_date", ">=", $report_date )
    ->whereIn("book_status",  array(  'cancel_by_client','cancel_by_owner',   'canceled','cancelled'  ) )
    ->where("ba_service_booking.route_to_frno", 0)
    ->select( DB::Raw("date( service_date ) as serviceDate"), DB::Raw("count(*) as totalCancelled")  ) 
    ->groupBy("serviceDate");

    $order_cancel_rate = DB::table("ba_pick_and_drop_order")
    ->whereDate("service_date", ">=", $report_date )
    ->whereIn("book_status",  array(  'cancel_by_client','cancel_by_owner',   'canceled','cancelled'  ) )
    ->where("ba_pick_and_drop_order.route_to_frno", 0)
    ->union($order_cancel_first_query)
    ->select( DB::Raw("date( service_date ) as serviceDate"), DB::Raw("count(*) as totalCancelled")  ) 
    ->groupBy("serviceDate")
    ->get();

    if( $request->session()->get('_user_role_' ) == "1000" )

      {

        return view("admin.dashboard_cc")->with("data",
        array(
        "active_users" => $active_users,
        "sales" => $sales_summary, 
        "target_achieved" =>$target_achieved,
        'normal_orders' => $normal_orders,
        'pick_and_drop' =>$total_pick_drop,
        'total_completed_pick_drop' =>$total_completed_pick_drop,
        'normal_completed_orders' =>$normal_completed_orders,
        'total_completed_assist_order' =>$total_completed_assist_order,
        'total_assist_order'=>$total_assist_order,
        'new_customers' => $new_signup, 
        'new_business' => $new_business,
        'pnd_sales_chart_data' => $pnd_sales_chart_data,
        'normal_sales_chart_data' => $normal_sales_chart_data,
        'pnd_assists_revenue' =>$pnd_assists_revenue,
        'pnd_sales_earning_data' => $pnd_sales_earning_data,
        'migrated_users' => $migrated_users ,
        'sales_target' =>$sales_target ,
        'pnd_income_achieved' =>  ( $pnd_income_achieved == null ) ? 0.00 :  $pnd_income_achieved->deliveryCommission,
        'total_remark_count' => $total_remark_count,
        'order_cancel_rate' => $order_cancel_rate,
      ) ); 
      }
      else 
      { 
        return view("admin.dashboard_admin")->with("data",
        array(
        "active_users" => $active_users,
        "sales" => $sales_summary, 
        "target_achieved" =>$target_achieved,
        'normal_orders' => $normal_orders,
        'pick_and_drop' =>$total_pick_drop,
        'total_assist_order'=>$total_assist_order,
        'total_completed_pick_drop' =>$total_completed_pick_drop,
        'normal_completed_orders' =>$normal_completed_orders,
        'total_completed_assist_order' =>$total_completed_assist_order,
        'new_customers' => $new_signup, 
        'new_business' => $new_business,
        'pnd_sales_chart_data' => $pnd_sales_chart_data,
        'normal_sales_chart_data' => $normal_sales_chart_data,
        'pnd_sales_earning_data' => $pnd_sales_earning_data,
        'pnd_assists_revenue' =>$pnd_assists_revenue,
        'migrated_users' => $migrated_users ,
        'sales_target' =>$sales_target,
        'pnd_income_achieved' =>  ( $pnd_income_achieved == null ) ? 0.00 :  $pnd_income_achieved->deliveryCommission, 'total_remark_count' => $total_remark_count,
        'reminder'=>$msg_payment,'day'=>$day,
        'order_cancel_rate' => $order_cancel_rate,
      )); 
      }
  
      
    } 

    protected function viewAllOrders(Request $request)
    {      
      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)) ; 
      }
      else 
      {
        $orderDate = date('Y-m-d' , strtotime("1 days ago"))  ; 
      }


      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );


        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
          if( $dbstatus == "-1")
          {
            $status = array( 'new','confirmed', 'cancel_by_client', 'cancelled', 'delivered', 
              'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
              'package_picked_up','delivery_scheduled' ) ; 
          }
          else 
          {
            $status[] =  $dbstatus;
          }
        }
        else if(strcasecmp($dbstatus,"new")==0)
        {
         $status =array("new");
       }


       else 
       {
        $status = array( 'new','confirmed', 'cancel_by_client', 'cancelled', 'delivered', 
          'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
          'package_picked_up','delivery_scheduled' ) ; 
      }

    }
    else 
    {

      $request->session()->remove('_last_search_date' );

      $status = array( 'new','confirmed',  'order_packed','pickup_did_not_come','in_route',  'package_picked_up','delivery_scheduled' ) ; 
    }


    $day_bookings = DB::table("ba_service_booking")
    ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
    ->whereDate("ba_service_booking.book_date", "=", $orderDate )
    ->where("ba_order_sequencer.type",  "normal")
    ->whereIn("ba_service_booking.book_status", $status)  
    ->whereIn("ba_service_booking.bin", function($query)   { 
      $query->select("id")
      ->from("ba_business")
      ->where("frno",   0 ); 
    }) 
    ->orderBy("ba_service_booking.service_date", "asc")
    ->select("ba_service_booking.*" )
    ->get();



    if($request->filter_date == "")
    {
      $orderDate = date('Y-m-d') ;

      $day_bookings = DB::table("ba_service_booking")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("ba_service_booking.service_date", ">=", $orderDate )
      ->where("ba_order_sequencer.type",  "normal")
      ->whereIn("ba_service_booking.book_status", $status)  
      ->whereIn("ba_service_booking.bin", function($query)   { 
        $query->select("id")
        ->from("ba_business")
        ->where("frno",   0 ); 
      }) 
      ->orderBy("ba_service_booking.service_date", "asc")
      ->select("ba_service_booking.*" )
      ->get();

    }


    $order_ids = array();
    $all_bins = array();
    $book_by = array();
    $phone_nos = array();
    foreach($day_bookings as $bitem )
    {
      $all_bins[] = $bitem->bin; 
      $book_by[] = $bitem->book_by;
      $phone_nos[] =$bitem->customer_phone;
      $order_ids[] = $bitem->id; 
    }


    $flag_customers = DB::table("ba_ban_list")
    ->whereIn("phone", $phone_nos)    
    ->get();


    $all_bins = array_filter($all_bins);

    $all_businesses = DB::table("ba_business")
    ->whereIn("id", $all_bins)    
    ->get();


    $order_statuses = array(   'completed', 'order_packed' , 'in_route','delivered' , 'package_picked_up','delivery_scheduled' );

    $book_by = array_filter($book_by);

    $book_status = array( 'cancel_by_owner','pickup_did_not_come', 'cancel_by_client',  'canceled','order_packed' ) ; 


    $book_counts = DB::table("ba_service_booking")
    ->whereIn("book_by", $book_by)    
    ->whereNotIn("book_status", $book_status)  
    ->select("book_by", DB::raw('count(*) as totalOrders') )
    ->groupBy("book_by")
    ->get(); 


        //finding free agents  
    $today = date('Y-m-d');  
    $all_agents = DB::table("ba_profile")
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
    ->whereIn("ba_profile.id", function($query) use ( $orderDate ) { 
      $query->select("agent_id")
      ->from("ba_agent_attendance")
      ->whereDate("log_date",   date('Y-m-d') ); 
    })

    ->where("ba_users.category", 100)
    ->where("ba_users.status", "<>" , "100") 
    ->where("ba_profile.fullname", "<>", "" )
    ->where("ba_users.franchise", 0) 
    ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 
    ->get();




    $delivery_orders = DB::table("ba_delivery_order")
    ->whereRaw(" date(accepted_date) = '$today'" )
    ->where("completed_on", null )
    ->where("order_type",  'client order'  )
    ->select("member_id", DB::Raw("count(*) as cnt") )
    ->groupBy('member_id')
    ->get();

    $free_agents = array();


        //Fetching assigned orders
    $delivery_orders_list = DB::table("ba_delivery_order")
    ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
    ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
    ->whereRaw(" date(accepted_date) ='$today'" )
    ->get(); 


    foreach ($all_agents as $item)
    {
     $is_free = true  ;
     $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
     foreach($delivery_orders as $ditem)
     {
      if($item->id == $ditem->member_id )
      {
        $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
        if( $ditem->cnt >= 50 )
        {
          $is_free = false ; 
          break;
        } 
      } 
    }

    if(  !$is_free )
    {
      $item->isFree = "no";
    }



           //image compression
    if($item->profile_photo != null )
    {
      $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
      $pathinfo = pathinfo( $source  );
      $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
      $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

      if(file_exists(  $source ) &&  !file_exists( $destination ))
      {
        $info = getimagesize($source ); 

        if ($info['mime'] == 'image/jpeg')
          $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png')
          $image = imagecreatefrompng($source);


        $original_w = $info[0];
        $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                 0, 0,
                 0, 0,
                 $original_w, $original_h,
                 $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

          }


          $last_keyword = "0";
          if($request->session()->has('_last_search_' ) ) 
          {
            $last_keyword = $request->session()->get('_last_search_' );
          }


          $last_keyword_date = date('d-m-Y') ;
          if($request->session()->has('_last_search_date' ) ) 
          {
            $last_keyword_date = $request->session()->get('_last_search_date' );
          }

          $global_settings = DB::table("ba_global_settings") 
          ->where("config_key", "end_hour" )
          ->first(); 


      //order notifications 
          $order_ids[]=0;
          $all_notifications = DB::table("ba_order_notifications") 
          ->whereIn("order_no", $order_ids ) 
          ->get(); 

// take away service list
          $selfpickup = DB::table("ba_business_selfpickup")
          ->where("status","active")
          ->get();
         //ends here
          $data = array( 
            'title'=>'Manage Orders' , 
            'results' => $day_bookings, 
            'flag_customers' => $flag_customers,
            'date' => $orderDate, 
            'all_agents' => $all_agents , 
            'delivery_orders_list' => $delivery_orders_list, 
            'all_businesses' =>$all_businesses, 
            'book_counts' => $book_counts,
            'last_keyword' => $last_keyword, 
            'last_keyword_date' =>  $last_keyword_date , 
            'refresh' => 'true','selfpickup' =>$selfpickup,
            'global_settings' => $global_settings, 'all_notifications' =>$all_notifications );

          return view("admin.orders.all_orders_table")->with('data',$data);
        }



    //update delivery date
    protected function updateDeliveryDate(Request $request) 
    {
        if(  $request->orderno  == ""  ||  $request->servicedate  == "" ||  $request->tareason  == "" )
        {
            return redirect("/admin/customer-care/orders/view-all")->with("err_msg",  'Important field are missing.' );
        } 


        $order_info = ServiceBooking::find($request->orderno); 

        if(   !isset( $order_info)  )
        {
          return redirect("/admin/customer-care/orders/view-all")->with("err_msg",  'No matching order found.'  );
        }


        DB::table('ba_order_status_log')
        ->where('order_no', $request->orderno )
        ->increment('seq_no', 1);

        $orderStatus = new OrderStatusLogModel;
        $orderStatus->order_no =  $request->orderno  ; 
        $orderStatus->order_status = "service date changed";
        $orderStatus->log_date = date('Y-m-d H:i:s');
        $orderStatus->remarks = $request->tareason  ;
        $orderStatus->save(); 
 
        $order_info->service_date = date('Y-m-d', strtotime($request->servicedate ) );
        $order_info->save();
  
  
        return redirect("/admin/customer-care/orders/view-all")->with("err_msg",  'Order information updated!'  );

    }


    protected function viewCompletedOrders(Request $request)
    {
      
      $franchise =  0; 
      
      if( isset($request->btn_search) )
      {
        $orderDate = date('Y-m-d', strtotime($request->orderdate)); 
      }
      else 
      {
        $orderDate = date('Y-m-d'); 
      }

      
      $day_bookings= DB::table("ba_service_booking")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereDate("ba_service_booking.service_date",$orderDate) 
      ->whereIn("ba_service_booking.book_status",array(  'completed', 'delivered',  
        'pickup_did_not_come', 'returned', 'cancelled'))
      ->where("ba_order_sequencer.type","normal")
      ->where("route_to_frno", $franchise )
      ->orderBy("ba_service_booking.service_date", "asc")
      ->select("ba_service_booking.*",
       DB::Raw("'na'  agentName"),
       DB::Raw("'na'  agentPhone"),
       DB::Raw("'na'  profile_photo"), 
       DB::Raw("'na'  businessName"),
       DB::Raw("'na'  businessPhone"),
       DB::Raw("'na'  businessBanner")  
       )
      ->get();

     
        //finding free agents  
        $all_agents = DB::table("ba_profile")
        ->join("ba_delivery_order", "ba_delivery_order.member_id", "=", "ba_profile.id")
        ->select( "ba_profile.id", "ba_profile.fullname",   "ba_profile.phone", "ba_profile.profile_photo" , "ba_delivery_order.order_no" )  
        ->where("order_type", "client order")
        ->get();
 
         
   
        foreach ($all_agents as $item)
        {
            
           //image compression
            if($item->profile_photo != null )
            {
              $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

              if(file_exists(  $source ) &&  !file_exists( $destination ))
              {
                  $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
          

      $bins = array(0);
      foreach($day_bookings as $bitem)
      {
        $bins[] = $bitem->bin;

        foreach ($all_agents as $item)
        {    
            if($bitem->id == $item->order_no)
            { 
               $bitem->agentName = $item->fullname;
               $bitem->agentPhone = $item->phone;
               $bitem->profile_photo =    $item->profile_photo;

              break;
            }

          }
         }

         $businesses = DB::table("ba_business") 
        ->whereIn( "id", $bins)  
        ->get(); 

       $data = array( 'title'=>'Manage Orders' ,'results' => $day_bookings, 'date' => $orderDate, 'all_agents' => $all_agents, 'businesses' =>$businesses  );

         return view("admin.orders.all_in_completed_orders_table")->with('data',$data);
    }
 

     protected function viewOrderDetails(Request $request)
     {
        $cdn_url =   config('app.app_cdn')    ;  
        $cdn_path =  config('app.app_cdn_path')  ; 
        $cms_base_url =  config('app.app_cdn')   ;  
        $cms_base_path = $cdn_path  ;  
        $image  =   $cdn_url .  "/assets/image/no-image.jpg";
        $userid= $request->session()->get('__user_id_');
        $orderno = $request->orderno;      

        if($orderno == "")
        {
            return redirect("/admin/customer-care/orders/view-all"); 
        }
        $order_info   = DB::table("ba_service_booking") 
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
        ->select("ba_service_booking.*", "ba_business.name as businessName","ba_business.locality as businessLocality", 
          "ba_business.landmark as businessLandmark", "ba_business.city as businessCity", 
          "ba_business.state as businessState", "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",  "ba_business.banner" )
        ->where("ba_service_booking.id", $orderno )    
        ->first() ;
 
        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/orders/view-all"); 
        }

        $bin = $order_info->bin;

      $order_items   = DB::table("ba_shopping_basket")   
      ->join("ba_products", "ba_products.pr_code",  "=", "ba_shopping_basket.pr_code")
      ->where("ba_shopping_basket.order_no", $orderno )    
      ->select("ba_shopping_basket.*" , DB::Raw("'na' photos")  )
      ->get() ;
      

      if( count ($order_items) == 0)
      {
        $order_items   = DB::table("ba_shopping_basket")   
        ->join("ba_product_variant", "ba_product_variant.prsubid",  "=", "ba_shopping_basket.prsubid")
        ->where("ba_shopping_basket.order_no", $orderno )    
        ->select("ba_shopping_basket.*",  DB::Raw("'na' photos"))
        ->get() ;
      } 

      $customer_info   = DB::table("ba_profile") 
      ->where("id", $order_info->book_by )    
      ->first() ;

       
      if( !isset($customer_info))
      {
        return redirect("/admin/customer-care/orders/view-all"); 
      }
 

      $agent_info=  DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
      ->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
      ->where("order_no", $orderno  )
      ->where("order_type",  'client order'  )
      ->first(); 
 

      $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ;

      //images  
      $agent_uploaded_images =  DB::table("ba_agent_delivery_logs")  
      ->where("order_no",  $orderno )  
      ->select("image_url as imageUrl", "upload_date as uploadedDate" )
      ->get();
      

      $today = date('Y-m-d');

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        }) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0)
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();
      
      
      $subids =  array(); 
      foreach( $order_items as $item )
      {
          $subids[] = $item->prsubid;
      }
      $subids[] =0;

      $product_images =  DB::table('ba_product_photos') 
      ->whereIn('prsubid', $subids)
      ->select( "prsubid", "image_url" )   
      ->get();
 
      $image_url = "https://cdn.booktou.in/assets/image/no-image.jpg";

      foreach($order_items as $order_item)
      {
        foreach($product_images as $image )
        {
          if( $order_item->prsubid == $image->prsubid)
          {
            $image_url = $image->image_url ; 
            break;
          } 
        }
        
        $order_item->photos =  $image_url ; 
      }

      /* photo processing ends */


     $franchise_list    = DB::table("ba_franchise")   
      ->where("current_status", "active" )  
      ->select()  
      ->get() ;
 
      $routed_franchise = DB::table("ba_franchise")   
      ->where("current_status", "active" ) 
      ->where("frno",$order_info->route_to_frno) 
      ->first() ;
 


      $user_info = DB::table("ba_users")
      ->where('id',$userid)
      ->first();

      $adminFrno = $user_info->franchise;

      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("is_booktou_staff", "yes" )
      ->where("ba_users.status", "<>", "100" )  
      ->where("ba_users.category",   "100" )  
      ->where("franchise", 0)
      ->select("ba_profile.*", "ba_users.category" ) 
      ->get();



      $result = array('order_info' => $order_info , 
        'adminFrno' => $adminFrno ,
        'customer' =>  $customer_info, 
        'order_items' => $order_items, 
        'agent_info' => $agent_info, 
        'remarks' => $remarks ,
        'all_agents' =>$all_agents, 
        'staffs' =>$all_staffs,
        'franchise'=>$franchise_list, 
        'routed_frno'=>$routed_franchise,
        'agent_uploaded_images' => $agent_uploaded_images );

      return view("admin.orders.order_detailed_view")->with('data',$result);

     }


     protected function redirectToOrderDetailsView(Request $request)
     {
        
       $orderno = $request->orderno;

      if($orderno == "")
      {
        return redirect("/admin"); 
      }

      $order_info   = DB::table("ba_order_sequencer")
      ->where("id", $orderno)  
      ->first() ;

      if( !isset($order_info))
      {
        return redirect("/admin")->with("err_data", "No order information found!"); 
      }

      

      
      if( $order_info->type == "pnd")
      {
        return redirect("/admin/customer-care/pnd-order/view-details/" .  $orderno ); 
      }
      else if( $order_info->type == "assist")
      {
        return redirect("/admin/customer-care/assist-order/view-details/" .  $orderno ); 
      }
      else if( strcasecmp($order_info->type, "booking") == 0 || strcasecmp($order_info->type, "appointment") == 0 ||strcasecmp($order_info->type, "offers") == 0  )
      {

        $water_booking =WaterBookingModel::find("$orderno");
           if(isset($water_booking))
           {
             return redirect("/services/booking/view-details/$orderno/?bin={$water_booking->bin}");
           }
       else
           {
              $serv_booking =  ServiceBooking::find($orderno);  
                if(isset($serv_booking))
                   {
                       return redirect("/services/booking/view-details/$orderno/?bin={$serv_booking->bin}"); 
                    }
            }
      }

      else
      {
        return redirect("/admin/customer-care/order/view-details/" .  $orderno );  
      }
 
     }


    protected function viewPnDOrderDetails(Request $request)
    {
     

      $userid= $request->session()->get('__user_id_');

      $user_info = DB::table("ba_users")
      ->where('id',$userid)
      ->first();

      $adminFrno = $user_info->franchise;

      $orderno = $request->orderno;
      if($orderno == "")
      {
        return redirect("/admin/customer-care/business/view-all"); 
      }

      $pndOrder = PickAndDropRequestModel::find($orderno); 
      $from = $pndOrder->request_from;
      $order_info   = DB::table("ba_pick_and_drop_order") 
      ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
      ->select("ba_pick_and_drop_order.*", 
        "ba_business.name as businessName","ba_business.locality as businessLocality", 
        "ba_business.landmark as businessLandmark", "ba_business.city as businessCity", 
        "ba_business.state as businessState", "ba_business.pin as businessPin", 
        "ba_business.phone_pri as businessPhone",  "ba_business.banner" )
      ->where("ba_pick_and_drop_order.id", $orderno )    
      //->where("request_from", "business")
      ->where("request_from", $from)
      ->first() ;

      if( !isset($order_info))
      {
        return redirect("/admin/customer-care/business/view-all"  ); 
      }


      $agent_info=  DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
         ->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
         ->where("order_no", $orderno  )
         ->where("order_type",  "pnd" )
         ->first(); 

      $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ;


      $today = date('Y-m-d'); 
      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("agent_id")
          ->from("ba_agent_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();


      $banned_list    = DB::table("ba_ban_list")   
      ->where("phone", $order_info->pickup_phone )  
      ->orWhere("phone", $order_info->pickup_phone )     
      ->get() ;


      $franchise_list    = DB::table("ba_franchise")   
      ->where("current_status", "active" )  
      ->select()  
      ->get() ;
 
      $routed_franchise = DB::table("ba_franchise")   
      ->where("current_status", "active" ) 
      ->where("frno",$order_info->route_to_frno) 
      ->first() ;

      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("is_booktou_staff", "yes" )
      ->where("ba_users.status", "<>", "100" )  
      ->where("ba_users.category",   "100" )  
      ->where("franchise", 0)
      ->select("ba_profile.*", "ba_users.category" ) 
      ->get();

       //images  
       $agent_uploaded_images =  DB::table("ba_agent_delivery_logs")  
       ->where("order_no",  $orderno )  
       ->select("image_url as imageUrl", "upload_date as uploadedDate" )
       ->get();


      $result = array('order_info' => $order_info , 
      'banned_list' => $banned_list, 
      'agent_info' => $agent_info , 
      'all_agents' => $all_agents ,  
      'remarks' => $remarks ,
      'staffs' => $all_staffs, 
      'agent_uploaded_images' =>$agent_uploaded_images,
      'franchise'=>$franchise_list,'adminFrno'=>$adminFrno,'routed_frno'=>$routed_franchise);
      
      return view("admin.orders.pnd_order_detailed_view")->with('data',$result);


     }

     //assits view  
     protected function viewAssistOrderDetails(Request $request)
     {
      $orderno = $request->orderno;
      if($orderno == "")
      {
        return redirect("/admin/customer-care/business/view-all"); 
      }

      $pndOrder = PickAndDropRequestModel::find($orderno);
       
      $from = $pndOrder->request_from;

      $order_info   = DB::table("ba_pick_and_drop_order") 
      ->join("ba_profile", "ba_pick_and_drop_order.request_by", "=", "ba_profile.id")

      ->select("ba_pick_and_drop_order.*", 
        "ba_profile.fullname as customerName",
        "ba_profile.locality as customerAddress",
         "ba_profile.landmark as customerLandmark",
         "ba_profile.phone as customerPhone")
      ->where("ba_pick_and_drop_order.id", $orderno )    
      //->where("request_from", "business")
      ->where("request_from", $from)
      ->first() ;

       if( !isset($order_info))
      {
        return redirect("/admin/customer-care/business/view-all"  ); 
      }


      $agent_info=  DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
         ->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
         ->where("order_no", $orderno  )
         ->where("order_type",  "assist" )
         ->first(); 

      $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ;


      $today = date('Y-m-d'); 
      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();


      $franchise = DB::table('ba_franchise')
        ->where('current_status','active')
        ->select()
        ->get();

        $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("is_booktou_staff", "yes" )
      ->where("ba_users.status", "<>", "100" )  
      ->where("ba_users.category",   "100" )  
      ->where("franchise", 0)
      ->select("ba_profile.*", "ba_users.category" ) 
      ->get();

      $result = array('order_info' => $order_info , 
      'franchise' =>$franchise, 
       'agent_info' => $agent_info , 
       'all_agents' => $all_agents ,
        'staffs' => $all_staffs,   
       'remarks' => $remarks );
      return view("admin.orders.assist_order_detailed_view")->with('data',$result);


     }

     //assists view ends here

     protected function updatePnDOrderCCRemarks(Request $request)
     {

      $orderno = $request->orderno;
      if($orderno == "")
      {
        return redirect("/admin/customer-care/business/view-all"); 
      }
  
     if( $request->remarks == "")
      {
        return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )->with("err_msg", "PnD remarks missing!"); 
      }

      $order_info   = PickAndDropRequestModel::find( $orderno) ;

       if( !isset($order_info))
      {
        return redirect("/admin/customer-care/business/view-all"  )->with("err_msg", "No matching order found!"); 
      }

      $order_info->cc_remarks = $request->remarks ;
      $order_info->save();   
      return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )->with("err_msg", "PnD remarks updated!");  
     }

 
  protected function updatePnDOrderInformation(Request $request)
  {

      $orderno = $request->orderno;
      if($orderno == "")
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all"); 
      }

      if( $request->remarks == ""   )
      {
        return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )
        ->with("err_msg", "To edit an order, you need to provide reason for editing!"); 
      }

      $order_info   = PickAndDropRequestModel::find( $orderno) ;
      if( !isset($order_info))
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all"  )
        ->with("err_msg", "No matching order found!"); 
      }
  

      if( date('d-m-Y', strtotime($request->servicedate)) != date('d-m-Y', strtotime( $order_info->servicedate ))  )
      {
        $order_info->service_date =  date('Y-m-d', strtotime( $request->servicedate )) ;  
      }

      $order_info->pickup_details = $request->orderdetails;

        if($request->total != "" )
        {
          $order_info->item_total =  $request->total ;  
        }
        
        if($request->delivery != "" )
        {
          $order_info->delivery_charge = $request->delivery;
          //$order_info->requested_fee  =  $order_info->service_fee = $request->delivery;  
        }

        if($request->packcost != "" )
        {
          $order_info->packaging_cost  =  $request->packcost;  
        }

        if($request->servicefee != "" )
        {
          $order_info->service_fee  =  $request->servicefee;  
        }


        $order_info->save();  


        //save remarks 
        $remark = new OrderRemarksModel();
        $remark->order_no =  $orderno ;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "payment update" ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();  
  
        if($order_info->request_from == "business" || $order_info->request_from == "merchant" )
        {
            return redirect("/admin/customer-care/pnd-order/view-details/" .  $orderno )
            ->with("err_msg", "Order remarks updated!");   
        }
        else 
        {
            return redirect("/admin/customer-care/assist-order/view-details/" . $orderno )
            ->with("err_msg", "Order remarks updated!");   
        }
}


protected function updatePnDOrderPaymentUpdate(Request $request)
  {

      $orderno = $request->orderno;
      if($orderno == "")
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all"); 
      }

      if( $request->remarks == ""   )
      {
        return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )
        ->with("err_msg", "To edit an order, you need to provide reason for editing!"); 
      }

      $order_info   = PickAndDropRequestModel::find( $orderno) ;
      if( !isset($order_info))
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all"  )
        ->with("err_msg", "No matching order found!"); 
      }

      $paytarget =   $request->paytarget ;
      $paymode =  $request->paymode ;
      $order_info->payment_target =$paytarget;
      $order_info->pay_mode  =  $paymode ; 
      $order_info->save();  

      //save remarks 
      $remark = new OrderRemarksModel();
      $remark->order_no =  $orderno ;
      $remark->remarks = $request->remarks ;
      $remark->rem_type = "payment-target-update" ;  
      $remark->remark_by = $request->session()->get('__member_id_' );
      $remark->save();  

      if($order_info->request_from == "business" || $order_info->request_from == "merchant" )
      {
            return redirect("/admin/customer-care/pnd-order/view-details/" .  $orderno )
            ->with("err_msg", "Order remarks updated!");   
      }
      else 
      {
            return redirect("/admin/customer-care/assist-order/view-details/" . $orderno )
            ->with("err_msg", "Order remarks updated!");   
      }
}


protected function updatePnDOrderStatus(Request $request)
{
    $orderno = $request->orderno;
    if($orderno == "")
    {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all"); 
    }

    if( $request->status == ""   )
    {
      return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )
      ->with("err_msg", "Order status missing!"); 
    }

    $order_info   = PickAndDropRequestModel::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $order_info->book_status =  $request->status ;  
        $order_info->save();  
    

        //save remarks 
        $remark = new OrderRemarksModel();
        $remark->order_no =  $orderno ;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "order status update" ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();  


        $orderStatus = new OrderStatusLogModel;
        $orderStatus->order_no =  $orderno ;
        $orderStatus->order_status =  $request->status ;
        $orderStatus->remarks =  $request->remarks ;  
        $orderStatus->log_date = date('Y-m-d H:i:s');
        $orderStatus->save(); 

      if($order_info->request_from == "business" || $order_info->request_from == "merchant" )
        {
            return redirect("/admin/customer-care/pnd-order/view-details/" .  $orderno )
            ->with("err_msg", "Order remarks updated!");   
        }
        else  if(  $order_info->request_from == "customer")
        {
            return redirect("/admin/customer-care/assist-order/view-details/" . $orderno )
            ->with("err_msg", "Order remarks updated!");   
        } 
        else 
        {
          return redirect("/admin/customer-care/pick-and-drop-orders/view-all"); 
        }
  

     }



     protected function updateNormalOrderStatus(Request $request)
     {

        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/admin/customer-care/orders/view-all"); 
        }

        if( $request->status == ""   )
        {
          return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order status missing!"); 
        }


        $order_info   = ServiceBooking::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $order_info->book_status =  $request->status ;  
        $order_info->save();  
    
   
         return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order status updated!"); 


     }



     protected function updateNormalOrderPrice(Request $request)
     {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/admin/customer-care/orders/view-all"); 
        }

        if(   $request->remarks == ""  )
        {
          return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Remarks missing!"); 
        }


        if( $request->price == ""   )
        {
          return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order amount missing!"); 
        }


        $order_info   = ServiceBooking::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $order_info->seller_payable =  $request->price ;  
        $order_info->total_cost = $request->price  ;  
        $order_info->save();


        $remark = new OrderRemarksModel();
        $remark->order_no =$orderno;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "Price Correction." ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();  
 
   
         return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order status updated!"); 


     }

     protected function updateRefundAmount(Request $request)
     {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/admin/customer-care/orders/view-all"); 
        }

        if(   $request->remarks == ""  )
        {
          return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Remarks missing!"); 
        }


        if( $request->refund == ""   )
        {
          return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order amount missing!"); 
        }


        $order_info   = ServiceBooking::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $order_info->refunded =  $request->refund ;    
        $order_info->save();


        $remark = new OrderRemarksModel();
        $remark->order_no =$orderno;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "Refund amount entry." ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();  
 
   
         return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order status updated!"); 


     }


     protected function removeItemFromOrderBasket(Request $request)
     {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/admin/customer-care/orders/view-all"); 
        }

        if(   $request->remarks == ""  )
        {
          return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Remarks missing!"); 
        }


        if( $request->itemno == ""   )
        {
          return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order item to remove not selected!"); 
        }
        $itemno = $request->itemno; 

        $order_info   = ServiceBooking::find( $orderno) ; 
        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/orders/view-all")->with("err_msg", "No matching order found!"); 
        }
  

        //find all cart items 
        $cart_items = DB::table("ba_shopping_basket")
        ->where("order_no" , $orderno)
        ->get();

        //search for item to delete 
        $found = false;
        $remaing_items = array();
        foreach($cart_items as $item)
        {
          if($item->id == $itemno)
          {
            $found = true; 
          } 
          else 
          {
            $remaing_items[] =  $item;
          }
        }

        if( !$found)
        {
          return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Selected item to remove not found in the order!"); 
        }

        //remove now  
        DB::table("ba_shopping_basket")
        ->where("id" ,  $itemno) 
        ->delete();
        
        $new_cost = 0.00;
        foreach($remaing_items as $item)
        {
            $new_cost += ($item->qty * $item->price) + ($item->qty * $item->package_charge); 
        }

        DB::table("ba_service_booking")
        ->where("id" ,  $orderno)
        ->update(['seller_payable' => $new_cost, 'total_cost' => $new_cost] );


        $remark = new OrderRemarksModel();
        $remark->order_no =$orderno;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "Item deletion from order and price adjustment." ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();  
  
   
         return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order status updated!"); 


     }


     //use this to update remarks
    protected function updateNormalOrderRemarks(Request $request)
     {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/admin/customer-care/orders/view-all"); 
        }
    
        if( $request->remarks == "" || $request->type == "" )
        {
          if($request->turl == "list")
          {
            return redirect("/admin/customer-care/orders/view-all")->with("err_msg", "Order remarks fields missing!"); 
          }
          else 
          {
            return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order remarks fields missing!"); 
          }
          
        }

        $order_info   = ServiceBooking::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $remark = new OrderRemarksModel();
        $remark->order_no = $orderno;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = $request->type ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();   

         if($request->turl == "list")
          {
            return redirect("/admin/customer-care/orders/view-all")->with("err_msg", "Order remarks updated!");   
          }
          else 
          {
            return redirect("/admin/customer-care/order/view-details/" . $orderno )->with("err_msg", "Order remarks updated!");   
          }

     
     }




     protected function updatePnDOrderRemarks(Request $request)
     {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/admin/customer-care/pick-and-drop-orders/view-all"); 
        }
    
        if( $request->remarks == "" || $request->type == "" )
        {
          if($request->turl == "list")
          {
            return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "Order remarks fields missing!"); 
          }
          else 
          {
            return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )->with("err_msg", "Order remarks fields missing!"); 
          }
          
        }

        $order_info   =  PickAndDropRequestModel::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $remark = new OrderRemarksModel();
        $remark->order_no = $orderno;
        $remark->remarks = ($request->remarks == "" ? "payment info update" :  $request->remarks ) ;
        $remark->rem_type = $request->type ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();   

         if($order_info->request_from == "business" || $order_info->request_from == "merchant" )
          {
            return redirect("/admin/customer-care/pnd-order/view-details/" .  $orderno )->with("err_msg", "Order remarks updated!");   
          }
          else 
          {
            return redirect("/admin/customer-care/assist-order/view-details/" . $orderno )->with("err_msg", "Order remarks updated!");   
          }
 
     }



    public function manageProductCategory(Request $request)
    {
  
       $businessCategory = DB::table("ba_business_category")
        ->orderBy("name","asc")
        ->get();

 


        if( isset($request->btnSearch) )
      {

        $key = $request->search_key  ;
          if ($key=="all") 
          {
            $productCategory = DB::table("ba_product_category")
            ->select("id as categoryId","category_name as categoryName",
             "category_desc as categoryDescription", 
             "business_category as businessCategory",
             "icon_path as productIcon", "icon_url as iconUrl")
            ->orderBy("category_name","asc")
            ->get();
          }
          else
          {
             $productCategory = DB::table("ba_product_category")
             ->select("id as categoryId","category_name as categoryName",
             "category_desc as categoryDescription", 
             "business_category as businessCategory",
             "icon_path as productIcon", "icon_url as iconUrl")
            ->where("business_category",$key)
            ->orderBy("category_name","asc")
            ->get();
          }
       
        }
        else 
        {
           $productCategory = DB::table("ba_product_category")
          ->select("id as categoryId","category_name as categoryName",
          "category_desc as categoryDescription", 
          "business_category as businessCategory",
          "icon_path as productIcon", "icon_url as iconUrl")
          ->orderBy("category_name","asc")
          ->get();
        }



        $data = array('productCategory' => $productCategory, 'businessCategory' => $businessCategory,   'breadcrumb_menu' => 'system_sub_menu');
    
     return view("admin.store.products.category.add_edit_update")->with("data",$data);  
    }


 // Update Product Category
  
  public function saveProductCategory(Request $request)
  { 


    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; // cnd - /var/www/html/api/public


    if( isset($request->btnSave) && $request->btnSave =="Save"):
 

      if($request->categoryName == "" || $request->categoryDescription == "" || 
        $request->businessCategory == "")
      {
         return redirect("/admin/products/category/manage-categories")->with("err_msg", "Field missing!");
      }
    
            $productCategory = ProductCategoryModel::find($request->cat_id);

              if(!isset($productCategory)) 
             {

              $productCategory= new ProductCategoryModel;
              //  return redirect("/admin/product-category/update-form")->with("err_msg", "Product category successfully save!");

              }
            if( $request->categoryName != "")
            {
             $productCategory->category_name = $request->categoryName ;
            }
            if( $request->categoryDescription != "")
            {
             $productCategory->category_desc = $request->categoryDescription ;
            }
            if( $request->businessCategory != "")
            {
             $productCategory->business_category = $request->businessCategory ;
            }

            $productCategory->product_type = $request->productType ;


            if( $request->file('photo') != "")
            {
              $imageName = "ico_" . time() . '.' .$request->photo->getClientOriginalExtension();
              $request->photo->move(  $cdn_path . '/assets/app/icons' ,  $imageName ); 
            }
             
            if( $request->file('photo') != "")
            {
              $productCategory->icon_path = '/public/assets/app/icons/'.$imageName ;
              $productCategory->icon_url =  $cdn_url . '/assets/app/icons/'.$imageName ;

            }
           $save = $productCategory->save();
           if ($save) {
             return back()->with("err_msg", "Product category successfully updated!");
           }
            else
            {
              return back()->with("err_msg", "Product category update failed!");
            }  
           

         else:

              return redirect("/"); 

            endif;
  }




     //Delete/Destroy Category
      public function destroyCategory(Request $request ) 
    {

      $id = $request->id;

          $categoryInfo = ProductCategoryModel::where('id',$id)->get();
        if(count($categoryInfo)>0)
        {
            foreach ($categoryInfo as $item) 
            {
                File::delete('public'.$item->icon_path);
                $categoryData = ProductCategoryModel::find($item->id);
                $categoryData->delete();
            }
            $data= ["message" => "success", "status_code" =>  555 ,  'detailed_msg' => 'Delete category success.'  ];
    
        }
        else{
            $data= ["message" => "failure", "status_code" =>  556 ,  'detailed_msg' => 'Delete category failed.'  ];
        }

        return back();

    }




    protected function viewListedBusinesses(Request $request)
    {
     
        $business= array();

        $key = $request->search_key;
        $blocked = $request->blocked;

      if( isset($request->btnSearch) )
      {
        if($key=="")
        {
          $business= DB::table("ba_business") 
          ->where("is_block", $blocked )  
          ->paginate(20); 
        }
        else 
        {
          $business= DB::table("ba_business")  
          ->where("name","like", "%" . $key . "%")  
          ->where("is_block", $blocked )  
          ->paginate(20);  
        }
          
 
       }
       else 
       {

         $business= DB::table("ba_business") 
         ->where("is_block","no" )  
        ->paginate(20); 

       }

      $bins = array();
      foreach( $business as $item)
      {
        $bins[] =  $item->id;
      }

      $bins[]=0;

      $promo_businesses= DB::table("ba_premium_business")
      ->whereIn("bin",$bins) 
      ->get();

      $qr_menu= DB::table("ba_business_menu")
      ->whereIn("bin", $bins) 
      ->select("bin", DB::Raw("count(*) as pageCount"))
      ->groupBy("bin")
      ->get();

 

       $data = array('results' => $business, 'promo_businesses'=>$promo_businesses, 'qr_menu' => $qr_menu);
       return view("admin.business.business_list")->with('data',$data);
    
  }


  protected function viewBusinessProfile(Request $request)
  {
    $id = $request->busi_id;
    $businessInfo= DB::table("ba_business")
    ->where("id", $id ) 
    ->first();
     
     $businessPayment = DB::table('ba_billing_cycle')
      ->where('bin',$id)
      ->where('month',date('m'))
      ->where('year', date('Y') ) 
    ->first();
    $selfpickup = SelfPickUpModel::where('bin',$id)->first();
     
    $productCategory= DB::table("ba_product_category")
    ->get();


    $commissions= DB::table("ba_business_commission")
    ->where("bin",$id  )
    ->where("comm_status", "active")
    ->first();


    $business_bank_info = DB::table("ba_business_bank_account")
    ->where("bin", $id)
    ->first();


    $data = array('business' => $businessInfo, 'category' => $productCategory, 
      'commissions' =>$commissions,'bank_info' =>$business_bank_info, 'bin' => $id,
      'biz_pay_cycle'=>$businessPayment, "selfpickup"=>$selfpickup, );
    return view("admin.business.business_profile")->with('data',$data);
    
  }



public function uploadBannerImage(Request $request) 
{
 
   if(isset($request->btnupload))
   {
        $key = $request->key;

        if(  $key == ""  || $request->photo == "")
        {
           return redirect("/admin/customer-care/business/view-all")->with('err_msg', 'Missing data to perform action.' );
        }

        $business = Business::find(   $key) ; 
        if(   !isset($business))
        {
          return redirect("/admin/customer-care/business/view-all")->with('err_msg',  "No matching business found!");
        }


        $cdn_url =   config('app.app_cdn')    ; 
        $cdn_path =  config('app.app_cdn_path')  ; // cnd - /var/www/html/api/public
         

        $folder_url  =  $cdn_url . '/assets/image/business/bin_'. $key   ;
        $folder_path =  $cdn_path  . '/assets/image/business/bin_'.   $key  ;


        if( !File::isDirectory($folder_path))
        {
            File::makeDirectory( $folder_path, 0777, true, true);
        }
 

        $file = $request->photo; 
        $originalname = $file->getClientOriginalName();
        $extension = $file->extension( );
        $filename = "bb_". time() . ".{$extension}";
        $file->storeAs('uploads',  $filename ); 
        $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename,   $folder_path . "/" . $filename);

  
        $business->banner =  '/assets/image/business/bin_'. $key . "/" . $filename ;
        $save=$business->save();

        return redirect("/admin/customer-care/business/view-profile/" . $key )->with('err_msg',  "Banner image uploaded!");

      }
      
      return redirect("/admin/customer-care/business/view-all");
 
 
 
   }



  protected function viewBusinessProducts(Request $request)
  {
    
    $bin = $request->bin;

    if($bin == "")
    {
      return redirect('admin/customer-care/business/view-all');
    }

    $productCategory= DB::table("ba_product_category")->get();

    $products= DB::table("ba_products")
    ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
    ->where("ba_products.bin",$bin)
    ->select("ba_products.*", "ba_product_variant.*","ba_product_variant.prsubid as bsin", DB::Raw("'na' image") )
    ->paginate(20);


    $prsubids = array(); 
    foreach($products as $item)
    {
      $prsubids[] = $item->prsubid;
    }
    $prsubids[] =0;

    $promotion_products = DB::table("ba_product_promotions")
    ->whereIn("prsubid", $prsubids )
    ->get();

    $popular_category = DB::table('app_popular_category')
     
    ->get();

    $categories = DB::table("ba_bookable_category")
      ->select(DB::raw('distinct(main_module)as main'))
        
      ->get();

    $product_images =  DB::table("ba_product_photos")  
		->whereIn("prsubid", $prsubids )
		->select("id", "prsubid", "image_url as imageUrl" )
		->get();


    foreach($products as $item)
		{
		  $item->image = "https://cdn.booktou.in/assets/image/no-image.jpg";
      foreach( $product_images as $images)
      {
        if( $item->prsubid ==  $images->prsubid )
        {
          $item->image = $images->imageUrl;
          break;
        }
      }
    } 
 

    $business= Business::find( $bin) ;

    $data = array(
                  'products' => $products, 
                  'business' => $business, 
                  'category' => $productCategory,  
                  'promotion_products' =>$promotion_products, 
                  'bin'=>$bin,
                  'categories'=>$categories,
                  'popular_category'=>$popular_category

                );

    
    return view("admin.business.business_products")->with('data',$data);

  }

    protected function viewBusinessItem(Request $request)
    {
  

  //return json_encode($request->search_key);
        $productCategory= DB::table("ba_product_category")
      ->get();
       

       $products= array();
      if( isset($request->btnSearch) )
      {

        $products= DB::table("ba_products")
      ->where("bin",$request->businessId)
      ->where("category",$request->search_key)
      ->get(); 
   }
    $data = array('items' => $products,
                  'category' => $productCategory);

         return view("admin.business_products")->with('data',$data);
    
  }


  protected function addToPromotion(Request $request)
  {
   
    if( isset($request->btnsaveconfirmation) && $request->btnsaveconfirmation =="save")
           {
            if($request->bin == ""   )
            {
              return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business selected.");
            }

             $bin = $request->bin; 
             $business  = Business::find($bin); 
             if( !isset($business ))
             {
                return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business found.");
             }


             if(  $request->prsubid == "" )
             {
              return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "No product selected.");
             }

             $product = ProductVariantModel::find( $request->prsubid ) ;

             if(  !isset($product ) )
             {
                return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "No product found.");
             }


             $promotion = ProductPromotion::where("prsubid", $request->prsubid )->first();
             if(  !isset($promotion ) )
             {
                $promotion = new ProductPromotion;
             }

             $promotion->prsubid = $product->prsubid ;
             $promotion->todays_deal = $request->todaysdeal; 
             $promotion->save(); 
             
             return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "Product added to promotion list.");

           }
           else 
           {
             return redirect("/admin/customer-care/business/view-all") ;
           } 
  }
// ...............meena............................
  protected function updatePrice(Request $request)
  {
   
    if( isset($request->btnsaveconfirmation) && $request->btnsaveconfirmation =="save")
           {
            if($request->bin == ""   )
            {
              return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business selected.");
            }

             $bin = $request->bin; 
             $business  = Business::find($bin); 
             if( !isset($business ))
             {
                return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business found.");
             }

            if ($request->key=="") 
             {
                return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "field missing!");
             }
         

             $key = $request->key;
             $product = ProductVariantModel::find($key);

              if ( !isset(  $product ) ) 
             {
                return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "No product inventory found!");
             }


             $product_info = ProductModel::find( $product->prid  );

              if ( !isset(  $product_info ) ) 
             {
                return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "No matching product found!");
             }

 

             $product->prsubid = $product->prsubid ;
             $product->discount = $request->Discount; 
             $product->discountPc = $request->percentage;
      
             $product->save(); 
           
             $production = new AddDiscountProductsModel();      
             $production->bin = $request->bin;
             $production->bsin = $request->key;
             $production->category = $product_info->category; 
             $production->is_sponsored = $request->isSponsored;
             $production->main_module = $request->mainmodule;
             $production->save();
             
             return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "Update discount list.");

           }
           else 
           {
             return redirect("/admin/customer-care/business/view-all") ;
           } 
  }

  protected function addPopularProducts(Request $request)
  {
    
   
    if( isset($request->btnsaveconfirmation) && $request->btnsaveconfirmation =="save")
           {
            if($request->bin == ""   )
            {
              return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business selected.");
            }

             $bin = $request->bin; 

             $business  = Business::find($bin); 
             if( !isset($business ))
             {
                return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business found.");
             }


             if(  $request->bsin == "" )
             {
              return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "No product selected.");
             }

             $product = ProductVariantModel::find( $request->bsin ) ;

             if(  !isset($product ) )
             {
                return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "No product found.");
             }

             $production = PopularProductsModel::where("id", $request->bsin )->first();
             if(  !isset($production ) )
             {
                $production = new PopularProductsModel;
             }
             $production->bin = $request->bin;
             $production->bsin = $request->bsin;
             $production->category = $request->category;
             $production->display_name = $request->display_name;              
             $production->save(); 
             
             return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "Product added to Popular category list.");

           }
           else 
           {
             return redirect("/admin/customer-care/business/view-all") ;
           } 
  }
   // .................
    
   protected function viewDiscountProducts(Request $request)
   {

     $bin = $request->bin;
 


    if($bin == "")
    {
      return redirect('admin/customer-care/business/view-all');
    }

    $productCategory= DB::table("ba_product_category")->get();

    $products= DB::table("ba_products")
    ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
    ->where("ba_products.bin",$bin)
    ->whereRaw(" ( ba_product_variant.discount > 0 or ba_product_variant.discountPc > 0 )") 
    ->select("ba_products.*", "ba_product_variant.*", DB::Raw("'na' image") )
    ->paginate(20);


    $prsubids = array(); 
    foreach($products as $item)
    {
      $prsubids[] = $item->prsubid;
    }
    $prsubids[] =0;

    $promotion_products = DB::table("ba_product_promotions")
    ->whereIn("prsubid", $prsubids )
    ->get();


    $product_images =  DB::table("ba_product_photos")  
    ->whereIn("prsubid", $prsubids )
    ->select("id", "prsubid", "image_url as imageUrl" )
    ->get();

    foreach($products as $item)
    {
      $item->image = "https://cdn.booktou.in/assets/image/no-image.jpg";
      foreach( $product_images as $images)
      {
        if( $item->prsubid ==  $images->prsubid )
        {
          $item->image = $images->imageUrl;
          break;
        }
      }
    } 
 

    $business= Business::find( $bin) ;

    $data = array('products' => $products, 'business' => $business, 'category' => $productCategory,  'promotion_products' =>$promotion_products, 'bin' => $bin);
    return view("admin.discount.view_discount")->with('data',$data); 
  }

  protected function updateDiscountProducts(Request $request)
  {
   
   
            if($request->bin == ""   )
            {
              return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business selected.");
            }

             $bin = $request->bin; 
             $business  = Business::find($bin); 
             if( !isset($business ))
             {
                return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business found.");
             }

              if ($request->key=="") 
             {
              return redirect("/admin/customer-care/business/view-products/" . $bin )->with("err_msg", "field missing!");
             }
         

             $key = $request->key;
             $product = ProductVariantModel::find($key);
             $product->prsubid = $product->prsubid ;
             $product->discount = $request->discount;
             $product->unit_price = $request->price;
             $product->subcategory = $request->category;             
             $save = $product->save();   
            
        if ($save) {
            return redirect("/admin/customer-care/business/view-discount-products/" . $bin )->with("err_msg", "record udpate successfully!");
            }  
           else 
           {
             return redirect("/admin/customer-care/business/view-all") ;
           } 
  }
  protected function deleteDiscountProducts(Request $request)
     
     {

         $bin = $request->bin;

         if($bin=="")
         {

         }
        if ($request->key=="") {
            return redirect("/admin/customer-care/business/view-all" . $bin)
            ->with("err_msg","field missing!");
        }

        $key = $request->key;
 
        $delete = DB::table("ba_product_variant")
        ->where("prsubid",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/customer-care/business/view-discount-products/" . $bin)
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/admin/customer-care/business/view-products/" . $bin)
            ->with("err_msg","failed!");
        }

    $key = $request->key;
  
    $product = ProductVariantModel::find($key);
    $product->discount =  0.00;
    $product->discountPc =  0.00;
    $save = $product->save();

    $delete = DB::table("app_discounted_products")
    ->where("bsin",$key) 
    ->delete();

    if ($delete) {
      return redirect("/admin/customer-care/business/view-discount-products/" . $bin)
      ->with("err_msg","record deleted!");
    }else{
      return redirect("/admin/customer-care/business/view-products/" . $bin)
      ->with("err_msg","failed!");
    }

  }
 
    protected function changeUserPassword(Request $request)
     {
         
        $bin  = $request->businessid; 
         
        $business =  Business::find($bin);

        if (!isset($business)) 
        {
          return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business record found!." ); 
        }

        if ($request->password!=$request->confirm_password) {
            return redirect("/admin/customer-care/business/view-all")->with("err_msg", "password mismatch!." ); 
        }

        $phone = $business->phone_pri;


        $user_info  = DB::table('ba_users')
        ->where('phone',$phone)
        ->where('category',1)
        ->first();

        if (!isset($user_info)) {
           return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No user record found!." );
        }


        $user = User::find($user_info->id);

        if(!isset($user))
        {
          return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No user record found!." );
        }
       $user->password =  Hash::make($request->password)   ; 
     
       $save = $user->save();
        if ($save) {
            return redirect("/admin/customer-care/business/view-all")->with("err_msg", "Business visibility updated successfully." );
        }else{
            return redirect("/admin/customer-care/business/view-all")->with("err_msg", "Business password not successfully." );
        }  
   }

    // ........................end meena.............................
  // Search business/shop products
  protected function searchBusinessProducts(Request $request)
  {
 
      if(isset($request->busi_id))
      {
          $request->session()->put('_bin_', $request->busi_id ); 
          $id = $request->busi_id;
      }
      else if( $request->session()->has('_bin_' ) )
      {
        $id = $request->session()->get('_bin_' );

      }
      else 
      {
         return redirect("/admin/customer-care/business/view-all");
      }

  
       
      $productCategory= DB::table("ba_product_category")
       ->get();
       
 

      if( isset($request->btnSearch) )
      {
        
        $request->session()->put('_key_', $request->search_key ); 
        $key = $request->search_key  ;
        if($key == "all")
        {
          $products= DB::table("ba_products")
          ->where("bin",$id) 
          ->paginate(20); 
        }
        else 
        {
          $products= DB::table("ba_products")
          ->where("bin",$id)
          ->where("category",  $key )
          ->paginate(20); 
        }
 
        
      }
      else 
        if($request->session()->has('_key_' ) )
        { 
          $key = $request->session()->get('_key_' ); 

           $products= DB::table("ba_products")
           ->where("bin",$id)
           ->where("category",  $key )
           ->paginate(20); 

        }
        else 
        {
          return redirect("/admin/customer-care/business/view-products/" . $id);
        }

    $business = Business::find($id);

  
    $prcodes = array(); 
    foreach( $products as $item)
    {
      $prcodes[] = $item->pr_code;
    }
    $prcodes[] = "other";

    $promotion_products = DB::table("ba_product_promotions")
    ->whereIn("pr_code", $prcodes )->get();


        $data = array(
          'business' => $business,
          'products' => $products,
          'category' => $productCategory,
          'promotion_products' =>$promotion_products, 
          'bin' => $id
        );

 
    return view("admin.business.business_products")->with('data',$data); 

 
 
  }



   // View Business Earning Report for daily
     protected function viewBusinessDailyEarning(Request $request  )
     { 
        if(!isset($request->bin ) || $request->bin == "" )
        {
          return redirect("/admin/customer-care/business/view-all");
        }

        if($request->filter_date == "" )
        {
          $today =   date('Y-m-d'); 
        }
        else
        {
            $today = date('Y-m-d', strtotime( $request->filter_date ));
        }

        

        $totalEarning = 0; 

        $business_info  = Business::find( $request->bin);

        if( !isset($business_info ))
        {

          return redirect("/customer-care/business/view-all")->with("err_msg", "No businss found.");
        }

        $business_owner_info  = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id" )
        ->where("ba_users.bin",   $request->bin)
        ->select("ba_profile.*")
        ->first();

        
        if( !isset($business_owner_info ))
        {
          return redirect("/customer-care/business/view-all")->with("err_msg", "No businss found.");
        }


        $status = [  'completed', 'delivered'  ];
        
        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id") 
        ->where("ba_service_booking.bin", $request->bin )
        ->whereIn("ba_service_booking.book_status",  $status )
        ->whereDate('ba_service_booking.service_date', $today)  
        ->select("ba_service_booking.id as orderNo", 
                 "ba_profile.fullname as orderBy",  
                 'book_status as orderStatus',
                 "total_cost as totalCost",
                 'delivery_charge as deliveryCharge'  )
        ->get();
 


       $paymentInfo  = DB::table("ba_payment_dealings") 
       ->where("bin",  $request->bin )
       ->whereRaw("date(bill_date) ='$today'")
       ->get();


       $data = array( 

            'business' => $business_info , 'owner_profile' => $business_owner_info,  
            "totalEarning" => $totalEarning, 'payments' =>  $paymentInfo,  'results' =>  $booking_list , 'bin' => $request->bin  );

 

      return view("admin.earning_clearance.business_daily_earning")->with('data',$data);
    }
 


     // View Business Earning Report for monthly
     protected function viewBusinessMonthlySales(Request $request )
     {
        
        $bin = $request->bin;
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }

 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $totalEarning = 0; 
 

        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")

        ->where("ba_service_booking.bin",$bin) 
        ->whereMonth('ba_service_booking.book_date', $month )
        ->whereYear('ba_service_booking.book_date', $year )  
        ->select("ba_service_booking.*", 
                 "ba_profile.fullname as orderBy" ,
                 DB::Raw("'na'  deliverBy") )

        ->get();


        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {

           if( $item->book_status != "cancel_by_client" && $item->book_status != "cancel_by_owner" && 
            $item->book_status != "canceled"  && $item->book_status != "returned")
           {
              $totalEarning += $item->seller_payable;
           } 
            
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->id; 
        }

        $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                } 
            }   
       }
       $business  = Business::find(  $bin)  ;


       
      $order_items  = DB::table("ba_shopping_basket")
      ->whereIn("order_no", $orderNo ) 
      ->get();


       $data = array("totalEarning" => $totalEarning, 'business' =>$business, 
        'results' =>  $booking_list,
        'orderItems' => $order_items,
        'bin' => $bin );
       return view("admin.earning_clearance.business_monthly_earning")->with('data',$data);

     }




     // View Business PnD Report for monthly
     protected function viewBusinessMonthlyPnD(Request $request )
     {


       $bin = $request->bin;

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }

 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $totalEarning = 0; 
  
        $booking_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->where("ba_pick_and_drop_order.request_by", $bin) 
        ->whereMonth('ba_pick_and_drop_order.book_date', $month )
        ->whereYear('ba_pick_and_drop_order.book_date', $year )  
        ->select("ba_pick_and_drop_order.*",  "ba_business.name as orderBy" , DB::Raw("'na'  deliverBy") )
        ->get();


        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {

           if( $item->book_status != "cancel_by_client" && $item->book_status != "cancel_by_owner" && 
            $item->book_status != "canceled"  && $item->book_status != "returned")
           {
              $totalEarning += $item->total_amount;
           } 
            
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->id; 
        }

        $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                } 
            }   
       }
       $business  = Business::find(  $bin)  ;

 

       $data = array("totalEarning" => $totalEarning, 'business' =>$business, 
        'results' =>  $booking_list,
        
        'bin' => $bin );
       return view("admin.earning_clearance.pnd_monthly_clearance_report")->with('data',$data);

     }


 // View Business Normal and PnD Sales Report for monthly
  protected function salesAndClearanceReport(Request $request )
  {


       $bin = $request->bin;

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }

 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $totalEarning = 0; 
  
        $pnd_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->where("ba_pick_and_drop_order.request_from",  'business') 
        ->where("ba_pick_and_drop_order.request_by", $bin) 
        ->whereMonth('ba_pick_and_drop_order.book_date', $month )
        ->whereYear('ba_pick_and_drop_order.book_date', $year )  
        ->select( "ba_pick_and_drop_order.id",   
          "source",
          "book_date", 
          "clerance_status",
          DB::Raw("(total_amount - refunded) as orderCost"),  
          "refunded",
          "service_fee",
          "pay_mode",
          "book_status",
          "fullname as customerName", 
          "ba_business.name",  
          DB::Raw("'na'  deliverBy"), 
          DB::Raw("'pnd' orderType") ) ;

        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id") 
        ->where("ba_service_booking.bin", $bin) 
        ->whereMonth('ba_service_booking.book_date', $month )
        ->whereYear('ba_service_booking.book_date', $year )  
        ->select( "ba_service_booking.id",   
          "source",
          "book_date", 
          "clerance_status",   
           DB::Raw("(seller_payable - refunded) as total_amount"),  
          "refunded",
          "delivery_charge as service_fee",
          "payment_type as pay_mode",
          "book_status",
          "customer_name as customerName" , 
          "ba_business.name",   
          DB::Raw("'na'  deliverBy"), 
          DB::Raw("'normal' orderType") )
        ->union($pnd_list)
        ->get();
 
        $agentIds = array();
        $orderNo =array();

        foreach ($booking_list as $item) 
        {
          if( $item->book_status != "cancel_by_client" && $item->book_status != "cancel_by_owner" &&  
            $item->book_status != "canceled"  && $item->book_status != "returned")
          {
            $totalEarning += $item->total_amount;
          }
    
          $agentIds[] = $item->deliverBy;
          $orderNo[] = $item->id; 
        }

        $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        foreach ($booking_list as $item) 
        {
          foreach ($agentInfo as $aitem) 
          {
            if($item->deliverBy == $aitem->deliverBy )
            {
              $item->deliverBy= $aitem->fullname;break;
            } 
          }   
       }
 

       $business  = Business::find(  $bin)  ;
       $data =  array("totalEarning" => $totalEarning, 'business' =>$business,  
        'results' =>  $booking_list, 'bin' => $bin );
       return view("admin.earning_clearance.sales_monthly_clearance_report")->with('data',$data); 
     }


     protected function salesClearanceForm(Request $request )
     {
        if( !isset($request->btnsubmit))
        {

        }

        $bin = $request->bin;
        $orders = $request->selection; 
        $orders[]=0;  
        $totalEarning = 0; 
  
        $normal_orders  = DB::table("ba_service_booking")
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id") 
        ->where("ba_service_booking.bin", $bin) 
        ->whereIn('ba_service_booking.id', $orders )  
        ->whereNotIn("book_status", array("cancel_by_client", "cancel_by_owner", "canceled", "returned" ) ) 
        ->select( "ba_service_booking.id",   
          "source",
          "book_date", 
          "clerance_status",
          "seller_payable as total_amount",
          "delivery_charge as service_fee",
          "payment_type as pay_mode",
          "book_status",
          "customer_name as customerName" , 
          "ba_business.name",    
          "ba_business.commission",
          DB::Raw("'normal' orderType") );

        $booking_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->where("ba_pick_and_drop_order.request_by", $bin) 
        ->whereIn('ba_pick_and_drop_order.id', $orders )   
        ->whereNotIn("book_status", array("cancel_by_client", "cancel_by_owner", "canceled", "returned" ) ) 
        ->select( "ba_pick_and_drop_order.id",   
          "source",
          "book_date", 
          "clerance_status",
          "total_amount",
          "service_fee",
          "pay_mode",
          "book_status",
          "fullname as customerName", 
          "ba_business.name",    
          "ba_business.commission",
          DB::Raw("'pnd' orderType") )
        ->union($normal_orders)
        ->get();

        
        $total_due =0.00;
        foreach ($booking_list as $item) 
        {

          if($item->book_status != "delivered" && $item->book_status != "completed"   )
          {
            continue;
          }


          if( $item->orderType != "pnd"  )
          {
            if($item->source == "booktou" || $item->source == "customer" )
            {
              $total_due += $item->total_amount -  ( $item->commission * 0.01 * $item->total_amount ) ;
            }
            else 
            {
              $total_due += $item->total_amount;
            }
          } 
          else //apply all commission
          {
            $total_due += $item->total_amount -  ( $item->commission * 0.01 * $item->total_amount ) ;
          }
 
        }


        $business  = Business::find(  $bin)  ;
        $data = array("total_due" => $total_due, 'business' =>$business, 
        'results' =>  $booking_list,
        
        'bin' => $bin );
       return view("admin.earning_clearance.sales_monthly_clearance_form")->with('data',$data);

     }


      function saveSalesClearance(Request $request )
      {
        if( $request->bin == "" || $request->amount == "" ||$request->paymode == "" || $request->clearedon == ""  )
        {
          return redirect('/')->with("err_msg", "No data provided for clearance.");
        }
        $orders = $request->selection;  
        if(count($orders) == 0)
        {
          return redirect('/')->with("err_msg", "No data provided for clearance.");
        } 
        if( !isset($request->btnsubmit))
        {
          return redirect('/')->with("err_msg", "No data provided for clearance.");
        }
        
        $bin = $request->bin;
        

        $payment = new PaymentDealingModel;
        $payment->bin = $bin  ;
        $payment->amount =  $request->amount ;
        $payment->cleared_on = date('Y-m-d', strtotime($request->clearedon))  ;
        $payment->transact_no =  ( $request->refno != "" ) ? $request->refno  : "not provided"  ;
        $payment->payment_mode = $request->paymode;
        $payment->remarks =   ( $request->remarks != "" ) ? $request->remarks  : "not provided"  ;
        $payment->order_nos =  implode(",",$orders) ;
        $payment->save();

        DB::table("ba_pick_and_drop_order")  
        ->whereIn('id', $orders )   
        ->update( ['clerance_status' => 'paid'] ) ; 

        DB::table("ba_service_booking")  
        ->whereIn('id', $orders )   
        ->update( ['clerance_status' => 'paid'] ) ; 


 
        return redirect("/admin/business/sales-and-clearance-report?bin=" . $bin  )->with('err_msg', "Payment clerance information saved." );

    }



    protected function salesAndClearanceHistory(Request $request )
    {

        $bin = $request->bin;
        if($request->year != "")
        {
          $year  = $request->year ; 
        }
        else
        {
          $year  = date('Y' );
        }

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $payment_history =  DB::table("ba_payment_dealings") 
        ->whereMonth('cleared_on', $month )
        ->whereYear('cleared_on', $year )  
        ->where("bin", $bin )
        ->orderBy("id", "desc")
        ->get();

        $order_no_str = "";
        foreach($payment_history as $item)
        {
          $order_no_str .=  $item->order_nos . ",";
        }

        $order_nos = explode(",", $order_no_str);

        $order_nos = array_unique(array_filter($order_nos));


        $booking_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->whereIn("ba_pick_and_drop_order.id", $order_nos )  
        ->select("ba_pick_and_drop_order.*",  "ba_business.name as orderBy" , DB::Raw("'na'  deliverBy") )
        ->get();

 


        $totalEarning= 0;
        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {

           if( $item->book_status != "cancel_by_client" && $item->book_status != "cancel_by_owner" && 
            $item->book_status != "canceled"  && $item->book_status != "returned")
           {
              $totalEarning += $item->total_amount;
           } 
            
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->id; 
        }

        $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                } 
            }   
       }
       $business  = Business::find(  $bin)  ;

 

       $data = array("totalEarning" => $totalEarning, 'business' =>$business, 
        'results' =>  $booking_list, 'bin' => $bin , 'payment_history' => $payment_history);
       return view("admin.earning_clearance.sales_monthly_clearance_history")->with('data',$data);

     }




    protected function getDeliveryAgents(Request $request)
    {
       if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 3;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                $startDay   =  1 ;  
                $endDay   =    7;
                $cycle = 1;
            } 
        }
 
 
        
        $totalEarning = 0;
 
        $agents  = DB::table("ba_profile")
        
        ->whereIn("ba_profile.id", function($query) use($month, $year) {
          $query->select("profile_id")
          ->from("ba_users")
          ->join("ba_staff_attendance", "ba_staff_attendance.staff_id", "=", "ba_users.profile_id")
          ->where("ba_users.category", '100' )
          ->where("ba_users.status", '<>', '100' )
          ->where("ba_users.franchise", 0 )
          ->whereMonth("log_date", $month )
          ->whereYear("log_date", $year ) ; 
        }) 

        ->select("ba_profile.id", "ba_profile.fullname", "ba_profile.phone")
        ->get(); 

        $agentids = array(0);
        foreach($agents as $agent)
        {
          $agentids[] =$agent->id;
        }
 
      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->whereIn("ba_delivery_order.member_id", $agentids )
       
        ->whereMonth("ba_service_booking.service_date", $month )
        ->whereYear("ba_service_booking.service_date", $year ) 


        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            DB::Raw("'customer' source"),
            "ba_service_booking.service_date",
            "ba_service_booking.preferred_time as delivery_time",
            'book_status as orderStatus',
            'seller_payable as totalAmount',
            'delivery_charge as serviceFee',
            "payment_type as payMode",
             DB::Raw("'booktou' paymentTarget"),
            DB::Raw("'normal' orderType"),
            "ba_delivery_order.member_id",
            "ba_service_booking.is_deposited"
        ) ; 

        $assist_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->whereIn("ba_delivery_order.member_id", $agentids )  
        ->whereMonth("ba_pick_and_drop_order.service_date", $month )
        ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
        ->whereIn("request_from", array ('customer' ) )   
        ->whereIn("book_status", array('completed', 'delivered') )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'assist' orderType"),
            "ba_delivery_order.member_id",
            "ba_pick_and_drop_order.is_deposited"
        ) ;


        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->whereIn("ba_delivery_order.member_id", $agentids )
        ->whereMonth("ba_pick_and_drop_order.service_date", $month )
        ->whereYear("ba_pick_and_drop_order.service_date", $year ) 

        ->whereIn("request_from", array ('business', 'merchant') )
        ->whereIn("book_status", array('completed', 'delivered') )
        ->union( $normalorders )        
        ->union($assist_orders)
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'pnd' orderType"),
            "ba_delivery_order.member_id",
            "ba_pick_and_drop_order.is_deposited"
        )
        ->get();
  

        $merchantIds =array();
        $customerIds =array();
        $toalamount =0.00;
        foreach ($all_orders as $item) 
        {
            $toalamount += $item->totalAmount + $item->serviceFee;

          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }
        $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();
 
        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;
                    break;
                }

            }
        }
                 
       }

 $data =  array(   
          
        'month' =>  $month , 
        'year' => $year ,
        'start' =>  $startDay , 
        'end' =>  $endDay ,  
        "total_amount" => $toalamount,  
        'agents' =>  $agents,  
        'all_orders' =>  $all_orders );

      return view("admin.agents.monthly_collection")->with('data',$data);
       

    }




    protected function getDeliveryAgentsLocations(Request $request)
    {

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>", 100)
      ->where("ba_profile.fullname", "<>", "" )
      ->select("ba_profile.*", DB::Raw("'0' latitude"), DB::Raw("'0' longitude"),
      DB::Raw("'NIL' orderNo") )
      ->orderBy("fullname", "asc")
      ->get();

      $agentids=array();

      foreach ($all_agents as $item)
      { 
        $agentids[] = $item->id;
      }
       $agentids[] = 0;

      $livelocations = DB::table("ba_agent_locations") 
      ->join("ba_staff_attendance", "ba_staff_attendance.staff_id", "=", "ba_agent_locations.staff_id")
      ->whereIn("ba_agent_locations.staff_id", $agentids) 
      ->where("seq_no", 1)  
      ->whereDate("created_at",  date('Y-m-d') ) 
      ->whereDate("ba_staff_attendance.log_date",  date('Y-m-d') ) 
      ->get();

 

      $all_tasks = DB::table("ba_order_sequencer") 
      ->whereDate("entry_date",  date('Y-m-d') ) 
      ->get();

      $pndIds  =  $normalIds = array();

      $pndIds[] =  $normalIds[] = 0;
      foreach($all_tasks as $item)
      {
        if( $item->type == "pnd")
        {
          $pndIds[]  =  $item->id;
        }
        else 
        {
          $normalIds[] =  $item->id; 
        }
      }



      $pnd_task_lists = DB::table("ba_pick_and_drop_order") 
      ->join("ba_delivery_order", "ba_delivery_order.order_no", "=", "ba_pick_and_drop_order.id")
      ->whereIn("ba_pick_and_drop_order.id", $pndIds) 
      ->whereNotIn("book_status" ,  
        array( 'completed','cancel_by_client','cancel_by_owner', 'pickup_did_not_come', 'delivered','returned', 'canceled'))
      ->select("ba_pick_and_drop_order.id", "ba_delivery_order.member_id")  ;
 
      $all_task_lists = DB::table("ba_service_booking") 
      ->join("ba_delivery_order", "ba_delivery_order.order_no", "=", "ba_service_booking.id")
      ->union($pnd_task_lists) 
      ->whereIn("ba_service_booking.id", $normalIds) 
      ->whereNotIn("book_status" ,  
        array( 'completed','cancel_by_client','cancel_by_owner', 'pickup_did_not_come', 'delivered','returned', 'canceled'))
      ->select("ba_service_booking.id", "ba_delivery_order.member_id") 
      ->get();


      foreach ($all_agents as $item)
      {
        foreach ($livelocations as $litem)
        { 
          if( $item->id  == $litem->staff_id )
          { 
            $item->latitude = $litem->latitude;
            $item->longitude = $litem->longitude;
            break;
          } 
        }

        $taskid = array();
        foreach ($all_task_lists as $titem)
        {
          if( $item->id  == $titem->member_id )
          { 
            $taskid[] =   $titem->id;  
          } 
        }

        $item->orderNo = count($taskid) > 0 ? implode(",",  $taskid) : "No Task" ;
      }
 
      $data = array("agents" =>   $all_agents, 'livelocations' => $livelocations );
      return view("admin.agents.live_locations")->with('data',$data);

    }



  
    protected function fetchDeliveryAgentsPerformance(Request $request)
    {

       $date =  date('Y-m-d');  

        $all_staffs =  DB::table("ba_profile")   
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("is_booktou_staff", "yes" )
        ->where("ba_users.status", "<>", "100" )  
        ->where("ba_users.category",   "100" )  
        ->select("ba_profile.*", "ba_users.category" )
        ->get(); 


        $agent_ids = array(0);
        foreach( $all_staffs as $item)
        {
          $agent_ids[] = $item->id;
        }

        $normal_deliveries = DB::table("ba_service_booking")
        ->where('book_status', 'delivered')
        ->select("id", "delivery_charge", DB::Raw("'normal' order_type")) ;

        $all_orders = DB::table("ba_pick_and_drop_order")
        ->where('book_status', 'delivered')
        ->select("id", "service_fee as delivery_charge" , DB::Raw("'pnd' order_type"))
        ->union( $normal_deliveries)
        ->get();


        $active_staffs = DB::table("ba_staff_attendance")    
        ->whereDate("log_date",  $date )   
        ->get();

        $total_count = array();
          

        $normalDeliveries = Db::table('ba_service_booking')
        ->join('ba_delivery_order','ba_delivery_order.order_no',"=", 'ba_service_booking.id')
        ->whereIn("ba_delivery_order.member_id", $agent_ids )
        ->where("ba_service_booking.book_status", "delivered")
        ->select("member_id", DB::Raw('count(*) as total_count'), DB::Raw('sum(delivery_charge) as total_earned') )
        ->groupBy('ba_delivery_order.member_id') ;


        $allDeliveries = Db::table('ba_pick_and_drop_order')
        ->join('ba_delivery_order','ba_delivery_order.order_no',"=", 'ba_pick_and_drop_order.id')
        ->whereIn("ba_delivery_order.member_id", $agent_ids )
        ->where("ba_pick_and_drop_order.book_status", "delivered")
        ->select("member_id", DB::Raw('count(*) as total_count'), DB::Raw('sum(service_fee) as total_earned') )  
        ->groupBy('ba_delivery_order.member_id')
        ->union( $normalDeliveries )
        ->get();


      $data = array(  'date' => $date , 'staffs' => $all_staffs , 'active_staffs' => $active_staffs ,'all'=>$total_count, 'allDeliveries' => $allDeliveries  );  
       return view("admin.staffs.delivery_agent_performance")->with('data', $data); 

    }




     // Get Delivery Agent Earning Report for daily
     protected function viewAgentDailyEarningReport(Request $request)
     {

        $id=$request->aid;
        if($request->aid == "" )
        {
          return redirect("/admin/customer-care/delivery-agents")->with("err_msg", "No agent selected.");
        }

        $today =   date('Y-m-d'); 
        
       
        $totalEarning = 0; 
 

        $delivery_list  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $id )
        ->whereDate('accepted_date', $today)  
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            'book_status as orderStatus',
            'delivery_charge as deliveryCharge',
            "completed_on"
        )
        ->get();
 

        $customerIds =array();
        foreach ($delivery_list as $item) 
        {
            $totalEarning += $item->deliveryCharge;
          $customerIds[] = $item->orderBy;
        }

       
         $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();

        
       foreach ($delivery_list as $item) 
       {
            foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;break;
                }

            }   
       }

        $paymentInfo  = PaymentDealingModel::where("member_id", $id )
        ->whereRaw("date(bill_date) ='$today'")
        ->get();
  


      $data = array("totalEarning" => $totalEarning,  'delivery_list' =>  $delivery_list, 
         'payments' =>  $paymentInfo, 'aid' => $id );

      return view("admin.earning_clearance.agent_daily_earning")->with('data',$data);

     }


     protected function saveAgentDailyEarning(Request $request)
     {
        if( isset($request->btnSave) && $request->btnSave =="save")
           {
            if($request->aid == "" )
            {
              return redirect("/admin/customer-care/delivery-agents")->with("err_msg", "No agent selected.");
            }

             $id=$request->aid;


              $payments = new PaymentDealingModel; 
              $payments->member_id = $id; 
              $payments->amount = $request->paid_amount; 
              $payments->bill_date = date('Y-m-d H:i:s', strtotime($request->billing_date) );
              $payments->dealing_date = date('Y-m-d H:i:s' , strtotime($request->pay_date) );
              $payments->transact_no = $request->tsact_no; 
              $payments->clearance_type =  $request->clearance_type;
              $payments->remarks =  $request->remarks;
              $payments->save(); 
           }

        return redirect("/admin/customer-care/delivery-agent/get-daily-earning/" . $id ); 

     }
     

     protected function saveBusinessDailyEarning(Request $request)
     {
        if( isset($request->btnSave) && $request->btnSave =="save")
           {
            if($request->bin == "" )
            {
              return redirect("customer-care/business/view-all")->with("err_msg", "No business selected.");
            }

             $bin=$request->bin;

             $business_owner_info  = DB::table("ba_profile")
             ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id" )
             ->where("ba_users.bin",  $bin )
             ->select("ba_profile.*")
             ->first();

             if( !isset($business_owner_info ))
             {
                return redirect("/customer-care/business/view-all")->with("err_msg", "No business found.");
             }
  
              $payments = new PaymentDealingModel; 
              $payments->member_id = $business_owner_info->id ; 
              $payments->amount = $request->paid_amount; 
              $payments->bill_date = date('Y-m-d H:i:s', strtotime($request->billing_date) );
              $payments->dealing_date = date('Y-m-d H:i:s' , strtotime($request->pay_date) );
              $payments->transact_no = $request->tsact_no; 
              $payments->clearance_type =  $request->clearance_type;
              $payments->remarks =  $request->remarks;
              $payments->save(); 
           }

        return redirect("/admin/customer-care/business/get-daily-earning/" . $bin ); 

     }


     protected function searchAgentDailyEarningReport(Request $request)
     {

       
        if($request->aid == "" )
        {
          return redirect("/admin/customer-care/delivery-agents")->with("err_msg", "No agent selected.");
        }

         $id=$request->aid;
  
       if($request->filter_date == "" )
        {
          $today =   date('Y-m-d'); 
        }
        else
        {
            $today = date('Y-m-d', strtotime( $request->filter_date ));
        }
       
        $totalEarning = 0; 

        if( isset($request->btn_search) )
        {

          $delivery_list  = DB::table("ba_delivery_order")
          ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
          ->where("ba_delivery_order.member_id",$id)
          ->whereDate('accepted_date', $today)  
          ->select(
               "ba_service_booking.id as orderNo", 
              "book_by as orderBy",  
              'book_status as orderStatus',
              'delivery_charge as deliveryCharge',
              "completed_on"
          )
          ->get();
 

          $customerIds =array();
          foreach ($delivery_list as $item) 
          {
              $totalEarning += $item->deliveryCharge;
              $customerIds[] = $item->orderBy;
          }

       
         $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();

        
         foreach ($delivery_list as $item) 
         {
              foreach ($customers as $citem) 
              {
                  if($item->orderBy == $citem->orderBy )
                  {
                      $item->orderBy= $citem->fullname;break;
                  }

              }   
         }
          $paymentInfo  = PaymentDealingModel::where("member_id", $id )
          ->get();
   }
   $data = array("totalEarning" => $totalEarning,  'delivery_list' =>  $delivery_list, 
         'payments' =>  $paymentInfo, 'aid' => $id );

      return view("admin.earning_clearance.agent_daily_earning")->with('data',$data);

     }



    // Get Delivery Agent Earning Report for daily
    protected function allDeliveryAgentsDailyCollectionReport(Request $request)
    {
       if(isset($request->filter_date))
       {
          $today =   date('Y-m-d', strtotime($request->filter_date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }
        
        $totalEarning = 0;
 
        $agents  = DB::table("ba_profile")
        ->join("ba_staff_attendance", "ba_staff_attendance.staff_id", "=", "ba_profile.id")
        ->whereIn("ba_profile.id", function($query){
          $query->select("profile_id")->from("ba_users")
          ->where("category", '100' )
          ->where("status", '<>', '100' )
          ->where("franchise", 0 );

        })
        ->whereDate("log_date",    $today  )
        ->select("ba_profile.id", "ba_profile.fullname", "ba_profile.phone")
        ->get(); 

        $agentids = array(0);
        foreach($agents as $agent)
        {
          $agentids[] =$agent->id;
        }


      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->whereIn("ba_delivery_order.member_id", $agentids )
        ->whereDate('ba_service_booking.service_date', $today) 
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            DB::Raw("'customer' source"),
            "ba_service_booking.service_date",
            "ba_service_booking.preferred_time as delivery_time",
            'book_status as orderStatus',
            'seller_payable as totalAmount',
            'delivery_charge as serviceFee',
            "payment_type as payMode",
             DB::Raw("'booktou' paymentTarget"),
            DB::Raw("'normal' orderType"),
            "ba_delivery_order.member_id",
            "ba_service_booking.is_deposited"
        ) ; 

        $assist_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->whereIn("ba_delivery_order.member_id", $agentids )
        ->whereDate('ba_pick_and_drop_order.service_date', $today) 
        ->whereIn("request_from", array ('customer' ) )   
        ->whereIn("book_status", array('completed', 'delivered') )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'assist' orderType"),
            "ba_delivery_order.member_id",
            "ba_pick_and_drop_order.is_deposited"
        ) ;


        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->whereIn("ba_delivery_order.member_id", $agentids )
        ->whereDate('ba_pick_and_drop_order.service_date', $today)   
        ->whereIn("request_from", array ('business', 'merchant') )
        ->whereIn("book_status", array('completed', 'delivered') )
        ->union( $normalorders )        
        ->union($assist_orders)
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'pnd' orderType"),
            "ba_delivery_order.member_id",
            "ba_pick_and_drop_order.is_deposited"
        )
        ->get();
  

        $merchantIds =array();
        $customerIds =array();
        $toalamount =0.00;
        foreach ($all_orders as $item) 
        {
            $toalamount += $item->totalAmount + $item->serviceFee;

          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }
        $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;
                    break;
                }

            }
        }
                 
       }

 

       $data = array(  'date' =>$today  ,  
        "total_amount" => $toalamount,  
        'agents' =>  $agents,  
        'all_orders' =>  $all_orders );

      return view("admin.earning_clearance.all_agent_daily_collection")->with('data',$data);

     }


    // Get Delivery Agent Collection Report
    protected function deliveryAgentDailyCollectionReport(Request $request)
    {
       if(isset($request->filter_date))
       {
          $today =   date('Y-m-d', strtotime($request->filter_date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }

        $aid =0;
        if(isset($request->aid))
        {
            $aid =   $request->aid;  
        }
        else 
        {
          return redirect('/admin/delivery-agent/combined-daily-collection-report?filter_date=' . $today )
          ->with("err_msg", "No agent selected!");
        }
 
        
        $totalEarning = 0; 
        $agent_profile = DB::table("ba_profile") 
        ->where("ba_profile.id", $aid ) 
        ->select("ba_profile.id", "ba_profile.fullname","ba_profile.phone")
        ->first(); 
  
      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_service_booking.service_date', $today)  
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            DB::Raw("'customer' source"),
            "ba_service_booking.service_date",
            "ba_service_booking.preferred_time as delivery_time",
            'book_status as orderStatus',

            "total_cost as totalAmount",
            "item_total as itemTotal",
            "packing_charge as packagingCost",
            'delivery_charge as deliveryCharge',            
            'service_fee as serviceFee',
            "gst",
            
            "payment_type as payMode",
             DB::Raw("'booktou' paymentTarget"),
            DB::Raw("'normal' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        ) ; 


        $assist_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_pick_and_drop_order.service_date', $today)  
        ->whereIn("request_from", array ('customer' ) )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',

            DB::Raw('(item_total + packaging_cost) as totalAmount'),
            "item_total as itemTotal",
            "packaging_cost as packagingCost",
            'delivery_charge as deliveryCharge',            
            'service_fee as serviceFee',
            "tax as gst",

            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'assist' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        ) ; 

        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_pick_and_drop_order.service_date', $today) 
        ->whereIn("request_from", array ('business', 'merchant') ) 
        ->union( $normalorders )
        ->union( $assist_orders )        
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',

            DB::Raw('(item_total + packaging_cost) as totalAmount'),
            "item_total as itemTotal",
            "packaging_cost as packagingCost",
            'delivery_charge as deliveryCharge',            
            'service_fee as serviceFee',
            "tax as gst",

            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'pnd' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        )
        ->orderBy("orderNo" )
        ->get();


        $merchantIds =array();
        $customerIds =array();
        $toalamount =0.00;
        foreach ($all_orders as $item) 
        {
          $toalamount += $item->totalAmount + $item->serviceFee;
          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }


        $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
          {
            if($item->orderBy == $citem->orderBy )
            {
              $item->orderBy= $citem->fullname;
              break;
            }

          }
        }
                 
       }

       $data = array(  'date' =>$today  ,  
        "total_amount" => $toalamount,  
        'agent_info' =>  $agent_profile,  
        'all_orders' =>  $all_orders );
       return view("admin.earning_clearance.agent_daily_collection")->with('data',$data); 
    }


    //Get Delivery Agent Collection Report
    protected function printableDeliveryAgentDailyCollectionReport(Request $request)
    {
       if(isset($request->filter_date))
       {
          $today =   date('Y-m-d', strtotime($request->filter_date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }

        $aid =0;
        if(isset($request->aid))
        {
            $aid =   $request->aid;  
        }
        else 
        {
          return redirect('/admin/delivery-agent/combined-daily-collection-report?filter_date=' . $today )
          ->with("err_msg", "No agent selected!");
        }
 
        
        $totalEarning = 0; 
        $agent_profile = DB::table("ba_profile") 
        ->where("ba_profile.id", $aid ) 
        ->select("ba_profile.id", "ba_profile.fullname","ba_profile.phone")
        ->first(); 
  
      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_service_booking.service_date', $today)  
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            DB::Raw("'customer' source"),
            "ba_service_booking.service_date",
            "ba_service_booking.preferred_time as delivery_time",
            'book_status as orderStatus',

            'seller_payable as totalAmount',
            "item_total as itemTotal",
            "packing_charge as packagingCost",
            'delivery_charge as deliveryCharge',
            'service_fee as serviceFee',
            "gst",

            "payment_type as payMode",
             DB::Raw("'booktou' paymentTarget"),
            DB::Raw("'normal' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        ) ; 

 



        $assist_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_pick_and_drop_order.service_date', $today)  
        ->whereIn("request_from", array ('customer' ) )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',

            DB::Raw('(item_total + packaging_cost) as totalAmount'),
            "item_total as itemTotal",
            "packaging_cost as packagingCost",
            'delivery_charge as deliveryCharge',            
            'service_fee as serviceFee',
            "tax as gst",

            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'assist' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        ) ; 

        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_pick_and_drop_order.service_date', $today) 
        ->whereIn("request_from", array ('business', 'merchant') ) 
        ->union( $normalorders )
        ->union( $assist_orders )        
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',

            DB::Raw('(item_total + packaging_cost) as totalAmount'),
            "item_total as itemTotal",
            "packaging_cost as packagingCost",
            'delivery_charge as deliveryCharge',            
            'service_fee as serviceFee',
            "tax as gst",

  
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'pnd' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        )
        ->orderBy("orderNo" )
        ->get();
  

        $merchantIds =array();
        $customerIds =array();
        $toalamount =0.00;
        foreach ($all_orders as $item) 
        {
          $toalamount += $item->totalAmount + $item->serviceFee;
          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }


        $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;
                    break;
                }

            }
        }
                 
       }


  $pdf_file =  "deposit_slip_" .time(). '_' .   ".pdf";
  $save_path = public_path() . "/docs/"  .  $pdf_file    ;

       $data = array(  'date' =>$today  ,  
        "total_amount" => $toalamount,  
        'agent_info' =>  $agent_profile,  
        'all_orders' =>  $all_orders );
      $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('admin.earning_clearance.daily_deposit_slip_a4' ,  $data  )   ;
     // return view('admin.earning_clearance.daily_deposit_slip_a4')->with( $data );
 
return $pdf->download( $pdf_file  ); 
     }




    //prepare delivery commission
    protected function updateDeliveryAgentDailyCommission(Request $request)
    {
        if(isset($request->filter_date))
        {
          $today =   date('Y-m-d', strtotime($request->filter_date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }

        $aid =0;
        if(isset($request->aid))
        {
            $aid =   $request->aid;  
        }
        else 
        {
          return redirect('/admin/delivery-agent/update-daily-delivery-commission?filter_date=' . $today )
          ->with("err_msg", "No agent selected!");
        }

        if(isset($request->btnsave))
        {
          //update commission  
          foreach( $request->comms as $orderno => $comm) {
            
            $agent_comm = AgentEarningModel::where("member_id", $aid  )
            ->where("order_no",  $orderno)
            ->first();

            if( !isset($agent_comm))
            {
              $agent_comm = new AgentEarningModel ;
            }
            $agent_comm->member_id  = $aid ;
            $agent_comm->order_no  = $orderno ;
            $agent_comm->commission  =$comm ;
            $agent_comm->save(); 
          }  
        }

            
        
        $totalEarning = 0; 
        $agent_profile = DB::table("ba_profile") 
        ->where("ba_profile.id", $aid ) 
        ->select("ba_profile.id", "ba_profile.fullname","ba_profile.phone")
        ->first();  
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_service_booking.service_date', $today)  
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            DB::Raw("'customer' source"),
            "ba_service_booking.service_date",
            "ba_service_booking.preferred_time as delivery_time",
            'book_status as orderStatus',
            'seller_payable as totalAmount' ,
            'delivery_charge as serviceFee',
            "payment_type as payMode",
             DB::Raw("'booktou' paymentTarget"),
            DB::Raw("'normal' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        ) ; 


        $assist_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_pick_and_drop_order.service_date', $today)  
        ->whereIn("request_from", array ('customer' ) )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            DB::Raw('(total_amount + packaging_cost) as totalAmount'), 
            'service_fee as serviceFee',
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'assist' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        ) ; 

        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_pick_and_drop_order.service_date', $today) 
        ->whereIn("request_from", array ('business', 'merchant') ) 
        ->union( $normalorders )
        ->union( $assist_orders )        
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            DB::Raw('(total_amount + packaging_cost) as totalAmount'), 
            'service_fee as serviceFee',
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'pnd' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        )
        ->orderBy("orderNo" )
        ->get();


        $merchantIds =array();
        $customerIds =array();
        $oids =array();
        $toalamount =0.00;
        foreach ($all_orders as $item) 
        {
          $oids[] =  $item->orderNo;

          $toalamount += $item->totalAmount + $item->serviceFee;
          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        } 

        $oids[] = $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
          {
            if($item->orderBy == $citem->orderBy )
            {
              $item->orderBy= $citem->fullname;
              break;
            }

          }
        }
                 
       }

       //commissions
       $order_commissions  = DB::table("ba_agent_earning")
        ->where("member_id", $aid  )
        ->whereIn("order_no",   $oids) 
        ->get();


       $data = array(  'date' =>$today  ,  
        "total_amount" => $toalamount,  
        'agent_info' =>  $agent_profile,  
        'all_orders' =>  $all_orders ,
        'order_commissions' => $order_commissions);
       return view("admin.earning_clearance.agent_daily_commission")->with('data',$data); 
    }



    //Get Delivery Agent Earning Report for daily
    protected function getDeliveryAgentDailyOrdersReport(Request $request)
    {

        $id=$request->agent;
        if($request->agent == "" )
        {
          return redirect("/admin/customer-care/delivery-agents")->with("err_msg", "No agent selected.");
        }

        if(isset($request->filter_date))
        {
          $today =   date('Y-m-d', strtotime($request->filter_date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }
        
        $totalEarning = 0;

      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $id )
        ->whereDate('ba_service_booking.service_date', $today)  
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            'book_status as orderStatus',
            'seller_payable as totalAmount',
            'delivery_charge as serviceFee',
            "payment_type as payMode",
            DB::Raw("'normal' orderType")
        ) ;

        


        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $id )
        ->whereDate('ba_pick_and_drop_order.service_date', $today)   
        ->union( $normalorders )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            DB::Raw("'pnd' orderType")
        )
        ->get();
 
 

        $merchantIds =array();
        $customerIds =array();
        $totalPnDAmount =0.00;
        foreach ($all_orders as $item) 
        {
          $totalPnDAmount += $item->totalAmount + $item->serviceFee;

          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }
        $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;
                    break;
                }

            }
        }
             

               
       }

       
       $profile = CustomerProfileModel::find($id);


      $data = array( "agentName" =>  $profile->fullname ,   'agent' => $id, 'date' =>$today  , 
        "total_amount" => $totalPnDAmount,  'all_orders' =>  $all_orders );

      return view("admin.earning_clearance.agent_daily_orders")->with('data',$data);

     }



     // Get Delivery Agent Earning Report for monthly
    protected function viewMonthlyEarningReport(Request $request)
    {

        $id=$request->a_id;
 
       if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }

        if($request->month !="" )
        {

            $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }
        $totalEarning = 0; 

        if( isset($request->btnSearch) )
      {


        $delivery_list  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no") 
        ->whereMonth('completed_on', $month )
        ->whereYear('completed_on', $year )
        ->select(
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            'book_status as orderStatus',
            'delivery_charge as deliveryCharge'
        )
        ->get();
 

        $customerIds =array();
        foreach ($delivery_list as $item) 
        {
            $totalEarning += $item->deliveryCharge;
          $customerIds[] = $item->orderBy;
        }

       
         $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();

        
       foreach ($delivery_list as $item) 
       {
            foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;break;
                }

            }   
       }
   }
   else
   {
    $delivery_list  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id",$id)
        ->whereMonth('completed_on', $month )
        ->whereYear('completed_on', $year )
        ->select(
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            'book_status as orderStatus',
            'delivery_charge as deliveryCharge'
        )
        ->get();
 

        $customerIds =array();
        foreach ($delivery_list as $item) 
        {
            $totalEarning += $item->deliveryCharge;
          $customerIds[] = $item->orderBy;
        }

       
         $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();

        
       foreach ($delivery_list as $item) 
       {
            foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;break;
                }

            }   
       }
   }

      $data = array("totalEarning" => $totalEarning,  'results' =>  $delivery_list, 'aid' => $id );


      return view("admin.earning_clearance.agent_monthly_earning")->with('data',$data);

     }



    // Update payment
    public function updatePayment(Request $request)
    { 
      
 
     if( isset($request->btnSave) && $request->btnSave =="save"):
     
         $id=$request->a_id;
    
          $payments = new PaymentDealingModel; 
          $payments->member_id = $id; 
          $payments->amount = $request->paid_amount; 
          $payments->bill_date = date('Y-m-d H:i:s' );
          $payments->dealing_date = date('Y-m-d H:i:s' );
          $payments->status = "yes";
      
          $payments->save();

          return back();

        else:
        endif;

  }

 

  public function viewImportableProducts(Request $request)
  {

    $bin = $request->bin;


    $products= DB::table("ba_products_imported")
    ->where("bin",  $bin)
    ->paginate(50);


    $category= DB::table("ba_product_category")  
    ->get();


    $business=  Business::find( $bin);



    $data = array('products' => $products, 'category' => $category, 'bin' => $bin, 'business' => $business ); 
    return view("admin.imports.importable_products")->with('data',$data); 

  }


  public function saveImportableProducts(Request $request)
  {

     $bin = $request->bin;
 


     $pr_code_count  = ProductModel::where("bin", $request->bin ) 
     ->count()  ;
 
    $pr_code_count += 1;
    

     $added_ids = array();

     for( $i= 0 ; $i < count( $request->cb_select); $i++ )
     {
 

        if( isset($request->cb_select[$i]) &&  $request->cb_select[$i] != "") 
        {

          $pos = $request->cb_select[$i]; 
          $pr_code = substr($request->pr_category[$pos],0, 3) .  substr($request->pr_name[$pos],0, 3)  . $pr_code_count; 
          $pr_code_count++;

          $product = new ProductModel ; 
          $product->bin = $bin ;
           $product->pr_code =   $pr_code  ;
           $product->pr_name = $request->pr_name[$pos];
           $product->gst_code =0;
           $product->mfr_id = 9;
           $product->description = $request->pr_desc[$pos];
           $product->initial_stock = $request->pr_stock[$pos];
           $product->stock_inhand = $request->pr_stock[$pos];
           $product->min_required =5;

           $product->category = $request->pr_category[$pos]; 
           $product->max_order = 5;  
           $product->unit_price = $request->pr_price[$pos]; 
           $product->unit_value = $request->pr_unit[$pos]; 
           $product->unit_name = $request->pr_unitname[$pos]; 
           $save=$product->save();  

           if($save)
           {
              $added_ids[] = $request->id[$pos];
           }
  
     

       }
     }
 

    DB::table("ba_products_imported")
     ->whereIn("id", $added_ids)
    ->delete(); 


    return redirect("/admin/customer-care/business/view-importable-product/" . $bin)->with("err_msg", "All selected products are imported.");
 
  }




  public function viewPromotionBusinesses(Request $request)
  {
    $businessData= DB::table("ba_business") 
    ->join("ba_premium_business", "ba_premium_business.bin", "=", "ba_business.id")
    ->where("category",'VARIETY STORE') 
      ->select("ba_business.id","name","tags","shop_number","locality","landmark","city","state","rating","pin","is_open","category", 
        DB::Raw("'YES' buttonStatus"))
      ->get();

     

       $data = array('businesses' => $businessData  ); 
          //return json_encode( $data);
       return view("admin.store.premium_businesses")->with('data',$data);
    }



   public function saveBusinessPromotion(Request $request) 
     {
      if( isset($request->btnSave) && $request->btnSave =="save")
      {  
      
       $businessId  = $request->bin;
      // return json_encode($businessId);

       $business_info =  Business::find($businessId);

       if (!$business_info) 
       {

         return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business record found!." ); 
       }

       $prem_busi_info = PremiumBusiness::where('bin', $business_info->id)
       ->first();

      
        if( !isset($prem_busi_info))
        {
          $prem_busi_info = new PremiumBusiness; 
        } 
        
        $prem_busi_info->bin  = $business_info->id; 
        $prem_busi_info->start_from  = date('Y-m-d H:i:s'); 
        $prem_busi_info->ended_on = date("Y-m-d H:i:s",strtotime("+30 day"));
        $prem_busi_info->save();
        return redirect("/admin/customer-care/business/view-all")->with("err_msg", "Business promo updated successfully." );

    }
    else
    {
      return redirect("/admin/customer-care/business/view-all")->with("err_msg", "Business promo update failed!." );

    }


   }



    public function updateBusinessVisibility(Request $request) 
    {
      if( isset($request->btnSave) && $request->btnSave =="save")
      {  
        $businessId  = $request->bin; 
        $business_info =  Business::find($businessId);
        if (!$business_info) 
        {
          return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business record found!." ); 
        }
        $business_info->limit_by_distance  = $request->dbvisibility;  
        $business_info->save();
        return redirect("/admin/customer-care/business/view-all")->with("err_msg", "Business visibility updated successfully." );

    }
    else
    {
      return redirect("/admin/customer-care/business/view-all")->with("err_msg", "Business visibility update failed!." ); 
    } 

   }

    public function deleteFromPromoList(Request $request) 
    {

      if( isset($request->btnDelete) && $request->btnDelete =="delete")
      {
        $businessId  = $request->bin;
        PremiumBusiness::where("bin",$businessId)->delete();


        return redirect("/admin/customer-care/business/view-promotion-businesses")->with("err_msg", "Business successfully deleted from promo-list!." ); 
       }
       else
       {
         return redirect("/admin/customer-care/business/view-promotion-businesses")->with("err_msg", "Business deleted from promo-list failed.Please consult admin!" );
       }

   }




  public function prepareProductImportExcel(Request $request) 
  {

    $all_business  = DB::table("ba_business") 
       ->get(); 


    $data = array('Title' => "Product Import from Excel", 'businesses' => $all_business ); 
          //return json_encode( $data);
    return view("admin.imports.product_import")->with('data',$data);

   }



  public function uploadProductImportExcel (Request $request) 
  {

     $excel_path ="none";
     if(isset($request->file))
     {
       $file = $request->file;
       $originalname = $file->getClientOriginalName();
       $extension = $file->extension( );
       $filename = "products_" .  time(). ".{$extension}";
       $request->file->storeAs('uploads',  $filename );
       $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename,
       public_path().'/upload/docs/'. $filename);
       $excel_path = public_path() . '/upload/docs/'. $filename ; 
       //saving in DB
       $newfile = new ImportProductFileModel;
       $newfile->filepath =  $excel_path  ;
       $newfile->bin = $request->bin;
       $newfile->save(); 

     }
 
     if(file_exists( $excel_path  ))
     {
       Excel::import(new ProductsImport,  $excel_path );
       return redirect('/admin/systems/view-imported-products')->with('err_msg', 'Products are imported. Verify and publish them!');
     } 

   }
 
   
    public function viewProductImportExcel (Request $request) 
    {

        $products= DB::table("ba_products_imported") 
        ->where("category",$productCategory->category_name)
        ->get();

 
    }



    public function viewProductImages(Request $request) 
    {

      $bin = $request->bin; 
      $pid = $request->pid;

      $business=  Business::find( $bin);

      $product= DB::table("ba_products")
      ->join("ba_product_photos","ba_product_photos.prid","=", "ba_products.id")
      ->where("ba_products.id",  $pid)
      ->first();


      if( !isset($business) || !isset($product))
      {
        return redirect("/admin/customer-care/business/view-all")->with("err_mgs", "No business or product found!");
      }


      $category= DB::table("ba_product_category")  
      ->get();

 

    $data = array( 
      'title'  => 'View Product Image',
      'product' => $product, 
      'category' => $category, 
      'bin' => $bin, 
      'business' => $business, 
      'pid' => $pid
     ); 
    return view("admin.store.products.view_product_image")->with('data',$data);

   }

 
   

    public function uploadProductImage(Request $request) 
    {

      $bin = $request->bin; 
      $pid = $request->pid;

      if( $pid == ""  ||  $bin == "" )
      {
         return redirect("/admin/customer-care/business/view-all")->with('detailed_msg', 'Missing data to perform action.' );
      }

      if( $bin  > 0 )
      {
        $folder_url  = '/assets/image/store/bin_'. $bin   ;
        $folder_path = public_path(). '/assets/image/store/bin_'.  $bin    ;
      }

      if( !File::isDirectory($folder_path))
      {
         File::makeDirectory( $folder_path, 0777, true, true);
      }

      $file_names=array();
 
    //get all existing images  

     $e_photos = DB::table("ba_product_photos") 
     ->where("prid",  $pid ) 
     ->select("image_url")
     ->first() ;

     if(isset($e_photos))
     {
       $file_names = explode("," , $e_photos->image_url);
     }
 

    $i=1;
    foreach($request->file('photos') as $file)
    {

      $originalname = $file->getClientOriginalName();
      $extension = $file->extension( );
      $filename = "prim_" .  $pid  . $i . time()  . ".{$extension}";
      $file->storeAs('uploads',  $filename );
      $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
      $file_names[] =  $filename ;
      $i++;

    }

    $photos =  implode(",", $file_names);

    DB::table("ba_product_photos") 
    ->where("prid", $pid  ) 
    ->update(['image_url' => $photos ]) ; 

      return redirect("/admin/customer-care/business/view-product-photo/$bin/$pid" );

   }


   
   protected function manageCloudMessages(Request $request)
   {

      $today = date('Y-m-d');
      $cloud_message= DB::table("ba_cloud_message") 
      ->orderBy("id", "desc")
      ->paginate(12);
      $biz = Business::get();

      $data = array( 'title'=>'Manage Cloud Messages' ,'results' => $cloud_message, 'breadcrumb_menu' => 'marketing_sub_menu','business'=>$biz);
      return view("admin.cloud_message.view_all")->with('data',$data);
    }


  protected function broadcastCloudMessage(Request $request)
  {

      $cmid = $request->cmid; 
      $msg = CloudMessageModel::find( $request->cmid) ; 
      
      if( !isset($msg) )
      {
         return redirect("/admin/systems/manage-cloud-messages" )->with("err_msg", "No cloud message found.");
      }
      $business_name = DB::table("ba_business")
      ->where('id', $msg->bin)
      ->select('name')
      ->first();
     
      $service = DB::table("ba_service_products")
      ->where('id',$msg->package_id)
      ->select('srv_name')
      ->first();

      $product = DB::table("ba_products")
      ->where('id',$msg->prid)
      ->select('pr_name','variance_in as varianceIn')
      ->first();
 
      if (isset($product)) 
      {
          if($product->varianceIn == "")
        {
          $varianceIn = "na";   
        }
        else 

          $varianceIn = $product->varianceIn;
      }

      else { $varianceIn = "na"; }
            
    $notif_body = array();

    if( $request->newtitle != "" && $request->newbody != "" )
    {
      $response['title']     =  $request->newtitle  ;
      $response['message']   = $request->newbody ; 

      $notif_payload = array(
        "title" =>  $request->newtitle , 
        "message" =>  $msg->body, 
        "body" =>  $msg->body
      );
  
      //updating notification details
      $msg->title = $request->newtitle  ;
      $msg->body = $request->newbody ; 
      $msg->save();

    }
    else
    {

      $notif_payload = array(
          "title" =>  $request->newtitle , 
          "message" =>  $msg->body, 
          "body" =>  $msg->body
        ); 

      $notif_body['title']     = $msg->title;
      $notif_body['message']   = $msg->body;  
      $notif_body['body']   = $msg->body;  
    }
    
    $response['image'] = $msg->image_url ;  
    $firebase = new FirebaseCloudMessage(); 
    $response = $firebase->sendToTopic ( "BOOKTOU_GENERAL_TOPIC", $response );

     if ( $response === false ) 
       {
        $errmsg =  'Cloud message could not be send.'  ; 
       }
       else 
       {
        $errmsg ="Cloud message send.";
       }

 

    $payload_data['bin'] = $msg->bin;
    $payload_data['bizName'] = $business_name->name;
    $payload_data['productId'] = $msg->prid;
    $payload_data['productSubId'] = $msg->bsin;    
    $payload_data['varianceIn'] =$varianceIn;
    $payload_data['packageId'] = $msg->package_id; 



    //go to firebase library file uploaded from API project
    $firebase = new FirebaseCloudMessage();
    $response = $firebase->sendTopic( "BOOKTOU_GENERAL_TOPIC",   $notif_body,  $payload_data   );
 

    if ( $response === false ) 
    {
      $errmsg =  'Cloud message could not be send.'  ; 
    }
    else 
    {
      $errmsg ="Cloud message send.";
    }


    return redirect("/admin/systems/manage-cloud-messages" )->with("err_msg", $errmsg );

}



public function saveCloudMessage(Request $request)
    {
    
      if($request->title == "" || $request->body == ""  || $request->product == "" )
      {
        return redirect("/admin/systems/manage-cloud-messages")->with("err_msg", 'Missing data to perform action.'  ); 
      }           
       
        if( isset($request->photo) )
        {
          $imageName = "promo_ad_" . time() . '.' .$request->photo->getClientOriginalExtension();
          $request->photo->move( config('app.app_cdn_path')  . '/webapp/upload' , $imageName); 
          $url = config('app.app_cdn') . '/webapp/upload/' . $imageName ; 
        }
        else 
        {
          $url = "https://booktou.in/assets/images/home_banner.jpg";
        }
 

        $business = $request->business;
        
        $business_explode = explode(',', $business);
        $bin =(int)$business_explode[0];
        $business_name = $business_explode[1];
        $product_details = $request->product;
        $product_explode = explode(',', $product_details);
        $package_id = $product_explode[0];
        $product_id = $product_explode[1];
        $bsin = $product_explode[2];

        $cloudmsg = new CloudMessageModel; 
        $cloudmsg->title=  $request->title;
        $cloudmsg->body = $request->body;
        $cloudmsg->bin = $bin;
        $cloudmsg->business_name = $business_name;
        $cloudmsg->prid = $product_id;
        $cloudmsg->bsin = $bsin;
        $cloudmsg->package_id = $package_id;
        $cloudmsg->image_url = $url ; 
        $cloudmsg->expire_on = date('Y-m-d H:i:s'); 
        $save=$cloudmsg->save();

        if($save)
        {
         return redirect("/admin/systems/manage-cloud-messages")->with("err_msg", 'Cloud message saved.'  );

        }

        return redirect("/admin/systems/manage-cloud-messages")->with("err_msg", 'fail to save message.'  );  
     
    } 
//cloud msg delete
    protected function broadcastCloudMessageDelete(Request $request)
    {
     $key = $request->key;

     if ($request->key=="") {
      return redirect("/admin/systems/manage-cloud-messages")
      ->with("err_msg","field missing!");
    }
    $delete = DB::table("ba_cloud_message")
    ->where("id",$key) 
    ->delete();

    if ($delete) {
      return redirect("/admin/systems/manage-cloud-messages")
      ->with("err_msg","record deleted!");
    }else{
      return redirect("/admin/systems/manage-cloud-messages")
      ->with("err_msg","failed!");
    }
  }
//end msg delete
    public function fetchBusinessProductList(Request $request)
    {

      $bin = $request->items;


       $services = DB::table('ba_service_products')
       ->where('bin',$bin)
       ->select("srv_name","id as package_id",DB::Raw("'0' product_id"),DB::Raw("'0' prsubid")); 

      $products = DB::table('ba_products')
            ->union( $services)
            ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")
            ->where('bin',$bin)
            ->select("pr_name as srv_name",DB::Raw("'0' package_id"),"id as product_id","prsubid")
            ->get();

       $data = array('products'=>$products);
       $html  = view('admin.cloud_message.business_product')->with($data)->render();
       $code = 2000;
       return response()->json(array('success'=>true,'code'=>$code,'html'=>$html));
       //return response()->json(array('success'=>true,'html'=>$returnHTML));
    }



  protected function manageCustomers(Request $request)
  {


      $cloud_message= DB::table("ba_cloud_message") 
      ->get();
     

     $keyname = $request->search_key;

     if($keyname != "") 
        $key_name_where = " fullname like '%$keyname%' or ba_profile.phone like '%$keyname%' ";
    else
      $key_name_where = "  id > 0";



      if( isset($request->btn_search) )
      {
        if($request->filter_key=="Present") 
        {
           $customers= DB::table("ba_profile")
           ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
           ->select("ba_profile.*", "ba_users.firebase_token", "ba_users.category", "ba_users.otp")
           ->where("category", "0")
           ->whereRaw( $key_name_where  )
           ->where("ba_users.firebase_token","!=", "") 
           ->orderBy("fullname")
           ->paginate(50);
        }
        else if ($request->filter_key=="Absent") 
        {
           $customers= DB::table("ba_profile")
           ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
           ->select("ba_profile.*", "ba_users.firebase_token","ba_users.category",  "ba_users.otp")
           ->where("category", "0")
           ->whereRaw( $key_name_where  )
           ->whereNull("ba_users.firebase_token") 
           ->orderBy("fullname")
           ->paginate(50);
        }
        else   
        {
           $customers= DB::table("ba_profile")
          ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
          ->select("ba_profile.*", "ba_users.firebase_token", "ba_users.category", "ba_users.otp")
          ->where("category", "0")
          ->whereRaw( $key_name_where  )
          ->orderBy("fullname")
          ->paginate(50); 
        }
        
       }
       else 
       {

         $customers= DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->select("ba_profile.*", "ba_users.firebase_token", "ba_users.category", "ba_users.otp")
        ->where("category", "0")
        ->orderBy("fullname")
        ->paginate(50); 

       }


       $cid = array(0);
       foreach($customers as $item)
       {
        $cid[] = $item->id;
       }

      $orders= DB::table("ba_service_booking") 
      ->select("book_by",  "book_status")
      ->whereIn("book_by",  $cid ) 
      ->get();


     
      $data = array( 'title'=>'Manage All Customers' ,'results' => $customers, 'messages' => $cloud_message, 'orders' =>$orders  );
      return view("admin.customers.view_all")->with('data',$data);
    }
    
    protected function viewMigrationReadylist(Request $request)
    {

      if ($request->month) {
       $month = $request->month;
      }else
      {
        $month = date('m');
      }

      if ($request->year) {
        $year = $request->year;
      }else
      {
        $year = date('Y');
      }

      $customers= DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->select("ba_profile.*", "ba_users.firebase_token", "ba_users.category", "ba_users.otp")
        ->where("category", "0")
        ->where("ba_users.franchise", 0)
        ->orderBy("fullname")
        ->get(); 

      $cid = $phone = array(0);
      foreach ($customers as $cus) {
         $cid[] = $cus->id;
         $phone[] = $cus->phone;
      }
 
      $pndorders = DB::table("ba_pick_and_drop_order")
      ->select(DB::Raw("drop_phone as phoneNo, drop_name, count(*) as orderCount"))
      ->whereIn("drop_phone", function($query){ 
          $query->select(DB::Raw("distinct drop_phone"))
          ->from("ba_pick_and_drop_order"); 
        })
      ->whereNotIn("drop_phone", function($query){ 
          $query->select("phone")
          ->from("ba_profile"); 
        })
      ->whereMonth("service_date",$month)
      ->whereYear("service_date",$year)
      ->groupBy("drop_phone")
      ->groupBy("drop_name")
      ->orderBy("orderCount","desc")
      ->paginate(50); 
      $data = array("pndorders"=>$pndorders); 
      
      return view("admin.customers.customer_list_for_migration")->with('data',$data);

    }


     protected function viewFrequentPurchaseCustomer(Request $request)
    {

      if ($request->month) {

       $month = $request->month;
      }else
      {
        $month = date('m');
      }

      if ($request->year) {
        $year = $request->year;
      }else
      {
        $year = date('Y');
      }

      $customers= DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->select("ba_profile.*", "ba_users.firebase_token", "ba_users.category", "ba_users.otp")
        ->where("category", "0")
        ->where("ba_users.franchise", 0)
        ->orderBy("fullname")
        ->get(); 

      $cid = $phone = array(0);
      foreach ($customers as $cus) {
         $cid[] = $cus->id;
         $phone[] = $cus->phone;
      }
      
      $normalorders= DB::table("ba_service_booking")
      ->join("ba_profile","ba_service_booking.book_by","=","ba_profile.id") 
      ->select( 
        DB::Raw("count(*) as orderCount"), 
        "book_by",  
        "book_status",
        "ba_profile.fullname as customerName",
        "phone as phoneNo",
        "locality",
        "ba_profile.landmark",
        "ba_profile.id as customerid" 
      )
      ->whereIn("book_status",array("delivered","completed"))
      ->whereIn("book_by",  $cid )
      ->whereMonth("service_date",$month)
      ->whereYear("service_date",$year)
      ->orderBy("orderCount","desc") 
      ->groupBy("book_by")
      ->groupBy("book_status")
      ->paginate(50)
      ;
      
      //dd($normalorders);
      
      $data = array("orders"=>$normalorders); 
      
      return view("admin.customers.frequently_order_online_customer_list")->with('data',$data);

    }
    
    public function viewCustomerProfile(Request $request)
    {

        $profileId = $request->cus_id;

        $customerProfile = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_profile.id",$profileId)
        ->select("ba_profile.id as cusProfileId","fullname as cusFullName", 
                 "ba_profile.dob as cusDateOfBirth", "ba_profile.locality as cusLocality", 
                 "ba_profile.landmark as cusLandmark", "ba_profile.city as cusCity",
                 "ba_profile.state as cusState", "ba_profile.pin_code as cusPin",
                 "ba_profile.phone as cusPhone", "ba_profile.email as cusEmail",
                  "ba_profile.profile_photo as cusImage", "ba_users.flagged")
        ->first();

        $orders = DB::table("ba_service_booking")
        ->where("book_by",$profileId)
        ->orderBy("id","desc")
        ->get();

       $rate = $ratedata =  $rateAvg = $ratingFiveCount = $ratingFourCount = 
        $ratingThreeCount =  $ratingTwoCount =  $ratingOneCount ="0";  

      
      $reviews = DB::table("ba_customer_reviews")
      ->where("customer_id", $profileId)
      ->get();


      $ban_list = DB::table("ba_ban_list")
      ->where("member_id", $profileId)
      ->orWhere("phone", $customerProfile->cusPhone )
      ->first();

 

      foreach($reviews as $item)
      {
              
        $rate += $item->rating;
        $ratedata = ''.$rate.'';
        
      }
      
     // return ($ratedata);
      
      $count = $reviews->count();
      if($count>1){
          $checkrate = $rate / $count;
          $rateAvg = $checkrate;
          $rateAvgdata = ''.$rateAvg.''; 
      }
      else{

           $rateAvgdata = ''.$rate.'';

      }


      $countRating = DB::table("ba_customer_reviews")
      ->where("customer_id", $profileId)
      ->select('rating',DB::raw('count(id)  as 
          total_count'))
      ->groupBy("rating")
      ->get();
      foreach ($countRating as $keyitem) {
        if($keyitem->rating ==1){
          $ratingOneCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==2){
          $ratingTwoCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==3){
          $ratingThreeCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==4){
          $ratingFourCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==5){
          $ratingFiveCount= $keyitem->total_count;
        }
        else{

        }
      }
  

        $data = array('profiles' => $customerProfile, 
            'orders' => $orders, 
            'reviews' => $reviews,
            'banDetails' => $ban_list,
                      'totalRating' => $ratedata,
                      'ratingAverage'=>$rateAvgdata, 
                      'ratingFiveCount' => $ratingFiveCount,
                      'ratingFourCount' => $ratingFourCount,
                      'ratingThreeCount' => $ratingThreeCount,
                      'ratingTwoCount' => $ratingTwoCount,
                      'ratingOneCount' => $ratingOneCount);
    
     return view("admin.customers.customer_profile")->with("data",$data);  
    }
    


    protected function manageVouchers(Request $request)
   {
  
      $codes= DB::table("ba_coupons")
      ->orderBy("id", "desc")
      ->get();

 
      $businesses= DB::table("ba_business")
      ->orderBy("name", "asc")
      ->get();

      $biz_categories= DB::table("ba_business_category")
      ->orderBy("name", "asc")
      ->get();


      $data = array( 'title'=>'Manage All Vouchers' ,'results' => $codes , 'businesses' => $businesses , 'biz_categories' =>$biz_categories);
      return view("admin.vouchers.view_all")->with('data',$data);
    }


   protected function saveCoupon(Request $request)
   {

     $coupon = new CouponModel;
     $coupon->code = $request->voucherCode; 
     $coupon->bin = $request->business;
     $coupon->biz_category = $request->category; 
     $coupon->total_discount = $request->discountSeller;
     $coupon->product_discount_pc = $request->discountSeller;
     $coupon->valid_from = date('Y-m-d', strtotime($request->validdate));
     $coupon->ends_on = date('Y-m-d', strtotime($request->enddate));


    $coupon->start_time = date('H:i:s', strtotime($request->validtime));
    $coupon->end_time = date('H:i:s', strtotime($request->validendtime));

     $coupon->description =  $coupon->validity_text = $request->description;
     $save = $coupon->save();

     if($save)
     {
        $msg = "Voucher code is saved!";
     }
     else 
     {
      $msg = "Voucher code could not be saved!";
     } 
  
      return redirect("/admin/systems/manage-voucher-codes")->with("err_msg", $msg);
    }



    protected function applyCoupon(Request $request)
    {

      $orderno = $request->order_no;
      $code = $request->coupon;

      $order_info = ServiceBooking::find($orderno);   
      if( !isset($order_info))
      {
        return redirect("admin/customer-care/order/view-details/" . $orderno )->with("err_msg", 'No order found to apply coupon.'  );  
      }
 

      $order_costing =  DB::table("ba_shopping_basket")
      ->where("order_no", $orderno  ) 
      ->get();
 

      if( !isset($order_costing))
      {
        return redirect("admin/customer-care/order/view-details/" . $orderno )->with("err_msg", 'No order found to apply coupon.'  );  
      }

      $item_cost = 0.00;
      $packaging_cost = 0.00;
      foreach($order_costing as $item)
      {
        $item_cost  += $item->qty * $item->price;  
        $packaging_cost +=  $item->qty * $item->package_charge;
      }
 

      $code_info= DB::table("ba_coupons")
      ->where("code", $code)
      ->where("bin", $order_info->bin )
      ->first();
 
      if(  isset($code_info))
      {

          //check if discount is sapplicable for the product group
          if( strcasecmp($order_info->book_category,   $code_info->biz_category ) != 0  )
          {
              return redirect("admin/customer-care/order/view-details/" . $orderno )->with("err_msg", 'Coupon is invalid!'  );   
          } 

            if( $code_info->product_discount_pc > 0 )
            {
              $total_discount  = ( $code_info->product_discount_pc / 100 ) *  $item_cost ;  
            }
            else 
            {
                $total_discount =  0;
            } 
 
          $order_info->coupon  = $code; 
          $order_info->discount =  $total_discount;
          $order_info->total_cost  =  $order_info->seller_payable  =  $item_cost  +  $packaging_cost  ;
          $order_info->save();

          return redirect("/admin/customer-care/order/view-details/" .  $orderno )->with("err_msg", 'Coupon discount applied.'  ); 
          
      }
      else 
      {

        return redirect("/admin/customer-care/order/view-details/" .  $orderno )->with("err_msg", 'Invalid Coupon Code.'  );     
      }
 
    }



    protected function saveDeliveryCharge(Request $request)
    {

      $orderno = $request->order_no;
      $deliverycharge = $request->deliverycharge;
      $order_info = ServiceBooking::find($orderno);
 

        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/orders/view-all")->with("err_msg", 'No order found to change delivery charge.'  );  
        }
        else 
        {

          $order_info->agent_payable  =  (0.6  *  $deliverycharge  ) ; //60% of total charge
          $order_info->service_fee  =  $deliverycharge -  $order_info->agent_payable ; //60% of total charge  
          $order_info->delivery_charge = $deliverycharge; 
          $order_info->save(); 
          return redirect("/admin/customer-care/order/view-details/" .  $orderno )->with("err_msg", 'Delivery charge updated.'  ); 
        } 
     
 
    }

 
     protected function removeCouponDiscount(Request $request)
    {

      if($request->btnsave =='save')
      {
        $orderno = $request->key;
        $order_info = ServiceBooking::find($orderno);
        if( !isset($order_info))
        {
            return redirect("/admin/customer-care/orders/view-all")->with("err_msg", 'No order found to remove coupon.'  );  
        }
        else 
        {
          $order_costing =  DB::table("ba_shopping_basket")
          ->where("order_no", $orderno  ) 
          ->get();

          if( !isset($order_costing))
          {
            return redirect("admin/customer-care/orders/view-all")->with("err_msg", 'No order found to remove coupon.'  );  
          }

          $item_cost = 0.00;
          $packaging_cost = 0.00;
          foreach($order_costing as $item)
          {
            $item_cost  += $item->qty * $item->price;  
            $packaging_cost +=  $item->qty * $item->package_charge;
          }

          $order_info->coupon  = null ; 
          $order_info->discount = 0;
          $order_info->seller_payable =  $order_info->total_cost =  ($item_cost + $packaging_cost);
          $order_info->save(); 
          
          return redirect("/admin/customer-care/order/view-details/" .  $orderno )->with("err_msg", 'Coupon discount removed.'  ); 
        }
      } 
      
 
    }


 

 protected function mangeBusinessCategories(Request $request)
 {
    $biz_categories = DB::table("ba_business_category")->get();
    $metakeys=DB::table("ba_package_meta_data")->get();

    $data = array('results' => $biz_categories , 'breadcrumb_menu' => 'system_sub_menu','metakeys'=>$metakeys);
    return view("admin.business_categories_list")->with('data',$data); 
  }



  
  protected function saveBusinessCategories(Request $request)
 {
    try
    {

    if(isset($request->btnSave) &&  $request->btnSave == "update")
    {
      $bizcatname = $request->tb_bizcategory;
      $bizoldcatname = $request->tb_oldbizcategory;
      $maintype  = $request->db_maintype; 

      
      if( $bizcatname == "" || $maintype == ""  )
      {
        return redirect("admin/systems/manage-business-categories")->with("err_msg_right", "Important fields missing.");  
      }
      
      //new business category should be unique
      $bizcategory = BusinessCategory::where("name", $bizcatname )->first();
      if(isset($bizcategory))
      {
        return redirect("admin/systems/manage-business-categories")->with("err_msg_right", "A similar business category exists.");  
      }

      //existing business category should be found
      $bizcategory = BusinessCategory::where("name", $bizoldcatname )->first(); 
      if(!isset($bizcategory))
      {
        return redirect("admin/systems/manage-business-categories")->with("err_msg_right", "No business category exists.");  
      } 
  
      $bizcategory->name  = $bizcatname;
      $bizcategory->main_type = $maintype; 
      $save = $bizcategory->save();
      
      if(isset($save))
      {
        $msg  = "Business category is saved.";
      }
      else 
      {
        $msg  = "Business category could not be saved.";
      }
       
      return redirect("admin/systems/manage-business-categories")->with("err_msg_right",  $msg  );  
    }
    else 
      if(isset($request->btnSave) &&  $request->btnSave == "save")
    {
      $bizcatname = $request->tb_bizcategory; 
      $maintype  = $request->db_maintype; 
 
      if( $bizcatname == "" || $maintype == ""  )
      {
        return redirect("admin/systems/manage-business-categories")->with("err_msg_right", "Important fields missing.");  
      }
      
      //new business category should be unique
      $bizcategory = new BusinessCategory ;  
      $bizcategory->name  = $bizcatname;
      $bizcategory->main_type = $maintype; 
      $save = $bizcategory->save();
      
      if(isset($save))
      {
        $msg  = "Business category is saved.";
      }
      else 
      {
        $msg  = "Business category could not be saved.";
      }
       
      return redirect("admin/systems/manage-business-categories")->with("err_msg_right",  $msg  );  
    }
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return redirect("/admin/systems/manage-business-categories")->with("err_msg_right", "An error occurred while saving."); 
    }

  }

  protected function uploadBusinessCategoriesPhoto(Request $request)
  {
      $cdn_url = config('app.app_cdn'); 
      $cdn_path =  config('app.app_cdn_path');

      if ($request->catid=="") {
         return redirect('admin/systems/manage-business-categories');
      }

      $catid = $request->catid;

      $bizcategory = BusinessCategory::find($catid);

      if (!$bizcategory) {
         
         return redirect('admin/systems/manage-business-categories');
      } 

      // uploading photo
      $folder_name = 'category_'. $bizcategory->name; 
      $folder_path =  $cdn_path."/assets/upload/".'category_'.$bizcategory->name; 

      if ($request->hasFile('photo')) {

              if( !File::isDirectory($folder_path))
                  {
                    File::makeDirectory( $folder_path, 0777, true, true);
                  } 
        
                $file = $request->photo;
                $originalname = $file->getClientOriginalName(); 
                $extension = $file->extension( );
                $filename =  $bizcategory->name. time()  . ".{$extension}";
                $file->storeAs('upload',  $filename );
                $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
                $file_names[] =  $filename ;
                $folder_url  = $cdn_url.'/assets/upload/category_'. $bizcategory->name."/".$filename ;

                $bizcategory->icon_url = $folder_url;
                 
      }

      $save = $bizcategory->save();
      //uploading photo ends here

      if ($save) {
        return redirect('admin/systems/manage-business-categories')->with('err_msg','photo uploaded');
      }else{
        return redirect('admin/systems/manage-business-categories')->with('err_msg','failed to upload');
      }


  }

  public function removeBusinessCategories(Request $request)
  {
    try
    {

    if(isset($request->btnDel))
    {
      $id = $request->key;
      
      if( $id == ""  )
      {
        return redirect("/admin/systems/manage-business-categories")->with("err_msg", "No business category selected to delete."); 
      }

      BusinessCategory::where("id", $id)->delete(); 
      $msg  = "Business category removed successfully.";
      return redirect("/admin/systems/manage-business-categories")->with("err_msg",  $msg ); 
    } 
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return redirect("/admin/systems/manage-business-categories")->with("err_msg", "An error occurred while deleting."); 
    }

  }

  


    protected function viewAllProducts(Request $request)
    {
        $businessCategory= DB::table("ba_business_category")
        ->get();

        if( isset($request->btnSearch) )
      {


        $key = $request->search_key  ;
        $key1 = $request->search_filter  ;

        if ($key1!="") 
        {
            $products= DB::table("ba_products")
            ->where("pr_name","like", "%" . $key1 . "%")
            ->orWhere("pr_code","like", "%" . $key1 . "%")
            ->orderBy('category')
            ->paginate(20);
        }
        else
        {
          if ($key=="all") 
          {
            $products= DB::table("ba_products")
            ->orderBy('category')
            ->paginate(20);
          }
          else
          {
             $productCategory = DB::table("ba_product_category")
            ->where("business_category",$key)
            ->get();

            $category  = array();

            foreach ($productCategory as $item) 
            {
              
              $category[] = $item->category_name;
            }

            $products= DB::table("ba_products")
            ->whereIn("category",$category)
            ->orderBy('category')
            ->paginate(20);
          }
        }
        }
        else
        {
          $products= DB::table("ba_products")
        ->orderBy('category')
        ->paginate(20);
        }

 

    $data = array('products' => $products, 'businessCategory' => $businessCategory);

         return view("admin.products.view_all_products")->with('data',$data);
    
  }


  public function saveAsFeatureProduct(Request $request) 
     {
      if( isset($request->btnSave) && $request->btnSave =="save")
      {  
      
       $pid  = $request->pr_id;

       $products =  ProductModel::find($pid);

       if (!$products) 
       {

         return back()->with("err_msg", "No product record found!." ); 
       }

       $promo_product = ProductPromotion::where('pr_code', $products->pr_code)
       ->first();

      
        if( !isset($promo_product))
        {
          $promo_product = new ProductPromotion; 
        } 
        
        $promo_product->pr_code  = $products->pr_code; 
        $promo_product->todays_deal  = "no"; 
        $promo_product->save();


        return back()->with("err_msg", "Product saved under featured list." );

    }
    else
    {
      return back()->with("err_msg", "Product could not be saved under featured list!." );

    }


   }



   protected function viewFeaturedProducts(Request $request)
    {
        if( isset($request->btn_search) )
      {

        $key = $request->search_key  ;
          if ($key=="all") 
          {
            $promotionProducts = DB::table("ba_product_promotions") 
            ->orderBy("id","desc")
            ->paginate(20);  
          }
          else
          {
            $promotionProducts = DB::table("ba_product_promotions") 
            ->where("ba_product_promotions.todays_deal",$key)
            ->orderBy("id","desc")
            ->paginate(20);  
          }
       
        }
        else
        {
          $promotionProducts = DB::table("ba_product_promotions") 
          ->orderBy("id","desc")
            ->paginate(20);  
        } 
           
          $prsubids=array(); 
          foreach($promotionProducts as $item)
          {
            $prsubids[] = $item->prsubid;
          }
          $prsubids[] =0; 
          $all_products = DB::table("ba_product_variant")
          ->join("ba_products","ba_products.id","=","ba_product_variant.prid")
          ->select("ba_products.id", "ba_products.bin"  , "ba_products.pr_name", "ba_products.category"  ,"ba_product_variant.*")
          ->whereIn("ba_product_variant.prsubid",  $prsubids )
          ->get();

          $bins = array();
          foreach($all_products as $item)
          {
            $bins[] = $item->bin; 
          }
          $bins[] = 0;  
           $businesses = DB::table("ba_business")  
          ->whereIn("id", $bins )
          ->get();


        $data = array('promotionProducts' => $promotionProducts, 'businesses' =>$businesses, 'all_products' =>$all_products);
        return view("admin.products.view_promotion_products")->with('data',$data);
    
  }


  public function updateTodaysDeal(Request $request) 
  {

      if( isset($request->btnSave) && $request->btnSave =="save")
      {
        $id  = $request->promo_id;

        $promo_lists = ProductPromotion::find($id);

        if (!isset($promo_lists)) 
        {
          return redirect("/admin/customer-care/business/products/view-featured-products")->with("err_msg", "No record found!." ); 
        }
        
        $promo_lists->todays_deal = "yes";
        $promo_lists->save();
        

        return redirect("/admin/customer-care/business/products/view-featured-products")->with("err_msg", "Product successfully added todays deal!." ); 
       }
       else if( isset($request->btnUpdate) && $request->btnUpdate =="update")
      {
        $id  = $request->promo_id;

        $promo_lists = ProductPromotion::find($id);

        if (!isset($promo_lists)) 
        {
          return redirect("admin/customer-care/business/products/view-featured-products")->with("err_msg", "No record found!." ); 
        }
        
        $promo_lists->todays_deal = "no";
        $promo_lists->save();
        

        return redirect("admin/customer-care/business/products/view-featured-products")->with("err_msg", "Product successfully off todays deal!." ); 
       }
       else
       {
         return redirect("admin/customer-care/business/products/view-featured-products")->with("err_msg", "Todays deal add failed.Please consult admin!" );
       }

   }


   public function deleteFeaturedProduct(Request $request) 
    {

      if( isset($request->btnDelete) && $request->btnDelete =="delete")
      {
        $id  = $request->promo_id;
        ProductPromotion::find($id)
        ->delete();


        return redirect("/admin/customer-care/business/products/view-featured-products")->with("err_msg", "Product successfully deleted from featured list!." ); 
       }
       else
       {
         return redirect("/admin/customer-care/business/products/view-featured-products")->with("err_msg", "Product deleted from featured list failed. Please consult admin!" );
       }

   }
    
    
    public function activeUsersActivityHistory(Request $request)
    {

      
      $today = date('Y-m-d');
      $activeUsers =  DB::table("ba_profile") 
      ->join("ba_active_user_log", "ba_active_user_log.member_id", "=", "ba_profile.id")
      ->whereDate("ba_active_user_log.log_date",  $today ) 
      ->select(  "ba_profile.*", DB::Raw("count(*) as openCount"), DB::Raw("'unknown' category") )
      ->groupBy("ba_profile.id")
      ->paginate(50);
    
        $mids = array();

        foreach($activeUsers as $item)
        {
          $mids[]  = $item->id;
        }

        $mids[] =0;
        
      $visitlogs =  DB::table("ba_active_user_log")  
      ->whereIn("member_id" , $mids ) 
      ->whereDate("ba_active_user_log.log_date",  $today ) 
      ->select( "member_id", "category" ) 
      ->get(); 


      $data = array( 'title'=>'Analytics of Active Users' ,'results' => $activeUsers , 'visitlogs' => $visitlogs);
      return view("admin.user_analytics")->with('data' , $data );
        
    }




  protected function newPickAndDropRequest(Request $request)
  { 

    if(   $request->cname == "" ||  $request->cphone  == ""  ||  $request->address   == "" ||   
          $request->sfee   == "" ||  $request->total   == "" ||  $request->servicedate    == "" || $request->bin     == "" )
    {

      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "Important fields are missing!");
    }
      
     
     $business_info = Business::find(  $request->bin   );
     if( !isset($business_info) )
     {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching business found.'  );
     }  
  
     $address  =  $request->address;
     $landmark  =  $request->landmark; 
     $latitude   =  $request->latitude;
     $longitude  =  $request->longitude;
 
    //save sequencer  
    $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G'); 
    $rp_1 = mt_rand(0, 9);
    $rp_2 = mt_rand(0, 9); 
    $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;  
    $sequencer = new OrderSequencerModel;
    $sequencer->type = "pnd";
    $sequencer->tracker_session =  $tracker;
    $save = $sequencer->save();
    
    if( !isset($save) )
    { 
       return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Failed to generate order no sequence.'  );
    }
 

     $new_order = new PickAndDropRequestModel ; 
     $new_order->id = $sequencer->id ; 
     $new_order->request_by =  $request->bin ; 
     $new_order->request_from = "business";    
     $new_order->otp  =   mt_rand(222222, 999999); 
     
     
     $new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  : null);
     $new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : null);
     $new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : null);
     $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);
     


     $new_order->address = $address;
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ; 
   

     $new_order->drop_name =  preg_replace('/[^A-Za-z0-9\-]/', '', $request->cname);  
     $new_order->drop_phone =   preg_replace('/[^A-Za-z0-9\-]/', '',  $request->cphone  );  
     $new_order->drop_address =  $address;
     $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->drop_latitude =   0.00 ;
     $new_order->drop_longitude =  0.00 ; 

     $new_order->fullname  = $request->cname; //obsolete
     $new_order->phone  = $request->cphone ; //obsolete

 


     $new_order->book_status = "new";
     $new_order->service_fee = $new_order->requested_fee =  $request->sfee ;  
     $new_order->packaging_cost = ($request->packcost != "" ? $request->packcost : 0.00); 
     $new_order->total_amount = $request->total ; 
     $new_order->book_date =  date('Y-m-d H:i:s');   
     $new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));

     $new_order->pickup_details =  ($request->remarks != "" ? preg_replace('/[^A-Za-z0-9\-]/', ' ',  $request->remarks )   : null);  
     $new_order->source =  ( $request->source != "" ? $request->source : "booktou" );  

     if($request->session()->has('__user_id_' ) ) 
     {
        $new_order->staff_user_id  =  $request->session()->get('__user_id_' );
     }
     $new_order->tracking_session = $tracker;   
     $save=$new_order->save(); 

     if($save)
     {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'New pick-and-drop order placed!'  ); 

     }
     else
     {
        
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Your pick-and-drop order could not be placed. Please retry'  );
     } 
  }



  //making order from receipt
  protected function convertReceiptToPickAndDropRequest(Request $request)
  {

      if(   $request->cname == "" ||  $request->cphone  == ""  ||  $request->address   == "" ||   
            $request->sfee   == "" ||  $request->total   == "" ||  $request->servicedate    == "" || $request->erono  == "" )
      {

        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "Important fields are missing!");
      }

      $new_order = PickAndDropRequestModel::find(   $request->erono  );
      if( !isset($new_order) )
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching receipt found.'  );
      }  
  

     $address  =  $request->address;
     $landmark  =  $request->landmark; 
     $latitude   =  $request->latitude;
     $longitude  =  $request->longitude;  

     $new_order->otp  =  mt_rand(222222, 999999); 
     $new_order->fullname  = $request->cname;
     $new_order->phone  = $request->cphone ; 
     $new_order->address = $address;
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ;


  
     $new_order->request_by =  $request->ebin ;    
     $new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  : null);
     $new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : null);
     $new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : null);
     $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);
      
     $new_order->address = $address;
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ; 
   

     $new_order->drop_name = $request->cname; 
     $new_order->drop_phone =  $request->cphone ; 
     $new_order->drop_address =  $address;
     $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->drop_latitude =   0.00 ;
     $new_order->drop_longitude =  0.00 ; 


     //////

     $new_order->service_fee = $new_order->requested_fee =  $request->sfee ;  
     $new_order->total_amount = $request->total  ;    
     $new_order->packaging_cost = ($request->packcost != "" ? $request->packcost : 0.00); 
     $new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));
     $new_order->pickup_details =  ($request->remarks != "" ? $request->remarks : null);  

     $new_order->pay_mode =   $request->paymode ;  
     $save=$new_order->save(); 

     if($save)
     {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Receipt converted to pick-and-drop order!'  );  
     }
     else
     {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Pick-and-drop order could not be saved. Please retry'  );
     } 
  }




  //making order from receipt
  protected function updateAssistOrder(Request $request)
  {

      if(   $request->cname == "" ||  $request->cphone  == ""  ||  $request->address   == "" ||   
            $request->sfee   == "" ||  $request->total   == "" ||  $request->servicedate    == "" || $request->erono  == "" )
      {

        return redirect("/admin/orders/assists/view-all")->with("err_msg", "Important fields are missing!");
      }

      $new_order = PickAndDropRequestModel::find(   $request->erono  );
      if( !isset($new_order) )
      {
        return redirect("/admin/orders/assists/view-all")->with("err_msg",  'No matching receipt found.'  );
      } 

     $address  =  $request->address;
     $landmark  =  $request->landmark; 
     $latitude   =  $request->latitude;
     $longitude  =  $request->longitude;   
     $new_order->otp  =  mt_rand(222222, 999999);  
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ; 
  
     $new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  : null);
     $new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : null);
     $new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : null);
     $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);
       
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ;  
     $new_order->drop_name = $request->cname; 
     $new_order->drop_phone =  $request->cphone ; 
     $new_order->drop_address =  $address;
     $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->drop_latitude =   0.00 ;
     $new_order->drop_longitude =  0.00 ; 


     //////

     $new_order->service_fee = $new_order->requested_fee =  $request->sfee ;  
     $new_order->total_amount = $request->total  ;   
     $new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));
     $new_order->pickup_details =  ($request->remarks != "" ? $request->remarks : null);  

     $new_order->pay_mode =   $request->paymode ;  
     $new_order->payment_target =  'booktou';  
     $save=$new_order->save(); 

     if($save)
     {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Assist order updated!'  );  
     }
     else
     {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Assist order updated!'  );
     } 
  }


    protected function viewPickAndDropOrders(Request $request)
    {

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)); 
        $todayDate = date('Y-m-d');
      }
      else  
      { 
        $todayDate = date('Y-m-d'); 
        $orderDate = date('Y-m-d' , strtotime("15 days ago"))  ;   
      }
         

      if( isset($request->btn_search) )
      {

        //change from here  
        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );

        $bin = $request->filter_business;
        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled',
                'order_packed','pickup_did_not_come','in_route', 'package_picked_up','delivery_scheduled' ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
          $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner','order_packed','pickup_did_not_come', 
            'delivered', 'cancelled',
            'in_route', 'package_picked_up','delivery_scheduled' ) ; 
        }
 
        if($bin == -1)
        {
           
          $day_bookings  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "business" )
        ->where("ba_business.frno",  0 ) 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ") 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ")

 
        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        )  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->get();
 
        }
        else    
        {


        $day_bookings  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status) 
        ->where("ba_business.id",$bin) 
        ->where("ba_pick_and_drop_order.request_from", "business" )  

        ->where("ba_business.frno",  0 )  
        
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ") 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ")


        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        ) 
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->get();

 
  
        } 
 

        //changes ends here for Sanatomba 
      }
      else 
      {
        

        $request->session()->remove('_last_search_date' );
        $status = array( 'new','confirmed', 'cancel_by_client', 
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
        'package_picked_up','delivery_scheduled' ) ;  

        $day_bookings  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$todayDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "business" ) 
        ->where("ba_business.frno",  0 ) 
       ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ")  
       ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
       ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ") 
        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        )  
        ->orderBy("ba_pick_and_drop_order.id", "desc") 
        ->get();
 
       
      } 
      //finding free agents  
      $today = date('Y-m-d'); 

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 

          $query->select("agent_id")
          ->from("ba_agent_attendance") 
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();

  


      $delivery_orders = DB::table("ba_delivery_order")
      ->whereDate("accepted_date",  $orderDate )
      ->where("completed_on", null )
      ->where("order_type", 'pnd' )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();

      $free_agents = array();

      //Fetching assigned orders
      $delivery_orders_list = DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
      ->select("ba_delivery_order.*", "ba_delivery_order.accepted_date", 
        "ba_delivery_order.id as aid", "ba_profile.fullname")
      ->whereDate("accepted_date", ">=", $orderDate) 
      ->get(); 
 
   
      foreach ($all_agents as $item)
      {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id  )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 80 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           } 


          //image compression
          if($item->profile_photo != null )
          {
            
            $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

            if(file_exists(  $source ) &&  !file_exists( $destination ))
            {
              $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 
 

      $requests_to_process  = DB::table("ba_order_process_request")  
      ->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
      ->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
      ->whereDate("log_date", $today ) 
      ->get(); 

  

       $oids = $customerphones = $merchants = array();
      foreach($day_bookings as $item)
      {
        $merchants[] = $item->bin;
        $customerphones[] = $item->phone; 
        $oids[] = $item->id;
      }
      $merchants[] = $oids[] =  0;

      $merchants = array_unique($merchants);

      $businesses  = DB::table("ba_business") 
      ->whereIn("id", $merchants ) 
      ->get(); 

  
      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->get(); 
      
      
      $all_staffs = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname",  "ba_users.id as user_id" ) 
      ->where("ba_users.category", ">", "100")
      ->where("ba_users.status", "<>" , "100")  
      ->get();
       

      $banned_list = DB::table("ba_ban_list")
      ->whereIn("phone",  $customerphones) 
      ->get();

      $remarks = DB::table("ba_order_remarks")   
      ->whereIn("order_no",  $oids)
      ->orderBy("id", "asc")   
      ->get();

 
       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  'staffs' =>$all_staffs,
        'delivery_orders_list' => $delivery_orders_list, 
        'results' => $day_bookings,  
        'requests_to_process' =>$requests_to_process,
        'all_business' => $all_business,
        'todays_business' => $businesses,
        'last_keyword' => $last_keyword,
        'last_keyword_date' =>  $last_keyword_date , 
        'banned_list' =>$banned_list,
        'remarks' => $remarks ,
        'refresh' => 'true',
        'breadcrumb_menu' => 'pnd_sub_menu'
      );
       return view("admin.orders.pick_and_drop_orders_table")->with('data',$data);
    }


    //View completed pick and drop
    protected function viewPickAndDropCompletedOrders(Request $request)
    {

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date));  
        $today  = date('Y-m-d', strtotime($request->filter_date)); 
      }
      else 
      {
        $today = date('Y-m-d'); 
        $orderDate = date('Y-m-d'); 
      }


      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );


        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
          $status =  array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
        }


        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone" ) 
        ->get();


      }
      else 
      {

        $request->session()->remove('_last_search_date' );
        $status = array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
 
        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone" ) 
        ->get();

      }  

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->get();

      $delivery_orders = DB::table("ba_delivery_order")
      ->whereRaw(" date(accepted_date) = '$today'" )
      ->where("completed_on", null )
      ->where("order_type", 'pnd' )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();

      $free_agents = array();

      //Fetching assigned orders
      $delivery_orders_list = DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
      ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
      ->whereRaw(" date(accepted_date) ='$today'" ) 
      ->get(); 
 
   
      foreach ($all_agents as $item)
      {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           } 


          //image compression
          if($item->profile_photo != null )
          {
            
            $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

            if(file_exists(  $source ) &&  !file_exists( $destination ))
            {
              $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 

      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->get(); 
 
 
       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  
        'delivery_orders_list' => $delivery_orders_list, 
        'results' => $day_bookings,  
        'all_business' =>$all_business,
        'last_keyword' => $last_keyword, 'last_keyword_date' =>  $last_keyword_date , 
        'refresh' => 'true',
        'breadcrumb_menu' => 'pnd_old_sub_menu'
      );
       return view("admin.orders.pick_and_drop_old_orders_table")->with('data',$data);
    }


    protected function viewAllAssistOrders(Request $request)
    {

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)); 
        $todayDate = date('Y-m-d');
      }
      else  
      { 
        $todayDate = date('Y-m-d'); 
        $orderDate = date('Y-m-d' , strtotime("15 days ago"))  ;   
      }
         

      if( isset($request->btn_search) )
      {

        //change from here  
        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );

        $bin = $request->filter_business;
        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled',
                'order_packed','pickup_did_not_come','in_route', 'package_picked_up','delivery_scheduled' ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
          $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner','order_packed','pickup_did_not_come', 
            'delivered', 'cancelled',
            'in_route', 'package_picked_up','delivery_scheduled' ) ; 
        }
 
        if($bin == -1)
        {

          $day_bookings = DB::table("ba_pick_and_drop_order")
          ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
          ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
          ->whereIn("ba_pick_and_drop_order.book_status", $status)
          ->where("ba_pick_and_drop_order.request_from", "customer" )    
          ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ") 
          ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
          ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ") 
          ->select("ba_pick_and_drop_order.*",
            "ba_profile.id as mid", 
            DB::Raw("'0' bin") ,
            "ba_profile.fullname as name", 
            "ba_profile.phone as primaryPhone", 
            "ba_profile.locality" ,
            DB::Raw("'assist' orderType")) 
          ->orderBy("ba_pick_and_drop_order.id", "desc")
          ->get(); 

        }
        else    
        {
          $day_bookings = DB::table("ba_pick_and_drop_order")
          ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
          ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
          ->whereIn("ba_pick_and_drop_order.book_status", $status) 
          ->where("ba_pick_and_drop_order.request_from", "customer" )    
          ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ") 
          ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
          ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ") 
          ->select("ba_pick_and_drop_order.*",   
            "ba_profile.id as mid", 
            DB::Raw("'0' bin") ,
            "ba_profile.fullname as name", 
            "ba_profile.phone as primaryPhone", 
            "ba_profile.locality" ,
            DB::Raw("'assist' orderType")) 
          ->orderBy("ba_pick_and_drop_order.id", "desc")
          ->get();
  
        }
      }
      else 
      {
        

        $request->session()->remove('_last_search_date' );
        $status = array( 'new','confirmed', 'cancel_by_client', 
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
        'package_picked_up','delivery_scheduled' ) ;
        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$todayDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)
        ->where("ba_pick_and_drop_order.request_from", "customer" ) 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ") 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ")
        
        ->select("ba_pick_and_drop_order.*", 
          "ba_profile.id as mid", 
          DB::Raw("'0' bin") ,
          "ba_profile.fullname as name", 
          "ba_profile.phone as primaryPhone", 
          "ba_profile.locality" ,
          DB::Raw("'assist' orderType") ) 
        ->orderBy("ba_pick_and_drop_order.id", "desc") 
        ->get();  
      } 

      //finding free agents  
      $today = date('Y-m-d');  
      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0)  
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();
 

      $delivery_orders = DB::table("ba_delivery_order")
      ->whereDate("accepted_date",  $orderDate )
      ->where("completed_on", null )
      ->where("order_type", 'pnd' )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();

      $free_agents = array();

      //Fetching assigned orders
      $delivery_orders_list = DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
      ->select("ba_delivery_order.*", "ba_delivery_order.accepted_date", 
        "ba_delivery_order.id as aid", "ba_profile.fullname")
      ->whereDate("accepted_date", ">=", $orderDate) 
      ->get(); 
 
   
      foreach ($all_agents as $item)
      {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id  )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 80 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           } 


          //image compression
          if($item->profile_photo != null )
          {
            
            $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

            if(file_exists(  $source ) &&  !file_exists( $destination ))
            {
              $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 
 

      $requests_to_process  = DB::table("ba_order_process_request")  
      ->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
      ->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
      ->whereDate("log_date", $today ) 
      ->get(); 

  

       $oids = $customerphones = $merchants = array();
      foreach($day_bookings as $item)
      {
        $merchants[] = $item->bin;
        $customerphones[] = $item->phone; 
        $oids[] = $item->id;
      }
      $merchants[] = $oids[] =  0;

      $merchants = array_unique($merchants);

      $businesses  = DB::table("ba_business") 
      ->whereIn("id", $merchants ) 
      ->get(); 

  
      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->get(); 
      
      
      $all_staffs = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname",  "ba_users.id as user_id" ) 
      ->where("ba_users.category", ">", "100")
      ->where("ba_users.status", "<>" , "100")  
      ->get();
       

      $banned_list = DB::table("ba_ban_list")
      ->whereIn("phone",  $customerphones) 
      ->get();

      $remarks = DB::table("ba_order_remarks")   
      ->whereIn("order_no",  $oids)
      ->orderBy("id", "asc")   
      ->get();

 
       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  'staffs' =>$all_staffs,
        'delivery_orders_list' => $delivery_orders_list, 
        'results' => $day_bookings,  
        'requests_to_process' =>$requests_to_process,
        'all_business' => $all_business,
        'todays_business' => $businesses,
        'last_keyword' => $last_keyword,
        'last_keyword_date' =>  $last_keyword_date , 
        'banned_list' =>$banned_list,
        'remarks' => $remarks ,
        'refresh' => 'true',
        'breadcrumb_menu' => 'assist_sub_menu'
      );
       return view("admin.orders.assist_orders_table")->with('data',$data);
    }




    //View completed assist 
    protected function viewAllAssistOrdersCompleted(Request $request)
    {

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date));  
        $today  = date('Y-m-d', strtotime($request->filter_date)); 
      }
      else 
      {
        $today = date('Y-m-d'); 
        $orderDate = date('Y-m-d'); 
      }


      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );


        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
          $status =  array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
        }


        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "customer" ) 
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
        "ba_profile.id as uid", "ba_profile.fullname", "ba_profile.phone" , "ba_profile.locality", 
         "ba_profile.landmark", "ba_profile.city", "ba_profile.state", "ba_profile.pin_code") 
        ->get();


      }
      else 
      {

        $request->session()->remove('_last_search_date' );
        $status = array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
 
        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "customer" ) 
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_profile.id as uid", "ba_profile.fullname", "ba_profile.phone" , "ba_profile.locality", 
         "ba_profile.landmark", "ba_profile.city", "ba_profile.state", "ba_profile.pin_code") 
        ->get();

      }  

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->get();

      $delivery_orders = DB::table("ba_delivery_order")
      ->whereRaw(" date(accepted_date) = '$today'" )
      ->where("completed_on", null )
      ->where("order_type", 'pnd' )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();

      $free_agents = array();

      //Fetching assigned orders
      $delivery_orders_list = DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
      ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
      ->whereRaw(" date(accepted_date) ='$today'" ) 
      ->get(); 
 
   
      foreach ($all_agents as $item)
      {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           } 


          //image compression
          if($item->profile_photo != null )
          {
            
            $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

            if(file_exists(  $source ) &&  !file_exists( $destination ))
            {
              $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 

      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->get(); 
 
 
       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  
        'delivery_orders_list' => $delivery_orders_list, 
        'results' => $day_bookings,  
        'all_business' =>$all_business,
        'last_keyword' => $last_keyword, 'last_keyword_date' =>  $last_keyword_date , 
        'refresh' => 'true',
        'breadcrumb_menu' => 'assist_sub_menu'
      );
       return view("admin.orders.assist_old_orders_table")->with('data',$data);
    }



    protected function editConfiguration(Request $request)
   {
      $configs = DB::table("ba_global_settings")->get();

      $data = array('configs' => $configs , 'breadcrumb_menu' => 'system_sub_menu');
      return view("admin.settings.config")->with('data',$data); 
    }
 

 protected function saveConfiguration(Request $request)
   {

    if($request->save) //save individual
    {
      $selected_key = $request->save;
      $ckeys = $request->config_name;
      
      $i=0;
      foreach($ckeys as $key )
      {
        if( $selected_key == $key)
        {
          break;
        }
        $i++;
      }

      $cvalues = $request->config_value; 
      $settings = GlobalSettingsModel::where("config_key", $selected_key )->first(); 
      $settings->config_value = $cvalues[$i];
      $settings->save();  
    }
  
    return redirect('/admin/systems/edit-configuration')->with("err_msg", "Configuration updated!");


    }


   

 protected function viewCampaignAreas(Request $request)
 {
    $areas = DB::table("ba_marketing_campaign_areas")->get();

    $data = array('areas' => $areas  );
    return view("admin.marketing.campaign_areas")->with('data',$data); 
  }


  

  protected function viewCampaignAudiences(Request $request)
 {

    if( !isset($request->key) || $request->key == "")
    { 
       return redirect("/admin/marketing/campaign-area")->with("err_msg", "No campaign area selected!");
    }

    $area_name = $request->key;


    $audiences = DB::table("ba_profile")
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
    ->where("ba_users.category", 0)
    ->where(function ($query) use( $area_name ) {

               $query->where( 'ba_profile.locality', 'like',   "%$area_name%" )
               ->orWhere( 'ba_profile.city', 'like',   "%$area_name%" );
      })
    ->select("ba_profile.*", "ba_users.firebase_token", DB::Raw("'0' logCount") )

     ->paginate(30);

     $ids = array();
     $ids[] =0;
     foreach($audiences as $item)
     { 
       $ids[] = $item->id;
     }


    $allLogs = DB::table("ba_marketing_call_log") 
    ->whereIn("member_id", $ids) 
    ->select("member_id", DB::Raw("count(*) as logCount")) 
    ->groupBy('member_id')
    ->get();
  

    foreach($audiences as $item)
     { 
        foreach($allLogs as $log)
       { 
          if($log->member_id == $item->id)
          { 
            $item->logCount =  $log->logCount;
            break;
          }
       }
     }

 

    $data = array('results' => $audiences , 'title' => 'Campaign Target Audience' );
    return view("admin.marketing.customer_audiences")->with('data',$data); 
  }




  protected function manageAppBanners(Request $request)
  {
    $slides = DB::table("ba_slides")->get(); 

    $businesses = DB::table("ba_business")
    ->where("is_block", "no") 
    ->get(); 

    $data = array('slides' => $slides , 'businesses' => $businesses );
    return view("admin.cms.app_manage_banners")->with('data',$data); 
  }

  protected function uploadAppBanner(Request $request)
  {
    if($request->caption == ""   )
    {
      return redirect("/admin/systems/manage-app-banner")->with("err_msg", 'Missing data to perform action.'  ); 
    }
      
      //saving image 
      if( $request->file('photo') != "")
      {
        $imageName = "app_slider_" . time() . '.' .$request->photo->getClientOriginalExtension();
        $request->photo->move( config('app.app_cdn_path')  . '/assets/app/slides/' , $imageName);  
        $url =   '/assets/app/slides/' . $imageName ; 
      }
      else 
      {
        $url =  "/assets/images/home_banner.jpg" ; //"https://booktou.in/assets/images/home_banner.jpg";
      }
      
      //incrementing
      DB::table('ba_slides')
      ->increment('sequence', 1);


      $slider  = new SliderModel; 
      $slider->caption=  $request->caption ; 
      $slider->path = $url ; 
      $slider->bin = $request->bin ; 
      $slider->status = 1;  
      $save = $slider->save();

      return redirect("/admin/systems/manage-app-banner")->with("err_msg", 'App slider image saved.'  ); 
  }




 protected function disableAppBanner(Request $request)
  {
    
      if( $request->key == ""  || $request->status == "" )
      {
        return redirect("/admin/systems/manage-app-banner")->with("err_msg", 'Missing data to perform action.'  ); 
      }

      if($request->status == "true")
      {
        $status = 1;
      }
      else 
      {
        $status = 0;
      }
      
      //incrementing
      DB::table('ba_slides')
      ->where('id', $request->key)
      ->update(["status" => 0, 'status' => $status ]); 

      return redirect("/admin/systems/manage-app-banner")->with("err_msg", 'Slider item disabled'  ); 
  }


 

  protected function deleteAppBanner(Request $request)
  {
    
      if( $request->key == ""   )
      {
        return redirect("/admin/systems/manage-app-banner")->with("err_msg", 'Missing data to perform action.'  ); 
      }
 
      //incrementing
      DB::table('ba_slides')
      ->where('id', $request->key)
      ->delete(); 

      return redirect("/admin/systems/manage-app-banner")->with("err_msg", 'Slider item deleted.'  ); 
  }


  protected function removeAgentFromAssist(Request $request)
  {
    
      if( $request->key == ""   )
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", 'Missing data to perform action.'  ); 
      }

      $orderinfo = DeliveryOrderModel::where('order_no',$request->key)->first();

 

      $delete =  DB::table('ba_delivery_order')
      ->where('order_no', $request->key)
      ->where('order_type',  "assist" )
      ->delete();


      if ($delete) {
          DB::table('ba_pick_and_drop_order')
          ->where('id', $request->key)
          ->update(["book_status" =>  'new' ]); 
      } 


      return redirect("/admin/customer-care/assist-order/view-details/" .  $request->key )->with("err_msg", 'Agent freed from assist order. Please reassign another free agent!'  ); 
  }

  
  protected function removeAgentFromPnDOrder(Request $request)
  {
    
      if( $request->key == ""   )
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", 'Missing data to perform action.'  ); 
      }
 
      //incrementing
      // DB::table('ba_delivery_order')
      // ->where('order_no', $request->key)
      // ->where('order_type',  "pick and drop")
      // ->delete();


      //  DB::table('ba_pick_and_drop_order')
      // ->where('id', $request->key)
      // ->update(["book_status" => 'new'  ]);  

      $orderinfo = DeliveryOrderModel::where('order_no',$request->key)->first();

 

      $delete =  DB::table('ba_delivery_order')
      ->where('order_no', $request->key)
      ->where('order_type', "pnd")
      ->delete();


      if ($delete) {
          DB::table('ba_pick_and_drop_order')
          ->where('id', $request->key)
          ->update(["book_status" => 'new'  ]); 
      } 


      return redirect("/admin/customer-care/pnd-order/view-details/" .  $request->key )->with("err_msg", 'Agent freed from PnD Order. Please reassign another free agent!'  ); 
  }




  protected function removeAgentFromNormalOrder(Request $request)
  {
    
      if( $request->key == ""   )
      {
        return redirect("/admin/customer-care/orders/view-all")->with("err_msg", 'Missing data to perform action.'  ); 
      }
 
      //incrementing
      DB::table('ba_delivery_order')
      ->where('order_no', $request->key)
      ->where('order_type',  "client order")
      ->delete();

      //Updating log
      DB::table('ba_order_status_log')
     ->where('order_no', $request->key)
     ->decrement('seq_no', 1);

     //removing one entry
     DB::table('ba_order_status_log')
     ->where('order_no', $request->key)
     ->where("order_status", "delivery_scheduled")
     ->delete();

     /*
     $updateOrderStatus = new OrderStatusLogModel;
     $updateOrderStatus->order_no =  $request->key;
     $updateOrderStatus->order_status= $order_info->book_status;
     $updateOrderStatus->log_date=  date('Y-m-d H:i:s' ); 
     $updateOrderStatus->save();
     */ 

       DB::table('ba_service_booking')
      ->where('id', $request->key)
      ->update(["book_status" => 'confirmed'  ]);   

      return redirect("/admin/customer-care/orders/view-all")->with("err_msg", 'Agent freed from Order. Please reassign another free agent!'  ); 
  }




  public function viewBestPerformers(Request $request)
  {
      
      $month = $request->month == "" ?  date('m') : $request->month  ;
      $year = $request->year == "" ?  date('Y')  : $request->year  ;  

 
      
    $sales = DB::table("ba_service_booking")  
     ->whereRaw("month(service_date)= '$month'")  
     ->whereRaw("year(service_date)= '$year'")  
      ->where("book_status", "delivered")
     ->select( "bin",  DB::Raw("count(*) as saleCount"), DB::Raw("sum(seller_payable) as totalSales") )
     ->groupBy("bin")
     ->orderBy("totalSales", "desc")
     ->get();

     $bins = array();
     foreach($sales as $item)
     {
      $bins[] = $item->bin;
     }
      $bins[]=0;


    $businesses= DB::table("ba_business")   
    ->whereIn("id" ,  $bins) 
    ->get( );

    $data = array('results' => $businesses, 'sales' => $sales  ); 
          //return json_encode( $data);
       return view("admin.business.best_performers")->with('data',$data);
    }

    public function viewLeastPerformers(Request $request)
  {
    $businessData= DB::table("ba_business") 
    ->join("ba_premium_business", "ba_premium_business.bin", "=", "ba_business.id")
    ->where("category",'VARIETY STORE') 
      ->select("ba_business.id","name","tags","shop_number","locality","landmark","city","state","rating","pin","is_open","category", 
        DB::Raw("'YES' buttonStatus"))
      ->get();

     

       $data = array('businesses' => $businessData  ); 
          //return json_encode( $data);
       return view("admin.store.premium_businesses")->with('data',$data);
    }




  public function viewPndBestPerformers(Request $request)
  {

    $month = $request->month == "" ?  date('m') : $request->month  ;
    $year = $request->year == "" ?  date('Y')  : $request->year  ;  
    $sales = DB::table("ba_pick_and_drop_order")  
    ->whereRaw("month(service_date)= '$month'")  
    ->whereRaw("year(service_date)= '$year'")  
    ->where("book_status", "delivered")
    ->where("request_from", "business")
    ->select( "request_by as bin", DB::Raw("count(*) as saleCount"), DB::Raw("sum(total_amount-service_fee) as totalSales") )
    ->groupBy("request_by")
    ->orderBy("totalSales", "desc")
    ->get();

    
    $bins = array();
    foreach($sales as $item)
    {
      $bins[] = $item->bin;
    }
    $bins[]=0;

    $businesses= DB::table("ba_business")    
    ->whereIn("id" ,  $bins) 
    ->get();

    $data = array('results' => $businesses, 'sales' => $sales  ); 
          //return json_encode( $data);
       return view("admin.business.pnd_best_performers")->with('data',$data);
  }

 
  public function viewNorderOrderSalesBreakDown(Request $request) 
  {
      $month = date('m'); 
      $year = date('Y'); 
      $bin = $request->bin;


      if( $bin == "")
      {
        return redirect("/admin/customer-care/business/normal-orders/best-performers")->with("err_msg", "No business details specified!" );
      }


      $business = Business::find($bin);

      $sales = DB::table("ba_service_booking")  
      ->whereRaw("month(service_date)= '$month'")  
      ->whereRaw("year(service_date)= '$year'")
      ->where("bin", $bin)
      ->get();

       $orderno = array(); 
       foreach($sales as $item)
       { 
          $orderno[] = $item->id ;
       }

      $orderno[] =0;

      $sales_analytics = DB::table("ba_shopping_basket")  
      ->whereIn("order_no",  $orderno )  
      ->select( "pr_code",  DB::Raw("sum(qty) as totalQty") , DB::Raw("sum(price) as totalSales") )
      ->groupBy( "pr_code")
      ->orderBy("totalQty", "desc") 
      ->get();


      $prcodes=array();

      foreach($sales_analytics as $item )
      {

        $prcodes[] = $item->pr_code;
      }
      $prcodes[]  = "na";

      $products= DB::table("ba_products")
      ->whereIn("pr_code", $prcodes ) 
      ->paginate(100);
 
      $data = array('business' => $business, 'sales' => $sales, 'sales_analytics' => $sales_analytics, 'products' => $products  );
      view("admin.business.sales_analytics")->with('data',$data); 
  }




  public function liveLocationAgents()
    {
      $allstaffs = DB::table("me_staff")
      ->orderBy("fname")
      ->get( );
      
      $active = array();
      $inactive = array();
      foreach($allstaffs as $item)
      {
        $active[] = $item;
      }
      $data = ['active' => $active, 'inactive' => $inactive]; 
      return view("map.index")->with('data', $data); 
    }
 
    


  public function monthlyEarningReport(Request $request)
  {
      $endmonth =12;
      $year = $request->year == "" ?  date('Y')  : $request->year  ;  
      $franchise =  0;

      $report_data = array( );

      for($i=1; $i <= $endmonth  ; $i++){

  
      $income = DB::table('ba_service_booking')    
      ->whereYear('service_date', $year ) 
      ->whereIn("book_status",  array('delivered', 'completed'))  
      ->where("route_to_frno",  $franchise) 
      ->groupBy("currentMonth") 
      ->select(  
        DB::Raw("month(service_date) as currentMonth"),  
        DB::Raw("sum(seller_payable)  as totalSale") , 
        DB::Raw("sum(packing_charge)   as  packagingCost")  ,
        DB::Raw("sum(delivery_charge)  as deliveryCommission")  )
      ->get();


      $pnd_income = DB::table('ba_pick_and_drop_order')
      ->whereYear('service_date', $year )
      ->whereIn("book_status",  array('delivered', 'completed')) 
        ->where("route_to_frno",  $franchise ) 
         ->groupBy("currentMonth")  
         ->select( 
          DB::Raw("month(service_date) as currentMonth"),
          DB::Raw("sum(total_amount) as totalSale") ,
          DB::Raw("sum(packaging_cost) as packagingCost"),
          DB::Raw("sum(service_fee) as deliveryCommission")  )
        ->get();
 

      

        $report_data[$i] = array(  'month' =>  date("F", mktime(0, 0, 0, $i, 10)), 'year' => $year , 
          'normalSale' => 0,  'normalOrderCommission' => 0 ,
          'pndSale' => 0,  'pndOrderCommission' => 0  );
 
        foreach($income as $item)
        { 
          if($item->currentMonth == $i)
           {
              $report_data[$i]["normalSale"] += $item->totalSale + $item->packagingCost;
              $report_data[$i]["normalOrderCommission"] += $item->deliveryCommission; 
              break;
           }
        }
  
        $monthlyPndSales =0;
        $monthlyPndCommission =0;
        foreach($pnd_income as $item)
        {
           if($item->currentMonth == $i) 
           {
             $monthlyPndSales+= $item->totalSale + $item->packagingCost;
             $monthlyPndCommission += $item->deliveryCommission; 
             break;
           } 
        }

        $report_data[$i]["pndSale"]  =  $monthlyPndSales;
        $report_data[$i]["pndOrderCommission"]  = $monthlyPndCommission;

      } 

      $data = array(   'year' =>  $year, 'report_data' => $report_data  );  
      return view("admin.reports.income_report")->with('data', $data); 
    }

 

  public function manageAttendance(Request $request)
  {
      $date =  date('Y-m-d');  

      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("franchise", "0" )
      ->where("category", "100" )
      ->where("ba_users.status", "<>", "100" )  
      ->select("ba_profile.*", "ba_users.category" )
      ->get(); 

      $active_staffs = DB::table("ba_staff_attendance")    
      ->whereDate("log_date",  $date )   
      ->get();


     $data = array(  'date' => $date , 'staffs' => $all_staffs , 'active_staffs' => $active_staffs );  
     return view("admin.staffs.staffs_attendance")->with('data', $data); 
    }

  
  protected function activeAttendance(Request $request)
  {
    if($request->filter_date=="")
    {
        $today = date('Y-m-d');
    }else{
        $today = date('Y-m-d',strtotime($request->filter_date));
    }

    $status = ['ready-for-task', 'ready-for-multiple-task'];

    $all_agents =  DB::table("ba_profile")   
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("agent_id")
          ->from("ba_agent_attendance")
          ->whereDate("log_date",   date('Y-m-d') ); 
        })
    ->where("ba_users.category", 100)
    ->where("ba_users.status", "<>",  100) 
    ->select("ba_profile.*")
    ->get();
    //use foreach to get Ids
    $agent_ids = array();
    $agent_ids[] = 0; //dummy filler
    foreach($all_agents as $agent)
    {
      $agent_ids[] = $agent->id;
    }
    
    $agents_status = DB::table("ba_agent_locations")
    ->whereRaw("date(updated_at) = '$today' " ) 
    ->whereIn("agent_id", $agent_ids )
    ->whereIn("agent_status",$status)
    ->select("agent_id", "agent_status")
    ->orderBy("seq_no", "asc") 
    ->get();

      $data = array('today'=>$today, 'agents' => $all_agents , 'agents_status' => $agents_status );  
     return view("admin.staffs.active_agents")->with('data', $data); 
    }
  public function saveAttendance(Request $request)
  {

    $date =  date('Y-m-d'); 
    
    $active_staff = StaffAttendanceModel::where("staff_id", $request->key )
    ->whereDate("log_date",  $date )    
    ->first();

    if( !isset($active_staff))
    {
      $active_staff = new StaffAttendanceModel; 
    }

    $active_staff->staff_id = $request->key;
    $active_staff->shift= $request->shift;
    $active_staff->staff_status= $request->status;
    $active_staff->log_date= $date; 
    $active_staff->start_log= date('Y-m-d H:i:s', strtotime($request->start));
    $active_staff->save(); 

    $lastlocation = $request->location;
    $agentlocation =  AgentCacheModel::where("staff_id", $request->key )->first();


    if( !isset($agentlocation))
    {
        $agentlocation =  new AgentCacheModel; 
    }

    $agentlocation->staff_id =$request->key;
    $agentlocation->location = $lastlocation ;
    $agentlocation->on_field = 'YES'; 
    $agentlocation->save() ;


     return redirect("/admin/staffs/manage-attendance")->with("err_msg", "Attendance log taken");
  }
 


  //block user account 
  public function blockUserAccount(Request $request)
  {
 
    
    $staff_info  = CustomerProfileModel::find(  $request->agid ) ;

    if( !isset($staff_info))
    {
       return redirect("admin/staffs/manage-attendance")->with("err_msg", "No matching staff found!");
    }

    $user_account  = User::where("profile_id",   $request->agid )->first() ;

    if( !isset($user_account))
    {
       return redirect("admin/staffs/manage-attendance")->with("err_msg", "No matching account found!");
    }

    
    $user_account->status =100;
    $user_account->save();
 

     return redirect("/admin/staffs/manage-attendance")->with("err_msg", "Account disabled!");
  }


    //View callback requests
    protected function viewCallbackRequests(Request $request)
    {

      if($request->filter_date == "")
      {
        $orderDate = date('Y-m-d');
      }
      else 
      {
        $orderDate = date('Y-m-d', strtotime( $request->filter_date )); 
      }

      
       
      if( isset($request->btn_search) )
      {
 

        $callbacks = DB::table("ba_callback_enquiry") 
        ->whereDate("enquiry_date",  $orderDate)  
        ->where("is_verified", "no") 
        ->get();

      }
      else 
      {
 
 
        $callbacks = DB::table("ba_callback_enquiry") 
        ->whereDate("enquiry_date",  $orderDate)  
        ->where("is_verified", "no") 
        ->where("body","<>", null ) 
        ->get();

      }  

      $data = array( 'title'=>'Manage callback requests' ,  
        'date' => $orderDate, 'all_requests' => $callbacks );

       return view("admin.orders.callback_requests_orders_card")->with('data',$data);
    }


 //View callback requests
    protected function viewQuotationRequests(Request $request)
    {

      $orderDate = date('Y-m-d');
      

      if( isset($request->btn_search) )
      {
        $callbacks = DB::table("ba_quote_request") 
        ->whereDate("request_date",  $orderDate)  
        ->where("is_processed", "no") 
        ->get();

      }
      else 
      {
        $callbacks = DB::table("ba_quote_request") 
        ->whereDate("request_date",  $orderDate)  
        ->where("is_processed", "no") 
        ->get(); 
      }  

      $data = array( 'title'=>'Manage rate quotation requests' ,   'date' => $orderDate, 'all_requests' => $callbacks );

       return view("admin.orders.quotation_requests_orders_card")->with('data',$data);
    }


    protected function normalOrderStatsLog(Request $request)
    {

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)) ; 
      }
      else 
      {
        $orderDate = date('Y-m-d' , strtotime("10 days ago"))  ; 
      }

      $status = array( 'new','confirmed', 'delivered', 'order_packed','delivery_scheduled','task_accepted',  'waiting_package','package_picked_up','in_route', 'delivered' ) ; 


      if( isset($request->btn_search) )
      {
 
        $request->session()->put('_last_search_date',  $request->filter_date ); 
      }
      else 
      {

        $request->session()->remove('_last_search_date' ); 
      }

      
      $day_bookings= DB::table("ba_service_booking")
      ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
      ->whereRaw("date(ba_service_booking.service_date) >= '$orderDate'")
      ->whereIn("ba_service_booking.book_status",$status)   
      ->select("ba_service_booking.*", "ba_profile.fullname", "ba_profile.phone" )
      ->orderBy("ba_service_booking.id", "desc")
      ->get();

 
      $bookid  = array();
      $all_bins = array();
      $book_by = array();
      foreach($day_bookings as $bitem )
      {
        $all_bins[] = $bitem->bin; 
        $book_by[] = $bitem->book_by;
        $bookid[] = $bitem->id;
      }
 
      $order_logs = DB::table("ba_order_status_log")
      ->whereIn("order_no", $bookid)    
      ->orderBy("order_no", "asc" )
      ->orderBy("seq_no", "desc" )
      ->get();



      $all_bins = array_filter($all_bins); 
      $all_businesses = DB::table("ba_business")
      ->whereIn("id", $all_bins)    
      ->get();


      $order_statuses = array(   'completed', 'order_packed' , 'in_route','delivered' , 'package_picked_up','delivery_scheduled' );

      $book_by = array_filter($book_by);

       $book_status = array( 'cancel_by_owner','pickup_did_not_come', 'cancel_by_client',  'canceled','order_packed' ) ; 
 
 
      $book_counts = DB::table("ba_service_booking")
      ->whereIn("book_by", $book_by)    
      ->whereNotIn("book_status", $book_status)  
      ->select("book_by", DB::raw('count(*) as totalOrders') )
      ->groupBy("book_by")
      ->get(); 
 
     
        //finding free agents   
        $all_agents = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo"  ) 
        ->where("ba_users.category", 100)
        ->where("ba_users.status", "<>" , "100") 
        ->where("ba_profile.fullname", "<>", "" )
        ->get();


        $delivery_orders = DB::table("ba_delivery_order")
        ->whereRaw(" date(accepted_date) >= '$orderDate'" )
        ->where("completed_on", null )
         ->where("order_type",  'client order'  )
        ->select("member_id", DB::Raw("count(*) as cnt") )
        ->groupBy('member_id')
        ->get();

        $free_agents = array();


        //Fetching assigned orders
        $delivery_orders_list = DB::table("ba_delivery_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
        ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
        ->whereRaw(" date(accepted_date) ='$orderDate'" )
        ->get(); 
 
   
        foreach ($all_agents as $item)
        {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           }



           //image compression
            if($item->profile_photo != null )
            {
              $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

              if(file_exists(  $source ) &&  !file_exists( $destination ))
              {
                  $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 



       $data = array( 'title'=>'Manage Orders' ,'results' => $day_bookings, 'date' => $orderDate, 'all_agents' => $all_agents , 
        'delivery_orders_list' => $delivery_orders_list, 'all_businesses' =>$all_businesses, 'book_counts' => $book_counts,
        'last_keyword' => $last_keyword, 'last_keyword_date' =>  $last_keyword_date , 'refresh' => 'true', 'order_logs' => $order_logs);

         return view("admin.orders.stats")->with('data',$data);
    }

 



  public function viewFrequentlyBrowsedProducts(Request $request)
  {
      
      $month = $request->month == "" ?  date('m') : $request->month  ;
      $year = $request->year == "" ?  date('Y')  : $request->year  ;

      $promotionProducts = DB::table("ba_tracker_product_visit")
      ->join("ba_products","ba_products.id","=","ba_tracker_product_visit.pid")
      ->whereMonth("visit_date",$month )
      ->whereYear("visit_date",$year )
      ->select("ba_products.*",  DB::Raw("count(*) as viewCount") )
      ->groupBy("ba_products.id" )
      ->orderBy("viewCount","desc")
      ->paginate(50); 
        

          $bins = array();
          foreach($promotionProducts as $item)
          {
            $bins[] = $item->bin;
          }
          $bins[] = 0;

          $businesses = DB::table("ba_business")  
          ->whereIn("id", $bins )
          ->get(); 
          $data = array(  'title'=>'Most Frequent Browsed Products' ,  'promotionProducts' => $promotionProducts, 'businesses' =>$businesses);
          return view("admin.products.view_frequently_browsed_products")->with('data',$data); 
    }


  public function mostViewedCustomers(Request $request)
  {

      $month = $request->month == "" ?  date('m') : $request->month  ;
      $year = $request->year == "" ?  date('Y')  : $request->year  ;
      $prid = $request->prid  ;


      $profiles = DB::table("ba_profile") 
      ->join("ba_users","ba_users.profile_id","=","ba_profile.id")
      ->whereIn("ba_profile.id",  function($query) use ($month, $year, $prid)
      {
        $query->select("member_id")
        ->distinct()
        ->from('ba_tracker_product_visit')
        ->where("pid", $prid)
        ->whereMonth("visit_date", $month )
        ->whereYear("visit_date", $year )
        ->select('member_id');  
      })
      ->select("ba_profile.*", "ba_users.firebase_token", DB::Raw("'0' viewCount"))
      ->paginate(50); 



      $memberid = array();
      foreach($profiles as $item)
      {
        $memberid[] = $item->id;
      }
      $memberid[]=0;  

      $view_logs = DB::table("ba_tracker_product_visit")
      ->where("pid", $prid)
      ->whereIn("member_id", $memberid)
      ->groupBy("member_id")
      ->select("member_id", DB::Raw("count(*) as viewCount"))
      ->orderBy("viewCount", "desc")
      ->get(); 



      $product = DB::table("ba_products")
      ->where("id", $prid) 
      ->first(); 



      $data = array( 'title'=>'Most Frequent Customers' , 'profiles' =>$profiles ,'view_logs' =>$view_logs , 'product' =>$product);
      return view("admin.products.frequent_viewers")->with('data',$data);  
    }


  public function merchantPageViews(Request $request)
  {
      $month = $request->month == "" ?  date('m') : $request->month  ;
      $year = $request->year == "" ?  date('Y')  : $request->year  ;
      $prid = $request->prid  ;


      $profiles = DB::table("ba_profile") 
      ->join("ba_users","ba_users.profile_id","=","ba_profile.id")
      ->whereIn("ba_profile.id",  function($query) use ($month, $year, $prid)
      {
        $query->select("member_id")
        ->distinct()
        ->from('ba_tracker_product_visit')
        ->where("pid", $prid)
        ->whereMonth("visit_date", $month )
        ->whereYear("visit_date", $year ) ;  
      })
      ->select("ba_profile.*", "ba_users.firebase_token", DB::Raw("'0' viewCount"))
      ->paginate(50); 



      $memberid = array();
      foreach($profiles as $item)
      {
        $memberid[] = $item->id;
      }
      $memberid[]=0;  

      $view_logs = DB::table("ba_tracker_product_visit")
      ->where("pid", $prid)
      ->whereIn("member_id", $memberid)
      ->groupBy("member_id")
      ->select("member_id", DB::Raw("count(*) as viewCount"))
      ->orderBy("viewCount", "desc")
      ->get(); 



      $product = DB::table("ba_products")
      ->where("id", $prid) 
      ->first(); 



      $data = array( 'title'=>'Most Frequent Customers' , 'profiles' =>$profiles ,'view_logs' =>$view_logs , 'product' =>$product);
      return view("admin.products.frequent_viewers")->with('data',$data);  
    }



  protected function addQrMenu(Request $request)
  {
    $bin =0;
    if($request->session()->has('selection_biz'))
    {
      $bin =  $request->session()->get('selection_biz'  );    
    }
    


    $slides = DB::table('ba_business_menu')
    ->where('bin', $bin )
    ->get( );

    $businesses = DB::table("ba_business")
    ->where("is_block", "no") 
    ->get(); 

    $data = array('slides' => $slides , 'businesses' => $businesses );
    return view("admin.cms.add_qr_menu_item")->with('data',$data); 
  }

  protected function updateWebMenuImage(Request $request)
  {
      if($request->bin == ""   )
      {
        return redirect("/admin/systems/add-qr-menu")->with("err_msg", 'Missing data to perform action.'  ); 
      }
      
      $image_url =   config('app.app_cdn').   "/store/menu/btm-" . $request->bin . "/";
      $folder_path =  config('app.app_cdn_path') . "/store/menu/btm-" . $request->bin ;

       $request->session()->put('selection_biz', $request->bin );  

      //saving image 
      if( $request->file('photo') != "")
      { 

        if( !File::isDirectory($folder_path))
        {
           File::makeDirectory( $folder_path, 0777, true, true);
        } 
        $imageName = "wm_" . time() . '.' .$request->photo->getClientOriginalExtension();
        $request->photo->move( $folder_path , $imageName);  
        $image_url =  $image_url . $imageName ; 


        DB::table('ba_business_menu')
        ->where('bin',$request->bin )
        ->increment('page_no', 1);


        $menu = new BusinessMenu;
        $menu->bin = $request->bin;
        $menu->url = $image_url;
        $menu->page_no = 0;
        $menu->save();

      }
      else 
      {
          return redirect("/admin/systems/add-qr-menu")->with("err_msg", 'Missing data to perform action.'  );  
      }
        

      return redirect("/admin/systems/add-qr-menu")->with("err_msg",  'QR Menu slide image saved.'  ); 
  }


   protected function removeQrMenuItem(Request $request)
  {
    if($request->key == ""   )
      {
        return redirect("/admin/systems/add-qr-menu")->with("err_msg", 'Missing data to perform action.'  ); 
      }

    $menuitem =  BusinessMenu::find($request->key); 
    $bin = $menuitem->bin; 
    $menuitem->delete();
      
    $pages = DB::table('ba_business_menu')
    ->where('bin', $bin )
    ->orderBy("id", "asc")
    ->get();

    $page_no = 0;
    foreach($pages as $item)
    {
      DB::table('ba_business_menu')
      ->where('id', $item->id )
      ->update( ['page_no' =>  $page_no] );

      $page_no++;
    }
 

    return redirect("/admin/systems/add-qr-menu")->with("err_msg", 'Menu item removed.'  ); 
  }



  protected function pendingPayments(Request $request )
  {

      if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }
 

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }

        $inner_query = DB::table('ba_pick_and_drop_order')
        ->whereRaw("month(service_date) = '". $month . "'")
        ->whereRaw("year(service_date) = '". $year . "'") 
        ->whereRaw("clerance_status = 'un-paid'")
        ->whereRaw("book_status in  (  'completed', 'delivered') " )
        ->select('request_by as bin', DB::raw('sum(total_amount) as totalDue'))
        ->groupBy('bin');

        $pnds = DB::table('ba_business') 
        ->join(DB::raw('(' . $inner_query->toSql() . ') sales'), 'ba_business.id', '=', 'sales.bin') 
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "sales.totalDue" ) 
        ->orderBy("ba_business.name",'asc')  ;

 

        $inner_query_2 = DB::table('ba_service_booking')
        ->whereRaw("month(service_date) = '". $month . "'")
        ->whereRaw("year(service_date) = '". $year . "'") 
        ->whereRaw("clerance_status = 'un-paid'")
        ->whereRaw("book_status in  (  'completed', 'delivered') " )
        ->select('bin', DB::raw('sum(seller_payable) as totalDue'))
        ->groupBy('bin');

        $result = DB::table('ba_business') 
        ->join(DB::raw('(' . $inner_query_2->toSql() . ') sales'), 'ba_business.id', '=', 'sales.bin') 
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "sales.totalDue" ) 
        ->union($pnds) 
        ->get();






        $data = array(  'result' => $result , 'month' =>  $month ,  'year' => $year ); 
        return view("admin.reports.pending_payments")->with(  $data);


    }




    protected function processAgentSelfAssignRequest(Request $request)
    {
    
    if(  $request->confsakey == "" ||  $request->confsaaid == ""  )
    {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", 'Missing data to perform action.');
    }


    
    $member_info  = CustomerProfileModel::find($request->confsaaid);
    if(   !isset( $member_info)  )
    {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching agent profile found.'   );
    }


    $order_info = PickAndDropRequestModel::find( $request->confsakey );
    if(   !isset( $order_info)  )
    {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",   'No matching order found.'  );
    }


    $business_owner  = DB::table("ba_business")
    ->select( "name", "phone_pri as phone")
    ->where("id", $order_info->request_by )
    ->first(); 

    if(  !isset( $business_owner)  )
    {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",     'No matching business found.'  );
    }

    $delivery_order = DeliveryOrderModel::where("member_id", $request->confsaaid )
    ->where("order_type", "pnd" )
    ->where("order_no", $request->confsakey)
    ->first();

    if( !isset($delivery_order))
    {
      $delivery_order = new DeliveryOrderModel; 
    }

    $delivery_order->member_id = $request->confsaaid;
    $delivery_order->order_no = $request->confsakey;
    $delivery_order->accepted_date = date('Y-m-d H:i:s');
    $delivery_order->order_type =  "pnd";
    $save = $delivery_order->save(); 

    if($save)
    {
      $order_info->book_status = "delivery_scheduled";
      $order_info->save();  
      
      $orderStatus = new OrderStatusLogModel;
      $orderStatus->order_no = $order_info->id; 
      $orderStatus->order_status = "delivery_scheduled";
      $orderStatus->log_date = date('Y-m-d H:i:s');
      $orderStatus->save(); 
        
        
         /* 

        //send cloud SMS 
        $response = array();
        $response['title']     = "New Order Assigned";
        $response['message']   = "Order # " . $order_info->id . " is assigned to you."; 
        $response['imageUrl'] = config('app.app_cdn')  . "/assets/image/delivery-task.jpg"; 
        
        $receipent = User::where("profile_id",  $request->confsaaid )->first()  ; 
        
       

        if(  isset($receipent ) )
        {
          $firebase = new FirebaseCloudMessage();
          $regId    = $receipent->firebase_token;
          $response = $firebase->sendToSingleMember( $regId, $response, "agent" );
           
        }

        
        
        
        //send sms to customer  
          $phone = $order_info->phone  ;
          $tracking_sms  =  "Your order (# " . $order_info->id  . 
          ") from " . urlencode( $business_owner->name ) . " will be delivered via bookTou. Please track the progress using this link - https://booktou.in/shopping/where-is-my-order/" . $order_info->tracking_session   ;
          $this->sendSmsAlert(  array($phone)   ,  $tracking_sms); 
        
        */

       
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",      "Order assigned to delivery agent." );

      }
      else
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",    'Order assignment to agent failed.' );
      }

    
     }



     //from sanatomba image upload section


protected function EnterImageSlide()
{

  $slide = DB::table('cms_banner_sliders')
                    ->orderBy("position")
                    ->get();

 $businesses = DB::table('ba_business')
                    ->where("is_block", "no")
                    ->get();
                   
  return view ('admin.slide.slide')->with( array('slide' => $slide, 'businesses' => $businesses ) );
}


protected function updateSlidePosition(Request $request)
{
    $key = $request->key;

    $web_slider  = CmsBannerSlidersModel::find( $key); 


    $old_position  = CmsBannerSlidersModel::where('position',$request->position)->first();

    if (isset($old_position)) {
      
       $old_slider_position_update  = CmsBannerSlidersModel::find( $old_position->id);

       $old_slider_position_update->position = $web_slider->position;
       $old_slider_position_update->save(); 

    }

    if( isset($web_slider) )
      {

        $web_slider->position = $request->position;
        $save = $web_slider->save();

      }

      return redirect("admin/slide/enter-image-slide") ;  
}




protected function toggleSliderStatus(Request $request)
{
    $key = $request->key;

    if($request->status == "true")
      {
        $status = "yes";
      }
      else 
      {
        $status = "no";
      }


    $web_slider  = CmsBannerSlidersModel::find( $key); 
 

      if( isset($web_slider) )
      { 
        $web_slider->published = $status;
        $save = $web_slider->save();
      }

      return redirect("admin/slide/enter-image-slide") ;  
}




protected function photoSlide(Request $request)
{

      $position = $request->sliding_position;
      $bin =  $request->session()->get('_bin_' );
      if ($position==0) {
        return redirect('admin/slide/enter-image-slide')
        ->with("err_msg_right", 'Set image position first'); ;
      }

      if( $request->file('photo') != "")
      {
        $imageName =  time() . '.' .$request->photo->getClientOriginalExtension();
 
        $url =   'public/webapp/upload/' . $imageName ; 
        
        $request->photo->move(public_path('webapp/upload'), $imageName);

        $cmsSlider  = new CmsBannerSlidersModel(); 
        $cmsSlider->image_url = $url;
        $cmsSlider->bin = $request->bin;
        $cmsSlider->position = $position;
        $save = $cmsSlider->save();
        if ($save) {
          return redirect("admin/slide/enter-image-slide")->with("err_msg_right", 'Image saved.'  ); 
       }

      }
      else 
      {
         return redirect("admin/slide/enter-image-slide")->with("err_msg_right", 'Please select a photo!'  ); 
      }
  
      
          


}

   

   protected function deleteSlide(Request $request)
   {
      $id = $request->key;  
      if ( $id != "" ) 
      {
        $delete = DB::table('cms_banner_sliders')
        ->where('id',   $id)
        ->delete();
   
          if ($delete) 
          {
            return redirect("/admin/slide/enter-image-slide")->with("err_msg", 'Slider image removed sucessfully!'  );              
          }
      }

      return redirect("/admin/slide/enter-image-slide") ;  

   }



  //cms_product_groups creation section
  protected function enterProductMaster(Request $request)
  {
      $id = $request->id;
     
      if (isset($id)) {

        $resultbyId = DB::table('cms_product_groups')
        ->where('id',$id)
        ->select()
        ->get();
      }

      $result = DB::table('cms_product_groups')
                ->get();


      if($id) {
      $all = array('result' => $result,'resultbyId'=>$resultbyId);            
      }else {
        $all = array('result' => $result);            
      }

     //dd($all);die();
      return view('admin.cms.view_product_groups')->with('all',$all);    
   }



   protected function saveProductGroupMaster(Request $request)
   {
      if($request->btn_save=='Save')
      {

      $title = $request->pr_title;
      $group = $request->pr_group;
      $description = $request->pr_group_description;
      $landingurl = $request->landing_url;
      //$product_code = $request->prod_code;

      if(!isset($title)&& !isset($group) && !isset($description) && !isset($landingurl))
      {
          return redirect('admin/product/enter-product-master')
          ->with('err_msg_right','please provide complete information first!');
      }

      $productGroups = new CmsProductGroupsModel();
      $productGroupsList = new CmsProductsListedModel();

      $productGroups->product_group = $group;
      $productGroups->title  = $title;
      $productGroups->description   = $description;
      $productGroups->landing_url  = $landingurl;

      //
      //$productGroupsList->group_name =$group;
      //$productGroupsList->pr_code =$product_code;

      $save = $productGroups->save();
      //$savegroupList = $productGroupsList->save();

      if ($save) {
      return redirect('admin/product/enter-product-master')->
      with('err_msg_right',"Information save");      
      }



      }
      else {

      $title = $request->pr_title;
      $group = $request->pr_group;
      $description = $request->pr_group_description;
      $landingurl = $request->landing_url;
      //$product_code = $request->prod_code;
      $id = $request->group_id;
      //$listed = $request->listed_id;


      if(!isset($id))
      {

        return redirect('admin/product/enter-product-master')
        ->with('err_msg_right','information mismatch');
      }

      if(!isset($title)&& !isset($group) && !isset($description) && !isset($landingurl))
      {
          return redirect('admin/product/enter-product-master')
          ->with('err_msg_right','please provide complete information first!');
      }

      $update = DB::table('cms_product_groups')
      ->where("id",$id)
      ->update(array(
        'product_group' => $group,'title' => $title,
        'description' => $description,'landing_url'=>$landingurl

      ));

      // $updateListed = DB::table('cms_products_listed')
      // ->where("id",$listed)
      // ->update(array('pr_code' => $product_code,'group_name' => $group));



      
      if ($update) {
        
      return redirect('admin/product/enter-product-master')
      ->with('err_msg_right',"Information update sucessfull!");    
      }else
      {
        
        return redirect('admin/product/enter-product-master')
      ->with('err_msg_right',"Failed to update record!");    
      }
}
   }


//deleting cms_product_groups

protected function deleteProductGroup(Request $request)
{

  $id = $request->id;

  if (isset($id)) {
    
     $deleteProductGroup = CmsProductGroupsModel::find($id);

      

     $deleteProductListGroup = DB::table('cms_products_listed')
     ->where('group_name', '=', $deleteProductGroup->product_group)->delete();
     
     

     $deleteProductGroup = DB::table('cms_product_groups')
                    ->where('id', '=', $id)->delete();


     
  
    if ($deleteProductGroup) {
      return redirect('admin/product/enter-product-master')->with('err_msg','record deleted sucessfull!');
    }else{
      return redirect('admin/product/enter-product-master')->with('err_msg','failed to deleted record!');
    }

  }else
      {

      }

}




public function getProductName(Request $request)

{
      $search = $request->search;

      if($search)
      {
         $result = 
         ProductModel::select('pr_code','pr_name')->where('pr_name', 'like', '%' .$search . '%')
         ->limit(20)
         ->get();
      }

      $response = array();

      foreach($result as $result){

         $response[] = array("value"=>$result->pr_code,"label"=>$result->pr_name);

      }
      echo json_encode($response);
      exit;

        

}



//cms_product_groups sectionends here

//cms productlisted group starts from here

protected function enterSaveProductListed(Request $request)
   {
      $id = $request->id;
     
      if (isset($id)) {

        $resultbyId = DB::table('cms_products_listed')
        ->where('id',$id)
        ->select()
        ->get();
      }

      $result = DB::table('cms_products_listed')
                ->get();

      $productGroup = DB::table('cms_product_groups')
                      ->get();

      if($id) {
      $all = array('result' => $result,'resultbyId'=>$resultbyId,'group'=>$productGroup);            
      }else {
        $all = array('result' => $result,'group'=>$productGroup);            
      }

    // dd($all);die();



      return view('admin.cms.view_product_listed')->with('all',$all);    
   }



protected function saveProductListedMaster(Request $request)
{
    if($request->btn_save=='Save')
    {

            $groupname = $request->group_name;
            $prodName = $request->prod_code;
            
            if(!isset($groupname)&& !isset($prodName))
            {
                return redirect('admin/product/enter-save-product-listed')
                ->with('err_msg_right','please provide complete information first!');
            }

            $productGroupsList = new CmsProductsListedModel();

            $productGroupsList->group_name = $groupname;
            $productGroupsList->pr_code = $prodName;

            $save = $productGroupsList->save();
            if ($save) {
              return redirect('admin/product/enter-save-product-listed')
              ->with('err_msg_right','Information save sucessfull!');
            }else{
              return redirect('admin/product/enter-save-product-listed')
              ->with('err_msg_right','Failed!');
            }

    }else
    {
      $id = $request->listed_id;
      
      if(!isset($id))
      {

        return redirect('admin/product/enter-save-product-listed')
        ->with('err_msg_right','information mismatch');
      }
      $groupname = $request->group_name;
      $prodName = $request->prod_code;

      if(!isset($groupname) && !isset($prodName))
      {
          return redirect('admin/product/enter-save-product-listed')
          ->with('err_msg_right','please provide complete information first!');
      }

      

      $update = DB::table('cms_products_listed')
      ->where("id",$id)
      ->update(array(
        'group_name' => $groupname,
        'pr_code' => $prodName
      ));

      if ($update) {
        return redirect('admin/product/enter-save-product-listed')
          ->with('err_msg_right','Information updated sucessfull!'); 
      }
      else
      {
        return redirect('admin/product/enter-save-product-listed')
          ->with('err_msg_right','Failed!'); 
      }

    }

}



protected function deleteProductListed(Request $request)
{

  $id = $request->id;
  if (!isset($id)) {
    return redirect('admin/product/enter-save-product-listed')
    ->with('err_msg','information mismatch!');
  }
  if (isset($id)) {
    
    $deleteProductListGroup = DB::table('cms_products_listed')
    ->where('id', $id)->delete();

    if ($deleteProductListGroup) {
      return redirect('admin/product/enter-save-product-listed')->with('err_msg','record deleted sucessfull!');
    }else{
      return redirect('admin/product/enter-save-product-listed')->with('err_msg','failed to deleted record!');
    }

  }else
      {
          return redirect('admin/product/enter-save-product-listed')
          ->with('err_msg','information mismatch!');
      }

}

//cms productlisted group ends here


//e_bill


public function generateServiceBill( Request $request)
{

  $order_info = $order_items =null;
  if( $request->o  != ""  )
  {
    $orderno = $request->o;
    $order_info =  DB::table("ba_service_booking") 
    ->join("ba_profile","ba_profile.id","=","ba_service_booking.book_by")  
    ->where("ba_service_booking.id", $orderno )
    ->select("ba_service_booking.*","ba_profile.fullname", "ba_profile.phone", "ba_profile.locality")    
    ->first() ;

    $order_items   = DB::table("ba_service_booking_details")  
    ->where("ba_service_booking_details.book_id", $orderno ) 
    ->get() ;

  }
  else
  {
    return;
    //return redirect("/services/new-bookings")->with("err_msg", "Failed to download invoice!"); 
  }
  
  $public_path = public_path();
  
  $folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $order_info->bin .  "/"     ;
  $folder_path =   $public_path  .  '/assets/bills/btm-' .  $order_info->bin .  "/"     ;
  
  if( !File::isDirectory($folder_path)){
        File::makeDirectory( $folder_path, 0777, true, true);
  }
  
  $pdf_file =  "btr-" . $request->o . ".pdf";
  $save_path = $folder_path .  $pdf_file  ;
  
  $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
  ->setPaper('a4', 'portrait')
  ->loadView('docs.ebill_services' ,  array('data' =>   $order_info , 'order_items' => $order_items )  )  ;


  return $pdf->download('ebill-' . $request->o  .   mt_rand(0,20)  .    '.pdf');


}


 protected function bookingAppointment(Request $request)
    {
        $setData=$request->o;
       
        $result_staff = DB::table('ba_service_booking')
        ->join('ba_users' ,'ba_service_booking.staff_id', '=', 'ba_users.profile_id')
        ->join('ba_service_booking_details','ba_service_booking.id', '=', 'ba_service_booking_details.book_id')
        ->join('ba_profile','ba_users.profile_id','=','ba_profile.id')

    
        ->where("ba_service_booking.id",'=',$setData)
        ->select()
        ->get();



        $result_venue = DB::table('ba_service_booking')
        ->join('ba_business','ba_service_booking.bin','=', 'ba_business.id')
        ->where("ba_service_booking.id",'=',$setData)
        ->select()
        ->get();


        

        $result_customer = DB::table('ba_service_booking')
        ->join(DB::Raw("ba_profile as bp"),'ba_service_booking.book_by' ,'=', 'bp.id')
        ->where("ba_service_booking.id",'=',$setData)
        
        ->select("fullname","bp.locality","bp.landmark","bp.city","bp.state","bp.pin_code","bp.latitude","bp.longitude","bp.phone","bp.email")
        ->get();

        $result_billing =DB::table('ba_service_booking') 
        ->join('ba_service_booking_details','ba_service_booking.id','=','ba_service_booking_details.book_id')
        ->where('ba_service_booking.id',$setData)
        ->select ("ba_service_booking.id","service_date",DB::Raw('sum(discount) as discount, sum(service_fee) as fees, sum(service_charge) as charge')  )
        ->groupBy('ba_service_booking.id')
        ->groupBy('service_date')
        ->get();

        
        $data = array(  'result' => $result_staff,'venue'=>$result_venue,
            'customer'=>$result_customer,'bill'=>$result_billing,'service_id'=>$setData);  

        return view("admin.bookings.booking_appointment")->with($data);
    }


 protected function updateRemark(Request $request)
    {

        $remark = $request->cus_remark;
        $id= $request->serviceid;
        
        $update = DB::table('ba_service_booking')
              ->where('id', $id )
              ->update(['cust_remarks' => $remark]);

              if($update)
              {
                $msg = "Remark added!";

              }else{
                $msg ="Failed to add remark";
              }
        
        return response()->json(array('success' => true, 'detail_msg' => $msg ));
    }


  //cms_product_groups creation section
  protected function prepareDailyCollection(Request $request)
  {


      if(isset($request->filter_date))
       {
          $today =   date('Y-m-d', strtotime($request->filter_date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }
        
        $totalEarning = 0;

        $aid = $request->ai;

        $agent_info  = DB::table("ba_profile") 
        ->where( "id",  $aid )  
        ->select("ba_profile.id", "ba_profile.fullname")
        ->first(); 

       

      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_service_booking.service_date', $today)  
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            "ba_service_booking.service_date",
            "ba_service_booking.preferred_time as delivery_time",
            'book_status as orderStatus',
            'seller_payable as totalAmount',
            'delivery_charge as serviceFee',
            "payment_type as payMode",
            DB::Raw("'normal' orderType"),
            "ba_delivery_order.member_id"
        ) ;
 


        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid )
        ->whereDate('ba_pick_and_drop_order.service_date', $today)   
        ->union( $normalorders )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            DB::Raw("'pnd' orderType"),
            "ba_delivery_order.member_id"
        )
        ->get();
  

        $merchantIds =array();
        $customerIds =array();
        $toalamount =0.00;
        foreach ($all_orders as $item) 
        {
            $toalamount += $item->totalAmount + $item->serviceFee;

          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }
        $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;
                    break;
                }

            }
        }
                 
       }

       $data = array(  'date' =>$today  ,  "total_amount" => $toalamount,  'agent' =>  $agent_info,  'all_orders' =>  $all_orders );

      return view("admin.earning_clearance.daily_collection_from_agent")->with('data',$data);


  }

  protected function saveDailyCollection(Request $request)
  {
     if( isset($request->submit ))
     {
        $agent = $request->ai;

        $online = ( $request->online != "" ? $request->online : 0.00 );
        $cash = ( $request->cash  != "" ? $request->cash : 0.00 );
        $due = ( $request->due != "" ? $request->due : 0.00 );
        $total = ( $request->total != "" ? $request->total : 0.00 );
        $accountdate = ( $request->accountdate != "" ? date('Y-m-d', strtotime($request->accountdate)) : date('Y-m-d' ) );
        $actualdate = ( $request->actualdate != "" ? date('Y-m-d', strtotime($request->actualdate)) : date('Y-m-d' ) );
        $remarks = ( $request->remarks != "" ? $request->remarks : "money collection from agent recorded" );
        $agent = $request->ai;

        $account = new AccountsLog ;
        $account->agent_id = $agent ;
        $account->cash_pay = $cash ;
        $account->online_pay = $online ;
        $account->total_collected = $total ;
        $account->total_due = $due ; 
        $account->account_date = $accountdate ;
        $account->collection_date = $actualdate ;
        $account->remarks = $remarks ;
        $save = $account->save(); 
        return redirect("/admin/accounting/prepare-daily-collection?ai=" . $agent )->with("err_msg", "Collection information updated.");
     }

  }
 
  
  protected function remarksAndFeedback(Request $request)
  {
     if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)) ; 
      }
      else 
      {
        $orderDate = date('Y-m-d'  )  ; 
      }


  

      if( isset($request->btn_search) )
      {
 
        $request->session()->put('_last_search_date',  $request->filter_date ); 
      }
      else 
      {

        $request->session()->remove('_last_search_date' ); 
      }


      if( isset($request->filter_orderno))
      {

          $pnd_list  = DB::table("ba_pick_and_drop_order") 
          ->where("id",   $request->filter_orderno )
          ->select( "ba_pick_and_drop_order.id",    
              "request_by as bin", 
              "book_date",   
              "book_status",
              "fullname",  
              DB::Raw("'pnd' orderType") ) ;

            $day_bookings  = DB::table("ba_service_booking")
            ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
            ->where("ba_service_booking.id",   $request->filter_orderno )
            ->select( "ba_service_booking.id",    
              "ba_service_booking.bin", 
              "book_date",   
              "book_status",
              "ba_profile.fullname", 
              DB::Raw("'normal' orderType") ) 
            ->union($pnd_list)
            ->paginate(10);

        }
      else 
      {

        $pnd_list  = DB::table("ba_pick_and_drop_order") 
          ->where("id",   $request->filter_orderno )
          ->select( "id",    
              "request_by as bin", 
              "book_date",   
              "book_status",
              "fullname",  
              DB::Raw("'pnd' orderType") ) ;

            $day_bookings  = DB::table("ba_service_booking")
            ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
            ->whereDate("ba_service_booking.service_date",   $orderDate )
            ->select( "ba_service_booking.id",    
              "ba_service_booking.bin", 
              "book_date",   
              "book_status",
              "ba_profile.fullname", 
              DB::Raw("'normal' orderType") ) 
            ->union($pnd_list)
          ->paginate(10);

        
      }
  

      $bookid  = array();
      $all_bins = array(); 
      foreach($day_bookings as $bitem )
      {
        $all_bins[] = $bitem->bin;  
        $bookid[] = $bitem->id;
      }



      $all_remarks = DB::table("ba_order_remarks")
      ->join("ba_profile", "ba_profile.id", "=", "ba_order_remarks.remark_by")
      ->whereIn("order_no", $bookid)    
      ->select("ba_order_remarks.*", "ba_profile.fullname" )
      ->get();


      $all_bins = array_filter($all_bins); 
      $all_businesses = DB::table("ba_business")
      ->whereIn("id", $all_bins)    
      ->get();

      $last_keyword = "0";
      if($request->session()->has('_last_search_' ) ) 
      {
        $last_keyword = $request->session()->get('_last_search_' );
      }

      $last_keyword_date = date('d-m-Y') ;
      if($request->session()->has('_last_search_date' ) ) 
      {
        $last_keyword_date = $request->session()->get('_last_search_date' );
      }

      $data = array( 'title'=>'Order Feedback and Remarks', 'results' => $day_bookings, 'date' => $orderDate,   
        'all_businesses' =>$all_businesses, 'all_remarks' =>$all_remarks,
        'last_keyword' => $last_keyword, 'last_keyword_date' =>  $last_keyword_date , 'refresh' => 'true' );

         return view("admin.orders.remarks")->with('data',$data);

  }



  //
  protected function prepareAllocationTablePnD(Request $request)
    {

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)); 
      }
      else 
      {
        $orderDate = date('Y-m-d' , strtotime("7 days ago"))  ; 
      }
        
  

      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );

        $bin = $request->filter_business;
        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new' ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
          $status = array( 'new'  ) ; 
        }
 
        if($bin == -1)
        {
          $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)   
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone", "ba_business.locality as businessLocality" ) 
       ->orderBy("ba_business.locality", "asc")
        ->get();
          }
          else 
            
            {
              $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_business.id", $bin)
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone", "ba_business.locality as businessLocality" ) 
        ->orderBy("ba_business.locality", "asc")
        ->get();
            }
      


      }
      else 
      {

        $request->session()->remove('_last_search_date' );
        $status = array( 'new' ) ;  

        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone", "ba_business.locality as businessLocality" ) 
        ->orderBy("ba_business.locality", "asc")
        ->get();

      } 

      //finding free agents  
      $today = date('Y-m-d'); 

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 

      ->get();
 


      $agent_locations = DB::table("ba_cache_agent_location")  
      ->get();

     
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 

      

      $requests_to_process  = DB::table("ba_order_process_request")  
      ->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
      ->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
      ->whereDate("log_date", $today ) 
      ->get(); 

  

      $merchants = array();
      foreach($day_bookings as $item)
      {
        $merchants[] = $item->bin;
      }
      $merchants[] = 0;

      $merchants = array_unique($merchants);

      $businesses  = DB::table("ba_business") 
      ->whereIn("id", $merchants ) 
      ->get(); 

  
      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->get(); 
      
      
     
       
 
       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  
        'results' => $day_bookings,  
        'requests_to_process' =>$requests_to_process,
        'all_business' => $all_business,
        'todays_business' => $businesses,
        'last_keyword' => $last_keyword,
        'last_keyword_date' =>  $last_keyword_date , 
        'refresh' => 'true' ,
        'agent_locations' => $agent_locations
      );
       return view("admin.orders.prepare_allocation_table_pnd")->with('data',$data);
    }


    protected function routeAndTasksTable(Request $request)
    {

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)); 
      }
      else 
      {
        $orderDate = date('Y-m-d' , strtotime("7 days ago"))  ; 
      }
        
  

      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );

        $bin = $request->filter_business;
        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new' ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
          $status = array( 'new'  ) ; 
        }
 
        if($bin == -1)
        {
          $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)   
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone", "ba_business.locality as businessLocality" ) 
       ->orderBy("ba_business.locality", "asc")
        ->get();
          }
          else 
            
            {
              $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_business.id", $bin)
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone", "ba_business.locality as businessLocality" ) 
        ->orderBy("ba_business.locality", "asc")
        ->get();
            }
      


      }
      else 
      {

        $request->session()->remove('_last_search_date' );
        $status = array( 'new' ) ;  

        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone", "ba_business.locality as businessLocality" ) 
        ->orderBy("ba_business.locality", "asc")
        ->get();

      } 

      //finding free agents  
      $today = date('Y-m-d'); 

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 

      ->get();

 
 


     $agent_locations = DB::table("ba_cache_agent_location") 
      ->join("ba_profile", "ba_profile.id", "=", "ba_cache_agent_location.staff_id")
      ->select("ba_profile.*", "ba_cache_agent_location.location")
      ->where("on_field", "YES")
      ->get();
     
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 

      

      $requests_to_process  = DB::table("ba_order_process_request")  
      ->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
      ->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
      ->whereDate("log_date", $today ) 
      ->get(); 

  

      $merchants = array();
      foreach($day_bookings as $item)
      {
        $merchants[] = $item->bin;
      }
      $merchants[] = 0;

      $merchants = array_unique($merchants);

      $businesses  = DB::table("ba_business") 
      ->whereIn("id", $merchants ) 
      ->get(); 

  
      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->get(); 
      
      
      $all_staffs = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname",  "ba_users.id as user_id" ) 
      ->where("ba_users.category", ">", "100")
      ->where("ba_users.status", "<>" , "100")  
      ->get();
       
 
       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  'staffs' =>$all_staffs, 
        'results' => $day_bookings,  
        'requests_to_process' =>$requests_to_process,
        'all_business' => $all_business,
        'todays_business' => $businesses,
        'last_keyword' => $last_keyword,
        'last_keyword_date' =>  $last_keyword_date , 
        'refresh' => 'true' ,
        'agent_locations' => $agent_locations,
        'sub_menu_name' =>  "menu.submenu.stocks_menu"
      );
       return view("admin.orders.route_and_task_table_pnd")->with('data',$data);
    }



  protected function updateAgentLocation(Request $request)
  {
      $agentid = $request->key;
      $lastlocation = $request->lastlocation;

      if($agentid == "" || $lastlocation == "" )
      {
        return redirect("/admin/pick-and-drop-orders/prepare-allocation-table"); 
      }
      
      $agentlocation =  AgentCacheModel::where("staff_id",$agentid)->first();

      if( !isset($agentlocation))
      {
        $agentlocation =  new AgentCacheModel; 
      }

      $agentlocation->staff_id =$agentid;
      $agentlocation->location = $lastlocation ;
      $agentlocation->save() ;
      return redirect("/admin/pick-and-drop-orders/prepare-allocation-table"); 
    }


//new section to add in adminDashboard Controller

  public function generatePNDServiceBill( Request $request)
    {

      $order_info = $order_items =null;
      if( $request->o  != ""  )
      {

       
        $orderno = $request->o;

        $order_info =  DB::table("ba_order_sequencer") 
        ->join("ba_pick_and_drop_order","ba_order_sequencer.id","=","ba_pick_and_drop_order.id")  
        ->where("ba_pick_and_drop_order.id", $orderno )
        ->select()    
        ->first();

         
      }
      else
      {
        return;
        //return redirect("/services/new-bookings")->with("err_msg", "Failed to download invoice!"); 
      }
      
      $public_path = public_path();
      
      $folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $order_info->request_by .  "/"     ;
      $folder_path =   $public_path  .  '/assets/bills/btm-' .  $order_info->request_by .  "/"     ;
      
      if( !File::isDirectory($folder_path)){
            File::makeDirectory( $folder_path, 0777, true, true);
      }
      
      $pdf_file =  "btr-" . $request->o . ".pdf";
      $save_path = $folder_path .  $pdf_file  ;
      
      $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
      ->setPaper('a4', 'portrait')
      ->loadView('docs.pnd_ebill_services' ,  array('data' =>  $order_info )  )  ;


      return $pdf->download('ebill-' . $request->o  .   mt_rand(0,20)  .    '.pdf');


    }


    //prepare monthly tax page
   protected function prepareMonthlyTaxes(Request $request)
    {
        if( $request->month  == "")
        { 
            $month   =  date('m');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
        else 
        {
          $month   =  $request->month ;
          $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
 

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }


           

        //retrieving business list and getting total sales amount for the merchant upto current month
        $business = DB::table('ba_business')
        ->select()
        ->where('is_block','no')
        ->get();

        $bin = array();

        foreach ($business as $biz) {
          $bin[] = $biz->id;
        }
        

        // $merchantTotalAmount = DB::table('ba_service_booking')
        // ->whereIn('bin',$bin)
        // ->whereYear("ba_service_booking.service_date",$year)
        // ->where('book_status','delivered')
        // ->select(DB::Raw('sum(total_cost) as taxableAmount'),"bin") 
        // ->groupBy('bin')
        // ->get();
       
        // $normalTotalAmount = DB::table('ba_service_booking')
        // ->whereIn('bin',$bin)
        // ->whereYear("ba_service_booking.service_date",$year)
        // ->where('book_status','delivered')
        // ->select(DB::Raw('sum(total_cost) as taxableAmount'),"bin") 
        // ->groupBy('bin')
        // ->get();

        // $pndTotalAmount = DB::table('ba_pick_and_drop_order')
        // ->select(DB::Raw('sum(total_amount) as taxableAmount'),"request_by as bin")
        // ->whereIn('request_by',$bin)
        // ->where('request_from','business')
        // ->where('source','business')
        // ->where('pay_mode','ONLINE')
        // ->whereYear("ba_pick_and_drop_order.service_date",$year)
        // ->where('book_status','delivered')
        // ->groupBy('bin')
        // ->get();


        $merchantTotalAmount = DB::select('select sum(taxableAmount) as taxableAmount,bin 
          from
          (
            select sum(total_cost)as taxableAmount, bin from ba_service_booking
            where book_status="delivered" and year(service_date)='.$year.' 
            and bin in ('.implode(",",$bin).')  
            group by bin
    
            UNION
    
            select sum(total_amount)as taxableAmount,request_by as bin 
            from ba_pick_and_drop_order
            where book_status="delivered" and 
            year(service_date) = '.$year.' and 
            source="business"
            and request_from="business" 
            and pay_mode="online"
            and request_by in ('.implode(",",$bin).') 

            group by bin
            
            )npnd group by bin' );
       

        //dd($merchantTotalAmount);
           
        // $merchantTotalAmount = DB::select("SELECT 
        //       sum(taxableAmount) as taxableAmount
        //       FROM ({$pndTotalAmount->toSql()}) AS b
        //   ", $pndTotalAmount->getBindings())
        //  ->groupBy('bin')
        //  ->get();
          

       // dd($merchantTotalAmount);

         

        //retrieving business list ends here
        $normal_orders= DB::table("ba_service_booking")
        ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
        ->whereMonth("ba_service_booking.service_date",$month)
        ->whereYear("ba_service_booking.service_date",$year)
        ->select("ba_service_booking.id as orderNo","ba_service_booking.bin as bin",
                  "ba_service_booking.book_date as orderDate",
                  "ba_service_booking.total_cost as total_amount",
                  "ba_service_booking.delivery_charge",
                  "ba_service_booking.payment_type","ba_service_booking.source",
                  "ba_service_booking.service_date as receiptDate",
                  "ba_service_booking.service_date as payDate",
                  "ba_service_booking.book_status as orderStatus", 
                  "ba_business.name","ba_business.category",
                  "ba_business.gstin as gst", 
                  "ba_business.commission",
                  DB::Raw("'Normal' type")); 

        
        $pnd_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_pick_and_drop_order.id")
        ->whereMonth("ba_pick_and_drop_order.service_date",$month)
        ->whereYear("ba_pick_and_drop_order.service_date",$year)
        ->unionAll($normal_orders)
        ->select("ba_pick_and_drop_order.id as orderNo",
                "ba_pick_and_drop_order.request_by as bin",
                "ba_pick_and_drop_order.book_date as orderDate",
                "ba_pick_and_drop_order.total_amount as total_amount",
                "ba_pick_and_drop_order.service_fee as delivery_charge",
                "ba_pick_and_drop_order.pay_mode as payment_type",
                "ba_pick_and_drop_order.source",
                "ba_pick_and_drop_order.service_date as receiptDate",
                "ba_pick_and_drop_order.service_date as payDate",
                "ba_pick_and_drop_order.book_status as orderStatus",
                DB::Raw("'NA' name ,
                         'Service' category,
                         'NA' gst,  
                         '0.00' commission 
                         "), 
                "ba_order_sequencer.type as type")

        ->orderBy('orderNo','asc')
        ->paginate(100);
        //->get();
        //ends here
         
       $day_bookings = $pnd_orders;
        
        

        //generating the same query to show total amount at the top of the page:


        $n_orders= DB::table("ba_service_booking")
        ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
        ->whereMonth("ba_service_booking.service_date",$month)
        ->whereYear("ba_service_booking.service_date",$year)
        ->select("ba_service_booking.id as orderNo","ba_service_booking.bin as bin",
                  "ba_service_booking.book_date as orderDate",
                  "ba_service_booking.total_cost as total_amount",
                  "ba_service_booking.delivery_charge",
                  "ba_service_booking.payment_type","ba_service_booking.source",
                  "ba_service_booking.service_date as receiptDate",
                  "ba_service_booking.service_date as payDate",
                  "ba_service_booking.book_status as orderStatus", 
                  "ba_business.name","ba_business.category",
                  "ba_business.gstin as gst", 
                  "ba_business.commission",
                  DB::Raw("'Normal' type")); 

        
        $p_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_pick_and_drop_order.id")
        ->whereMonth("ba_pick_and_drop_order.service_date",$month)
        ->whereYear("ba_pick_and_drop_order.service_date",$year)
        ->unionAll($n_orders)
        ->select("ba_pick_and_drop_order.id as orderNo",
                "ba_pick_and_drop_order.request_by as bin",
                "ba_pick_and_drop_order.book_date as orderDate",
                "ba_pick_and_drop_order.total_amount as total_amount",
                "ba_pick_and_drop_order.service_fee as delivery_charge",
                "ba_pick_and_drop_order.pay_mode as payment_type",
                "ba_pick_and_drop_order.source",
                "ba_pick_and_drop_order.service_date as receiptDate",
                "ba_pick_and_drop_order.service_date as payDate",
                "ba_pick_and_drop_order.book_status as orderStatus",
                DB::Raw("'NA' name ,
                         'NA' category,
                         'NA' gst,  
                         '0.00' commission 
                         "), 
                "ba_order_sequencer.type as type")

        ->orderBy('orderNo','asc')
        ->get();
        $cal_amount_bookings = $p_orders;


        //query for generating total amount at the top of the pages ends here


        //calculation starts from here
        $total_bill_amount=0;
        $total_delivery_charges=0;
        $total_amount_from_customer=0;
        $total_gst_tcs = 0;
        $total_income_tax_tds = 0;
        $total_commission=0;
        $total_payable_to_merchant = 0;
        $total_booktou_income = 0;
        $total_output_cgst = 0;
        $total_output_sgst = 0;
        $total_gross_revenue_for_booktou = 0;
        $annual_income=0;
        $tax= 500000;

        foreach ($cal_amount_bookings as $total_booking_amount) {
          //calculating cgst sgst
          if ($total_booking_amount->orderStatus=="delivered"){
             
             //calculation part for cgst sgst
             if ($total_booking_amount->gst=="" || $total_booking_amount->gst=="NA") {
          
                  $gst="NA";
                  $cgst = 0;
                  $sgst = 0;
                }else{

                  $gst = $total_booking_amount->gst;
                  $cgst = 25;
                  $sgst = 25;
                }
            //

            //taxable amount
                $taxableAmount = $total_booking_amount->total_amount;
            ////

                if($total_booking_amount->type=="pnd"){
                  $cgst = 0;
                  $sgst = 0;
                }

                //total bill   

                $total_bill = $taxableAmount + $cgst+$sgst;
                //

                //delivery charge
                $delivery_charge = $total_booking_amount->delivery_charge;
                ////


                //bill amount
                $billAmount =  $total_bill + $delivery_charge;
                //


                //gst tcs 
                if ($total_booking_amount->gst=="" || $total_booking_amount->gst=="NA") {
                    $gst_tcs = 0; 
                }else{
                    $gst_tcs = $taxableAmount * 1/100;
                }
                //

                

                //annual income for merchant exceeding 5 lakh of product sales through booktou if exceeds income tax will be calculated
                foreach ($merchantTotalAmount as $amount) {
                 if ($amount->bin==$total_booking_amount->bin) {
                    
                    $annual_income+=$amount->taxableAmount;
                 }
                }
                ////

                //calculating income tax
                $income_tax = ($annual_income > $tax) ? $taxableAmount*.01 : 0;
                $income_tax_tds = $income_tax;
                ////

                //commission for booktou
                switch ($total_booking_amount->type) {
                          case 'Normal':
                             $percentage = $total_booking_amount->commission / 100;
                            break;
                          case 'pnd':

                              if ($total_booking_amount->source=="booktou" || "customer") {
                                 foreach ($business as $biz) {
                                   if ($biz->id==$total_booking_amount->bin) {
                                      
                                      $percentage = $biz->commission / 100;
                                   }
                                  }
                              }
                              if ($total_booking_amount->source=="business") {
                                $percentage=0;
                              }
                            
                            break;
                            case 'assist':
                                      $percentage = 0;
                            break;
                           
                        }
 
                $final_commision = $percentage * $total_bill;

                $booktou_commission = $final_commision;

                ////

                $payable_to_merchant = $total_bill - $gst_tcs-$income_tax_tds-$booktou_commission;


                ////////



                //total income
                $percentage_income = 18/100;
                $total_percent = 1+$percentage_income;
                $chargesncommission = $total_booking_amount->delivery_charge + $booktou_commission;
                $total_income = $chargesncommission/$total_percent;
                ///////
                
                
                //output cgst sgst
                $outputcgst= 9/100;
                $total_outputcgst = $total_income*$outputcgst;
                $total_outputsgst = $total_outputcgst;
                // 

                //total calculation to be display in view
                $total_bill_amount+=$total_bill;
                $total_delivery_charges +=$delivery_charge;
                $total_amount_from_customer+=$total_booking_amount->total_amount + $total_booking_amount->delivery_charge;
                $total_gst_tcs+=$gst_tcs;
                $total_income_tax_tds+=$income_tax_tds;
                 $total_payable_to_merchant+=$payable_to_merchant;
                $total_booktou_income+=$total_income;
                $total_commission+=$booktou_commission;
                $total_output_cgst+=$total_outputcgst;
                $total_output_sgst+=$total_outputsgst;
                $total_gross_revenue_for_booktou+=$total_income + $total_outputcgst + $total_outputsgst;



          }

      }

       
      //query ends here


        $data = array( 'month' =>  $monthname ,  'year' => $year ,
          'totalbill'=>$total_bill_amount,'totaldelivery'=>$total_delivery_charges,
          'totalamountcustomer'=>$total_amount_from_customer,'totalgst_tcs'=>$total_gst_tcs,
          'total_income_tax'=>$total_income_tax_tds, 'booktou_commission'=>$total_commission,
          'total_merchant_pay'=>$total_payable_to_merchant,'booktou_income'=>$total_booktou_income,
          'total_output_cgst'=>$total_output_cgst,'total_output_sgst'=>$total_output_sgst,
          'gross_revenue'=>$total_gross_revenue_for_booktou,
          'orderDetails'=> $day_bookings,
          'binTotalAmount'=>$merchantTotalAmount,'business'=>$business); 


        return view('admin.taxes.monthly_taxes')->with($data);

    }

    protected function exportMerchantTaxes(Request $request)
    {
        //section for merchant list when the link is clicked. 
        $business = DB::table('ba_business')
        ->select()
        ->where('is_block','no')
        ->orderBy('name','asc')
        ->get();
        //merchant lists ends here.

        //if the merchant name is selected do the action here
        if ($request->merchantname!="") {
           
           $bin = $request->merchantname;
         } 
  
        if( $request->month  == "")
        { 
            $month   =  date('m');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
        else 
        {
          $month   =  $request->month ;
          $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }


        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }



        if (isset($bin)) {

        // $merchantTotalAmount = DB::table('ba_service_booking')
        // ->where('bin',$bin)
        // ->select(DB::Raw('sum(total_cost) as taxableAmount'),"bin")
        // ->whereYear("ba_service_booking.service_date",$year)
        // ->where('book_status','delivered')
        // ->groupBy('bin')
        // ->get();

        $merchantTotalAmount = DB::select('select sum(taxableAmount) as taxableAmount,bin 
          from
          (
            select sum(total_cost)as taxableAmount, bin 
            from ba_service_booking
            where book_status="delivered" and 
            year(service_date)='.$year.' 
            and bin='.$bin.'  
            group by bin
    
            UNION
    
            select sum(total_amount)as taxableAmount,request_by as bin 
            from ba_pick_and_drop_order
            where book_status="delivered" and 
            year(service_date) = '.$year.' and 
            source="business"
            and request_from="business" 
            and pay_mode="online"
            and request_by = '.$bin.' 

            group by bin
            
            )npnd group by bin' );

        //dd($merchantTotalAmount);

        $normal_orders= DB::table("ba_service_booking")
        ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
        ->where("ba_service_booking.bin",$bin)
        ->whereMonth("ba_service_booking.service_date",$month)
        ->whereYear("ba_service_booking.service_date",$year)
        ->select("ba_service_booking.id as orderNo","ba_service_booking.bin as bin",
                  "ba_service_booking.book_date as orderDate",
                  "ba_service_booking.total_cost as total_amount",
                  "ba_service_booking.delivery_charge",
                  "ba_service_booking.payment_type","ba_service_booking.source",
                  "ba_service_booking.service_date as receiptDate",
                  "ba_service_booking.service_date as payDate",
                  "ba_service_booking.book_status as orderStatus", 
                  "ba_business.name","ba_business.category",
                  "ba_business.gstin as gst", 
                  "ba_business.commission",
                  DB::Raw("'Normal' type")); 

        $pnd_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_pick_and_drop_order.id")
        ->where("ba_pick_and_drop_order.request_by",$bin)
        ->where("ba_pick_and_drop_order.request_from",'business')
        ->whereMonth("ba_pick_and_drop_order.service_date",$month)
        ->whereYear("ba_pick_and_drop_order.service_date",$year)
        ->unionAll($normal_orders)
        ->select("ba_pick_and_drop_order.id as orderNo",
                "ba_pick_and_drop_order.request_by as bin",
                "ba_pick_and_drop_order.book_date as orderDate",
                "ba_pick_and_drop_order.total_amount as total_amount",
                "ba_pick_and_drop_order.service_fee as delivery_charge",
                "ba_pick_and_drop_order.pay_mode as payment_type",
                "ba_pick_and_drop_order.source",
                "ba_pick_and_drop_order.service_date as receiptDate",
                "ba_pick_and_drop_order.service_date as payDate",
                "ba_pick_and_drop_order.book_status as orderStatus",
                DB::Raw("'NA' name ,
                         'Service' category,
                         'NA' gst,  
                         '0.00' commission 
                         "), 
                "ba_order_sequencer.type as type")

        ->orderBy('orderNo','asc')
        ->get();
        //ends here
         
       $day_bookings = $pnd_orders;


        
       $data = array('merchant'=>$business,'orderDetails'=>$day_bookings,
          'binTotalAmount'=>$merchantTotalAmount,'month'=>$monthname,'year'=>$year);
         
          
      }else{

          $data = array('merchant'=>$business);
        }


      return view('admin.taxes.merchant_taxes')->with($data);


    }


  public function monthlyTaxesExportToExcel(Request $request) 
    
    {
         // return Excel::download(new MonthlyTaxesExport($request->key), 'monthly_taxes.xlsx');

       
          if( $request->bin  == "")
          { 
              return redirect('/admin/accounting/export-merchant-taxes');
          }

          if( $request->bin  != "")
          { 
             $bin = $request->bin;
              
          }
          if( $request->month   == "")
          { 
              $month = date('m');
          }
          else 
          {
            $month   =  $request->month; 
          }

          if( $request->year   == "")
          { 
              $year = date('Y');
          }
          else 
          {
            $year   =  $request->year; 
          }
          
          //echo $year;die();
           
       return Excel::download(new MonthlyTaxesExport($bin,$month,$year), 'monthly_taxes.xlsx');
    }

     


    protected function enterSalaryPage(Request $request)
    {
      
      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("category", "100" )
      ->where("ba_users.status", "<>", "100" )  
      ->select("ba_profile.id","ba_profile.fullname" )
      ->get(); 

      if (isset($request->id)) {
         
        $staff_salary = StaffSalaryModel::find($request->id);

        $staffid = $staff_salary->staff_id;
        $entry_date = $staff_salary->entry_date;
        $month = date('m',strtotime($entry_date));
        $year = date('Y',strtotime($entry_date));
        if ($staff_salary) {

        $expense =  DB::table("ba_daily_expense")   
                  ->where("use_by", $staffid)
                  ->whereMonth("use_date", $month)
                  ->select()
                  ->get(); 

        // calculating staff leave
        $staffleave = DB::table('ba_staff_attendance')
            ->where('staff_id',$staffid)
            ->whereMonth('log_date',$month)
            ->whereYear('log_date',$year)
            ->where('staff_status','leave')
            ->select()
            ->get();

            
            
         
            
            //$data = array('salary'=>$staff_salary,'staffprofile'=>$all_staffs);
            $data = array('salary'=>$staff_salary,'staffprofile'=>$all_staffs,
                          'staffleave'=>$staffleave,'staffexpense'=>$expense);

        }

      }else{

            $data = array('staffprofile'=>$all_staffs);

      }

      
      return view('admin.staffs.add_salary_entry_page')->with($data);

    }

    protected function saveStaffSalaryEntry(Request $request)
    {
      if ($request->id="")  
        {

          if ($request->agentsName==""|| $request->basicPay=="" || 
            $request->da=="" || $request->ta=="" || $request->hra=="" || $request->bonus=="" || 
            $request->gross=="") 
          {
            return redirect('admin/staff/enter-salary-page')->with('err_msg','mismatch Information!');
          }
        }

        //selected date
      if (isset($request->entryDate)) {
            
            $year = date('Y',strtotime($request->entryDate));
            $month = date('m',strtotime($request->entryDate));
            $monthname = date('F', mktime(0, 0, 0, $month, 10));

        }
         

      if (isset($request->agentsName)) {
          
            $result = DB::table('ba_staff_salary')
            ->where('month',$month)
            ->where('year',$year)
            ->where('staff_id',$request->agentsName)
            ->first();

            if($result){
              return redirect('admin/staff/enter-salary-page')->with('err_msg','Salary record already exist for the month of '.$monthname.' '.$year.'!');
            }
        }
        
      if ($request->saveStaffSalary=="save") {

          $salary = new StaffSalaryModel;
          $salary->staff_id = $request->agentsName;
          $salary->entry_date = $request->entryDate;
          $salary->month = $month;
          $salary->year = $year;

        }else{

          $id =  $request->staffSalaryid;
          
          $salary = StaffSalaryModel::find($id);
        }


        $salary->basic_pay = $request->basicPay;
        $salary->da = $request->da;
        $salary->ta = $request->ta;
        $salary->hra = $request->hra;
        $salary->bonus = $request->bonus;
        $salary->deduction = $request->deduction;
        $salary->no_of_leave = $request->leave;
        $salary->gross = $request->gross;
        
        
        
        $save = $salary->save();

        if ($save) {
          return redirect('admin/staff/enter-salary-page')
          ->with('err_msg','Staff salary information added!');
        }


    }

    protected function viewStaffSalary(Request $request)
    {

         

        if (isset($request->month) && isset($request->year)) {

         $month =  $request->month;
         $year =   $request->year;


         $allstaffs = DB::table('ba_staff_salary')
         ->join('ba_profile','ba_profile.id','=','ba_staff_salary.staff_id')
         ->where('month',$month)
         ->where('year',$year)
         ->select('ba_staff_salary.*','ba_profile.fullname')
         ->get();

          
         $data = array('staffs'=>$allstaffs);
         return view('admin.staffs.view_staff_salary_list')->with($data);
        }

       
        return view('admin.staffs.view_staff_salary_list');

    }

    public function eStaffSalarySlip( Request $request)
    {

      $order_info = $order_items =null;
      if( $request->o  != ""  )
      {

       
        $orderno = $request->o;

        $order_info =  DB::table("ba_staff_salary") 
        ->join("ba_profile","ba_profile.id","=","ba_staff_salary.staff_id")  
        ->where("ba_staff_salary.id", $orderno )
        ->select("ba_staff_salary.*","ba_profile.fullname")    
        ->first();

         
      }
      else
      {
        return;
        //return redirect("/services/new-bookings")->with("err_msg", "Failed to download invoice!"); 
      }
      
      $public_path = public_path();
      
      $folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $order_info->staff_id .  "/"     ;
      $folder_path =   $public_path  .  '/assets/bills/btm-' .  $order_info->staff_id .  "/"     ;
      
      if( !File::isDirectory($folder_path)){
            File::makeDirectory( $folder_path, 0777, true, true);
      }
      
      $pdf_file =  "btr-" . $request->o . ".pdf";
      $save_path = $folder_path .  $pdf_file  ;
      
      $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
      ->setPaper('a4', 'portrait')
      ->loadView('docs.ebill_salary_slip' ,  array('data' =>  $order_info )  )  ;


      return $pdf->download('ebill-' . $request->o  .   mt_rand(0,20)  .    '.pdf');


    }


    protected function enterDailyExpenses(Request $request)
    {

       $staffs =  DB::table("ba_profile")   
                  ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
                  ->where("category", "100" )
                  ->where("franchise", "0" )
                  ->where("ba_users.status", "<>", "100" )  
                  ->select("ba_profile.id","ba_profile.fullname" )
                  ->get();  


      return view('admin.accounts.daily_expenses')->with("data", array("staffs" => $staffs) );
    }

    protected function saveDailyExpenses(Request $request)
    {
      if ($request->btn_save=="save") {
        
         $dailyExpense = new DailyExpenseModel; 
         $dailyExpense->use_by = $request->useby;
         $dailyExpense->details = $request->details;
         $dailyExpense->amount = $request->amount;
         $dailyExpense->use_date = date('Y-m-d', strtotime($request->expensedate));
         $dailyExpense->category = $request->category; 
         $save = $dailyExpense->save();
         if ($save) {
          
            return redirect('/admin/accounts/add-expenditure')->with('err_msg','Expense information saved!');

         }

      }

      return redirect('/admin/accounts/add-expenditure');
    }



    protected function addCashDeposit(Request $request)
    {

      $businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->select()
      ->get();  

       $staffs =  DB::table("ba_profile")   
                  ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
                  ->where("category", "100" )
                  ->where("franchise", "0" )
                  ->where("ba_users.status", "<>", "100" )  
                  ->select("ba_profile.id","ba_profile.fullname" )
                  ->get();  


      return view('admin.accounts.daily_deposit_entry')->with("data", array("staffs" => $staffs, 'businesses' => $businesses) );
    }

    protected function saveCashDeposit(Request $request)
    {
      if ($request->btn_save=="save") {
         

        $deposit = new DailyDepositModel; 
        $deposit->deposit_from = $request->depositfrom;
        $deposit->deposit_by = $request->depositby;
        $deposit->bin = $request->bin;
        $deposit->details = ( $request->details == "" ? "Deposit log entry" : $request->details);
        $deposit->amount = $request->amount;
        $deposit->deposit_date =  date('Y-m-d', strtotime($request->depodate));
        $deposit->category = $request->category; 
        $save= $deposit->save();


         if ($save) {
          
            return redirect('/admin/accounts/add-deposit')->with('err_msg','Deposi information saved!');

         }

      }

      return redirect('/admin/accounts/add-deposit');
    } 




    protected function getBooktouAndStaff(Request $request)
    {

      if ($request->expensesby=="booktou") {

          $returnHTML='<input type="hidden" value="0" name="useby"/> ';
          return response()->json(array('success'=>true,'html'=>$returnHTML));
                 
      }elseif ($request->expensesby=="staff") {


                 $all_staffs =  DB::table("ba_profile")   
                  ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
                  ->where("category", "100" )
                  ->where("franchise", "0" )
                  ->where("ba_users.status", "<>", "100" )  
                  ->select("ba_profile.id","ba_profile.fullname" )
                  ->get();  


                $data = array('staff'=>$all_staffs);

                $returnHTML = view('admin.staffs.staff_list')->with($data)->render();
                return response()->json(array('success'=>true,'html'=>$returnHTML));
            }

    }

    protected function viewExpenditureReport(Request $request)
    {


      $month = ($request->month == "") ? date('m') : $request->month;
      $year = ($request->year == "") ? date('Y') : $request->year;

      $dailyExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  ) 
      ->orderBy("use_date", "asc")
      ->get();

      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  0 )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();


      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  ) 
      ->orderBy("deposit_date", "asc")
      ->get();

      $bins = array();
      foreach($all_deposits as $deposit)
      {
        $bins[] = $deposit->bin;
      }

      $all_businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->whereIn("id", $bins )
      ->select("id", "name", "locality")
      ->get();


      $data = array(
        'expense'=>$dailyExpense, 
        'all_staffs' => $all_staffs, 
        'deposits'=>$all_deposits, 
        'all_businesses' =>$all_businesses,
        'month'=>$month, 
        'year'=>$year);          

       return view('admin.accounts.deposit_expense_report')->with($data);
    }

 

 
    protected function viewAccountsDashboard(Request $request)
    {


      $month = ($request->month == "") ? date('m') : $request->month;
      $year = ($request->year == "") ? date('Y') : $request->year;

      $dailyExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  ) 
      ->orderBy("use_date", "asc")
      ->select(DB::Raw("sum(amount) as expensesAmount"))
      ->first();
 

      $totalAdvance = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  )
      ->where("category","advance") 
      ->orderBy("use_date", "asc")
      ->select(DB::Raw("sum(amount) as totalAdvance"))
      ->first();

      $totalReimbursement = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  ) 
      ->where("category","fuel") 
      ->orderBy("use_date", "asc")
      ->select(DB::Raw("sum(amount) as Totalreimbursement"))
      ->first();

      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  ) 
      ->orderBy("deposit_date", "asc")
      ->select(
                DB::Raw("sum(amount) as depositAmount"),
                DB::Raw("sum(due_amount) as dueAmount"),
                DB::Raw("sum(extra_amount) as extraAmount"))
      ->first();


      $cash_in_upi =  DB::table("ba_deposit_target")   
        ->whereMonth('verified_date', $month )
        ->whereYear('verified_date', $year  ) 
        ->select(DB::Raw("sum(verified_amount) as upiAmount"))
        ->first();


      $normal_orders= DB::table("ba_service_booking")
        ->whereMonth("ba_service_booking.service_date",$month)
        ->whereYear("ba_service_booking.service_date",$year)
        ->select("ba_service_booking.id as orderNo",
          "ba_service_booking.bin as bin",
                  "ba_service_booking.book_date as orderDate",
                  "ba_service_booking.total_cost as orderCost",
                  "ba_service_booking.delivery_charge as deliveryCharge",
                  "ba_service_booking.payment_type as paymentType",
                  "ba_service_booking.source",
                  "ba_service_booking.service_date as receiptDate",
                  "ba_service_booking.service_date as payDate",
                  "ba_service_booking.book_status as bookingStatus",
                  DB::Raw("'booktou' paymentTarget"),  
                  DB::Raw("'normal' orderType")); 



        $all_orders = DB::table("ba_pick_and_drop_order")
        ->whereMonth("ba_pick_and_drop_order.service_date",$month)
        ->whereYear("ba_pick_and_drop_order.service_date",$year)
        ->unionAll($normal_orders)
        ->select("ba_pick_and_drop_order.id as orderNo",
                "ba_pick_and_drop_order.request_by as bin",
                "ba_pick_and_drop_order.book_date as orderDate",
                "ba_pick_and_drop_order.total_amount as orderCost",
                "ba_pick_and_drop_order.service_fee as deliveryCharge",
                "ba_pick_and_drop_order.pay_mode as paymentType",
                "ba_pick_and_drop_order.source",
                "ba_pick_and_drop_order.service_date as receiptDate",
                "ba_pick_and_drop_order.service_date as payDate",
                "ba_pick_and_drop_order.book_status as bookingStatus",
                "ba_pick_and_drop_order.payment_target as paymentTarget",
                DB::Raw("'pnd' orderType")
              )

        ->orderBy('orderNo','asc')
        ->get();


      $data = array(
        'expense'=>$dailyExpense, 
        'reimbursement' => $totalReimbursement, 
        'deposits'=>$all_deposits, 
        'advance'=>$totalAdvance,
        'upi'=>$cash_in_upi,
        'all_orders'=>$all_orders,
        'month'=>$month, 
        'year'=>$year);          

       return view('admin.accounts.accounts_dashboard')->with($data);
    }


    public function eDailyExpense( Request $request)
    {

       
      if( $request->o  != ""  )
      {

       
        $expensedate = $request->o;

        $dailyExpense = DB::table('ba_daily_expense')
        ->join('ba_profile','ba_profile.id','=','ba_daily_expense.use_by')
        ->where('use_date',$expensedate)
        ->select()
        ->get();

         $month1 = date('m',strtotime($expensedate));
         $monthname = date('F', mktime(0, 0, 0, $month1, 10));
         $year = date('Y',strtotime($expensedate));

        
         
      }
      else
      {
        return;
        //return redirect("/services/new-bookings")->with("err_msg", "Failed to download invoice!"); 
      }
      
      $public_path = public_path();
      
      $folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $expensedate .  "/"     ;
      $folder_path =   $public_path  .  '/assets/bills/btm-' .  $expensedate .  "/"     ;
      
      if( !File::isDirectory($folder_path)){
            File::makeDirectory( $folder_path, 0777, true, true);
      }
      
      $pdf_file =  "btr-" . $request->o . ".pdf";
      $save_path = $folder_path .  $pdf_file  ;
      
      $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
      ->setPaper('a4', 'portrait')
      ->loadView('docs.ebill_daily_expense_slip' , 
        array('expense'=>$dailyExpense,'month'=>$monthname,'mnumber'=>$month1,'year'=>$year));

      return $pdf->download('ebill-' . $request->o  .   mt_rand(0,20)  .    '.pdf');
    }

    protected function getStaffLeaveDailyDetails(Request $request)
    { 


        $staff_id = $request->agentsName;
        $filter_date = date('Y-m-d');
        $month = date('m');
        $year = date('Y');
        $daily_expense =  DB::table("ba_daily_expense")   
                  ->where("use_by", $staff_id)
                  ->whereMonth("use_date", $month)
                  ->select(DB::Raw('sum(amount) as total_expense'))
                  ->first(); 

        $expense =  DB::table("ba_daily_expense")   
                  ->where("use_by", $staff_id)
                  ->whereMonth("use_date", $month)
                  ->select()
                  ->get(); 

        if ($daily_expense) {
          $total_expense = $daily_expense->total_expense;
        }else
        {
          $total_expense=0;
        }

        // calculating staff leave
        $staffleave = DB::table('ba_staff_attendance')
            ->where('staff_id',$staff_id)
            ->whereMonth('log_date',$month)
            ->whereYear('log_date',$year)
            ->where('staff_status','leave')
            ->select()
            ->get();


        $leave = DB::table('ba_staff_attendance')
            ->where('staff_id',$staff_id)
            ->whereMonth('log_date',$month)
            ->whereYear('log_date',$year)
            ->where('staff_status','leave')
            ->select(DB::Raw('COUNT(staff_id) as leaveDays'))
            ->first();
        $total_leave = $leave->leaveDays;
        if ($leave) {
          $total_leave = $leave->leaveDays;
        }else
        {
          $total_leave=0;
        }


        $expenseDetails=array('expense'=>$expense);
        $leaveDetails=array('staffleave'=>$staffleave);
        
        $returnExpenseview = view('admin.staffs.expense')->with($expenseDetails)->render();
        $returnLeaveview = view('admin.staffs.leave')->with($leaveDetails)->render();
            
        return response()->json(array('success'=>true,
          'daily'=>$total_expense,
          'leave'=>$total_leave,
          'html1'=>$returnExpenseview,
          'html2'=>$returnLeaveview 
          ));
    }

    protected function allOrderListReport(Request $request)
    {
      if (isset($request->bin)) 
      {
        
         $business= DB::table("ba_business")
              ->Where("id",$request->bin)
              ->first(); 

         $bin = $business->id;
         $name = $business->name;

         $month =date('m');
         $monthName = date('F', mktime(0, 0, 0, $month, 10));
         
         $normal_orders = DB::table('ba_service_booking')
          ->join('ba_order_sequencer','ba_service_booking.id','=', 'ba_order_sequencer.id')
          ->whereMonth("ba_service_booking.service_date",$month)
          ->where('bin',$bin)
          ->select("type","ba_order_sequencer.id as orderNo","ba_service_booking.total_cost as amount",
            "book_status","source as source","payment_type as ptype",
            "delivery_charge as deliverycharge",DB::Raw("'bookTou'  target_pay"));

   $pnd_orders = DB::table("ba_pick_and_drop_order")
          ->join('ba_order_sequencer','ba_pick_and_drop_order.id','=', 'ba_order_sequencer.id')
          ->whereMonth("ba_pick_and_drop_order.service_date",$month)
          ->where('request_by', $bin)
          ->unionAll($normal_orders)
           ->select("type","ba_order_sequencer.id as orderNo","ba_pick_and_drop_order.total_amount as amount", "book_status","source as source","pay_mode as ptype","service_fee as deliverycharge",
            "payment_target as target_pay"
          ) 
    ->get();
            
              $data = array('allresult'=>$pnd_orders,'business'=>$business,'name'=>$name,'monthname'=>$monthName);

         return view('admin.orders.all_order_list')->with('data',$data); 

      }else {

          return view('admin.orders.all_order_list');
      } 

    }


public function exportToExcel(Request $request) 
  {
        return Excel::download(new AdminDashboardExport($request->key), 'monthly_orders.xlsx');
  }


protected function viewClearanceReportMerchantwise(Request $request)
{ 

          if ($request->paymentid=="") {
            
              return view('admin/business/sales-and-clearance-history')->with('err_msg','mismatch information!');;
          }

          $id = $request->paymentid;

          $clearance = DB::table('ba_payment_dealings')
          ->where('id',$id)
          ->select()
          ->get();

        if ($clearance->isEmpty()) {
          
             return view('admin/business/sales-and-clearance-history')
             ->with('err_msg','No payment info found!');

        }else{
         
          

          foreach ($clearance as $clearPayment) {
                  
            $orderNo = explode(',', $clearPayment->order_nos);
            
          }


        }
       
        $pnd_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->whereIn("ba_pick_and_drop_order.id",$orderNo) 
        ->select( "ba_pick_and_drop_order.id",   
          "source",
          "book_date", 
          "clerance_status",
          "total_amount",
          "service_fee",
          "pay_mode",
          "book_status",
          "fullname as customerName", 
          "payment_target",
          "ba_business.commission",  
          DB::Raw("'na'  deliverBy"), 
          DB::Raw("'pnd' orderType"));

        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id") 
        ->whereIn("ba_service_booking.id",$orderNo)  
        ->select("ba_service_booking.id",   
                  "source",
                  "book_date", 
                  "clerance_status",
                  "seller_payable as total_amount",
                  "delivery_charge as service_fee",
                  "payment_type as pay_mode",
                  "book_status",
                  "customer_name as customerName" ,
                  DB::Raw("'booktou'  payment_target"),
                  "ba_business.commission",
                  DB::Raw("'na'  deliverBy"), 
                  DB::Raw("'normal' orderType"))
       ->union($pnd_list)
       ->orderBy('id','asc')
       ->get();
 

        $data = array('clearanceReport'=>$booking_list,'clearanceid'=>$id,'paid'=>$clearance);

        return view('admin.earning_clearance.sales_clearance_report_merchantwies')->with($data);

}



public function eClearanceReport( Request $request)
{

  
  if( $request->paymentid  != ""  )
  {
     
    
          $id = $request->paymentid;

          $clearance = DB::table('ba_payment_dealings')
          ->where('id',$id)
          ->select()
          ->get();

         

        if ($clearance->isEmpty()) {
          
             // return view('admin.earning_clearance.sales_clearance_report_merchantwies')
             // ->with('err_msg','No payment info found!');

        }else{
         
        
         
          foreach ($clearance as $clearPayment) {
                  
            $orderNo= explode(',', $clearPayment->order_nos);

           }

        
        
        }
 
         
        

        $pnd_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->whereIn("ba_pick_and_drop_order.id",$orderNo) 
        ->select( "ba_pick_and_drop_order.id",   
          "source",
          "book_date", 
          "clerance_status",
          "total_amount",
          "service_fee",
          "pay_mode",
          "book_status",
          "fullname as customerName", 
          "payment_target",
          "ba_business.commission",  
          DB::Raw("'na'  deliverBy"), 
          DB::Raw("'pnd' orderType"));

        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id") 
        ->whereIn("ba_service_booking.id",$orderNo)  
        ->select("ba_service_booking.id",   
                  "source",
                  "book_date", 
                  "clerance_status",
                  "seller_payable as total_amount",
                  "delivery_charge as service_fee",
                  "payment_type as pay_mode",
                  "book_status",
                  "customer_name as customerName" ,
                  DB::Raw("'booktou'  payment_target"),
                  "ba_business.commission",
                  DB::Raw("'na'  deliverBy"), 
                  DB::Raw("'normal' orderType"))
       ->union($pnd_list)
       ->orderBy('id','asc')
       ->get();




  }
  else
  {
    return;
    //return redirect("/services/new-bookings")->with("err_msg", "Failed to download invoice!"); 
  }
  
  $public_path = public_path();
  
  $folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $request->bin .  "/"     ;
  $folder_path =   $public_path  .  '/assets/bills/btm-' .  $request->bin .  "/"     ;
  
  if( !File::isDirectory($folder_path)){
        File::makeDirectory( $folder_path, 0777, true, true);
  }
  
  $pdf_file =  "btr-" . $request->bin . ".pdf";
  $save_path = $folder_path .  $pdf_file  ;
  
  $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
    ->setPaper('a4', 'portrait')
    ->loadView('docs.ebill_clearance' ,  array('data' => $booking_list,'clearanceid'=>$id,
    'paid'=>$clearance));


  return $pdf->download('ebill-' . $request->bin  .   mt_rand(0,20)  .    '.pdf');

  // return view('docs.ebill_clearance',  array('data' => $booking_list,'clearanceid'=>$id,'paid'=>$clearance));
}


protected function updatePanGstFssai(Request $request)
{

  if ($request->key=="") {
    return redirect("/admin/customer-care/business/view-profile/".$bin )
    ->with('err_msg',  "mismatch information!");
  }
  
  $bin =$request->key;

  $business = Business::find($bin);

   

  $business->pan_no = $request->panNo;
  $business->gstin = $request->gstNo;
  $business->fssai = $request->fssaiNo;

  $business->non_gst_enrolled = ( $request->enrollednongst == "on"  ) ? "yes" : "no";


  $save = $business->save();
  if ($save) {
    
     return redirect("/admin/customer-care/business/view-profile/".$bin )
     ->with('err_msg',  "information update sucessfull!");

  }
}
//admincontroller section ends part


protected function updateCommission(Request $request)
{
   
  if ($request->key==""   ) 
  {
    return redirect("/admin/customer-care/business/view-all" )->with('err_msg',  "Missing important field!");
  }
  
  if(  $request->comm ==""  || $request->pndcommission ==""|| $request->pndcommvalidfrom ==""|| $request->commvalidfrom =="" ) 
  {
    return redirect("/admin/customer-care/business/view-profile/".  $request->key )->with('err_msg',  "Missing important field!");
  }


  $bin =$request->key;
  $business = Business::find($bin);
  if( !isset($business))
  {
    return redirect("/admin/customer-care/business/view-all" )->with('err_msg',   "No business found!");
  }

  $comm = BusinessCommissionModel::where("bin", $bin)
  ->where("comm_status", "active")
  ->first() ;

  if( isset($comm) )
  {
    return redirect("/admin/customer-care/business/view-profile/".  $request->key )->with('err_msg',  "Commission actively in used cannot be edited!");
  }

  $comm = new BusinessCommissionModel;
  $comm->bin  =  $bin ;
  $comm->commission = $request->comm;
  $comm->pnd_commission  = $request->pndcommission;
  $comm->comm_effective_from  = date('Y-m-d', strtotime(  $request->commvalidfrom   ));
  $comm->pndcomm_effective_from  = date('Y-m-d', strtotime(  $request->pndcommvalidfrom )); 
  $comm->comm_status = ( $request->markactive == "on"  ) ? "active" : "unused";
  $comm->save();

  return redirect("/admin/customer-care/business/view-profile/" . $bin )->with('err_msg',  "information update sucessfull!");

  
}


protected function archiveActiveCommission(Request $request)
{
  
  if ($request->key== ""   ) 
  {
    return redirect("/admin/customer-care/business/view-all" )->with('err_msg',  "Missing important field!");
  }

  $bin =$request->key;
  $business = Business::find($bin);
  if( !isset($business))
  {
    return redirect("/admin/customer-care/business/view-all" )->with('err_msg',   "No business found!");
  }

  $business->commission = 0.00;
  $business->pnd_commission = 0.00;
  $business->save();

  $comm = BusinessCommissionModel::where("bin", $bin)
  ->where("comm_status", "active")
  ->first() ;

  if( isset($comm) )
  {
    $comm->comm_status ="archived";
    $comm->save();
  }

  return redirect("/admin/customer-care/business/view-profile/" . $bin )->with('err_msg',  "information update sucessfull!");
}


protected function updatePndComission(Request $request)
{
  
  if ($request->key=="" || $request->month ==""  || $request->year =="" ) 
  {
    return redirect("/admin/customer-care/business/view-all" )->with('err_msg',  "Missing important field!");
  }
  
  $bin =$request->key;
  $pnd_comm = BusinessCommissionModel::find($bin);
  if( !isset($pnd_comm)) 
  {
    return redirect("/admin/customer-care/business/view-all" )->with('err_msg',   "No business found!");
  }

  $comm = BusinessCommissionModel::where("bin", $bin)
  ->where("month", $request->month)
  ->where("year", $request->year)
  ->first() ;

  if( !isset($comm)) 
  {
    $comm = new BusinessCommissionModel;
    $comm->bin  =  $bin ;
  }

  $comm->pnd_commission  = $request->comm; 
  $comm->effective_from  = date('Y-m-d');
  $comm->month  = $request->month; 
  $comm->year   = $request->year;
  $comm->save();  
 
  return redirect("/admin/customer-care/business/view-profile/" . $bin )
  ->with('err_msg',  "information update sucessfull!");

  
}

  public function monthlySalesReportExporter(Request $request) 
  {
        if( $request->month  == "")
        { 
            $month   =  date('m');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
        else 
        {
          $month   =  $request->month ;
          $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
 

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }

        $all_orders = DB::table('ba_order_sequencer') 
        ->whereMonth('entry_date',  $month)
        ->whereYear('entry_date',  $year)
        ->paginate(500);


        $normal_ids = array(); 
        $pnd_ids = array(); 
        $assist_ids = array();
        $bins = array(); 

        foreach ($all_orders as $item) 
        {
          if($item->type == "normal")
            $normal_ids[] = $item->id;
          elseif($item->type == "pnd")
            $pnd_ids[] = $item->id;
          elseif($item->type == "assist")
            $assist_ids[] = $item->id; 
        }


        $normal_orders  = DB::table('ba_service_booking') 
        ->whereIn('id', $normal_ids )
        ->get();

        $pnd_orders  = DB::table('ba_pick_and_drop_order') 
        ->whereIn('id', $pnd_ids )
        ->get();

        $assist_orders  = DB::table('ba_pick_and_drop_order') 
        ->whereIn('id', $assist_ids )
        ->get();

        

        $first_part = DB::table('ba_business')  
        ->whereIn("id", function($query) use (  $month, $year ) { 
          $query->select("bin")
          ->from("ba_service_booking")
          ->whereMonth("book_date",   $month )
          ->whereYear("book_date",   $year ); 
        });


        $businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (  $month, $year ) { 
          $query->select("request_by")
          ->from("ba_pick_and_drop_order")
          ->where("request_from",  "business"  )
          ->whereMonth("book_date",   $month )
          ->whereYear("book_date",   $year ); 
        })
        ->union( $first_part  )
        ->get();


        
         
       
      //query ends here


        $data = array( 
          'month' =>  $monthname ,  
          'year' => $year ,
          'all_orders'=> $all_orders,
          'normal_orders'=> $normal_orders,
          'pnd_orders'=> $pnd_orders,
          'assist_orders'=> $assist_orders,
          'businesses' => $businesses,

           ); 


        return view('admin.reports.monthly_sales_report')->with($data);
  }


  protected function viewFeedback(Request $request)
  {


    if ($request->month=="") {
       $month = date('m');
    }else{
       $month = $request->month;
    }

    if ($request->year=="") {
       $year = date('Y');
    }else{
       $year = $request->year;
    }


    $orderno = array();

    if ($request->btnSearch=="search") {

          $feedback = DB::table('ba_complaint')
            ->join('ba_profile','ba_complaint.member_id','=','ba_profile.id')
            ->select("ba_complaint.*",
              "ba_profile.fullname",
              "ba_profile.phone",
              "ba_profile.locality",
              "ba_profile.landmark")
            ->whereMonth('posted_on',$month)
            ->whereYear('posted_on',$year)
            ->orderBy('id','desc')
            ->paginate(30);

            foreach ($feedback as $complaint) {
                $orderno[] =$complaint->order_no;
                }

                $order_info = DB::table('ba_order_sequencer')
                  ->whereIn('id',$orderno)
                  ->select("id","type")
                  ->get();

                 $complaint = DB::table('ba_complaint')
                 ->whereMonth('posted_on',$month)
                 ->whereYear('posted_on',$year)
                 ->select(DB::Raw('count(*) as total_complaint'))
                 ->first();

                  $no_reply = DB::table('ba_complaint')
                  ->whereNull('reply')
                  ->whereMonth('posted_on',$month)
                  ->whereYear('posted_on',$year)
                  ->select(DB::Raw('count(*) as not_reply'))
                  ->first();

                  $total_no_of_complaint = $complaint->total_complaint;
                  $total_no_reply = $no_reply->not_reply;

                $data = array('complaint'=>$feedback,'order_type'=>$order_info,
                'month'=>$month,'year'=>$year,'reply'=>$total_no_reply,
                'total_complaint'=>$total_no_of_complaint); 

                return view('admin.customers.feedback')->with($data);

      }
    
              $feedback = DB::table('ba_complaint')
              ->join('ba_profile','ba_complaint.member_id','=','ba_profile.id')
              ->select("ba_complaint.*",
                "ba_profile.fullname",
                "ba_profile.phone",
                "ba_profile.locality",
                "ba_profile.landmark")
              ->whereMonth('posted_on',$month)
              ->whereYear('posted_on',$year)
              ->orderBy('id','desc')
              ->paginate(30);

           

            foreach ($feedback as $complaint) {
              $orderno[] =$complaint->order_no;
            }

            $order_info = DB::table('ba_order_sequencer')
            ->whereIn('id',$orderno)
            ->select("id","type")
            ->get();

            //total complaint and no reply 


           $complaint = DB::table('ba_complaint')
           ->whereMonth('posted_on',$month)
           ->whereYear('posted_on',$year)
           ->select(DB::Raw('count(*) as total_complaint'))
           ->first();

           $no_reply = DB::table('ba_complaint')
            ->whereNull('reply')
            ->whereMonth('posted_on',$month)
            ->whereYear('posted_on',$year)
            ->select(DB::Raw('count(*) as not_reply'))
            ->first();

            $total_no_of_complaint = $complaint->total_complaint;
            $total_no_reply = $no_reply->not_reply;

           
    $data = array('complaint'=>$feedback,'order_type'=>$order_info,
                  'month'=>$month,'year'=>$year,'reply'=>$total_no_reply,
                  'total_complaint'=>$total_no_of_complaint);

    return view('admin.customers.feedback')->with($data);


  }


  protected function replyFromBookTou(Request $request)
  {


        if ($request->feedback_id=="") {
           return redirect('admin/customer/view-feedback')->with('err_msg','mismatch information!');
        }



        $id = $request->feedback_id;
        $complaint = ComplaintModel::find($id);

        $complaint->reply = $request->complaintReply;
        $complaint->replied_on = now();
        $complaint->replied_by = Session::get('__user_id_') ;

        $update = $complaint->save();

        if ($update) {
           return redirect('admin/customer/view-feedback');
        }
  }


protected function updatePaymentCycle(Request $request)
  {

    
    if ($request->bin=="") 
    {
      return redirect("/admin/customer-care/business/view-all" )->with('err_msg',  "Missing important field!");
    }
   
    $bin =$request->bin;


    $businessPayment = DB::table('ba_business_repayment')
    ->where('bin',$bin)
    ->select()
    ->first();

    if( !isset($businessPayment)) 
    {
       
        $businessPayment = new BusinessPaymentModel;

    } else{

        $id =  $businessPayment->id;

        $businessPayment = BusinessPaymentModel::find($id);
    }

        $businessPayment->bin = $request->bin; 
        $businessPayment->payment_mode = $request->pay_type; 
        $businessPayment->cycle = $request->payment_cycle; 

        $businessPayment->save();

    return redirect("/admin/customer-care/business/view-profile/" . $bin )
    ->with('err_msg',  "information update sucessfull!");

    
  }

   protected function viewListedBusinessesByDay(Request $request)
    {
     
      $bin= array();
      $month = date('m');
      $year = date('Y');
      $days = $request->day;

      $filter_date = date('Y-m-d',strtotime("7 days ago"));
      $status=['delivered'];
       

      
      $booking_list = DB::select('select sum(dueAmount) as total_due,bin  from
          ( select sum(total_amount) as dueAmount,
                  request_by as bin,
                  ba_business.name,
                  ba_business.shop_number,
                  ba_business.locality,
                  ba_business.landmark,
                  ba_business.city,
                  ba_business.state,
                  ba_business.pin,
                  ba_business.phone_pri,
                  ba_business.commission

                  from ba_pick_and_drop_order
                  join ba_business on ba_pick_and_drop_order.request_by=ba_business.id

                  where date(ba_pick_and_drop_order.book_date) <= "'.$filter_date.'"
                  and year(ba_pick_and_drop_order.book_date)="'.$year.'"
                  and clerance_status="un-paid"
                  and book_status= "delivered"
          group by bin
           
                  UNION


             select sum(seller_payable) as dueAmount,
                  bin,
                  ba_business.name,
                  ba_business.shop_number,
                  ba_business.locality,
                  ba_business.landmark,
                  ba_business.city,
                  ba_business.state,
                  ba_business.pin,
                  ba_business.phone_pri,
                  ba_business.commission

                  from ba_service_booking
                  join ba_business on ba_service_booking.bin=ba_business.id
                  
                  where date(ba_service_booking.book_date) <= "'.$filter_date.'"
                  and year(ba_service_booking.book_date)="'.$year.'"
                  and clerance_status="un-paid"
                  and book_status="delivered"   



            group by bin
            
          )npnd group by bin');



        

      
      foreach ($booking_list as $book) {
          $bin[]=$book->bin;
      }

      

      $business = DB::table('ba_business')
      ->whereIn('id',$bin)
      ->where('is_block','no')
      ->select()
      ->paginate(20);

       
 

       $data = array('results' => $business,'booking_list'=>$booking_list);
       return view("admin.business.unpaid_business_list")->with('data',$data);
    
  }



  protected function salesAndClearanceReportByDays(Request $request )
     {


        $bin = $request->bin;

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }

 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m');
        }

        $totalEarning = 0; 

        //payment cycle for merchant

          $payment_cycle = DB::table('ba_business_repayment')
          ->where('bin',$bin)
          ->select()
          ->first();

          $day = $payment_cycle->cycle;
           
           if ($day=="7") {
              $filter_date = date('Y-m-d',strtotime("7 days ago"));
           }else
           {
              $filter_date = date('Y-m-d',strtotime("10 days ago"));
           }
           
        //payment cycle ends here


        $pnd_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->where("ba_pick_and_drop_order.request_by", $bin) 
        ->whereRaw("date(ba_pick_and_drop_order.book_date)<= '$filter_date' ")
        ->whereYear('ba_pick_and_drop_order.book_date', $year )  
        ->where('clerance_status','un-paid')
        ->where('book_status','delivered')
        ->select( "ba_pick_and_drop_order.id",   
          "source",
          "book_date", 
          "clerance_status",
          "total_amount",
          "service_fee",
          "pay_mode",
          "book_status",
          "fullname as customerName", 
          "ba_business.name",  
          DB::Raw("'na'  deliverBy"), 
          DB::Raw("'pnd' orderType") ) ;

        $booking_list  = DB::table("ba_service_booking")
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id") 
        ->where("ba_service_booking.bin", $bin) 
        ->whereRaw("date(ba_service_booking.book_date)<= '$filter_date' ")
        ->whereYear('ba_service_booking.book_date', $year ) 
        ->where('clerance_status','un-paid')
        ->where('book_status','delivered') 
        ->select( "ba_service_booking.id",   
          "source",
          "book_date", 
          "clerance_status",
          "seller_payable as total_amount",
          "delivery_charge as service_fee",
          "payment_type as pay_mode",
          "book_status",
          "customer_name as customerName" , 
          "ba_business.name",   
          DB::Raw("'na'  deliverBy"), 
          DB::Raw("'normal' orderType") )
        ->union($pnd_list)
        ->get();

        
 
        $agentIds = array();
        $orderNo =array();

        foreach ($booking_list as $item) 
        {
          if( $item->book_status != "cancel_by_client" && $item->book_status != "cancel_by_owner" &&  
            $item->book_status != "canceled"  && $item->book_status != "returned")
          {
            $totalEarning += $item->total_amount;
          }
    
          $agentIds[] = $item->deliverBy;
          $orderNo[] = $item->id; 
        }

        $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        foreach ($booking_list as $item) 
        {
          foreach ($agentInfo as $aitem) 
          {
            if($item->deliverBy == $aitem->deliverBy )
            {
              $item->deliverBy= $aitem->fullname;break;
            } 
          }   
       }
 

       $business  = Business::find($bin);
       $data =  array("totalEarning" => $totalEarning, 'business' =>$business,  
        'results' =>  $booking_list, 'bin' => $bin );
       return view("admin.earning_clearance.unpaid_sales_by_days_clearance_report")->with('data',$data); 
     }
 
 
    protected function profileWarningStatusUpdate(Request $request)
     {

          if ($request->profileid=="") {
            return redirect('admin/systems/search-customers');
          }

          $id=$request->profileid;
          $profile = ProfileModel::find($id);

          
          $profile->flag_status = $request->statuslist;


          $banlist = new BanlistModel;
          $banlist->member_id=$profile->id;
          $banlist->name= $profile->fname;
          $banlist->phone=$profile->phone;
          $banlist->flag=$request->statuslist;
          $banlist->reason=$request->reason_for;

          $save = $banlist->save();

          $update = $profile->save();

          if ($save && $update) {
            
            return redirect("admin/customer/view-complete-profile/".$id);
          }
     }


     protected function orderDeliveredByMonth(Request $request)
     {
       
      $today = date('Y-m-d');

      if ($request->month!="") {

        $month = $request->month;
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }else{

        $month = date('m');
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }

      if ($request->year!="") {

        $year = $request->year;

      }else{

        $year = date('Y');
        
      }


        $all_agents  = DB::table("ba_profile")
        ->join("ba_staff_attendance", "ba_staff_attendance.staff_id", "=", "ba_profile.id")
        ->whereIn("ba_profile.id", function($query){
          $query->select("profile_id")->from("ba_users")
          ->where("category", '100' )
          ->where("status", '<>', '100' )
          ->where("franchise", 0 );

        })
        ->whereMonth("log_date",    $month  )
        ->whereYear("log_date",    $year  )
        ->select("ba_profile.id", "ba_profile.fullname", "ba_profile.phone")
        ->groupBy("ba_profile.id")
        ->get();

      //dd($all_agents);

      $memberid = array();
      foreach ($all_agents as $profile) {
         $memberid[] = $profile->id;
      }



       $normal_orders = DB::table("ba_delivery_order")
       ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_service_booking.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_service_booking.service_date", $month )
       ->whereYear("ba_service_booking.service_date", $year )        
       ->select(
        "ba_delivery_order.member_id as member_id", 

        DB::Raw(" 'normal' as type"),
        DB::Raw("count(ba_service_booking.id) as orderCount"),
        DB::Raw("sum(ba_service_booking.delivery_charge) as total_earned")

       )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
       ->get();

        

       $pnd_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereIn("request_from", array ('business', 'merchant') )
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
        "ba_delivery_order.member_id as member_id",
         
         DB::Raw(" 'pnd' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
	 DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned")
	 )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
        ->get();
        
       $assist_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereIn("request_from", array ('customer' ) )
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
        "ba_delivery_order.member_id as member_id",
         
         DB::Raw(" 'assist' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned")


        )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
        ->get();


        $merged = $normal_orders->merge($pnd_orders);
         
        $_orders = $merged->merge($assist_orders);

        $all_orders = $_orders->all();
        
        //dd($all_orders);
         
        $data = array(
          "agents" =>$all_agents, 
          'orders'=>$all_orders ,
          'month'=>$monthname,
          'year'=>$year,
          'mont'=>$month 
        );
        return view('admin.agents.delivery_report_by_agents_months')->with('data',$data);

     }

     protected function monthlyCommissionReport(Request $request)
     {
       
      $today = date('Y-m-d');

      if ($request->month!="") {

        $month = $request->month;
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }else{

        $month = date('m');
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }

      if ($request->year!="") {

        $year = $request->year;

      }else{

        $year = date('Y');
        
      }
      $month = 02;
      $memberid = $request->member;

      $agents  = DB::table("ba_profile") 
        ->select("ba_profile.id", "ba_profile.fullname", "ba_profile.phone")
        ->where("id",$memberid)
        ->first(); 
 

      $normal_orders = DB::table("ba_agent_earning")
       ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_agent_earning.order_no") 
       ->where('ba_agent_earning.member_id',$memberid)
       ->whereMonth("ba_service_booking.service_date", $month )
       ->whereYear("ba_service_booking.service_date", $year )        
       ->select(
        "ba_agent_earning.*",
        "ba_service_booking.total_cost as totalAmount",
        "ba_service_booking.service_date",
        DB::Raw("'' type")
       ) 
       ->get(); 

       $pnd_orders = DB::table("ba_agent_earning")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_agent_earning.order_no")
       ->where('ba_agent_earning.member_id',$memberid)
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
        "ba_agent_earning.*",
        "ba_pick_and_drop_order.service_fee as totalAmount",
        "ba_pick_and_drop_order.service_date",
        DB::Raw("'' type")
         )
       ->get(); 

        $orders = $normal_orders->merge($pnd_orders); 
        $all_orders = $orders->all();
         
        $orderid = array();
        foreach ($all_orders as $items) {
           $orderid[] = $items->order_no;
        }

        $type = DB::table('ba_delivery_order')
        ->whereIn('order_no',$orderid)
        ->get();



        foreach($all_orders as $items)
        {
           foreach($type as $typelist)
           {
              if ($items->order_no==$typelist->order_no) {
                $items->type= $typelist->order_type;
                break;
              }
           }
        }

        
        $data = array(
          "agents" =>$agents, 
          'orders'=>$all_orders ,
          'month'=>$monthname,
          'year'=>$year,
          'mont'=>$month 
        );
        return view('admin.agents.delivery_commission_report_for_agents_monthly')->with('data',$data);

     }

     protected function getDailyOrdersReportAgentWise(Request $request,$memberid)
     {

        if ($request->month!="") {

        $month = $request->month;
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }else{

        $month = date('m');
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }

      if ($request->year!="") {

        $year = $request->year;

      }else{

        $year = date('Y');
        
      }

        

       $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->where("ba_users.category", 100)
      ->where("ba_users.franchise",0)
      ->where("ba_users.status","<>",100)
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_profile.id",$memberid)
      ->select("ba_profile.*")
      ->first();


       $normal_orders = DB::table("ba_delivery_order")
       ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
       //->where("ba_service_booking.book_status","delivered")
       ->where('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_service_booking.service_date", $month )
       ->whereYear("ba_service_booking.service_date", $year )        
       ->select(
        DB::Raw("date(service_date) as serviceDate") , 
        DB::Raw(" 'normal' as type"),
        DB::Raw("count(ba_service_booking.id) as orderCount"),
        DB::Raw("sum(ba_service_booking.delivery_charge) as total_earned")
       )
       ->groupBy("serviceDate")
       ->get();



       $pnd_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
       ->whereIn("request_from", array ('business', 'merchant') )
       //->where("ba_pick_and_drop_order.book_status","delivered")
       ->where('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
         DB::Raw("date(service_date) as serviceDate"),
         DB::Raw(" 'pnd' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned") 
        )
       ->groupBy("serviceDate")
        
        ->get();

        $assist_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
       ->whereIn("request_from", array ('customer' ) )
       //->where("ba_pick_and_drop_order.book_status","delivered")
       ->where('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
         DB::Raw("date(service_date) as serviceDate"),
         DB::Raw(" 'assist' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned")
        )
       ->groupBy("serviceDate")
        
        ->get();

         

        $merged = $normal_orders->merge($pnd_orders);
         
        $_orders = $merged->merge($assist_orders);

        $all_orders = $_orders->all();

         
        $data = array(
          "agents" =>$all_agents, 
          'orders'=>$all_orders,
          'month'=>$monthname,
          'month_no'=>$month,
          'year'=>$year
        );


        return view('admin.agents.daily_order_delivery')->with('data',$data);
     }



     public function exportCommissionReport(Request $request) 
    
      {     
            if( $request->month   == "")
            { 
                $month = date('m');
            }
            else 
            {
              $month   =  $request->month; 
            }

            if( $request->year   == "")
            { 
                $year = date('Y');
            }
            else 
            {
              $year   =  $request->year; 
            }

            $memberid = $request->member;

            return Excel::download(new MonthlyCommissionExport($month,$year,$memberid), 'agents_commission_orders_count.xlsx');
      }

     public function agentsMonthlyOrdersExportToExcel(Request $request) 
    
    {
          

           
          if( $request->month   == "")
          { 
              $month = date('m');
          }
          else 
          {
            $month   =  $request->month; 
          }

          if( $request->year   == "")
          { 
              $year = date('Y');
          }
          else 
          {
            $year   =  $request->year; 
          }
          
          //echo $year;die();
           
       return Excel::download(new MonthlyAgentsOrdersExport($month,$year), 'agents_monthly_orders_count.xlsx');
    }

      
    public function agentsDailyOrdersExportToExcel(Request $request) 
    
    {
          

           
          if( $request->month   == "")
          { 
              $month = date('m');
          }
          else 
          {
            $month   =  $request->month; 
          }

          if( $request->year   == "")
          { 
              $year = date('Y');
          }
          else 
          {
            $year   =  $request->year; 
          }

          $memberid = $request->memberid;
          
          
          return Excel::download(new DailyAgentsOrdersExport($memberid,$month,$year), 'agents_daily_orders_count.xlsx');
    }


     //franchise orders details view

    protected function showAllFranchise(Request $request)
    {

      $all_franchise = DB::table('ba_franchise')
      ->join("ba_users", "ba_franchise.user_id", "=", "ba_users.id")
      ->where('current_status','active')
      ->select("ba_franchise.*", "ba_users.phone" )
      ->get();
      
      return view('admin.franchise_orders.all_franchise')->with('data',$all_franchise);


    }

    protected function franchiseDailyOrdersDetails(Request $request)
    {

        $franchise = DB::table('ba_franchise')
        ->where('current_status','active')
        ->select()
        ->get();


        if ($request->filter_date!="") {

          $today_date = $request->filter_date;
          
        }else
        {
          $today_date = date('Y-m-d');
        }

        $frno = $request->franchise;
        if ($frno!="") {

           

           $selected_franchise = DB::table("ba_franchise")
           ->where("ba_franchise.frno",$frno)
           ->first();

           $normal_orders = DB::table("ba_service_booking")
           ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
           ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
           ->whereDate("ba_service_booking.service_date", "$today_date" )
           ->where('ba_business.frno',$frno)
           ->select(
                    "ba_service_booking.id as orderNo",
                    "ba_service_booking.service_date as serviceDate",
                    "ba_service_booking.book_status as status",
                    "ba_service_booking.total_cost as amount", 
                    DB::Raw(" 'normal' as type")
          )
          ->get();

          $pnd_orders = DB::table("ba_pick_and_drop_order")
           ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
           ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
           ->whereIn("request_from", array ('business', 'merchant'))
           ->whereDate("ba_pick_and_drop_order.service_date", "$today_date" ) 
           ->where('ba_business.frno',$frno)
           ->select(
                        "ba_pick_and_drop_order.id as orderNo",
                        "ba_pick_and_drop_order.service_date as serviceDate",
                        "ba_pick_and_drop_order.book_status as status",
                        "ba_pick_and_drop_order.total_amount as amount", 
                        DB::Raw(" 'pnd' as type") 
            )
           ->get();

          $assist_orders = DB::table("ba_pick_and_drop_order")
           ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
           ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
           ->whereIn("request_from", array ('customer' ) )
           ->whereDate("ba_pick_and_drop_order.service_date", "$today_date" ) 
           
           ->whereIn("ba_profile.id", function($query) use($frno){
            $query->select("profile_id")->from("ba_users")
            ->where("franchise", $frno);

           })
           ->select(
                        "ba_pick_and_drop_order.id as orderNo",
                        "ba_pick_and_drop_order.service_date as serviceDate",
                        "ba_pick_and_drop_order.book_status as status",
                        "ba_pick_and_drop_order.total_amount as amount", 
                        DB::Raw(" 'assist' as type") 
            )
            ->get();

          $merged = $normal_orders->merge($pnd_orders);
         
          $_orders = $merged->merge($assist_orders);

          $all_orders = $_orders->all();

          $data = array('franchise'=>$franchise,
                        'orders'=>$all_orders,
                        'selected_franchise'=>$selected_franchise,'dated'=>$today_date);

          return view('admin.franchise_orders.franchise_daily_orders')->with('data',$data);
        }

        $data = array('franchise'=>$franchise);

        return view('admin.franchise_orders.franchise_daily_orders')->with('data',$data);

    }

    protected function franchiseMonthlyOrdersDetails(Request $request)
    {
        $franchise = DB::table('ba_franchise')
        ->where('current_status','active')
        ->select()
        ->get();


        if( $request->month   == "")
          { 
              $month = date('m');
              $monthname = date('F', mktime(0, 0, 0, $month, 10));
          }
          else 
          {
            $month   =  $request->month; 
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
          }

          if( $request->year   == "")
          { 
              $year = date('Y');
          }
          else 
          {
            $year   =  $request->year; 
          }

          $frno = $request->franchise;
          
          if ($frno!="") {

           $selected_franchise = DB::table("ba_franchise")
           ->where("ba_franchise.frno",$frno)
           ->first();

           $normal_orders = DB::table("ba_service_booking")
           ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
           ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
           ->whereMonth("ba_service_booking.service_date", $month ) 
           ->whereYear("ba_service_booking.service_date", $year )
           ->where('ba_business.frno',$frno)
           ->select(
                    "ba_service_booking.id as orderNo",
                    "ba_service_booking.service_date as serviceDate",
                    "ba_service_booking.book_status as status",
                    "ba_service_booking.total_cost as amount", 
                    DB::Raw(" 'normal' as type")
          )
          ->get();

          $pnd_orders = DB::table("ba_pick_and_drop_order")
           ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
           ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
           ->whereIn("request_from", array ('business', 'merchant'))
           ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
           ->whereYear("ba_pick_and_drop_order.service_date", $year )
           ->where('ba_business.frno',$frno)
           ->select(
                        "ba_pick_and_drop_order.id as orderNo",
                        "ba_pick_and_drop_order.service_date as serviceDate",
                        "ba_pick_and_drop_order.book_status as status",
                        "ba_pick_and_drop_order.total_amount as amount", 
                        DB::Raw(" 'pnd' as type") 
            )
           ->get();

          $assist_orders = DB::table("ba_pick_and_drop_order")
           ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
           ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
           ->whereIn("request_from", array ('customer' ) )
           ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
           ->whereYear("ba_pick_and_drop_order.service_date", $year )

           ->whereIn("ba_profile.id", function($query) use($frno){
            $query->select("profile_id")->from("ba_users")
            ->where("franchise", $frno);

           })
           ->select(
                        "ba_pick_and_drop_order.id as orderNo",
                        "ba_pick_and_drop_order.service_date as serviceDate",
                        "ba_pick_and_drop_order.book_status as status",
                        "ba_pick_and_drop_order.total_amount as amount", 
                        DB::Raw(" 'assist' as type") 
            )
            ->get();

             

          $merged = $normal_orders->merge($pnd_orders);
         
          $_orders = $merged->merge($assist_orders);

          $all_orders = $_orders->all();

          $data = array('franchise'=>$franchise,
                        'orders'=>$all_orders,
                        'selected_franchise'=>$selected_franchise,
                        'monthname'=>$monthname,
                        'year'=>$year);

          return view('admin.franchise_orders.franchise_monthly_orders')->with('data',$data);
        }




        $data = array('franchise'=>$franchise);
        return view('admin.franchise_orders.franchise_monthly_orders')->with('data',$data);
    }

    //


    protected function arrangeIcon(Request $request)
    {

      $biz_categories = DB::table("ba_business_category")
      ->where('is_active','yes')
      ->orderBy('display_order','asc')
      ->get();



      return view('admin.category.categories')->with('data',$biz_categories);
      
    }



    protected function updateCategoriesByDrag(Request $request)
    {

      $code = $request->code;
      $order_list = $request->orderlist;
      $new_index =$request->neworderlist;

      //echo $new_index.' '. $order_list;die();

      //section for old index

       $oldbizcategory = DB::table('ba_business_category')
              ->where('display_order', $new_index )
              ->update(['display_order'=>$order_list]);
              if (!$oldbizcategory) {
                 return redirect('/admin/arrange-icon');
              }
      //section for old index ends here

      //section for new index
      $category = BusinessCategory::find($code);
      $category->display_order = $new_index;
      $update = $category->save();

      //section for new index ends here

      if ($update) { 

        $biz_categories = DB::table("ba_business_category")
        ->where('is_active','yes')
        ->orderBy('display_order','asc')
        ->get();

        $data = array('data'=>$biz_categories);

        $returnHTML = view('admin.category.list_category')->with($data)->render();

        $msg = "record update successfully!";
        return response()->json(array('success' => true, 'err_msg' => $msg,'html'=>$returnHTML));
        
      }

    }


    protected function viewPndFutureOrders(Request $request)
    {
       if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)); 
        $todayDate = date('Y-m-d');
      }
      else 
      {
        $orderDate = date('Y-m-d'); 
        $todayDate = date('Y-m-d');
      }



      if( isset($request->btn_search) )
      {

        //change from here  
        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );

        $bin = $request->filter_business;
        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled',
                'order_packed','pickup_did_not_come','in_route', 'package_picked_up','delivery_scheduled' ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
          $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner','order_packed','pickup_did_not_come', 
            'delivered', 'cancelled',
            'in_route', 'package_picked_up','delivery_scheduled' ) ; 
        }
 
        if($bin == -1)
        {
           
          $pnd_orders  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) > '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "business" )  
        ->where("ba_business.frno",  0 )  
        //->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        ) 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date) = '$todayDate' desc")
        ->get();



        $assist_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) > '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)
        ->where("ba_pick_and_drop_order.request_from", "customer" )   
        //->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         
         "ba_profile.id as mid", 
         DB::Raw("'0' bin") ,
         "ba_profile.fullname as name", 
         "ba_profile.phone as primaryPhone", 
         "ba_profile.locality" ,
          DB::Raw("'assist' orderType")  
        ) 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date) = '$todayDate' desc")
        ->get();
 

        }
        else 
          
          {


        $pnd_orders  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) > '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status) 
        ->where("ba_business.id",$bin) 
        ->where("ba_pick_and_drop_order.request_from", "business" )  
        ->where("ba_business.frno",  0 )  
        //->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        ) 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date) = '$todayDate' desc")
        ->get();



        $assist_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) > '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)
        ->where("ba_pick_and_drop_order.request_from", "customer" )   
        //->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         
         "ba_profile.id as mid", 
         DB::Raw("'0' bin") ,
         "ba_profile.fullname as name", 
         "ba_profile.phone as primaryPhone", 
         "ba_profile.locality" ,
          DB::Raw("'assist' orderType")  
        ) 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date) = '$todayDate' desc")
        ->get();

 
             
          }
        

        $merged = $pnd_orders->merge($assist_orders);
        $day_bookings = $merged->all();

        //changes ends here for Sanatomba


      }
      else 
      {
        

        $request->session()->remove('_last_search_date' );
        $status = array( 'new','confirmed', 'cancel_by_client', 
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
        'package_picked_up','delivery_scheduled' ) ;  

        $pnd_orders  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) > '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "business" )  
        ->where("ba_business.frno",  0 )  
         
        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        ) 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date) = '$todayDate' desc")
        ->get();



        $assist_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) > '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)
        ->where("ba_pick_and_drop_order.request_from", "customer" ) 
        ->select("ba_pick_and_drop_order.*",  
         
         "ba_profile.id as mid", 
         DB::Raw("'0' bin") ,
         "ba_profile.fullname as name", 
         "ba_profile.phone as primaryPhone", 
         "ba_profile.locality" ,
          DB::Raw("'assist' orderType")  
        ) 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date) = '$todayDate' desc")
        ->get();


        $merged = $pnd_orders->merge($assist_orders);
        $day_bookings = $merged->all(); 

      }
       


      $today = date('Y-m-d'); 

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();

  


      $delivery_orders = DB::table("ba_delivery_order")
      ->whereDate("accepted_date",  $orderDate )
      ->where("completed_on", null )
      ->where("order_type", 'pnd' )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();

      $free_agents = array();

      //Fetching assigned orders
      $delivery_orders_list = DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
      ->select("ba_delivery_order.*", "ba_delivery_order.accepted_date", 
        "ba_delivery_order.id as aid", "ba_profile.fullname")
      ->whereDate("accepted_date", ">=", $orderDate) 
      ->get(); 
 
   
      foreach ($all_agents as $item)
      {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id  )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 80 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           } 


          //image compression
          if($item->profile_photo != null )
          {
            
            $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

            if(file_exists(  $source ) &&  !file_exists( $destination ))
            {
              $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 
 

      $requests_to_process  = DB::table("ba_order_process_request")  
      ->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
      ->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
      ->whereDate("log_date", $today ) 
      ->get(); 

  

      $customerphones = $merchants = array();
      foreach($day_bookings as $item)
      {
        $merchants[] = $item->bin;
        $customerphones[] = $item->phone;
      }
      $merchants[] = 0;

      $merchants = array_unique($merchants);

      $businesses  = DB::table("ba_business") 
      ->whereIn("id", $merchants ) 
      ->get(); 

  
      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->get(); 
      
      
      $all_staffs = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname",  "ba_users.id as user_id" ) 
      ->where("ba_users.category", ">", "100")
      ->where("ba_users.status", "<>" , "100")  
      ->get();
       

      $banned_list = DB::table("ba_ban_list")
      ->whereIn("phone",  $customerphones) 
      ->get();
 
 
       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  'staffs' =>$all_staffs,
        'delivery_orders_list' => $delivery_orders_list, 
        'results' => $day_bookings,  
        'requests_to_process' =>$requests_to_process,
        'all_business' => $all_business,
        'todays_business' => $businesses,
        'last_keyword' => $last_keyword,
        'last_keyword_date' =>  $last_keyword_date , 
        'banned_list' =>$banned_list,
        'refresh' => 'true'
      );


        return view("admin.orders.pick_and_drop_future_orders")->with('data',$data);

    }


    protected function SortPickandDropOrders(Request $request)
    {


        if($request->sortinglist==1)
        {
          $orderDate = date('Y-m-d'); 
          $todayDate = date('Y-m-d');
        }
        else 
        {
          $orderDate = date('Y-m-d' , strtotime("7 days ago")); 
          $todayDate = date('Y-m-d');
        }

       $status = array( 'new','confirmed', 'cancel_by_client', 
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
        'package_picked_up','delivery_scheduled' ) ;



        //future date section
        if ($request->sortinglist==1) {

        $pnd_orders  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) > '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "business" )  
        ->where("ba_business.frno",  0 )

         //->orderBy("ba_pick_and_drop_order.id", "desc")

        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        ) 
        ->orderBy("ba_pick_and_drop_order.service_date", "asc")
        ->get();



        $assist_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) > '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)
        ->where("ba_pick_and_drop_order.request_from", "customer" )   
        //->orderBy("ba_pick_and_drop_order.id", "desc")
       

        ->select("ba_pick_and_drop_order.*",  
         
         "ba_profile.id as mid", 
         DB::Raw("'0' bin") ,
         "ba_profile.fullname as name", 
         "ba_profile.phone as primaryPhone", 
         "ba_profile.locality" ,
          DB::Raw("'assist' orderType")  
        ) 
        ->orderBy("ba_pick_and_drop_order.service_date", "asc")
        ->get();
        
        }else


        //current date section
        {


        $pnd_orders  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "business" )  
        ->where("ba_business.frno",  0 )

        ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ") 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ")

        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        ) 
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->get();



        $assist_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)
        ->where("ba_pick_and_drop_order.request_from", "customer" )   
        
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ") 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ")

        ->select("ba_pick_and_drop_order.*",  
         
         "ba_profile.id as mid", 
         DB::Raw("'0' bin") ,
         "ba_profile.fullname as name", 
         "ba_profile.phone as primaryPhone", 
         "ba_profile.locality" ,
          DB::Raw("'assist' orderType")  
        ) 
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->get();
        


      }

      $merged = $pnd_orders->merge($assist_orders);
      $day_bookings = $merged->all();
      //sorting section data ends here

       

      $today = date('Y-m-d'); 

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();

  


      $delivery_orders = DB::table("ba_delivery_order")
      ->whereDate("accepted_date",  $orderDate )
      ->where("completed_on", null )
      ->where("order_type", 'pnd' )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();

      $free_agents = array();

      //Fetching assigned orders
      $delivery_orders_list = DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
      ->select("ba_delivery_order.*", "ba_delivery_order.accepted_date", 
        "ba_delivery_order.id as aid", "ba_profile.fullname")
      ->whereDate("accepted_date", ">=", $orderDate) 
      ->get(); 
 
   
      foreach ($all_agents as $item)
      {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id  )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 80 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           } 


          //image compression
          if($item->profile_photo != null )
          {
            
            $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

            if(file_exists(  $source ) &&  !file_exists( $destination ))
            {
              $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 
 

      $requests_to_process  = DB::table("ba_order_process_request")  
      ->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
      ->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
      ->whereDate("log_date", $today ) 
      ->get(); 

  

      $customerphones = $merchants = array();
      foreach($day_bookings as $item)
      {
        $merchants[] = $item->bin;
        $customerphones[] = $item->phone;
      }
      $merchants[] = 0;

      $merchants = array_unique($merchants);

      $businesses  = DB::table("ba_business") 
      ->whereIn("id", $merchants ) 
      ->get(); 

  
      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->get(); 
      
      
      $all_staffs = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname",  "ba_users.id as user_id" ) 
      ->where("ba_users.category", ">", "100")
      ->where("ba_users.status", "<>" , "100")  
      ->get();
       

      $banned_list = DB::table("ba_ban_list")
      ->whereIn("phone",  $customerphones) 
      ->get();
 
 
       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  'staffs' =>$all_staffs,
        'delivery_orders_list' => $delivery_orders_list, 
        'results' => $day_bookings,  
        'requests_to_process' =>$requests_to_process,
        'all_business' => $all_business,
        'todays_business' => $businesses,
        'last_keyword' => $last_keyword,
        'last_keyword_date' =>  $last_keyword_date , 
        'banned_list' =>$banned_list,
        'refresh' => 'true',
        'breadcrumb_menu' => 'pnd_sub_menu'
      );
       

        

       $returnHTML = view('admin.orders.sortedlist_pick_and_drop')->with('data',$data)->render();
       return response()->json(array('success'=>true,'html'=>$returnHTML));




    }

        


    protected function changeToFranchise(Request $request)
    {

        if ($request->routetoFrno!="") {

          $orderno = $request->routetoFrno;
          $frno = $request->franchise;


          $pnd = PickAndDropRequestModel::find($orderno);

          $pnd->route_to_frno = $frno;
          $update = $pnd->save();

          if ($update) {
             
            return redirect('admin/customer-care/pnd-order/view-details/'.$orderno);

          }
          
        }else{

          return redirect('customer-care/pick-and-drop-orders/view-all');
        }

    }
    

    // Get Delivery Agent Collection Report
    protected function saveAgentDailyCollectionReport(Request $request)
    {
       if(isset($request->date))
       {
          $today =   date('Y-m-d', strtotime($request->date));  
       }
       else 
       {
          $today =   date('Y-m-d');
       }

        $aid =0;
        if(isset($request->aid))
        {
            $aid =   $request->aid;  
        }
        else 
        {
          return redirect('/admin/delivery-agent/combined-daily-collection-report?filter_date=' . $today )
          ->with("err_msg", "No agent selected!");
        } 

        $oids =  $request->ordernos; 
        $oids[]=0;  
        $totalEarning = 0;  

        //updating normal order 
        DB::table("ba_service_booking") 
        ->whereIn("id", $oids ) 
        ->update(array("is_deposited" =>  "yes")); 
        DB::table("ba_pick_and_drop_order") 
        ->whereIn("id", $oids ) 
        ->update(array("is_deposited" =>  "yes"));  

        //logging deposit
        $deposit = new DailyDepositModel; 
        $deposit->deposit_from = "agent";
        $deposit->deposit_by = $aid  ;
        $deposit->bin = 0;
        $deposit->details = ( $request->details == "" ? "Deposit log entry" : $request->details);
        $deposit->amount = $request->totalcollected; 
        $deposit->due_amount = $request->dueamount;
        $deposit->extra_amount = $request->extraamount; 
        $deposit->deposit_date =  $today;
        $deposit->category = "daily collection deposit"; 
        $deposit->save();


        foreach($oids as $oid)
        {
            $depositlog = new DepositTargetModel;
            $depositlog->order_no = $oid;
            $depositlog->ac_name = $request->acname;
            $depositlog->save();
        }
        

        return redirect('/admin/delivery-agent/daily-collection-report?aid='. $aid . '&filter_date=' . $today )->with("err_msg", "Daily collection deposited!");

     }


  //route order to franchise
  protected function routeOrderToFranchise(Request $request)
  {

      if(   $request->remarks == "" ||  $request->frno  == ""  ||  $request->orderno   == ""  )
      {

        return redirect("/admin/customer-care/pick-and-drop-orders/view-all" )
        ->with("err_msg", "Important fields are missing!"); 
      }

      $order_seq = OrderSequencerModel::find($request->orderno);

      if( !isset($order_seq) )
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all" )
        ->with("err_msg", "No matching order found!"); 
      }  

      $redirect_url ="/admin";
      if($order_seq->type == "normal" || $order_seq->type == "booking")
      {
        $order_info = ServiceBooking::find( $request->orderno ) ;
        $redirect_url ="/admin/customer-care/order/view-details/" .  $request->orderno ;
      }
      else if($order_seq->type == "pnd" || $order_seq->type == "assist")
      {
        $order_info = PickAndDropRequestModel::find( $request->orderno ) ;
        $redirect_url ="/admin/customer-care/pnd-order/view-details/" .  $request->orderno ;

      }else 
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all" )
        ->with("err_msg", "No matching order found!"); 
      }

      if( !isset($order_info) )
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all" )
        ->with("err_msg", "No order information found!"); 
      }  

      $order_info->route_to_frno = $request->frno;
      $order_info->save(); 

      //save remarks
      $remark = new OrderRemarksModel();
      $remark->order_no = $request->orderno ;
      $remark->remarks =  $request->remarks == "" ? "order migration"  : $request->remarks  ;
      $remark->rem_type = "CC Remarks";  
      $remark->remark_by = $request->session()->get('__member_id_' ); 
      $remark->save(); 

      return redirect( $redirect_url )->with("err_msg",  'Order migrated successfully!'  );  
      
  }

  
  protected function updateAssistOrderPaymentUpdate(Request $request)
  {

      $orderno = $request->orderno;
      if($orderno == "")
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all"); 
      }

      if( $request->delivery == "" || $request->paymode == "" || $request->orderdetails == "")
      {
        return redirect("/admin/customer-care/assist-order/view-details/" . $orderno )
        ->with("err_msg", "Assist order info info missing!"); 
      }

      $order_info   = PickAndDropRequestModel::find( $orderno) ;
      if( !isset($order_info))
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all"  )
        ->with("err_msg", "No matching order found!"); 
      }

      $paymode =  $request->paymode ;  

      if( date('d-m-Y', strtotime($request->servicedate)) != date('d-m-Y', strtotime($order_info->service_date))  )
      {
        $order_info->service_date =  date('d-m-Y', strtotime($request->servicedate)) ;  
      } 
 
      $order_info->pickup_details = $request->orderdetails; 
      $order_info->pay_mode  =  $paymode ; 

        if($request->total != "" )
        {
          $order_info->total_amount =  $request->total ;  
        }
        
        if($request->delivery != "" )
        {
          $order_info->requested_fee  =  $order_info->service_fee = $request->delivery;  
        }

        if($request->packcost != "" )
        {
          $order_info->packaging_cost  =  $request->packcost;  
        }

        $order_info->save();  


        //save remarks 
        $remark = new OrderRemarksModel();
        $remark->order_no =  $orderno ;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "assist order details update" ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();
        return redirect("/admin/customer-care/assist-order/view-details/" . $orderno )
        ->with("err_msg", "Order details updated!");   
      
}

    protected function updateBusinessPhoneNo(Request $request)
    {
      //parameter missing check
      if ($request->bin=="" || $request->n_phoneNo =="" || $request->oldphone_no == "" ) 
      {
        return redirect("/admin/customer-care/business/view-profile/" . $request->bin  )->with('err_msg',  "Missing important field!");
      }

      $bin =$request->bin;
      
      //search for the merchant
      $business = Business::find($bin); 
      if( !isset($business)) 
      {
        return redirect("/admin/customer-care/business/view-all" )->with('err_msg',   "No business found!");
      }


      //searching existing account
      $user_info = User::where("category", 1)
      ->where("phone", $request->oldphone_no)
      ->first();

      if ($user_info)
      {
        //searching any other user with the new phone number in profile
        $similar_account = User::where("phone", $request->n_phoneNo)
        ->first();

        //merchant with same phone found. Block phone update
        if ( isset($similar_account) ) 
        {
          return redirect("/admin/customer-care/business/view-profile/" . $bin )
          ->with('err_msg',   "Phone no. update block, there is another merchant registered with the same phone number!");
        }

        //merchant with new phone not found. Allow phone update in user table

        //phone update in account table
        $user_info->phone = $request->n_phoneNo;
        $user_info->save();

        //phone update in merchant profile
        $business->phone_pri = $request->n_phoneNo; 
        $business->phone_alt = $request->n_phoneNo;
        $business->save();

        //reload profile page
        return redirect("/admin/customer-care/business/view-profile/" . $bin )
        ->with('err_msg',  "phone no update sucessfull!");

      }
      else
      {
        return redirect("/admin/customer-care/business/view-all" )->with('err_msg',  "No matching business found!"); 
      }

  }



  protected function updateVerificationTick(Request $request)
  {
      //parameter missing check
      if ($request->bin=="" || $request->verifiedtick ==""   ) 
      {
        return redirect("/admin/customer-care/business/view-profile/" . $request->bin  )->with('err_msg',  "Missing important field!");
      }

      $bin =$request->bin;
      
      //search for the merchant
      $business = Business::find($bin); 
      if( !isset($business)) 
      {
        return redirect("/admin/customer-care/business/view-all" )->with('err_msg',   "No business found!");
      }

      //phone update in merchant profile
      $business->is_verified =  $request->verifiedtick ;
      $business->save();

      //reload profile page
      return redirect("/admin/customer-care/business/view-profile/" . $bin )
        ->with('err_msg',  "Verification tick updated sucessfully!");

      

  }

protected function updatePosMerchant(Request $request)
    {
      //parameter missing check
      if ($request->bin=="" || $request->pos_merchant ==""   ) 
      {
        return redirect("/admin/customer-care/business/view-profile/" . $request->bin  )->with('err_msg',  "Missing important field!");
      }

      $bin =$request->bin;
      
      //search for the merchant
      $business = Business::find($bin); 
      if( !isset($business)) 
      {
        return redirect("/admin/customer-care/business/view-all" )->with('err_msg',   "No business found!");
      }

      //phone update in merchant profile
      $business->pos_merchant =  $request->pos_merchant ;
      $business->save();

      $subscription = PosSubscription::find($business->bin);

      if (!$subscription) {
          $subscription = new PosSubscription; 
      }

      $subscription->bin= $request->bin;
      $subscription->start_date = date('Y-m-d',strtotime($request->subscription_date));
      $subscription->subscription_amount = $request->subscription_amount;
      $subscription->status = 1;
      $save = $subscription->save();

      //reload profile page
      return redirect("/admin/customer-care/business/view-profile/" . $bin )
        ->with('err_msg',  "subscribe for pos!");
    }

    protected function updatePosPayment(Request $request)
    {
      //parameter missing check
      if ($request->bin=="" || $request->amount =="" || $request->month =="" || $request->year =="") 
      {
        return redirect("/admin/customer-care/business/view-profile/" . $request->bin  )->with('err_msg',  "Missing important field!");
      }

      $bin =$request->bin; 
      //search for the merchant
      $business = Business::find($bin); 
      if( !isset($business)) 
      {
        return redirect("/admin/customer-care/business/view-profile/" . $request->bin  )->with('err_msg',  "No business found!");
      } 
      $month = $request->month;
      $year = $request->year;

      $payment = DB::table('ba_pos_payment')
      ->where('month',$month)
      ->where('year',$year)
      ->where('bin',$bin)
      ->first();

      if (isset($payment)) {
         return redirect("/admin/customer-care/business/view-profile/" . $request->bin  )->with('err_msg',  "already made payment for selected month!");
      }

      $pos_payment = new PosPayment;

      $pos_payment->bin= $request->bin;
      $pos_payment->pay_date = date('Y-m-d');
      $pos_payment->amount = $request->amount;
      $pos_payment->remarks = $request->remarks;
      $pos_payment->month = $request->month;
      $pos_payment->year = $request->year;

      $save = $pos_payment->save();

      //reload profile page
      return redirect("/admin/customer-care/business/view-profile/" . $bin )
        ->with('err_msg',  "payment made successfull");
    }
    

    protected function enableSelfPickupService(Request $request)
    {
        //parameter missing check
      if ($request->bin=="" || $request->selfpickupservice ==""   ) 
      {
        return redirect("/admin/customer-care/business/view-profile/" . $request->bin  )->with('err_msg',  "Missing important field!");
      }

      $bin =$request->bin;
      
      //search for the merchant
      $business = Business::find($bin); 
      if( !isset($business)) 
      {
        return redirect("/admin/customer-care/business/view-all" )->with('err_msg',   "No business found!");
      }

      //$pickup = SelfPickUpModel::where('bin',$bin)->first();
      // echo $pickup->id;die();
      //if (isset($pickup)) {
        // $selfpickup = SelfPickUpModel::find($pickup->id);
      //} 

      //if (!isset($selfpickup)) {
         // $selfpickup = new SelfPickUpModel; 
      //}

      //$selfpickup->bin= $request->bin;
      //$selfpickup->status = $request->selfpickupservice;
      
      //$save = $selfpickup->save();
      $business->allow_self_pickup = $request->selfpickupservice; 
      $business->save();  

      //reload profile page
      return redirect("/admin/customer-care/business/view-profile/".$bin)
        ->with('err_msg',"self pickup service enable!");

    }

    protected function updateCoupon(Request $request)
    {

       $voucherid = $request->voucher;
       $coupon = CouponModel::find($voucherid);

       if ($coupon) {

           $coupon->code = $request->voucherCode; 
           $coupon->bin = $request->business;
           $coupon->biz_category = $request->category; 
           $coupon->total_discount = $request->discountSeller;
           $coupon->product_discount_pc = $request->discountSeller;
           $coupon->valid_from = date('Y-m-d', strtotime($request->validdate));
           $coupon->ends_on = date('Y-m-d', strtotime($request->enddate));


           $coupon->start_time = date('H:i:s', strtotime($request->validtime));
           $coupon->end_time = date('H:i:s', strtotime($request->validendtime));

           $coupon->description =  $coupon->validity_text = $request->description;
           $save = $coupon->save();

          if($save)
           {
              $msg = "Voucher code is saved!";
           }
           else 
           {
            $msg = "Voucher code could not be saved!";
           } 
      
          return redirect("/admin/systems/manage-voucher-codes")->with("err_msg", $msg);
       }else{
          $msg = "Voucher code update failed!";
          return redirect("/admin/systems/manage-voucher-codes")->with("err_msg", $msg);
       }
       

      
    }


    protected function removeAttendance(Request $request)
    {

        if ($request->logId=="") {
          return redirect("/admin/staffs/manage-attendance");
        }

         
        $delete =  DB::table("ba_staff_attendance")
        ->where("staff_id", $request->logId)
        ->where("log_date", date('Y-m-d'))
        ->delete(); 


        if ($delete) {
          
          return redirect("/admin/staffs/manage-attendance");

        } else{
          return redirect("/");
        }


    }


 
     // Get Delivery Agent Collection Report
    protected function deliveryAgentMonthlyCollectionReport(Request $request)
    {
       if(isset($request->month))
       {
          $month  =   $request->month ;  
        }
        else 
        {
          $month =   date('m');   
        }

        if(isset($request->year))
       {
          $year  =   $request->year ;  
        }
        else 
        {
          $year =   date('Y');   
        }

        $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 3;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                $startDay   =  1 ;  
                $endDay   =    7;
                $cycle = 1;
            } 
        } 
  
 
        $aid =0;
        if(isset($request->aid))
        {
            $aid =   $request->aid;  
        }
        else 
        { 
          return redirect('/admin/delivery-agents' )->with("err_msg", "No agent selected!"); 
        }
 
        
        $totalEarning = 0; 
        $agent_profile = DB::table("ba_profile") 
        ->where("ba_profile.id", $aid ) 
        ->select("ba_profile.id", "ba_profile.fullname","ba_profile.phone")
        ->first(); 
  
      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid ) 
        ->whereMonth('ba_service_booking.service_date', $month)  
        ->whereYear('ba_service_booking.service_date', $year)  
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay ) 
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            DB::Raw("'customer' source"),
            "ba_service_booking.service_date",
            "ba_service_booking.preferred_time as delivery_time",
            'book_status as orderStatus',
            'seller_payable as totalAmount',
            'delivery_charge as serviceFee',
            "payment_type as payMode",
             DB::Raw("'booktou' paymentTarget"),
            DB::Raw("'normal' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        ) ; 


        $assist_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no") 
        ->where("ba_delivery_order.member_id", $aid ) 
        ->whereMonth('ba_pick_and_drop_order.service_date', $month)  
        ->whereYear('ba_pick_and_drop_order.service_date', $year) 
         ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )  
        ->whereIn("request_from", array ('customer' ) )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'assist' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        ) ; 

        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $aid ) 
        ->whereMonth('ba_pick_and_drop_order.service_date', $month)  
        ->whereYear('ba_pick_and_drop_order.service_date', $year)  
         ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay ) 
        ->whereIn("request_from", array ('business', 'merchant') ) 
        ->union( $normalorders )
        ->union( $assist_orders )        
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "source",
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            "payment_target as paymentTarget",
            DB::Raw("'pnd' orderType"),
            "ba_delivery_order.member_id",
            "is_deposited as isDeposited"
        )
        ->orderBy("orderNo" )
        ->get();
  

        $oids =array();
        $merchantIds =array();
        $customerIds =array();
        $toalamount =0.00;
        foreach ($all_orders as $item) 
        {
          $oids[] = $item->orderNo;
          $toalamount += $item->totalAmount + $item->serviceFee;
          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }
 
        $oids[] =$customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;
                    break;
                }

            }
        }
                 
       }
       //commissions
       $order_commissions  = DB::table("ba_agent_earning")
        ->where("member_id", $aid  )
        ->whereIn("order_no",   $oids) 
        ->get();

 
       $data = array(  
        'cycle' => $cycle,
        'month' =>  $month , 
        'year' => $year,
        "total_amount" => $toalamount,  
        'agent_info' =>  $agent_profile,  
        'all_orders' =>  $all_orders,
        'order_commissions' => $order_commissions );
       return view("admin.agents.weekly_collection")->with('data',$data); 

     }


     protected function saveAgentMonthlyCollectionReport(Request $request)
    {
       if(isset($request->date))
       {
          $today =   date('Y-m-d', strtotime($request->date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }

        $aid =0;
        if(isset($request->aid))
        {
            $aid =   $request->aid;  
        }
        else 
        {
          return redirect('/admin/delivery-agent/combined-daily-collection-report?filter_date=' . $today )
          ->with("err_msg", "No agent selected!");
        }


        $oids =  $request->ordernos; 
        $oids[]=0;
        

      

        $totalEarning = 0;  

        //updating normal order 
        DB::table("ba_service_booking") 
        ->whereIn("id", $oids ) 
        ->update(array("is_deposited" =>  "yes")); 
        DB::table("ba_pick_and_drop_order") 
        ->whereIn("id", $oids ) 
        ->update(array("is_deposited" =>  "yes"));  

        

       return redirect('/admin/delivery-agent/monthly-collection-report?aid='. $aid . '&filter_date=' . $today )
       ->with("err_msg", "Daily collection deposited!"); 

     }


     protected function dailyDueMerchantwise(Request $request)
      {

        //$endmonth =12;
        $year = $request->year == "" ?  date('Y')  : $request->year;  
        $today = $request->reportDate == "" ?  date('Y-m-d')  : date('Y-m-d',strtotime($request->reportDate)); 

        $frno = $request->frno == "" ? 0 : $request->frno; 


        if ($frno==0) {

        $income = DB::table("ba_service_booking")  
        ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin") 
         ->whereRaw("year(service_date)= '$year'") 
         ->where("service_date",$today) 
         ->where("ba_business.frno",$frno)
         ->where("ba_service_booking.route_to_frno",$frno)  
         ->where("book_status", "delivered")
         ->groupBy("bin")
         ->select(
          "bin", 
          "ba_business.name as businessName",
          "ba_business.frno as frno",
          DB::Raw("'normal' orderType"), 
          DB::Raw("sum(seller_payable) as totalSale"), 
          DB::Raw("sum(delivery_charge) as deliveryCommission"))
         //->get()
         ;

           

         $all_income = DB::table("ba_pick_and_drop_order")
         ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")   
         ->whereRaw("year(service_date)= '$year'") 
         ->where("service_date",$today)
         ->where("ba_business.frno",$frno)
         ->where("ba_pick_and_drop_order.route_to_frno",$frno)
         ->where("book_status", "delivered")
         ->whereIn("request_from", array ('business', 'merchant') ) 
         ->groupBy("bin")
         ->union($income)
          ->select(
          DB::Raw("request_by as bin"), 
          "ba_business.name as businessName",
          "ba_business.frno as frno",
          DB::Raw("'pnd' orderType"),  
          DB::Raw("sum(total_amount) as totalSale"), 
          DB::Raw("sum(service_fee) as deliveryCommission"))
          ->get();

        }else{

          //if franchise is not headquarter get the result of normal and pnd according to field route_to_frno

         $normal_route_to = DB::table("ba_service_booking")  
        ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin") 
         ->whereRaw("year(service_date)= '$year'") 
         ->where("service_date",$today) 
         ->where("ba_service_booking.route_to_frno",$frno) 
         ->where("book_status", "delivered")
         ->groupBy("bin")
         ->select(
          "bin", 
          "ba_business.name as businessName",
          "ba_business.frno as frno",
          DB::Raw("'normal' orderType"), 
          DB::Raw("sum(seller_payable) as totalSale"), 
          DB::Raw("sum(delivery_charge) as deliveryCommission"))
         //->get()
         ;
 

        $income = DB::table("ba_service_booking")  
        ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin") 
         ->whereRaw("year(service_date)= '$year'") 
         ->where("service_date",$today) 
         ->where("ba_business.frno",$frno) 
         ->where("book_status", "delivered")
         ->groupBy("bin")
         ->select(
          "bin", 
          "ba_business.name as businessName",
          "ba_business.frno as frno",
          DB::Raw("'normal' orderType"), 
          DB::Raw("sum(seller_payable) as totalSale"), 
          DB::Raw("sum(delivery_charge) as deliveryCommission"))
         //->get()
         ;


         $pnd_route_to = DB::table("ba_pick_and_drop_order")
         ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")   
         ->whereRaw("year(service_date)= '$year'") 
         ->where("service_date",$today)
         ->where("ba_pick_and_drop_order.route_to_frno",$frno)
         ->where("book_status", "delivered")
         ->whereIn("request_from", array ('business', 'merchant') ) 
         ->groupBy("bin")
          ->select(
          DB::Raw("request_by as bin"), 
          "ba_business.name as businessName",
          "ba_business.frno as frno",
          DB::Raw("'pnd' orderType"),  
          DB::Raw("sum(total_amount) as totalSale"), 
          DB::Raw("sum(service_fee) as deliveryCommission"));

         $all_income = DB::table("ba_pick_and_drop_order")
         ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")   
         ->whereRaw("year(service_date)= '$year'") 
         ->where("service_date",$today)
         ->where("ba_business.frno",$frno)
         ->where("book_status", "delivered")
         ->whereIn("request_from", array ('business', 'merchant') ) 
         ->groupBy("bin")
         ->union($income)
         ->union($normal_route_to)
         ->union($pnd_route_to)
          ->select(
          DB::Raw("request_by as bin"), 
          "ba_business.name as businessName",
          "ba_business.frno as frno",
          DB::Raw("'pnd' orderType"),  
          DB::Raw("sum(total_amount) as totalSale"), 
          DB::Raw("sum(service_fee) as deliveryCommission"))
          ->get();


        }
        
        

          $bin = array();
          $frnos = array();
          foreach ($all_income as $items) {
             $bin[] = $items->bin;
             $frnos[] = $items->frno;
          }

          $business_franchise = DB::table("ba_franchise")
          ->whereIn("ba_franchise.frno",$frnos)
          ->select(
             
            "ba_franchise.frno",
            "ba_franchise.zone as franchiseName"
          )
          ->get();

          $franchise = DB::table("ba_franchise")
          ->select()
          ->get();
 
         
       

     $data = array(   'year' =>  $year,"today"=>$today,"fr_nos"=>$frno,"allincome"=>$all_income ,
                      "franchise"=>$business_franchise,"fr_list"=>$franchise); 

     return view("admin.reports.merchant_wise_income_report")->with('data', $data); 

  }



  protected function viewDepositsDetails(Request $request)
    {


      $month = date('m');
      $year = date('Y');

      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  ) 
      ->orderBy("deposit_date", "asc")
      ->get();


      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  0 )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

      $bins = array();
      foreach($all_deposits as $deposit)
      {
        $bins[] = $deposit->bin;
      }

      $all_businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->whereIn("id", $bins )
      ->select("id", "name", "locality")
      ->get();


      $data = array(
         
        'deposits'=>$all_deposits, 
        'all_staffs'=>$all_staffs,
        'all_businesses'=>$all_businesses,
        'month'=>$month, 
        'year'=>$year);          


       return view('admin.accounts.view_all_deposits')->with($data);
    }



    protected function viewExpensesDetails(Request $request)
    {


      $month = date('m');
      $year = date('Y');

      $dailyExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  ) 
      ->orderBy("use_date", "asc")
      ->get();


      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  0 )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

       

      $data = array(
         
        'expense'=>$dailyExpense, 
        'all_staffs'=>$all_staffs,
        //'all_businesses'=>$all_businesses,
        'month'=>$month, 
        'year'=>$year);          


       return view('admin.accounts.view_all_expenses')->with($data);
    }


    protected function viewAdvanceDetails(Request $request)
    {


      $month = date('m');
      $year = date('Y');

      $advanceExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  )
      ->where("category","advance") 
      ->orderBy("use_date", "asc")
      ->get();


      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  0 )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

       

      $data = array(
         
        'advance'=>$advanceExpense, 
        'all_staffs'=>$all_staffs,
        'month'=>$month, 
        'year'=>$year);          


       return view('admin.accounts.view_all_advance_or_expenses')->with($data);
    }

    protected function viewReimbursementDetails(Request $request)
    {


      $month = date('m');
      $year = date('Y');

      $fuelExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  )
      ->where("category","fuel") 
      ->orderBy("use_date", "asc")
      ->get();


      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  0 )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

       

      $data = array(
         
        'fuelexpense'=>$fuelExpense, 
        'all_staffs'=>$all_staffs,
        'month'=>$month, 
        'year'=>$year);          


       return view('admin.accounts.view_all_advance_or_expenses')->with($data);
    }

    protected function viewCashInUPIDetails(Request $request)
    {


      $month = date('m');
      $year = date('Y');

      $all_deposits_target = DB::table('ba_deposit_target') 
      ->whereMonth('verified_date', $month )
      ->whereYear('verified_date', $year  )  
      ->get() ;
       

      $data = array( 'cash_upi'=>$all_deposits_target,  
                     'month'=>$month, 
                     'year'=>$year);          


      return view('admin.accounts.view_all_deposit_target')->with($data);
    }

    protected function viewDepositsExtraDetails(Request $request)
    {


      $month = date('m');
      $year = date('Y');

      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  )
      ->whereRaw("extra_amount > 0") 
      ->orderBy("deposit_date", "asc")
      ->get();


      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  0 )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

      $bins = array();
      foreach($all_deposits as $deposit)
      {
        $bins[] = $deposit->bin;
      }

      $all_businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->whereIn("id", $bins )
      ->select("id", "name", "locality")
      ->get();


      $data = array(
         
        'deposits'=>$all_deposits, 
        'all_staffs'=>$all_staffs,
        'all_businesses'=>$all_businesses,
        'month'=>$month, 
        'year'=>$year);          


       return view('admin.accounts.view_deposit_extra')->with($data);
    }


    protected function viewDepositsDueDetails(Request $request)
    {


      $month = date('m');
      $year = date('Y');

      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  )
      ->whereRaw("due_amount > 0") 
      ->orderBy("deposit_date", "asc")
      ->get();


      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  0 )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

      $bins = array();
      foreach($all_deposits as $deposit)
      {
        $bins[] = $deposit->bin;
      }

      $all_businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->whereIn("id", $bins )
      ->select("id", "name", "locality")
      ->get();


      $data = array(
         
        'deposits'=>$all_deposits, 
        'all_staffs'=>$all_staffs,
        'all_businesses'=>$all_businesses,
        'month'=>$month, 
        'year'=>$year);          


       return view('admin.accounts.view_deposit_due_amount')->with($data);
    }


    protected function feedbackFromMerchant(Request $request)
    {
        $all_business  = DB::table("ba_business") 
        ->get(); 

        $data =  array('businesses' => $all_business);

        return view('admin.feedback.feedback_by_merchant')->with( $data );

    }


    protected function viewMerchantFeedback(Request $request)
    {
        
        $all_business  = DB::table("ba_business") 
        ->get();

        if ($request->month=="") {
            
            $month = date('m');

        }else
        {

            $month = $request->month;
        }
        
        if ($request->bin!="" && $request->month!="") {

          //for selected merchant
           $bin = $request->bin;
           

           $feedback = DB::table("ba_feedback_merchant")
           ->where("bin",$bin)
           ->whereMonth("entered_date",$month)
           ->whereYear("entered_date",date('Y'))
           ->select()
           ->get();

           $data =  array('businesses' => $all_business,"feedback"=>$feedback,"month"=>$month);
           return view('admin.feedback.view_feedback_from_merchant')->with($data);

           //ends here
        }else{

          //for current month
             
           

           $feedback = DB::table("ba_feedback_merchant")
           ->whereMonth("entered_date",$month)
           ->whereYear("entered_date",date('Y'))
           ->select()
           ->get();

           $data =  array('businesses' => $all_business,"feedback"=>$feedback,"month"=>$month);
           return view('admin.feedback.view_feedback_from_merchant')->with($data);

          // 
        } 

    }

     

    protected function saveMerchantFeedback(Request $request)
    {

       
      if ($request->bin!="" && $request->comment!="" && $request->source!="") {
        
          $bin = $request->bin;
          $remarks = $request->remarks;
          $feedback = $request->comment;
          $source = $request->source;
          $enter_by = $request->session()->get('__member_id_');

          $merchantFeedback = new FeedbackMerchant;

          $merchantFeedback->bin = $bin;
          $merchantFeedback->source = $source;
          $merchantFeedback->remarks = $remarks;
          $merchantFeedback->feedback = $feedback;
          $merchantFeedback->entered_by = $enter_by;
          $merchantFeedback->entered_date = date('Y-m-d');


          $save = $merchantFeedback->save();

          return redirect('admin/feedback/feedback-from-merchant');
      }



    }


    protected function merchantDailySalesList(Request $request)
    {
        if ($request->todayDate!="") {
            
            $todayDate = $request->todayDate;
        }else
        {

            $todayDate = date('Y-m-d');
        }
          
        $all_businesses = DB::table('ba_business')
        ->where("is_block","no")
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "ba_business.phone_pri"  ) 
        ->orderBy("ba_business.name",'asc') 
        ->get();  

        $data = array("business"=>$all_businesses,"todayDate"=>$todayDate);


        return view("admin.billing.merchant_daily_sales_report")->with($data);
        }
      


    public function MerchantDailyOrdersExport(Request $request) 
    
    {
         
            $todayDate  = date('Y-m-d');
            $bin = $request->bin;

            $totalEarning = 0;
             
            $first = DB::table('ba_service_booking')
                ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
                ->wheredate('ba_service_booking.service_date', $todayDate)
                ->whereIn("book_status", array('completed', 'delivered') ) 
                ->where("ba_business.frno", "0")
                ->where("ba_service_booking.bin",$bin) 
                ->select('ba_service_booking.id', 
                  DB::Raw("'customer' source"), 
                  "ba_business.id as bin" ,   
                  'total_cost as orderCost', 
                  DB::Raw("'0.00' packingCost"), 
                  'delivery_charge as deliveryCharge', 
                  'discount',  'payment_type  as paymentType',
                  'book_status as bookingStatus', 
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",
                  DB::Raw("'booktou' paymentTarget"),
                  DB::Raw("'normal' orderType")) ; 

            $result = DB::table('ba_pick_and_drop_order')
                ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
                ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
                ->whereIn("book_status", array('completed', 'delivered') ) 
                ->where("request_from", "business")
                ->where("ba_business.frno", "0")
                ->where("ba_pick_and_drop_order.request_by",$bin)
                ->union($first) 
                ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"))
                ->orderBy("id", "asc")  
                ->get();


            $oids =  $bins = array();
            foreach($result as $item)
            {
                if($item->orderType == "normal")
                {
                    $oids[] = $item->id ;  
                }
            }
         

            $normal_sales_cart = DB::table("ba_shopping_basket") 
            ->whereIn("order_no",  $oids  )  
            ->get();  
     
     
            $all_business  = DB::table("ba_business")  
            ->where("id", $bin )
            ->select("name") 
            ->first(); 
      



            $data = array(  'sales' =>  $result , 
            'cart_items' =>$normal_sales_cart,  
            'date' =>  $todayDate,
            'businesses' => $all_business );  


         $pdf_file =  "booktou_salesbill_" .time(). '_' . $todayDate . ".pdf";
         $save_path = public_path() . "/assets/ccportal/bills/"  .  $pdf_file    ;

         $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('admin.exports.sales_report_per_day' ,  $data  )   ;
               return $pdf->download( $pdf_file  );  
           
    }

    protected function saveSectionItems(Request $request)
    { 

      $category = DB::table("cms_main_menu")
      ->get(); 

      $section = DB::table("cms_section_type")
      ->where("status",1)
      ->get();

      $data = array("category"=>$category,"section"=> $section,'breadcrumb_menu' => 'cms_sub_menu');

      //for saving record
      if ($request->category!="" || $request->title!="" || $request->rowsection!="" 
          || $request->colsection!="") 
      {
            if ($request->keyid=="") {
                 $section  = new SectionTypeModel;
              }else{
                $section  = SectionTypeModel::find($request->keyid);
            }

            $section->title = strtoupper($request->title);
            $section->section_tag = $request->tag;
            $section->description = $request->description;
            $section->category = $request->category;
            $section->no_of_cols = $request->colsection;
            $section->no_of_rows = $request->rowsection;
            $section->allow_view = $request->allowview;

            $save = $section->save();

            if ($save) {
               return redirect('admin/systems/save-section-items')->with("err_msg","record save sucessfull!");
            }else{
              return redirect('admin/systems/save-section-items')->with("err_msg","failed to save  record!");
            }
      }
      //

      return view('admin.settings.add_section')->with("data",$data);
    }

    protected function deleteSectionItems(Request $request)
    {
       $keyid =  $request->keyid;
       if ($keyid=="") {
         return redirect("admin/systems/save-section-items")->with("err_msg","failed");
      }
       
      $section  = SectionTypeModel::find($keyid);

      $section->status = 0;
      $delete = $section->save();

      if ($delete) {
               return redirect('admin/systems/save-section-items')->with("err_msg","record deleted sucessfull!");
            }else{
              return redirect('admin/systems/save-section-items')->with("err_msg","failed to delete  record!");
            }
    }

    protected function productSectionDetails(Request $request)
    {
      $section = DB::table("cms_section_type")
      ->where("allow_view","yes")
      ->where("status",1)
      ->get();

      $business = DB::table("ba_business")
      ->where("is_block","no")
      ->get();

      $data = array("section"=>$section,"business"=>$business,'breadcrumb_menu' => 'cms_sub_menu');

      if ($request->bin!="" || $request->sectiontitle!="") {
        
        $biz_category = $request->sectiontitle;

        $category = explode('-', $biz_category);

        $business_cat = $category[0];
        $sectionid = $category[1];

        $business_category = DB::table("ba_product_category")
        ->where("business_category",$business_cat)
        ->get();
        
        $category = array();
        foreach ($business_category as $items) {
              $category[] = $items->category_name;
        } 
       
       
        $product = DB::table("ba_products")
        ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
        ->where("bin",$request->bin)
        ->whereIn("category",$category) 
        ->orderBy(DB::raw('RAND()'))
        ->select("ba_products.*","ba_product_variant.*")
        ->paginate(20);  

      $data = array(
        "section"=>$section,
        "business"=>$business,
        "product"=>$product,
        "title"=>$business_cat,
        "sectionid"=>$sectionid,
        'breadcrumb_menu' => 'cms_sub_menu'); 
      }

      return view("admin.settings.add_section_details")->with("data",$data);
    } 

    protected function saveSectionDetails(Request $request)
    {
       if($request->btn_save=='Save')
        {       


                $section_category = $request->sectionname;
                $id = $request->sectionid;
                 
                if(!isset($request->prsubid)&& !isset($section_category))
                {
                    return redirect('admin/systems/product-section_details')
                    ->with('err_msg','please provide complete information first!');
                }

                $sectionid = SectionTypeModel::where("category",$section_category)
                ->where("id",$id)
                ->where("allow_view","yes")
                ->where("status",1)
                ->first();
                
                $columns = $sectionid->no_of_cols;

                $subDetails = SectionDetailsModel::where("section_id",$sectionid->id)
                ->where("status",1)
                ->get();

                // foreach loop starts from here
                $productsubid = $request->prsubid;
                //checking column equal or not
                if (count($subDetails)>$columns) {
                    return redirect('admin/systems/product-section_details')
                    ->with('err_msg','no of columns exceed!,Try again');
                }

                if (count($productsubid)>$columns) {
                    return redirect('admin/systems/product-section_details')
                    ->with('err_msg','no of columns exceed!,Try again');
                }
                //counting column from table
                for($i=0;$i<count($productsubid);$i++)
                {
                  
                  $sectionDetails = new SectionDetailsModel; 
                  $sectionDetails->prsubid = $productsubid[$i];
                  $sectionDetails->section_id = $sectionid->id;

                  $save = $sectionDetails->save(); 
                } 

                if ($save) {
                    return redirect('admin/systems/product-section_details')
                    ->with('err_msg','Information save sucessfull!');
                  }else{
                    return redirect('admin/systems/product-section_details')
                    ->with('err_msg','Failed!');
                  }
                 //foreach loop for product adding 
        } 

    }


    protected function editProductSectionDetails(Request $request)
    {

      $sectiontype = DB::table("cms_section_type")
      ->where("status",1)
      ->where("allow_view","yes")
      ->get();

      $data = array("section"=>$sectiontype,'breadcrumb_menu' => 'cms_sub_menu');
      if ($request->sectionid!="") { 

      $sectionlist = DB::table("cms_section_details")
       ->join("cms_section_type","cms_section_details.section_id","=","cms_section_type.id")
       ->where("section_id",$request->sectionid)
       ->where("cms_section_details.status",1)
       ->select("cms_section_type.title",
                "cms_section_type.description",
                "cms_section_details.*",DB::Raw("'' prname"))
       ->get(); 
        
       $prid = array();
       foreach ($sectionlist as $items) {
         $prid[] = $items->prsubid;
       }

       $product = DB::table("ba_products")
       ->whereIn("id",$prid)
       ->get(); 

       foreach ($sectionlist as $items) {
          foreach ($product as $lists) {
             if ($items->prsubid==$lists->id) {
                $items->prname = $lists->pr_name;
                break;
             }
          }
       } 
       $data = array("section"=>$sectiontype,"sectionlist"=>$sectionlist,'breadcrumb_menu' => 'cms_sub_menu');

       return view("admin.settings.view_section_list")->with("data",$data);

      } 

      return view("admin.settings.view_section_list")->with("data",$data);

    }

    protected function deleteProductSectionDetails(Request $request)
    {

      $keyid =  $request->keyid;
       if ($keyid=="") {
         return redirect("admin/systems/edit-product-section_details")->with("err_msg","failed");
      }


      $section  = SectionDetailsModel::find($keyid);

      $section->status = 0;
      $delete = $section->save();

      if ($delete) {
               return redirect('admin/systems/edit-product-section_details')->with("err_msg","record deleted sucessfull!");
          }else{
              return redirect('admin/systems/edit-product-section_details')->with("err_msg","failed to delete  record!");
      }

    }

    protected function viewRoutedOrderDashboard(Request $request)
    {
      $frno = $request->franchise;

      if ($request->month!="") { 
            $month = $request->month;
            $monthname = date('F', mktime(0, 0, 0, $month, 10)); 
        }else
        {
            $month = date('m'); 
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }

        if ($request->year!="") {
            $year = $request->year; 
        }else
        { 
          $year = date('Y');
        }

        if ($frno!="") {
        
          $selected_franchise = DB::table("ba_franchise")
           ->where("ba_franchise.frno",$frno)
           ->first();

          $normal_orders = DB::table("ba_service_booking")
           ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
           ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
           ->whereMonth("ba_service_booking.service_date", $month )
           ->whereYear("ba_service_booking.service_date", $year )
           ->where('ba_service_booking.route_to_frno',$frno)
           ->select(
                    "ba_service_booking.id as orderNo",
                    "ba_service_booking.service_date as serviceDate",
                    "ba_service_booking.book_status as status",
                    "ba_service_booking.total_cost as amount", 
                    DB::Raw(" 'normal' as type")
          )
          ->get();

          $pnd_orders = DB::table("ba_pick_and_drop_order")
           ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
           ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
           ->whereIn("request_from", array ('business', 'merchant'))
           ->whereMonth("ba_pick_and_drop_order.service_date", $month )
           ->whereYear("ba_pick_and_drop_order.service_date", $year )
           ->where('ba_pick_and_drop_order.route_to_frno',$frno)
           ->select(
                        "ba_pick_and_drop_order.id as orderNo",
                        "ba_pick_and_drop_order.service_date as serviceDate",
                        "ba_pick_and_drop_order.book_status as status",
                        "ba_pick_and_drop_order.total_amount as amount", 
                        DB::Raw(" 'pnd' as type") 
            )
           ->get();

           $assist_orders = DB::table("ba_pick_and_drop_order")
           ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
           ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
           ->whereIn("request_from", array ('customer' ) )
           ->whereMonth("ba_pick_and_drop_order.service_date", $month )
           ->whereYear("ba_pick_and_drop_order.service_date", $year )
           ->where('ba_pick_and_drop_order.route_to_frno',$frno)
           
           // ->whereIn("ba_profile.id", function($query) use($frno){
           //  $query->select("profile_id")->from("ba_users")
           //  ->where("franchise", $frno);

           // })
           ->select(
                        "ba_pick_and_drop_order.id as orderNo",
                        "ba_pick_and_drop_order.service_date as serviceDate",
                        "ba_pick_and_drop_order.book_status as status",
                        "ba_pick_and_drop_order.total_amount as amount", 
                        DB::Raw(" 'assist' as type") 
            )
            ->get(); 



            $merged = $normal_orders->merge($pnd_orders); 
            $_orders = $merged->merge($assist_orders);
            $all_orders = $_orders->all();

            $data = array(
              "all_orders"=>$all_orders,
              "selected_franchise"=>$selected_franchise,
              "monthname"=>$monthname,
              "year"=>$year,
              "month"=>$month);
            return view('admin.franchise_orders.view_routed_orders')->with($data);

      } 

     //if franchise no is empty redirecting to franchise list
      $all_franchise = DB::table('ba_franchise')
      ->where('current_status','active')
      ->select()
      ->get();


      return view('admin.franchise_orders.all_franchise')->with('data',$all_franchise);
    }

    protected function routedOrderDetails(Request $request)
    {
       // $franchise = DB::table('ba_franchise')
       // ->where('current_status','active')
       // ->select()
       // ->get();

       if ($request->filter_date!="") {

          $today_date = $request->filter_date;
          
        }else
        {
          $today_date = date('Y-m-d');
        }

        if ($request->month!="") { 
            $month = $request->month;
            $monthname = date('F', mktime(0, 0, 0, $month, 10)); 
        }else
        {
            $month = date('m'); 
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }

        if ($request->year!="") {
            $year = $request->year; 
        }else
        { 
          $year = date('Y');
        }

        $frno = $request->franchise;
        $ordertype = $request->otype;

        $selected_franchise = DB::table("ba_franchise")
        ->where("ba_franchise.frno",$frno)
        ->first();

        if ($frno!="") {

           switch ($ordertype) {
             case 'normal':
                     
                     $normal_orders = DB::table("ba_service_booking")
                     ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
                     ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
                     ->whereMonth("ba_service_booking.service_date", $month )
                     ->whereYear("ba_service_booking.service_date", $year )
                     ->where('ba_service_booking.route_to_frno',$frno)
                     ->select(
                              "ba_service_booking.id as orderNo",
                              "ba_service_booking.service_date as serviceDate",
                              "ba_service_booking.book_status as status",
                              "ba_service_booking.total_cost as amount",  
                              "payment_type as paymentMode",
                              DB::Raw("'booktou' paymentTarget"), 
                              DB::Raw(" 'normal' as type")
                    )
                    ->get();
                    $routed_orders = $normal_orders;
               break;
             
             case 'pnd':

                   $pnd_orders = DB::table("ba_pick_and_drop_order")
                   ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
                   ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
                   ->whereIn("request_from", array ('business', 'merchant'))
                   ->whereMonth("ba_pick_and_drop_order.service_date", $month )
                   ->whereYear("ba_pick_and_drop_order.service_date", $year )
                   ->where('ba_pick_and_drop_order.route_to_frno',$frno)
                   ->select(
                                "ba_pick_and_drop_order.id as orderNo",
                                "ba_pick_and_drop_order.service_date as serviceDate",
                                "ba_pick_and_drop_order.book_status as status",
                                "ba_pick_and_drop_order.total_amount as amount",  
                                "ba_pick_and_drop_order.pay_mode as paymentMode",
                                "ba_pick_and_drop_order.payment_target as paymentTarget",
                                DB::Raw(" 'pnd' as type") 
                    )
                   ->get(); 

                    $routed_orders = $pnd_orders;

               break;
               default: 

                    $assist_orders = DB::table("ba_pick_and_drop_order")
                     ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
                     ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
                     ->whereIn("request_from", array ('customer' ) )
                     ->whereMonth("ba_pick_and_drop_order.service_date", $month )
                     ->whereYear("ba_pick_and_drop_order.service_date", $year )
                     ->where('ba_pick_and_drop_order.route_to_frno',$frno) 
                     // ->whereIn("ba_profile.id", function($query) use($frno){
                     //  $query->select("profile_id")->from("ba_users")
                     //  ->where("franchise", $frno);

                     // })
                     ->select(
                                  "ba_pick_and_drop_order.id as orderNo",
                                  "ba_pick_and_drop_order.service_date as serviceDate",
                                  "ba_pick_and_drop_order.book_status as status",
                                  "ba_pick_and_drop_order.total_amount as amount", 
                                  "ba_pick_and_drop_order.pay_mode as paymentMode",
                                  "ba_pick_and_drop_order.payment_target as paymentTarget",
                                  DB::Raw(" 'assist' as type") 
                      )
                      ->get(); 
                      $routed_orders = $assist_orders;
               break;
           }

           if ($request->otype=="") {
              
            $normal_orders = DB::table("ba_service_booking")
           ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
           ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
           ->whereMonth("ba_service_booking.service_date", $month )
           ->whereYear("ba_service_booking.service_date", $year )
           ->where('ba_service_booking.route_to_frno',$frno)
           ->select(
                    "ba_service_booking.id as orderNo",
                    "ba_service_booking.service_date as serviceDate",
                    "ba_service_booking.book_status as status",
                    "ba_service_booking.total_cost as amount", 
                     "payment_type as paymentMode",
                     DB::Raw("'booktou' paymentTarget"), 
                    DB::Raw(" 'normal' as type")
          )
          ->get();

          $pnd_orders = DB::table("ba_pick_and_drop_order")
           ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
           ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
           ->whereIn("request_from", array ('business', 'merchant'))
           ->whereMonth("ba_pick_and_drop_order.service_date", $month )
           ->whereYear("ba_pick_and_drop_order.service_date", $year )
           ->where('ba_pick_and_drop_order.route_to_frno',$frno)
           ->select(
                        "ba_pick_and_drop_order.id as orderNo",
                        "ba_pick_and_drop_order.service_date as serviceDate",
                        "ba_pick_and_drop_order.book_status as status",
                        "ba_pick_and_drop_order.total_amount as amount", 
                         "ba_pick_and_drop_order.pay_mode as paymentMode",
                         "ba_pick_and_drop_order.payment_target as paymentTarget",
                        DB::Raw(" 'pnd' as type") 
            )
           ->get();

           $assist_orders = DB::table("ba_pick_and_drop_order")
           ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
           ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
           ->whereIn("request_from", array ('customer' ) )
           ->whereMonth("ba_pick_and_drop_order.service_date", $month )
           ->whereYear("ba_pick_and_drop_order.service_date", $year )
           ->where('ba_pick_and_drop_order.route_to_frno',$frno)
           
           // ->whereIn("ba_profile.id", function($query) use($frno){
           //  $query->select("profile_id")->from("ba_users")
           //  ->where("franchise", $frno);

           // })
           ->select(
                        "ba_pick_and_drop_order.id as orderNo",
                        "ba_pick_and_drop_order.service_date as serviceDate",
                        "ba_pick_and_drop_order.book_status as status",
                        "ba_pick_and_drop_order.total_amount as amount", 
                         "ba_pick_and_drop_order.pay_mode as paymentMode",
                                "ba_pick_and_drop_order.payment_target as paymentTarget",
                        DB::Raw(" 'assist' as type") 
            )
            ->get();


            $merged = $normal_orders->merge($pnd_orders); 
            $_orders = $merged->merge($assist_orders);
            $routed_orders = $_orders->all();

           }
          

          $data = array(//'franchise'=>$franchise,
                        'orders'=>$routed_orders,
                        'selected_franchise'=>$selected_franchise,
                        'dated'=>$today_date,
                        'monthname'=>$monthname,'year'=>$year);

          return view('admin.franchise_orders.routed_orders')->with('data',$data);
        }

      // $data = array("franchise"=>$franchise);
       //return view("admin.franchise_orders.routed_orders")->with($data);
    }



    //update original order source
  protected function updateOrderSourceZone(Request $request)
  {
      if(  $request->frno  == ""  ||  $request->orderno   == ""  )
      {
        return redirect("/admin/customer-care/orders/view-all" )->with("err_msg", "Important fields are missing!"); 
      }
 
      $order_seq = OrderSequencerModel::find($request->orderno); 

      if( !isset($order_seq) )
      {
        return redirect("/admin/customer-care/orders/view-all" )->with("err_msg", "Important fields are missing!");
      }  

      $redirect_url ="/admin"; 

      if($order_seq->type == "normal" || $order_seq->type == "booking")
      {
        $order_info = ServiceBooking::find( $request->orderno ) ;
        $redirect_url ="/admin/customer-care/order/view-details/" .  $request->orderno ;

      }
      else if($order_seq->type == "pnd" || $order_seq->type == "assist")
      {
        $order_info = PickAndDropRequestModel::find( $request->orderno ) ;
        $redirect_url ="/admin/customer-care/pnd-order/view-details/" .  $request->orderno ;

      }else 
      {
        return redirect("/admin/customer-care/orders/view-all" )
        ->with("err_msg", "No matching order found!"); 
      }

      if( !isset($order_info) )
      {
        return redirect("/admin/customer-care/orders/view-all" )
        ->with("err_msg", "No order information found!"); 
      } 
 
      $order_info->original_franchise = $request->frno;
      $order_info->save(); 

      //save remarks
      $remark = new OrderRemarksModel();
      $remark->order_no = $request->orderno ;
      $remark->remarks =  $request->remarks == "" ? "order zone update"  : $request->remarks  ;
      $remark->rem_type = "Order Remarks";  
      $remark->remark_by = $request->session()->get('__member_id_' );
      $remark->save();  
      return redirect( $redirect_url )->with("err_msg",  'Order zone updated successfully!'  );  
      
  }

  protected function appSection(Request $request)
  {

    return view('admin.app_section.app_section');
  }

  public function saveAppSection(Request $request)
       {
        
        if ($request->key=="")
         {
              $app = new AppSectionModel();
         }
          else
         { 
              $app = AppSectionModel::find($request->key); 
         }

         $app->title = $request->title;
         $app->show_title = $request->showtitle;
         $app->rows = $request->rows;
         $app->cols = $request->cols; 
        
        // section for background image
         $cdn_url = config('app.app_cdn'); 
         $cdn_path =  config('app.app_cdn_path'); 

         $folder_name = 'section_'. $request->title;  
      
         $folder_path =  $cdn_path."/assets/upload/".'section_'. $request->title;
         
         if ($request->hasFile('photo')) 
            {
              if( !File::isDirectory($folder_path))
                {
                  File::makeDirectory( $folder_path, 0777, true, true);
                }

              $file = $request->photo;

              $originalname = $file->getClientOriginalName(); 
              $extension = $file->extension( );
              $filename = $request->title. time()  . ".{$extension}";
              $file->storeAs('upload',  $filename );
              $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
              $file_names[] =  $filename ;
              $folder_url  = $cdn_url.'/public/assets/upload/section_'. $request->title."/".$filename ;

              // 
              $app->bgimage = $folder_url; 
            } 


                     //section backgroud ends here  
                     
                     $app->show_bgimage = $request->showbackground;
                     $app->bgcolor = $request->bgColor;
                     $app->item_shape = $request->itemshape; 
                     $app->item_height = $request->itemheight; 
                     $app->item_width = $request->itemwidth; 
                     $app->ui_type = $request->uitype; 
                     $app->sub_module = $request->submodule; 
                     $app->published = $request->publish; 

                     $save = $app->save();
             
                     if($save)
                     {
                         $msg = "Save successfully!";
                         return redirect('admin/systems/app-section')->with('err_msg',$msg);
                            
                     }
                      else
                     {
                        $msg = "failed!";
                        return redirect('admin/systems/app-section')->with('err_msg',$msg);
                     }
        }

        protected function viewAppSection(Request $request)
        {
           $result = DB::table('app_landing_section')
                   ->where('published','yes')
                   ->get();

           $data = array('results' => $result);
           return view('admin.app_section.view_section')->with($data);
        }

        protected function deleteSection(Request $request)
        {

          $id = $request->sid;
          $section = DB::table('app_landing_section')
          ->where('id', $id)
          ->delete();
        
          if ($section) {
            $msg  = 'record deleted sucessfull!';
            return redirect('admin/systems/view-app-section')->with('err_msg',$msg);
          }else{
            $msg  = 'failed to delete record!';
            return redirect('admin/systems/view-app-section')->with('err_msg',$msg);
          }
        }

protected function appSectionDetails(Request $request)
{ 
  $section = DB::table('app_landing_section')
  ->where('published','yes')
  ->get();
 
  $data = array('results'=>$section);
  return view('admin.app_section.app_section_details')->with($data);
}

protected function saveAppSectionDetails(Request $request)
{
    if($request->key !="")
      {
        $sectionDetails = AppSectionDetailsModel::find($request->key);
      }else
      {
        $sectionDetails = new AppSectionDetailsModel; 
      }

         // section for background image
         $cdn_url = config('app.app_cdn'); 
         $cdn_path =  config('app.app_cdn_path');  
         $folder_name = 'section_'. $request->sectionid; 
         $folder_path =  $cdn_path."/assets/upload/".'section_'. $request->sectionid;
         
         if ($request->hasFile('photo')) 
            {
              if( !File::isDirectory($folder_path))
                {
                  File::makeDirectory( $folder_path, 0777, true, true);
                }

              $file = $request->photo; 
              $originalname = $file->getClientOriginalName(); 
              $extension = $file->extension( );
              $filename = $request->sectionid. time().".{$extension}";
              $file->storeAs('upload',  $filename );
              $imgUpload = File::copy(storage_path().'/app/upload/'.$filename,$folder_path."/".$filename);
              $file_names[] =  $filename ;
              $folder_url  = $cdn_url.'/public/assets/upload/section_'.$request->sectionid."/".$filename; 
              // 
              $sectionDetails->image_url = $folder_url; 
            } 
        //section backgroud ends here 
        
        $sectionDetails->section_id  =$request->sectionid;
        $sectionDetails->caption =$request->caption;
        $sectionDetails->sub_caption=$request->subcaption;
        $sectionDetails->caption_layout=$request->layout;
        $sectionDetails->rating =$request->rating;
        $sectionDetails->type =$request->type;
        $sectionDetails->target_app_url =$request->appurl;
        $sectionDetails->target_web_url=$request->weburl;
        $sectionDetails->is_sponsored=$request->sponsored;
        $save =$sectionDetails->save();

        if($save)
        {
          $msg = "section details save successfully!";
          if ($request->_token!="") {
             return redirect("/admin/systems/view-app-section-details")->with('err_msg',$msg);
          }
          return redirect("/admin/systems/app-section-details")->with('err_msg',$msg);
        }
        else
        {
          $msg = "failed!";
          if ($request->_token!="") {
             return redirect("/admin/systems/view-app-section-details")->with('err_msg',$msg);
          }
          return redirect("/admin/systems/app-section-details")->with('err_msg',$msg);
        } 
}

protected function viewAppSectionDetails(Request $request)
{ 
  
    $section = DB::table('app_landing_section')
    ->where('published','yes')
    ->get();
    
    $data = array('results'=>$section);

  if($request->sectionid!="")
  {
     $sectionid = $request->sectionid;
     $sectiondetails = DB::table('app_landing_section_content')
    ->where('section_id',$sectionid)
    ->get();

 
    $data = array('results'=>$section,'sectiondetails'=>$sectiondetails);
  } 

  return view('admin.app_section.view_app_section_details')->with($data);
}

protected function deleteSectionDetails(Request $request)
{
          $id = $request->sid;
          $section = DB::table('app_landing_section_content')
          ->where('id',$id)
          ->delete();
        
          if ($section) {
            $msg  = 'record deleted sucessfull!';
            return redirect('admin/systems/view-app-section-details')->with('err_msg',$msg);
          }else{
            $msg  = 'failed to delete record!';
            return redirect('admin/systems/view-app-section-details')->with('err_msg',$msg);
          }
}

protected function createBrandName(Request $request)
{ 
    $brand = DB::table("ba_water_brand")
    ->get();
 
    $data =array('breadcrumb_menu' => 'system_sub_menu','brand'=>$brand);  
    return view('admin.bookings.create_water_brand')->with('data',$data);
}

protected function saveBrandName(Request $request)
{

  $cdn_url = config('app.app_cdn'); 
  $cdn_path =  config('app.app_cdn_path'); 

  if ($request->brandid=="") {
     $brand = new BrandModel;
  }else{
    $brand = BrandModel::find($request->brandid);
  }

  $brand->brand_url = $request->weburl;
  $brand->brand_name = $request->brandname;

  // uploading brand photo and jar photo
    $folder_name = 'brand_'. $request->brandname; 
    $folder_path =  $cdn_path."/assets/upload/".'brand_'. $request->brandname;

    $folder_name_jar = 'brand_jar'. $request->brandname; 
    $folder_path_jar =  $cdn_path."/assets/upload/".'brand_jar'. $request->brandname;

    if ($request->hasFile('photo')) {

            if( !File::isDirectory($folder_path))
                {
                  File::makeDirectory( $folder_path, 0777, true, true);
                } 
      
              $file = $request->photo;
              $originalname = $file->getClientOriginalName(); 
              $extension = $file->extension( );
              $filename = $request->brandname. time()  . ".{$extension}";
              $file->storeAs('upload',  $filename );
              $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
              $file_names[] =  $filename ;
              $folder_url  = $cdn_url.'/assets/upload/brand_'. $request->brandname."/".$filename ;

              $brand->brand_image = $folder_url;
               
    }

    if ($request->hasFile('jarphoto')) {

            if( !File::isDirectory($folder_path_jar))
                {
                  File::makeDirectory( $folder_path_jar, 0777, true, true);
                } 
      
              $file = $request->jarphoto;
              $originalname = $file->getClientOriginalName(); 
              $extension = $file->extension( );
              $filename = $request->brandname. time()  . ".{$extension}";
              $file->storeAs('upload',  $filename );
              $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path_jar . "/" . $filename);
              $file_names[] =  $filename ;
              $folder_url_jar  = $cdn_url.'/assets/upload/brand_jar'. $request->brandname."/".$filename ;

              $brand->brand_jar = $folder_url_jar;
               
    }

  //ends here

  
  $save = $brand->save();
  if ($save) {
     return redirect('/admin/system/water-booking/create-brand-name');    
  }
  else{
     return redirect('/admin/system/water-booking/create-brand-name');
  }
} 
 



    public function manageServiceCategory(Request $request)
    {
  
       $booktable_categories = DB::table("ba_bookable_category")
        ->orderBy("sub_module","asc")
        ->get();

 

 
         $service_categories = DB::table("ba_service_category") 
          ->orderBy("category","asc")
          ->get();
       

        $data = array('service_categories' => $service_categories, 'booktable_categories' => $booktable_categories, 'breadcrumb_menu' => 'system_sub_menu' );
    
     return view("admin.products.update_service_product_category")->with("data",$data);  
    }


    
    public function updateServiceCategory(Request $request)
  { 


    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; // cnd - /var/www/html/api/public


    if( isset($request->btnsave) && $request->btnsave =="save"):
 

      if($request->submodule == "" || $request->category == "" ||  $request->description == "")
      {
         return redirect("/admin/services/manage-category")->with("err_msg", "Field missing!");
      }
    
            $category_info = ServiceCategoryModel::find($request->catid);
            
            if(!isset($category_info)) 
            {

              $category_info = new ServiceCategoryModel; 

            } 

            $category_info->category = $request->category  ;
            $category_info->bookable_category =  $request->submodule ;
            $category_info->cat_des = $request->description ; 
            $category_info->display_order = 0;



            if( $request->file('photo') != "")
            {
              $imageName = "ico_" . time() . '.' .$request->photo->getClientOriginalExtension();
              $request->photo->move(  $cdn_path . '/assets/app/icons' ,  $imageName ); 
              $category_info->icon_url =  $cdn_url . '/assets/app/icons/'.$imageName ;  

            }

            $save = $category_info->save();



           if ($save) 
           {
             return back()->with("err_msg", "Service category successfully updated!");
           }
            else
            {
              return back()->with("err_msg", "Service category update failed!");
            }  

        else:

              return redirect("/admin/services/manage-category"); 

      endif;
  }


  //Delete/Destroy Service Category
      public function deleteServiceCategory(Request $request ) 
    {

       $id = $request->key;

       if($id == "" || $id == 0 )
       {
        return redirect("/admin/services/manage-category")->with("err_msg", "No category selected for removal!"); 
       }


       $category_info = ServiceCategoryModel::find( $id );

       if( !isset($category_info) )
       {
          return redirect("/admin/services/manage-category")->with("err_msg", "No category information found!"); 
       }



       $service_categories_count = DB::table("ba_service_products") 
       ->where("service_category", $category_info->category )
       ->count();
 


       if( $service_categories_count > 0 )
        {
            
            $data= ["message" => "success", "status_code" =>  555 ,  'detailed_msg' => 'Category deletion blocked as there are services under it!'  ];
    
        }
        else{

          $category_info->delete();
            $data= ["message" => "failure", "status_code" =>  556 ,  'detailed_msg' => 'Category removed succeeded!'  ];
        }

        return back();

    }

  protected function viewAgentAdvanceHistory(Request $request)
  {
     
      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("category", "100" )
      ->where("ba_users.status", "<>", "100" )  
      ->select("ba_profile.id","ba_profile.fullname" )
      ->get(); 

      if ($request->staffName!="") {

          $year  = ($request->year=="") ?  date('Y') : $request->year;  
          $month  = ($request->month=="") ?  date('m') : $request->month; 

          $advance_history = DB::table("ba_salary_and_advance")
            ->where("member_id",$request->staffName)
            ->whereMonth("request_date", $month ) 
            ->whereYear("request_date", $year) 
            ->select("id","details", "amount","request_date as advanceRequestDate",  "paid_date as advanceTakenDate", "category as type", "request_status as requestStatus")
            ->orderBy("request_date", "asc")
            ->get();
            
          if( !isset($advance_history))
              {
                return redirect('admin/accounts/view-agent-advance-history');
          }
          
          $advance_config = DB::table("ba_global_settings")
          ->where("config_key", "agent_advance_limit" )  
          ->first(); 
          
          $fuelAdvance = $salaryAdvance= 0.00;
          foreach($advance_history  as $item)
          {
            if(   $item->type == "fuel")
            {
              $fuelAdvance += $item->amount;
            }
            else 
            {
              $salaryAdvance += $item->amount;
            } 
          }

          $data = array('staff'=>$all_staffs,'result'=>$advance_history,'fuel'=>$fuelAdvance,'salary'=>$salaryAdvance);
          return view('admin.agents.agent_advance_history')->with($data);
      }

      $data = array('staff'=>$all_staffs);
      return view('admin.agents.agent_advance_history')->with($data);
  }


  protected function agentAdvanceAmountEntry(Request $request)
  {
     
      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("category", "100" )
      ->where("ba_users.status", "<>", "100" )  
      ->select("ba_profile.id","ba_profile.fullname" )
      ->get();  

      $data = array('staff'=>$all_staffs);
      return view('admin.agents.add_agents_advance_amount')->with($data);
  }

  protected function addAgentAdvanceAmountEntry(Request $request)
  {
      if ($request->staffName=="" || $request->category=="" || $request->amount=="" || $request->details=="" || $request->requestdate=="") {
          
        return redirect('admin/agents/agent-advance-amount-entry')->with('err_msg','missing data to perform action!');
      }

        $advance = new AgentSalaryAdvanceModel;
        $advance->amount = $request->amount;
        $advance->member_id = $request->staffName;
        $advance->details = $request->details;
        $advance->category = $request->category;
        $advance->request_date = date('Y-m-d',strtotime($request->requestdate));
        $advance->request_status="pending";

        $save = $advance->save();
        if ($save) {
          return redirect('admin/agents/agent-advance-amount-entry')->with('err_msg','record save sucessfull!');
        }else
        {
          return redirect('admin/agents/agent-advance-amount-entry')->with('err_msg','failed');
        }
        
  }
  protected function viewServicesOffered(Request $request)
  {
    
    $bin = $request->bin;

    if($bin == "")
    {
      return redirect('admin/customer-care/business/view-all');
    }

    $services= DB::table("ba_service_products")
    ->where("ba_service_products.bin",$bin)
    ->paginate(10);

    $business= Business::find( $bin) ;

    $data = array(
                  'services' => $services, 
                  'bin'=>$bin,
                  'business'=>$business
                  
                );   
    return view("admin.business.services_offered")->with('data',$data);

  }

  protected function businessTimeSlotListing(Request $request)
  {

    $bin = $request->bin;  
    if($bin == "")
    {
      return redirect('admin/customer-care/business/view-all'.$bin)->with('err_msg','missing');
    }
     $month = ($request->month == "") ? date('m') : $request->month;
     $year = ($request->year == "") ? date('Y') : $request->year;
    //$month =  $request->month;
  
    //$year =   $request->year;
    $sales = DB::table('ba_service_days')
    ->where("bin",$bin)
    ->where('month',$month)
    ->where('year',$year)       
      // ->orderBy("bin")
    ->paginate(10);

    $time_slots = DB::table("ba_services_duration")
    ->where("bin",$bin)
    ->whereMonth('updated_at',$month)
    ->whereYear('updated_at',$year)
    ->orderBy("day_name" )
    ->paginate(10);

    $business= Business::find( $bin) ;
    return view("admin.business.time_slot")->with(array('time_slots' => $time_slots, 'sales' => $sales,'month'=>$month,'year'=>$year,"business"=>$business));   
  }


  protected function updateBankAccountInfo(Request $request)
    {

      if ($request->key=="") {
        return redirect("/admin/customer-care/business/view-profile/".$bin )
        ->with('err_msg',  "mismatch information!");
      }
      
      
      $bin =$request->key;
      $business = Business::find($bin);

      if( !isset($business)) 
      {
        return redirect("/admin/customer-care/business/view-profile/" . $business->id  )->with('err_msg',  "No business found!");
      }

      $spitpay = new BusinessBankAccountModel;
      $spitpay->bin= $business->id;
      $spitpay->bank_acc= $request->bank_acc;
      $spitpay->ifsc_code= $request->ifsc_code;
      $spitpay->bank_name= $request->bank_name;
      $spitpay->rzp_acc_id= $request->rzp_acc_id;
      $save = $spitpay->save();
      if ($save) {
        
         return redirect("/admin/customer-care/business/view-profile/".$bin )
         ->with('err_msg',  "information update sucessfull!");

      }
    }

}

