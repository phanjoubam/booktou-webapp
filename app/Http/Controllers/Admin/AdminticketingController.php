<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Traits\Utilities;
use App\TicketingModel; 

class AdminticketingController extends Controller
{
    use Utilities;
    protected function ticketCounts(Request $request)
    {
      $key = $request->search_key;
      $status=$request->status;
      $ticket_type= $request->type;
      $tickets = DB::table('event_ticket')
      ->get();
      $paid_ticket = DB::table('event_ticket_purchase')
      ->where("payment_status",'paid')
      ->count();
      $unpaid_ticket = DB::table('event_ticket_purchase')
      ->where("payment_status",'unpaid')
      ->count();

      if( isset($request->btnSearch) )
      {
        if($key=="")
        {
          return redirect("/admin/ticket-count")->with('err_msg',"Missing data"); 
        }
        else 
        {

          $tickets_filter = DB::table('event_ticket')
          ->where("visitor_phone","like", "%" . $key . "%")
          ->orWhere("ticket_no","like", "%" . $key . "%")
          ->paginate(10);
          $data = array(
            "sold_tickts"=>$tickets, "tickets_filter"=>$tickets_filter,
            "status"=>$status,"ticket_type"=>$ticket_type,'paid_ticket'=>$paid_ticket,'unpaid_ticket'=>$unpaid_ticket);
          return view("admin.ticketing.ticket_counts_view")->with("data",$data);
            
        }
          
 
       }

       $filter_status=0;
      if(strcasecmp($status,"active")==0)
      {
        $filter_status = 0;
      }
      else if(strcasecmp($status,"inside")==0)
      {
        $filter_status = 1;
      }
      else if(strcasecmp($status,"tempexit")==0)
      {
        $filter_status = 10;
      }
      else if(strcasecmp($status,"exit")==0)
      {
        $filter_status = 100;
      }
      else
      {
        $filter_status=0;
      }

      $tickets_filter = DB::table('event_ticket')
      ->where("current_status",$filter_status)
      ->where("ticket_format",$ticket_type)
      ->paginate(10);
      
      $data = array(
          "sold_tickts"=>$tickets, "tickets_filter"=>$tickets_filter,
          "status"=>$status,"ticket_type"=>$ticket_type,
          'paid_ticket'=>$paid_ticket,'unpaid_ticket'=>$unpaid_ticket);
       return view("admin.ticketing.ticket_counts_view")->with("data",$data);
    }

    protected function ticketPaymentStatus(Request $request)
    {
      $key = $request->custphone;
      $status=$request->status;
      if(strcasecmp($request->status,"paid")==0)
      {
        $status="paid";
      }
      else
      {
        $status="unpaid";
      }
      if( isset($request->btn_search) )
      {
        if($key=="")
        {
          return redirect("/admin/ticket-payment-status")->with('err_msg',"Missing data"); 
        }
        else 
        {

          $ticket_payment=DB::table("event_ticket_purchase")
          ->where("phone","like", "%" . $key . "%")
          ->orWhere("visitor_name","like", "%" . $key . "%")
          ->select("event_ticket_purchase.*",DB::Raw("'0.00' total_amount"))
          ->paginate(10);
       $purchase_id = $ticket_payment->pluck("id")->toArray();
      $purchase_details = DB::table("event_ticket_purchase_details")
      ->whereIn("purchase_id",$purchase_id)
      ->select("purchase_id","pricing","ticket_type")
      ->get();
      $qr_codes=DB::table("event_ticket")
      ->whereIn("purchase_id",$purchase_id)
      ->select("purchase_id","price","qrcode_url")
      ->get();
      foreach($ticket_payment as $item)
      {
        $total_price = 0;
        foreach ($purchase_details as $value)
        {  
          if($item->id == $value->purchase_id){$total_price += $value->pricing;}   
        }
        $item->total_amount = $total_price;
      }
          $data=array("ticket_payment"=>$ticket_payment,
                  "status"=>$status,
                  'purchase_details'=>$purchase_details,
                'qr_codes'=>$qr_codes);
      return view("admin.ticketing.ticket_payment_status")->with("data",$data);
            
        }
          
 
       }

      $ticket_payment=DB::table("event_ticket_purchase")
      ->where("payment_status",$status)
      ->select("event_ticket_purchase.*",DB::Raw("'0.00' total_amount"))
      ->orderBy('id',"desc")
      ->paginate(10);
      $purchase_id = $ticket_payment->pluck("id")->toArray();
    
      $purchase_details = DB::table("event_ticket_purchase_details")
      ->whereIn("purchase_id",$purchase_id)
      ->select("purchase_id","pricing","ticket_type")
      ->get();

      $qr_codes=DB::table("event_ticket")
      ->whereIn("purchase_id",$purchase_id)
      ->select("purchase_id","price","qrcode_url")
      ->get();
      foreach($ticket_payment as $item)
      {
        $total_price = 0;
        foreach ($purchase_details as $value)
        {  
          if($item->id == $value->purchase_id){$total_price += $value->pricing;}   
        }
        $item->total_amount = $total_price;
      }
      $data=array("ticket_payment"=>$ticket_payment,
                  "status"=>$status,
                'purchase_details'=>$purchase_details,
                'qr_codes'=>$qr_codes);
      return view("admin.ticketing.ticket_payment_status")->with("data",$data);

    }
    protected function ticketScanFilterbyStaff(Request $request)
    {
      $scan=DB::table("event_ticket_scan_log")
      ->select('member_id')
      ->distinct()
      ->get();
      $staff_id = $scan->pluck("member_id")->toArray();
      $profile=DB::table("ba_profile")
      ->whereIn('id',$staff_id)
      ->get();
      $data=array('scan'=>$scan,
        'profile'=>$profile);
      return view("admin.ticketing.ticket_scan_filter")->with('data',$data);
    }

    // protected function ticketStaffSelecting(Request $request)
    // {
    //   $mem_id=$request->staff_id;
    //   if($mem_id!=="")
    //   {
    //     $scan_ticket=DB::table('event_ticket_scan_log')
    //     ->where('member_id',$mem_id)
    //     ->select('ticket_no')
    //     ->distinct()
    //     ->get();
    //     $ticket_no=$scan_ticket->pluck("ticket_no")->toArray();
    //     $ticket_detail=DB::table("event_ticket")
    //     ->whereIn('ticket_no',$ticket_no)
    //     ->get();
    //     $data=array('scan_ticket'=>$scan_ticket,'ticket_detail'=>$ticket_detail);
    //     return response()->json($data);
    //   }
    // }

}
