<?php

namespace App\Http\Controllers\Admin; 

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ServiceBooking;
use App\ServiceBookingDetailsModel;
use App\ShoppingBasketModel;
use App\Business;
use App\BusinessCategory;
use App\OrderSequencerModel;
use App\ServiceProductModel;
use App\WaterBookingModel;
use App\BusinessHour;
use App\ServiceDuration;
use App\BusinessServiceDays;
use App\ShoppingBasketServiceProductModel;
use PDF;
class ServiceBookingController extends Controller
{
    
    protected function addServiceBooking()
    {

     $service_category = DB::table('ba_service_category')
                        ->where("bookable_category",  "SNS")
                        ->get();


     $service_product = DB::table('ba_service_products')
                        ->where("service_category",  "HAIR")
                        ->where("bin",  "421")
                        ->get();

     return view ('admin.bookings.add_booking')->with( array('categories' =>  $service_category ,'products' =>  $service_product) ); 

 
    }


protected function showServiceProduct(Request $request)
    {
      $categories = $request->sbservicecat;
      $service_product = DB::table('ba_service_products')
                        ->where("service_category",  "$categories")
                        ->where("bin",  "421")
                        ->get();

    $returnHTML = view('admin.servicebooking.service_product')->with('products', $service_product)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
    //return response()->['products' =>  $service_product]; 

    }



    // protected function saveServiceBooking(Request $request)
    // {
    //         if (isset($request->btn_save)) 
    //         {
               
    //         $servicebooking = new ServiceBookingModel();
    //         $servicebooking->id = 14;
    //         $servicebooking->bin = 421 ;
    //         $servicebooking->book_category= "SNS";
    //         $servicebooking->book_by  = 0 ;
    //         $servicebooking->staff_id  = 0;
    //         $servicebooking->otp = 836571 ;
    //         $servicebooking->address = "NA" ;
    //         $servicebooking->landmark = "NA" ;
    //         $servicebooking->city = "NA" ;
    //         $servicebooking->state = "NA" ;
    //         $servicebooking->pin_code = "NA" ;
    //         $servicebooking->latitude = 0 ;
    //         $servicebooking->longitude = 0 ;
    //         $servicebooking->delivery_charge = 0 ;
    //         $servicebooking->agent_payable = 0 ;
    //         $servicebooking->cashback = 0 ;
    //         $servicebooking->delivery_otp = 123444 ;
    //         $servicebooking->sms_sent = "no" ;
    //         $servicebooking->order_source = "browser" ;

    //         $servicebooking->book_date = new \DateTime();
    //         $servicebooking->service_date = new \DateTime( $request->sbdate );
    //         $servicebooking->payment_type = $request->sbpayment ;
    //         $servicebooking->seller_payable = $request->sbsellerpayable;
    //         $servicebooking->service_fee = $request->sbfee;
    //         $servicebooking->discount = $request->sbdiscount;
    //         $servicebooking->coupon_code  = $request->sbcoupon;
             
    //         $total = $request->sbsellerpayable + $request->sbfee;
    //         $servicebooking->total_cost =  $total;

            
    //         //saving the record in ba_service_booking table
    //         $save = $servicebooking->save();
    //         //

    //         //
    //         //service booking details table record insertion part
    //         $servicebookingdetails = new ServiceBookingDetailsModel();
    //         $servicebookingdetails->book_id = 14;
    //         $servicebookingdetails->service_product_id = $request->sbserviceproduct;
    //         $servicebookingdetails->service_name= $request->sproduct;
    //         $servicebookingdetails->service_code="SNS21";
    //         //$servicebookingdetails->slot_no=4;
    //         $servicebookingdetails->service_time= $request->shour.':'. $request->smin;

    //         //echo  $request->smin;die();

    //         $save1 = $servicebookingdetails->save();


    //         if ($save && $save1) {
    //         return redirect("/enter-service-booking")->with("msg", "Service booking add sucessfull!");
    //         }
    //         else
    //         {
    //             return redirect("/enter-service-booking")->with("msg", "Failed!");
    //         }

    //         }
       
    // }



     protected function pendingPayments(Request $request )
  { 

 

        if( $request->month  == "")
        {
            //$month   =  date('m');
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }

 

        if( $request->year   == "")
        {
            //$year = date('Y');
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }

        $totalEarning = 0;


 
        $result = DB::table('ba_business')
        ->whereIn("id",  function($query) use ($month,$year ) {
             $query->from('ba_service_booking')
             ->whereMonth('book_date', $month)
             ->whereYear('book_date', $year)
             ->where("book_status",'=',"delivered")
             ->select( "bin")
              ->distinct()  ;
          
        })
        ->orderBy("id",'desc' )
        ->get(); 


      $pending_dues = DB::table("ba_service_booking") 
      ->whereIn("bin", function($subquery) use ($month, $year) { 
        //nested query part
          $subquery->select("id")
          ->from("ba_business")
          ->whereMonth("book_date",  $month  )
          ->whereYear("book_date",   $year  );  
        //nested query ends here
      })

      //selecting bin and seller_payable from ba_service_booking
      ->select("bin", DB::Raw(" sum(seller_payable) as sellerPayable"))
      ->where("book_status",'=',"delivered")
      ->groupBy("bin")
      ->orderBy("sellerPayable",'desc' )
      ->get();


        $data = array(  'result' => $result , 'month' =>  $month , 
            'year' => $year,'pending_due'=>$pending_dues);  

 

        return view("admin.reports.pending_payments")->with(  $data);  
    }


    protected function dailySalesReport(Request $request)
    {

        if( $request->month  == "")
        {
             $month   =  date('m') ;
        }
        else 
        {
          $month   = $request->month ; 
        }
 
  


        if( $request->year   == "")
        {
             $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }
 
        $totalEarning = 0;
 
       $result = DB::table('ba_business')
        ->whereIn("id",  function($query) use ($month,$year ) {
             $query->from('ba_service_booking')
             ->whereMonth('book_date', $month)
             ->whereYear('book_date', $year)
             ->where("book_status",'=',"delivered")
             ->select( "bin")
              ->distinct()  ;
          
        })
        ->orderBy("id",'desc' )
        ->get(); 


      $pending_dues = DB::table("ba_service_booking") 
      ->whereIn("bin", function($subquery) use ($month, $year) { 
        //nested query part
          $subquery->select("id")
          ->from("ba_business")
          ->whereMonth("book_date",  $month  )
          ->whereYear("book_date",   $year  );  
        //nested query ends here
      })

      //selecting bin and seller_payable from ba_service_booking
      ->select("bin", DB::Raw(" sum(seller_payable) as sellerPayable"))
      ->where("book_status",'=',"delivered")
      ->groupBy("bin")
      ->orderBy("sellerPayable",'desc' )
      ->get();

 
        $data = array(  'result' => $result , 'month' =>  $month , 
            'year' => $year,'pending_due'=>$pending_dues);  

 

        return view("admin.reports.daily_payments")->with(  $data); 

    }

    protected function bookingAppointment()
    {
        $setData=$request->orderno;
       
        $result_staff = DB::table('ba_service_booking')
        ->join('ba_users' ,'ba_service_booking.staff_id', '=', 'ba_users.profile_id')
        ->join('ba_service_booking_details','ba_service_booking.id', '=', 'ba_service_booking_details.book_id')
        ->join('ba_profile','ba_users.profile_id','=','ba_profile.id')

    
        ->where("ba_service_booking.id",'=',$setData)
        ->select()
        ->get();



        $result_venue = DB::table('ba_service_booking')
        ->join('ba_business','ba_service_booking.bin','=', 'ba_business.id')
        ->where("ba_service_booking.id",'=',$setData)
        ->select()
        ->get();


        

        $result_customer = DB::table('ba_service_booking')
        ->join(DB::Raw("ba_profile as bp"),'ba_service_booking.book_by' ,'=', 'bp.id')
        ->where("ba_service_booking.id",'=',$setData)
        
        ->select("fullname","bp.locality","bp.landmark","bp.city","bp.state","bp.pin_code","bp.latitude","bp.longitude","bp.phone","bp.email")
        ->get();

        $result_billing =DB::table('ba_service_booking') 
        ->join('ba_service_booking_details','ba_service_booking.id','=','ba_service_booking_details.book_id')
        ->where('ba_service_booking.id',$setData)
        ->select ("service_date",DB::Raw('sum(discount) as discount, sum(service_fee) as fees, sum(service_charge) as charge')  )
        ->groupBy('ba_service_booking.id')
        ->get();

        
        $data = array(  'result' => $result_staff,'venue'=>$result_venue,
            'customer'=>$result_customer,'bill'=>$result_billing,'service_id'=>$setData);  

        return view("admin.orders.booking_appointment")->with($data);
    }




    protected function updateRemark(Request $request)
    {

        $remark = $request->cus_remark;
        $id= $request->serviceid;
        
        $update = DB::table('ba_service_booking')
              ->where('id', $id )
              ->update(['cust_remarks' => $remark]);

              if($update)
              {
                $msg = "Remark added!";

              }else{
                $msg ="Failed to add remark";
              }
        
        return response()->json(array('success' => true, 'detail_msg' => $msg ));
    }




public function manageBooking(Request $request)
{
   if ($request->sdate=="") {
        $today= date('Y-m-d');     
   }else{ 
    $today=date('Y-m-d',strtotime($request->sdate));
   } 
   $bin= $request->bin;
   if ($bin=="") {
      return redirect("/services/view-merchant-wise-booking")->with("err_msg" , "No business selected!");
  }
   $business = Business::find($bin);



  if ( !isset($business)){
      return redirect("/services/view-merchant-wise-booking")->with("err_msg" , "Selected business not found!");
  }

  $status = ['new', 'confirmed','engaged','in_queue'];

   
      $normal_orders = DB::table("ba_service_booking")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id") 
      ->where("ba_order_sequencer.type", "booking")
      ->whereIn("ba_service_booking.book_status",$status)
      ->where("bin",$bin)
      ->whereRaw("date(book_date) >= '$today' " )
      ->select("ba_service_booking.id","bin", "service_date", "book_by", "book_date", "service_date"  ,"book_status",  
          DB::Raw("'na' service_time"),DB::Raw("'na' service_name"), DB::Raw("'na' service_code"),DB::Raw("'na' added_to_queue"),DB::Raw("'na' status"),
      "customer_name", "address", "landmark", "total_cost","payment_type as pay_mode" , 
      "ba_order_sequencer.type", DB::Raw(" 'Customer' orderSource")  )
      ->get();   
  
 
    $book_nos = array( '0' );
    $cust_ids = array( '0' );
    foreach($normal_orders as $item)
    {
      $book_nos[] = $item->id;
      $cust_ids[] = $item->book_by;
    }
 
    $booked_services= DB::table("ba_service_booking_details")
    ->whereIn("book_id", $book_nos)
    ->select("book_id", "service_product_id", "service_name", "service_time", "added_to_queue", "status")
    ->get();
     
    // $cust_ids = array_filter($cust_ids);
     $cust_ids =  array_unique($cust_ids); 
 
    $users = DB::table("ba_users")
    ->whereIn("profile_id", $cust_ids) 
    ->select('profile_id','category')
    ->get(); 



    $allorders = array();
    foreach($normal_orders as $item)
    {
        foreach ($users as $customer) {
            if ($item->book_by== $customer->profile_id) {
                   if ($customer->category>=1) {
                   $item->orderSource = 'Business';
                    break; 
                   }
            }                
        }    
     


        foreach($booked_services as $bookeditem)
        if($bookeditem->book_id == $item->id )
        {
          $item->service_product_id  = $bookeditem->service_product_id;
          $item->service_time = $bookeditem->service_time; 
          $item->added_to_queue = $bookeditem->added_to_queue;
          $item->status = $bookeditem->status;
          $item->service_name = $bookeditem->service_name; 
          break; 
        }    
        
        $allorders[] = $item;
    } 
 
    $keys = array_column($allorders, 'service_time'); 
    array_multisort($keys, SORT_ASC, $allorders); 
    
     
    $data = array('orders' =>  $allorders , 'booked_services' => $booked_services, 
    'business' =>$business,   'page_title' => 'Manage Listed Services' );

    return view('admin.bookings.view_bookings')->with(   $data ); 

}

protected function viewBookingOffer(Request $request)
{
    if($request->sdate=="")
    {
        $today = date('Y-m-d');
    }else{
        $today = $request->sdate;
    }
     
    $bin = $request->bin;
    $status = ['new', 'confirmed','engaged','in_queue'];
    $booking = DB::table('ba_service_booking')
    ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
    ->whereRaw("date(ba_service_booking.service_date)>='$today'") 
    ->where("ba_order_sequencer.type", 'offers')
    ->where('ba_service_booking.bin',$bin)
    ->whereIn("ba_service_booking.book_status",$status)
    ->select()
    ->get(); 
     

    $bin =  array();
    $book_nos = array();
    $cust_ids = array();

    foreach ($booking as $items) {
      $bin[] = $items->bin;
      $book_nos[] = $items->id;
      $cust_ids[] = $items->book_by; 
    }
    
    $offer = DB::table('ba_shopping_basket_srv_products')
    ->whereIn('order_no',$book_nos)
    ->get();
  
    $business = DB::table('ba_business')
    ->whereIn('id',$bin)
    ->where("is_open","open")
    ->where("is_block","no")
    ->select()
    ->first();

    $customer = DB::table('ba_profile')
    ->whereIn('id',$cust_ids)
    ->get();

    $data = array('biz'=>$business,'offer'=>$offer,'today'=>$today,'customer'=>$customer,'booking'=>$booking);
    return view('admin.bookings.view_offer')->with($data);
}



protected function viewWaterBooking(Request $request)
{
        if ($request->sdate=="") {
                $today= date('Y-m-d');     
           }else{ 
            $today= $request->sdate;
           } 
           $bin= $request->bin;

          if ($bin=="") {
              return redirect("/services/view-merchant-wise-booking");
          }

          $status = ['new', 'confirmed','engaged','in_queue'];
        
          $water = DB::table("ba_water_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_water_booking.id") 
          ->where("ba_order_sequencer.type", "booking")
          ->whereIn("ba_water_booking.book_status",$status)
          ->where("bin",$bin)
          ->whereRaw("date(service_date)>= '$today' " )
          ->select("ba_water_booking.id","bin", "service_date", "book_by", "book_date", "service_date"  ,"book_status",  
          "preferred_time" , DB::Raw("'na' service_code"),DB::Raw("'na' added_to_queue"),DB::Raw("'na' status"),
          "customer_name" , "address", "landmark", "total_cost","payment_type as pay_mode" , 
          "ba_order_sequencer.type" ,DB::Raw("'' service_name")  )
          ->get() ; 

          // $bookid = array();

          // foreach ($water as $items) {
          //     $bookid[] = $items->id;
          // }

          // $basket = DB::table('ba_water_booking_basket')
          // //->join('ba_water_services','ba_water_booking_basket.prsubid','=','ba_water_services.id')
          // ->whereIn('book_id',$bookid)
          // ->select('book_id','service_name','unit_name','brand','unit_value')
          // ->get(); 
           
          // foreach($water as $items)
          // {
          //   foreach ($basket as $list) {
          //        if ($items->id==$list->book_id) {
          //             $items->service_name = $list->service_name;
          //             break;
          //        }
          //   }

          // } 
          
          $data = array('result'=>$water,'sdate'=>$today);

          return view('admin.bookings.view_water_booking_list')->with($data);
}

public function removeBooking (Request $request)
{
     
    $id = $request->code;
    $bin = $request->bin;

    if ($id!=null) 
        {           
                    $delete = DB::table('ba_service_booking')
                    ->where('id', '=', $id)->delete();
    
                    $delete_bs_details = DB::table('ba_service_booking_details')
                    ->where('book_id', '=', $id)->delete(); 

                    if ($delete && $delete_bs_details) {
                       $msg ="record deleted successfull!"; 
                       return redirect('services/view-bookings?bin='.$bin)->with("err_msg",$msg);
                    }else{
                      $msg ="failed to delete record!";
                      return redirect('services/view-bookings?bin='.$bin)->with("err_msg",$msg);     
                    }


              
        }else
            {
                $msg ="mismatch information";        
            }

// return response()->json(array('success' => true, 'msg'=>$msg,'html'=>$returnHTML));
}




public function salesAndServicePerformance(Request $request)
{
    $bin = $request->bin; 

    if($bin == "")
    {
      return redirect("/admin/customer-care/business/view-all")->with("err_msg", "No business selected!");
    }


    $year =  date('Y');
    $month = date('m');

    $result = DB::table('ba_shopping_basket')
    ->select("ba_shopping_basket.pr_name", 
        DB::Raw('sum(ba_shopping_basket.qty) as quantity ,
                 sum(ba_shopping_basket.qty * ba_shopping_basket.price) as actualPrice, 
                 
                 sum(ba_shopping_basket.cgst) as cGST,
                 sum(ba_shopping_basket.sgst) as sGST '))
    ->join('ba_products',"ba_products.pr_code","=","ba_shopping_basket.pr_code")
    ->join('ba_service_booking',"ba_service_booking.id","=","ba_shopping_basket.order_no")
    ->whereYear('service_date',$year)
    ->whereMonth('service_date',$month)
    ->where('ba_products.bin',"=",$bin)
    ->groupBy('ba_shopping_basket.pr_name')
    //->groupBy('ba_shopping_basket.price')
    ->get();

    $dataResult =  array('serv_list' => $result );

    $months=array(); 
    $sales=0;

    foreach ($result as  $value) {
       $sales+= $value->actualPrice;

    }

     $revenueMonthResult = DB::table('ba_service_booking')
    ->select(  
        DB::Raw('sum(if(month(service_date) = 1, seller_payable,0)) as Jan,
                 sum(if(month(service_date) = 2, seller_payable,0)) as Feb ,
                 sum(if(month(service_date) = 3,seller_payable,0)) as Mar ,
                 sum(if(month(service_date) = 4, seller_payable,0)) as  Apr ,
                 sum(if(month(service_date) = 5, seller_payable,0)) as  May ,
                 sum(if(month(service_date) = 6, seller_payable,0)) as  Jun ,
                 sum(if(month(service_date) = 7, seller_payable,0)) as   Jul ,
                 sum(if(month(service_date) = 8, seller_payable,0)) as  Aug ,
                 sum(if(month(service_date) = 9, seller_payable,0)) as  Sep ,
                 sum(if(month(service_date) = 10, seller_payable,0)) as  Oct ,
                 sum(if(month(service_date) = 11, seller_payable,0)) as  Nov ,
                 sum(if(month(service_date) = 12, seller_payable,0)) as  "Dec" '))
        ->whereYear('service_date',$year)
        ->where('bin',"=",$bin)
        ->get();

        $revdata = array();
        
        foreach ($revenueMonthResult as $rev) {
         array_push(
          $revdata,$rev->Jan,$rev->Feb,$rev->Mar,$rev->Apr,$rev->May,
          $rev->Jun,$rev->Jul,$rev->Aug,$rev->Sep,$rev->Oct,$rev->Nov,
          $rev->Dec);
        }


//pnd commission calculation logic

    $PNDCommisionMonthWiseResult = DB::table('ba_pick_and_drop_order')
    ->join('ba_business','ba_pick_and_drop_order.request_by','=','ba_business.id')
    ->select(  
    DB::Raw(
    'sum(if(month(service_date) = 1,total_amount*commission/100,0)) as Jan,
    sum(if(month(service_date) = 2,total_amount*commission/100,0)) as Feb,
    sum(if(month(service_date) = 3,total_amount*commission/100,0)) as Mar,
    sum(if(month(service_date) = 4,total_amount*commission/100,0)) as Apr,
    sum(if(month(service_date) = 5,total_amount*commission/100,0)) as May,
    sum(if(month(service_date) = 6,total_amount*commission/100,0)) as Jun,
    sum(if(month(service_date) = 7,total_amount*commission/100,0)) as Jul,
    sum(if(month(service_date) = 8,total_amount*commission/100,0)) as Aug,
    sum(if(month(service_date) = 9,total_amount*commission/100,0)) as Sep,
    sum(if(month(service_date) = 10,total_amount*commission/100,0))as Oct,
    sum(if(month(service_date) = 11,total_amount*commission/100,0))as Nov,
    sum(if(month(service_date) = 12,total_amount*commission/100,0)) as "Dec" '))
    ->whereYear('service_date',$year)
    ->where('request_by',$bin)
    ->where(DB::Raw('source="booktou" or source="customer" '))
    ->get();

//setting the variable for commision
    $jantotal=0;$febtotal=0;$martotal=0;$aprtotal=0;
    $maytotal=0;$juntotal=0;$jultotal=0;$augtotal=0;
    $septotal=0;$octtotal=0;$novtotal=0;$dectotal=0;
//variable setting ends here

    foreach ($PNDCommisionMonthWiseResult as $commission) {
      $jantotal = $commission->Jan;
      $febtotal = $commission->Feb;
      $martotal = $commission->Mar;
      $aprtotal = $commission->Apr;
      $maytotal = $commission->May;
      $juntotal = $commission->Jun;
      $jultotal = $commission->Jul;
      $augtotal = $commission->Aug;
      $septotal = $commission->Sep;
      $octtotal = $commission->Oct;
      $novtotal = $commission->Nov;
      $dectotal = $commission->Dec;      
    }

    //ends here


       //pnd revenue months
        $PNDrevenueMonthWiseResult = DB::table('ba_pick_and_drop_order')
    ->select(  
        DB::Raw('sum(if(month(service_date) = 1, total_amount,0)) as Jan,
                 sum(if(month(service_date) = 2, total_amount,0)) as Feb ,
                 sum(if(month(service_date) = 3,total_amount,0)) as Mar ,
                 sum(if(month(service_date) = 4, total_amount,0)) as  Apr ,
                 sum(if(month(service_date) = 5, total_amount,0)) as  May ,
                 sum(if(month(service_date) = 6, total_amount,0)) as  Jun ,
                 sum(if(month(service_date) = 7, total_amount,0)) as   Jul ,
                 sum(if(month(service_date) = 8, total_amount,0)) as  Aug ,
                 sum(if(month(service_date) = 9, total_amount,0)) as  Sep ,
                 sum(if(month(service_date) = 10, total_amount,0)) as  Oct ,
                 sum(if(month(service_date) = 11, total_amount,0)) as  Nov ,
                 sum(if(month(service_date) = 12, total_amount,0)) as  "Dec"  '))
        ->whereYear('service_date',$year)
        ->where('request_by',"=",$bin)
        ->get();

       //echo json_encode($revenueMonthResult);die();

    
    $pnddata=array();
    
    foreach ($PNDrevenueMonthWiseResult as  $pnd) {
      array_push(
      $pnddata,
      $pnd->Jan-$jantotal,
      $pnd->Feb-$febtotal,
      $pnd->Mar-$martotal,
      $pnd->Apr-$aprtotal,
      $pnd->May-$maytotal,
      $pnd->Jun-$juntotal,
      $pnd->Jul-$jultotal,
      $pnd->Aug-$augtotal,
      $pnd->Sep-$septotal,
      $pnd->Oct-$octtotal,
      $pnd->Nov-$novtotal,
      $pnd->Dec-$dectotal);
    }

     

    $PNDrevenueMonthResult = DB::table('ba_pick_and_drop_order')
        ->select(  
        DB::Raw('sum(total_amount) as totalAmount'))
        ->whereYear('service_date',$year)
        ->whereMonth('service_date',$month)
        ->where('request_by',"=",$bin)
        ->get();

    $pndAmount = 0;
        foreach ($PNDrevenueMonthResult as  $value) {
        $pndAmount+= $value->totalAmount;

    }  

     $PNDResult = DB::table('ba_pick_and_drop_order')
     ->join('ba_business','ba_pick_and_drop_order.request_by', '=' ,'ba_business.id')
     ->whereYear('service_date',$year)
     ->whereMonth('service_date',$month)
     ->where('request_by',"=",$bin)
     ->select('cust_remarks','source','commission','total_amount')
     ->get();
    //dd($PNDrevenueMonthResult);die();
    
    $datarevenueResult =  array('rev_list' => $revenueMonthResult );
    $dataPNDResult =  array('pnd_list' => $PNDResult );
    
    return view('admin.reports.sales_and_service_performance',
        ['revdata'=>$revdata,'pnddata'=>$pnddata])
    ->with($dataResult)
    ->with($dataPNDResult)
    ->with("sales",$sales)
    ->with("pndsales",$pndAmount)
    ->with($datarevenueResult);

}

 


//section for booking order details by bin
 
public function viewBookingsByBin(Request $request)
{
  return view('admin.bookings.search_bookings');
}


public function autoBusineesName(Request $request)

{
        
      $search = $request->search;

      if($search)
      {
         $result = 
         Business::select('id','name')->where('name', 'like', '%' .$search . '%')
         ->limit(30)
         ->get();
      }

      $response = array();

      foreach($result as $result){

         $response[] = array("label"=>$result->name);

      }
      echo json_encode($response);
      exit;

}


public function getBookingsByBin(Request $request)
{

$name = $request->merchantcode;
$bin=0;

$getBookings = DB::table('ba_business')
              ->select('id')
              ->where('name','=',$name)
              ->get();


if($getBookings)
{

    foreach ($getBookings as $val) {
    $bin = $val->id;
    }

}


if($request->filter_date)
  {
      
      //$today = date('Y-m-d');
      $fromdate = $request->filter_date;
      
      $normal_orders = DB::table("ba_service_booking")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->where("bin",$bin) 
      ->where("ba_order_sequencer.type", "booking")
      //->whereRaw("date(service_date) >= '$today' " )
      ->whereRaw("date(service_date) >=  '$fromdate' " )
      ->select("ba_service_booking.id", "service_date", "book_by", "book_date", "service_date" , "book_status",   DB::Raw("'na' customer_name"), "address", "landmark", "total_cost","payment_type as pay_mode" , 
       "ba_order_sequencer.type" )
      ->paginate(20) ;   

     
       

      
  }
  else 
  {

      $normal_orders = DB::table("ba_service_booking")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->where("bin",$bin)  
      ->where("ba_order_sequencer.type", "booking")
      ->select("ba_service_booking.id", "service_date", "book_by", "book_date", "service_date"  ,"book_status",  
      DB::Raw("'na' service_time"), DB::Raw("'na' service_code"),DB::Raw("'na' added_to_queue"),DB::Raw("'na' status"),
      DB::Raw("'na' customer_name"), "address", "landmark", "total_cost","payment_type as pay_mode" , 
      "ba_order_sequencer.type"   )
      ->paginate(20) ;   
  }
 
    $book_nos = array( '0' );
    $cust_ids = array( '0' );
    foreach($normal_orders as $item)
    {
      $book_nos[] = $item->id;
      $cust_ids[] = $item->book_by;
    }
 
    $booked_services= DB::table("ba_service_booking_details")
    ->whereIn("book_id", $book_nos)
    ->select("book_id", "service_product_id", "service_name", "service_time", "added_to_queue", "status")
    ->get();
     
    $cust_ids = array_filter($cust_ids);
    $cust_ids =  array_unique($cust_ids); 
 
    $customers = DB::table("ba_profile")
    ->whereIn("id", $cust_ids) 
    ->get();
 
    $allorders = array();
    foreach($normal_orders as $item)
    {
      foreach($booked_services as $bookeditem)
        if($bookeditem->book_id == $item->id )
        {
          $item->service_product_id  = $bookeditem->service_product_id;
          $item->service_time = $bookeditem->service_time; 
          $item->added_to_queue = $bookeditem->added_to_queue;
          $item->status = $bookeditem->status; 
          break; 
        }    
        
        $allorders[] = $item;
    }


 
    $keys = array_column($allorders, 'service_time'); 
    //array_multisort($keys, SORT_ASC, $allorders); 
 
    $data = array('orders' =>  $allorders , 'booked_services' => $booked_services,  'customers' => $customers,   'page_title' => 'Manage Listed Services' );


    $returnHTML = view('orders.view_bookings_by_binList')->with($data)->render();
    return response()->json(array('success'=>true,'html'=>$returnHTML));

}




public function removeBookingsByBin (Request $request)
{

    $id = $request->key;

    if ($id!=null) 
        {           
                $name = $request->merchantcode;

                $getBookings = DB::table('ba_business')
                ->select('id')
                ->where('name','=',$name)
                ->get();


                  if($getBookings)
                  {

                      foreach ($getBookings as $val) {
                      $bin = $val->id;
                      }

                  }


                    $delete = DB::table('ba_service_booking')
                    ->where('id', '=', $id)->delete();
    
                    $delete_bs_details = DB::table('ba_service_booking_details')
                    ->where('book_id', '=', $id)->delete();

                    
                    
    
    if($request->filter_date)
    {
      //$today = date('Y-m-d');
      $fromdate = $request->filter_date;
      $normal_orders = DB::table("ba_service_booking")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->where("bin",$bin) 
      ->where("ba_order_sequencer.type", "booking")
      ->whereRaw("date(service_date) >= '$fromdate' " )
      ->select("ba_service_booking.id", "service_date", "book_by", "book_date", "service_date" , "book_status",   DB::Raw("'na' customer_name"), "address", "landmark", "total_cost","payment_type as pay_mode" , 
       "ba_order_sequencer.type"   )
      ->paginate(20) ;   
  }
  else 
  {
      $normal_orders = DB::table("ba_service_booking")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->where("bin",$bin)  
      ->where("ba_order_sequencer.type", "booking")
      ->select("ba_service_booking.id", "service_date", "book_by", "book_date", "service_date"  ,"book_status",  
      DB::Raw("'na' service_time"), DB::Raw("'na' service_code"),DB::Raw("'na' added_to_queue"),DB::Raw("'na' status"),
      DB::Raw("'na' customer_name"), "address", "landmark", "total_cost","payment_type as pay_mode" , 
      "ba_order_sequencer.type"   )
      ->paginate(20) ;   
  }
 
    $book_nos = array( '0' );
    $cust_ids = array( '0' );
    foreach($normal_orders as $item)
    {
      $book_nos[] = $item->id;
      $cust_ids[] = $item->book_by;
    }
 
    $booked_services= DB::table("ba_service_booking_details")
    ->whereIn("book_id", $book_nos)
    ->select("book_id", "service_product_id", "service_name", "service_time", "added_to_queue", "status")
    ->get();
     
    $cust_ids = array_filter($cust_ids);
    $cust_ids =  array_unique($cust_ids); 
 
    $customers = DB::table("ba_profile")
    ->whereIn("id", $cust_ids) 
    ->get();
 
    $allorders = array();
    foreach($normal_orders as $item)
    {
      foreach($booked_services as $bookeditem)
        if($bookeditem->book_id == $item->id )
        {
          $item->service_product_id  = $bookeditem->service_product_id;
          $item->service_time = $bookeditem->service_time; 
          $item->added_to_queue = $bookeditem->added_to_queue;
          $item->status = $bookeditem->status; 
          break; 
        }    
        
        $allorders[] = $item;
    }


 
    $keys = array_column($allorders, 'service_time'); 
    //array_multisort($keys, SORT_ASC, $allorders); 
 
    $data = array('orders' =>  $allorders , 'booked_services' => $booked_services,  'customers' => $customers,   'page_title' => 'Manage Listed Services' );

    
    $returnHTML = view('orders.view_bookings_by_binList')->with($data)->render();


    $msg ="record deleted sucessfully";        
    }else
    {
        $msg ="failed to delete the record";        
    }

    return response()->json(array('success' => true, 'msg'=>$msg,'html'=>$returnHTML));
}
 
  protected function viewMerchantWiseBooking(Request $request)
  {

    if($request->filter_date=="")
    {
        $today = date('Y-m-d');
    }else{
        $today = date('Y-m-d',strtotime($request->filter_date));
    }
     
    $status = ['new', 'confirmed','engaged','in_queue'];
 
    $appointment = DB::table('ba_service_booking')
    ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
    ->whereRaw("date(ba_service_booking.book_date)>='$today'") 
    ->whereIn("ba_order_sequencer.type", array('booking','offers'))
    ->whereIn("ba_service_booking.book_status",$status)
    ->select("ba_service_booking.id","ba_service_booking.id",'ba_order_sequencer.type');

    $allbooking = DB::table('ba_water_booking')
    ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_water_booking.id")
    ->whereRaw("date(ba_water_booking.book_date)>='$today'") 
    ->where("ba_order_sequencer.type", "booking")
    ->whereIn("ba_water_booking.book_status",$status)
    ->union($appointment)
    ->select("ba_water_booking.bin","ba_water_booking.id","ba_order_sequencer.type")
    ->get();
    
    // dd($allbooking);
    $bin = $businessid = $type = $orderno = array();
    foreach ($allbooking as $business) {
      $bin[]=$business->bin; 
    }

    $businessid = array_unique($bin); 
     
    $business = DB::table('ba_business')
    ->whereIn('id',$businessid) 
    ->select()
    ->paginate(20);


    $data = array('biz'=>$business,'booking'=>$allbooking,'today'=>$today);

    return view('admin.bookings.merchant_list')->with('data',$data);

  }



  protected function viewOrderDetails(Request $request, $orderno)
  {
    $bin = $request->bin;
    if($orderno == "" || $request->bin=="")
    {
        return redirect("/services/view-merchant-wise-booking")->with("err_msg", "Missing parameter!"); 
    }

    $ordersequence = OrderSequencerModel::find($orderno);
    if( !isset($ordersequence))
    {
        return redirect("/services/view-merchant-wise-booking")->with("err_msg", "No matching order found!"); 
    }

    $business = Business::find($bin) ;

    



    if (!isset($business) )
    {
        return redirect("/services/view-merchant-wise-booking")->with("err_msg", "No matching business found!"); 
    }

        if(strcasecmp($business->sub_module,"WTR")==0) {

          $order_info   = DB::table("ba_water_booking") 
          ->join("ba_business","ba_water_booking.bin", "=", "ba_business.id")
          ->select("ba_water_booking.*",DB::Raw("'0' discount"),  "ba_business.name as businessName",
            "ba_business.locality as businessLocality", "ba_business.landmark as businessLandmark",
             "ba_business.city as businessCity", "ba_business.state as businessState", 
             "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",  
             "ba_business.banner",DB::Raw("'0' offerAmount"),DB::Raw("'0' purchaseId") )
          ->where("ba_water_booking.id", $orderno )    
          ->first() ;
           // dd($business->sub_module);
             
        
        } else {

          $order_info   = DB::table("ba_service_booking") 
          ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
          ->select("ba_service_booking.*", "ba_business.name as businessName",
            "ba_business.locality as businessLocality", "ba_business.landmark as businessLandmark",
             "ba_business.city as businessCity", "ba_business.state as businessState", 
             "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",  
             "ba_business.banner",DB::Raw("'0' offerAmount"),"offer_purchaseid as purchaseId")
          ->where("ba_service_booking.id", $orderno )    
          ->first();

        }
            
        if( !isset($order_info))
        {
            return redirect("/services/view-merchant-wise-booking")->with("err_msg", "No matching order found!"); 
        }


        $offer = DB::table("ba_shopping_basket_srv_products")
          ->where("id",$order_info->purchaseId)
          ->where("order_no",$order_info->id)
          ->first();         
          if (isset($offer)) {
               $order_info->offerAmount = $offer->price;
          } 


          $customer_info   = DB::table("ba_profile") 
          ->where("id", $order_info->book_by )    
          ->first() ;         
          if( !isset($customer_info))
          {
            abort(404); 
          }

          if(strcasecmp($ordersequence->type,"booking") ==0 && strcasecmp($business->sub_module,"WTR")==0) {
                
                $booking_details   = DB::table("ba_water_booking")
                ->join("ba_water_booking_basket","ba_water_booking.id","=","ba_water_booking_basket.book_id")
                ->join("ba_water_services","ba_water_booking_basket.prsubid",
                      "=","ba_water_services.id")  
                ->where("ba_water_booking.id", $orderno )
                ->select()    
                ->get();
                $staff=""; 
                $result_venue=""; 
                 $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ; 

            }
       elseif($ordersequence->type=="offers")
       {
            $booking_details   = DB::table("ba_service_booking")
                    ->join("ba_shopping_basket_srv_products","ba_service_booking.id","=","ba_shopping_basket_srv_products.order_no")  
                    ->where("ba_service_booking.id", $orderno )
                    ->select('*')    
                    ->get();
                
                     
                    $staff = DB::table('ba_service_booking')
                    ->join('ba_users' ,'ba_service_booking.staff_id', '=', 'ba_users.profile_id')
                    ->join('ba_service_booking_details','ba_service_booking.id', '=', 'ba_service_booking_details.book_id')
                    ->join('ba_profile','ba_users.profile_id','=','ba_profile.id')
                    ->where("ba_service_booking.id",'=',$orderno)
                    ->select('ba_profile.fullname','ba_profile.email')
                    ->groupby('ba_profile.fullname','ba_profile.email')
                    ->get();

                    $result_venue = DB::table('ba_service_booking')
                    ->join('ba_business','ba_service_booking.bin','=', 'ba_business.id')
                    ->where("ba_service_booking.id",'=',$orderno)
                    ->select()
                    ->get();
                     $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ;
       }


            else{
                    $booking_details   = DB::table("ba_service_booking")
                    ->join("ba_service_booking_details","ba_service_booking.id","=","ba_service_booking_details.book_id")  
                    ->where("ba_service_booking.id", $orderno )
                    ->select('*')    
                    ->get();
                
                     
                    $staff = DB::table('ba_service_booking')
                    ->join('ba_users' ,'ba_service_booking.staff_id', '=', 'ba_users.profile_id') 
                    ->join('ba_profile','ba_users.profile_id','=','ba_profile.id')
                    ->where("ba_service_booking.id",'=',$orderno)
                    ->select('ba_profile.fullname','ba_profile.email')
                    ->groupby('ba_profile.fullname','ba_profile.email')
                    ->get();

                    $result_venue = DB::table('ba_service_booking')
                    ->join('ba_business','ba_service_booking.bin','=', 'ba_business.id')
                    ->where("ba_service_booking.id",'=',$orderno)
                    ->select()
                    ->get();
                     $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ;

            }

        $staffs = DB::table('ba_profile')
            ->join('ba_users' ,'ba_profile.id', '=', 'ba_users.profile_id')
            ->where("ba_users.bin", $bin )
            ->select('ba_profile.id', 'ba_profile.fullname','ba_profile.email') 
            ->get();


        $order_items = "";
        $agent_info = "";
        $result = array('order_info' => $order_info , 
                        'customer' =>  $customer_info, 
                        'booking'=>$booking_details,
                        'order_type'=>$ordersequence->type,
                        'staff'=>$staff,
                        'staffs'=>$staffs,
                        'venue'=>$result_venue,
                        'scode'=>$business->sub_module,
                        'business'=>$business,
                        "remarks"=>$remarks,
                        "offer"=> $offer
                         );
        return view("admin.bookings.view_booking_orders")->with('data',$result); 


    
    }

    // update booking status

    public function updateBookingStatus(Request $request)
    {

        
        $id= $request->bookingno;
        $status = $request->status;
        $remark = $request->remarks;

        if (isset($request->filter_date) && isset($request->merchantcode)) 
        {    
            // if request parameter contains date and merchant code 
          $filter_date = $request->filter_date;
          $merchantcode = $request->merchantcode;
          

            if(isset($id))
            {

              $booking = ServiceBooking::find($id);
               
              $booking->book_status=$status;
              $booking->cc_remarks=$remark;
              
              $update = $booking->save();

              if($update)
              {
                $msg = "Status updated!";
                //return view('orders.view_bookings_by_bin');
                return redirect()->back()
                ->with("merchantcode",$merchantcode)
                ->with("filter_date",$filter_date);
              }else{
                $msg ="Failed to update status";
                return redirect()->back()
                ->with("merchantcode",$merchantcode)
                ->with("filter_date",$filter_date);
              }  
            }



        }else{

          
          //if request contains only id 
          if(isset($id))
            {
              $booking = ServiceBooking::find($id);
               
              $booking->book_status=$status;
              $booking->cc_remarks=$remark;
              
              $update = $booking->save();

              if($update)
              {
                $msg = "Status updated!";
                //return view('orders.view_bookings_by_bin');
                return redirect()->back();
              }else{
                $msg ="Failed to update status";
                return redirect()->back();
              }  
            }
        }     
        
    }

    public function updateWaterBookingStatus(Request $request)
    {

        
        $id= $request->bookingno;
        $status = $request->status;
        $remark = $request->remarks;
        $bin = $request->bin;
        $date = $request->sdate; 

        if ($id=="") {
               return redirect('services/view-water-bookings?bin='.$bin.'&sdate='.$date.'');
          }  
          //if request contains only id 
          if(isset($id))
            {
              $booking = WaterBookingModel::find($id);
               
              $booking->book_status=$status;
              $booking->cc_remarks=$remark;
              
              $update = $booking->save();

              if($update)
              {
                $msg = "Status updated!";
                //return view('orders.view_bookings_by_bin');
                return redirect()->back();
              }else{
                $msg ="Failed to update status";
                return redirect()->back();
              }  
            }
              
        
    }

    protected function updateCustomerRemarks(Request $request)
    {

        $orderid = $request->orderkey;
        $bin = $request->bin;

        if ($orderid=="") {
             return redirect('/services/view-merchant-wise-booking');
        }

        $remark = ServiceBooking::find($orderid);
        $remark->cc_remarks = $request->ccremarks;
        $update = $remark->save();

        if ($update) {
            return redirect('/services/booking/view-details/'.$orderid.'?bin='.$bin)->with('err_msg','cc remakrs updated');
        }else{
            return redirect('/services/booking/view-details/'.$orderid.'?bin='.$bin)->with('err_msg','failed!');
        } 
    }

    //upload sns service category image
    protected function snsBusinessList(Request $request)
    {
        $bookable_module = DB::table("ba_bookable_category")
        ->select(DB::Raw('distinct (main_module) as moduleName'))
        ->where('main_module','!=','SHOPPING')
        ->get();

        $sns_business = DB::table("ba_business")
        ->where("main_module","Appointment")->get();

        $booking = DB::table("ba_business") 
        ->where("main_module","Booking")
        ->get();

        $data = array("paa"=>$booking,'sns'=>$sns_business,'module'=>$bookable_module);

        if ($request->bin!="") {
            $bin =$request->bin;
            $servie_products = DB::table("ba_service_products") 
                         ->where("bin",$bin)
                         ->select("ba_service_products.*")
                         ->paginate(20);

            $data = array("paa"=>$booking,'sns'=>$sns_business,'module'=>$bookable_module,"products"=>$servie_products);
        } 
 
        return view('admin.bookings.view_sns_service_category')->with($data);
    }

    protected function saveServiceCategoryPhoto(Request $request)
    {
      if ($request->key=="") {
            
            return redirect('services/sns-business-list');
        }
        $cdn_url = config('app.app_cdn'); 
        $cdn_path =  config('app.app_cdn_path'); 

        $id = $request->key; 
        $folder_name = 'sns_'. $request->key; 
        $folder_path =  $cdn_path."/assets/upload/".'sns-'. $request->key;

        
        $service_product = ServiceProductModel::find($id);
        $bin = $service_product->bin;
        $service_product->srv_name = $request->service_name;
        if ($request->hasFile('photo')) {

            if( !File::isDirectory($folder_path))
                {
                  File::makeDirectory( $folder_path, 0777, true, true);
                } 
      
              $file = $request->photo;
              $originalname = $file->getClientOriginalName(); 
              $extension = $file->extension( );
              $filename = $request->key. time()  . ".{$extension}";
              $file->storeAs('upload',  $filename );
              $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
              $file_names[] =  $filename ;
              $folder_url  = $cdn_url.'/assets/upload/sns-'. $request->key."/".$filename ;

              $service_product->photos=$folder_url;
        }

              $save = $service_product->save();

              if ($save) {
                $msg = "save!";
                 return redirect('services/sns-business-list?bin='.$bin)->with('err_msg',$msg);
              }else{
                $msg = "failed!";
                 return redirect('services/sns-business-list?bin='.$bin)->with('err_msg',$msg);
              }

    }

    //sns part ends here

    public function downloadWaterBookingReceipt( Request $request)
    {
      $order_info = $order_items =null;
      if( $request->o  != ""  )
      {


        $order_type = OrderSequencerModel::find($request->o);
        if( !isset($order_type)  )
        { return ; 
        }
 
          
          $order_info = WaterBookingModel::find( $request->o );

          $business = Business::find($order_info->bin);
          $order_items = DB::table("ba_water_booking_basket")
          ->join('ba_water_services','ba_water_booking_basket.prsubid','=','ba_water_services.id')
          ->where("book_id", $order_info->id)
          ->select(
            
          )
          ->get();     


          $pdf_file =  "rcp-" .time(). '-' . $request->o . ".pdf";
          $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ; 

          $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
          ->setPaper('a4', 'portrait')
          ->loadView('admin.orders.booking_bill_small_size' ,  
            array('data'=>$order_info , 
                  'order_items' => $order_items,'scode'=>$business->sub_module,
                  'business'=>$business));
           
          // return view("admin.orders.booking_bill_small_size")->with(array('data'=>$order_info , 
          //         'order_items' => $order_items,'scode'=>$business->sub_module));

  

       return $pdf->download( $pdf_file  ); 

      }
      else
      {
        return ;
      } 
    }




     //adding timeslot

  public function saveTimeSlot(Request $request)
  {

            if(isset($request->btn_save) && $request->btn_save == "save")
            {
                $bin = $request->bin;


            $business = Business::find($request->bin);
            if( !isset($business))
            {
              $data= ["message" => "failure",    "status_code" =>  999 , 'detailed_msg' => 'Business information not found!'  ];
              return json_encode( $data);
            }

            $business_category = BusinessCategory::where("name", $business->category)->first();
            if( !isset($business_category))
            {
              $data= ["message" => "failure",    "status_code" =>  999 , 'detailed_msg' => 'Business category not found!'  ];
              return json_encode( $data);
            }

            

                $dayNames = $request->dayNames;
                $openTimes = $request->openTimes;
                $closeTimes = $request->closeTimes;
                $breakUpTimes = $request->breakUpTimes;

                $base_size = count($dayNames);



                if (   count($openTimes) == $base_size   && 
                    count($closeTimes)  == $base_size  && 
                    count($breakUpTimes)  == $base_size ) 
                {

                    for($i=0; $i < $base_size; $i++ )
                    {

                        $dayName = $dayNames[$i];
                        $openTime = $openTimes[$i];
                        $closeTime = $closeTimes[$i];
                        $breakUpTime = $breakUpTimes[$i];

                        //check main module

                        if( strcasecmp(   $business->main_module ,"appointment") == 0)
                        {
                          $business_hours = BusinessHour::where('bin',$request->bin)
                          ->get();
                          $found = false ;
                          foreach($business_hours  as $item)
                          {
                            if($item->day_name == $dayName  )
                            {
                              $found = true; break;
                          }
                        }
                      $reset_slots= false;
                      if($found)
                      {
                        DB::table("ba_shop_hours")
                        ->where("bin",  $request->bin)
                        ->where("day_name",  $dayName)
                        ->update([ 'day_open' => date('H:i:s', strtotime($openTime)) ,
                            'day_close' => date('H:i:s', strtotime($closeTime)),
                            'slot_duration' => $breakUpTime    ]);
                        $message = "success";
                        $err_code = 1061;
                        $err_msg = "Service slot saved.";
                        $reset_slots= true;
                    }
                    else
                    {
                        $business_hour = new  BusinessHour;
                        $business_hour->bin = $request->bin;
                        $business_hour->day_name = $dayName;
                        $business_hour->day_open =  date('H:i:s', strtotime($openTime));
                        $business_hour->day_close = date('H:i:s', strtotime($closeTime));
                        $business_hour->slot_duration = $breakUpTime;
                        $save =$business_hour->save();
                        if(  $save  )
                        {
                          $reset_slots= true;
                          $message = "success";
                          $err_code = 1061;
                          $err_msg = "Service slots saved.";
                      }
                      else
                      {
                          $message = "failure";
                          $err_code = 1062;
                          $err_msg = "Service slots could not be saved!";
                      }
                  }

              //prepare slots for the month

                  $month =     date('m');
                  $year =   date('Y');

                  $any_staff  =   DB::table("ba_users")
                  ->select(DB::Raw("'0' as profile_id") )
                  ->distinct();

                  $staffs  =   DB::table("ba_users")
                  ->select("profile_id"  )
                  ->where("bin", $request->bin)
                  ->whereIn("category", array("1", '10' ) )
                  ->union($any_staff)
                  ->get();

                  $all_slots = DB::table("ba_services_duration")
                  ->where("bin", $request->bin)
                  ->orderBy("day_name","asc")
                  ->orderBy("slot_number","asc")
                  ->get();

                  $all_days = DB::table("ba_service_days")
                  ->where("bin", $request->bin)
                  ->whereMonth("service_date", $month )
                  ->whereYear("service_date", $year)
                  ->get();

                  $days = cal_days_in_month(CAL_GREGORIAN, $month , $year );

                  if( $month + 1 <= 12 )
                  {
                      $max_month =  $month + 1 ;
                  }
                  else{
                      $max_month = 12;
                  }



                  $duration = "+" . $breakUpTime . " minutes" ;
                  if(  $reset_slots  )
                  {

               //resetting slots ranges
                     DB::table("ba_services_duration")
                     ->where("bin",  $request->bin)
                     ->where("day_name",  $dayName)
                     ->delete();

                  //saving new slots ranges
                     $start_time = $openTime;
                     $slotno=1;
                     while(true)
                     {
                        $startTime = date("H:i:s", strtotime( $start_time  ));
                        $service_duration = new ServiceDuration;
                        $service_duration->bin =$request->bin;
                        $service_duration->day_name =$dayName;
                        $service_duration->start_time = $startTime ;
                        $service_duration->slot_number = $slotno;

                        if( strtotime( $duration , strtotime( $start_time  ) )  <   strtotime( $closeTime )  )
                        {
                          $endTime = date("H:i:s", strtotime( $duration ,  strtotime( $start_time  ) ));
                          $service_duration->end_time = $endTime ;
                          $service_duration->status = "open";
                          $service_duration->save();
                          $start_time = $endTime;
                      }
                      else
                      {
                          $endTime = date("H:i:s", strtotime( $closeTime ) );
                          $service_duration->end_time = $endTime ;
                          $service_duration->status = "open";
                          $service_duration->save();
                          break;
                      }
                      $slotno++;
                  }
              }

          }

        }

        }
        else
        {
            return redirect("/admin/business/time-slot-listing/" . $bin)->with("err_msg", "Slot configuration has missing value!");
        }



        }

}

protected function viewPicnicBooking(Request $request)
{



      if( isset($request->filter_date))
      {
        $seek_date = date('Y-m-d', strtotime($request->filter_date)) ; 
      }

      else 
      {
        $seek_date = date('Y-m-d' , strtotime("10 days ago"))  ; 
      }
        

      if( !isset($request->btn_search) )
      {
        echo "search not set";
        // Return to view with default search 
      }


        $filter_by = $request->filter_by;
        $filter_status = $request->status; 



        if( $filter_status == "-1")
        {
          $status = array( 'new','confirmed', 'cancel_by_client',
            'cancel_by_owner','cancelled' ) ; 
        }
        elseif(!isset($filter_status))
        {
            $status = array('new','confirmed') ; 
        }
        
        else 
        {
            $status = array($filter_status) ; 
        }

        if(strcasecmp($filter_by,"booking_date")==0)
        {
            $paa_booking = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id") 
          ->where("ba_order_sequencer.type", "booking")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate('ba_service_booking.service_date','=',$seek_date)
          ->select("ba_service_booking.id","ba_service_booking.bin", "ba_service_booking.service_date", "ba_service_booking.book_by",
           "ba_service_booking.book_date","ba_service_booking.book_status", "ba_service_booking.preferred_time" , 
          "ba_service_booking.customer_name", "ba_service_booking.customer_phone", "ba_service_booking.address", "ba_service_booking.landmark", "total_cost",
          "ba_service_booking.payment_type as pay_mode", "ba_service_booking.payment_status", "ba_order_sequencer.type","ba_service_booking.gst")
          ->get();

        }

        else if(strcasecmp($filter_by,"service_date")==0)
        {
             $paa_booking = DB::table("ba_service_booking")
             ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
          ->where("ba_order_sequencer.type", "booking")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate("ba_service_booking.book_date",'=',$seek_date )
         ->select("ba_service_booking.id","ba_service_booking.bin", "ba_service_booking.service_date", "ba_service_booking.book_by",
           "ba_service_booking.book_date","ba_service_booking.book_status", "ba_service_booking.preferred_time" , 
          "ba_service_booking.customer_name", "ba_service_booking.customer_phone", "ba_service_booking.address", "ba_service_booking.landmark", "total_cost",
          "ba_service_booking.payment_type as pay_mode", "ba_service_booking.payment_status", "ba_order_sequencer.type","ba_service_booking.ba_service_booking.gst")
         ->get();
        }

        else
        {
            $paa_booking = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
          ->where("ba_order_sequencer.type", "booking")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate("ba_service_booking.book_date",'>=',$seek_date )
          ->select("ba_service_booking.id","ba_service_booking.bin", "ba_service_booking.service_date", "ba_service_booking.book_by",
           "ba_service_booking.book_date","ba_service_booking.book_status", "ba_service_booking.preferred_time" , 
          "ba_service_booking.customer_name", "ba_service_booking.customer_phone", "ba_service_booking.address", "ba_service_booking.landmark", "total_cost",
          "ba_service_booking.payment_type as pay_mode", "ba_service_booking.payment_status", "ba_order_sequencer.type","ba_service_booking.gst")
          ->get();


        }
          
          $data = array('result'=>$paa_booking);

          return view('admin.bookings.view_picnic_booking_list')->with($data);
}


protected function viewAllActiveAppointment(Request $request)
{

//     dd($request);
// "filter_date" => "19-10-2022"
//       "filter_by" => "booking_date"
//       "status" => "new"
//       "btn_search" => "search"

      if( isset($request->filter_date))
      {
        $seek_date = date('Y-m-d', strtotime($request->filter_date)) ; 
      }

      else 
      {
        $seek_date = date('Y-m-d' , strtotime("10 days ago"))  ; 
      }
        

      if( !isset($request->btn_search) )
      {
        echo "search not set";
        // Return to view with default search 
      }


        $filter_by = $request->filter_by;
        $filter_status = $request->status; 



        if( $filter_status == "-1")
        {
          $status = array( 'new','confirmed', 'cancel_by_client',
            'cancel_by_owner','cancelled' ) ; 
        }
        elseif(!isset($filter_status))
        {
            $status = array('new','confirmed') ; 
        }
        
        else 
        {
            $status = array($filter_status) ; 
        }


        if(strcasecmp($filter_by,"service_date")==0)
        {
            $appointment_booking = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
          ->join("ba_service_booking_details","ba_service_booking_details.book_id", "=", "ba_service_booking.id") 
          ->where("ba_order_sequencer.type", "appointment")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate('service_date','=',$seek_date)
          ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type","ba_service_booking_details.service_time")
          ->get();

        }

        else if(strcasecmp($filter_by,"booking_date")==0)
        {
             $appointment_booking = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
          ->join("ba_service_booking_details","ba_service_booking_details.book_id", "=", "ba_service_booking.id") 
          ->where("ba_order_sequencer.type", "appointment")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate("ba_service_booking.book_date",'=',$seek_date )
          ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type","ba_service_booking_details.service_time")
          ->get();
        }

        else
        {
            $appointment_booking = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
          ->join("ba_service_booking_details","ba_service_booking_details.book_id", "=", "ba_service_booking.id") 
          ->where("ba_order_sequencer.type", "appointment")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate("ba_service_booking.book_date",'>=',$seek_date )
          ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type","ba_service_booking_details.service_time")
          ->get(); 


        }
          
          $data = array('appointment_booking'=>$appointment_booking);

          return view('admin.bookings.view_appointment_list')->with($data);
}


protected function viewAllOffers(Request $request)
{

      if( isset($request->filter_date))
      {
        $seek_date = date('Y-m-d', strtotime($request->filter_date)) ; 
      }

      else 
      {
        $seek_date = date('Y-m-d' , strtotime("10 days ago"))  ; 
      }
        

      if( !isset($request->btn_search) )
      {
        echo "search not set";
        // Return to view with default search 
      }


        $filter_by = $request->filter_by;
        $filter_status = $request->status; 



        if( $filter_status == "-1")
        {
          $status = array( 'new','confirmed', 'cancel_by_client',
            'cancel_by_owner','cancelled' ) ; 
        }
        elseif(!isset($filter_status))
        {
            $status = array('new','confirmed') ; 
        }
        
        else 
        {
            $status = array($filter_status) ; 
        }


        if(strcasecmp($filter_by,"service_date")==0)
        {
            $offers = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id") 
          ->where("ba_order_sequencer.type", "offers")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate('service_date','=',$seek_date)
          ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
          ->get();

        }

        else if(strcasecmp($filter_by,"booking_date")==0)
        {
             $offers = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id") 
          ->where("ba_order_sequencer.type", "offers")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate("ba_service_booking.book_date",'=',$seek_date )
          ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
          ->get();
        }

        else
        {
            $offers = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id") 
          ->where("ba_order_sequencer.type", "offers")
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate("ba_service_booking.book_date",'>=',$seek_date )
          ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
          ->get(); 


        }
          
          $data = array('offers'=>$offers);

          return view('admin.bookings.view_offers_order_list')->with($data);
}



protected function viewAllWaterOrders(Request $request)
{

      if( isset($request->filter_date))
      {
        $seek_date = date('Y-m-d', strtotime($request->filter_date)) ; 
      }

      else 
      {
        $seek_date = date('Y-m-d' , strtotime("7 days ago"))  ; 
      }
        

      if( !isset($request->btn_search) )
      {
        echo "search not set";
        // Return to view with default search 
      }


        $filter_by = $request->filter_by;
        $filter_status = $request->status; 



        if( $filter_status == "-1")
        {
          $status = array( 'new','confirmed', 'cancel_by_client',
            'cancel_by_owner','cancelled' ) ; 
        }
        elseif(!isset($filter_status))
        {
            $status = array('new','confirmed') ; 
        }
        
        else 
        {
            $status = array($filter_status) ; 
        }


        if(strcasecmp($filter_by,"service_date")==0)
        {
            $water_orders = DB::table("ba_water_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_water_booking.id") 
          ->where("ba_order_sequencer.type", "booking")
          ->where("ba_water_booking.book_category", "WATER")
          ->whereIn("ba_water_booking.book_status",$status)
          ->whereDate('service_date','=',$seek_date)
          ->select("ba_water_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
          ->get();

        }

        else if(strcasecmp($filter_by,"booking_date")==0)
        {
              $water_orders = DB::table("ba_water_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_water_booking.id") 
          ->where("ba_order_sequencer.type", "booking")
          ->where("ba_water_booking.book_category", "WATER")
          ->whereIn("ba_water_booking.book_status",$status)
          ->whereDate('service_date','=',$seek_date)
          ->select("ba_water_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
          ->get();
        }

        else
        {
            $water_orders = DB::table("ba_water_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_water_booking.id") 
          ->where("ba_order_sequencer.type", "booking")
          ->where("ba_water_booking.book_category", "WATER")
          ->whereIn("ba_water_booking.book_status",$status)
          ->whereDate('ba_water_booking.service_date','>=',$seek_date)
          ->select("ba_water_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
          ->get();

        }
          
          $data = array('water_orders'=>$water_orders);

          return view('admin.bookings.view_all_water_orders')->with($data);
}





  public function updatePicnicBookingStatus(Request $request)
    {
   
        $id= $request->bookingno;
        $status = $request->status;
        $remark = $request->remarks;
        $bin = $request->bin;
        $date = $request->sdate; 

        if ($id=="") {
               return redirect('services/view-picnic-bookings?bin='.$bin.'&sdate='.$date.'');
          }  
          //if request contains only id 
          if(isset($id))
            {
              $booking = ServiceBooking::find($id);
               
              $booking->book_status=$status;
              $booking->cc_remarks=$remark;
              
              $update = $booking->save();

              if($update)
              {
                $msg = "Status updated!";
                //return view('orders.view_bookings_by_bin');
                return redirect()->back();
              }else{
                $msg ="Failed to update status";
                return redirect()->back();
              }  
        }       
    }

      public function downloadPicnicBookingReceipt( Request $request)
    {
      $order_info = $order_items =null;
      if( $request->o  != ""  )
      {


        $order_type = OrderSequencerModel::find($request->o);
        if( !isset($order_type)  )
        { return ; 
        }
 
          
          $order_info = WaterBookingModel::find( $request->o );

          $business = Business::find($order_info->bin);

          $order_items = DB::table("ba_water_booking_basket")
          ->join('ba_water_services','ba_water_booking_basket.prsubid','=','ba_water_services.id')
          ->where("book_id", $order_info->id)
          ->select(
            
          )
          ->get();


          $pdf_file =  "rcp-" .time(). '-' . $request->o . ".pdf";
          $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ; 

          $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
          ->setPaper('a4', 'portrait')
          ->loadView('admin.orders.booking_bill_small_size' ,  
            array('data'=>$order_info , 
                  'order_items' => $order_items,'scode'=>$business->sub_module));
           
          // return view("admin.orders.booking_bill_small_size")->with(array('data'=>$order_info , 
          //         'order_items' => $order_items,'scode'=>$business->sub_module));

  

       return $pdf->download( $pdf_file  ); 

      }
      else
      {
        return ;
      } 
    }


} 