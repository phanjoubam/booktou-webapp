<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;

use App\User; 
use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;
use App\BusinessPaymentModel;
use App\Franchise;
use App\BusinessCommissionModel;
use App\DailyDepositModel;
use App\FeedbackMerchant;
use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\MonthlyAgentsOrdersExport;
use App\Exports\DailyAgentsOrdersExport;
use App\Exports\DailySalesOrderExport;
use App\Traits\Utilities; 
use PDF;
use App\DepositTargetModel;
use App\ProductVariantModel;
use App\ImportProductFileModel;
use App\SectionTypeModel;
use App\SectionDetailsModel;
use App\PopularcategoryModel;

class PopularcategoryController extends Controller
{
   protected function addPopularCategory(Request $request)
    
    {       
        $category = DB::table("ba_bookable_category")
    	->select(DB::raw('distinct(main_module)as main'))
        // ->where('is_open','open')
    	->get();

        $data = array('category'=>$category);
        return view('admin.popular_category.add_category')->with($data);   
    }

  public function savePopularCategory(Request $request)
       {
        
        $cdn_url = config('app.app_cdn'); 
        $cdn_path =  config('app.app_cdn_path'); 

        $folder_name = 'category_'. $request->mainmodule;  
        $folder_path =  $cdn_path."/assets/upload/".'category_'. $request->mainmodule; 

        $popular_category = new PopularcategoryModel;

        // if image is found
        if ($request->hasFile('photo')) {
            if( !File::isDirectory($folder_path))
                {
                  File::makeDirectory( $folder_path, 0777, true, true);
                }  
              $file = $request->photo;
              $originalname = $file->getClientOriginalName(); 
              $extension = $file->extension( );
              $filename = $request->type. time()  . ".{$extension}";
              $file->storeAs('upload',  $filename );
              $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
              $file_names[] =  $filename ;
              $folder_url  = $cdn_url.'/assets/upload/category_'.$request->mainmodule."/".$filename ;
              $popular_category->image = $folder_url;
        }
        // 

        $popular_category->main_module = $request->mainmodule;
        $popular_category->sub_module = $request->submodule;
        $popular_category->category = $request->category;
        $popular_category->display_name = $request->display_name;
 
        $save = $popular_category->save();
  
        if ($save) {
            return redirect('/admin/systems/add-popular-category')->with('detailed_msg',' save successfully!');
        }  
     return redirect('/admin/systems/add-popular-category');   
    }

    protected function viewSubmoduleCategory(Request $request)
    {
        if ($request->items=="") {
             return response()->json(array('success'=>false));
        }

        $module = $request->items;

        $submodule = DB::table('ba_bookable_category')
        ->where('main_module',$module)
        ->get();

        $category = DB::table('ba_business_category')
        ->where('main_module',$module)
        ->get();

        $data = array('category'=>$category,'submodule'=>$submodule);

        $returnHTML= view('admin.popular_category.view_submodule_category')->with($data)->render();

        return response()->json(array('success'=>true,'html'=>$returnHTML));

    }

    protected function popularCategoryRecord(Request $request)
    {
        $category = DB::table('app_popular_category')
        ->select('app_popular_category.*')
        ->get();
      

        $submodule = DB::table('ba_bookable_category')
       
        ->get();

        $categorys = DB::table('ba_business_category')
       
        ->get();
        $data = array('category'=>$category,'submodule'=>$submodule,'categorys'=>$categorys);


        return view('admin.popular_category.category_update')->with($data);

       
    }

    protected function deleteCategory(Request $request)
    {

        if ($request->key=="") {
            return redirect("/admin/systems/popular-category-record")
            ->with("err_msg","field missing!");
        }

        $key = $request->key;

        $delete = DB::table("app_popular_category")
        ->where("id",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/systems/popular-category-record")
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/admin/systems/popular-category-record")
            ->with("err_msg","failed!");
        }

    }
     protected function updateCategory(Request $request)
    {
        

        if ($request->key=="") 
        {
            return redirect("/admin/systems/popular-category-record")->with("err_msg","field missing!");
        }
         

        $key = $request->key;
        $popular_category = PopularcategoryModel::find($key);
        $cdn_url = config('app.app_cdn'); 
        $cdn_path =  config('app.app_cdn_path'); 

        $folder_name = 'category_'. $request->mainmodule;  
        $folder_path =  $cdn_path."/assets/upload/".'category_'. $request->mainmodule;
         // if image is found
        if ($request->hasFile('photo')) {
            if( !File::isDirectory($folder_path))
                {
                  File::makeDirectory( $folder_path, 0777, true, true);
                }  
              $file = $request->photo;
              $originalname = $file->getClientOriginalName(); 
              $extension = $file->extension( );
              $filename = $request->type. time()  . ".{$extension}";
              $file->storeAs('upload',  $filename );
              $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
              $file_names[] =  $filename ;
              $folder_url  = $cdn_url.'/public/assets/upload/category_'. $request->type."/".$filename ;
              $popular_category->image = $folder_url;
        }
        // 
        $popular_category->category = $request->category;
        $popular_category->main_module = $request->mainmodule;
        $popular_category->sub_module = $request->submodule;
        

        $save = $popular_category->save();
        if ($save) {
            return redirect("/admin/systems/popular-category-record")
            ->with("err_msg","record udpate successfully!");
        }else{
            return redirect("/admin/systems/popular-category-record")
            ->with("err_msg","failed!");
        }
    }

 protected function addPremiumBusiness(Request $request)

 {

     $mainmodule = DB::table("ba_business")
      ->select(DB::raw('distinct(main_module)as main'))
      ->get();

    $business = DB::table('ba_business')        
    ->where('is_open','open')
    ->where('is_block','no')       
    ->get(); 
    

    $data = array("business"=>$business,"mainmodule"=>$mainmodule);
    return view("admin.store.add_premium_business")->with('data',$data);
    
 }

 protected function savePremiumBusiness(Request $request)
 {
      if($request->btnSave =="save")
      {        
       $businessid  = $request->id; 
       $business_info =  Business::find($businessid);
       if (!$business_info) 
       {
         return redirect("/admin/business/add-premium-business")->with("err_msg", "No business record found!." ); 
       }
       $prem_busi_info = PremiumBusiness::where('bin', $business_info->id)
       ->first();      
        if( !isset($prem_busi_info))
        {
          $prem_busi_info = new PremiumBusiness; 
        } 
        $prem_busi_info->bin  = $business_info->id; 
        $prem_busi_info->start_from  = date('Y-m-d H:i:s',strtotime($request->start_from)); 
        $prem_busi_info->ended_on = date("Y-m-d H:i:s",strtotime($request->ended_on));
        $prem_busi_info->save();
        return redirect("/admin/business/add-premium-business")->with("err_msg", "Add premium business  successfully." );
    }
    else
    {
      return redirect("/admin/business/add-premium-business")->with("err_msg", "Add premium business failed!." );
    }
   }
   protected function viewPremiumBusinessList(Request $request)
    {
        if ($request->items=="") {
             return response()->json(array('success'=>false));
        }

        $module = $request->items;
        $business = DB::table('ba_business')
        ->where('main_module',$module)
        ->where('is_open','open') 
        ->get();
      
        $data = array('business'=>$business);

        $returnHTML= view('admin.store.view_premium_business_list')->with($data)->render();

        return response()->json(array('success'=>true,'html'=>$returnHTML));

    }
  protected function viewPremiumBusiness(Request $request)
  {
    
    $businessData= DB::table("ba_business") 
    ->join("ba_premium_business", "ba_premium_business.bin", "=", "ba_business.id")
    // ->where("category",'VARIETY STORE') 
      ->select("ba_business.id",
               "ba_premium_business.id as premiumid",
               "ba_premium_business.bin",
               "name",
               "ended_on",
               "start_from",
               "locality",
               "landmark","city","state","rating","pin","phone_pri","category")
      ->get();

       $data = array("premium_info"=>$businessData); 
       return view("admin.store.view_premium_business")->with('data',$data);
    }
protected function deletePremiumBusiness(Request $request)
    {

        if ($request->key=="") {
            return redirect("/admin/business/view-premium-business")
            ->with("err_msg","field missing!");
        }

        $key = $request->key;

        $delete = DB::table("ba_premium_business")
        ->where("bin",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/business/view-premium-business")
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/admin/business/view-premium-business")
            ->with("err_msg","failed!");
        }

    }

    protected function editPremiumBusiness(Request $request)

      {
      // if($request->btndel =="save")
      // {    

      if ($request->key=="") {
               return redirect("/admin/business/view-premium-business")->with("err_msg", "missing data to perform action!."); 
          }

      //  $businessid  = $request->bin; 

      //  $business_info =  Business::find($businessid);
      //  if (!$business_info) 
      //  {
      //    return redirect("/admin/business/view-premium-business")->with("err_msg", "No business record found!." ); 
      //  }

       // $prem_business = PremiumBusiness::where('id', $business_info->id)
       // ->first(); 

       //  if(!isset($prem_business))
       //  {
       //       return redirect("/admin/business/view-premium-business")->with("err_msg", "No premium business found!.");
       //  }
            //dd($request->key);
            $prem_busi_info = PremiumBusiness::find($request->key);         
            //$prem_busi_info->bin = $request->key; 
            $prem_busi_info->ended_on = date('Y-m-d',strtotime($request->ended_on)); 
            $save = $prem_busi_info->save();

            if ($save)
            {
               return redirect("/admin/business/view-premium-business")->with("err_msg", "edit premium business  successfully." );
            }
            else   
            {
              return redirect("/admin/business/view-premium-business")->with("err_msg", "edit premium business failed!." );
            }
         }

    }

