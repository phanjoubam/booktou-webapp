<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;
use Maatwebsite\Excel\Concerns\Exportable;
 



use App\AppLandingScreenModel;
use App\LandingScreenSectionModel;
use App\LandingScreenSectionContenModel;
use App\Business;
use App\MerchantOrderSequencer;
use App\ServiceBooking;
use App\ServiceBookingDetails;
use App\OrderSequencerModel;
use App\CustomerProfileModel;
use App\User;



use App\Traits\Utilities; 
use PDF;

class ServiceBookingAppointmentController extends Controller
{
    use Utilities;




    //reset cart
    public function resetCart(Request $request)
    {

        if( $request->bin != ""  )
        {
            $bin =  $request->bin ; 
            DB::table("ba_temp_booking_cart")
            ->where("bin",  $bin  )
            ->delete();

            return redirect("/admin/services/business/package/search?bin=" . $request->bin )->with("err_msg", "Booking cart cleared!");
        }
        else
        {
            return redirect("/admin/services/business/search")->with("err_msg",  "Missing data to perform action!");
        }
    }



    protected function searchBusiness(Request $request)
    {
        $business= array();

        $key = $request->search_key;
        $blocked = $request->blocked;

      if( isset($request->btnSearch) )
      {
        if($key=="")
        {
          $business= DB::table("ba_business") 
          ->where("is_block", $blocked )
          ->whereIn("main_module", array('appointment', 'booking') )
          ->paginate(20); 
        }
        else 
        {
          $business= DB::table("ba_business")  
          ->where("name","like", "%" . $key . "%")  
          ->where("is_block", $blocked )
          ->whereIn("main_module", array('appointment', 'booking') )
          ->paginate(20);  
        }
          
 
       }
       else 
       {

         $business= DB::table("ba_business") 
         ->where("is_block","no" )
         ->whereIn("main_module", array('appointment', 'booking') )
        ->paginate(20); 

       }

      $bins = array();
      foreach( $business as $item)
      {
        $bins[] =  $item->id;
      }

      $bins[]=0;

      $promo_businesses= DB::table("ba_premium_business")
      ->whereIn("bin",$bins) 
      ->get();

      $qr_menu= DB::table("ba_business_menu")
      ->whereIn("bin", $bins) 
      ->select("bin", DB::Raw("count(*) as pageCount"))
      ->groupBy("bin")
      ->get();

 

       $data = array('results' => $business, 'promo_businesses'=>$promo_businesses, 'qr_menu' => $qr_menu);
       return view("admin.business.search")->with('data',$data);
   }


   protected function searchPackage(Request $request)
  {
    $bin = $request->bin;
    if($bin == "")
    {
      return redirect('admin/services/business/search');
    }

    $member_id = $request->session()->has('__member_id_' ) ?  $request->session()->get('__member_id_' ) : 0;
    $key = $request->key;

    $package_categories = DB::table("ba_service_category")
    ->whereIn("category", function($query) use($bin){
        $query->select("service_category")
        ->from("ba_service_products")->where("bin", $bin);
    })
    ->get();

    if($key == "")
    {
        $packages = DB::table("ba_service_products")
        ->where("ba_service_products.bin",$bin)
        ->paginate(20); 
    }
    else
    {
        $packages = DB::table("ba_service_products")
        ->where("ba_service_products.bin",$bin)
        ->where("service_category",  $key )
        ->paginate(20); 
    }

    $business = Business::find( $bin);

    $business_hours = DB::table("ba_shop_hours")
    ->where("bin", $bin )
    ->select( "day_name as dayName", DB::Raw("  CONCAT( time_format( day_open,  '%r' )  , ' to ',  time_format( day_close,  '%r' ) ) as  shifts") )
    ->get();

    $package_specifications = DB::table('ba_business_service_profile') 
    ->where('bin', $bin)
    ->get();



    $data = array( 'packages' => $packages, 
        'package_specifications' =>$package_specifications,
        'business' => $business,
        'business_hours' =>$business_hours,
        'categories' => $package_categories,
        'bin'=>$bin );

    
    return view("admin.bookings.packagse_list")->with('data',$data);

  }


  protected function bookingDateAndTimeSelection(Request $request)
  {
    $bin = $request->bin;
    if($bin == "")
    {
      return redirect('admin/services/business/search');
    }

    $key = $request->key;

    $package_categories = DB::table("ba_service_category")
    ->whereIn("category", function($query) use($bin){
        $query->select("service_category")
        ->from("ba_service_products")->where("bin", $bin);
    })
    ->get();


    $selected_packages = DB::table("ba_temp_booking_cart") 
    ->where("bin", $bin)
    ->get();

    $package_ids = $selected_packages->pluck("package_id")->toArray();
 
    if(count($package_ids) == 0)
    {
        return redirect('admin/services/business/package/search?bin=' . $bin )->with("err_msg", "No package is selected!");
    }

    $business= Business::find( $bin);

    $business_hours = DB::table("ba_shop_hours")
    ->where("bin", $bin )
    ->select( "day_name as dayName", DB::Raw("  CONCAT( time_format( day_open,  '%r' )  , ' to ',  time_format( day_close,  '%r' ) ) as  shifts") )
    ->get();


    $service_days_slot = DB::table("ba_service_days") 
    ->where("bin", $bin )
    ->whereDate("service_date",  date('Y-m-d') )
    ->whereIn("package_id",  $package_ids )
    ->select("slot_number as slotNo", "start_time as startTime", "end_time as endTime", DB::Raw(" group_concat(status) as statuses"))
    ->groupBy("slotNo")
    ->groupBy("startTime")
    ->groupBy("endTime")
    ->get();


    if( isset($service_days_slot) && $service_days_slot->count() > 0 )
    {
        $package_statuses =  explode(",",  $service_days_slot[0]->statuses );
    }
    else
    {
        $package_statuses =   array("close");
    }
    
    if(count($package_statuses) == count($package_ids))
    {
        $slot_status = ( count(array_intersect( array( 'engage', 'ended', 'booked' ) , $package_statuses))) ? 'close' : 'open';
    }
    else
    {
        $slot_status =  'close' ;
    }
    

    $staffs = DB::table("ba_profile")
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereIn("ba_users.category", array("1", '10' ) )
    ->where("bin", $bin ) 
    ->select("ba_profile.id as staffId", "fullname as staffName", "profile_image_url as profilePicture" ) 
    ->get();


    $data = array( 'selected_packages' => $selected_packages, 
                  'business' => $business, 
                  'business_hours' =>$business_hours,
                  'timeslots' => $service_days_slot, 
                  'slot_status' => $slot_status,
                  'staffs' => $staffs,
                  'bin'=>$bin );
    return view("admin.bookings.booking_calendar")->with('data',$data);

  }


  protected function reviewOrderSummary(Request $request)
  {

    $bin = $request->bin;
    if($bin == "")
    {
      return redirect('admin/services/business/search');
    }
    $business= Business::find( $bin);
    if( !isset($business))
    {
        return redirect('admin/services/business/search')->with("err_msg", "No matching business found!");
    }
    if( strcasecmp($business->main_module, "booking" ) != 0)
    {
        return redirect('admin/services/business/search')->with("err_msg", "Business is not registered under booking module!");
    }


    $member_id =  $request->session()->get('__member_id_' );
    $cust_info =  CustomerProfileModel::find( $member_id) ;
    if(!isset($cust_info))
    {
      return redirect("/admin/services/business/package/search?bin=" . $bin )->with("err_msg",  "Missing data to perform action!");
    }
 
    $temp_cart_info = DB::table("ba_temp_booking_cart")
    ->where("bin", $bin )
    ->where("member_id", $member_id)
    ->first();

    if( !isset($temp_cart_info))
    {
        return redirect('admin/services/business/search')->with("err_msg", "Cart is empty!");
    }



    $packages = DB::table("ba_service_products")
    ->whereIn("id", function($query) use($bin, $member_id ) {
        $query->select("package_id")->from("ba_temp_booking_cart")->where("bin", $bin)->where("member_id", $member_id);

    })
    ->get();

    
    $package_specifications = DB::table('ba_business_service_profile')
    ->whereIn("service_product_id", function($query) use($bin) {
        $query->select("package_id")->from("ba_temp_booking_cart")->where("bin", $bin);
    })
    ->where('bin', $bin)
    ->get();
 

    $temp_cart_infos = DB::table("ba_temp_booking_cart") 
    ->where("bin", $bin )
    ->where("member_id", $member_id )
    ->get();


    if(isset($temp_cart_infos) && $temp_cart_infos->count() > 0)
    {
        $temp_cart_info  = $temp_cart_infos[0];
    }
    else
    {
      return redirect("/admin/services/business/package/search?bin=" . $bin)
      ->with("err_msg", "Booking cart is empty. Please start by selectign a package!");
    }


    $slot_no = $temp_cart_info->starting_slot;
    $service_date = date('Y-m-d', strtotime($temp_cart_info->service_date));
    $staff_id = $temp_cart_info->staff_id;

    $available_slot = DB::table("ba_service_days") 
    ->where("bin", $bin )
    ->where("slot_number", $slot_no )
    ->whereDate("service_date", $service_date )
    ->select("slot_number as slotNo", "start_time as startTime", "end_time as endTime")
    ->first();

    //checking if slots are available
    if( !isset($available_slot))
    {
        return redirect('admin/services/business/package/booking-date-and-time-selection?bin=' . $bin )
        ->with("err_msg", "Please select an available timeslot!");
    }


    $staff_info = DB::table("ba_profile")
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereIn("ba_users.category", array("1", '10' ) )
    ->where("ba_profile.id", $staff_id )
    ->select("ba_profile.id as staffId", "fullname as staffName", "profile_image_url as profilePicture" ) 
    ->first();
 

     //checking if slots are available
    if( !isset($staff_info))
    {
        return redirect('admin/services/business/package/booking-date-and-time-selection?bin=' . $bin )
        ->with("err_msg", "Please select an available service staff!");
    }

    $data = array('bin'=>$bin, 'business' => $business, 'timeslot' => $available_slot,  'staff' => $staff_info, 
        'service_date' =>$service_date, 'packages' => $packages, 'package_specifications' => $package_specifications );

    return view("admin.bookings.booking_summary")->with('data',$data);
}

  

protected function viewOnCallOrders(Request $request)
{

    if( isset($request->filter_date))
    {
        $seek_date = date('Y-m-d', strtotime($request->filter_date)) ; 
    }
    else 
    {
        $seek_date = date('Y-m-d'  )  ; 
    }

    $filter_by = $request->filter_by;
    $filter_status = $request->status;
    if( $filter_status == "-1")
    {
        $status = array( 'new','confirmed', 'cancelled' ) ; 
    }
    elseif(!isset($filter_status))
    {
        $status = array('new','confirmed') ; 
    }
    else 
    {
        $status = array($filter_status) ; 
    }

    if(strcasecmp($filter_by,"service_date")==0)
    {
            $appointment_booking = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id") 
          ->whereIn("ba_order_sequencer.type", array("appointment", "booking") )
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate('service_date','=',$seek_date)
          ->where("origin", "customer-care")
          ->orderBy("ba_order_sequencer.id", "desc")
          ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
          ->get();
    }
    else if(strcasecmp($filter_by,"booking_date")==0)
    {
        $appointment_booking = DB::table("ba_service_booking")
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id") 
        ->whereIn("ba_order_sequencer.type", array("appointment", "booking") )
        ->whereIn("ba_service_booking.book_status",$status)
        ->whereDate("ba_service_booking.book_date",'=',$seek_date )
        ->where("origin", "customer-care")
        ->orderBy("ba_order_sequencer.id", "desc")
        ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost", "gst", "discount",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
        ->get();
    }
    else
    {
            $appointment_booking = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id") 
          ->whereIn("ba_order_sequencer.type", array("appointment", "booking") )
          ->whereIn("ba_service_booking.book_status",$status)
          ->whereDate("ba_service_booking.book_date",'>=',$seek_date )
          ->where("origin", "customer-care")
          ->orderBy("ba_order_sequencer.id", "desc")
          ->select("ba_service_booking.id","bin", "service_date", "book_by",
           "book_date", "service_date"  ,"book_status", "preferred_time" , 
          "customer_name", "customer_phone", "address", "landmark", "total_cost","gst", "discount",
          "payment_type as pay_mode", "payment_status", "ba_order_sequencer.type")
          ->get(); 


        }
          
          $data = array('appointment_booking'=>$appointment_booking);

          return view('admin.bookings.oncall_bookings')->with($data);
}




  //make sns booking
  public function placeAppointmentReservation(Request $request)
  {
    
    if($request->bin == "" || $request->serviceDate == "" || $request->staffId == "" || $request->packageNos == ""  )
    {
      return redirect("/appointment")->with("err_msg",  "Missing data to perform action!");
    }

    $staff_id =   $request->staffId; 
    $bin = $request->bin;
    $business = Business::find( $bin );
    
    if( !isset($business))
    {
      return redirect("/admin/services/business/search")->with("err_msg", "Business information not found!");
    }

    //collect customer information
    $cname =   $request->cname; 
    $cphone =   $request->cphone; 
    $caddress =   $request->caddress;
    $service_date = date('Y-m-d'  , strtotime($request->serviceDate ));
    $packageNos  = $request->packageNos;

    $cust_id = 0;
    if(strcasecmp($request->linkcustomer, "yes") == 0 )
    {
        $cust_info =  User::where("phone", $cphone)->where("category", 0)->first();
        if(isset($cust_info))
        {
            $cust_id= $cust_info->profile_id;
        }
    }


    if(sizeof($packageNos) == 0 )
    {
        return redirect("/admin/services/business/package/search?bin=" . $bin )
        ->with("err_msg", "No package was selected to book. Please select a package to start booking!");
    }
 

    $selected_packages  = DB::table("ba_service_products")
    ->where("bin", $bin)
    ->whereIn("id", $packageNos)
    ->get();
    if( !isset($selected_packages) )
    {
        return redirect("/admin/services/business/package/search?bin=" . $bin )
        ->with("err_msg", "Selected packages are no longer available!");
    }
    
    $total_duration_required = 0;
    $total_cost = $total_discount = $total_gst =  0.00;
    $package_with_duration = array();
    foreach($packageNos as $packageid)
    {
      $service_duration =0;
      foreach($selected_packages as $item)
      {
        if($packageid == $item->id )
        {
            $service_duration += ( $item->duration_hr * 60 ) + $item->duration_min;
            $total_duration_required +=  ( $item->duration_hr * 60 ) + $item->duration_min;
            $total_cost +=  $item->actual_price;
            $total_discount  += $item->discount;
            $actual_cost = ( $item->actual_price -  $item->discount );
            $total_gst +=   ( ( $item->cgst_pc +  $item->sgst_pc ) * $actual_cost * 0.01);
        }
      }

      $package_with_duration[] = array( 'packageId' => $packageid, 'duration' => $service_duration );
    }
 
    
    $otp = mt_rand(111111, 999999);//order OTP generation

    if( $request->homevisitservice != "")
    {

    }
    else
    { 
        //check if slot is available for the day
        $slotno= $request->serviceTimeSlot;
        $day_name = date('l', strtotime( $service_date ));


        //ascending order sort is important otherwise it will break code
        $available_slots = DB::table("ba_service_days")
        ->where("slot_number", ">=", $slotno )
        ->where("staff_id",  $staff_id )
        ->whereDate("service_date",  $service_date )
        ->where("bin", $bin )
        ->where("status", "open")
        ->orderBy("slot_number", "asc")
        ->get();

        $availableSlots = $book_slots = array();
        $total_slots_min = 0;
        $starting_slot_found = false;
        $ending_slot_found = false;
      


        $lastFoundSlot = $lastScannedSlot = 0;
        $scanCount =0;
        foreach($package_with_duration as $serviceProductDuration )
        {
            foreach($available_slots as $item )
            {
               if( $item->slot_number == $request->serviceTimeSlot  )
               {
                    $starting_slot_found = true;
                }
                if($lastScannedSlot == 0 )
                {
                    $lastScannedSlot = $item->slot_number;
                }
                  //skip upto last processed slot from second scan onwards
                if( $scanCount > 0 && $lastScannedSlot >= $item->slot_number )
                {
                    continue;
                }

              $lastScannedSlot = $item->slot_number; //logging last scaned slot
              $start_time =   strtotime( $item->start_time );
              $end_time =   strtotime( $item->end_time );
              $gap =  abs( ( $end_time - $start_time) / 60);
              $serviceProductDuration['duration'] -= $gap;
              $total_slots_min += $gap;
              $total_duration_required -= $gap; //reducing total duration
              
              $availableSlots[] = array(
                  'serviceProductId' => $serviceProductDuration['packageId'],
                  "slot" => $lastScannedSlot ,
                  "duration" => abs($gap),
                  "startTime" =>$start_time,
              );
              if( $serviceProductDuration['duration'] <= 0 )
              {
                break;
              }
          }
         $scanCount++;
     }



     $allowBooking = true;
      foreach($package_with_duration as $serviceDuration)
      {
        $totalDuration = $serviceDuration['duration'];
        foreach($availableSlots as $availableSlot)
        {
          if( $serviceDuration['packageId'] == $availableSlot['serviceProductId'])
          {
            $totalDuration -= $availableSlot['duration'];
          }
        }
        
        if($totalDuration > 0  )
        {
          $allowBooking = false;
          break;
        }
      } 
 

      // if no available slot is found or no starting slot is found or duration of the slot found is less than required
      //block booking
      //if( !$starting_slot_found ||  $total_duration_required > 0  )
      if(count($availableSlots) == 0 || !$starting_slot_found || !$allowBooking  )
      {

        return redirect("/admin/services/business/package/search?bin=" . $bin )
        ->with("err_msg",   'Insufficient slots. Please change date or reduce sevice selected.' );
      }

      foreach($availableSlots as $availableSlot)
      {
        $book_slots[] = $availableSlot['slot'];
      }


      //generate order sequencer
      $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
      $rp_1 = mt_rand(0, 9);
      $rp_2 = mt_rand(0, 9);
      $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;
      
      $sequencer = new OrderSequencerModel;
      $sequencer->type = "appointment";
      $sequencer->tracker_session =  $tracker;
      $save = $sequencer->save();
      //generate order sequencer ends
      
      if( !isset($save) )
      {
        return redirect("/admin/services/business/package/search?bin=" . $bin )
        ->with("err_msg", 'Failed to generate order number sequence. Please retry again!'  );
      }

      //generate merchant sequencer
      $merchant_order_no =1;
      $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
      if(isset($merchant_order_info))
      {
        $merchant_order_no =  $merchant_order_info->last_order + 1;
      }
      else
      {
        $merchant_order_info = new MerchantOrderSequencer;
        $merchant_order_info->bin  = $request->bin;
      }
      //generate merchant sequencer ends 

      //make new booking
      $newbooking = new ServiceBooking;
      $newbooking->id = $sequencer->id ;
      $newbooking->customer_name =  $cname;
      $newbooking->customer_phone = $cphone ;
      $newbooking->address = $caddress ; 
      $newbooking->biz_order_no = $merchant_order_no;
      $newbooking->bin = $bin;
      $newbooking->book_category= $business->category;
      $newbooking->book_by = $cust_id;
      $newbooking->staff_id = $staff_id;
      $newbooking->book_status = "new";
      $newbooking->total_cost =  $total_cost;
      $newbooking->seller_payable =  $total_cost;
      $newbooking->discount = $total_discount;
      $newbooking->gst =  $total_gst;
      $newbooking->service_fee  =  0.00;
      $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
      $newbooking->cashback =  0;
      $newbooking->payment_type = "ONLINE";
      $newbooking->source = "booktou";
      $newbooking->origin = "customer-care";
      $newbooking->latitude =  $newbooking->longitude =  0;
      $newbooking->book_date =  date('Y-m-d H:i:s');
      $newbooking->service_date =  $service_date;
      $newbooking->cust_remarks = ($request->bookingRemark != "" ? $request->bookingRemark : "booked from cc portal");
      $newbooking->transaction_no =  "na" ;
      $newbooking->reference_no =  "na"  ;
      $newbooking->otp  =  $otp ;
      $save=$newbooking->save();
      
      $item_notes = array();
      if($save)
      {
        //update merchant order sequencer log
        $merchant_order_info->last_order  = $merchant_order_no;
        $merchant_order_info->save();
        //ending merchant order sequencer
        $pos = 0;
        $size = sizeof($availableSlots);
        
        foreach($selected_packages as $item)
        {
          if( $pos >= $size )
          {
            $pos = $size-1;
          }
          
          $startSlot = 0;
          $startTime = time();
          
          foreach($availableSlots as $availableSlot)
          {
            if( $availableSlot['serviceProductId'] == $item->id  )
            {
              $startSlot = $availableSlot['slot'];
              $startTime = $availableSlot['startTime'];
              break;
            }
          }
          $newbookingDetails = new ServiceBookingDetails;
          $newbookingDetails->book_id = $sequencer->id;
          $newbookingDetails->service_product_id = $item->id    ;
          $newbookingDetails->service_name = $item->srv_name  ;
          $newbookingDetails->slot_no = $startSlot;
          $newbookingDetails->status = "new";
          $newbookingDetails->service_time = date('H:i:s', $startTime );
          $newbookingDetails->service_charge = $item->actual_price;
          $newbookingDetails->save();
          $pos++;
        }

        //updating occupied
        DB::table("ba_service_days")
        ->whereIn("slot_number" , $book_slots )
        ->whereDate("service_date", $service_date)
        ->where("bin", $bin )
        ->where("staff_id",  $staff_id )
        ->where("status",  "open" )
        ->update(array("status" => "booked") ) ;

        //clearing temporary cart
        DB::table("ba_temp_booking_cart")
        ->where("bin", $bin )
        ->delete();
        
      }
    }

    return redirect("/admin/services/view-oncall-orders")->with('Appointment placed successfully!');
}



  //make paa booking
  public function placeBookingReservation(Request $request)
  {

    if($request->bin == "" || $request->serviceDate == "" || $request->staffId == "" || $request->packageNos == ""  || $request->preftime == "" )
    {
      return redirect("/appointment")->with("err_msg",  "Missing data to perform action!");
    }

    $bin = $request->bin;
    $business = Business::find( $bin );
    if( !isset($business))
    {
      return redirect("/admin/services/business/search")->with("err_msg", "Business information not found!");
    }

    if( !$request->session()->has('__user_id_' ))
    {
      return redirect("/shopping/login")->with("err_msg", "Please login to place an order!");
    }
    $member_id = $request->session()->get('__member_id_' );
    $cust_info =  CustomerProfileModel::find( $member_id) ;

    //collect customer information
    $cname =   $request->cname; 
    $cphone =   $request->cphone; 
    $caddress =   $request->caddress;
    $cust_id = 0;
    if(strcasecmp($request->linkcustomer, "yes") == 0 )
    {
        $cust_info =  User::where("phone", $cphone)->where("category", 0)->first();
        if(isset($cust_info))
        {
            $cust_id= $cust_info->profile_id;
        }
    }

    $service_date = date('Y-m-d'  , strtotime($request->serviceDate ));

    $packages_selected = DB::table("ba_temp_booking_cart")
    ->where("bin", $bin )
    ->where("member_id", $member_id )
    ->whereDate("service_date", $service_date )
    ->select("package_id", "staff_id", "starting_slot")
    ->get();

    $packages_selected_ids = $packages_selected->pluck("package_id")->toArray();

    if(count($packages_selected_ids) == 0)
    {
        return redirect("/admin/services/business/package/search?bin=" . $bin )
        ->with("err_msg", "No package was selected to book. Please select a package to start booking!");
    }

    $staff_id = $packages_selected[0]->staff_id;
    $slotno = $packages_selected[0]->starting_slot;

    $service_days_slot = DB::table("ba_service_days") 
    ->where("bin", $bin )
    ->whereDate("service_date",  $service_date )
    ->whereIn("package_id",  $packages_selected_ids )
    ->select("slot_number as slotNo", "start_time as startTime", "end_time as endTime", DB::Raw(" group_concat(status) as statuses"))
    ->groupBy("slotNo")
    ->groupBy("startTime")
    ->groupBy("endTime")
    ->first();

    if( isset($service_days_slot)  )
    {
        $package_statuses =  explode(",",  $service_days_slot->statuses );
    }
    else
    {
        $package_statuses =   array("close");
    }

    $slot_available = false;
    if(count($package_statuses) == count($packages_selected_ids))
    {
        $slot_available = ( count(array_intersect( array( 'engage', 'ended', 'booked' ) , $package_statuses)) > 0 ) ?  false : true ;
    }
    
    if(!$slot_available) 
    {
        return redirect("/admin/services/business/package/search?bin=" . $bin )
        ->with("err_msg", "Selected service is already booked. Please choose another date or change service.");
    }


    //proceed for booking
    $selected_packages  = DB::table("ba_service_products")
    ->where("bin", $bin)
    ->whereIn("id", $packages_selected_ids )
    ->get();

    if( !isset($selected_packages) )
    {
        return redirect("/admin/services/business/package/search?bin=" . $bin )
        ->with("err_msg", "Selected packages are no longer available!");
    }

    $total_cost = $total_discount = $total_gst =  0.00;
    foreach($selected_packages as $item)
    {
        $total_cost +=  $item->pricing;
        $total_discount  += $item->discount;
        $actual_cost = $item->pricing   ;
        $total_gst +=  ( ( $item->cgst_pc +  $item->sgst_pc ) * $actual_cost * 0.01);
    }

    $otp = mt_rand(111111, 999999);
    if( $request->homevisitservice != "")
    {

    }
    else
    {
        //generate order sequencer
        $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
        $rp_1 = mt_rand(0, 9);
        $rp_2 = mt_rand(0, 9);
        $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;
        $sequencer = new OrderSequencerModel;
        $sequencer->type = "booking";
        $sequencer->tracker_session =  $tracker;
        $save = $sequencer->save();
        //generate order sequencer ends
        if( !isset($save) )
        {
            return redirect("/admin/services/business/package/search?bin=" . $bin )
            ->with("err_msg", 'Failed to generate order number sequence. Please retry again!'  );
        }

      //generate merchant sequencer
      $merchant_order_no =1;
      $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
      if(isset($merchant_order_info))
      {
        $merchant_order_no =  $merchant_order_info->last_order + 1;
      }
      else
      {
        $merchant_order_info = new MerchantOrderSequencer;
        $merchant_order_info->bin  = $request->bin;
      }
      //generate merchant sequencer ends 

      //make new booking
      $newbooking = new ServiceBooking;
      $newbooking->id = $sequencer->id ;
      $newbooking->customer_name =  $cname;
      $newbooking->customer_phone = $cphone ;
      $newbooking->address = $caddress ; 
      $newbooking->biz_order_no = $merchant_order_no;
      $newbooking->bin = $bin;
      $newbooking->book_category= $business->category;
      $newbooking->book_by = $cust_id;
      $newbooking->staff_id = $staff_id;
      $newbooking->book_status = "new";
      $newbooking->total_cost =  $total_cost;
      $newbooking->seller_payable =  $total_cost;
      $newbooking->discount = $total_discount;
      $newbooking->gst =  $total_gst;
      $newbooking->service_fee  =  0.00;
      $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
      $newbooking->cashback =  0;
      $newbooking->payment_type = "ONLINE";
      $newbooking->source = "booktou";
      $newbooking->origin = "customer-care";
      $newbooking->latitude =  $newbooking->longitude =  0;
      $newbooking->book_date =  date('Y-m-d H:i:s');
      $newbooking->preferred_time =  date('H:i:s', strtotime( $request->preftime ));
      $newbooking->service_date =  $service_date;
      $newbooking->cust_remarks = ( $request->bookingRemark != "" ? $request->bookingRemark : "booked from cc portal" );
      $newbooking->transaction_no =  "na" ;
      $newbooking->reference_no =  "na"  ;
      $newbooking->otp  =  $otp ;
      $save=$newbooking->save();
      
      $item_notes = array();
      if($save)
      {
        //update merchant order sequencer log
        $merchant_order_info->last_order  = $merchant_order_no;
        $merchant_order_info->save();
        //ending merchant order sequencer 

        $book_slots = array();
        foreach($selected_packages as $item)
        {
            $newbookingDetails = new ServiceBookingDetails;
            $newbookingDetails->book_id = $sequencer->id;
            $newbookingDetails->service_product_id = $item->id;
            $newbookingDetails->service_name = $item->srv_name;
            $newbookingDetails->slot_no = $slotno;
            $newbookingDetails->cgst_pc = $item->cgst_pc;
            $newbookingDetails->sgst_pc = $item->sgst_pc;
            $newbookingDetails->status = "new";
            $newbookingDetails->service_time = date('H:i:s', strtotime( $request->preftime));
            $newbookingDetails->service_charge = $item->actual_price;
            $newbookingDetails->save();
        }

        //updating occupied
        DB::table("ba_service_days")
        ->whereDate("service_date", $service_date)
        ->where("bin", $bin )
        ->whereIn("package_id",  $packages_selected_ids )
        ->update(array("status" => "booked") ) ;

        //clearing temporary cart
        DB::table("ba_temp_booking_cart")
        ->where("bin", $bin )
        ->where("member_id", $cust_id )
        ->delete();
      }
    }
    return redirect("/admin/services/view-oncall-orders")->with('Appointment placed successfully!');
}


}

