<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;
use Maatwebsite\Excel\Concerns\Exportable;
 



use App\AppLandingScreenModel;
use App\LandingScreenSectionModel;
use App\LandingScreenSectionContenModel;



use App\Traits\Utilities; 
use PDF;

class AppCmsController extends Controller
{
    use Utilities;


    protected function addAppScreen(Request $request)
    {
        $msg ="Missing data to create app screen.";

        $screen_name = $request->screename;
        $sections = $request->sections;

        if($screen_name == "" || $sections == "")
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , $msg);
        }

        $dynamic_screen  = AppLandingScreenModel::where("screen_name",  $screen_name)
        ->first();

        if(isset($dynamic_screen))
        {
            $msg ="Screen name is already used!";
        }
        else
        {
            $new_screen = new AppLandingScreenModel;
            $new_screen->screen_name  = $screen_name;
            $new_screen->total_sections  = $sections;
            $save = $new_screen->save();  

            if($save)
            {
                $msg ="Screen created!";
            }
            else
            {
                $msg ="Screen could not be created!";
            }

        }

        return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , $msg);

    }


    
     protected function manageAppScreen(Request $request)
    {
        $dynamic_screens = DB::table("app_landing_screens")->get();


        $data = array( 
        'title'=>'App Landing Screens' ,  
        'screens' => $dynamic_screens );

         return view("admin.cms.screens.view_all_screens")->with( $data);

    }



    protected function manageSections(Request $request)
    {

        $msg ="No app screen selected.";

        $sid = $request->sid;
        if($sid == ""  )
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , $msg);
        }

        $app_screen  = AppLandingScreenModel::find( $sid);

        if( !isset($app_screen))
        {
           return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" ,  "No app screen found.");
        }

        $screen_name = $app_screen->screen_name;


        $screen_sections = DB::table("app_landing_screen_section")
        ->where("screen_name", $screen_name)
        ->get();


        $data = array( 
        'title'=>'App Landing Screens' ,  
        'app_screen' => $app_screen,
        'sections' => $screen_sections );

         return view("admin.cms.screens.view_screen_sections")->with( $data);

    }


    protected function addSection(Request $request)
    {
        $sid = $request->sid;
        if($sid == ""  )
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , "No screen selected.");
        }

        $app_screen  = AppLandingScreenModel::find( $sid);
        if( !isset($app_screen))
        {
           return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" ,  "No app screen found.");
        }

        $screen_name = $app_screen->screen_name;
        $main_module = $request->main_module;
        $sub_module = $request->sub_module;
        $heading_text = $request->heading_text;
        $layout_type = $request->layout_type;
        $content_type  = $request->content_type;
        $row = $request->row;
        $col = $request->col;
        $horizontal_scroll = $request->horizontal_scroll;
        $show_in_app = $request->show_in_app;
        $msg ="Missing data to add app screen section.";

        if(  $main_module == "" || $sub_module == "" || $heading_text == "" || $content_type == "" ||
            $layout_type == "" || $row == ""  || $col == "" || $horizontal_scroll == "" || $show_in_app == ""  )
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , $msg);
        }

        $section = new LandingScreenSectionModel;
        $section->main_module = $main_module;
        $section->sub_module = $sub_module;
        $section->heading_text = $heading_text;
        $section->screen_name = $screen_name;
        $section->layout_type = $layout_type;
        $section->content_type = $content_type;
        $section->row = $row;
        $section->col = $col;
        $section->horizontal_scroll = $horizontal_scroll;
        $section->display_sequence = 0;
        $section->show_in_app = $request->show_in_app;
        $save = $section->save();
        if($save)
        {
            $msg ="Screen section saved!";
        }
        else
        {
            $msg ="Screen section could not be saved!";
        }

        return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', $msg );
   }


   protected function updateSectionLeadingIcon(Request $request)
   {

        $sid = $request->sid;
        if($sid == ""  )
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , "No screen selected.");
        }

        $app_screen  = AppLandingScreenModel::find( $sid);
        if( !isset($app_screen))
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" ,  "No app screen found.");
        }
        $sectionid = $request->sectionid;
        if($sectionid == ""  )
        {
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No section selected.");
        }

        $section_info = LandingScreenSectionModel::find($sectionid);
        if( !isset($section_info))
        {
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No matching section found.");
        }



        $cdn_url = config('app.app_cdn');
        $cdn_path =  config('app.app_cdn_path');
        $folder_name = 'icon_'. $request->main_module;
        $folder_path =  $cdn_path."/assets/upload/". $folder_name;

        if ($request->hasFile('photo'))
        {
            if( !File::isDirectory($folder_path))
            {
                File::makeDirectory( $folder_path, 0777, true, true);
            }  
            $file = $request->photo;
            $originalname = $file->getClientOriginalName(); 
            $extension = $file->extension( );
            $filename = "appicon_". time()  . ".{$extension}";
            $file->storeAs('upload',  $filename );
            $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
            $file_names[] =  $filename ;
            $image_url  = $cdn_url.'/assets/upload/' . $folder_name ."/".$filename ;
            $section_info->leading_icon = $image_url;
            $section_info->save();
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "Leading icon updated.");
        }
        else
        {
          return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No image selected to replace leading icon.");
        }

    }



    protected function manageContents(Request $request)
    {

        $msg ="No app screen selected.";
        $sid = $request->sid;
        if($sid == ""  )
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , $msg);
        }

        $app_screen  = AppLandingScreenModel::find( $sid);

        if( !isset($app_screen))
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" ,  "No app screen found.");
        }
        $screen_name = $app_screen->screen_name;

        $sectionid = $request->sectionid;
        if($sectionid == ""  )
        {
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No section selected.");
        }

        $section_info = LandingScreenSectionModel::find($sectionid);
        if( !isset($section_info))
        {
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No matching section found.");
        }

        $package_metas = DB::table("ba_package_meta_data")
        ->where("sub_module", $section_info->sub_module)
        ->get();

        $section_contents = DB::table("app_landing_screen_section_content")
        ->where("section_id",  $sectionid)
        ->get();


        //get businesses categories 
        $business_categories = DB::table("ba_business")
        ->select('category')
        ->distinct()
        ->get();


        $product_categories = DB::table("ba_products")
        ->select('category')
        ->distinct()
        ->get();
 



        $data =  array(
            'business_categories' =>$business_categories,
            'product_categories' => $product_categories,
            'app_screen' => $app_screen,
            'section' => $section_info,
            "section_contents"=> $section_contents,
            "package_metas"=>$package_metas );
        return view('admin.cms.screens.view_screen_section_contents')->with($data);
    }


    protected function addContent(Request $request)
    {

        $msg ="No app screen selected.";
        $sid = $request->sid;
        if($sid == ""  )
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , $msg);
        }

        $app_screen  = AppLandingScreenModel::find( $sid);

        if( !isset($app_screen))
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" ,  "No app screen found.");
        }
        $screen_name = $app_screen->screen_name;

        $sectionid = $request->sectionid;
        if($sectionid == ""  )
        {
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No section selected.");
        }

        $section_info = LandingScreenSectionModel::find($sectionid);
        if( !isset($section_info))
        {
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No matching section found.");
        }

        $section_content = new LandingScreenSectionContenModel;
        $section_content->section_id = $sectionid;
        $section_content->meta_key = $request->meta_key;
        $section_content->meta_value = $request->meta_value;
        $section_content->label_text = $request->label;
        $section_content->view_mode = $request->view_mode;
        $save = $section_content->save();
        if ($save)
        {
           $msg = "Section content created!";
        }
        else
        {
            $msg = "Section content could not be created!";
        }

        return redirect('/admin/app-screen/sections/manage-contents?sid=' . $sid . '&sectionid=' . $sectionid)->with('err_msg',  $msg);
    }


    protected function updateContentSliderImageIcon(Request $request)
    {

        $msg ="No app screen selected.";
        $sid = $request->sid;
        if($sid == ""  )
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" , $msg);
        }

        $app_screen  = AppLandingScreenModel::find( $sid);
        if( !isset($app_screen))
        {
            return redirect("/admin/app-screen/manage-app-screen")->with("err_msg" ,  "No app screen found.");
        }
        $screen_name = $app_screen->screen_name;

        $sectionid = $request->sectionid;
        if($sectionid == ""  )
        {
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No section selected.");
        }

        $section_info = LandingScreenSectionModel::find($sectionid);
        if( !isset($section_info))
        {
            return redirect('/admin/app-screen/manage-sections?sid=' . $sid)->with('err_msg', "No matching section found.");
        }
        
        $contentid = $request->contentid;
        if($contentid == ""  )
        {
            return redirect('/admin/app-screen/sections/manage-contents?sid=' . $sid . '&sectionid=' . $sectionid)
            ->with('err_msg', "No section selected.");
        }

        $content_info = LandingScreenSectionContenModel::find($contentid);
        if( !isset($content_info))
        {
            return redirect('/admin/app-screen/sections/manage-contents?sid=' . $sid . '&sectionid=' . $sectionid)
            ->with('err_msg', "No matching content found.");
        }


        $cdn_url = config('app.app_cdn');
        $cdn_path =  config('app.app_cdn_path');
        $folder_name = 'image_'. $request->main_module;
        $folder_path =  $cdn_path."/assets/upload/". $folder_name;

        if ($request->hasFile('photo'))
        {
            if( !File::isDirectory($folder_path))
            {
                File::makeDirectory( $folder_path, 0777, true, true);
            }  
            $file = $request->photo;
            $originalname = $file->getClientOriginalName(); 
            $extension = $file->extension( );
            $filename = "appicon_". time()  . ".{$extension}";
            $file->storeAs('upload',  $filename );
            $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
            $file_names[] =  $filename ;
            $image_url  = $cdn_url.'/assets/upload/' . $folder_name ."/".$filename;


            $content_info->image_url = $image_url;
            $content_info->save();
            return  redirect('/admin/app-screen/sections/manage-contents?sid=' . $sid . '&sectionid=' . $sectionid)
            ->with('err_msg', "Content image/icon updated.");
        }
        else
        {
            return  redirect('/admin/app-screen/sections/manage-contents?sid=' . $sid . '&sectionid=' . $sectionid)
            ->with('err_msg', "Content image/icon could not be updated.");
        }
   }


}

