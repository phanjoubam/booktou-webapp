<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;

use App\User; 
use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;
use App\BusinessPaymentModel;
use App\Franchise;
use App\BusinessCommissionModel;
use App\DailyDepositModel;
use App\FeedbackMerchant;
use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\MonthlyAgentsOrdersExport;
use App\Exports\DailyAgentsOrdersExport;
use App\Exports\DailySalesOrderExport;
use App\Traits\Utilities; 
use PDF;
use App\DepositTargetModel;
use App\ProductVariantModel;
use App\ImportProductFileModel;
use App\SectionTypeModel;
use App\SectionDetailsModel;

class POSAdminController extends Controller
{
    use Utilities;

    protected function monthlyMerchantWiseReport(Request $request)
    {

    	$month = $request->month == "" ?  date('m')  : $request->month;
    	$year  = date('Y');
    	$business = DB::table('ba_business')
    	->where('pos_merchant','yes')
    	->where('is_open','open')
    	->where('is_block','no')
    	->select(
    		"id as bin",
    		"name as bizname",
    		"frno",
    		DB::Raw("'0' totalSales"),
    		DB::Raw("'0' orderCount")
    	)
    	->get();

    	$bin = array();

    	foreach ($business as $items) {
    		$bin[] = $items->bin;
    	}

    	$pos_sales_statistics = DB::table('ba_pos_order')
    	->whereMonth("service_date",$month)
    	->whereIn("bin",$bin)
    	->whereIn("book_status",array('delivered','completed'))
    	->select(
		    	 		"bin",
		    	 		DB::Raw("sum(total_cost)  as totalSale") ,
		    	 		DB::Raw("count(biz_order_no)  as orderCount") 
		    	 	)
		->groupBy("bin")
    	->get();

    	foreach ($business as $items) {
    		foreach ($pos_sales_statistics as $pos) {
    			 if ($items->bin==$pos->bin) {
    			 	$items->totalSales  = $pos->totalSale;
    			 	$items->orderCount = $pos->orderCount;
    			 	break;
    			 }
    		}
    	}

    	$data = array("results"=>$business,'year'=>$year,'month'=>$month);

    	return view('admin.pos.pos_merchant_wise_report')->with($data);
    }

    protected function monthlySalesReport(Request $request)
    {

    	$business = DB::table("ba_business")
    	->where("pos_merchant","yes")
    	->where('is_open','open')
    	->where('is_block','no')
    	->get();

    	$data = array("results"=>$business);

    	if ($request->bin!="" || $request->year!="") {
    	 	$bin = $request->bin;
			$year = $request->year;
			$endmonth =12;

			$report_data = array( );

      		for($i=1; $i <= $endmonth  ; $i++){
		    	 	$pos = DB::table("ba_pos_order")
		    	 	->where("bin",$bin)
		    	 	->whereIn("book_status",  array('delivered', 'completed'))  
		    	 	->whereYear("service_date",$year) 
		    	 	->select(
		    	 		DB::Raw("month(service_date) as currentMonth"),
		    	 		DB::Raw("sum(total_cost)  as totalSale") ,
		    	 		DB::Raw("count(biz_order_no)  as orderCount") 
		    	 	)
		    	 	->groupBy("currentMonth")  
		    	 	->get();

		   		$report_data[$i] = array('month' => date("F", mktime(0, 0, 0, $i, 10)), 
		   	    'year' => $year , 'totalSales' => 0,  'orderCount' => 0);

		   	    foreach($pos as $item)
		        { 
		          if($item->currentMonth == $i)
		           {
		              $report_data[$i]["totalSales"] += $item->totalSale ;
		              $report_data[$i]["orderCount"] += $item->orderCount; 
		              break;
		           }
		        }
    	 	}
    	 	$data  = array("results"=>$business,"report_data"=>$report_data,"year"=>$year);
    	 	//return view("admin.pos.pos_sales_statistics")->with($data); 
    	 } 

    	
    	return view("admin.pos.pos_sales_statistics")->with($data); 

    }

    protected function topSellingProduct(Request $request)
    {
    	if ($request->bin=="" || $request->month=="") {
    		 return redirect('admin/pos/merchant-wise-report');
    	}
    	$bin = $request->bin;
		$month = $request->month;

    	$business = DB::table("ba_business")
    	->where("pos_merchant","yes")
    	->where('is_open','open')
    	->where('is_block','no')
    	->where('id',$bin)
    	->first();

    	$pos_order = DB::table("ba_pos_order")
    	->where("bin",$bin)
    	->whereIn("book_status",array('delivered','completed'))
    	->whereMonth("service_date",$month)
    	->select(
    		"id as orderNo",
    		"biz_order_no as bizOrderNo",
    		"bin"
    	)
    	->get();

    	$orderNo = $bizOrderNo = array();

    	foreach ($pos_order as $items) {
    		 $orderNo[]=$items->orderNo;
    		 $bizOrderNo[] = $items->bizOrderNo;
    	}

    	$top_10_products = DB::table('ba_pos_order_basket')
    	->whereIn('order_no',$orderNo)
    	->select( 
    		"pr_name as productName",
    		DB::Raw("sum(qty) as quantity") 
    	)
    	->groupBy("productName")
    	->orderBy("quantity","desc")
    	->take(10)
    	->get();
    	 
    	$data = array(
    		'results'=>$top_10_products,
    		'business'=>$business,
    		'bin'=>$bin,
    		'month'=>date("F", mktime(0, 0, 0, $month, 10))
    		);
    	return view("admin.pos.view_top_selling_products")->with($data);
    }

}
