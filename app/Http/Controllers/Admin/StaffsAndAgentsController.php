<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

use App\User; 
use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;
use App\BusinessPaymentModel;
use App\Franchise;
use App\BusinessCommissionModel;
use App\DailyDepositModel;
use App\FeedbackMerchant;
use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\MonthlyAgentsOrdersExport;
use App\Exports\DailyAgentsOrdersExport;
use App\Exports\DailySalesOrderExport;
use App\Exports\MonthlyCommissionExport;
use App\Traits\Utilities; 
use PDF;
use App\DepositTargetModel;
use App\ProductVariantModel;
use App\ImportProductFileModel;
use App\SectionTypeModel;
use App\SectionDetailsModel;
use App\AppSectionModel;
use App\AppSectionDetailsModel;
use App\PosSubscription;
use App\SelfPickUpModel;
use App\PosPayment;
use App\BrandModel;
use App\ServiceCategoryModel;
use App\AgentEarningModel;
use App\AgentSalaryAdvanceModel;
use App\PopularProductsModel;
use App\AddDiscountProductsModel;
use App\WaterBookingModel;


class StaffsAndAgentsController extends Controller
{

  use Utilities;

  public function viewActiveAgents(Request $request)
  {

      $today =  date('Y-m-d');

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 

          $query->select("agent_id")
          ->from("ba_agent_attendance") 
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();


      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("franchise", "0" )
      ->where("category", "100" )
      ->where("ba_users.status", "<>", "100" )  
      ->select("ba_profile.*", "ba_users.category" )
      ->get(); 

      $active_staffs = DB::table("ba_staff_attendance")    
      ->whereDate("log_date",  $date )   
      ->get();


     $data = array(  'date' => $date , 'staffs' => $all_staffs , 'active_staffs' => $active_staffs );  
     return view("admin.staffs.staffs_attendance")->with('data', $data); 
    }
  


    protected function activeAttendance(Request $request)
  {
    if($request->filter_date=="")
    {
        $today = date('Y-m-d');
    }else{
        $today = date('Y-m-d',strtotime($request->filter_date));
    }

    $status = ['ready-for-task','engaged','break','request-end-of-day','ready-for-multiple-tasks'];

    $all_agents =  DB::table("ba_profile")   
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("agent_id")
          ->from("ba_agent_attendance")
          ->whereDate("log_date",   date('Y-m-d') ); 
        })
    ->where("ba_users.category", 100)
    ->where("ba_users.status", "<>",  100) 
    ->select("ba_profile.*")
    ->get();
    //use foreach to get Ids
    $agent_ids = array();
    $agent_ids[] = 0; //dummy filler
    foreach($all_agents as $agent)
    {
      $agent_ids[] = $agent->id;
    }
    
    $agents_status = DB::table("ba_agent_locations")
    ->whereRaw("date(updated_at) = '$today' " ) 
    ->whereIn("agent_id", $agent_ids )
    ->whereIn("agent_status",$status)
    ->select("agent_id", "agent_status")
    ->orderBy("seq_no", "asc") 
    ->get();

      $data = array('today'=>$today, 'agents' => $all_agents , 'agents_status' => $agents_status );  
     return view("admin.staffs.active_agents")->with('data', $data); 
    }


}

