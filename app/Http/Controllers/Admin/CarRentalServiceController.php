<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

use App\User; 
use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;
use App\BusinessPaymentModel;
use App\Franchise;
use App\BusinessCommissionModel;
use App\DailyDepositModel;
use App\FeedbackMerchant;
use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\MonthlyAgentsOrdersExport;
use App\Exports\DailyAgentsOrdersExport;
use App\Exports\DailySalesOrderExport;
use App\Exports\MonthlyCommissionExport;
use App\Traits\Utilities; 
use PDF;
use App\DepositTargetModel;
use App\ProductVariantModel;
use App\ImportProductFileModel;
use App\SectionTypeModel;
use App\SectionDetailsModel;
use App\AppSectionModel;
use App\AppSectionDetailsModel;
use App\PosSubscription;
use App\SelfPickUpModel;
use App\PosPayment;
use App\BrandModel;
use App\ServiceCategoryModel;
use App\AgentEarningModel;
use App\AgentSalaryAdvanceModel;
use App\PopularProductsModel;
use App\AddDiscountProductsModel;
use App\ServiceProductModel;
use App\PackageMetaModel;
use App\ServicePackageSpecModel;
use App\SponsoredPackageModel;
use App\LandingScreenSectionModel;
use App\LandingScreenSectionContenModel;
use App\MetaKeyModel;
class CarRentalServiceController  extends Controller
{
    use Utilities;
    public function dashboardBooking(Request $request)
    {
      $status = array('new', 'confirmed','in_queue',  'order_packed', 'in_route', 'delivered', 'completed',  'package_picked_up', 'delivery_scheduled' ); 

       if($request->filter_date=="")
        {
            $today = date('Y-m-d');
        }else{
            $today = date('Y-m-d',strtotime($request->filter_date));
        }
      

      $picnic = DB::table('ba_service_booking')
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      ->whereRaw("date(ba_service_booking.book_date)>='$today'") 
      ->whereIn("ba_order_sequencer.type", array('booking'))   
      ->where("book_category", "PICNIC AND ACTIVITIES")
      ->where("ba_service_booking.book_status", "new")
      ->count();

     // dd($picnic);


      // $water = DB::table('ba_water_booking')
      // ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_water_booking.id")
      // ->whereRaw("date(ba_water_booking.book_date)>='$today'") 
      // ->whereIn("ba_order_sequencer.type", array('booking','appoinment'))      
      // ->get();

      // $crs = DB::table('ba_service_booking')
      // ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
      // ->whereRaw("date(ba_service_booking.book_date)>='$today'") 
      // ->whereIn("ba_order_sequencer.type", array('booking','appoinment'))   
      // ->where("book_category", "CAR RENTAL BOOKING")
      // ->get();

      // $beauty = DB::table('ba_service_products')
      //  ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_products.id")
      // ->whereRaw("date(ba_service_products.valid_start_date)>='$today'") 
      //  ->whereIn("ba_order_sequencer.type", array('offers','appoinment'))
      //  ->where("service_category", "HAIR")   
      // ->get();
 
        return view("admin.bookings.dashboard_admin")->with("data",
        array(       
        "picnic" => $picnic,
        // "crs" => $crs,
        // "water" => $water,
        // "beauty" => $beauty,
         "today"=>$today
       
      ));       
    } 


    protected function viewAllOrders(Request $request)
    {

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)) ; 
      }
      else 
      {
        $orderDate = date('Y-m-d' , strtotime("30 days ago"))  ; 
      }
        

      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );


        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new','confirmed', 'cancel_by_client', 'canceled', 'delivered', 
                'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
                'package_picked_up','delivery_scheduled' ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
            $status = array( 'new','confirmed', 'cancel_by_client', 'canceled', 'delivered', 
            'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
            'package_picked_up','delivery_scheduled' ) ; 
        }

      }
      else 
      {

        $request->session()->remove('_last_search_date' );

        $status = array( 'new','confirmed',  'order_packed','pickup_did_not_come','in_route',  'package_picked_up','delivery_scheduled' ) ; 
      }
  
      $day_bookings = DB::table("ba_service_booking")
      ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
       ->whereDate("ba_service_booking.book_date", ">=", $orderDate )
      ->where("ba_order_sequencer.type",  "normal")
       ->where("ba_service_booking.book_category","PICNIC AND ACTIVITIES")
       ->whereIn("ba_service_booking.book_status", $status)  
       ->whereIn("ba_service_booking.bin", function($query)   { 
           $query->select("id")
           ->from("ba_business")
           ->where("frno",   0 ); 
        }) 
      ->orderBy("ba_service_booking.service_date", "asc")
      ->select("ba_service_booking.*" )
      ->get();
      $order_ids = array();
      $all_bins = array();
      $book_by = array();
      $phone_nos = array();
      foreach($day_bookings as $bitem )
      {
        $all_bins[] = $bitem->bin; 
        $book_by[] = $bitem->book_by;
        $phone_nos[] =$bitem->customer_phone;
        $order_ids[] = $bitem->id; 
      }


      $flag_customers = DB::table("ba_ban_list")
      ->whereIn("phone", $phone_nos)    
      ->get();

      $all_bins = array_filter($all_bins);

      $all_businesses = DB::table("ba_business")
      ->whereIn("id", $all_bins)    
      ->get();

      $order_statuses = array(   'completed', 'order_packed' , 'in_route','delivered' , 'package_picked_up','delivery_scheduled' );

      $book_by = array_filter($book_by);

       $book_status = array( 'cancel_by_owner','pickup_did_not_come', 'cancel_by_client',  'canceled','order_packed' ) ; 
 
 
      $book_counts = DB::table("ba_service_booking")
       ->whereIn("book_by", $book_by)    
       ->whereNotIn("book_status", $book_status) 
      ->where("book_category","PICNIC AND ACTIVITIES") 
      ->select("book_by", DB::raw('count(*) as totalOrders') )
      ->groupBy("book_by")
      ->get(); 
 
 
   
        //finding free agents  
        $today = date('Y-m-d');  
        $all_agents = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->whereIn("ba_profile.id", function($query) use ( $orderDate ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   date('Y-m-d') ); 
        })

        ->where("ba_users.category", 100)
        ->where("ba_users.status", "<>" , "100") 
        ->where("ba_profile.fullname", "<>", "" )
        ->where("ba_users.franchise", 0) 
         ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 
        ->get();
        $delivery_orders = DB::table("ba_delivery_order")
        ->whereRaw(" date(accepted_date) = '$today'" )
        ->where("completed_on", null )
         ->where("order_type",  'client order'  )
        ->select("member_id", DB::Raw("count(*) as cnt") )
        ->groupBy('member_id')
        ->get();

        $free_agents = array();


        //Fetching assigned orders
        $delivery_orders_list = DB::table("ba_delivery_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
        ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
        ->whereRaw(" date(accepted_date) ='$today'" )
        ->get();    
        foreach ($all_agents as $item)
        {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           }



           //image compression
            if($item->profile_photo != null )
            {
              $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

              if(file_exists(  $source ) &&  !file_exists( $destination ))
              {
                  $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
    
      $global_settings = DB::table("ba_global_settings") 
      ->where("config_key", "end_hour" )
      ->first(); 
 

      //order notifications 
      $order_ids[]=0;
      $all_notifications = DB::table("ba_order_notifications") 
      ->whereIn("order_no", $order_ids ) 
      ->get(); 

// take away service list
         $selfpickup = DB::table("ba_business_selfpickup")
         ->where("status","active")
         ->get();

         //ends here
        $data = array( 
        'title'=>'Manage Orders' , 
        'results' => $day_bookings, 
        'flag_customers' => $flag_customers,
        'date' => $orderDate, 
        'all_agents' => $all_agents , 
        'delivery_orders_list' => $delivery_orders_list, 
        'all_businesses' =>$all_businesses, 
        'book_counts' => $book_counts,
        'last_keyword' => $last_keyword, 
        'last_keyword_date' =>  $last_keyword_date , 
        'refresh' => 'true','selfpickup' =>$selfpickup,
        'global_settings' => $global_settings, 'all_notifications' =>$all_notifications );

         return view("admin.bookings.all_booking_orders")->with('data',$data);
    }

    protected function viewCompletedOrdersBooking(Request $request)
    {
      $franchise =  0;
      $status = array(  'completed', 'delivered', 
        'cancel_by_client','cancel_by_owner', 
        'pickup_did_not_come', 'delivered', 'returned', 'canceled') ; 
      
      if( isset($request->btn_search) )
      {
        $orderDate = date('Y-m-d', strtotime($request->orderdate)); 
      }
      else 
      {
        $orderDate = date('Y-m-d'); 
      }

      
      $day_bookings= DB::table("ba_service_booking")
      ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
       ->whereRaw("date(ba_service_booking.service_date)  ='$orderDate'") 
       ->whereIn("ba_service_booking.book_status",$status)
       ->where("ba_service_booking.book_category", "PICNIC AND ACTIVITIES")
       ->where("route_to_frno", $franchise )
       ->orderBy("ba_service_booking.service_date", "asc")
      ->select("ba_service_booking.*", "ba_profile.fullname", "ba_profile.phone", 
       DB::Raw("'na'  agentName"),
       DB::Raw("'na'  agentPhone"),
       DB::Raw("'na'  profile_photo"), 
       DB::Raw("'na'  businessName"),
       DB::Raw("'na'  businessPhone"),
       DB::Raw("'na'  businessBanner")  
       )
      ->get();

     
        //finding free agents  
        $all_agents = DB::table("ba_profile")
        ->join("ba_delivery_order", "ba_delivery_order.member_id", "=", "ba_profile.id")
        ->select( "ba_profile.id", "ba_profile.fullname",   "ba_profile.phone", "ba_profile.profile_photo" , "ba_delivery_order.order_no" )  
        ->where("order_type", "client order")
        ->get();
 
         
   
        foreach ($all_agents as $item)
        {
            
           //image compression
            if($item->profile_photo != null )
            {
              $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

              if(file_exists(  $source ) &&  !file_exists( $destination ))
              {
                  $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
          

      $bins = array(0);
      foreach($day_bookings as $bitem)
      {
        $bins[] = $bitem->bin;

        foreach ($all_agents as $item)
        {    
            if($bitem->id == $item->order_no)
            { 
               $bitem->agentName = $item->fullname;
               $bitem->agentPhone = $item->phone;
               $bitem->profile_photo =    $item->profile_photo;

              break;
            }

          }
         }

         $businesses = DB::table("ba_business") 
        ->whereIn( "id", $bins)  
        ->get(); 

 

       $data = array( 'title'=>'Manage Orders' ,'results' => $day_bookings, 'date' => $orderDate, 'all_agents' => $all_agents, 'businesses' =>$businesses  );

         return view("admin.bookings.all_completed_orders")->with('data',$data);
    }
    protected function viewCarRentalServices(Request $request)
    {     
        $business= array();

        $key = $request->search_key;
        $blocked = $request->blocked;

      if( isset($request->btnSearch) )
      {
        if($key=="")
        {
          $business= DB::table("ba_business") 
          ->where("is_block", $blocked )
          ->where('sub_module','CRS')  
          ->paginate(20); 
        }
        else 
        {
          $business= DB::table("ba_business")  
          ->where("name","like", "%" . $key . "%")  
          ->where("is_block", $blocked )
           ->where('sub_module','CRS')   
          ->paginate(20);

        } 
       }
       else 
       {

         $business= DB::table("ba_business") 
         ->where("is_block","no" )
        ->where("sub_module","CRS")   
        ->paginate(20); 

       }

      $bins = array();
      foreach( $business as $item)
      {
        $bins[] =  $item->id;
      }

      $bins[]=0;

      $promo_businesses= DB::table("ba_premium_business")
      ->whereIn("bin",$bins) 
      ->get();
       $data = array('results' => $business, 'promo_businesses'=>$promo_businesses);
       return view("admin.car_services.view_car_rental_services")->with('data',$data); 
  }
 
  protected function allCarRentalServices(Request $request)
  {
   $business= DB::table("ba_business") 
   ->where("is_block","no" )
   ->where("sub_module","CRS")   
   ->paginate(20);
   
   if(count($business)<= 0 )
   {
    $msg = "no business found ";
    return $msg;
   }
  $bin = array();
  foreach( $business as $item)
  {
    $bin[] =  $item->id;
  }
  $bin[]=0;
  $packages= DB::table("ba_service_products")
  ->join("ba_business","ba_service_products.bin","=","ba_business.id")
  ->whereIn("ba_service_products.bin",$bin) 
  ->select("ba_service_products.*","ba_business.name","ba_business.phone_pri")
  ->paginate(10);
  if($request->search_key !="")
  {
    $key = $request->search_key;
    $search = DB::table("ba_service_package_spec")
    ->where("meta_value","like", "%$key%")
    ->get();
    $service_id = array();
    foreach($search as $ser)
    {
      $service_id [] = $ser->srv_prid;
    }

    $pack= DB::table("ba_service_products")
    ->join("ba_business","ba_service_products.bin","=","ba_business.id")
    ->whereIn("ba_service_products.bin",$bin)
    ->whereIn("ba_service_products.id",$service_id)
    ->select("ba_service_products.*","ba_business.name","ba_business.phone_pri");
    $packages= DB::table("ba_service_products")
    ->join("ba_business","ba_service_products.bin","=","ba_business.id")
    ->union ($pack)
    ->whereIn("ba_service_products.bin",$bin)
    ->where("ba_service_products.srv_name", "like", "%$key%")
    ->orWhere("ba_service_products.srv_details", "like", "%$key%")
    ->select("ba_service_products.*","ba_business.name","ba_business.phone_pri")
    ->paginate(10);

  if(count($packages) ==0)
    {
      return redirect("/admin/customer-care/all-car-rental-service")->with("err_msg", "No car rental services found.");
    }
  }



  $sponsored_packages= DB::table("ba_sponsored_package")
  ->whereIn("bin",$bin) 
  ->get();

  $data = array(  'business'=>$business,
    'packages' => $packages, 
    "sponsored_packages"=>$sponsored_packages);


  return view("admin.car_services.crs_all_services")->with('data',$data);

}

  protected function viewCarServices(Request $request)
  
  {
    
    $bin = $request->bin;

    if($bin == "")
    {
      return redirect('/admin/customer-care/car-rental-services-view-all');
    }

    $services= DB::table("ba_service_products")
    ->where("ba_service_products.bin",$bin)
    ->paginate(10);

    $business= Business::find( $bin) ;
   $srv_category=DB::table("ba_service_category")
   ->where("bookable_category","CRS")
   ->get();
    $data = array(
                  'services' => $services, 
                  'bin'=>$bin,
                  'business'=>$business,"srv_category"=>$srv_category
                  
                );   
    return view("admin.car_services.car_service_details")->with('data',$data);

  }

 protected function addCarRentalServices(Request $request)

  {

if( isset($request->btnsaveconfirmation) && $request->btnsaveconfirmation =="save")
      { 

    $bin = $request->bin; 

    $business  = Business::find($bin); 
    if( !isset($business ))
    {
      return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "No car rental services found.");
    }

    $cdn_url = config('app.app_cdn'); 
    $cdn_path =  config('app.app_cdn_path'); 
    $folder_name = 'services_'. $bin ; 
    $folder_path =  $cdn_path . "/assets/upload/" . $folder_name ;
 
    $service_product = new ServiceProductModel; 
 

    if ($request->hasFile('photo')) {

      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0777, true, true);
      }  
      
      $file = $request->photo;
      $originalname = $file->getClientOriginalName(); 
      $extension = $file->extension();
      $filename = "service_" . time()  . ".{$extension}";
      $file->storeAs('upload',  $filename );
      $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
      $file_names[] =  $filename ;
      $file_url = $cdn_url . '/assets/upload/' . $folder_name  . "/" . $filename ; 
      $service_product->photos= $file_url;
    }
        $service_product->service_category = $request->service_category;
        $service_product->bin  = $business->id;
        $service_product->srv_name = $request->srv_name;
        $service_product->srv_details = $request->srv_details;
        $service_product->pricing = $request->pricing;
        $service_product->actual_price = $request->actual_price;
        $service_product->max_price = $request->max_price;
        // $service_product->discount = $request->discount;
        // $service_product->gst_code = $request->gst_code;
        $service_product->duration_hr = $request->duration_hr;
        $service_product->duration_min = $request->duration_min; 
        $service_product->srv_status = $request->srv_status;
        $service_product->service_type = $request->service_type;
        $service_product->gender = $request->gender;
        $service_product->valid_start_date = date("Y-m-d H:i:s",strtotime($request->valid_start_date));
        $service_product->valid_end_date = date("Y-m-d H:i:s",strtotime($request->valid_end_date));
        $service_product->save();   

      return redirect("/admin/customer-care/view-car-service/" . $bin )->with("err_msg", "record add successfully!");
    }   
    else 
    {
     return redirect("/admin/customer-care/car-rental-services-view-all") ;
   }

 }
 protected function deleteCarRentalServices(Request $request)
    {

         if($request->bin == ""   )
            {
              return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "No car rental services selected.");
            }

             $bin = $request->bin; 
             $business  = Business::find($bin); 
             if( !isset($business ))
             {
                return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "No car rental services found.");
             }

              if ($request->key=="") 
             {
              return redirect("/admin/customer-care/view-car-service/" . $bin )->with("err_msg", "field missing!");
             }

        $key = $request->key;

        $delete = DB::table("ba_service_products")
        ->where("id",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/customer-care/view-car-service/" . $bin )
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/admin/customer-care/view-car-service/" . $bin )
            ->with("err_msg","failed!");
        }

    } 
   public function editCarRentalService(Request $request)
  {


    if($request->bin == ""   )
    {
      return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "No car rental services selected.");
    }

    $bin = $request->bin; 
    $business  = Business::find($bin); 
    if( !isset($business ))
    {
      return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "No car rental services found.");
    }

    if ($request->key=="") 
    {
      return redirect("/admin/customer-care/view-car-service/" . $bin )->with("err_msg", "field missing!");
    }

    $cdn_url = config('app.app_cdn'); 
    $cdn_path =  config('app.app_cdn_path'); 

    $id = $request->key; 

    $folder_name = 'services_'. $bin ; 
    $folder_path =  $cdn_path . "/assets/upload/" . $folder_name ;
 

    $service_product = ServiceProductModel::find($id);
    $bin = $service_product->bin; 

    if ($request->hasFile('photo')) {

      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0777, true, true);
      }  
      
      $file = $request->photo;
      $originalname = $file->getClientOriginalName(); 
      $extension = $file->extension( );
      $filename = "service_" . time()  . ".{$extension}";
      $file->storeAs('upload',  $filename );
      $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
      $file_names[] =  $filename ;
      $file_url = $cdn_url . '/assets/upload/' . $folder_name  . "/" . $filename ;

      $service_product->photos= $file_url;
    }
      $service_product->srv_name = $request->srv_name;
      $service_product->pricing = $request->pricing;
      $service_product->service_type = $request->service_type;     
      $save = $service_product->save();   

    if ($save) {
      return redirect("/admin/customer-care/view-car-service/" . $bin )->with("err_msg", "record udpate successfully!");
    }  
    else 
    {
     return redirect("/admin/customer-care/car-rental-services-view-all") ;
   } 
 }
public function savePromotion(Request $request) 
     {
      if( isset($request->btnSave) && $request->btnSave =="save")
      {  
      
       $businessId  = $request->bin;
      // return json_encode($businessId);

       $business_info =  Business::find($businessId);

       if (!$business_info) 
       {

         return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "No business record found!." ); 
       }

       $prem_busi_info = PremiumBusiness::where('bin', $business_info->id)
       ->first();

      
        if( !isset($prem_busi_info))
        {
          $prem_busi_info = new PremiumBusiness; 
        } 
        
        $prem_busi_info->bin  = $business_info->id; 
        $prem_busi_info->start_from  = date('Y-m-d H:i:s'); 
        $prem_busi_info->ended_on = date("Y-m-d H:i:s",strtotime("+30 day"));
        $prem_busi_info->save();
        return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "Business promo updated successfully." );

    }
    else
    {
      return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "Business promo update failed!." );

    }
   }
 public function editAllCarRentalService(Request $request)
  {


    if($request->bin == ""   )
    {
      return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "No business selected.");
    }

    $bin = $request->bin; 
    $business  = Business::find($bin); 
    if( !isset($business ))
    {
      return redirect("/admin/customer-care/car-rental-services-view-all")->with("err_msg", "No business found.");
    }

    if ($request->key=="") 
    {
      return redirect("/admin/customer-care/view-car-service/" . $bin )->with("err_msg", "field missing!");
    }

    $cdn_url = config('app.app_cdn'); 
    $cdn_path =  config('app.app_cdn_path'); 

    $id = $request->key; 
    $folder_name = 'services_'. $bin ; 
    $folder_path =  $cdn_path . "/assets/upload/" . $folder_name ;
 

    $service_product = ServiceProductModel::find($id);
    $bin = $service_product->bin; 

    if ($request->hasFile('photo')) {

      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0777, true, true);
      }  
      
      $file = $request->photo;
      $originalname = $file->getClientOriginalName(); 
      $extension = $file->extension( );
      $filename = "service_" . time()  . ".{$extension}";
      $file->storeAs('upload',  $filename );
      $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
      $file_names[] =  $filename ;
      $file_url = $cdn_url . '/assets/upload/' . $folder_name  . "/" . $filename ;

      $service_product->photos= $file_url;
    }
      $service_product->srv_name = $request->srv_name;
      $service_product->service_category = $request->service_category;
      $service_product->pricing = $request->pricing;
      $service_product->service_type = $request->service_type;
      $service_product->gender = $request->gender;
     
    $save = $service_product->save();   

    if ($save) {
      return redirect("/admin/customer-care/all-car-rental-service" )->with("err_msg", "record udpate successfully!");
    }  
    else 
    {
     return redirect("/admin/customer-care/car-rental-services-view-all") ;
   } 
 } 
 protected function deleteAllCarRentalServices(Request $request)
    {

        if ($request->key=="") {
            return redirect("/admin/customer-care/all-car-rental-service")
            ->with("err_msg","field missing!");
        }

        $key = $request->key;

        $delete = DB::table("ba_service_products")
        ->where("id",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/customer-care/all-car-rental-service")
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/admin/customer-care/all-car-rental-service")
            ->with("err_msg","failed!");
        }

    } 

  protected function viewMoreDetailsCRS(Request $request)
  
  {
    
    $id = $request->srv_prid;

    if($id == "")
    {
      return redirect('/admin/customer-care/car-rental-services-view-all');
    }

    $services= DB::table("ba_service_products")
    ->where("ba_service_products.id",$id)
    ->first();
    $package_meta_key=DB::table("ba_package_meta_data")
    ->get();
    $srv_package_spec=DB::table("ba_service_package_spec")
    ->where("srv_prid",$id)
    ->get();
    $data = array('crs_services' =>$services,"pk_meta_key"=>$package_meta_key,"srv_package_spec"=>$srv_package_spec);   
    return view("admin.car_services.view_more_details")->with('data',$data);

  }

  protected function addServicePackageSpec(Request $request)
  {   
    if( isset($request->btnsaveconfirmation) && $request->btnsaveconfirmation =="save")
    {

      $id = $request->srv_prid;

      if($id == "")
      {
        return redirect('/admin/customer-care/car-rental-services-view-all');
      }

      $service_product = ServiceProductModel::find( $id ) ;

      if(  !isset($service_product ) )
      {
        return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "No service product found.");
      }

      $packages_spec = ServicePackageSpecModel::where("srv_prid", $request->id )->first();
      if(  !isset($production ) )
      {
        $packages_spec = new ServicePackageSpecModel;
      }
    
      $packages_spec->srv_prid = $request->srv_prid;
      $packages_spec->meta_key = $request->metakey;
      $packages_spec->meta_value = $request->metavalue;
      $packages_spec->meta_key_text = $request->meta_key_text;

      $packages_spec->save(); 

      return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", " Added to packages service specification.");

    }
    else 
    {
     return redirect("/admin/customer-care/car-rental-services-view-all") ;
   } 
 }

 protected function editServicePackageSpec(Request $request)
 {

  $id = $request->srv_prid;

  if($id == "")
  {
    return redirect('/admin/customer-care/car-rental-services-view-all');
  }

  $service_product = ServiceProductModel::find( $id ) ;

  if(  !isset($service_product ) )
  {
    return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "No service product found.");
  }

  if ($request->key=="") {
   return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "missing data to perform action!."); 
 }

 $key = $request->key;

 $packages_spec = ServicePackageSpecModel::find($key);
 $packages_spec->meta_key = $request->meta_key ;
 $packages_spec->meta_value = $request->meta_value; 
 $packages_spec->meta_key_text = $request->meta_keytext;           
 $save = $packages_spec->save();   

 if ($save) {
  return redirect("/admin/customer-care/business/view-more-details-crs/" . $id)->with("err_msg", "record udpate successfully!");
}  
else 
{
 return redirect("/admin/customer-care/car-rental-services-view-all") ;
} 
}

 protected function deleteServicePackageSpec(Request $request)
 {

  $id = $request->srv_prid;

  if($id == "")
  {
    return redirect('/admin/customer-care/car-rental-services-view-all');
  }

  $service_product = ServiceProductModel::find( $id ) ;

  if(  !isset($service_product ) )
  {
    return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "No service product found.");
  }

  if ($request->key=="") {
   return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "missing data to perform action!."); 
 }

 $key = $request->key;

  $delete = DB::table("ba_service_package_spec")
  ->where("id",$key) 
  ->delete(); 

 if ($delete) {
  return redirect("/admin/customer-care/business/view-more-details-crs/" . $id)->with("err_msg", "record delete successfully!");
}  
else 
{
 return redirect("/admin/customer-care/business/view-more-details-crs/" . $id)->with("err_msg","failed!") ;
} 
}

public function addSponsoredPackage(Request $request) 


{

  if( isset($request->btnSave) && $request->btnSave =="save")
  {
    if($request->bin == ""   )
    {
      return redirect("/admin/customer-care/all-car-rental-service")->with("err_msg", "No business selected.");
    }

    $bin = $request->bin; 
    $business  = Business::find($bin); 
    if( !isset($business ))
    {
      return redirect("/admin/customer-care/all-car-rental-service")->with("err_msg", "No business found.");
    }


    if(  $request->srv_prid == "" )
    {
      return redirect("/admin/customer-care/all-car-rental-service" )->with("err_msg", "No package selected.");
    }

    $service_product = ServiceProductModel::find( $request->srv_prid ) ;

    if(  !isset($service_product ) )
    {
      return redirect("/admin/customer-care/all-car-rental-service" )->with("err_msg", "No package found.");
    }


    $srvpackage = SponsoredPackageModel::where("package_id", $request->srv_prid )->first();
    if(  !isset($promotion ) )
    {
      $srvpackage = new SponsoredPackageModel();
    }

    $srvpackage->bin  = $request->bin;
    $srvpackage->package_id = $request->srv_prid;
    $srvpackage->is_sponsored = $request->is_sponsored;  
    $srvpackage->save(); 

    return redirect("/admin/customer-care/all-car-rental-service" )->with("err_msg", "Package added to sponsored list.");

  }
  else 
  {
   return redirect("/admin/customer-care/all-car-rental-service" ) ;
 } 
}
protected function addMetaKey(Request $request)
{
  $metakey = new MetaKeyModel;
  $metakey->meta_key = $request->metakey;
  $metakey->meta_key_text = $request->metakeytext;        
  $save = $metakey->save();
  if($save)
  {
    return redirect('/admin/systems/manage-business-categories')->with('err_msg',' save successfully!');
  }
  return redirect('/admin/systems/manage-business-categories'); 
}
protected function deleteMetaKey(Request $request)
    {

        if ($request->metadel=="") {
            return redirect("/admin/systems/manage-business-categories")
            ->with("err_msg","field missing!");
        }

        $key = $request->metadel;

        $delete = DB::table("ba_package_meta_data")
        ->where("id",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/systems/manage-business-categories")
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/admin/systems/manage-business-categories")
            ->with("err_msg","failed!");
        }

    }
    protected function editMetaKey(Request $request) 
    {
        
        if ($request->keys=="") 
        {
            return redirect("/admin/systems/manage-business-categories")->with("err_msg","field missing!");
        }       
        $key = $request->keys;  
        $metakey = MetaKeyModel::find($key);      
        $metakey->meta_key = $request->meta_key;
        $metakey->meta_key_text = $request->meta_keytext;
        $save = $metakey->save();
        if ($save) {
            return redirect("/admin/systems/manage-business-categories")
            ->with("err_msg","record udpate successfully!");
        }else{
            return redirect("/admin/systems/manage-business-categories")
            ->with("err_msg","failed!");
        }
    }
   
 protected function appLandingScreenSections(Request $request)
 {
  $bookables=DB::table("ba_bookable_category")
  ->where("sub_module","CRS")
  ->get();
  $sections =DB::table("app_landing_screen_section")
  ->where('main_module','booking')
  ->where('sub_module','CRS')
  ->get();


  return view('admin.car_services.crs_add_landing_section')->with(  array( 'sections' => $sections,"bookables"=>$bookables));
  
 }

 

 protected function addLandingScreenSections(Request $request)
 {

        $cdn_url = config('app.app_cdn'); 
        $cdn_path =  config('app.app_cdn_path'); 
        $folder_name = 'icon_'. $request->main_module;  
        $folder_path =  $cdn_path."/assets/upload/". $folder_name; 
        $section = new LandingScreenSectionModel;
        if ($request->hasFile('photo')) {
          if( !File::isDirectory($folder_path))
          {
            File::makeDirectory( $folder_path, 0777, true, true);
          }  
          $file = $request->photo;
          $originalname = $file->getClientOriginalName(); 
          $extension = $file->extension( );
          $filename = "icon_". time()  . ".{$extension}";
          $file->storeAs('upload',  $filename );
          $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
          $file_names[] =  $filename ;
          $folder_url  = $cdn_url.'/assets/upload/' . $folder_name ."/".$filename ;
          $section->leading_icon = $folder_url;
        }

        $section->main_module = $request->main_module;
        $section->sub_module = $request->sub_module;
        $section->heading_text = $request->heading_text;
        $section->layout_type = $request->layout_type;
        $section->row = $request->row;
        $section->col = $request->col;
        $section->horizontal_scroll = $request->horizontal_scroll;
        $section->display_sequence = $request->display_sequence;
        $section->show_in_app = $request->show_in_app;

        $save = $section->save();
        if($save)
        {
        return redirect('/admin/app-landing-screen-sections')->with('err_msg',' save successfully!');
        }
      return redirect('/admin/app-landing-screen-sections'); 
   }

   
protected function editLandingScreenSectionsIcon(Request $request)
 {

   if ($request->icon=="") 
    {
        return redirect("/admin/app-landing-screen-sections")->with("err_msg","field missing!");
    }
    $cdn_url = config('app.app_cdn'); 
   $cdn_path =  config('app.app_cdn_path'); 

   $folder_name = 'icon_'. $request->main_module;  
   $folder_path =  $cdn_path."/assets/upload/". $folder_name; 

    $icon = $request->icon;  
    $section = LandingScreenSectionModel::find($icon);
    if ($request->hasFile('photo')) {
      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0777, true, true);
      }  
      $file = $request->photo;
      $originalname = $file->getClientOriginalName(); 
      $extension = $file->extension( );
      $filename = "icon_". time()  . ".{$extension}";
      $file->storeAs('upload',  $filename );
      $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
      $file_names[] =  $filename ;
      $folder_url  = $cdn_url.'/assets/upload/' . $folder_name ."/".$filename ;
      $section->leading_icon = $folder_url;
    }
    $save = $section->save();
    if ($save) {
        return redirect("/admin/app-landing-screen-sections")
        ->with("err_msg","record  successfully!");
    }else{
        return redirect("/admin/app-landing-screen-sections")
        ->with("err_msg","failed!");
    }
   }
    protected function editLandingScreenSections(Request $request)

    {
        
        if ($request->key=="") 
        {
            return redirect("/admin/app-landing-screen-sections")->with("err_msg","field missing!");
        }
        $key = $request->key;  
        $section = LandingScreenSectionModel::find($key);
        $section->heading_text = $request->heading_text;
        $section->layout_type = $request->layout_type;
        $section->row = $request->row;
        $section->col = $request->col;
        $section->horizontal_scroll = $request->horizontal_scroll;
        $section->display_sequence = $request->display_sequence;
        $section->show_in_app = $request->show_in_app;
        $save = $section->save();
        if ($save) {
            return redirect("/admin/app-landing-screen-sections")
            ->with("err_msg","record  successfully!");
        }else{
            return redirect("/admin/app-landing-screen-sections")
            ->with("err_msg","failed!");
        }
    }
    protected function deleteLandingScreenSections(Request $request)
    {

        if ($request->keys=="") {
            return redirect("/admin/app-landing-screen-sections")
            ->with("err_msg","field missing!");
        }

        $key = $request->keys;

        $delete = DB::table("app_landing_screen_section")
        ->where("id",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/app-landing-screen-sections")
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/admin/app-landing-screen-sections")
            ->with("err_msg","failed!");
        }

    }
protected function manageLandingScreenSections(Request $request)
{
  $sections = DB::table("app_landing_screen_section")
  ->orderBy("display_sequence")
  ->orderBy("layout_type")

  ->get(); 
   $sectionid = array();
      foreach( $sections as $item)
      {
        $sectionid[] =  $item->id;
      }
      $section_contents= DB::table("app_landing_screen_section_content")
      ->whereIn("section_id",$sectionid) 
      ->get();
  return view('admin.car_services.crs_landing_sections')->with(  array( 
    'sections' => $sections,
    "section_contents"=>$section_contents ));

}
//Use this method to update landin screen section order

protected function reorderLandingScreenSections(Request $request)
    {
      if(isset($request->btnupdate) && strcasecmp($request->btnupdate, "reorder") == 0 )
      {

        $keys = $request->keys;

        $newpos = 1;
        foreach($keys as $key)
        {
            DB::table('app_landing_screen_section')
            ->where('id',  $key )
            ->update( ['display_sequence'=>   $newpos  ] ); 

            $newpos++;
        } 
      }
      return redirect("/admin/manage-landing-screen-sections")->with("err_msg" ,"Display sequence reordered!");
    }
 protected function LandingScreenSectionsEditor(Request $request)
 {
  $id = $request->section_id;

   $updatediscount = $request->displaysequence;

      $section = LandingScreenSectionModel::find($id);
        if( !isset($section))
        {
          return redirect("/admin/manage-landing-screen-sections");  
        }
        else 
        { 
          $section->display_sequence = $updatediscount; 
          $section->save(); 
          return redirect("/admin/manage-landing-screen-sections")->with("err_msg", 'Section updated.'  ); 
        }    
 }

  protected function landingSectionContents(Request $request)
 {

  $package_metas = DB::table("ba_package_meta_data")
  // ->join("ba_service_package_spec","ba_service_package_spec.meta_key","=","ba_package_meta_data.meta_key")     
  // ->select("ba_package_meta_data.*","ba_package_meta_data.meta_key_text")
  ->get();

   $sections = DB::table("app_landing_screen_section")
   ->orderBy("layout_type")
   ->get();
  

  $section_contents = DB::table("app_landing_screen_section_content")
  ->orderBy("section_id" )
  ->get();
 $business=DB::table("ba_business")
   ->get();
 return view('admin.car_services.crs_landing_section_contents')->with( 
    array( 'sections' => $sections,
      "section_contents"=> $section_contents,
      "package_metas"=>$package_metas,"business"=>$business  ));
   
 }


//API method to fetched 
 protected function landingSectionContentsBusiness(Request $request)
 {
      if ($request->bus_view=="")
      {
        return response()->json(array('success'=>false));
      }

      $bus_view =$request->bus_view;       
        $businesses = DB::table('ba_business')
        ->get();

         $data = array('results'=> $businesses);

         return json_encode(  $data);


        //$returnHTML= view('admin.car_services.view_business')->with($data)->render();

        //return response()->json(array('success'=>true,'html'=>$returnHTML));

    //} 
  }
protected function addLandingScreenSectionContent(Request $request)

{

  $cdn_url = config('app.app_cdn'); 
  $cdn_path =  config('app.app_cdn_path'); 

  $folder_name = 'slider_'. $request->main_module;  
  $folder_path =  $cdn_path."/assets/upload/". $folder_name; 

  $section_content = new LandingScreenSectionContenModel;

        // if image is found
  if ($request->hasFile('photo')) {
    if( !File::isDirectory($folder_path))
    {
      File::makeDirectory( $folder_path, 0777, true, true);
    }  
    $file = $request->photo;
    $originalname = $file->getClientOriginalName(); 
    $extension = $file->extension( );
    $filename = "slider_". time()  . ".{$extension}";
    $file->storeAs('upload',  $filename );
    $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
    $file_names[] =  $filename ;
    $folder_url  = $cdn_url.'/assets/upload/' . $folder_name ."/".$filename ;
    $section_content->image_url = $folder_url;
  }
        // 

  $section_content->meta_key = $request->meta_key;
  $section_content->meta_value = $request->meta_value;
  $section_content->image_caption = $request->image_caption;
  $section_content->target_screen = $request->target_screen;
  $section_content->section_id = $request->section_id;
  $section_content->uid = 0; //defer adding UID in later screen

  $save = $section_content->save();
  
  if ($save) {
    return redirect('/admin/manage-landing-section-contents')->with('err_msg',' save successfully!');
  }  
  return redirect('/admin/manage-landing-section-contents');   
}

  protected function landingSectionPackageBusiness(Request $request)
  {
    $id = $request->id;
    $section_contents = DB::table("app_landing_screen_section_content")
    ->where("id",$id) 
    ->first();



    $business= DB::table("ba_business")
    ->where("main_module","BOOKING")
    ->where("sub_module","crs")
    ->pluck('id');

    $services= DB::table("ba_service_products")
    ->whereIn("bin",$business)
    ->paginate(10);

 
    $data = array('services' => $services,'business'=>$business,"section_contents"=>$section_contents,"id"=>$id);  
    
    return view("admin.car_services.view_business")->with('data',$data);

  }
 protected function landingSectionPackageBusinessSave(Request $request)

    {
       $key = $request->key;
       // dd($id);
       $section = LandingScreenSectionContenModel::find($key);
       if( !isset($section))
        {
          return redirect("/admin/manage-landing-section-contents");  
        }
    
 
        $section = LandingScreenSectionContenModel::find($key);
         
        $section->uid = $request->uid;
       
        $save = $section->save();
        if ($save) {
            return redirect("/admin/link-landing-section-with-package-business/".$key)
            ->with("err_msg","record udpate successfully!");
        }else{
            return redirect("/admin/link-landing-section-with-package-business/".$key)
            ->with("err_msg","failed!");
        }
    }
protected function deleteLandingScreensection(Request $request)
    {

        if ($request->key=="") {
            return redirect("/admin/manage-landing-section-contents")
            ->with("err_msg","field missing!");
        }

        $key = $request->key;

        $delete = DB::table("app_landing_screen_section_content")
        ->where("id",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/manage-landing-section-contents")
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/admin/manage-landing-section-contents")
            ->with("err_msg","failed!");
        }

    }
    protected function editLandingScreenSectionsImage(Request $request)
 {

   if ($request->image=="") 
    {
        return redirect("/admin/manage-landing-section-contents")->with("err_msg","field missing!");
    }
    $cdn_url = config('app.app_cdn'); 
   $cdn_path =  config('app.app_cdn_path'); 

   $folder_name = 'slider_'. $request->main_module;  
   $folder_path =  $cdn_path."/assets/upload/". $folder_name; 

    $image = $request->image;  
    $section_content = LandingScreenSectionContenModel::find($image);
     if ($request->hasFile('photo')) {
        if( !File::isDirectory($folder_path))
        {
          File::makeDirectory( $folder_path, 0777, true, true);
        }  
        $file = $request->photo;
        $originalname = $file->getClientOriginalName(); 
        $extension = $file->extension( );
        $filename = "slider_". time()  . ".{$extension}";
        $file->storeAs('upload',  $filename );
        $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
        $file_names[] =  $filename ;
        $folder_url  = $cdn_url.'/assets/upload/' . $folder_name ."/".$filename ;
        $section_content->image_url = $folder_url;
      }
    $save = $section_content->save();
    if ($save) {
        return redirect("/admin/manage-landing-section-contents")
            ->with("err_msg","record udpate successfully!");
    }else{
        return redirect("/admin/manage-landing-section-contents")
            ->with("err_msg","failed!");
    }
   }
    protected function editLandingScreensection(Request $request)

    {
        
        if ($request->key=="") 
        {
            return redirect("/admin/manage-landing-section-contents")->with("err_msg","field missing!");
        }
       
        $key = $request->key;
  
        $section_content = LandingScreenSectionContenModel::find($key);
        $section_content->meta_key = $request->metakey;
        $section_content->meta_value = $request->metavalue;
        $section_content->image_caption = $request->imagecaption;
        $section_content->target_screen = $request->targetscreen;
        $section_content->section_id = $request->sectionid;
       
        $save = $section_content->save();
        if ($save) {
            return redirect("/admin/manage-landing-section-contents")
            ->with("err_msg","record udpate successfully!");
        }else{
            return redirect("/admin/manage-landing-section-contents")
            ->with("err_msg","failed!");
        }
    }
   
     protected function viewMoreOrderDetails(Request $request)
     {

        $cdn_url =   config('app.app_cdn')    ;  
        $cdn_path =  config('app.app_cdn_path')  ; 
        $cms_base_url =  config('app.app_cdn')   ;  
        $cms_base_path = $cdn_path  ;  
        $image  =   $cdn_url .  "/assets/image/no-image.jpg";
        $userid= $request->session()->get('__user_id_');
        $orderno = $request->orderno;

        if($orderno == "")
        {
            return redirect("/services/view-merchant-wise-booking"); 
        }
        $order_info   = DB::table("ba_service_booking") 
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
        ->select("ba_service_booking.*", "ba_business.name as businessName","ba_business.locality as businessLocality", 
          "ba_business.landmark as businessLandmark", "ba_business.city as businessCity", 
          "ba_business.state as businessState", "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",  "ba_business.banner" )
        ->where("ba_service_booking.id", $orderno )    
        ->first() ;

        if( !isset($order_info))
        {
          return redirect("/services/view-merchant-wise-booking"); 
        }

        $bin = $order_info->bin;

      $order_items   = DB::table("ba_service_booking_details")   
      ->join("ba_service_products", "ba_service_products.srv_name",  "=", "ba_service_booking_details.service_name")
      ->where("ba_service_booking_details.book_id", $orderno )    
      ->select("ba_service_booking_details.*","ba_service_products.*")
      ->get() ;


      $customer_info   = DB::table("ba_profile") 
      ->where("id", $order_info->book_by )    
      ->first() ;
   
      if( !isset($customer_info))
      {
        return redirect("/services/view-merchant-wise-booking"); 
      }

      $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ;

  
      

      $today = date('Y-m-d');

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        }) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0)
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();

      $user_info = DB::table("ba_users")
      ->where('id',$userid)
      ->first();

      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("is_booktou_staff", "yes" )
      ->where("ba_users.status", "<>", "100" )  
      ->where("ba_users.category",   "100" )   
      ->select("ba_profile.*", "ba_users.category" ) 
      ->get();
  


      $result = array('order_info' => $order_info ,        
        'customer' =>  $customer_info, 
        'order_items' => $order_items,       
        'remarks' => $remarks ,
        'all_agents' =>$all_agents, 
        'staffs' =>$all_staffs,
       );

      return view("admin.bookings.view_more_booking_details")->with('data',$result);

     }
       //use this to update remarks
    protected function updateNormalOrderRemarks(Request $request)
     {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/admin/booking/orders-view-all"); 
        }
    
        if( $request->remarks == "" || $request->type == "" )
        {
          if($request->turl == "list")
          {
            return redirect("/admin/booking/orders-view-all")->with("err_msg", "Order remarks fields missing!"); 
          }
          else 
          {
            return redirect("/admin/booking/order/view-more-details/" . $orderno )->with("err_msg", "Order remarks fields missing!"); 
          }
          
        }

        $order_info   = ServiceBooking::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/admin/customer-care/orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $remark = new OrderRemarksModel();
        $remark->order_no = $orderno;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = $request->type ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();   

         if($request->turl == "list")
          {
            return redirect("/admin/booking/orders-view-all")->with("err_msg", "Order remarks updated!");   
          }
          else 
          {
            return redirect("/admin/booking/order/view-more-details/" . $orderno )->with("err_msg", "Order remarks updated!");   
          }     
     }
      protected function updateDiscount(Request $request)
    {
      $orderno = $request->order_no;
      $updatediscount = $request->updatediscount;
      $order_info = ServiceBooking::find($orderno);
        if( !isset($order_info))
        {
          return redirect("/admin/booking/orders-view-all")->with("err_msg", 'No order found to change delivery charge.'  );  
        }
        else 
        { 
          $order_info->discount = $updatediscount; 
          $order_info->save(); 
          return redirect("/admin/booking/order/view-more-details/" .  $orderno )->with("err_msg", 'Delivery charge updated.'  ); 
        }  
     }
 protected function manageLandingScreenSectionsEditor(Request $request)
 {
  $id = $request->id;
 
   $updatediscount = $request->displaysequence;
      $section = LandingScreenSectionModel::find($id);
        if( !isset($order_info))
        {
          return redirect("/admin/manage-landing-screen-sections")->with("err_msg", 'No order found to change delivery charge.'  );  
        }
        else 
        { 
          $section->display_sequence = $updatediscount; 
          $section->save(); 
          return redirect("/admin/manage-landing-screen-sections")->with("err_msg", 'Delivery charge updated.'  ); 
        }    
 }






}
