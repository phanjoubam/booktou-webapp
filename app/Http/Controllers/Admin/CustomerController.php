<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;


use App\CustomerProfileModel;
use App\Business; 
use App\ServiceBooking;
use App\Exports\CustomerThreeMonthsBackExport;
use App\Exports\CustomerShoppingTwoMonthsBackExport;
use App\Exports\CustomerBrowseLastWeekExport;
use App\Traits\Utilities; 
use PDF;

class CustomerController extends Controller
{
    protected function viewCUstomerAnalytics(Request $request)
    {

        $last3months = date('Y-m-d' , strtotime("90 days ago"));
        $todayDate = date('Y-m-d');

        $customer_list = DB::table('ba_service_booking')
        ->whereDate('book_date','>=',$last3months)
        ->where('source','customer')
        ->select(
            "book_by as profileId",
            "source",
            "id as orderNo"
        )
        ->get();

        $profile = array();
        foreach ($customer_list as $items) {
            $profile[] = $items->profileId; 
        }

        $userlist = DB::table('ba_users')
        ->whereNotIn('profile_id',$profile)
        ->where('category',0)
        ->get();

        foreach ($userlist as $items) {
             $cus_id[] = $items->profile_id;
        }

        $threemonthscount = DB::table('ba_profile')
        ->whereIn('id',$cus_id)
        ->orderBy('id','desc')
        ->select( DB::Raw("count(*) as totalCustomer"))
        ->first();


        $last2months = date('Y-m-d' , strtotime("60 days ago"));
        $todayDate = date('Y-m-d');

        $customer_list = DB::table('ba_service_booking')
        ->whereDate('book_date','>=',$last2months)
        ->where('source','customer')
        ->select( 
            "book_by as profileId" 
        )
        ->groupBy("book_by") 
        ->get();

        $profileId = array();
        foreach($customer_list as $items)
        {
            $profileId[] = $items->profileId;
        }

        $twomonthscount  = DB::table('ba_profile')
        ->whereIn('id',$profileId)
        ->select(DB::Raw("count(*) as countCustomer"))
        ->first();

        $lastweek = date('Y-m-d' , strtotime("7 days ago"));
        $two_months_back = date('Y-m-d',strtotime("60 days ago"));

        $browselist = DB::table('ba_logs_product_browsed')
        ->whereDate('browse_date',$lastweek) 
        ->select('member_id')
        ->groupBy('member_id')
        ->get();

        $id = $cid = $customerid = array();
        foreach($browselist as $items)
        {
            $id[] = $items->member_id;
        }

        $shopping = DB::table('ba_service_booking')
        ->whereDate('book_date','<=',$two_months_back)
        ->whereIn('book_by',$id)
        ->where('source','customer')
        ->select(DB::Raw("distinct(book_by) as profileId")) 
        ->get();

        foreach ($shopping as $items) {
            $cid[] = $items->profileId;
        }


        $userlist = DB::table('ba_users')
        ->whereIn('profile_id',$cid)
        ->where('category',0)
        ->get();

        foreach ($userlist as $items) {
             $customerid[] = $items->profile_id;
        }

        $lastweekcount  = DB::table('ba_profile')
        ->whereIn('id',$customerid)
        ->select(DB::Raw("count(*) as lastweekCount"))
        ->first(); 

        $data = array(
                        'analytics_one'=>$threemonthscount,
                        'analytics_two'=>$twomonthscount,
                        'analytics_three'=>$lastweekcount
                     );


        return view ('admin.customers.view_customer_analytics')->with($data);
    }

    protected function viewCustomerThreeMonthsBack(Request $request)
    {

        $last3months = date('Y-m-d' , strtotime("90 days ago"));
        $todayDate = date('Y-m-d');

        $customer_list = DB::table('ba_service_booking')
        ->whereDate('book_date','>=',$last3months)
        ->where('source','customer')
        ->select(
            "book_by as profileId",
            "source",
            "id as orderNo"
        )
        ->get();

        $profile = $cus_id = array();
        foreach ($customer_list as $items) {
            $profile[] = $items->profileId; 
        }

        $userlist = DB::table('ba_users')
        ->whereNotIn('profile_id',$profile)
        ->where('category',0)
        ->get();

        foreach ($userlist as $items) {
             $cus_id[] = $items->profile_id;
        }

        $result = DB::table('ba_profile')
        ->whereIn('id',$cus_id)
        ->orderBy('id','desc')
        ->select(
            "id as profileid",
            "fname",
            "mname",
            "lname",
            "fullname",
            "landmark",
            "locality",
            "phone" 
        )
        ->paginate(30); 

        $data = array('result'=>$result);

        return view ('admin.customers.view_customer_three_months_back')->with($data);
    }

    public function CustomerThreeMonthsBackToExcel(Request $request) 
    
    {
        $last3months = date('Y-m-d' , strtotime("90 days ago"));
        $todayDate = date('Y-m-d');
       
       return Excel::download(new CustomerThreeMonthsBackExport($last3months,$todayDate), 'customer_stop_using_booktou_3_months_back.xlsx');
    }


    protected function viewCustomerShoppingTwoMonthsBack(Request $request)
    {   
        $last2months = date('Y-m-d' , strtotime("60 days ago"));
        $todayDate = date('Y-m-d');

        $customer_list = DB::table('ba_service_booking')
        ->whereDate('book_date','>=',$last2months)
        ->where('source','customer')
        ->select( 
            "book_by as profileId" 
        )
        ->groupBy("book_by") 
        ->get();

        $profileId = $cus_id = array();
        foreach($customer_list as $items)
        {
            $profileId[] = $items->profileId;
        }

        $userlist = DB::table('ba_users')
        ->whereIn('profile_id',$profileId)
        ->where('category',0)
        ->get();

        foreach ($userlist as $items) {
             $cus_id[] = $items->profile_id;
        }

        $customer  = DB::table('ba_profile')
        ->whereIn('id',$cus_id)
        ->paginate(30);

        $data = array('result'=>$customer);
        return view ('admin.customers.view_shopping_two_months_back')->with($data);
    }

    protected function customerShoppingtwoMonthsBackToExcel(Request $request)
    {
        $last2months = date('Y-m-d' , strtotime("60 days ago"));
        $todayDate = date('Y-m-d');       
        return Excel::download(new CustomerShoppingTwoMonthsBackExport($last2months,$todayDate), 'customer_shopping_two_months_back.xlsx');
    }

    protected function viewCustomerBrowseLastWeek(Request $request)
    {
        $lastweek = date('Y-m-d' , strtotime("7 days ago"));

        $two_months_back = date('Y-m-d',strtotime("60 days ago"));

        $browselist = DB::table('ba_logs_product_browsed')
        ->whereDate('browse_date',$lastweek) 
        ->select('member_id')
        ->groupBy('member_id')
        ->get();

        $profileid = $cus_id = $customerid = array();
        foreach($browselist as $items)
        {
            $profileid[] = $items->member_id;
        }

        $shopping = DB::table('ba_service_booking')
        ->whereDate('book_date','<=',$two_months_back)
        ->whereIn('book_by',$profileid)
        ->where('source','customer')
        ->select(DB::Raw("distinct(book_by) as profileId")) 
        ->get();

        foreach ($shopping as $items) {
            $cus_id[] = $items->profileId;
        }


        $userlist = DB::table('ba_users')
        ->whereIn('profile_id',$cus_id)
        ->where('category',0)
        ->get();

        foreach ($userlist as $items) {
             $customerid[] = $items->profile_id;
        }

        $customer  = DB::table('ba_profile')
        ->whereIn('id',$customerid)
        ->paginate(30);
        $data = array('result'=>$customer);
        return view ('admin.customers.view_browse_last_week')->with($data);
    }

    protected function CustomerBrowseLastWeekToExcel(Request $request)
    {
        $lastweek = date('Y-m-d' , strtotime("7 days ago"));
        $two_months_back = date('Y-m-d',strtotime("60 days ago"));

        return Excel::download(new CustomerBrowseLastWeekExport($lastweek,$two_months_back), 'customer_last_week_browsing.xlsx');
    }

    protected function viewCustomerRatings(Request $request)
    {
        if ($request->month=="") {
           $month = date('m'); 
        }else{
           $month = $request->month;
        }
        
        if ($request->year=="") {
                    $year = date('Y');
        }else{
                    $year = $request->year;   
        }

        $monthname = date("F", mktime(0, 0, 0, $month, 1)); 
        $result = DB::table('ba_service_ratings')
        ->whereMonth('rated_on',$month)
        ->whereYear('rated_on',$year)
        ->select('ba_service_ratings.*',
                DB::Raw("'' orderType"), 
                DB::Raw("'' customerName") 
                )
        ->get();
       
        $orderid = $memberid = array();

        foreach($result as $items)
        {
            $orderid[] = $items->order_no;
            $memberid[] = $items->member_id;
        }


        $orderSequencer = DB::table('ba_order_sequencer')
        ->whereIn('id',$orderid)
        ->get();

        $customerProfile = DB::table('ba_profile')
        ->whereIn('id',$memberid)
        ->get();

        foreach($result as $res)
        {
            foreach($orderSequencer as $seq)
            {
                if ($res->order_no == $seq->id) {
                     $res->orderType = $seq->type;
                }
            }

            foreach($customerProfile as $customer)
            {
                if ($res->member_id == $customer->id) {
                     $res->customerName = $customer->fullname;
                }
            }
        }

        $data = array('result'=>$result,'monthname'=>$monthname,'year'=>$year);

        return view ('admin.customers.view_customer_ratings')->with($data);
    }

}
