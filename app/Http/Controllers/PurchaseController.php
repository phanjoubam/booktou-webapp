<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ServiceBookingModel;
use App\ServiceBookingDetailsModel;
use App\ShoppingBasketModel;


class PurchaseController extends Controller
{
    
   

public function enterPurchaseDetails(Request $request)
{ 
  
  $session = session()->get('prod_details');

if($session) 
{
   //$cookieData = stripcslashes(session()->get('prod_details'));
   $product = json_encode(session()->get('prod_details'));
   $itemdata =json_decode($product,true);
   $totalcart = count($itemdata);
   //return $itemdata;
   //$totalcart = 0;
    
}else
{
    $product =  array();     
    $totalcart = 0;
}


  
return view('purchase.purchase-details')
->with('total_cart',$totalcart);

}





protected function showlistofProduct(Request $request)
{
    

    $product_code = $request->product_code;
    $result = DB::table('ba_products')
                ->where('pr_code',$product_code)
                ->where('stock_inhand',">",0)
                ->select()
                ->get();

    $cart = session()->get('prod_details');
    if($result)
        {
         $data = array('product'=>$result );
         $returnHTML = view('purchase.productList')->with($data)->render(); 
        }

    //if no data in the session variable

    if(!$cart)
    {
      foreach ($result as $val) {
      $cart =[$val->id=>[

                                    'prCode'=> $val->pr_code,
                                    'prName'=> $val->pr_name,
                                    'pr_description'=> $val->description,
                                    'photos'=> $val->photos,
                                    'initialStock'=> $val->initial_stock,
                                    'stockInhand'=> $val->stock_inhand,
                                    'packType'=> $val->pack_type,
                                    'unitPrice'=> $val->unit_price,
                                    'quantity'=> "1",
                                    'unitValue'=> $val->unit_value,
                                    'unitName'=> $val->unit_name,
                                    'category'=> $val->category,
                                    'foodType'=> $val->food_type,
                                    'discount'=> $val->discount,
                                    'cgst'=> $val->cgst_pc,
                                    'sgst'=> $val->sgst_pc,
                                    'packaging'=> $val->packaging

        ]


      ];
    }


    

    session()->put('prod_details',$cart);

    $product = json_encode(session()->get('prod_details'));
    $itemdata =json_decode($product,true);
    $totalcart = count($itemdata);

    return response()->json(['success'=>true,'html'=>$returnHTML,
    'status'=>'"'.$product_code.'" Added to Cart','total_cart'=>$totalcart]);

    }


    //if data in the session variable
    foreach ($result as $val) {

      if(isset($cart[$val->id]))
      {
        $msg = "Product already in the cart";
        return response()->json(['success'=>true,'warning'=>$msg]);

      }else{
              $cart[$val->id] = [

                                    'prCode'=> $val->pr_code,
                                    'prName'=> $val->pr_name,
                                    'pr_description'=> $val->description,
                                    'photos'=> $val->photos,
                                    'initialStock'=> $val->initial_stock,
                                    'stockInhand'=> $val->stock_inhand,
                                    'packType'=> $val->pack_type,
                                    'unitPrice'=> $val->unit_price,
                                    'quantity'=> "1",
                                    'unitValue'=> $val->unit_value,
                                    'unitName'=> $val->unit_name,
                                    'category'=> $val->category,
                                    'foodType'=> $val->food_type,
                                    'discount'=> $val->discount,
                                    'cgst'=> $val->cgst_pc,
                                    'sgst'=> $val->sgst_pc,
                                    'packaging'=> $val->packaging

        ];

    session()->put('prod_details',$cart);

    $product = json_encode(session()->get('prod_details'));
    $itemdata =json_decode($product,true);
    $totalcart = count($itemdata);

    return response()->json(['success'=>true,'html'=>$returnHTML,
    'status'=>'"'.$product_code.'" Added to Cart','total_cart'=>$totalcart]);
      }   

  }

}



public function updateQuantity(Request $request)
    {
        $prod_code = $request->prodCode;
        $quantity = $request->quantity;
        $id = $request->txtproduct_id;
        
        //echo $prod_code;die();
        $cart = session()->get('prod_details');
        if(!$cart)
          {
            
          }

          $cookieData = json_encode(session()->get('prod_details'));
            $product = json_decode($cookieData, true);


            $item_id_list = array_column($product, 'prCode');
            $prod_code_check = $prod_code;

            if(in_array($prod_code_check, $item_id_list))
            {
                foreach($product as $keys=>$val)
                {
                  
                   //$input_quantity = $prod_code;
                   //$from_session = $values['prName'];
                   
                   if($val['prCode'] == $prod_code)
                     {

                      $cart[$keys]=

                                [     'prCode'=> $val['prCode'],
                                      'prName'=> $val['prName'],
                                      'pr_description'=> $val['pr_description'],
                                      'photos'=> $val['photos'],
                                      'initialStock'=> $val['initialStock'],
                                      'stockInhand'=> $val['stockInhand'],
                                      'packType'=> $val['packType'],
                                      'unitPrice'=> $val['unitPrice'],
                                      'quantity'=> $quantity,
                                      'unitValue'=> $val['unitValue'],
                                      'unitName'=> $val['unitName'],
                                      'category'=> $val['category'],
                                      'foodType'=> $val['foodType'],
                                      'discount'=> $val['discount'],
                                      'cgst'=> $val['cgst'],
                                      'sgst'=> $val['sgst'],
                                      'packaging'=> $val['packaging']

                        ];

                       session()->put('prod_details', $cart);
                     
                      //$msg=$val['pr_code'];
                     $msg="Quantity updated";
                     return response()->json(['success'=>true,'msg'=>$msg]);
                     
                     }
                }
            }



    }



protected function saveOrderDetails(Request $request)
{
    //
  $orderNO=0;  
  //fetching result id from ba_serviceBooking
  $resultOrder = DB::table('ba_service_booking')
  ->select(DB::Raw('max(id) as orderID'))
  ->get();
  //
  
  if($resultOrder)
    {
      foreach ($resultOrder as $order) {
        $orderNO = $order->orderID + 1;
      }
    }
    else
    {
      $orderNO = $orderNO + 1;
    }

  
  $cookieData = json_encode(session()->get('prod_details'));
  $product = json_decode($cookieData, true);

  
  if($product)
  {         

            $sellerPayable=0;
            $serviceFee=0;
            $discount =0;

            foreach ($product as $key => $val) {
              
            $sellerPayable +=  $val['quantity'] * $val['unitPrice']; 
            $discount += $val['discount'];
            }
            
            //echo $sellerPayable;die();


            //service booking adding parts starts here

            $servicebooking = new ServiceBookingModel();
            
            $servicebooking->id = $orderNO ;
            $servicebooking->bin = 240 ;
            $servicebooking->book_category= "VARIETY STORE";
            $servicebooking->book_by  = 0 ;
            $servicebooking->staff_id  = 0;
            $servicebooking->otp = 123456 ;
            $servicebooking->address = "NA" ;
            $servicebooking->landmark = "NA" ;
            $servicebooking->city = "NA" ;
            $servicebooking->state = "NA" ;
            $servicebooking->pin_code = "NA" ;
            $servicebooking->latitude = 0 ;
            $servicebooking->longitude = 0 ;
            $servicebooking->delivery_charge = 0 ;
            $servicebooking->agent_payable = 0 ;
            $servicebooking->cashback = 0 ;
            $servicebooking->delivery_otp = 123456 ;
            $servicebooking->sms_sent = "no" ;
            $servicebooking->order_source = "browser" ;

            

            $servicebooking->book_date = new \DateTime();
            $servicebooking->payment_type = "CASH";
            $servicebooking->seller_payable = $sellerPayable;
            $servicebooking->service_fee = $serviceFee;
            $servicebooking->discount = $discount ;
            
            
            
            //total_cost = seller_payable* service_fee; 
            $total = $sellerPayable + $serviceFee;//setting service fee as 0 for demo
            
            $servicebooking->total_cost =  $total;

            //service booking record entry ends here
 
            $save = $servicebooking->save();

            //saving the record in ba_service_booking table
           
            foreach ($product as $key => $shop) {

            $shoppingBasket = new ShoppingBasketModel();
            //shopping basket record entry part

            
              
            $shoppingBasket->order_no = $orderNO;
            $shoppingBasket->pr_code = $shop['prCode'];
            $shoppingBasket->pr_name =$shop['prName'];
            $shoppingBasket->description = $shop['pr_description'];
            $shoppingBasket->qty = $shop['quantity'];
            $shoppingBasket->unit = $shop['unitName'];
            $shoppingBasket->price = $shop['unitPrice'];
            $shoppingBasket->cgst = $shop['cgst'];
            $shoppingBasket->sgst = $shop['sgst'];
            $shoppingBasket->package_charge=$shop['packaging'];
            $shoppingBasket->category_name = $shop['category'];

            

            $saveShopping = $shoppingBasket->save();

            session()->forget('prod_details.'.$key);
            }

            
            
            
            if ($save && $saveShopping) {
              
            return redirect("/purchase/enter-purchase-details")->with("msg", "Purchase sucessfull!");
            }
            else
            {
                return redirect("/purchase/enter-purchase-details")->with("msg", "Failed!");
            }
  }

}






} 