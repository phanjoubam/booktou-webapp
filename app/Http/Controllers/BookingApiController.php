<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Business; 
use App\ServiceDuration;
use App\BusinessService;
use App\BusinessHour;
use App\BusinessPhoto;
use App\Traits\Utilities;
use App\ServiceBooking;
use App\User;
use App\StaffAvailability;


class BookingApiController extends Controller
{ 
	
	use Utilities;
	 
   
   
  
   
   public function cancelClientBooking(Request $request)
   {
	   
	  $user_info =  json_decode($this->getUserInfo($request->userId)); 
	  if(  $user_info->user_id == "na" )
	  {
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
		return json_encode($data);
	  }
	   
		if(  $request->bookingNo == "" 	)
	   {
		 $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
	   }
	    
	    
		$day_booking = ServiceBooking::find(  $request->bookingNo)  ;
		
		if( !isset($day_booking))
		{
			$data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'No booking record found!'  ];
			return json_encode($data);
		}
		
		
		if(  $request->source == "client" 	)
	   {
		  $action_by = 'cancel_by_client';
	   }
	   else 
	   {
		   $action_by = 'cancel_by_owner';
	   }
	   
	   
   
   if(count($request->serviceTime ) > 0)
   {
	   //cancel services by time 
	   DB::table('ba_service_booking_details')
	   ->where('book_id', $request->bookingNo  )
	   ->whereRaw("service_time in ('" .  implode("','",  $request->serviceTime  ) .  "' )")  
	   ->update(['status' => $action_by  ]);  
	   
   }
   else
   {
	   //cancel booking table 
	   DB::table('ba_service_booking')
	   ->where('id', $request->bookingNo  ) 
	   ->update(['book_status' => $action_by ]); 
	   
	   //cancel all services 
	   DB::table('ba_service_booking_details')
	   ->where('book_id', $request->bookingNo  ) 
	   ->update(['status' => $action_by  ]); 
	    
   }
   
   
	   $message = "success";
	   $err_code = 1065;
	   $err_msg = "Your booking is cancelled!";  
		
	
	$data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg  ]; 
    return json_encode( $data);
	
  }

	public function cancelAllClientBookingId(Request $request)
	{

		$user_info =  json_decode($this->getUserInfo($request->userId)); 
		if(  $user_info->user_id == "na" )
		{
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
		return json_encode($data);
		}

		if(  $request->bookingNo == "" 	)
		{
		 $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
		}


		$day_booking = ServiceBooking::find(  $request->bookingNo)  ;

		if( !isset($day_booking))
		{
			$data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'No booking record found!'  ];
			return json_encode($data);
		}


		if(  $request->source == "client" 	)
		{
		  $action_by = 'cancel_by_client';
		}
		else 
		{
		   $action_by = 'cancel_by_owner';
		}



		//cancel booking table 
		DB::table('ba_service_booking')
		->where('id', $request->bookingNo  ) 
		->update(['book_status' => $action_by ]); 

		//cancel all services 
		DB::table('ba_service_booking_details')
		->where('book_id', $request->bookingNo  ) 
		->update(['status' => $action_by  ]); 


		$message = "success";
		$err_code = 2023;
		$err_msg = "Your booking is cancelled!";  


		$data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg  ]; 
		return json_encode( $data);

	}
   
  
  
}
