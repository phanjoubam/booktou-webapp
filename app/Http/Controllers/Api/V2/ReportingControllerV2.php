<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;

use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\CustomerProfileModel;

use App\Traits\Utilities;
use App\User;

class ReportingControllerV2 extends Controller
{
    use Utilities;

    // View Earning Report for a day/month
       protected function viewDailyEarningReport(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));  
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 


        if($request->reportDate == "" )
        {

        	$today =   date('Y-m-d');

        }
        else
        {
        	$today = date('Y-m-d', strtotime( $request->reportDate ));
        }

        $totalEarning = 0; 

        $delivery_list  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id",$user_info->member_id)
        ->whereDate('completed_on', $today)  
        ->select(
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            'book_status as orderStatus',
            'delivery_charge as deliveryCharge'
        )
        ->get();
 


       // return json_encode($orderId);
        if( !isset( $delivery_list))
        {
          
           $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Earning report are fetched.' , 
           "totalEarning" => $totalEarning,  'results' =>  array() ];
          return json_encode( $data);
          
        }


        $customerIds =array();
        foreach ($delivery_list as $item) 
        {
            $totalEarning += $item->deliveryCharge;
        	$customerIds[] = $item->orderBy;
        }

       
         $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();

        
       foreach ($delivery_list as $item) 
       {
            foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;break;
                }

            }   
       }


        $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Earning report are fetched.' , 
        "totalEarning" => $totalEarning,  'results' =>  $delivery_list  ];

        return json_encode( $data);
     }




     protected function viewMonthlyEarningReport(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));  
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }

        if($request->month !="" )
        {

            $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }
        
        $totalEarning = 0;
 
        $orderId=$request->orderId;
        
        $delivery_list  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id",$user_info->member_id) 
        ->whereMonth('completed_on', $month )
        ->whereYear('completed_on', $year )

        ->select(
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            'book_status as orderStatus',
            'delivery_charge as deliveryCharge'
        )
        ->get(); 

       // return json_encode($orderId);
        if( !isset( $delivery_list))
        {
          
           $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Earning report are fetched.' , 
           "totalEarning" => $totalEarning,  'results' =>  array() ];
          return json_encode( $data);
          
        }


        $customerIds =array();
        foreach ($delivery_list as $item) 
        {
            $totalEarning += $item->deliveryCharge;
            $customerIds[] = $item->orderBy;
        }

       
         $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();

        
       foreach ($delivery_list as $item) 
       {
            foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;break;
                }

            }   
       }


        $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Earning report are fetched.' , 
        "totalEarning" => $totalEarning,  'results' =>  $delivery_list  ];

        return json_encode( $data);
     }


}
