<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

 
use App\Business;  
use App\ProductModel;

use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;

use App\Traits\Utilities;
use App\User;




class CustomerCareControllerV2 extends Controller
{
	use Utilities;

//Delete Customer Delivery Address
    public function deleteAddress(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }

        $addressInfo = DB::table("ba_member_address")
        ->where("member_id",$user_info->member_id    )
        ->where("address_id",$request->addressId)
        ->delete();


        $addressData = AddressModel::find($request->addressId);

         if ($addressData) {

        	  $addressData->delete();

        	 $data= ["message" => "success", "status_code" =>  5031 ,  'detailed_msg' => 'Address removed successfully.'  ];
            }

         else{

        	$data= ["message" => "failure", "status_code" =>  5032 ,  'detailed_msg' => 'Address could not be removed.'  ];
        }
       
        return json_encode( $data);
    }  

}
