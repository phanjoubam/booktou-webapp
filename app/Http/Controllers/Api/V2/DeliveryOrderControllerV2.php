<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Artisan;


use App\Jobs\SendAgentOrderProcessingSms; 

use App\Libraries\FirebaseCloudMessage;


use App\Business;  
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\AgentLocationModel;
use App\CustomerProfileModel;
use App\CustomerReviewModel;
use App\Traits\Utilities;
use App\User;
 

class DeliveryOrderControllerV2 extends Controller
{
    use Utilities;
 

    // Order Processing by delivery agent(Packages Pickedup/Delivered)
    public function updateDeliveryOrderStatus(Request $request)
    {

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data); 
        }

        //save agent location 
        if($request->latitude != "" && $request->longitude != "")
        {
          DB::table('ba_agent_locations')
          ->where('staff_id', $user_info->member_id  )
          ->whereDate("created_at", '=', date('Y-m-d'))
          ->increment('seq_no', 1); 

          $newloc = new AgentLocationModel;
          $newloc->latitude  = $request->latitude;
          $newloc->longitude  = $request->longitude; 
          $newloc->staff_id  = $user_info->member_id;
          $newloc->seq_no = 1;
          $newloc->save();
        }

        //block if  user is not delivery agent  code # 100 is agent
        if( $user_info->category  != 100 )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to delivery agent only.'  ];
          return json_encode($data);  
        }

        if( $request->orderNo == ""  ||  $request->orderStatus == ""  ||   
          
          ( $request->orderStatus != "delivered" && $request->orderStatus != "returned"  && $request->orderStatus != "package_picked_up"  ) 
        )
        {

         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
         
        } 

        $new_status = $request->orderStatus ; 

        //now check if the order is in his/her delivery list    
        $delivery_order_info = DeliveryOrderModel::where("member_id", $user_info->member_id)
        ->where("order_no", $request->orderNo)
        ->first(); 


        if( !isset($delivery_order_info))
        {
          $data= ["message" => "failure",   "status_code" =>  999 ,
          'detailed_msg' => 'Selected order is not in your delivery list!'  ];
          return json_encode( $data); 
        } 

        //find order 
        $order_info  = ServiceBooking::find( $request->orderNo  );
        if( !isset($order_info))
        {
            $data= ["message" => "failure",   "status_code" =>  999 ,  'detailed_msg' => 'Order Infomation not found!'  ];
            return json_encode( $data);
        } 


        if(  $request->orderStatus == "delivered" && $order_info->delivery_otp != $request->deliveryOtp   )
        {
            $data= ["message" => "failure",   "status_code" =>  999 , 
            'detailed_msg' => 'Delivery confirmation OTP does not match. Delivery is not complete.'  ];
            return json_encode( $data);
        }  
        
 
        //when order is picked up it should be marked as in route
        if(  $new_status == "package_picked_up"   )
        {
           $new_status = "in_route";
		   
		   //cloud message  
			$response = array();
			$response['title']     = "Hurray! Package is in-route";
			$response['message']   = "Package has been picked-up by a bookTou agent to deliver."; 
			$response['image'] = config('app.app_cdn')  . "/assets/image/in-route.jpg"; 
			
			$receipent = User::where("profile_id",  $order_info->book_by )->first()  ; 
			if(  isset($receipent ) )
			{
				$firebase = new FirebaseCloudMessage();
				$regId    = $receipent->firebase_token;
				$response = $firebase->sendToIndividual( $regId, $response );
				 
			}
			 
        }  

        //start updating  
        // value -  delivered, returned
       $order_info->book_status =  $new_status;
       $save = $order_info->save();
 
 

       //taking log  

       if( $save )
       {

          DB::table('ba_order_status_log')
          ->where('order_no', $order_info->id)
          ->increment('seq_no', 1); 
  
          //delivery log 
          if(  $request->orderStatus == "delivered"  || $request->orderStatus == "returned"   )
          {
              $delivery_order_info->completed_on= date('Y-m-d H:i:s');
              $delivery_order_info->delivery_remark = $request->remarks;
              $delivery_order_info->save(); 

              $updateOrderStatus = new OrderStatusLogModel;
              $updateOrderStatus->order_no = $order_info->id;
              $updateOrderStatus->order_status= $order_info->book_status;
              $updateOrderStatus->log_date=  date('Y-m-d H:i:s' ); 
              $updateOrderStatus->save(); 

            }
            else 
            {
  
              $updateOrderStatus = new OrderStatusLogModel;
              $updateOrderStatus->order_no = $order_info->id;
              $updateOrderStatus->order_status= "package_picked_up"  ;
              $updateOrderStatus->log_date=  date('Y-m-d H:i:s' ); 
              $updateOrderStatus->save();

              DB::table('ba_order_status_log')
              ->where('order_no', $order_info->id)
              ->increment('seq_no', 1); 
 
              $updateOrderStatus = new OrderStatusLogModel;
              $updateOrderStatus->order_no = $order_info->id;
              $updateOrderStatus->order_status= "in_route"  ;
              $updateOrderStatus->log_date=  date('Y-m-d H:i:s' ); 
              $updateOrderStatus->save();
 

            }  


            SendAgentOrderProcessingSms::dispatch( $order_info , $user_info->member_id, $request->orderStatus   ) ;   
            /*
            $memberid = $user_info->member_id;
            $order_status =  $request->orderStatus ;

            SendAgentOrderProcessingSms::dispatch($order_info, $memberid , $order_status, function ()
            { 
              Artisan::call('queue:work --stop-when-empty');
            });
            */


            $data= ["message" => "success", "status_code" =>  5071  ,  'detailed_msg' => 'Order delivery completed successfully.'];

       }
       else
       {

            $data= ["message" => "failure", "status_code" =>  5072 ,  'detailed_msg' => 'Order delivery status update failed.'];
       }
           
       return json_encode( $data);   


    }




    public function addOrderUnderDeliveryList(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data); 
        }
 
    
        //block if  user is not delivery agent  code # 100 is agent
        if( $user_info->category  != 100 )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to delivery agent only.'  ];
          return json_encode($data);  
        }

        if( $request->orderNo == ""  )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


        //find order 
        $order_info  = ServiceBooking::find( $request->orderNo  );
        if( !isset($order_info))
        {
            $data= ["message" => "failure",   "status_code" =>  999 ,  'detailed_msg' => 'Order Infomation not found!'  ];
            return json_encode( $data);
        } 


        if(  $order_info->book_status !=  "order_packed"  )
        {
            $data= ["message" => "failure",   "status_code" =>  999 ,  'detailed_msg' => 'Order is not ready for delivery assignment.'  ];
            return json_encode( $data);
        } 
 

        //now check if the order is is new or confirmed        
        $delivery_order_info = DeliveryOrderModel::where("order_no", $request->orderNo ) 
        ->first();
 

        if(  isset($delivery_order_info) )
        {
          $data= ["message" => "failure",   "status_code" =>  999 ,
          'detailed_msg' => 'Order is not available for delivery.'  ];
          return json_encode( $data); 
        } 

        
       $delivery_order_info = new DeliveryOrderModel;
       $delivery_order_info->order_no = $request->orderNo ;
       $delivery_order_info->member_id = $user_info->member_id ;
       $delivery_order_info->accepted_date = date('Y-m-d H:i:s');
       $save  = $delivery_order_info->save() ; 

       if( $save )
       {

          $cust_info = CustomerProfileModel::find($order_info->book_by);
          if(isset($cust_info))
          {
            $msg = "Your order delivery is picked by a delivery agent " . $user_info->name . " ( " . $user_info->phone ." ). Items will reach shortly."; 
            //sms to buyer
            $this->sendSmsAlert( array( $cust_info->phone  ),  $msg ); 
          }
           
          //start updating   
          $order_info->book_status = "in_route" ;
          $order_info->save();

          $data= ["message" => "success", "status_code" =>  5077  ,  'detailed_msg' => 'Order picked up by delivery agent.'];

       }
       else
       {

            $data= ["message" => "failure", "status_code" =>  5078 ,  'detailed_msg' => 'Order assignment for delivery failed.'];
       }
           
       return json_encode( $data);   


    }

      //List down all order for an agent
    public function getOrderLists(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }

         //save agent location 
       if($request->latitude != "" && $request->longitude != "")
       {
         DB::table('ba_agent_locations')
         ->where('staff_id', $user_info->member_id  )
         ->whereDate("created_at", '=', date('Y-m-d'))
         ->increment('seq_no', 1); 

         $newloc = new AgentLocationModel;
         $newloc->latitude  = $request->latitude;
         $newloc->longitude  = $request->longitude; 
         $newloc->staff_id  = $user_info->member_id;
         $newloc->seq_no = 1;
         $newloc->save();
       }
	   
	    //block if  user is not store owner code # 1 is store owner or business
        if( $user_info->category  != 100  )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to delivery agents only.'  ];
          return json_encode($data);
        }

        //block if  user is not agent. Code # 100 is agent
        if( $user_info->category  != 100  )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available agent only.'  ];
          return json_encode($data);
        }

        $date = date('Y-m-d', strtotime($request->assignDate));
 
        if ($request->assignDate=="")
         {
            $orderInfo = DB::table("ba_delivery_order")
            ->where("member_id",$user_info->member_id)
            ->whereDate("accepted_date",   date('Y-m-d')  )
            ->orderBy("id", "desc")
            ->get();
        }
        else{

            $orderInfo = DB::table("ba_delivery_order")
            ->where("member_id",$user_info->member_id)
            ->whereDate("accepted_date",$date) 
            ->orderBy("id", "desc")
            ->get();
        }

        if( !isset($orderInfo))
        {
          
          $data= ["message" => "failure",   "status_code" =>  999 ,
          'detailed_msg' => 'Delivery Order Infomation not found!'  ];
          return json_encode( $data);
          
        } 
        
		$orderId = array();
		$pndorderid  = array();
        $acceptedDate = "na";
        foreach ($orderInfo as $item)
		{
			$acceptedDate = $item->accepted_date;
			
			if( strcasecmp($item->order_type, "client order") == 0)
			{
				$orderId[] = $item->order_no; 
			}
			else  if( strcasecmp($item->order_type, "pick and drop") == 0)
			{
				$pndorderid[] = $item->order_no; 
			}
			
		}
	 
		 
		 
		$allorders = array(); 
		$orderDetails = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
        ->whereIn("ba_service_booking.id",$orderId)
        ->select("ba_service_booking.id as orderId",
              "ba_service_booking.service_date as bookDate",
              "ba_service_booking.book_status as orderStatus",
              "ba_service_booking.address as deliveryAddress",
              "ba_service_booking.landmark as deliveryLandmark",
              "ba_service_booking.city as deliveryCity",
              "ba_service_booking.state as deliveryState",
              "ba_service_booking.pin_code as deliveryPin", 
              "ba_service_booking.latitude as deliveryLat",
              "ba_service_booking.longitude as deliveryLong", 
              "ba_service_booking.delivery_charge as deliveryCharge",  
			  "ba_service_booking.agent_payable as agentCommission",  
			  "ba_service_booking.payment_type as payMode",  
              "ba_profile.fullname as customerName",  
              "ba_profile.phone as customerPhone",
              "ba_business.name as storeName",
              "ba_business.locality as pickUpLocality",
              "ba_business.landmark as pickUpLandmark",
              "ba_business.city as pickUpCity",
              "ba_business.state as pickUpState",
              "ba_business.pin as pickUpPin",
              "ba_business.phone_pri as businessPhone",
              "ba_business.latitude as businessLat",
              "ba_business.longitude as businessLong" ,
              "ba_service_booking.total_cost as totalAmountOfOrder",
			  DB::Raw("'normal customer order' remarks") ,
			  DB::Raw("'normal' orderType") ,
			  DB::Raw("'no' taskAccepted") ,
			  DB::Raw("'no' bizNotified") 
            )
            ->orderBy("ba_service_booking.id", "desc")
            ->get();

            foreach($orderDetails as $item)
            {
              $item->deliveryLat = number_format( $item->deliveryLat ,6 ,".",""); 
              $item->deliveryLong = number_format( $item->deliveryLong ,6 ,".",""); 
              $item->businessLat = number_format( $item->businessLat ,6 ,".",""); 
              $item->businessLong = number_format( $item->businessLong ,6 ,".",""); 
		
			
				if( strcasecmp($item->payMode, "Cash on delivery") ==  0 || strcasecmp($item->payMode, "cash") ==  0  )
				{
					$item->payMode = "COD";
				}
				else if( strcasecmp($item->payMode, "Pay online (UPI)") ==  0  ||  strcasecmp($item->payMode, "ONLINE") ==  0   )
				{
					$item->payMode = "ONLINE";
				}
				else 
				{
					$item->payMode = "COD";
				}  
				
				foreach($orderInfo as $oitem)
				{
					if($item->orderId == $oitem->order_no )
					{
						$item->taskAccepted = $oitem->agent_ready ;
						$item->bizNotified = $oitem->biz_alerted ;
						break;
					}
				}
				
				$allorders[] = $item;
            }
			
			
			
		//pick and drop orders 	
		$pickdroporders = DB::table("ba_pick_and_drop_order")
		->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
		->whereIn("ba_pick_and_drop_order.id", $pndorderid) 
        ->select("ba_pick_and_drop_order.id as orderId",
              "ba_pick_and_drop_order.service_date as bookDate",
              "ba_pick_and_drop_order.book_status as orderStatus",
              "ba_pick_and_drop_order.address as deliveryAddress",
              "ba_pick_and_drop_order.landmark as deliveryLandmark",
              
			  DB::Raw("'na' deliveryCity"),
			  DB::Raw("'na' deliveryState"),
			  DB::Raw("'na' deliveryPin"),
			  DB::Raw("'na' deliveryCity"),
			  "ba_pick_and_drop_order.latitude as deliveryLat",
			  "ba_pick_and_drop_order.latitude as deliveryLong",
			  
			  "ba_pick_and_drop_order.service_fee as deliveryCharge",  
			  DB::Raw("'0.00' agentCommission"),
			  "ba_pick_and_drop_order.pay_mode as  payMode" , 
			  "ba_pick_and_drop_order.fullname as customerName",  
              "ba_pick_and_drop_order.phone as customerPhone",
			   
              "ba_business.name as storeName",
              "ba_business.locality as pickUpLocality",
              "ba_business.landmark as pickUpLandmark",
              "ba_business.city as pickUpCity",
              "ba_business.state as pickUpState",
              "ba_business.pin as pickUpPin",
              "ba_business.phone_pri as businessPhone",
              "ba_business.latitude as businessLat",
              "ba_business.longitude as businessLong" ,
              "ba_pick_and_drop_order.total_amount as totalAmountOfOrder",
			  "cust_remarks as remarks" ,
			  DB::Raw("'pnd' orderType") ,
			   DB::Raw("'no' taskAccepted") ,
			   DB::Raw("'no' bizNotified") 
            )
            ->orderBy("ba_pick_and_drop_order.id", "desc")
            ->get();
		
			foreach($pickdroporders as $item)
            {
              $item->deliveryLat = number_format( $item->deliveryLat ,6 ,".",""); 
              $item->deliveryLong = number_format( $item->deliveryLong ,6 ,".",""); 
              $item->businessLat = number_format( $item->businessLat ,6 ,".",""); 
              $item->businessLong = number_format( $item->businessLong ,6 ,".",""); 
			  
			  $item->totalAmountOfOrder += $item->deliveryCharge;
			  
			  $item->agentCommission = $item->deliveryCharge - (0.4* $item->deliveryCharge) ."";
			
				if( strcasecmp($item->payMode, "Cash on delivery") ==  0 || strcasecmp($item->payMode, "cash") ==  0 )
				{
					$item->payMode = "COD";
				}
				else if( strcasecmp($item->payMode, "Pay online (UPI)") ==  0 ||  strcasecmp($item->payMode, "ONLINE") ==  0)
				{
					$item->payMode = "ONLINE";
				}
				else 
				{
					$item->payMode = "COD";
				}  
				
				foreach($orderInfo as $oitem)
				{
					if($item->orderId == $oitem->order_no )
					{
						$item->taskAccepted = $oitem->agent_ready ;
						$item->bizNotified = $oitem->biz_alerted ;
						break;
					}
				}
				
				$allorders[] = $item;
            }
			
			
           
            $data= ["message" => "success", "status_code" =>  5007 ,  'detailed_msg' => 'Order lists successfully fetched.' ,'assignDate' =>$acceptedDate, 'results' => $allorders];

         return json_encode( $data);
    }

       //Fiding Off Duty Staffs
    public function viewStaffStatus(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }

        if( $user_info->category  != 1  )
        {
          $data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'This feature is available to business owner.'  ];
          return json_encode($data);
        }

         if(  $request->staffId == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

          $staffProfile = DB::table("ba_profile")
            ->select("fullname as staffName",DB::Raw("'Off' isDuty"))
            ->where("id",$request->staffId)
            ->get();

            $agentInfo = DB::table("ba_delivery_order")
            ->where("member_id",$request->staffId)
            ->count();

           if($agentInfo<5) {
            foreach ($staffProfile as $value) {
        
                $value->isDuty = "On";
              
            }
          }
       
           $data= ["message" => "success", "status_code" =>  5007 ,  'detailed_msg' => 'Delivery Agent status successfully fetched.' ,'results' => $staffProfile];

         return json_encode( $data);
    }

     //View complete order details by delivery agent
      protected function getOrderDetails(Request $request)
     {
        $user_info =  json_decode($this->getUserInfo( $request->userId ));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 

        if( $user_info->category  != 100  )
        {
          $data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'This feature is available for agent only.'  ];
          return json_encode($data);
        }


        if( $request->orderNo   == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Order number not specified.'  ];
         return json_encode($data);
        }

        $order_info   = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->where("ba_service_booking.id", $request->orderNo )  
        ->select('ba_service_booking.id as orderNo',  "ba_service_booking.bin",
          "book_date as orderDate", 
           "ba_service_booking.service_date as serviceDate", 
            "fullname as customerName",  
            "address as deliveryAddress",  
            "phone as customerPhone",
            DB::Raw("'na' deliveryAgentName"),
            DB::Raw("'na' deliveryAgentPhone"),
            "book_status as status" ,
            "total_cost as totalCost",
            "delivery_charge as deliveryCharge",
			"ba_service_booking.payment_type as payMode", 
            "delivery_otp as deliveryOtp",
            "cashback as cashBack",
            "discount",
            "coupon", 
			DB::Raw("'no' taskAccepted") ,
            DB::Raw("'[]' orderItems") ,
            DB::Raw("'[]' timeline")
        ) 
        ->first() ;

        if( !isset($order_info) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching record found.'  ];
            return json_encode($data);
        }
		
		


        $order_items=  DB::table("ba_shopping_basket")
        ->join("ba_products","ba_shopping_basket.pr_code","=","ba_products.pr_code")
        ->select(
                "ba_products.id as productId", 
                "ba_shopping_basket.pr_code as productCode",
                 "ba_shopping_basket.pr_name as item",
                 "ba_shopping_basket.description",
                 "ba_shopping_basket.qty" , 
                 "ba_shopping_basket.unit",
                 "ba_shopping_basket.price",
                 "ba_shopping_basket.package_charge as packageCharge",
                 "ba_shopping_basket.custom_text as customText",
                 "ba_shopping_basket.category_name as category",
                 "photos as image" , 
                DB::Raw("'[]' addOns")
             )
        ->where("ba_shopping_basket.order_no", $order_info->orderNo  )
        ->get();



        $order_add_ons =  DB::table("ba_shopping_item_add_on")  
        ->where("order_no", $order_info->orderNo  )
        ->select("addon_id as addOnId", "pr_code as productCode", "addon_name as addOnName", "addon_price as addOnPrice")
        ->get(); 

        $orderStatusInfo =  DB::table("ba_order_status_log")
        ->select("order_status as orderStatus","log_date as statusDateTime")
        ->where("order_no", $order_info->orderNo  )
        ->get();
  
 

          //Fetch Agent Information
         $agentInfo=  DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
         ->select("ba_profile.fullname as deliveryAgentName","phone as deliveryAgentPhone", "agent_ready")
         ->where("order_no", $order_info->orderNo )
		 ->orderBy("ba_delivery_order.id", "desc")
         ->first(); 

         $agentName="na";
         $agentPhone="na";
		 $taskAccepted = "no";

         if(isset($agentInfo))
		 {
			 $agentName = $agentInfo->deliveryAgentName;
			 $agentPhone = $agentInfo->deliveryAgentPhone;
			 $taskAccepted = $agentInfo->agent_ready;
		 }
        

  
        $folder_path =  "/public/assets/image/store/bin_" . $order_info->bin .  "/";
        foreach( $order_items as $item)
        {
          
          $all_photo = URL::to("/public/assets/image/no-image.jpg");
          $files =    explode(",",  $item->image )   ;

          $files=array_filter($files); 
 
            foreach( $files  as $file)
            {
                $file = trim($file); 
                if(  file_exists( base_path() .  $folder_path .   $file )   )
                  {
                    $all_photo = URL::to( $folder_path  ) . "/" .    $file  ;
                    break;
                  }  
            }  
           $item->image = $all_photo; 

           $addOnes = array();
            foreach($order_add_ons as $addon)
            {
               if( $addon->productCode == $item->productCode )
               {
                $addOnes[] =  $addon;                 
               }
            } 
            $item->addOns =  $addOnes; 
            $item->customText = ($item->customText == "" ) ? "NONE" : $item->customText;


        }
 
        $order_info->orderItems =$order_items;
        $order_info->deliveryAgentName =$agentName;
        $order_info->deliveryAgentPhone =$agentPhone;   
        $order_info->timeline =$orderStatusInfo;
        $order_info->coupon = ($order_info->coupon == null ? "NONE" : $order_info->coupon); 
		$order_info->taskAccepted = $taskAccepted;
		
		if( strcasecmp($order_info->payMode, "Cash on delivery") ==  0 )
		{
			$order_info->payMode = "COD";
		}
		else if( strcasecmp($order_info->payMode, "Pay online (UPI)") ==  0 )
		{
			$order_info->payMode = "ONLINE";
		}
		else 
		{
			$order_info->payMode = "OTHER";
		} 
		
       
        $data= ["message" => "success", "status_code" =>  5009 ,  
        'detailed_msg' => 'Order complete details are fetched.' ,  
        'results' => $order_info ];

        return json_encode( $data);
     }



    public function postCustomerReview(Request $request)
    {

      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
        return json_encode($data);
      }

      if( $user_info->category  != 100  )
      {
          $data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'This feature is available for agent only.'  ];
          return json_encode($data);
      }


      if( $request->orderNo  == "" ||  $request->reviewTitle== "" || $request->reviewBody == "" || $request->star == ""  )
      {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' =>  "Field missing!"  ];
         return json_encode($data);
      }


      $order_info   = DB::table("ba_service_booking") 
      ->where("id", $request->orderNo )  
      ->first() ;

      if( !isset($order_info) )
      {
        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching record found.'  ];
        return json_encode($data);
      }

      //check if order is actually delivered by the user 



      $customer_profile = CustomerProfileModel::find(  $order_info->book_by  ) ;   
      if( !isset($customer_profile))
      {
        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching customer found for the order. Please retry refreshing.'  ];
        return json_encode($data);
      }
       
 
      
    $review = CustomerReviewModel::where('customer_id', $customer_profile->id )
    ->where('member_id',  $user_info->member_id   ) 
    ->where('order_no',  $request->orderNo   )
    ->first();


    if(!isset($review))
    {
      $review  = new CustomerReviewModel;
      $review->member_id  =   $user_info->member_id  ;
      $review->order_no  =  $request->orderNo  ;
      $review->customer_id  = $order_info->book_by ; 
    }
 

    $review->title  = $request->reviewTitle ;
    $review->body   =  $request->reviewBody ; 
    $review->rating  =  $request->star ;
    $review->added_on = date('Y-m-d H:i:s');    
    $save=$review->save();
    
    if($save)
    { 
      $reviewRates = CustomerReviewModel::where('customer_id', $order_info->book_by )->get();
      $rate = 0;

      foreach ($reviewRates as $keyitem)
      {
        $rate += $keyitem->rating; 
      }
      
      $count = $reviewRates->count();
      if($count > 1)
      {
        $rate =  $rate / $count ; 
      } 
  

      $customer_profile->rating =$rate;
      $customer_profile->save(); 
       
      $status_code= "success";
      $err_msg = "Customer review posted successfully!";
      $err_code = 7023;

    }
    else 
    {
      $status_code= "failure";
      $err_msg = "Customer review and rating could not be saved. Please consult admin!";
      $err_code = 7024;  
    }
    
    $data = array("message" => $status_code  ,  'status_code' => $err_code, "detailed_msg" => $err_msg) ; 
    return json_encode($data); 
    }


 
  public function postDeliveryReview(Request $request)
  {
      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
        return json_encode($data);
      } 

      if( $user_info->category  != 0  )
      {
          $data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'This feature is available for customer only.'  ];
          return json_encode($data);
      }  
 

      if( $request->orderNo  == "" ||  $request->reviewTitle== "" || $request->reviewBody == "" || $request->star == ""  )
      {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' =>  "Field missing!"  ];
         return json_encode($data);
      }

 


      $order_info   = ServiceBooking::where("id", $request->orderNo )->where("book_by", $user_info->member_id)->first() ; 

      if( !isset($order_info) )
      {
        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'You selected an order not placed by you. You can review delivery for your orders only.'  ];
        return json_encode($data);
      }

      $delivery = DeliveryOrderModel::where("order_no",$order_info->orderNo)
      ->first();

      //check if order is actually delivered by this agent


      $agent_profile = CustomerProfileModel::find($delivery->member_id);   
      if( !isset($agent_profile))
      {
        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching agent found for the order. Please retry refreshing.'  ];
        return json_encode($data);
      } 
      
    $review = CustomerReviewModel::where('customer_id', $agent_profile->id )
    ->where('member_id',  $order_info->bookBy  ) 
    ->where('order_no',  $request->orderNo   )
    ->first();
 

    if(!isset($review))
    {
        $status_code= "failure";
        $err_msg = "You already have reviewed the agent. Review edit is not allowed!";
        $err_code = 7030; 
    }
   
    
    $review  = new CustomerReviewModel;
    $review->member_id  =  $order_info->bookBy ;
    $review->order_no  =  $request->orderNo  ;
    $review->customer_id  = $agent_profile->id ; 
    $review->title  = $request->reviewTitle ;
    $review->body   =  $request->reviewBody ; 
    $review->rating  =  $request->star ;
    $review->added_on = date('Y-m-d H:i:s');    
    $save=$review->save();

    if($save)
    { 
      $reviewRates = CustomerReviewModel::where('customer_id', $agent_profile->id )->get();
      $rate = 0; 

      foreach ($reviewRates as $keyitem)
      {
        $rate += $keyitem->rating; 
      }
      
      $count = $reviewRates->count();
      if($count > 1)
      {
        $rate =  $rate / $count ; 
      } 
   

      $agent_profile->rating =$rate;
      $agent_profile->save(); 
       
      $status_code= "success";
      $err_msg = "Agent review posted successfully!";
      $err_code = 7027;

 

    }
    else 
    {
      $status_code= "failure";
      $err_msg = "Agent review and rating could not be saved. Please consult admin!";
      $err_code = 7028;  
    }
    
    $data = array("message" => $status_code  ,  'status_code' => $err_code, "detailed_msg" => $err_msg) ; 
    return json_encode($data); 
    }


 

    //Get all review for customer orders
    public function getAllReviewsForCustomerOrders(Request $request)
    {
      if ($request->agentId == "")
      {
        $data= [ "message" => "failure", "status_code" =>  999 , 'detailed_msg ' => 'Missing data to perform action.' ];
        return json_encode($data); 
      }


      $reviewDetails = DB::table("ba_customer_reviews")
      ->where("customer_id",$request->agentId)
      ->join("ba_profile", 'ba_profile.id', "=", "ba_customer_reviews.member_id")

      ->select("ba_customer_reviews.id as reviewId",
        "ba_customer_reviews.customer_id as customerId",
         "ba_customer_reviews.member_id as agentId",
         "ba_customer_reviews.order_no as orderNo",
         "ba_customer_reviews.title as reviewTitle",
         "ba_customer_reviews.body as reviewMessage",
         "ba_customer_reviews.rating as reviewRating", 
         "ba_customer_reviews.added_on as reviewDate",
         "ba_profile.fullname as reviewAuthorName",
        "ba_profile.phone as reviewAuthorPhone"   
        )
            ->get();

 
  

         if (!isset($reviewDetails)) 
         {
          $data= ["message" => "failure", "status_code" =>  5020 ,  'detailed_msg' => 'No review record found.' ];
          }
          else
          {
             $data= [
              "message" => "success", 
              "status_code" =>  5021 ,  
              'detailed_msg' => 'Review details are fetched.' ,
             "totalRating" => 5 ,
             "ratingAverage" => 5 ,
             "ratingFiveCount" => 1,
             "ratingFourCount" =>  0 ,
             "ratingThreeCount" =>  0 ,
             "ratingTwoCount" => 0 ,
             "ratingOneCount" => 0 , 
             'results' =>  $reviewDetails ];
          } 

 

        return json_encode( $data);
       
    }  

}
