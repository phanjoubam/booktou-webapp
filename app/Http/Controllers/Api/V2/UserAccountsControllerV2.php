<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

 
use App\CustomerProfileModel;


use App\Traits\Utilities;
use App\User;


class UserAccountsControllerV2 extends Controller
{
	use Utilities;

     public function linkPushNotificationToken(Request $request )
    {
      
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }



      if(  $request->token == "" )
      {
        $data= ["message" => "failure",  "status_code" =>  901 , 'detailed_msg' => 'Push notification token is missing.'  ];
        return json_encode( $data);
      }

 

      $user = User::find(  $request->userId );
      $user->push_token = $request->token ;
      $save =  $user->save();

      if($save)
      {
        $data= ["message" => "success",   "status_code" =>  5061 , 'detailed_msg' => 'Push notification token updated!'  ];
      }
      else
      {
        $data= ["message" => "failure",   "status_code" =>  5062 , 'detailed_msg' => 'Push notification token could not be saved.'  ];
      }
      return json_encode( $data);

    }
 



    //Generate OTP 
    public function otpGeneration(Request $request)
   {
      if( $request->phone == "" || $request->category  == "" )
      {
        $err_msg = "Field missing!";
        $err_code = 9000;
              
        $data = array(
          "message" => "failure" , 'status_code' => $err_code ,
          'detailed_msg' => $err_msg) ;
        
        return json_encode( $data ); 
      }

      $rand = mt_rand(111111, 999999);

      $user_info = User::where("phone", $request->phone )->where("category", $request->category )->first();

      if(!isset($user_info))
      {
          $data= ["message" => "failure",  "status_code" =>  997 , 'detailed_msg' => 'User is not registered with us.'  ];
          return json_encode( $data); 
      }

      //generate a confirmation OTP 
      $expire_date = strtotime( date('Y-m-d H:i:s') ) +(60*5) ;
      $user_info->otp = $rand ;
      $user_info->otp_expires =date("Y-m-d H:i:s", $expire_date);
      $user_info->save();



      $err_status ="success";
      $err_code =  3503;
      $err_msg =  "Your OTP sent as SMS to your registered mobile.";

        $phone = $user_info->phone ;

        $otp_msg = "Your  OTP is " . $rand.  urlencode(". Please DO NOT SHARE this OTP with anyone. bookTou never calls you asking for OTP." ) ; 
        //staging - u8IKLyQc6v6 Prod: 8KSDYMB+AxA

        $this->sendSmsAlert(  array($phone)   ,  $otp_msg); 

        $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg) ;
        return json_encode($data);  

    }
 

    //verify agent signup otp verification
    //It verifies phone and OTP. If there is a match, it will login

  public function verifySignupOTP(Request $request)
  {
    
     if(    $request->phone == "" || $request->otp == ""     )
     {
     $data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
     }
     
    $user = User::where("phone", $request->phone )->where("otp",  $request->otp )->where("category", 100)->first();
      
    if(!isset($user))
    {
      $data= ["message" => "failure",  "status_code" =>  997 ,
      'detailed_msg' => 'User not found.'  ];
      return json_encode( $data); 
    }
 

    if(date('Y-m-d H:i:s', strtotime($user->otp_expires)) >= date('Y-m-d H:i:s'))
    {
        $data['profilePicture'] = URL::to('/public/assets/image/no-image.jpg' );
        $data['coverPhoto'] =URL::to('/public/assets/image/no-image.jpg' );
        $fullname = ""; 
        $data['businessAddress'] = '';
        $data['businessCategory'] = '' ;
        $data['businessName'] = "";
        $data['bin'] = 0; 
        $data['businessSetup'] = ''; 
        $data['customerSetup'] = ""; 
        $data['agentSetup'] = "no";
        $data['name'] = $fullname ; 
        $data['category'] = $user->category ;
        $data['userId'] = $user->id ;
        $data['name'] = $user->phone  ;  
        $data['sessionToken'] = md5(time());
      
        $data["message"] = "success"; 
        $data["status_code"] = 1059;
        $data["detailed_msg"] =  'Login successful!'  ; 

    }
    else{
          $message = "failure";
          $err_code = 1060;
          $err_msg =  'Otp Expired!'  ; 
          
          
          $data["message"] = $message;
        $data["status_code"] = $err_code ;
        $data["detailed_msg"] =  $err_msg   ; 
    }
    
  
  return json_encode( $data);
  
  }



   //verify agent signup otp verification for customer and business
    //It verifies phone and OTP. If there is a match, it will login

  public function verifyUserSignupOTP(Request $request)
  {
    
     if(    $request->phone == "" || $request->otp == ""  || $request->category == ""   )
     {
     $data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
     }
     
    $user = User::where("phone", $request->phone )->where("category", $request->category )->first();
      
    if(!isset($user))
    {
      $data= ["message" => "failure",  "status_code" =>  997 ,
      'detailed_msg' => 'User not found.'  ];
      return json_encode( $data); 
    }


    if( $user->status == 1)
    {
        $data= ["message" => "failure",  "status_code" =>  7042,  'detailed_msg' => 'User is already verified user.'  ];
        return json_encode( $data); 
    }

 

    if( $user->otp == $request->otp)
    {
        $user->status = 1;
        $user->otp = -1;
        $user->save(); 
 
        $data["message"] = "success"; 
        $data["status_code"] = 7041;
        $data["detailed_msg"] =  'OTP verification successful!'  ; 

    }
    else{
          $message = "failure";
          $err_code = 7043;
          $err_msg =  'Wrong OTP specified. Verification failed.'  ; 
          
          
          $data["message"] = $message;
        $data["status_code"] = $err_code ;
        $data["detailed_msg"] =  $err_msg   ; 
    }
    
  
  return json_encode( $data);
  
  }


   //Password Reset for Forgot Password
   public function updateResetPassword(Request $request)
   {
      if( $request->phone == "" || $request->category  == "" || $request->confirmOtp  == "" )
      {
        $err_msg = "Field missing!";
        $err_code = 900;
              
        $data = array(
          "message" => "failure" , 'status_code' => $err_code ,
          'detailed_msg' => $err_msg) ;
        
        return json_encode( $data ); 
      }

 

      $user_info = User::where("phone", $request->phone )
      ->where("category", $request->category ) 
      ->first();
 

      if(!isset($user_info))
      {
          $data= ["message" => "failure",  "status_code" =>  997 , 'detailed_msg' => 'User is not registered with us.'  ];
          return json_encode( $data); 
      }

 


       if ($request->confirmOtp  != $user_info->otp  ) 
       {
          $data= ["message" => "failure",  "status_code" =>  995 , 'detailed_msg' => 'OTP mismatched.'  ];
          return json_encode( $data); 
       } 

 

      $password = $request->newPassword;
      $hashed = Hash::make($password); 
      $user_info->phone = $user_info->phone; 
      $user_info->password = $hashed;
      $save = $user_info->save();

 

      if ($save) 
      {
        $data = array( "message" => "success"  , "status_code" => 998, 'detailed_msg' => "Your password successfully reset.") ;
      }
       else
       {
          $data = array( "message" => "failure"  , "status_code" => 999, 'detailed_msg' => "Your password reset failed.") ;
       }

 

        return json_encode($data);   

    }



    // Password change
    public function updatePassword(Request $request )
   {

      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
        return json_encode($data);
      }

      if(  $request->newPassword == ""  ||  $request->currentPassword == "" )
      {
          $data= [ 
            "message" => "failure", 
            "status_code" =>  999 , 
            'detailed_msg ' => 'Missing data to perform action.' 
          ];
            return json_encode($data);
      }


      $userInfo = User::find( $request->userId );

      if( !isset($userInfo))
      {
          $data = [ "message" => "failure",  'status_code' => 900, 'detailed_msg' => 'No matching user found!'  ];
          return json_encode($data);
      }

        
 

        if(Hash::check( $request->currentPassword , $userInfo->password )) 
        {

          $userInfo->password =  Hash::make( $request->newPassword ); 
          $userInfo->save(); 
          $data = [ "message" => "success",  'status_code' => 902, 'detailed_msg' => 'Password updated successfully!'  ];
         
        }
        else
        {
           $data = [ "message" => "failure",  'status_code' => 993, 'detailed_msg' =>  'You need to speficy correct existing password!' ]; 
        }
          
        
       return json_encode($data);
    
  }



    // Password change
    public function updateFireBaseToken(Request $request )
   {

      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
        return json_encode($data);
      }

      if(  $request->firebaseToken == ""    )
      {
          $data= [ 
            "message" => "failure", 
            "status_code" =>  999 , 
            'detailed_msg ' => 'Notification token missing.' 
          ];
            return json_encode($data);
      }


      $userInfo = User::find( $request->userId );

      if( !isset($userInfo))
      {
          $data = [ "message" => "failure",  'status_code' => 900, 'detailed_msg' => 'No matching user found!'  ];
          return json_encode($data);
      } 

      $userInfo->firebase_token = $request->firebaseToken; 
      $userInfo->save(); 
      $data = [ "message" => "success",  'status_code' => 902, 'detailed_msg' => 'Cloud messaging token updated!'  ];   
       return json_encode($data);
    
  }


}
