<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmaOrderSms;


 
use App\ShoppingOrderItemsModel;  
use App\AgentServiceAreaModel; 
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\GlobalTokenModel;
use App\ProductUnitModel;
use App\Business;


use App\Traits\Utilities;
use App\User;




class SystemControllerV2  extends Controller
{
	use Utilities;
  

   protected function fetchSliders(Request $request)
   {

      $bizSlides  =  DB::table("ba_slides") 
      ->join("ba_business", "ba_business.id", "=", "ba_slides.bin")
      ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" )    
      ->select("ba_slides.id", "ba_slides.path as url", "ba_slides.caption", 
       "ba_slides.bin", "ba_business.name as businessName",  "ba_business.category as bizCategory",
       "ba_business_category.display_type as uiLayoutMode", "sequence", DB::Raw("'false' as premiumBiz") )
      ->orderBy("sequence")
      ->get(); 


      $bin  = $finalResult = array();   

      foreach($bizSlides  as $item)
      {
        $bin[] = $item->bin; 
        $source = public_path() .   $item->url;
        
        if(file_exists(  $source )  )
        {
            $item->url = URL::to("/public") . "/" .  $item->url  ;
        }
        else 
        {
            $item->url = URL::to("/public/assets/image/no-image.jpg");
        }

        $finalResult[] = $item;

      } 

      $bin[] =0; 
      $premiumBizs  =  DB::table("ba_premium_business")      
      ->select("bin" )
      ->whereIn("bin", $bin)
      ->get();

      foreach($bizSlides  as $item)
      {
        foreach($bizSlides  as $biz)
        {
          if($item->bin == $biz->bin  )
          {

            $item->premiumBiz = 'true';
            break;
          }
        } 
      }

      $slides = DB::table('ba_slides')
      ->whereNull('bin')
      ->select("ba_slides.id", "ba_slides.path as url", "ba_slides.caption", DB::Raw("'0' as bin") ,  DB::Raw("'na' as businessName"),  DB::Raw("'na' as bizCategory"), "sequence", 
      DB::Raw("'false' as premiumBiz") )
      ->get();

        foreach($slides  as $item)
        {
           
          $source = public_path() .   $item->url;
          if(file_exists(  $source )  )
          {
             $item->url = URL::to("/public") . "/" .  $item->url  ;
          }
          else 
          {
             $item->url = URL::to("/public/assets/image/no-image.jpg");
          }

           $finalResult[] = $item;
         
         }


         if(count($finalResult) > 0 )
         {
            $data = array("message" => "success", "status_code" =>  9001 ,  'detailed_msg' => 'Slider images fetched' ,   'results' =>  $finalResult ); 
         }
         else 
         {
          $data = array("message" => "failure", "status_code" =>  9002 ,  'detailed_msg' => 'Slider images fetched' ,   'results' =>  array() ); 
         }
        
 
        return json_encode( $data);
    } 



    public function checkVersion(Request $request)
    {

      $versions  =  DB::table("ba_version")  
      ->select("client_app", "agent_app",  "admin_app" )
      ->where("is_current", "1")
      ->first(); 

      $data = ["clientVersion" =>  0, "agentVersion" =>   0, "adminVersion" =>   0];

      if( isset($versions ) )
      {
        $data = ["clientVersion" =>  $versions->client_app*1 , "agentVersion" => $versions->agent_app*1, "adminVersion" => $versions->admin_app *1 ];
      }
      

      return json_encode($data); 
     
    } 



    public function orderPlacementSlots(Request $request)
    {

      $slot  =  DB::table("ba_global_settings")   
      ->get(); 
      $data = ["startHour" =>  0, "endingHour" =>  0];

        
      foreach($slot as $item)
      {
          switch( $item->config_key )
          {
            case "start_hour":
                $data["startHour"] =  date( 'h:i a' ,strtotime( $item->config_value )) ;
                break;
            case "end_hour":
                $data["endingHour"] = date( 'h:i a' ,strtotime( $item->config_value )) ;
                break;
            case "min_amount":
                $data["minimumOrderAmount"] =  $item->config_value  ;
                break;
            case "delivery_charge":
                $data["deliveryCharge"] =  $item->config_value  ;
                break;
            case "upi_primary":
                $data["primaryUPI"] =  $item->config_value  ;
                break;
            case "payee_name":
                $data["payeeName"] =  $item->config_value  ;
                break;    

          }
      }
 
      $data['businessName'] = "NA" ;
      $data['businessStatus'] = "close";
      $data['locality'] = "NA" ;
      $data['landmark'] = "NA" ;
      $data['city'] = "NA" ;
      $data['state'] = "NA" ;
      $data['pin'] = "NA" ;
      $data['bannerImgUrl'] = URL::to('/public/assets/image/no-image.jpg')  ; 
       $data['isOpen'] =  "NA";
      //checking business status 
      if( $request->bin  != "" )
      {
          $business = Business::find($request->bin);  
          if( isset($business))
          {
            $status = $business->is_open;  
            $name = $business->name;   
            $address  = $business->locality . ", ". $request->landmark . " ". $request->city . " ". $request->state . " - ". $request->pin  ;
            $bannerImgUrl = $business->banner;   
            if( $bannerImgUrl !=null )
            {
              $bannerImgUrl= URL::to('/public'. $bannerImgUrl );  
            } 
            $data['businessName'] = $name ;
            $data['businessStatus'] = $status;
            $data['locality'] = ( $business->locality != "" ? $business->locality : "not provided");
            $data['landmark'] = ( $business->landmark!= "" ? $business->landmark : "not provided");
            $data['city'] = ( $business->city!= "" ? $business->city : "not provided");
            $data['state'] = ( $business->state != "" ? $business->state : "not provided"); 
            $data['pin'] = ( $business->pin != "" ? $business->pin : "not provided");

            $data['isOpen'] =  ( $business->is_open  != "" ? $business->is_open : "NA");
            $data['bannerImgUrl'] = $bannerImgUrl ; 
          }
      }



      //product stock level
       $result = array(); 
      if(isset($request->productCodes)): 

      $productCodes = json_decode( $request->productCodes, true  ); 
     
      if(count( $productCodes[0] ) > 0)
      {


        $prCodes = array();
        foreach( $productCodes[0] as $key => $value)
        {
          $prCodes[] = $key;
        }
        $prCodes[]= "0" ;

        $products  = DB::table("ba_products")  
        ->whereIn("pr_code",  $prCodes )
        ->select( "pr_code", "stock_inhand")
        ->get(); 
   

        foreach( $products as $item)
        {  
          foreach($productCodes  as $key => $value)
          {
            if($item->pr_code == $key )
            {
                $result[] = array( "prodCode" => $item->pr_code, "stock" =>  $item->stock_inhand );
                break;
            }
          }       
        }
      } 

      endif;


      $data['results'] =  $result;
      return json_encode($data);  
    } 


 
    public function sendNewOrderConfirmationMail(Request $request)
    {

        SendEmaOrderSms::dispatch( );
    }
  

 

    public function getAllCoupons(Request $request)
    {

      $today = date('Y-m-d');
      $user_info =  json_decode($this->getUserInfo($request->userId));  
      if(  $user_info->user_id == "na" )
      {
          //here we fetch only coupons valid for the customer. Right now showing all coupons
          $coupons  =  DB::table("ba_coupons")  
          ->select("code", "total_discount as discountPc", "delivery_discount as deliveryDiscount" , "description", "image_url as imageUrl",
            "validity_text as validUpto"
           ) 
          ->whereRaw("date(ends_on) >= '$today'")
          ->get(); 
      }
      else 
      {

          $coupons  =  DB::table("ba_coupons")  
          ->select("code", "total_discount as discountPc", "delivery_discount as deliveryDiscount" , "description", "image_url as imageUrl",
          "validity_text as validUpto" ) 
          ->whereRaw("date(ends_on) >= '$today'")
          ->get(); 

      }


   
      
      $data= [
        "message" => "success", "status_code" =>  7101 ,  'detailed_msg' => 'Voucher codes fetched.', 
        'results' =>  $coupons
        ];
 
      return json_encode($data); 
     
    } 


  


    public function saveToken(Request $request)
    {
        
    
        if($request->token == ""  )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
            return json_encode($data);
        }

 

        if( $request->tokenIndex != ""  )
        {
        
          $token = GlobalTokenModel::find(  $request->tokenIndex ); 
        }
        else 
        {
          $token = new GlobalTokenModel; 
        }

        $token->token=  $request->token; 
        $save=$token->save();

        $tokenId=0;
        if($save)
        {
          $tokenId = $token->id;
        }


        $data= ["message" => "success", "status_code" =>  7105 , 'detailed_msg' => 'Token updated.', 'tokenIndex'=> $tokenId ];
        return json_encode($data);
     
    } 



}
