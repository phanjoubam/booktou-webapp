<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

use App\Jobs\SendOrderConfirmationSms;
use App\Jobs\SendOrderAssignToAgentSms;



 
use App\ShoppingOrderItemsModel;  
use App\AgentServiceAreaModel; 
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\ProductModel; 

use App\BusinessService;

use App\Traits\Utilities;
use App\User;




class SearchAndIndexControllerV2 extends Controller
{
	use Utilities;

  public function getAllTagList(Request $request)
  {
    if($request->keyword ==""   )
    {
      $data=[ "message"=> "failure", "status_code"=> 9000 , "detailed_msg"=> "Field missing!" ];
        return json_encode($data);
    } 

    $keyword = $request->keyword;  

    $biz_count = DB::table('ba_business') 
            ->where("ba_business.name", "like", "%$keyword%")
            ->orWhereIn("id", function( $query ) use ($keyword) {
                $query->select('bin') 
                ->from( "ba_products" )
                ->where("ba_products.pr_name", "like", "%$keyword%" )
                ->orWhere("ba_products.description", "like", "%$keyword%" )  
                ->distinct()
                ->get();
            })
            ->orWhere("ba_business.tag_line", "like", "%$keyword%" )   
            ->count( );

    $product_count = DB::table('ba_products') 
                ->where("pr_name", "like", "%$keyword%")
                ->orWhere("description", "like", "%$keyword%" )   
                ->count( );

    $service_count = DB::table('ba_business_services') 
                ->where("srv_name", "like", "%$keyword%")
                ->orWhere("srv_details", "like", "%$keyword%" )   
                ->count( );

    $product_categories_count = DB::table('ba_product_category') 
                ->where("category_name", "like", "%$keyword%")
                ->orWhere("category_desc", "like", "%$keyword%" )   
                ->count( );


     $result = array();
      
     if( $product_count > 0)
     {
      $result[] = [ 'meta' => 'product',  'count' =>$product_count ,   'description' => $product_count . ' matching products found' ];
     }

     if( $biz_count > 0)
     {
      $result[] =['meta' => 'business' , 'count' =>$biz_count ,  'description'  => $biz_count . ' matching business found' ]; 
     }
     if( $service_count > 0)
     {
      $result[] = ['meta' => 'service','count' =>$service_count ,   'description' => $service_count . ' matching services found']; 
     }
     if( $product_categories_count > 0)
     {
      $result[] = ['meta' => 'product category' ,'count' =>$product_categories_count ,    'description'   => $product_categories_count . ' matching product categories found' ]; 
     }    
 
        
    $data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business Tag Line', 'results' =>$result];         
      return json_encode($data); 
    } 

 

  public function searchProducts(Request $request)
  {
    $keyword = $request->keyword;


    $products  = DB::table("ba_products")
    ->join("ba_business", "ba_business.id", "=", "ba_products.bin" )  

    ->where("pr_name", "like",  "%$keyword%" )
    ->orWhere("description", "like",  "%$keyword%"  )
    ->select( "ba_products.id as productId", "ba_products.bin", "ba_business.name",  
       "ba_business.category as businessCategory",
      "ba_products.pr_code as productCode", "ba_products.pr_name as productName",  
      "ba_products.description",    "ba_products.stock_inhand as stock" ,  "ba_products.gst_code as GST" ,
       "ba_products.pack_type as packType" , "ba_products.max_order as maxOrderAllowed", 
       "ba_products.unit_price as unitPrice",  "ba_products.actual_price as actualPrice",
      "ba_products.unit_value as unitValue" , "ba_products.unit_name as unitName", 
      "ba_products.discount", "ba_products.discountPc", "ba_products.category as productCategory", 
      "ba_products.food_type as foodType", 
      "ba_products.photos as images" ,
      "ba_products.free_text as freeText", 
      "ba_products.addon_available as addon"
        )
      ->paginate(10);
 
   


      foreach( $products as $item)
      {
        $all_photos = array(); 
        $files =  explode(",",  $item->images ); 
        if(count($files) > 0 )
        {

          $files=array_filter($files);
          $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .  "/";

          foreach($files as $file )
          {

            $source = base_path() .  $folder_path .   $file; 
            if(file_exists( $source  ))
            {

              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
              if( file_exists( $destination ) ) //smaller file exists
              {
                $all_photos[]  =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name);
              }
              else 
              {
                if(filesize(  $source ) > 307200) // 300 Kb
                {

                  $this->chunkImage($source, $folder_path , $destination ); //chunking image size 
                  $all_photos[]  =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name); 

                }
                else 
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
              } 
            }
            else
            {
              $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
            }
        }
      }
      

        $item->images = $all_photos; 
        if($item->GST == "" || $item->GST == null )
        {
          $item->GST = 0;
        }  
    }

    $final_result['last_page'] = $products->lastPage( ); 
    $final_result['data'] = $products->items();  

    $data= ["message" => "success", "status_code" =>  1015 ,  'detailed_msg' => 'Matching products found.', 'results' => $final_result];         
    
    return json_encode($data); 

 
    }


 

  public function searchServices(Request $request)
  {
    $keyword = $request->keyword;

    $products = BusinessService::select( "srv_code as serviceCode", "srv_name as serviceName", 
    "srv_details as description",  "gst_code as GST", 
    "photos", "duration_hr as durationHr", "duration_min as durationMin", "pricing" )
    ->where("srv_name", "like", "%$keyword%")
    ->orWhere("srv_details", "like", "%$keyword%")
    ->paginate(10);
    

    foreach($products as $item)
    {
      if($item->GST == "" || $item->GST == null )
      {
        $item->GST = 0 ;

      }

      $item->pricing = $item->unipricingtPrice * 1;



      $item->photo = array( URL::to('/public/assets/image/no-image.jpg'), URL::to('/public/assets/image/no-image.jpg') )  ;   

    }

    $final_result['last_page'] = $products->lastPage( ); 
    $final_result['data'] = $products->items();  

    $data= ["message" => "success", "status_code" =>  1015 ,  'detailed_msg' => 'Matching services found.', 'results' => $final_result];         
    
    return json_encode($data); 

 
    } 


    public function searchBusiness( Request $request)
    {
        $keyword = $request->keyword;

         $businesses = DB::table('ba_business') 
         ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")
            ->where("ba_business.name", "like", "%$keyword%")
            ->orWhereIn("ba_business.id", function( $query ) use ($keyword) {
                $query->select('bin') 
                ->from( "ba_products" )
                ->where("ba_products.pr_name", "like", "%$keyword%" )
                ->orWhere("ba_products.description", "like", "%$keyword%" )  
                ->distinct()
                ->get();
            })
            ->orWhere("ba_business.tag_line", "like", "%$keyword%" )   
            ->where("latitude", "<>", 0)
            ->where("longitude", "<>", 0) 
            ->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 
            'ba_business.shop_number as shopNumber',  
            'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone', 
            "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin", 
            'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' , 
            "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
            DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"), 
            "ba_business.category as businessCategory",  DB::Raw("'0' distance") ,
            "ba_business_category.main_type as mainBusinessType",  
            "is_open as isOpen" ) 
            ->paginate(10);

   
        $final_result['last_page'] = $businesses->lastPage( ); 
        $final_result['data'] = $businesses->items();  

 
          
      $data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Matching businesses fetched', 'results' => $final_result];         
        return json_encode($data);
    }


}
