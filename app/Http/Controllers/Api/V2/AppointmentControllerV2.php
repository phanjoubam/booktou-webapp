<?php

namespace App\Http\Controllers\Api\V2; 
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

 
use App\Business; 
use App\ServiceHours;
use App\Traits\Utilities;
use App\User;
use App\BusinessOwner;
use App\ServiceDuration;
use App\BusinessService;
use App\ServiceBookingDetails;
use App\RealtimeQueue;


  
class AppointmentControllerV2  extends Controller
{
	use Utilities;
	

	public function prepareQueue(Request $request)
	{

		$user_info =  json_decode($this->getUserInfo($request->userId));
	   
		if(  $user_info->user_id == "na" )
		{
			$data= ["message" => "failure", "status_code" =>  901 ,
			'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
			return json_encode($data);
		}
	     
       
	   $business = Business::find($request->bin );  
		if( !isset($business))
		{
		
		$data= ["message" => "failure",   "status_code" =>  999 ,
		'detailed_msg' => 'Business information not found!'  ];
		return json_encode( $data);
		
		}
	
	
	if(    $request->bookingDate == ""  )
   {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
   }
 
	$bookDate = date('Y-m-d', strtotime($request->bookingDate)); 
   

	$all_bookings = DB::table("ba_service_booking")
	->whereRaw("date(book_date) = '" .   $bookDate   .  "'")
	->select("id"  )
	->get(); 

	$ids  = $actual_queue_id = array();

	foreach($all_bookings as $item)
	{
		$ids[] =  $item->id; 
	}


	$actual_q = DB::table('ba_service_booking_details')
	->whereRaw("book_id in ( " .  implode(",", $ids) .   "   ) " )
	->whereRaw("status not in ('completed', 'cancel_by_client', 'cancel_by_owner' )")
	->get(); 


	foreach( $actual_q as $item)
	{
		if($item->added_to_queue == "no")
		{  
		$realtime = new RealtimeQueue;
		$realtime->actual_queue_id = $item->id; 
		$realtime->book_id = $item->book_id; 
		$realtime->service_code =  $item->service_code; 
		$realtime->service_time =  $item->service_time; 
		$realtime->status = $item->status; 
		$save = $realtime->save();

		if($save)
		{
			$actual_queue_id[] =  $item->id; 
		} 
	}
	}

	if(count($actual_queue_id) > 0 )
	{ 
		DB::table('ba_service_booking_details')
		->whereRaw(" id in ( " .  implode(",", $actual_queue_id) .   "   ) " )
		->update(['added_to_queue' => 'yes' ]);
	}

 	$data= ["message" => "success", "status_code" =>  2021 ,  'detailed_msg' => "Day's service queue prepared."  ]; 
    return json_encode( $data); 

}

   public function getDayBooking(Request $request)
   {
	   
	  $user_info =  json_decode($this->getUserInfo($request->userId));
	   
	  if(  $user_info->user_id == "na" )
	  {
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
		return json_encode($data);
	  }
	 
	 
	  $business = Business::find($request->bin);
	  if( !isset($business))
	  {
		
		$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		'detailed_msg' => 'Business information not found!'  ];
		return json_encode( $data);
		
	  }
	
	
	 if( $request->bookingDate == ""  )
     {
		$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		return json_encode($data);
     }
 
	$bookDate = date('Y-m-d', strtotime($request->bookingDate)); 
	//show booking details 
	$day_bookings= DB::table("ba_realtime_queue")
	->join("ba_service_booking", "ba_service_booking.id", "=", "ba_realtime_queue.actual_book_id")
	->whereRaw("date(book_date) = '" . $bookDate . "'")
	->where("ba_service_booking.bin", $user_info->bin)

 

	->select("ba_service_booking.id as bookingNo", "book_by as bookingUserId", 
	DB::Raw("'na' bookingUserName"),
	"profile_photo as userProfilePhoto" ,  
	"ba_service_booking.otp as confirmationOTP", "book_status as bookingStatus", 
   	DB::Raw( "'na' appointmentTime"),
	  DB::Raw("'[]' bookedServices") )
	->get();
	 
 
   foreach($day_bookings as $item)
   {
	   $allBookings =  DB::table("ba_service_booking_details") 
		->join("ba_business_services", "ba_business_services.srv_code", "=", "ba_service_booking_details.service_code")
		->where("book_id", $item->bookingNo) 
		->select("service_time as serviceTime", "srv_name as serviceName", "ba_service_booking_details.status"  )
		->orderBy("service_time")
		->get();
		foreach ($allBookings as $itemTime) {
			$itemTime->serviceTime = date('h:i a',strtotime($itemTime->serviceTime));
		}
		
		$item->userProfilePhoto =url('/public/assets/image/member/').   $item->userProfilePhoto ;
		$item->bookedServices =$allBookings;
		$item->appointmentTime = isset( $allBookings ) ? date('h:i a',strtotime($allBookings[0]->serviceTime))  : 'na'  ; 

		$userName = User::where('id',$item->bookingUserId)->get();
				
			foreach ($userName as $name) 
			{
				if($name->mname!=null)
				{
					$item->bookingUserName=$name->fname ." ". $name->mname." ".$name->lname ;
				}
				else
				{
					$item->bookingUserName=$name->fname ." ".$name->lname ;
				}

			}
   }
    

    $data= ["message" => "success", "status_code" =>  1021 ,  'detailed_msg' => 'Day booking list fetched.' ,  
	  'results' => $day_bookings ]; 
    return json_encode( $data);
	
  }  
 
}
