<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

use App\Jobs\SendOrderConfirmationSms;
use App\Jobs\SendOrderAssignToAgentSms;



 
use App\ShoppingOrderItemsModel;  
use App\AgentServiceAreaModel; 
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\ProductModel; 

use App\Traits\Utilities;
use App\User;




class WebControllerV2 extends Controller
{
	use Utilities;

     protected function assignOrderToAgent(Request $request)
     {

        if(  $request->oid == "" || $request->aid == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


        $order_info = ServiceBooking::find( $request->oid );
 
        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }

        $customer_info  = CustomerProfileModel::find($order_info->book_by);
        if(   !isset( $customer_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching customer found.'  ];
         return json_encode($data);
        }
 

        $member_info  = CustomerProfileModel::find($request->aid);
        if(   !isset( $member_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching agent profile found.'  ];
         return json_encode($data);
        }

        $business_owner  = DB::table("ba_business")
        ->select("phone_pri as phone")
        ->where("id", $order_info->bin )
        ->first(); 


        if(   !isset( $business_owner)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
         return json_encode($data);
        }
 

        $delivery_order = DeliveryOrderModel::where("member_id", $request->aid)->where("order_no", $request->oid)->first();


        if( !isset($delivery_order))
        {
          $delivery_order = new DeliveryOrderModel; 
        } 
        $delivery_order->member_id = $request->aid;
        $delivery_order->order_no = $request->oid;
        $delivery_order->accepted_date = date('Y-m-d H:i:s');
        $save = $delivery_order->save(); 
 

        if($save)
        {

          $order_info->book_status = "delivery_scheduled";
          $order_info->save();  
          $orderStatus = new OrderStatusLogModel;
          $orderStatus->order_no = $order_info->id; 
          $orderStatus->order_status = "delivery_scheduled";
          $orderStatus->log_date = date('Y-m-d H:i:s');
          $orderStatus->save(); 

          $data= ["message" => "success", "status_code" =>  7001 ,  'detailed_msg' =>  "Order assigned to delivery agent." ];
        }
        else
        {
        	$data= ["message" => "failure", "status_code" =>  7002 ,  'detailed_msg' => 'Order assignment to agent failed.'];
        }

        //SendOrderAssignToAgentSms::dispatch( $order_info->id, $member_info->id );   
 
        return json_encode( $data); 
     }



 


      protected function viewAgentTasks(Request $request)
     {

        if(  $request->aid == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


        $order_info = ServiceBooking::find($request->oid);


        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }


        $member_info  = CustomerProfileModel::find($request->aid);
         if(   !isset( $member_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching agent profile found.'  ];
         return json_encode($data);
        }


        $delivery_order = DeliveryOrderModel::where("member_id", $request->aid)->where("order_no", $request->oid)->first();


        if( !isset($delivery_order))
        {
          $delivery_order = new DeliveryOrderModel; 
        } 
        $delivery_order->member_id = $request->aid;
        $delivery_order->order_no = $request->oid;
        $delivery_order->accepted_date = date('Y-m-d H:i:s');
        $save = $delivery_order->save();


        if($save)
        {

          $order_info->book_status = "delivery_scheduled";
          $order_info->save(); 


          $orderStatus = new OrderStatusLogModel;
          $orderStatus->order_no = $order_info->id; 
          $orderStatus->order_status = "delivery_scheduled";
          $orderStatus->log_date = date('Y-m-d H:i:s');
          $orderStatus->save();



          $data= ["message" => "success", "status_code" =>  7001 ,  'detailed_msg' =>  "Order assigned to delivery agent." ];
        }
        else
        {
            $data= ["message" => "failure", "status_code" =>  7002 ,  'detailed_msg' => 'Order assignment to agent failed.'];
        }
        return json_encode( $data); 
     }



     //List down all order for an agent
    public function getOrderListsForAgent(Request $request)
    {
         if(  $request->aid == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


        $deliveryInfo = DB::table("ba_delivery_order")
        ->where("member_id",  $request->aid )
        ->whereRaw(" date(accepted_date)   = '".  date('Y-m-d')  .  "'"  )
        ->get(); 
 

        if( !isset($deliveryInfo))
        {
          $data= ["message" => "failure",   "status_code" =>  999 , 
          'detailed_msg' => 'Agent has no assigned task!'  ];
          return json_encode( $data); 
        }


        $orderNo = array();
        foreach ($deliveryInfo as $item) 
        {
          $orderNo[] = $item->order_no;
        }


        $orderDetails = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
        ->whereIn("ba_service_booking.id",$orderNo)
        ->select("ba_service_booking.id as orderId",
            "ba_service_booking.service_date as bookDate",
              "ba_service_booking.book_status as orderStatus",
              "ba_service_booking.address as deliveryAddress",
              "ba_service_booking.landmark as deliveryLandmark",
              "ba_service_booking.city as deliveryCity",
              "ba_service_booking.state as deliveryState",
              "ba_service_booking.pin_code as deliveryPin",
              "ba_service_booking.delivery_charge as deliveryCharge",  
              "ba_profile.fullname as customerName",  
              "ba_profile.phone as customerPhone",
              "ba_business.name as storeName",
              "ba_business.locality as pickUpLocality",
              "ba_business.landmark as pickUpLandmark",
              "ba_business.city as pickUpCity",
              "ba_business.state as pickUpState",
              "ba_business.pin as pickUpPin")
        ->get();


        $data= ["message" => "success", "status_code" =>  5007 ,  'detailed_msg' => 'Order lists successfully fetched.' ,'results' => $orderDetails];
        return json_encode( $data); 

    }


    protected function updateCustomerConfirmation(Request $request)
    {
        if(  $request->oid  == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No order is selected.'  ];
         return json_encode($data);
        } 

        $order_info = ServiceBooking::find($request->oid); 

        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }

        if( $order_info->is_confirmed  == $request->confirmation )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'Nothing to process.'  ];
            return json_encode($data);
        } 


        $browser_refresh = "no";
        if($order_info->book_status == "new" && $request->confirmation == "yes" )
        {
           DB::table('ba_order_status_log')
          ->where('order_no', $request->oid )
          ->increment('seq_no', 1);

          $orderStatus = new OrderStatusLogModel;
          $orderStatus->order_no =  $request->oid ; 
          $orderStatus->order_status = "confirmed";
          $orderStatus->log_date = date('Y-m-d H:i:s');
          $orderStatus->save();

          $order_info->book_status = "confirmed"; 
          $browser_refresh = "yes";
  
        }

        $order_info->is_confirmed  = $request->confirmation;
        $order_info->save();


        SendOrderConfirmationSms::dispatch( $request->oid  );

  
        $data= ["message" => "success", "status_code" =>  7005 ,  'detailed_msg' =>  "Order confirmation from customer updated." ,
        'browser_refresh' => $browser_refresh ]; 
        return json_encode( $data); 

    }




    protected function cancelCustomerOrder(Request $request)
    {
        if(  $request->oid  == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No order is selected.'  ];
         return json_encode($data);
        } 

        $order_info = ServiceBooking::find($request->oid); 

        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }


        DB::table('ba_order_status_log')
        ->where('order_no', $request->oid )
        ->increment('seq_no', 1);

        $orderStatus = new OrderStatusLogModel;
        $orderStatus->order_no =  $request->oid ; 
        $orderStatus->order_status = "canceled";
        $orderStatus->log_date = date('Y-m-d H:i:s');
        $orderStatus->remarks =$request->reason  ;
        $orderStatus->save(); 

        $order_info->is_confirmed  =  "no";
        $order_info->book_status = "canceled";  
        $order_info->save();
 
 
  
        $data= ["message" => "success", "status_code" =>  7007 ,  'detailed_msg' =>  "Order is cancelled."  ]; 
        return json_encode( $data); 

    }


    public function getProductDetails(Request $request)
    {
      // $bin = $request->session()->get("_bin_");

        $products = DB::table("ba_products")
       ->select('unit_price as price')
        ->where("id",$request->pid)
       // ->where("bin",$bin)
        ->first();

      //  $data= ["message" => "success", "status_code" =>  5029 ,  'detailed_msg' => 'Product details fetched successfully.' ,'results' => $products];
      //  return json_encode( $data); 

      return response()->json($products);


    }



    public function updateProductField(Request $request)
    {
       

        $product = ProductModel::where("pr_code", $request->key ) 
        ->first();


        if(  !isset( $product)  )
        {
          $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching product found.'  ];
          return json_encode($data);
        }


        switch($request->col)
        {
          case "prdesc":
            $product->description = $request->value;
          break;

        }

        $product->save();
        $data= ["message" => "success", "status_code" =>  9091 ,  'detailed_msg' => 'Product details updated successfully.' ];
        return json_encode( $data); 


      }


}
