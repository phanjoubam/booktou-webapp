<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

 
use App\Business;  
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\AgentServiceAreaModel;

use App\CustomerProfileModel;


use App\Traits\Utilities;
use App\User;




class ProfileControllerV2 extends Controller
{
	use Utilities;

     protected function addAddress(Request $request)
     {
        $user_info =  json_decode($this->getUserInfo($request->userId));  
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 

        
       if(  $request->address == "" || $request->landmark == "" ||  
            $request->city  == ""||   $request->state == "" || $request->pinCode  == "" ||  $request->addressType == ""  )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

        if($user_info->category == 0 ) //customer 
        {
                $member_addresses = DB::table("ba_addresses")
                ->whereRaw("id in (select address_id from ba_member_address where member_id='" . $user_info->member_id  . "') ")
                ->get();
 

                $found = false;
                $aid = 0;
                foreach($member_addresses as $aitem)
                {

                    if( strcasecmp( $request->address, $aitem->address ) == 0 && 
                        strcasecmp( $request->landmark, $aitem->landmark ) == 0 && 
                        strcasecmp( $request->city, $aitem->city ) == 0 && 
                        strcasecmp( $request->state, $aitem->state ) == 0 && 
                        strcasecmp( $request->pinCode, $aitem->pin_code ) == 0 && 
                        strcasecmp( $request->addressType, $aitem->address_type ) == 0  
                    )
                    {
                        $aid =$aitem->id; 
                        $found = true;
                        break; 
                    } 
                }

                if( !$found)
                {
                    $address = new AddressModel ;  
                }
                else 
                {
                    $address = AddressModel::find($aid)  ; 
                }

                $address->address = $request->address;
                $address->landmark = $request->landmark;
                $address->city = $request->city;
                $address->state = $request->state;
                $address->pin_code  = $request->pinCode;   
                $address->latitude  = ( $request->latitude != "" || $request->latitude != 0 ? $request->latitude : 0.00) ;   
                $address->longitude  = (  $request->longitude != "" || $request->longitude  != 0 ? $request->longitude : 0.00) ;   
                $address->address_type =  $request->addressType;  
                $save=$address->save();  
                $msg = 'Address saved successfully.'; 


                $new_address = MemberAddressModel::where("member_id",  $user_info->member_id  )
                ->where("address_id", $address->id  )
                ->first();

                if(!isset($new_address))
                {
                    $member_address = new MemberAddressModel;
                    $member_address->member_id =$user_info->member_id;
                    $member_address->address_id =$address->id;
                    $member_address->save();
                }

        }
        else 
        {

             $address = new AddressModel ;  
             $address->address = $request->address;
             $address->landmark = $request->landmark;
             $address->city = $request->city;
             $address->state = $request->state;
             $address->pin_code  = $request->pinCode; 
             $address->address_type =  $request->addressType;
             $save=$address->save();  
             $msg = 'Address saved successfully.'; 

            $member_address = new MemberAddressModel;
            $member_address->member_id =$user_info->member_id;
            $member_address->address_id =$address->id;
            $member_address->save(); 
        
        }
 


 

        if($save)
        {
            $data= ["message" => "success", "status_code" =>  5027 ,  'detailed_msg' => $msg ];
        }
        else
        {
        	$data= ["message" => "failure", "status_code" =>  5028 ,  'detailed_msg' => 'Address could not be successfully.'];
        }
    	return json_encode( $data);


     }

 
     
      protected function viewAddresses(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));  
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 

        $all_address = array();
        if($user_info->category == 0 ) //customer 
        {
            $customer = CustomerProfileModel::find($user_info->member_id);
            if(!isset($customer))
            {
                $data= ["message" => "failure",   "status_code" =>  999 , 'detailed_msg' => 'Customer information not found!'  ];
                return json_encode( $data);
            }
 
          $all_address [] =   array ( 
            "addressId" =>  0 , 
            "address" => $customer->locality,
            "landmark" => $customer->landmark,
            "city" => $customer->city ,
            "state" => $customer->state,
            "pinCode" => $customer->pin_code,
            "addressType" =>"primary" ); 

        } 

        $addresses   = DB::table("ba_addresses")  
        ->whereRaw("id in ( select address_id from ba_member_address where member_id='" . $user_info->member_id . "')" )
        ->select( "id as addressId", "address", "landmark", "city", "state",   "pin_code as pinCode", "address_type as addressType" )
        ->get();  
  
         
        foreach($addresses as $item)
        {
          $all_address[] = $item; 
        }
  
        $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Address are fetched.' , 
        'results' =>  $all_address ];

        return json_encode( $data);
     }

  

  // Agent Profile Update
  public function updateProfile(Request $request)
  {

      $user_info =  json_decode($this->getUserInfo($request->userId));

      if(  $user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
        return json_encode($data);
      }



        $updateProfile = CustomerProfileModel::find($user_info->member_id);
        if( !isset($updateProfile))
        {
          
          $data= ["message" => "failure",   "status_code" =>  999 ,
          'detailed_msg' => 'Profile Infomation not found!'  ];
          return json_encode( $data);
          
        } 

 

            $name = $request->fullName;
            $arr2 = explode(" " , $name);
            $totalname = count( $arr2 );

            if($totalname == 3)
            {
                $updateProfile->fname = $arr2[0];
                $updateProfile->mname = $arr2[1];
                $updateProfile->lname = $arr2[2];
            }
            else if($totalname == 2)
            {
                $updateProfile->fname = $arr2[0];
                $updateProfile->lname = $arr2[1];
                $updateProfile->mname = null ;
            }
  

            $updateProfile->fullname= $name;
            
            $dob = date('Y-m-d', strtotime($request->dateOfBirth));
            $email = $request->email;
            $phone = $request->phone;
            $locality = $request->locality;
            $landmark = $request->landmark;
            $city = $request->city;
            $state = $request->state;
            $pin = $request->pinCode;
            $aadhaar = $request->aadhaar;
            $pan = $request->panNo;
            
             if($dob!=null)
            {
              $updateProfile->dob = $dob;
            }
            if($email!=null)
            {
              $updateProfile->email = $email;
            }
            if($phone!=null)
            {
              $updateProfile->phone = $phone;
            }
            if($locality!=null)
            {
              $updateProfile->locality = $locality;
            }
            if($landmark!=null)
            {
              $updateProfile->landmark = $landmark;
            }
            if($city!=null)
            {
              $updateProfile->city = $city;
            }
            if($state!=null)
            {
              $updateProfile->state = $state;
            }
            if($pin!=null)
            {
              $updateProfile->pin_code = $pin;
            }
            if($aadhaar!=null)
            {
              $updateProfile->aadhaar = $aadhaar;
            }
            if($pan!=null)
            {
              $updateProfile->pan = $pan;
            }

            $save = $updateProfile->save();

            if($save)
            {
              $data= ["message" => "success", "status_code" =>  1071 ,  'detailed_msg' => 'Your profile is successfully updated.']; 
            }

        else{
            $data= ["message" => "success", "status_code" =>  1072 ,  'detailed_msg' => 'Update profile failed.'];
        }
        return json_encode( $data);     

    }



    // Delivery Business Area Update
    public function updateDeliveryAreas(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }

  
        if( $request->area  == "" ||  $request->mode == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

        $area  =  $request->area  ;
    
        $area_entry = AgentServiceAreaModel::where("member_id", $user_info->member_id )
        ->where("area_name",$area  )
        ->first( );



        if($request->mode == "add")
        {
            if(isset($area_entry ))
            {
              $code = 5062;
              $msg = 'Service area already exists.';
            }
            else 
            {
                $area_entry = new AgentServiceAreaModel ;
               $area_entry->member_id = $user_info->member_id ;
               $area_entry->area_name = $area   ; 
               $area_entry->save();

               $code = 5061;
               $msg = 'Service area added.'; 
            } 
        }
        else 
        {
            if(isset($area_entry ))
            {
              $area_entry->delete();
              $code = 5063;
                $msg = 'Service area updated.';
            }
            else 
            {
                $code = 5064;
                $msg = 'Service area not found.';
            }
        }

        $data= ["message" => "success", "status_code" =>  $code ,  'detailed_msg' =>  $msg ];
        return json_encode( $data);     

    }

    //Fetch business area
    protected function viewBusinessArea(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));  
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        }

        $all_areas   = DB::table("ba_areas_sectors")   
        ->select("area_sector as areaName", DB::Raw("'no' isServed"))
        ->get() ;

       $agentAreas   = DB::table("ba_agent_areas")  
        ->where("member_id",$user_info->member_id)
        ->select("area_name as areaName")
        ->get() ;

        foreach ($all_areas as $item) {

          foreach ($agentAreas as $value) {

            if ($item->areaName==$value->areaName) 
            {
              $item->isServed = "Yes";

              break;

            }
            
          }
        }
        
        $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Areas are fetched.' , 
        'results' =>  $all_areas ];

        return json_encode( $data);
     }

     //Fetch delivery agent profile
      public function getAgentProfile(Request $request)
    {
      $user_info =  json_decode($this->getUserInfo($request->userId)); 
      if(  $user_info->user_id == "na" )
      {
      $data= ["message" => "failure", "status_code" =>  901 ,
      'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
      return json_encode($data);
      }

      $agentInfo = CustomerProfileModel::where('id',$user_info->member_id)
        ->select("fullname as agentName","dob as dateOfBirth","phone as phoneNumber","email","aadhaar",
          "pan","locality","landmark","city","state","pin_code as pinCode",  "profile_photo as photo") 
        ->get();

         
        foreach ($agentInfo as $item) 
        {

          

          if ($item->photo !=null )
          {
              $image = URL::to('/public/assets/image/member'). $item->photo;  
          }
          else
          {
              $image = URL::to('/public/assets/image/no-image.jpg');
          }
            
          $item->dateOfBirth = date('d-m-Y', strtotime($item->dateOfBirth));
          $item->photo = $image;

        }


    $data= ["message" => "success", "status_code" =>  2011 ,
    'detailed_msg' => 'Profile are fetched.' , 'results' => $agentInfo];
    
    return json_encode( $data);
    }


public function verifyPhoneNumber(Request $request)
{
    $agent_info  = DB::table('ba_users')
    ->where('phone', $request->phone )
    ->where("category", 100)
    ->first();  


    if( isset($agent_info)  )
    {
      $userid=  $agent_info->id;
      $msg ="An agent with the same phone number exists."; 
    }
    else 
    {
        $userid=0;
        $msg ="Phone number is not registered as agent.";
    }


    $data= ["message" => "success", "status_code" =>  2079 ,
    'detailed_msg' =>  $msg , 'userId' =>  $userid ];
    
    return json_encode( $data);
     

}



  public function checkAgentStatus(Request $request)
  {

    $user_info =  json_decode($this->getUserInfo($request->userId));
    if(  $user_info->user_id == "na" )
    {
      $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
      return json_encode($data);
    }

    //block if  user is not agent. code # 100 is store agent
    if( $user_info->category  != 100   )
    {
      $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to delivery agent only.'  ];
      return json_encode($data);  
    }

    $profile = CustomerProfileModel::find(   $user_info->member_id  ) ;  
    if( !isset($profile))
    {
      $setupComplete = "no";    
    }
    else 
    {
      if(  $profile->fname == "" ||  $profile->lname == "" || $profile->locality == ""  || 
      $profile->landmark== "" ||  $profile->city  == "" || $profile->state == "" ||  $profile->pin_code  == ""  || 
      $profile->phone  == "")
      {
        $setupComplete = "no";    
      }
      else 
      {
        $setupComplete = "yes"; 

        $agent_areas = DB::table("ba_agent_areas")
        ->where("member_id",  $profile->id)
        ->count();


        if($agent_areas == 0 )
        {
           $setupComplete = "no"; 
        }
      }
    }
  
   

     $data= ["message" => "success", "status_code" =>  1011 ,
      'detailed_msg' => 'Agent profile completeness is fetched.' , 
    'setupComplete' => $setupComplete ];
    
    return json_encode( $data);

  }


  public function customerProfileStatus(Request $request)
  {

    $user_info =  json_decode($this->getUserInfo($request->userId));
    if(  $user_info->user_id == "na" )
    {
      $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
      return json_encode($data);
    }

    //block if  user is not customer. code # 0 is customer
    if( $user_info->category  != 0   )
    {
      $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to customer only.'  ];
      return json_encode($data);  
    }

    $profile = CustomerProfileModel::find(   $user_info->member_id  ) ;  
    if( !isset($profile))
    {
      $setupComplete = "no";    
    }
    else 
    {
      if(  $profile->fname == "" ||  $profile->lname == "" || $profile->locality == ""  || 
      $profile->landmark== "" ||  $profile->city  == "" || $profile->state == "" ||  $profile->pin_code  == ""  || 
      $profile->phone  == "" || $profile->latitude == "" || $profile->longitude == "")
      {
        $setupComplete = "no";    
      }
      else 
      {
        $setupComplete = "yes";  
      }
    }
  
   

     $data= ["message" => "success", "status_code" =>  1011 ,
      'detailed_msg' => 'Customer profile completeness is fetched.' , 
    'setupComplete' => $setupComplete ];
    
    return json_encode( $data);

  }


  
  public function generateAgentOTP(Request $request)
  {
      if( $request->phone == ""  )
      {
        $err_msg = "Field missing!";
        $err_code = 9000;
              
        $data = array(
          "message" => "failure" , 'status_code' => $err_code ,
          'detailed_msg' => $err_msg) ;
        
        return json_encode( $data ); 
      }

      $rand = mt_rand(111111, 999999);

      $user_info = User::where("phone", $request->phone )->where("category",  100 )->first();

      if(isset($user_info))
      {
          $data= ["message" => "failure",  "status_code" =>  997 , 'detailed_msg' => 'User is already registered as agent.'  ];
          return json_encode( $data); 
      }
 

      //saving profile
      $profile = new CustomerProfileModel;
      $profile->fname = " "; 
      $profile->mname = " " ; 
      $profile->lname = " "; 
      $profile->fullname = " "; 
      $profile->phone = $request->phone; 
      $profile->save();  


      //generate a confirmation OTP 
      $expire_date = strtotime( date('Y-m-d H:i:s') ) +(60*5) ;
      $newuser = new User; 
      $newuser->profile_id =$profile->id;
      $newuser->bin =0 ;
      $newuser->otp = $rand ;
      $newuser->otp_expires =date("Y-m-d H:i:s", $expire_date);
      $newuser->phone = $request->phone;
      $newuser->password =  Hash::make( $request->phone  )  ;
      $newuser->category = 100;
      $newuser->save(); 
        
      //sending sms  
      $err_msg =  "Your OTP sent as SMS to your registered mobile.";
      $phone = $request->phone  ;
      $otp_msg = "Your  OTP is " . $rand.  urlencode(". Please DO NOT SHARE this OTP with anyone. bookTou never calls you asking for OTP." ) ; 
      $this->sendSmsAlert(  array($phone)   ,  $otp_msg); 



      $err_status ="success";
      $err_code =  3503;
      $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg) ;
      return json_encode($data);  

    }



    // Delivery Business Area Update
    public function deleteAddress(Request $request)
    {

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
  
        if( $request->addressId == 0 )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Removing profile address is not allowed.'  ];
            return json_encode($data);
        }


        if( $request->addressId  == "" )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
            return json_encode($data);
        }

        $address_info = MemberAddressModel::where("address_id",  $request->addressId )->where("member_id", $user_info->member_id )->first() ;

        if($address_info->member_id != $user_info->member_id )
        {
            $data= ["message" => "failure",   "status_code" =>  999 ,  'detailed_msg' => 'Address not found!'  ];
            return json_encode( $data); 
        }

        $address_info->delete(); 


        $data= ["message" => "success", "status_code" => 3523  ,  'detailed_msg' => "Address is deleted successfully."];
        return json_encode( $data);     

    }



}
