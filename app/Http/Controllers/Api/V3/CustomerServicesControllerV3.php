<?php

namespace App\Http\Controllers\Api\V3;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;


use App\Business;
use App\ServiceDuration;
use App\ServiceBooking;
use App\Booking;
use App\User;
use App\BusinessPhoto;
use GuzzleHttp\Client;
use App\Traits\Utilities;

use App\BusinessOwner;
use App\BusinessReview;
use App\FavouriteBusiness;
use App\BusinessService;
use App\ServiceBookingDetails;
use App\BusinessHour;
use App\StaffAvailability;
use App\BusinessCategory;
use App\CustomerProfileModel;
use App\CustomerReviewModel;
use App\OrderSequencerModel;
use App\BusinessServiceDays;
use App\MerchantOrderSequencer;
use App\BannerPhotoModel;


use Hash;

class CustomerServicesControllerV3 extends Controller
{
	use Utilities;

	//SNS Service Booking
   	public function makeServiceBooking(Request $request)
    {

    	$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
			$data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }

        $addressCustomer = $user_info->userAddress;

		//finding customer
		$customer_info = CustomerProfileModel::find($user_info->member_id);
		if(!isset($customer_info))
		{
			$data= ["message" => "failure",   "status_code" =>  999 , 'detailed_msg' => 'No matching customer found!'  ];
			return json_encode( $data);
        }
		//finding customer ends

		if(  $request->serviceDate != "" )
		{
			$service_date = date('Y-m-d'  , strtotime($request->serviceDate )); //service on specified date
		}
		else
		{
			$service_date = date('Y-m-d');
		}


		//finding registered business
		$business = Business::find($request->bin);
		if( !isset($business))
        {
			$data= ["message" => "failure",    "status_code" =>  999 , 'detailed_msg' => 'Business information not found!'  ];
			return json_encode( $data);
        }


		//finding service products
		$serviceProductIds = array_unique( array_filter(explode( ",",  $request->serviceProductIds)) );
		if(sizeof($serviceProductIds) == 0 )
		{
			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => "No service selected!" ];
		     return json_encode($data);
		}

		$service_products = DB::table("ba_service_products")
		->where("bin", $request->bin )
		->whereIn("id",$serviceProductIds)
		->get();

		if( !isset($service_products) )
		{
			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No service products found!'  ];
			return json_encode($data);
		}
		//finding service products ends

		$total_duration_required = 0;
		$total_cost= $total_discount = 0.00;
		$serviceProductWithDuration = array();
		foreach($serviceProductIds as $serviceProduct)
		{
			$serviceDuration =0;
			foreach($service_products as $item)
			{
				if($serviceProduct == $item->id )
				{
					$serviceDuration += ( $item->duration_hr * 60 ) + $item->duration_min;
					$total_duration_required +=  ( $item->duration_hr * 60 ) + $item->duration_min;
					$total_cost +=  $item->actual_price;
					$total_discount  += $item->discount;
				}
			}
			$serviceProductWithDuration[] = array( 'serviceProductId' => $serviceProduct,
			'duration' => $serviceDuration );
		}

		$otp = mt_rand(111111, 999999);//order OTP generation
		if( strcasecmp($request->subModule ,"paa" ) ==0 )
		{
			if($request->serviceDate == "" ||   $request->serviceProductIds == ""  )
			{
				$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
				return json_encode($data);
			}

			$staffId = 0;

			$booked_dates  = DB::table("ba_service_booking")
			->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
			->whereDate("ba_service_booking.service_date", $service_date   )
			->whereIn("ba_service_booking_details.service_product_id",  $serviceProductIds )
			->where("ba_service_booking.bin", $request->bin)
			->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment') )
			->select("ba_service_booking.id as book_id", "service_date",  "ba_service_booking_details.service_product_id",
				DB::Raw("'close' status") )
			->get();

			$days = array();
			$status ="open";
			$found = false;
			//if any service is close for booking the rest of the service in the selection will be blocked from booking
			foreach($serviceProductIds as $service_product_id)
			{
				foreach($booked_dates as $item)
				{
					if(  $item->service_product_id == $service_product_id)
					{
						$found = true;
						break;
					}
				}

				if( $found )
				{
					$status ="close";
					break;
				}
			}

 			if(  $found )
			{
				$data= ["message" => "failure", "status_code" =>  906 ,
				'detailed_msg' => 'Selected service is already booked. Please choose another date or change service.'  ];
				return json_encode($data);
			}

			//no need to check slot as whole day is booked
			$slotno = 0; //assuming full day


			//generate order sequencer
			$keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
			$rp_1 = mt_rand(0, 9);
			$rp_2 = mt_rand(0, 9);
			$tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

			$sequencer = new OrderSequencerModel;
			$sequencer->type = "booking";
			$sequencer->tracker_session =  $tracker;
			$save = $sequencer->save();
			//generate order sequencer ends

			if( !isset($save) )
			{
				$data= ["message" => "failure", "status_code" =>  997 , 'detailed_msg' => 'Failed to generate order no sequence.'  ];
				return json_encode($data);
			}

			//generate merchant sequencer
			$merchant_order_no =1;
			$merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
			if(isset($merchant_order_info))
			{
				$merchant_order_no =  $merchant_order_info->last_order + 1;
			}
			else
			{
				$merchant_order_info = new MerchantOrderSequencer;
				$merchant_order_info->bin  = $request->bin;
			}
			//generate merchant sequencer ends

			//make new booking
			$newbooking = new ServiceBooking;
			$newbooking->id = $sequencer->id ;
			$newbooking->customer_name = $customer_info->fullname;
			$newbooking->customer_phone = $customer_info->phone;
			$newbooking->address = $customer_info->locality;
			$newbooking->landmark =$customer_info->landmark;
			$newbooking->city = $customer_info->city ;
			$newbooking->state = $customer_info->state;
			$newbooking->biz_order_no = $merchant_order_no;
			$newbooking->bin = $request->bin;
			$newbooking->book_category= $business->category;
			$newbooking->book_by = $user_info->member_id;
			$newbooking->staff_id = $staffId;
			$newbooking->book_status = "new";
			$newbooking->total_cost =  $total_cost;
			$newbooking->seller_payable =  $total_cost;
			$newbooking->discount = $total_discount;
			$newbooking->service_fee  =  0.00;
			$newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
			$newbooking->cashback =  0;
			$newbooking->payment_type =   $request->payMode != "" ?  $request->payMode : "OTC";   //over the counter
			$newbooking->latitude =  $newbooking->longitude =  0;
			$newbooking->book_date =  date('Y-m-d H:i:s');
			$newbooking->service_date =  $service_date;
			$newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
			$newbooking->transaction_no =  "na" ;
			$newbooking->reference_no =  "na"  ;

			$newbooking->preferred_time =  date('H:i:s', strtotime( $request->preferredTime));
			$newbooking->otp = $otp ;
			$newbooking->address = $customer_info->locality;
			$newbooking->landmark = $customer_info->landmark;
			$newbooking->city = $customer_info->city;
			$newbooking->state = $customer_info->state;
			$newbooking->pin_code  = $customer_info->pin_code;
			$save=$newbooking->save();

			if($save)
			{
				//update merchant order sequencer log
				$merchant_order_info->last_order  = $merchant_order_no;
				$merchant_order_info->save();
				//ending merchant order sequencer

				//updatig which service were booked in this order
				$book_slots = array();
				foreach($service_products as $item)
				{
					$newbookingDetails = new ServiceBookingDetails;
					$newbookingDetails->book_id = $sequencer->id;
					$newbookingDetails->service_product_id = $item->id    ;
					$newbookingDetails->service_name = $item->srv_name  ;
					$newbookingDetails->slot_no = $slotno ;
					$newbookingDetails->status = "new";
					$newbookingDetails->service_time = date('H:i:s', strtotime( $request->preferredTime));
					$newbookingDetails->service_charge = $item->actual_price;
					$newbookingDetails->save();
				}
				//updating occuppied
				DB::table("ba_service_days")
				->whereDate("service_date", $service_date)
				->where("bin",$request->bin)
				->update(array("status" => "booked") ) ;
			}


		}
		else
		{
			if($request->serviceDate == "" || $request->staffId == "" || $request->serviceProductIds == ""  )
			{
				$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
				return json_encode($data);
			}
			$staffId =   $request->staffId;



			if( $request->preferredTime != "")
			{
				//home visit
				//generate order sequencer
				$keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
				$rp_1 = mt_rand(0, 9);
				$rp_2 = mt_rand(0, 9);
				$tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

				$sequencer = new OrderSequencerModel;
				$sequencer->type = "booking";
				$sequencer->tracker_session =  $tracker;
				$save = $sequencer->save();
				//generate order sequencer ends

				if( !isset($save) )
				{
					$data= ["message" => "failure", "status_code" =>  997 , 'detailed_msg' => 'Failed to generate order no sequence.'  ];
					return json_encode($data);
				}

				//generate merchant sequencer
				$merchant_order_no =1;
				$merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
				if(isset($merchant_order_info))
				{
					$merchant_order_no =  $merchant_order_info->last_order + 1;
				}
				else
				{
					$merchant_order_info = new MerchantOrderSequencer;
					$merchant_order_info->bin  = $request->bin;
				}
				//generate merchant sequencer ends

				//make new booking
				$newbooking = new ServiceBooking;
				$newbooking->id = $sequencer->id ;
				$newbooking->customer_name = $customer_info->fullname;
				$newbooking->customer_phone = $customer_info->phone;
				$newbooking->address = $customer_info->locality;
				$newbooking->landmark =$customer_info->landmark;
				$newbooking->city = $customer_info->city ;
				$newbooking->state = $customer_info->state;
				$newbooking->biz_order_no = $merchant_order_no;
				$newbooking->bin = $request->bin;
				$newbooking->book_category= $business->category;
				$newbooking->book_by = $user_info->member_id;
				$newbooking->staff_id = $staffId;
				$newbooking->book_status = "new";
				$newbooking->total_cost =  $total_cost;
				$newbooking->seller_payable =  $total_cost;
				$newbooking->discount = $total_discount;
				$newbooking->service_fee  =  0.00;
				$newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
				$newbooking->cashback =  0;
				$newbooking->payment_type =   $request->payMode != "" ?  $request->payMode : "OTC";   //over the counter
				$newbooking->latitude =  $newbooking->longitude =  0;
				$newbooking->book_date =  date('Y-m-d H:i:s');
				$newbooking->service_date =  $service_date;
				$newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
				$newbooking->transaction_no =  "na" ;
				$newbooking->reference_no =  "na"  ;
				$newbooking->preferred_time =  date('H:i:s', strtotime( $request->preferredTime));
				$newbooking->otp  =  $otp ;
				$newbooking->address = $customer_info->locality;
				$newbooking->landmark = $customer_info->landmark;
				$newbooking->city = $customer_info->city;
				$newbooking->state = $customer_info->state;
				$newbooking->pin_code  = $customer_info->pin_code;
				$save=$newbooking->save();

				if($save)
				{
					//update merchant order sequencer log
					$merchant_order_info->last_order  = $merchant_order_no;
					$merchant_order_info->save();
					//ending merchant order sequencer
				}
			}
			else
			{
				//check if slot is available for the day
				$slotno= $request->serviceTimeSlot;
				//ascending order slot is very important
				$day_name = date('l', strtotime( $service_date ));
				$staff_id = $request->staffId ;
				if($request->staffId  == -1) //any staff
				{
					//find available staff
					$any_staff_available = DB::table("ba_service_days")
					->where("slot_number", $slotno )
					->whereDate("service_date",  $service_date )
					->where("bin",$request->bin)
					->where("status", "open")
					->orderBy("slot_number", "asc")
					->first();

					if(isset($any_staff_available) == 0)
					{
						$data= ["message" => "failure", "status_code" =>  906 ,
						'detailed_msg' => 'No staff available in the selected slot. Please change date or reduce sevice selected.'  ];
						return json_encode($data);
					}

					$staff_id = $any_staff_available->staff_id;
				}

				//ascending order sort is important otherwise it will break code
				$available_slots = DB::table("ba_service_days")
				->where("slot_number", ">=", $slotno )
				->where("staff_id",  $staff_id )
				->whereDate("service_date",  $service_date )
				->where("bin",$request->bin)
				->where("status", "open")
				->orderBy("slot_number", "asc")
				->get();

				$availableSlots = $book_slots = array();
				$total_slots_min = 0;
				$starting_slot_found = false;
				$ending_slot_found = false;


				//$serviceProductWithDuration[] = array('serviceProductId' =>  1, 'duration' =>  4 );
				$lastFoundSlot = $lastScannedSlot = 0;
				$scanCount =0;
				foreach($serviceProductWithDuration as $serviceProductDuration )
				{
					foreach($available_slots as $item )
					{
						if( $item->slot_number == $request->serviceTimeSlot  )
						{
							$starting_slot_found = true; //user specified slot is found empty
						}
						if($lastScannedSlot == 0 )
						{
							$lastScannedSlot = $item->slot_number;
						}
						//skip upto last processed slot from second scan onwards
						if( $scanCount > 0 && $lastScannedSlot >= $item->slot_number )
						{
							continue;
						}

						$lastScannedSlot = $item->slot_number; //logging last scanne slot
						$start_time =   strtotime( $item->start_time );
						$end_time =   strtotime( $item->end_time );
						$gap =  abs( ( $end_time - $start_time) / 60);
						$serviceProductDuration['duration'] -= $gap;
						$total_slots_min += $gap;
						$total_duration_required -= $gap; //reducing total duration

						$availableSlots[] = array(
							'serviceProductId' => $serviceProductDuration['serviceProductId'],
							"slot" => $lastScannedSlot ,
							"duration" => abs($gap),
							"startTime" =>$start_time,
						);
						if( $serviceProductDuration['duration'] <= 0 )
						{
							break;
						}
					}
					$scanCount++;
				}


				$allowBooking = true;
				foreach($serviceProductWithDuration as $serviceDuration)
				{
					$totalDuration = $serviceDuration['duration'];
					foreach($availableSlots as $availableSlot)
					{
						if( $serviceDuration['serviceProductId'] == $availableSlot['serviceProductId'])
						{
							$totalDuration -= $availableSlot['duration'];
						}
					}

					if($totalDuration > 0  )
					{
						$allowBooking = false;
						break;
					}
				} 
				// if no available slot is fount or no starting slot is found or duration of the slot found is less than required
				//block booking
				//if( !$starting_slot_found ||  $total_duration_required > 0  )
				if(count($availableSlots) == 0 || !$starting_slot_found || !$allowBooking  )
				{
					$data= ["message" => "failure", "status_code" =>  906 ,
					'detailed_msg' => 'Insufficient slots. Please change date or reduce sevice selected.'  ];
					return json_encode($data);
				}


				foreach($availableSlots as $availableSlot)
				{
					$book_slots[] = $availableSlot['slot'];
				}


				//generate order sequencer
				$keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
				$rp_1 = mt_rand(0, 9);
				$rp_2 = mt_rand(0, 9);
				$tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

				$sequencer = new OrderSequencerModel;
				$sequencer->type = "booking";
				$sequencer->tracker_session =  $tracker;
				$save = $sequencer->save();
				//generate order sequencer ends

				if( !isset($save) )
				{
					$data= ["message" => "failure", "status_code" =>  997 , 'detailed_msg' => 'Failed to generate order no sequence.'  ];
					return json_encode($data);
				}

				//generate merchant sequencer
				$merchant_order_no =1;
				$merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
				if(isset($merchant_order_info))
				{
					$merchant_order_no =  $merchant_order_info->last_order + 1;
				}
				else
				{
					$merchant_order_info = new MerchantOrderSequencer;
					$merchant_order_info->bin  = $request->bin;
				}
				//generate merchant sequencer ends

				//make new booking
				$newbooking = new ServiceBooking;
				$newbooking->id = $sequencer->id ;
				$newbooking->customer_name = $customer_info->fullname;
				$newbooking->customer_phone = $customer_info->phone;
				$newbooking->address = $customer_info->locality;
				$newbooking->landmark =$customer_info->landmark;
				$newbooking->city = $customer_info->city ;
				$newbooking->state = $customer_info->state;
				$newbooking->biz_order_no = $merchant_order_no;
				$newbooking->bin = $request->bin;
				$newbooking->book_category= $business->category;
				$newbooking->book_by = $user_info->member_id;
				$newbooking->staff_id = $staffId;
				$newbooking->book_status = "new";
				$newbooking->total_cost =  $total_cost;
				$newbooking->seller_payable =  $total_cost;
				$newbooking->discount = $total_discount;
				$newbooking->service_fee  =  0.00;
				$newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
				$newbooking->cashback =  0;
				$newbooking->payment_type =   $request->payMode != "" ?  $request->payMode : "OTC";   //over the counter
				$newbooking->latitude =  $newbooking->longitude =  0;
				$newbooking->book_date =  date('Y-m-d H:i:s');
				$newbooking->service_date =  $service_date;
				$newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
				$newbooking->transaction_no =  "na" ;
				$newbooking->reference_no =  "na"  ;
				$newbooking->otp  =  $otp ;
				$newbooking->address = $customer_info->locality;
				$newbooking->landmark = $customer_info->landmark;
				$newbooking->city = $customer_info->city;
				$newbooking->state = $customer_info->state;
				$newbooking->pin_code  = $customer_info->pin_code;
				$save=$newbooking->save();

				if($save)
				{
					//update merchant order sequencer log
					$merchant_order_info->last_order  = $merchant_order_no;
					$merchant_order_info->save();
					//ending merchant order sequencer
					$pos = 0;
					$size = sizeof($availableSlots);

					foreach($service_products as $item)
					{
						if( $pos >= $size )
						{
							$pos = $size-1;
						}

						$startSlot = 0;
						$startTime = time();

						foreach($availableSlots as $availableSlot)
						{
							if( $availableSlot['serviceProductId'] == $item->id  )
							{
								$startSlot = $availableSlot['slot'];
								$startTime = $availableSlot['startTime'];
								break;
							}
						}
						$newbookingDetails = new ServiceBookingDetails;
						$newbookingDetails->book_id = $sequencer->id;
						$newbookingDetails->service_product_id = $item->id    ;
						$newbookingDetails->service_name = $item->srv_name  ;
						$newbookingDetails->slot_no = $startSlot; //$availableSlot[$pos]["slot"]  ;
						$newbookingDetails->status = "new";
						$newbookingDetails->service_time = date('H:i:s', $startTime );
						$newbookingDetails->service_charge = $item->actual_price;
						$newbookingDetails->save();
						$pos++;
					}


					//updating occupied
					DB::table("ba_service_days")
					->whereIn("slot_number" , $book_slots )
					->whereDate("service_date", $service_date)
					->where("bin", $request->bin )
					->where("staff_id",  $staff_id )
					->where("status",  "open" )
					->update(array("status" => "booked") ) ;
				}
			}
		}

		//upload attachment
		if($request->attachment  != "" )
        {
			$folder_path =  config('app.app_cdn_path')   . "/assets/image/uploads/order_" . $sequencer->id .    "/";
			if( !File::isDirectory($folder_path)){
				File::makeDirectory( $folder_path, 0755, true, true);
			}
			$file =  $request->file('attachment');
			$originalname = $file->getClientOriginalName();
			$extension = $file->extension( );
			$filename =  "attach_" . $sequencer->id . "_" .  time() . ".{$extension}";
			$file->storeAs('uploads',  $filename );
			$imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename ,   $folder_path .  $filename );
			$newbooking->attachment = config('app.app_cdn') . "/assets/image/uploads/order_" . $sequencer->id .    "/" . $filename;
		}
		//end attachment

		$message = "success";
		$err_code = 1027;
		$err_msg = "Your booking is placed successfully.";

        $data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg , 'confirmationOTP' => $otp ];
        return json_encode( $data);

    }



	public function makeOfflineServiceBooking(Request $request)
    {
		$staffId = 0 ;
		$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }

		$business = Business::find($request->bin);

		if( !isset($business))
        {
			$data= ["message" => "failure",    "status_code" =>  999 , 'detailed_msg' => 'Business information not found!'  ];
			return json_encode( $data);
        }

        if(  $request->serviceDate != "" )
        {
			$service_date = date('Y-m-d'  , strtotime($request->serviceDate )); //service on specified date
		}
		else
		{
			$service_date = date('Y-m-d');
		}


		if( strcasecmp($request->subModule ,"PAA" ) ==0 )
		{
			if(  $request->serviceProductIds == ""  )
			{
				$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
				return json_encode($data);
			}
			$staffId = 0 ;
		}

		//finding service products
		$serviceProductIds = array_unique( array_filter(explode( ",",  $request->serviceProductIds)) );
		if(sizeof($serviceProductIds) == 0 )
		{
			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => "No service selected!" ];
		     return json_encode($data);
		}

		$service_products = DB::table("ba_service_products")
		->whereIn("id",$serviceProductIds)
		->get();

		if( !isset($service_products) )
		{
			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No service products found.'  ];
            return json_encode($data);
		}


		$total_duration_required = 0;
		$total_cost= $total_discount = 0.00;
		$serviceProductWithDuration = array();
		foreach($serviceProductIds as $serviceProduct)
		{
			$serviceDuration =0;
			foreach($service_products as $item)
			{
				if($serviceProduct == $item->id )
				{
					$serviceDuration += ( $item->duration_hr * 60 ) + $item->duration_min;
					$total_duration_required +=  ( $item->duration_hr * 60 ) + $item->duration_min;
					$total_cost +=  $item->actual_price;
					$total_discount  += $item->discount;
				}
			}
			$serviceProductWithDuration[] = array(
				'serviceProductId' => $serviceProduct,
				'duration' => $serviceDuration );
		}

		//checked upto here

		$otp = mt_rand(111111, 999999);

		if( strcasecmp($request->subModule ,"paa" ) ==0 )
		{
			//no need to check slot as whole day is booked
			$slotno = 0; //assuming full day
			$staffId = 0;

			$booked_dates  = DB::table("ba_service_booking")
			->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
			->whereDate("ba_service_booking.service_date", $service_date   )
			->whereIn("ba_service_booking_details.service_product_id",  $serviceProductIds )
			->where("ba_service_booking.bin", $request->bin)
			->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment') )
			->select("ba_service_booking.id as book_id", "service_date",  "ba_service_booking_details.service_product_id",
				DB::Raw("'close' status") )
			->get();

			$days = array();
			$status ="open";
			$found = false;
			//if any service is close for booking the rest of the service in the selection will be blocked from booking
			foreach($serviceProductIds as $service_product_id)
			{
				foreach($booked_dates as $item)
				{
					if(  $item->service_product_id == $service_product_id)
					{
						$found = true;
						break;
					}
				}

				if( $found )
				{
					$status ="close";
					break;
				}
			}

			if(  $found )
			{
				$data= ["message" => "failure", "status_code" =>  906 ,
				'detailed_msg' => 'Selected service is already booked. Please choose another date or change service.'  ];
				return json_encode($data);
			}

			//save sequencer
			$keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
			$rp_1 = mt_rand(0, 9);
			$rp_2 = mt_rand(0, 9);
			$tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

			$sequencer = new OrderSequencerModel;
			$sequencer->type = "booking";
			$sequencer->tracker_session =  $tracker;
			$save = $sequencer->save();

			if( !isset($save) )
			{
				$data= ["message" => "failure", "status_code" =>  997 , 'detailed_msg' => 'Failed to generate order no sequence.'  ];
	            return json_encode($data);
			}

			//generate merchant sequencer
			$merchant_order_no =1;
			$merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
			if(isset($merchant_order_info))
			{
				$merchant_order_no =  $merchant_order_info->last_order + 1;
			}
			else
			{
				$merchant_order_info = new MerchantOrderSequencer;
				$merchant_order_info->bin  = $request->bin;
			}
			//generate merchant sequencer ends
			//make new booking
			$newbooking = new ServiceBooking;
			$newbooking->id = $sequencer->id ;
			$newbooking->customer_name = $request->fullname;
			$newbooking->customer_phone = $request->phone;
	        $newbooking->address = $request->locality;
	        $newbooking->landmark =$request->landmark;
	        $newbooking->city = $request->city ;
	        $newbooking->state = $request->state;
			$newbooking->pin_code  = $request->pinCode;
			$newbooking->biz_order_no = $merchant_order_no;
			$newbooking->bin = $request->bin;
			$newbooking->book_category= $business->category;
			$newbooking->book_by = $user_info->member_id;
			$newbooking->staff_id = $staffId;
			$newbooking->book_status = "new";
			$newbooking->total_cost =  $total_cost;
			$newbooking->seller_payable =  $total_cost;
			$newbooking->discount = $total_discount;
			$newbooking->service_fee  =  0.00;
			$newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
			$newbooking->cashback =  0;
			$newbooking->payment_type =   $request->payMode != "" ?  $request->payMode : "OTC";   //over the counter
			$newbooking->latitude =  $newbooking->longitude =  0;
			$newbooking->book_date =  date('Y-m-d H:i:s');
			$newbooking->service_date =  $service_date;
			$newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
			$newbooking->transaction_no =  "na" ;
			$newbooking->reference_no =  "na"  ;
			$newbooking->preferred_time =  date('H:i:s', strtotime( $request->preferredTime));
			$newbooking->otp = $otp ;
			//upload attachment
			if($request->attachment  != "" )
	        {
				$folder_path =  config('app.app_cdn_path')   . "/assets/image/uploads/order_" . $sequencer->id .    "/";
				if( !File::isDirectory($folder_path)){
					File::makeDirectory( $folder_path, 0755, true, true);
				}

				$file =  $request->file('attachment');
				$originalname = $file->getClientOriginalName();
				$extension = $file->extension( );
				$filename =  "attach_" . $sequencer->id . "_" .  time() . ".{$extension}";
				$file->storeAs('uploads',  $filename );
				$imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename ,   $folder_path .  $filename );
				$newbooking->attachment = config('app.app_cdn') . "/assets/image/uploads/order_" . $sequencer->id .    "/" . $filename;
			}
			//end attachment
			$save=$newbooking->save();


			if($save)
			{
				//update merchant order sequencer log
				$merchant_order_info->last_order  = $merchant_order_no;
				$merchant_order_info->save();
				//ending merchant order sequencer log
				//updating which service were booked in this order
				$book_slots = array();
				foreach($service_products as $item)
				{
					$newbookingDetails = new ServiceBookingDetails;
					$newbookingDetails->book_id = $sequencer->id;
					$newbookingDetails->service_product_id = $item->id    ;
					$newbookingDetails->service_name = $item->srv_name  ;
					$newbookingDetails->slot_no = $slotno ;
					$newbookingDetails->status = "new";
					$newbookingDetails->service_time = date('H:i:s', strtotime( $request->preferredTime));
					$newbookingDetails->service_charge = $item->actual_price;
					$newbookingDetails->save();
				}
				//updating occuppied
				DB::table("ba_service_days")
				->whereDate("service_date", $service_date)
				->where("bin",$request->bin)
				->update(array("status" => "booked") ) ;
			}
		}

		$message = "success";
		$err_code = 1027;
		$err_msg = "Your booking is placed successfully.";

        $data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg , 'confirmationOTP' => $otp ];
        return json_encode( $data);

    }


	public function cancelAllClientBookingId(Request $request)
	{
		$user_info =  json_decode($this->getUserInfo($request->userId));
		if(  $user_info->user_id == "na" )
		{
			$data= ["message" => "failure", "status_code" =>  901 ,
			'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
			return json_encode($data);
		}

		if(  $request->bookingNo == "" 	)
		{
		 $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
		}


		$day_booking = ServiceBooking::find(  $request->bookingNo)  ;
		if( !isset($day_booking))
		{
			$data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'No booking record found!'  ];
			return json_encode($data);
		}

		$business = Business::find( $day_booking->bin  ) ;
		if( !isset($business))
		{
			$data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'Business information not found!'  ];
			return json_encode($data);
		}

		$booking_details = ServiceBookingDetails::where("book_id",  $request->bookingNo)->first() ;
		if( !isset($booking_details))
		{
			$data= ["message" => "failure", "status_code" =>  903 , 'detailed_msg' => 'No booking details found!'  ];
			return json_encode($data);
		}

		if(  $request->source == "client" 	)
		{
		  $action_by = 'cancel_by_client';
		}
		else
		{
		   $action_by = 'cancel_by_owner';
		}

		//cancel booking table
		$day_booking->book_status =  $action_by;
		$day_booking->save();

		//cancel all services
		$booking_details->status = $action_by ;
		$booking_details->save();

		//cancel in realtime Q
		DB::table('ba_realtime_queue')
		->where('book_id', $request->bookingNo  )
		->where('staff_id', $day_booking->staff_id  )
		->update(['status' => $action_by  ]);

		//updating occupied to open
		if(   strcasecmp($business->main_module , "booking") == 0 )
		{
			DB::table("ba_service_days")
			->where("bin", $day_booking->bin)
			->whereDate("service_date", date('Y-m-d', strtotime( $day_booking->service_date)))
			->update(array("status" => "open") ) ;
		}
		else
		{
			DB::table("ba_service_days")
			->where("bin", $day_booking->bin)
			->whereDate("service_date", date('Y-m-d', strtotime( $day_booking->service_date)))
			->where("staff_id",  $day_booking->staff_id )
			->update(array("status" => "open") ) ;
		}


		$message = "success";
		$err_code = 2023;
		$err_msg = "Your booking is cancelled!";


		$data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg  ];
		return json_encode( $data);

	}


	protected function fetchBannerImages(Request $request)
     {
     	$user_info =  json_decode($this->getUserInfo($request->userId));
		if(  $user_info->user_id == "na" )
		{
			$data= ["message" => "failure", "status_code" =>  901 ,
			'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
			return json_encode($data);
		}


     	$buss= Business::where("id","$request->bin")
     		->select("name as businessName","id as bin",
     		DB::Raw("1  imageSequence"),'banner as imageUrl')
     		->get();

     	$banner_photos  = BannerPhotoModel::where('bin',$request->bin)
     	->select("bin",
     	DB::Raw("''  businessName"),
     	"id as imageSequence",
     	"image_url as imageUrl")
     	->orderBy('imageSequence', 'desc')
        ->take(10)
     	->get();

     	if(count($banner_photos)<=0)
     	{
     	foreach($buss as $biz){
     		$biz->imageUrl = url::to ($biz->imageUrl);

     	}
     	$data= ["message" => "success", "status_code" =>  1031 ,  'detailed_msg' => 'banner photos are fetched.', "result" => $buss];
      		return json_encode( $data);
     	}

     	foreach($banner_photos as $item )
     	{
     		foreach($buss as $buz)
     		{
     		if($item->bin = $buz->bin)

     			$item->businessName = $buz->businessName;
     		}
     	}

     $data= ["message" => "success", "status_code" =>  1031 ,  'detailed_msg' => 'banner photos are fetched.', "result" => $banner_photos];
      return json_encode( $data);
     }

/*======================check date available for booking===============================*/
     protected function checkDateAvailability(Request $request)
  {

  	 if(  $request->bin == "" || $request->serviceDate  == ""  || $request->serviceProductIds == "")
	   {
		     $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => "Missing data to perform action." ];
		     return json_encode($data);
	   }

		$service_product_ids  = explode(",", $request->serviceProductIds  );

		if(sizeof($service_product_ids) == 0 )
		{
			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => "No service selected!" ];
		     return json_encode($data);
		}



	   $bin = $request->bin ;
	   $business = Business::find($bin);

		 if(  !isset($business) )
     {
     		$data= ["message" => "failure", "status_code" =>  999 ,  'detailed_msg' => 'No matching business found!' ];
        return json_encode($data);
      }

      if( strcasecmp($business->sub_module, "sns") == 0 )
      {

      	 $booked_dates = DB::table("ba_service_days")
			   ->where("service_date", date('Y-m-d', strtotime($request->serviceDate  ) ) )
			   ->where("bin",$request->bin)
			   ->select(DB::Raw( "distinct service_date"), "status")
			   ->get();

			   for($i= 1 ; $i<=$lastday ; $i++)
			   {
				   $status ="closed";
				   $found = false;
				   foreach($booked_dates  as $item)
				   {
					   if( date('d', strtotime($item->service_date)) == $i)
					   {
						   if( $item->status == "engage" || $item->status == "booked" )
						   {
							   $status ="booked";
						   }
						   else
						   {
							   $status ="open";
						   }
						   $found = true;
						   break;
					   }
				   }

				   if( !$found)
				   {
					   $status ="close";
				   }
				   $days[] = array( 'serviceProductId' => 0,   "dayNumber" => $i, "status" => $status);
			   }

      }
      else if( strcasecmp($business->sub_module, "paa") == 0 )
      {


      	$booked_dates  = array();
      	//need selected product ID in future code update

      	$booked_dates  = DB::table("ba_service_booking")
				->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
				->whereDate("ba_service_booking.service_date", date('Y-m-d', strtotime( $request->serviceDate  ))  )
				->whereIn("ba_service_booking_details.service_product_id",  $service_product_ids )
				   ->where("ba_service_booking.bin", $request->bin)
				   ->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment') )
				   ->select("ba_service_booking.id as book_id", "service_date",  "ba_service_booking_details.service_product_id",
				   	DB::Raw("'close' status") )
				   ->get();

					 //preparing available days : FUTURE CODE
				   foreach($service_product_ids as $service_product_id)
				   {

				   	$status ="open";
						$found = false;

						foreach($booked_dates as $item)
							   {
								   if(  $item->service_product_id == $service_product_id)
								   {
								   	 $found = true;
									   break;
								   }
							   }

							   if( $found )
							   {
								   $status ="close";
							   }

							   $days[] = array( 'serviceProductId' => $service_product_id,   "dayNumber" =>  date('d', strtotime( $request->serviceDate  ))  ,  "status" => $status);

					 }

      }

      $data= ["message" => "success", "status_code" =>  "3021" ,
      'detailed_msg' => "Available dates are fetched.",
	   'results' =>  $days ];

	   	return json_encode( $data);

     }
}
