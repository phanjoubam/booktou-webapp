<?php

namespace App\Http\Controllers\Api\V3; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File; 
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

use App\Jobs\SendOrderConfirmationSms;
use App\Jobs\SendOrderAssignToAgentSms;
 

use App\ShoppingOrderItemsModel;  
use App\ShoppingCartTemporary;  
use App\AgentServiceAreaModel; 
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\ProductModel; 
use App\Business;
use App\Traits\Utilities;
use App\User; 
use App\MarketingCallLog;
use App\CallbackEnquiryModel;
use App\MemberWishlistModel;


class WebApiControllerV3 extends Controller
{
	use Utilities; 
     
  
    //fetch all slots for booking sub-module
  protected function checkDateAvailability(Request $request)
  {

     if(  $request->bin == "" || $request->date  == ""  || $request->serviceProductIds == "" )
     {
       return response()->json(array(
        "detailed_msg" => 'Missing data to perform action!', 
        "status_code" => 999,
        "message" => "failure"
      ));
     }

     $service_product_ids  = $request->serviceProductIds; 

     if(sizeof($service_product_ids) == 0 )
     {

       return response()->json(array(
        "detailed_msg" => 'No service selected!', 
        "status_code" => 999,
        "message" => "failure"));
     } 

     $bin = $request->bin ;
     $service_date = date( "Y-m-d",  strtotime($request->date ));
     $business = Business::find($bin); 
     if(  !isset($business) )
     {
      return response()->json(array(
        "detailed_msg" => 'No matching business found!', 
        "status_code" => 999,
        "message" => "failure"));
    }  
    if( strcasecmp($business->sub_module, "sns") != 0 )
    {
      return response()->json(array(
        "detailed_msg" => 'Selected business is not registered under booking service!', 
        "status_code" => 999,
        "message" => "failure"));
    }

    $booked_dates  = array(); 

    $booked_dates  = DB::table("ba_service_booking")  
    ->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
    ->whereDate("ba_service_booking.service_date", date('Y-m-d', strtotime( $request->date))) 
    ->whereIn("ba_service_booking_details.service_product_id",  $service_product_ids )   
    ->where("ba_service_booking.bin", $request->bin)  
             //->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment'))
    ->whereIn("book_status",  array('new','confirmed','engaged'))   
    ->select("ba_service_booking.id as book_id", "service_date",  
      "ba_service_booking_details.service_product_id", 
      DB::Raw("'close' status") ) 
    ->get();

    foreach($service_product_ids as $service_product_id)
    {

      $status ="open";
      $found = false;

      foreach($booked_dates as $item)
      { 
       if(  $item->service_product_id == $service_product_id)
       {
         $found = true; 
         break;
       } 
     }

     if( $found )
     {
       $status ="close";
     } 

     $days[] = array( 'serviceProductId' => $service_product_id,   "dayNumber" =>  date('d', strtotime( $request->date  ))  ,  "status" => $status ); 

   } 

   $business_hours = DB::table("ba_shop_hours")
   ->where("bin", $request->bin)
   ->where("day_name",  $request->dayName)
   ->select("day_name", DB::Raw("  CONCAT( time_format( day_open,  '%r' )  , '-',  time_format( day_close,  '%r' ) ) as  shifts") )
   ->get();


   $staff_ids = DB::table("ba_users")  
   ->where("bin", $bin ) 
   ->whereIn("category", array("1", '10' ) ) 
   ->pluck("profile_id"  );


   if(count( $staff_ids) > 0 )
   {
     $staff_available = DB::table("ba_profile") 
     ->whereNotIn("id",  function($query) use ( $service_date, $bin )
     {
      $query->select("staff_id") 
      ->from('ba_staff_day_off')
      ->where("bin",  $bin ) 
      ->whereDate("off_date",  $service_date ) ;  
    })
     ->whereIn("id",   $staff_ids )
     ->select("id as staffId", "fullname as staffName", "profile_image_url as profilePicture", 
      DB::Raw("'no' available") ) 
     ->get();
   }
   else
   {
    $staff_available =$staff_ids;
  }

  $available_slots = DB::table("ba_service_days")
  ->whereDate("service_date", $service_date )
  ->where("bin", $bin ) 
  ->distinct('slot_number','start_time', "end_time")
  ->select('slot_number','start_time', "end_time")
  ->get();

  foreach($available_slots as $available_slot)
  {
    $available_slot->start_time = date('h:i A', strtotime($available_slot->start_time));
    $available_slot->end_time = date('h:i A', strtotime($available_slot->end_time));
  }


  return response()->json(array(
    "message" => "success", 
    "status_code" =>  "3021" ,  
    'detailed_msg' => "Available dates are fetched.",
    'day_name' => $request->dayName,
    'business_hours' =>$business_hours,
    'staffs' => $staff_available , 
    'available_slots' => $available_slots,
    'results' =>  $days ));

}



  //check if a particular staff is available on selected slot
  public function checkStaffAvailability(Request $request)
  {

    if( $request->bin == "" || $request->date == ""  || $request->slotNo == "" || $request->staffId == "" )
    {
      $data= ["message" => "failure", "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
    }
    
    $date = $request->date == "" ?  date('Y-m-d') :  date('Y-m-d', strtotime( $request->date ) )  ;
    $bin = $request->bin;
    $slot_no = $request->slotNo;
    $staff_id = $request->staffId;

    $business = Business::find($bin); 
     if(  !isset($business) )
     {
      return response()->json(array(
        "detailed_msg" => 'No matching business found!', 
        "status_code" => 999,
        "message" => "failure"));
    }

    $staff_rows = DB::table("ba_users")  
    ->where("bin", $bin ) 
    ->whereIn("category", array("1", '10' ) ) 
    ->select("profile_id")
    ->get();
     $staff_ids = array();
     foreach($staff_rows as $staff_row)
     {
        $staff_ids[] = $staff_row->profile_id;
     }
 
     $staff_availability="close";

    if(  !in_array( $staff_id,  $staff_ids ) )
    {
      return response()->json(array(
        "detailed_msg" => 'Selected staff does not work under the business!', 
        "status_code" => 999,
        "message" => "failure"));
    }
    else
    {

      $staff_availability = DB::table("ba_service_days")
       ->where("staff_id",  $staff_id )
       ->where("bin",  $bin )
       ->where("slot_number", $slot_no)
       ->whereDate("service_date", $date)
       ->pluck("status")->first();
   } 
    
    $data= ["message" => "success", "status_code" =>  1091 , 
    'detailed_msg' => 'Staff availability fetched.' ,  'is_available' => $staff_availability]; 
    return json_encode( $data);
  
  }



}
