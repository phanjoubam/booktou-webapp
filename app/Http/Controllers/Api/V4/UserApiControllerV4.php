<?php

namespace App\Http\Controllers\Api\V4;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Http\Controllers\Controller;


 
use App\User;
use App\Business;
 
use App\Traits\Utilities;
use App\BusinessHour;
use App\ServiceDuration;
use App\BusinessService;
use App\RewardLogModel; 
use App\CustomerProfileModel;
use App\UserActivityLogModel;


class UserApiControllerV4 extends Controller
{
	use Utilities;
	
	public function registerCheck(Request $request)
   {
   		$bin = 0;

	   if(  $request->phone == ""  || $request->category == ""  )
	   {
		   $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	     return json_encode($data);
	   }

   	
   	if(  strlen($request->phone) < 10 )
   	{
	   	$data= ["message" => "failure", "status_code" =>  1000 , 'detailed_msg' => 'You need to specify a valid phone number!'  ];
	   	return json_encode($data);
		}

	   //check User data in record 
	   $user_info = User::where('phone', $request->phone)
	   ->where("category",$request->category) 
	   ->first();

	   	if(  !isset($user_info) && $request->category ==  0 )

	   	{

			//saving profile
	   		$profile = new  CustomerProfileModel;
	   		$profile->fname = " "; 
	   		$profile->mname = " "; 
	   		$profile->lname = " ";  
	   		$profile->fullname = $request->phone ; 
	   		$profile->phone = $request->phone; 
	   		$profile->referral_code = $request->phone;  
	   		$save = $profile->save();	

	   		if(	 $save  )
	   		{
	   			DB::table("ba_profile")
	   			->where('id', $profile->id )
	   			->update(['referral_code' => 'BU' . $profile->id ]);	     
	   		}

			//generate a confirmation OTP  
	   		$otp = mt_rand(232323,999999);
	   		$otpdate =  date('Y-m-d H:i:s');
	   		$currentDate = strtotime($otpdate);
	   		$futureDate = $currentDate+(60*5);
	   		$otp_expiresOn = date("Y-m-d H:i:s", $futureDate);


	   		$newuser = new User; 
	   		$newuser->profile_id = $profile->id;
	   		$newuser->phone = $request->phone;
	   		$newuser->password =  Hash::make( time() );
	   		$newuser->category = $request->category;
	   		$newuser->otp = 	$otp;

	   		if( strcasecmp($request->appSource, "booktou") != 0 && strcasecmp($request->appSource, "booktoux") != 0 )
	   		{
	   			$newuser->app_source =   "booktou" ;
	   		}
	   		else
	   		{
	   			$newuser->app_source = ($request->appSource == "" ? "booktou" : $request->appSource);
	   		}
	   		if( $request->referralCode != "" ) 
	   		{
	   			$newuser->invite_referral_code =  $request->referralCode;
	   		}

	   		$newuser->category = $request->category; 

	   		$save =$newuser->save(); 


	   		if(	 $save  )
	   		{
	   			if($request->category ==  0)
	   			{
			//compose message 
	   				$message_row = DB::table("ba_sms_templates")
	   				->where("name", "otp_generate")
	   				->first();

	   				if(isset($message_row))
	   				{
	   					$otp_msg =  urlencode( str_replace(  "###", $otp,  $message_row->message ) );
	   					$this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id ,   array($newuser->phone)  ,  $otp_msg  ); 
	   				} 
	   			}
	   		}


		   		$uid = $newuser->id; 
		   		$message = "success";
		   		$err_code = 1057;
		   		$err_msg = "Registration successfully.";
		   		$user_status = "new_user";

			   	$get_user = DB::table('ba_profile')
			    ->select('id as memberId','fullname as name','locality','phone','email','profile_photo as  profilePhoto')
			    ->where('id', function ($query) use ( $uid ){$query->select("profile_id")
		        ->from("ba_users")
	    	    ->where("id", $uid);})
			    ->first();

			    if($get_user->profilePhoto == null)
			     {

			      	$get_user->profilePhoto = URL::to('/public/assets/image/no-image.jpg' );
			     }

	   	}	

	   	else if(  isset($user_info) && $user_info->category ==  0 )
	    {
	    	//generate a confirmation OTP  
				$otp = mt_rand(232323,999999);
				$otpdate =  date('Y-m-d H:i:s');
				$currentDate = strtotime($otpdate);
				$futureDate = $currentDate+(60*5);
				$otp_expiresOn = date("Y-m-d H:i:s", $futureDate);

				$save_otp = User::find($user_info->id);
				$save_otp->otp = $otp;
				$save_otp->save();


				//compose message 
				$message_row = DB::table("ba_sms_templates")
				->where("name", "otp_generate")
				->first();
				
				if(isset($message_row))
				{
					$otp_msg =  urlencode( str_replace(  "###", $otp,  $message_row->message ) );
					$this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id ,   array($user_info->phone)  ,  $otp_msg  ); 
				}


			    $uid = $user_info->id; 
		   		$message = "success";
		   		$err_code = 1057;
		   		$err_msg = "user_exist";
		   		$user_status = "registered";	 

			  $get_user = DB::table('ba_profile')
		      ->select('id as memberId','fullname as name','locality','phone','email','profile_photo as  profilePhoto')
		      ->where('id', function ($query) use ( $uid ){$query->select("profile_id")
		      ->from("ba_users")
		      ->where("id", $uid);})
		      ->first();

		      if($get_user->profilePhoto == null)
		      {

		      	$get_user->profilePhoto = URL::to('/public/assets/image/no-image.jpg' );
		      }

	    }


	    else 
		{
			$uid = 0;
			$message = "failure";
			$err_code = 1058;
			$err_msg = "Registration failed!";
			$user_status = "error_signup";
			$get_user = "{}";
		}
	    
		 

		$data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg , 'user_status' =>$user_status,
		'userId' => $uid, 
		'bin' => $bin, 'result'=> $get_user]; 
	    return json_encode( $data);
	
  }


	  public function login(Request $request)
	  {
		  	if( $request->phone == "" || $request->category == "")
		  	{
		  		$data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		  		return json_encode($data);
		  	}


		  	$user = User::where("phone", $request->phone )
		  	->where("category",  $request->category )
		  	->where("status",  '<>' , 100 )
		  	->first();
		
		  	if(!isset($user))
		  	{
		  		$data= ["message" => "failure",  "status_code" =>  997 ,
		  		'detailed_msg' => 'User is not registered with us.',
		  		"user_status" => "not-found", ];
		  		return json_encode( $data); 
		  	}

		  	$data['memberId'] = $user->profile_id;

		  	if(Hash::check( $request->password , $user->password ) || $user->otp == $request->otp ) 
		  	{
		  		$message = "success";
		  		$err_code = 1059;
		  		$err_msg =  'Login successful!';
		  		$data['profilePicture'] = URL::to('/public/assets/image/no-image.jpg' );
		  		$data['coverPhoto'] =URL::to('/public/assets/image/no-image.jpg' );



			if(  $user->category == 0 ) //customer 
			{
			$fullname = "";
			$cust_profile = CustomerProfileModel::find(  $user->profile_id  ) ;  

			if( !isset($cust_profile))
			{
				$setupComplete = "no";    
			}
			else 
			{
				if( $cust_profile->fname == "" ||  $cust_profile->lname == "" || $cust_profile->locality == ""  || 
					$cust_profile->landmark== "" ||  $cust_profile->city  == "" || $cust_profile->state == "" ||  
					$cust_profile->pin_code  == "" ||   $cust_profile->phone  == ""   ) 
				{

					$setupComplete = "no";    
				}
				else 
				{ 
					$setupComplete = "yes"; 
				}

				if($cust_profile->profile_photo!=null)
				{
					$data['profilePicture'] = URL::to('/public/assets/image/member/') . $cust_profile->profile_photo ;
				} 
				if($cust_profile->cover_photo!=null)
				{
					$data['coverPhoto'] = URL::to('/public/assets/image/member/') . $cust_profile->cover_photo ;
				}  
			}


			$data['customerSetup'] = $setupComplete ;
			$data["detailed_msg"] =  $err_msg   ; 
			$data['isVerified'] ="yes";
			$data['name'] =  $cust_profile->fullname; 
		}
		}

		else 
		{
			$message = "failure";
			$err_code = 1060;
			$err_msg =  'Login failed due to wrong password!'  ; 
			$data["message"] = $message;
			$data["status_code"] = $err_code ;
			$data["detailed_msg"] =  $err_msg   ;  
		}

		$data["phone"] = $request->phone ;

		$result= ["message" => $message, "status_code" =>  1000 , 'results' =>$data]; 	
		return json_encode( $result); 
	}	




	 public function setPassword(Request $request )
   {

      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
        return json_encode($data);
      }

      if(  $request->newPassword == ""  ||  $request->otp == "" )
      {
          $data= [ 
            "message" => "failure", 
            "status_code" =>  999 , 
            'detailed_msg ' => 'Missing data to perform action.' 
          ];
            return json_encode($data);
      }


      $userInfo = User::find( $request->userId );

      if( !isset($userInfo))
      {
          $data = [ "message" => "failure",  'status_code' => 900, 'detailed_msg' => 'No matching user found!'  ];
          return json_encode($data);
      }

        if($request->otp == $userInfo->otp ) 
        {

          $userInfo->password =  Hash::make( $request->newPassword ); 
          $userInfo->save(); 
          $data = [ "message" => "success",  'status_code' => 902, 'detailed_msg' => 'Password set successfully!'  ];
         
        }
        else
        {
           $data = [ "message" => "failure",  'status_code' => 993, 'detailed_msg' =>  'OTP mismatched' ]; 
        }
          
        
       return json_encode($data);
    
  }
  
  
  	public function updatePrimaryAddress(Request $request )
   {

      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
        return json_encode($data);
      }
	  
	  if(  $request->version != "v4" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'Unauthorized access!'  ];
        return json_encode($data);
      }
	  
	  

		$member_info = CustomerProfileModel::find($user_info->member_id);
		if( !isset($member_info))
        {
        	$member_info = new  CustomerProfileModel;   
        }
	

        $name = $request->fullName;
        $names = explode(" " , $name);

        $member_info->fname = $names[0]; 
        $member_info->mname = ( count($names) >  1 ? $names[1] : " " ); 
        $member_info->lname = ( count($names) >  2 ? $names[2] : " " );  
        $member_info->fullname= $name;
 
			if( $request->locality != "" )
			{
				$member_info->locality = $request->locality ;
			}
			if( $request->landmark != "" )
			{
				$member_info->landmark = $request->landmark ;
			}
			if( $request->city != "" )
			{
				$member_info->city = $request->city ;
			}
			if( $request->state != "")
			{
				$member_info->state = $request->state ;
			}
			if( $request->mpin != "")
			{
				$member_info->pin_code  = $request->mpin ;
			}
			
			$member_info->latitude = ( $request->latitude  != "" ? $request->latitude : 0 );  
			$member_info->longitude = ( $request->longitude  != "" ? $request->longitude : 0 );  


			$save = $member_info->save();
          
        
			if($save)
            {
            	$data= ["message" => "success", "status_code" =>  1071 ,  'detailed_msg' => 'Your primary address successfully updated.']; 
            }

        else{
            $data= ["message" => "success", "status_code" =>  1072 ,  'detailed_msg' => 'Your primary address update failed.'];
        }
        return json_encode( $data);  
    
  }

}
