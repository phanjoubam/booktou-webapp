<?php

namespace App\Http\Controllers\Api\V4;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;



use App\Traits\Utilities;
use App\MerchamtComplaintModel;
use App\Business;

use App\User;


class MerchantComplaintsControllerV4 extends Controller
{  
   use Utilities;

   public function otpConfirmationForMerchant(Request $request)
   {

   } 




   public function merchantComplaintsOTPGeneration(Request $request)
   {

        if( strcasecmp($request->version, 'v4') != 0)
        {

            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'Invalid access.'  ];
            return json_encode($data);
        }


        $phone =  $request->phone;

        if($request->phone == ""  )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'Missing data to perform action.'  ];
            return json_encode($data);
        }

        $otp = mt_rand(111111,999999);

        $user_info = User::where("phone", $phone)->where("category",1)->first();
        
        if(!isset($user_info))
        {
            $data= ["message" => "failure", "status_code" =>  906 , 
            'detailed_msg' => 'No business found with the specified phone'  ];
            return json_encode($data);
        }

        $user_info->otp = $otp;
        $user_info->otp_expires = date('Y-m-d H:i:s',strtotime('+5 minutes'));
        $user_info->save();

        //send otp via sms
        $message_row = DB::table("ba_sms_templates")
                  ->where("name", "otp_generate")
                  ->first();
        if(isset($message_row))
                  {
                     $otp_msg =  urlencode( str_replace(  "###", $otp,  $message_row->message ) );
                     $this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id ,   array($user_info->phone)  ,  $otp_msg  ); 
                  }          


        $data= ["message" => "success", "status_code" =>  7001 ,  'detailed_msg' => 'OTP for complaint posting send!'  ];
        return json_encode($data);


   } 


}
