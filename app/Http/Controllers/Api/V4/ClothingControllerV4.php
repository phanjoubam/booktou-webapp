<?php

namespace App\Http\Controllers\Api\V4; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException; 

use App\Http\Controllers\Controller;
use App\Jobs\SendEmaOrderSms;

 
use App\ShoppingCartTemporary;  
use App\ShoppingItemAddonModel;
use App\Business;  
use App\ProductModel; 
use App\ProductVariantModel; 
use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\CustomerProfileModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\ProductWishListModel;
use App\OrderSequencerModel;
use App\Traits\Utilities;
use App\User;
use App\MerchantOrderSequencer;
use App\ShoppingOfferBasket;

class ClothingControllerV4 extends Controller
{
	use Utilities;


	protected function viewClothingProductDetails(Request $request)
    {
      
      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
         $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
         return json_encode($data);
      }
      
      
      if ( $request->productCode  == ""    )
      {
        $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
        return json_encode($data);
      }
 

      $product_info  = DB::table("ba_products")
      ->where("ba_products.pr_code", $request->productCode ) 
      ->select("ba_products.id as productId",  
	           "ba_products.bin",
	           "ba_products.pr_code as productCode", 
	           "ba_products.pr_name as productName",
	           "ba_products.category as categoryName", 
	           "ba_products.gst_code as GST", 
	           "ba_products.mfr_id as manufacturer",
	           "ba_products.description",
	           "ba_products.category",
	           "unit_value as unitValue",
	           "unit_name as unitName",   
	           "allow_gift_wrap as allowedGiftWrap",
	           "gift_wrap_charge as giftWrapCharge" ,
	           "ba_products.food_type as foodType", 
              "ba_products.free_text as freeText", 
              "ba_products.addon_available as addon", 
               DB::Raw("'[]' addOnList"),
              "ba_products.photos as images"  , 
              "is_bestseller as bestSeller",
              "ba_products.tags", 
              "ba_products.status" ,
			  "allow_enquiry as enquiryFlag",
			   DB::Raw("'na' productUrl"),
	           DB::Raw("'[]' sizeColorMap") ,  
	           DB::Raw("'[]' subDetails"))
      ->first() ;

       if( !isset( $product_info  ))
       {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No matching clothing product found!'  ];
         return json_encode($data);
       }
 		

 	  $business = DB::table('ba_business')
	  ->join('ba_business_category','ba_business_category.name','=','ba_business.category')
	  ->where('ba_business.id',$product_info->bin)
	  ->select("ba_business.*",
	  		   "ba_business_category.display_type")
 	  ->first();

 	  //retreiving business, business category and product related information
 	  $biz_category = $business->category;  
 	  $bin = $business->id;
 	  $layout = $business->display_type;
 	  $name = $business->name;

 	  $pr_name = $product_info->productName;
 	  $manufacturer=$product_info->manufacturer;
	  $description=$product_info->description;
	  $product_category=$product_info->category;
	  $food_type=$product_info->foodType;
	  $free_text=$product_info->freeText;

	  $addon_available = $product_info->addon;
	  $photos =$product_info->images; 
	  $is_bestseller =$product_info->bestSeller;
	  $tags =$product_info->tags;
	  $status =$product_info->status;
	  $allow_enquiry =$product_info->enquiryFlag;
	  $prod_url = $product_info->productUrl;

	  //ends here

      $product_subdetails  = DB::table("ba_product_variant")
      ->where("pr_code", $product_info->productCode) 
      ->select( "id as productSubId",
      		DB::Raw("'$bin' bin "),
      		DB::Raw("'$name' name "),
      		DB::Raw("'$biz_category' businessCategory "),
      		DB::Raw("'$layout' uiLayoutMode "),
       	   "pr_code as productCode", 
       	   DB::Raw("'$pr_name' productName "), 
       	   DB::Raw("'NULL' GST "),
       	   DB::Raw("'$manufacturer' manufacturer "),
       	    DB::Raw("'$description' description "),

           "bar_code as barCode",
           "stock_inhand as stock" ,
           "max_order as maximumOrderAllowed", 
           "unit_price as unitPrice" ,
           "unit_name as unitName" ,
		   "pack_type as packType" ,
		   "unit_value as unitValue" ,
		   "discount", 
           "discountPc",
           "actual_price as actualPrice",
           "packaging as packagingCharge",

           DB::Raw("'$product_category' productCategory"),
           DB::Raw("'$food_type' foodType"),
           DB::Raw("'$free_text' freeText"),
		   DB::Raw("'$addon_available' addon"), 
           DB::Raw("'[]' addOnList"),
           DB::Raw("'$photos' images"),
           DB::Raw("'$is_bestseller' bestSeller"),
           DB::Raw("'$tags' tags"),    
           DB::Raw("'$status' status"),    
           DB::Raw("'$allow_enquiry' enquiryFlag"),    
           DB::Raw("'$prod_url' productUrl"),

           "size",
           "color",
           "gender", 
           "pattern", 
           "initial_stock as initialStock",
           "min_required as minimumStock" ,  
           DB::Raw("'[]' productSpecs")
            )
       ->get() ;

       
   
	   $sizes = $subids =  array();

	   foreach( $product_subdetails as $item )
	   {
	      $sizes[] =  $item->size;
	      $subids[] = $item->productSubId;
	   }

	   $sizes = array_unique($sizes);
	   $subids[] =0;


	  $images  = DB::table("ba_product_photos")
	       ->where("pr_code", $product_info->productCode ) 
	       ->whereIn("sub_id", $subids)  
	       ->select( "sub_id", "image_url_sm" )
	       ->get() ;
	 

	   $sizeColorMap  =array();
	   $i=0;
	   foreach( $sizes as $item )
	   {
	     $sizeColorMap[$i]['size'] = $item ;
	     $color =  array();
	     foreach( $product_subdetails as $subitem )
	     {
	       if($item == $subitem->size )
	       {
	         $color[] =  $subitem->color;
	       } 
	     }

	     $sizeColorMap[$i]['color'] = $color ;
	     $i++;
	     
	   }
	   
	   $product_info->sizeColorMap = $sizeColorMap; 

	   $techSpecs  = DB::table("ba_product_specs")
	       ->where("pr_code", $product_info->productCode ) 
	       ->whereIn("sub_id", $subids)  
	       ->select( "sub_id as productSubId", "spec_title as title", "spec_value as details" )
	       ->get() ;
   
	   foreach( $product_subdetails as $item )
	   {
	        $allspecs =  $all_photos= $add_onList = array();
	        foreach($images as $file )
	          {
	            if( $item->productSubId == $file->sub_id)
	            {
	              $all_photos[]   = $file->image_url_sm ; 
	            } 
	          } 

	        if(count($all_photos) ==   0 )
	        {  
	          $all_photos[]   =  config('app.app_cdn')  . "/assets/image/no-image.jpg";
	        }
	        
	        $item->images = $all_photos;
	        $item->addOnList = $add_onList;
	        
	        
	        foreach($techSpecs  as $spec)
	        {
	          if($spec->productSubId ==   $item->productSubId)
	          {
	            $allspecs[] = $spec;
	          }
	        } 
	        
	        $item->productSpecs =  $allspecs ; 

	    }
	    
	    $product_info->subDetails  = $product_subdetails ; 
	    $product_info->images = $all_photos;
	    $product_info->addOnList = $add_onList;
 
    
       $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Product details fetched!' , 
       'results' => $product_info];

       return json_encode( $data);
    }



    protected function addItemToCart(Request $request)
    {
    	$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
        if( $user_info->category  != 0  )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to customer only.'  ];
            return json_encode($data);
        }
    
    if($request->prid == "" ||  $request->bin  == ""  || 
         $request->qty == ""  || $request->action == "" || $request->subid=="")
        {
           $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
           return json_encode($data);
        }
        
    
      $cart_item = ShoppingCartTemporary::where( "bin",  $request->bin )
        ->where("prid", $request->prid)
        ->where("subid", $request->subid)
        ->where("member_id", $user_info->member_id)
        ->first();

        if( !isset(  $cart_item )  )
        {
            $cart_item = new ShoppingCartTemporary;
            $cart_item->member_id = $user_info->member_id ;
            $cart_item->session_id =  null  ;
            $cart_item->prid = $request->prid;
            $cart_item->subid = $request->subid;
            $cart_item->bin = $request->bin;
        }
      
        //cart item count 
        $cart_count = ShoppingCartTemporary::where( "member_id",  $user_info->member_id ) 
        ->count();
        
        if($request->action == "+")
        {
          $cart_item->qty += $request->qty; 
        }else if($request->action == "-")
        {
          if ($cart_item->qty<=0) {
            $data= ["message" => "failure", "status_code" =>  904 , 'detailed_msg' => 'No item to remove from cart.'  ];
           return json_encode($data);
          }

          $cart_item->qty -= $request->qty; 


        }
        else 
        {
          $cart_item->qty = $request->qty; 
        }
    
        
        //calculating cost
        $product = DB::table("ba_products") 
        ->where("id", $request->prid ) 
        ->select("bin", "actual_price", "photos", "pr_name")
        ->first() ;
    
    	$cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path');  
        $cms_base_url = config('app.app_cdn');  
        $cms_base_path = $cdn_path;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg"; 
    
        if(isset($product))
        {
      		$cart_item->cost  =  $cart_item->qty * $product->actual_price ;  
    	} 
 
        $save = $cart_item->save();  
  
        if($save)
        {
          $data= ["message" => "success", "status_code" =>  6001 , 
      			  'detailed_msg' => 'Item added to cart.',    'cartCount' => $cart_count ];
        }
        else
        {
          $data= ["message" => "failure", "status_code" =>  6002 , 
      			  'detailed_msg' =>  "Item could not be added to cart." , 'cartCount' => 0 ];
        } 
 
        return json_encode( $data); 
     }

  		

  	protected function viewShoppingCartItems(Request $request)
    {
    
    	$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
        if( $request->bin  == ""  )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
      		return json_encode($data);
        }
    	
	    $mid = $user_info->member_id;
	    $bin = $request->bin;
	    
	    $business = DB::table("ba_business") 
	    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")
	    ->where( "ba_business.id",   $bin  )
	    ->select("ba_business.id as bin", 
	    "ba_business.category as bizCategory", 
	    "ba_business.name",
	    DB::Raw("'0' cartItemCount"),
	    DB::Raw("'0' cartTotalAmount"),
	    "ba_business_category.main_type as type" )  
	    ->first();
	    
    if(!isset($business))
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
      		return json_encode($data);
    	}
    
    if(strcasecmp($business->type,"shopping") == 0 )
    {
      $cart_items = DB::table("ba_temp_cart") 
      ->where( "member_id", $mid )
      ->where( "ba_temp_cart.bin",  $bin   )
      ->get();
      
      $prids = array();
      
      foreach($cart_items as $item)
      {
        $prids[] = $item->prid;
      }
      $prids[] = "0";
      
      $all_products = DB::table("ba_products")   
      ->whereIn("ba_products.id", $prids)
      ->join("ba_product_variant","ba_products.pr_code","=","ba_product_variant.pr_code")  
      ->select(
      "ba_products.id as productId",
      "ba_product_variant.id as productSubId",
      "ba_products.id as name",
      "ba_products.id as productId", 
      "ba_products.bin", 
      "ba_products.pr_code as productCode", 
      "ba_products.pr_name as productName", 
      "ba_products.description", 
     // DB::Raw("'1'  quantity"),
      "ba_product_variant.stock_inhand as stockInHand",
      "allow_enquiry as enquiryFlag",
      "ba_product_variant.pack_type as packType", 
      "ba_product_variant.max_order as maxOrderAllowed", 
      "ba_product_variant.unit_price as unitPrice", 
      "ba_product_variant.unit_value as unitValue", 
      "ba_product_variant.unit_name as unitName",
      "ba_product_variant.actual_price as actualPrice",
      "ba_product_variant.discount",
      "ba_product_variant.discountPc",
      "ba_product_variant.cgst_pc as cgstPc",
      "ba_product_variant.sgst_pc as sgstPc",
      "ba_products.category as productCategory",
      "ba_products.food_type as foodType", 
      "ba_products.free_text as freeText",
      DB::Raw("'none'  addon"),
      "ba_products.is_bestseller as bestseller",
      "ba_products.packaging as packagingCharge",
      "ba_products.food_type as foodType",  
      DB::Raw("'none'  businessCategory"),
      DB::Raw("'none'  extraRequestText"),
      "ba_products.photos as images",
      )
      ->get();
      
      
      $prids = array();
      $pr_code = array();
      $total = $packaging_charge = $delivery_charge = $discount = 0.00;
      foreach($all_products as $item)
      {
        foreach($cart_items as $cart)
        {
          if($cart->prid == $item->productId )
          {
            $item->quantity = $cart->qty;
            break;
          }
        }
        
        $total += ( $item->unitPrice * $item->quantity) * 1 ;
        $packaging_charge += $item->packagingCharge * 1;  
        $delivery_charge = 49.00;
        if($item->discount > 0)
        {
          $discount = ( $item->discount * $item->quantity )*1;
        }
        else 
        {
          $discount  = ( $item->discountPc * $item->quantity * $item->unitPrice )/ 100 ;
        }
         
        
        //image 
        $all_photos = array();  
        $files =  explode(",",  $item->images ); 
        if(count($files) > 0 )
        {
           
          $folder_path =  config('app.app_cdn_path')   . "/assets/image/store/bin_" .  $item->bin .    "/";
          $files=array_filter($files); 
            foreach($files as $file )
            {

            $source =  $folder_path . $file ;   
            if(file_exists( $source  ))
            {

              $pathinfo = pathinfo( $source  ); 
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];  
              $destination  =  $folder_path . $new_file_name ; 
              if( file_exists( $destination ) )  
              {
                $all_photos[]  = config('app.app_cdn')  .   "/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
              }
              else 
              {
              if(filesize(  $source ) > 307200) 
              {

                $this->chunkImage($source, $folder_path , $destination );  
                $all_photos[]  = config('app.app_cdn')  .  "/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;  
              }
              else 
              {
                $all_photos[]  =  config('app.app_cdn')  .  "/assets/image/store/bin_" . $item->bin  .  "/" .    $file  ;
              }
              } 
            }
              else
              {
                $all_photos[]   = config('app.app_cdn') .  "/assets/image/no-image.jpg" ;
              }
             
          }
        }

        $item->images = count($all_photos) > 0 ? array($all_photos[0]) : array("https://alpha.booktou.in/public/assets/image/no-image.jpg" ); 

        $pr_code[] = $item->productCode;
      }
      
      $business = Business::find($bin);

      $subDetails = array();

      // $product_subdetails = DB::table('ba_product_variant')
      // ->whereIn('pr_code',$pr_code)
      // ->select("id as subProductId",
      // 		   "pr_code as productCode",
      // 		   "bar_code as barCode",
      // 		   "initial_stock as Stock",
      // 		   "stock_inhand as availableStock",
      // 		   "min_required as minRequired",
      // 		   "max_order as maxOrderAllowed",
      // 		   "pack_type as packType",
      // 		   "unit_price as unitPrice",
      // 		   "actual_price as actualPrice",
      // 		   "discount as disCount",
      // 		   "discountPc",
      // 		   "packaging",
      // 		   "size",
      // 		   "gender",
      // 		   "color",
      // 		   "unit_value as unitValue",
      // 		   "unit_name as unitName",
      // 		   "pattern",
      // 		   "status",
      // 		   "is_bestseller as bestSeller",
      // 		   "sub_tags as subTags",
      // 		   "cgst_pc as cgstPc",
      // 		   "sgst_pc as sgstPc"
      // )
      // ->get();

      
      
      $result = array( 
      "bin" => $bin ,  
      "name" =>$business->name , 
      "totalAmount" =>  $total + $packaging_charge + $delivery_charge - $discount,
      "packagingCharge" => $packaging_charge ,
      "deliveryCharge" => $delivery_charge ,
      "discount" => $discount , 
      'items' => $all_products
      );
         
      
      $data= ["message" => "success", "status_code" =>  6003 ,  'detailed_msg' =>  "Shopping cart items fetched." , 'results' => $result ];
    
    }
    else  if(strcasecmp($business->type,"appointment") == 0 )
    {
      $cart_items = DB::table("ba_temp_cart") 
      ->where( "member_id", $mid )
      ->where( "ba_temp_cart.bin",  $bin   )
      
      ->get();
      
      $prids = array();
      
      foreach($cart_items as $item)
      {
        $prids[] = $item->prid;
      }
      $prids[] = 0;
      
       
 
 
      $service_products  = DB::table("ba_service_products")   
      ->whereIn("ba_service_products.id", $prids)  
      ->select(
      "id as serviceProductId",
      "srv_name as serviceName", 
      "bin",  
      "service_type as serviceType", 
      "srv_details as serviceDetails", 
      "srv_status as canBeBook",  
      "pricing",
      "actual_price as actualPrice",
      "discount", 
      DB::Raw("'1' quantity"),
      "valid_start_date as validStartDate", 
      "valid_end_date as validEndDate",
      "photos"
      )
      ->get();
       
      $total = $packaging_charge =   $discount = 0.00;
      foreach($service_products as $item)
      {
        foreach($cart_items as $cart)
        {
          if($cart->prid == $item->serviceProductId  )
          {
            $item->quantity = $cart->qty;
            break;
          }
        }
        
        $total += ( $item->actualPrice * $item->quantity) * 1 ;  
        if($item->discount > 0)
        {
          $discount = ( $item->discount * $item->quantity )*1;
        } 
         
        
        //image 
        $all_photos = array();  
        $files =  explode(",",  $item->photos ); 
        if(count($files) > 0 )
        {
           
          $folder_path =  config('app.app_cdn_path')   . "/assets/image/store/bin_" .  $item->bin .    "/";
          $files=array_filter($files); 
            foreach($files as $file )
            {

            $source =  $folder_path . $file ;   
            if(file_exists( $source  ))
            {

              $pathinfo = pathinfo( $source  ); 
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];  
              $destination  =  $folder_path . $new_file_name ; 
              if( file_exists( $destination ) )  
              {
                $all_photos[]  = config('app.app_cdn')  .   "/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
              }
              else 
              {
              if(filesize(  $source ) > 307200) 
              {

                $this->chunkImage($source, $folder_path , $destination );  
                $all_photos[]  = config('app.app_cdn')  .  "/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;  
              }
              else 
              {
                $all_photos[]  =  config('app.app_cdn')  .  "/assets/image/store/bin_" . $item->bin  .  "/" .    $file  ;
              }
              } 
            }
              else
              {
                $all_photos[]   = config('app.app_cdn') .  "/assets/image/no-image.jpg" ;
              }
             
          }
        }

        $item->photos = count($all_photos) > 0 ? array($all_photos[0]) : array("https://alpha.booktou.in/public/assets/image/no-image.jpg" ); 

        $item->validStartDate = $item->validStartDate == null ? "" : date('d-m-Y', strtotime($item->validStartDate));
        $item->validEndDate = $item->validEndDate == null ? "" : date('d-m-Y', strtotime($item->validEndDate));
        
 
      
      } 
       
       
      
      $result = array( 
      "bin" => $bin ,  
      "name" =>$business->name ,   
      'items' => $service_products
      );
      
      
      $data= ["message" => "success", "status_code" =>  6003 ,  'detailed_msg' =>  "Appointment cart items fetched." , 'results' => $result ];
    }
    else 
    {
      $data= ["message" => "failure", "status_code" =>  995 , 'detailed_msg' => 'Mismatch business category.'  ];
      return json_encode($data);
    }
     
        return json_encode( $data); 
     }






}