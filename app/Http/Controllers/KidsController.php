<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

use App\User; 
use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;
use App\BusinessPaymentModel;
use App\Franchise;
use App\BusinessCommissionModel;
use App\DailyDepositModel;
use App\FeedbackMerchant;
use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\MonthlyAgentsOrdersExport;
use App\Exports\DailyAgentsOrdersExport;
use App\Exports\DailySalesOrderExport;
use App\Exports\MonthlyCommissionExport;
use App\Traits\Utilities; 
use PDF;
use App\DepositTargetModel;
use App\ProductVariantModel;
use App\ImportProductFileModel;
use App\SectionTypeModel;
use App\SectionDetailsModel;
use App\AppSectionModel;
use App\AppSectionDetailsModel;
use App\PosSubscription;
use App\SelfPickUpModel;
use App\PosPayment;
use App\BrandModel;
use App\ServiceCategoryModel;
use App\AgentEarningModel;
use App\AgentSalaryAdvanceModel;
use App\PopularProductsModel;
use App\AddDiscountProductsModel;
use App\ServiceProductModel;
use App\PackageMetaModel;
use App\ServicePackageSpecModel;
use App\SponsoredPackageModel;
use App\LandingScreenSectionModel;
use App\LandingScreenSectionContenModel;
use App\MetaKeyModel;

class KidsController extends Controller
{
  use Utilities; 
  protected function appLandingScreenSectionsForKids(Request $request)
 {
  $bookables=DB::table("ba_bookable_category")
  ->where("sub_module","ECOM")
  ->where("module_code","ECOM")
  ->get();
  $sections =DB::table("app_landing_screen_section")
  ->where('main_module','SHOPPING')
  ->where('sub_module','ECOM')
  ->get();
  return view('admin.car_services.kids_add_landing_section')->with(  array( 'sections' => $sections,"bookables"=>$bookables));
  
 } 

 protected function addKidsLandingScreenSections(Request $request)
 {
        $cdn_url = config('app.app_cdn'); 
        $cdn_path =  config('app.app_cdn_path'); 
        $folder_name = 'icon_'. $request->main_module;  
        $folder_path =  $cdn_path."/assets/upload/". $folder_name; 
        $section = new LandingScreenSectionModel;
        if ($request->hasFile('photo')) {
          if( !File::isDirectory($folder_path))
          {
            File::makeDirectory( $folder_path, 0777, true, true);
          }  
          $file = $request->photo;
          $originalname = $file->getClientOriginalName(); 
          $extension = $file->extension( );
          $filename = "icon_". time()  . ".{$extension}";
          $file->storeAs('upload',  $filename );
          $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
          $file_names[] =  $filename ;
          $folder_url  = $cdn_url.'/assets/upload/' . $folder_name ."/".$filename ;
          $section->leading_icon = $folder_url;
        }

        $section->main_module = $request->main_module;
        $section->sub_module = $request->sub_module;
        $section->heading_text = $request->heading_text;
        $section->layout_type = $request->layout_type;
        $section->row = $request->row;
        $section->col = $request->col;
        $section->horizontal_scroll = $request->horizontal_scroll;
        $section->display_sequence = $request->display_sequence;
        $section->show_in_app = $request->show_in_app;

        $save = $section->save();
        if($save)
        {
        return redirect('/admin/app-landing-screen-sections-for-kids')->with('err_msg',' save successfully!');
        }
      return redirect('/admin/app-landing-screen-sections-for-kids'); 
   }



    protected function editKidsLandingScreenSectionsIcon(Request $request)
 {

   if ($request->icon=="") 
    {
        return redirect("/admin/app-landing-screen-sections-for-kids")->with("err_msg","field missing!");
    }
    $cdn_url = config('app.app_cdn'); 
   $cdn_path =  config('app.app_cdn_path'); 

   $folder_name = 'icon_'. $request->main_module;  
   $folder_path =  $cdn_path."/assets/upload/". $folder_name; 

    $icon = $request->icon;  
    $section = LandingScreenSectionModel::find($icon);
    if ($request->hasFile('photo')) {
      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0777, true, true);
      }  
      $file = $request->photo;
      $originalname = $file->getClientOriginalName(); 
      $extension = $file->extension( );
      $filename = "icon_". time()  . ".{$extension}";
      $file->storeAs('upload',  $filename );
      $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
      $file_names[] =  $filename ;
      $folder_url  = $cdn_url.'/assets/upload/' . $folder_name ."/".$filename ;
      $section->leading_icon = $folder_url;
    }
    $save = $section->save();
    if ($save) {
        return redirect("/admin/app-landing-screen-sections-for-kids")
        ->with("err_msg","record  successfully!");
    }else{
        return redirect("/admin/app-landing-screen-sections-for-kids")
        ->with("err_msg","failed!");
    }
   }


   protected function editKidsLandingScreenSections(Request $request)

    {
        
        if ($request->key=="") 
        {
            return redirect("/admin/app-landing-screen-sections-for-kids")->with("err_msg","field missing!");
        }
        $key = $request->key;  
        $section = LandingScreenSectionModel::find($key);
        $section->heading_text = $request->heading_text;
        $section->layout_type = $request->layout_type;
        $section->row = $request->row;
        $section->col = $request->col;
        $section->horizontal_scroll = $request->horizontal_scroll;
        $section->display_sequence = $request->display_sequence;
        $section->show_in_app = $request->show_in_app;
        $save = $section->save();
        if ($save) {
            return redirect("/admin/app-landing-screen-sections-for-kids")
            ->with("err_msg","record  successfully!");
        }else{
            return redirect("/admin/app-landing-screen-sections-for-kids")
            ->with("err_msg","failed!");
        }
    }

    protected function deleteKidsLandingScreenSections(Request $request)
    {

        if ($request->keys=="") {
            return redirect("/admin/app-landing-screen-sections-for-kids")
            ->with("err_msg","field missing!");
        }

        $key = $request->keys;

        $delete = DB::table("app_landing_screen_section")
        ->where("id",$key) 
        ->delete();

        if ($delete) {
            return redirect("/admin/app-landing-screen-sections-for-kids")
            ->with("err_msg","record deleted!");
        }
        else{
            return redirect("/admin/app-landing-screen-sections-for-kids")
            ->with("err_msg","failed!");
        }

    }
}
