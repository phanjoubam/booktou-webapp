<?php

namespace App\Http\Controllers\Erp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

 
use App\Business; 
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel; 
use App\Imports\ProductImport;
use App\OrderSequencerModel;
use App\Traits\Utilities;
use App\PickAndDropRequestModel;

class PDFController extends Controller
{
    use Utilities;


     function pdf()
    {
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_customer_data_to_html());
     return $pdf->stream();
    }


     function pdfSalesDetails(Request $request)
    {


    	$orderno = $request->order_no;

      $order_info   = DB::table("ba_service_booking") 
      ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
      ->select("ba_service_booking.*", "ba_business.name as businessName",
        "ba_business.locality as businessLocality", "ba_business.landmark as businessLandmark",
         "ba_business.city as businessCity", "ba_business.state as businessState", 
         "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",  
         "ba_business.banner" )
      ->where("ba_service_booking.id", $orderno )    
      ->first() ;

       if( !isset($order_info))
      {
        return redirect("/erp/orders/view-new-orders"); 
      }


      $order_items   = DB::table("ba_shopping_basket")
      ->join("ba_products","ba_shopping_basket.pr_code","=","ba_products.pr_code")  
      ->where("order_no", $orderno )
      ->select("ba_shopping_basket.*","ba_products.photos as image")    
      ->get() ;


      $customer_info   = DB::table("ba_profile") 
      ->where("id", $order_info->book_by )    
      ->first() ;

    

      $agent_info=  DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
         ->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
         ->where("order_no", $orderno  )
         ->first(); 

     
     $output = '

     <h3 align="center">Sales Receipt</h3>
     <hr/>

     <div>Name: '.$customer_info->fullname.'</div>

     <div>Phone: '.$customer_info->phone.'</div>

     <div>Address: '.$order_info->address.'</div><br/>
    


     <table width="100%" style="border-collapse: collapse; border: 0px;">
     <tr>
                      <th style="border: 1px solid; padding:12px;" width="20%">Sl.No</th>
                        <th style="border: 1px solid; padding:12px;" width="20%">Item</th>
                        <th style="border: 1px solid; padding:12px;" width="20%">Quantity</th>
                        <th style="border: 1px solid; padding:12px;" width="20%">Unit Price</th> 
                        <th style="border: 1px solid; padding:12px;" width="20%">Total</th> 
                    </tr>

     ';  
        $i = 0 ;
        $sub_total = 0;

     foreach ( $order_items   as $item)
     {

       $i++;

      $sub_total += $item->price * $item->qty;
               
    
      $output .= '
            
      <tr>
       <td align="center" style="border: 1px solid; padding:12px;">'.$i.'</td>
       <td align="center" style="border: 1px solid; padding:12px;">'.$item->pr_name.'</td>
       <td align="center" style="border: 1px solid; padding:12px;">'.$item->qty.' '.$item->unit.'</td>
       <td align="center" style="border: 1px solid; padding:12px;">'.$item->price.'</td>
       <td align="center" style="border: 1px solid; padding:12px;">'.number_format( $item->price * $item->qty, 2, ".", "" ).'</td>
      </tr>
    


      ';
  }
        $output .= ' <tfoot>
     <tr>
       <td style="border: none"></td>
       <td style="border: none"></td>
       <td style="border: none"></td>
       <td><b>Total Amount :</b></td>
       <td align="center">'.number_format( $sub_total , 2, ".", "" ).'</td>

     </tr>
   </tfoot>
   ';
     
     $output .= '</table>';
    // return $output;


      $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($output);
     return $pdf->stream();



    }

    public function downloadReceipt( Request $request)
    {     
     
      $order_info = $order_items =null;
      if( $request->o  != ""  )
      {


        $order_type = OrderSequencerModel::find($request->o);
        if( !isset($order_type)  )
        { return ; 
        }


        if(  $order_type->type == "normal")
        {
          $order_info = ServiceBooking::find( $request->o );
           
         

        $order_items   = DB::table("ba_shopping_basket")   
        ->join("ba_product_variant", "ba_product_variant.prsubid",  "=", "ba_shopping_basket.prsubid")
        ->where("ba_shopping_basket.order_no", $request->o )
        ->select("ba_shopping_basket.*" )
        ->get() ;



          $pdf_file =  "rcp-" .time(). '-' . $request->o . ".pdf";
          $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ; 

          $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
          ->setPaper('a4', 'portrait')
          ->loadView('admin.orders.bill_small_size' ,  
            array( 'data' =>   $order_info , 
                   'order_items' => $order_items));
 

          //echo $url = URL::to( "/public/assets/erp/bills/" .  $pdf_file  )  ;
          //return;

         
        }else   if(  $order_type->type == "booking"  )
        {
          
          $order_info = ServiceBooking::find( $request->o );      
          $offer = DB::table("ba_shopping_basket_srv_products")
          ->where("id",$order_info->offer_purchaseid)
          ->first();


          $business = Business::find($order_info->bin);
          $order_items = DB::table("ba_service_booking_details")
          ->where("book_id", $order_info->id )
          ->get();      

         

          $booking_orders = DB::table("ba_service_booking")
          ->where("id", $order_info->id )
          ->get();       
          

          $pdf_file =  "rcp-" .time(). '-' . $request->o . ".pdf";
          $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ; 

          $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
          ->setPaper('a4', 'portrait')
          ->loadView('admin.orders.booking_bill_small_size' ,  
            array('data'=>$order_info ,'coupon'=>$offer, 
                  'order_items' => $order_items,'scode'=>$business->sub_module,
                  'booking_orders'=>$booking_orders,'business'=>$business,
                  'order_type'=>$order_type));
           
          // return view("admin.orders.booking_bill_small_size")->with(array('data'=>$order_info , 
          //         'order_items' => $order_items,'scode'=>$business->sub_module));

        }

        else   if(  $order_type->type == "appointment"  )
        {
          
          $order_info = ServiceBooking::find( $request->o );      
          $offer = DB::table("ba_shopping_basket_srv_products")
          ->where("id",$order_info->offer_purchaseid)
          ->first();


          $business = Business::find($order_info->bin);

          $order_items = DB::table("ba_service_booking_details")
          ->where("book_id", $order_info->id )
          ->get();
         

          $service = Business::find($order_info->bin);

          $booking_orders = DB::table("ba_service_booking")
          ->where("id", $order_info->id )
          ->get();       
          

          $pdf_file =  "rcp-" .time(). '-' . $request->o . ".pdf";
          $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ; 

          $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
          ->setPaper('a4', 'portrait')
          ->loadView('admin.orders.booking_bill_small_size' ,  
            array('data'=>$order_info ,'coupon'=>$offer, 
                  'order_items' => $order_items,'scode'=>$business->sub_module,
                  'service'=>$service,'booking_orders'=>$booking_orders,'order_type'=>$order_type,
                'business'=>$business));
           
          // return view("admin.orders.booking_bill_small_size")->with(array('data'=>$order_info , 
          //         'order_items' => $order_items,'scode'=>$business->sub_module));

        }



        else   if(  $order_type->type == "offers"  )
        {
          
          $order_info = ServiceBooking::find( $request->o );      
          $offer = DB::table("ba_shopping_basket_srv_products")
          ->where("order_no",$order_info->id)
          ->first();


          $business = Business::find($order_info->bin);

          $order_items = DB::table("ba_service_booking_details")
          ->where("book_id", $order_info->id )
          ->get();
         

          $service = Business::find($order_info->bin);

          $booking_orders = DB::table("ba_service_booking")
          ->where("id", $order_info->id )
          ->get();       
          
 // dd($offer);
          $pdf_file =  "rcp-" .time(). '-' . $request->o . ".pdf";
          $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ; 

          $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
          ->setPaper('a4', 'portrait')
          ->loadView('admin.orders.offers_bill_small_size' ,  
            array('data'=>$order_info ,'coupon'=>$offer, 
                  'order_items' => $order_items,'scode'=>$business->sub_module,
                  'service'=>$service,'booking_orders'=>$booking_orders,'order_type'=>$order_type,
                'business'=>$business));
           
          // return view("admin.orders.booking_bill_small_size")->with(array('data'=>$order_info , 
          //         'order_items' => $order_items,'scode'=>$business->sub_module));

        }





        else{

          $order_info = PickAndDropRequestModel::find( $request->o );
           
          $pdf_file =  "rcp-" .time(). '-' . $request->o . ".pdf";
          $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ;
          $pdf = PDF::setOptions(['defaultFont' =>'sans-serif'])
          ->setPaper('a4', 'portrait')
          ->loadView('admin.orders.pnd_bill_small_size' , array('data'=>$order_info));

        }

        return $pdf->download( $pdf_file  ); 

      }
      else
      {
        return ;
      } 
    } 

}
