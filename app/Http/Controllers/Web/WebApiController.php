<?php

namespace App\Http\Controllers\Web; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File; 
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

use App\Jobs\SendOrderConfirmationSms;
use App\Jobs\SendOrderAssignToAgentSms;
 

use App\ShoppingOrderItemsModel;  
use App\ShoppingCartTemporary;  
use App\AgentServiceAreaModel; 
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\ProductModel; 
use App\Business;
use App\Traits\Utilities;
use App\User; 
use App\MarketingCallLog;
use App\CallbackEnquiryModel;
use App\MemberWishlistModel;
use App\SponsoredPackageModel;
use App\ServiceProductModel;
use App\LandingScreenSectionModel;

class WebApiController extends Controller
{

    use Utilities;
    protected function updateWebFcmToken(Request $request)
    {
      if(  $request->memkey == "" ||  $request->fcmtoken  == ""  )
      {
        $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
        return json_encode($data);
      }

      //updating fcm token
      $save  = DB::table("ba_users")
      ->where("profile_id", $request->memkey )
      ->update( array("firebase_token_web" =>$request->fcmtoken   ) );

      $data= ["message" => "success", "status_code" =>  9001 , 'detailed_msg' => 'FCM token updated.' ]; 
      return json_encode( $data); 
    }

}


