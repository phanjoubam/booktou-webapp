<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;
 use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;  
use Illuminate\Support\Facades\Hash;

use App\Business;
use App\AdvertisementModel;
use App\ServiceProductModel;


use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;


use App\Traits\Utilities;

class TestController extends Controller
{
	 	
	 use Utilities;
    
	protected function testing()
	{

		/*
		$apiKey = urlencode('1mxqIeHeiRg-12xPQ6p9RkzpueoptDFnbeLKaKczyS');
		// Prepare data for POST request
		$data = array('apikey' => $apiKey);
	 
		// Send the POST request with cURL
		$ch = curl_init('https://api.textlocal.in/get_templates/');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		
		// Process your response here
		$templates = json_decode(  $response);
 
 		
 		$message =  $templates->templates[0]->body  ;  

		
		*/

 		/*
		$apiKey = urlencode('1mxqIeHeiRg-12xPQ6p9RkzpueoptDFnbeLKaKczyS');
		// Prepare data for POST request
		$data = array('apikey' => $apiKey);


		$numbers=  array( "9612824492" ,"9740012097"  );
		// Account details
	      
	      // Message details
	    	$message = "OTP Sending Test using textlocal account";

	      $sender = urlencode('TXTLCL');
	      $message = rawurlencode( $message );
	       
	      $numbers = implode(',', $numbers);
	       
	      // Prepare data for POST request
	      $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message );
	      // Send the POST request with cURL

	      $ch = curl_init("https://api.textlocal.in/send/");
	      curl_setopt($ch, CURLOPT_POST, true);
	      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	      $response = curl_exec($ch);
	      curl_close($ch);
	      echo  $response;


	      */ 
	    
		  $date = date('Y-m-d'); 

        $order_list = DB::table("ba_service_booking")
        ->whereRaw("date(book_date) ='$date'")
        ->where("book_status", "<>", "cancelled")
        ->where("sms_sent", "no")
        ->get();

        $oids= array();  

        foreach ($order_list as $item)
        {
            $oids[] = $item->id;

            $customer_phone = DB::table("ba_profile")
            ->select("phone") 
            ->where("id",  $item->book_by )
            ->first();
    
            if(isset($customer_phone))
            {
                $message = 'Your order is placed successfully through BookTou. Order # is ' . $item->id . '. Our delivery partner will contact you soon.';
                $this->sendSmsAlert( array($customer_phone->phone) ,   $message );
            }

            //sms bookTou Team for new orders
            $message = 'There is a new order with reference # ' . $item->id . '. Please process it from the CC Portal.';
            $this->sendSmsAlert( array(  '7005894963', '9740012097', '8259839981', '7005188802' ) ,   $message );

        }
        DB::table("ba_service_booking") 
        ->whereIn("id", $oids)
        ->update(['sms_sent' => 'yes']);
  

	}
	
	
	protected function regenPassword (Request $request) 
	{
		echo  Hash::make( $request->password ) ;
		
	}

	
	protected function addNewAd(Request $request)
	{
		
		//find if user exists  (admin or agent app)
		$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
          $data= [
			"message" => "failure", 
			"status_code" =>  901 , 
			'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.' 
			] ;
			 
          return json_encode($data);
        }

		//check parameter missing 
        if( $request->bin == "" ||   $request->startedOn == "" || $request->validTill == ""   )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.' ]; 
            return json_encode($data);
        }
		
		
		//write the business logic
		
		$newadd = new AdvertisementModel;
		$newadd->bin = $request->bin;
		$newadd->started_on = date('Y-m-d',  strtotime( $request->startedOn ));
		$newadd->valid_till = date('Y-m-d',  strtotime(  $request->validTill )); 
		$save = $newadd->save();
		
		if($save)
		{
			$data= ["message" => "success", "status_code" =>  3601 , 'detailed_msg' => 'New ad posted.' ]; 
            return json_encode($data);

		}	
		else
		{
			$data= ["message" => "failure", "status_code" =>  3600 , 'detailed_msg' => 'New ad could not be posted.' ]; 
            return json_encode($data);
		}
				 
		
	}



//section for adding service product by sanatomba
protected function addNewServiceProduct(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
          $data= [
            "message" => "failure", 
            "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.' 
            ] ;
             
          return json_encode($data);
        }
        if( $request->bin == "" )
        {
            $data= ["message" => "failure", 
                    "status_code" =>  902 , 
                    'detailed_msg' => 'Missing data to perform action.' ]; 
            return json_encode($data);
        }

        $addNewServiceProduct = new ServiceProductModel();
        $addNewServiceProduct->bin = $request->bin;
        $addNewServiceProduct->service_category= $request->serviceCategory;
        $addNewServiceProduct->srv_name = $request->serviceName;
        $addNewServiceProduct->srv_details = $request->details;
        $addNewServiceProduct->pricing = $request->pricing;
       	$addNewServiceProduct->duration_hr  = $request->durationHour;
        $addNewServiceProduct->duration_min  = $request->durationMinute;
        $addNewServiceProduct->service_type = $request->serviceType;
        

        //if the service category is offer perform the operation
        if($request->serviceCategory=="OFFER")
        {

        		$addNewServiceProduct->actual_price = $request->actualPrice;
        		$addNewServiceProduct->discount  = $request->discount;
      			$addNewServiceProduct->valid_start_date = date('Y-m-d',  strtotime($request->validStartDate));
        		$addNewServiceProduct->valid_end_date =date('Y-m-d', strtotime($request->validEndDate)) ;

		       if(isset($request->photo))
		        {
						$addNewServiceProduct->photos = $request->photo;
						$save = $addNewServiceProduct->save();


						if ($save) 
						{

						            $data= ["message" => "success", "status_code" =>  3603 , 
						            'detailed_msg' => 'Service product added successfull!' ]; 
						            return json_encode($data);
						}
				}
					else
						        {
						        	$data= ["message" => "failure", "status_code" =>  3600 , 
				            		'detailed_msg' => 'Please select photo!' ]; 
				            		return json_encode($data);	
		        				}
        }//service category offer ends here	
        			else {
        			//else part is for all the other service

				        $save = $addNewServiceProduct->save();
						if ($save) {

				            $data= ["message" => "success", "status_code" =>  3603 , 
				            'detailed_msg' => 'Service product added successfull!' ]; 
				            return json_encode($data);
				        }else
				        {
				            $data= ["message" => "failure", "status_code" =>  3600 , 
				            'detailed_msg' => 'Failed to add new service!' ]; 
				            return json_encode($data);
				        }


				        }//ends here

		
        
        
        



    }


 protected function fetchBusinessServiceProducts(Request $request)
    {
if(  $request->bin == "" || $request->serviceCategory == "" )
       {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => "No business selected to it's services." ];
         return json_encode($data);
       }
       
       /*
       $business = DB::table("ba_business")
       ->where("id", $request->bin )
       ->select("id as bin", "name", "tag_line as tagLine", "phone_pri as phone",  
        "locality", "landmark", "city", "state", "pin", "profile_image as profileImgUrl", "banner as bannerImgUrl" , "rating" , 
        DB::Raw("'[]' services") 
        )
       ->first();
       
        if( !isset($business))
        {
          
          $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        
        $business->profileImgUrl = config('app.app_cdn')  .  '/' .  $business->bannerImgUrl; 
        $business->bannerImgUrl = config('app.app_cdn')  .  '/' .  $business->bannerImgUrl; 
        */
        $where_clause = "bin ='" . $request->bin . "' and service_category ='" . $request->serviceCategory . "'";
         
        if(  $request->gender != "" )
        {
            $where_clause .= " and ( gender ='" . $request->gender . "' or gender ='all')";
        }
        
        if(  $request->serviceType != ""  )
        {
            $where_clause .= " and service_type ='" . $request->serviceType . "'";
        }
        
        
        
        //show services 
        $business_services  = DB::table("ba_service_products") 
        ->whereRaw( $where_clause )  
        ->select("id as serviceProductId", "service_category as serviceCategory",    "srv_name as serviceName" , "srv_details as serviceDetails", 
        "pricing", "duration_hr as durationHour" , "duration_min as durationMinute","srv_status as canBeBook", "service_type as serviceType", "gender" ,"discount","actual_price as actualPrice" , "valid_start_date as validStartDate",
        "valid_end_date as validEndDate","photos as photo")
        ->get();
     
    
    //$business->services =     $business_services;
    
    $data= ["message" => "success", "status_code" =>  1025 ,
      'detailed_msg' => 'Business profile fetched.' , 
      'results' => $business_services ];
     
    
    return json_encode( $data);
    }




//client side
public function fetchClientBusinessServiceProducts(Request $request)
   {
	   
	   if(  $request->bin == ""   )
	   {
	     $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => "Missing data to perform action." ];
	     return json_encode($data);
	   }
	   
	   $where_clause = " bin ='". $request->bin . "'";
	   
	   
	   if($request->filters != "")
	   {	
			$allfilters = explode(",", $request->filters);
			$filterValues = explode(",", $request->filterValues);print_r($filterValues);
			
			$i=0;
			foreach($allfilters as $filter)
			{
				if($filter  == "service type")
				{
					//$where_clause .= " and service_category = '" .  $filterValues .  "'";
					//echo " and service_category = '" .  $filterValues[$i] .  "'";
				}
			}
		}
	   
	   
	   //show services
	   if($request->serviceCategory != "all")
	   { 
			$where_clause .= " and service_category = '" .  $request->serviceCategory .  "'";
		}
		
		if($request->gender != "all" &&  $request->gender  != "")
		{
			$where_clause .= " and gender = '" .  $request->gender .  "'";
		}
	
		if($request->serviceType  != "all" && $request->serviceType  != "")
		{
			$where_clause .= " and service_type = '" .  $request->serviceType .  "'";
		}
		
	$business_services  = DB::table("ba_service_products") 
	->whereRaw( $where_clause )  
	->select("id as serviceProductId", "service_category as serviceCategory",
	"srv_name as serviceName" , "srv_details as serviceDetails", 
	"pricing", "duration_hr as durationHour" , "duration_min as durationMinute", 
	"srv_status as canBeBook", "service_type as serviceType", "gender","discount","actual_price as actualPrice" , "valid_start_date as validStartDate",
	"valid_end_date as validEndDate","photos as photo")
	->get();
	
	//$business->services = 	$business_services; 
    $data= ["message" => "success", "status_code" =>  1031 ,
	'detailed_msg' => 'Business service products fetched.' , 
	'results' => $business_services ];
	return json_encode( $data);
  }





protected function sendToTarget(Request $request)
{
	
	$message_row = DB::table("ba_sms_templates")
	->where("name", "otp_generate")
	->first();
	
	$rand = mt_rand(1111, 9999);
	if( !isset($message_row))
	{
		$data= ["message" => "failure" , "status_code" =>  999  ,  'detailed_msg' => "Template not found!" ];
	} 
	

	$otp_msg =  urlencode( str_replace(  "###", $rand,  $message_row->message ) );
	$dlt_entity_id = $message_row->dlt_entity_id;
	$dlt_header_id = $message_row->dlt_header_id ;
	$dlt_template_id = $message_row->dlt_template_id ; 
	
	$phones = explode(",", $request->phone );
	
	$allphones = array(); 
	for($i=0; $i < sizeof($phones) ; $i++ )
	{
		$allphones[] .= "91" .  $phones[$i] ;
	}
	 
	
	$phones = implode(",", $allphones) ;
	 
	 
	 
	 
	  $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, "https://www.txtguru.in/imobile/api.php?");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,  "username=ezrasorokhaiba&password=bookTou@2020&source=BOOKTU&dmobile=". $phones . 
	  "&message=" . $otp_msg .  "&dltentityid=". $dlt_entity_id . "&dltheaderid=". $dlt_header_id ."&dlttempid=". $dlt_template_id . "&mt=u" );
      $response = curl_exec($ch);
      curl_close($ch);
	  
	  echo json_encode( $response);  
}




protected function subMenuGenerator(Request $request)
{


	$max_amount = DB::table("ba_product_variant")
	->whereIn("prid", function($query){
		$query->select("id")
		->from("ba_products")
		->where("category",  'restaurant');
	})
	->max("unit_price");

 

		$data = array(   'max_amount' => $max_amount );
        return view("store_front.side_menu_preview")->with(  $data ); 



	}



	protected function autoAssignOrder()
	{
		echo "here";

	}


}
