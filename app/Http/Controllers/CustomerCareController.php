<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
 
use App\Auth;
use App\ServiceBooking;
  

class CustomerCareController  extends Controller
{
   
  
 //View student attendance
  protected function viewCustomerCare(Request $request)
  { 
 
  $day_bookings= DB::table("ba_service_booking")
   
  ->get();


       $data = array('results' => $day_bookings );

         return view("admin.customerCare")->with('data',$data);
    }

    


     //Fetch customer details
    public function getCustomerDetails(Request $request)
    {
        $profileId = $request->memberId;
       // $profileId = $Id;
       
        $customerProfile = DB::table("ba_profile")
        ->where("id",6)
        ->get();

         $result = array('results' => $customerProfile );
  

       // $data= ["message" => "success", "status_code" =>  1061 ,  'detailed_msg' => 'View customer profile success',"results" => $customerProfile];

        return view("admin.profile")->with('data',$result);

    }
 
  /*
    $date1 = new \DateTime( $item->added_on );
                $date2 = new \DateTime( date('Y-m-d H:i:s') );
                $interval = $date1->diff($date2);
                $jobAge =  $interval->format('%a days %H hrs  %i mins');
                $date3 = new \DateTime(  $item->exact_delivery_on  );
                $interval2 = $date1->diff($date3);
                $completionTime =  $interval->format('%a days %H hrs %i mins');
  */


 } 