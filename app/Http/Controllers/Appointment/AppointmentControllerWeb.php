<?php

namespace App\Http\Controllers\Appointment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\file;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;


use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;

use App\User;
use App\CustomerProfileModel;
use App\CallbackEnquiryModel;
use App\QuoteRequestModel;
use App\SubscriberModel;
use App\OrderSequencerModel;
use App\ContactFormModel;
use App\Business;
use App\MerchantOrderSequencer;
use App\ServiceBookingDetails;
use App\PickAndDropRequestModel;
use App\ServiceRating;
use App\ShoppingBasketServiceProductModel;
use App\TempBookingCartModel;
use App\ServiceProductModel;



use Razorpay\Api\Api;
use App\Traits\Utilities;


class AppointmentControllerWeb extends Controller
{

  use Utilities;
  public function appointmentIndex(Request $request)
  {
    $name = $request->name; 
    //saving module selected in session
    $request->session()->put('module_active', 'appointment');


    $main_modules = DB::table("ba_bookable_category")
    ->select(DB::raw('distinct(main_module)as main'))
    ->get();

    $browse = DB::table("ba_business_category")
    ->where("main_module", "APPOINTMENT")
    ->where("is_public", "yes")
    ->where("is_active",'yes')
    ->get();



    $booking = DB::table("ba_service_booking_details")
    ->select(DB::Raw("count(*) as productCount"),"service_product_id as productid")
    ->groupBy("service_product_id")
    ->orderBy("productCount",'desc')
    ->paginate(10);

    $prid = array();

    foreach($booking as $items)
    {
      $prid[] = $items->productid;
    }

    $serviceProduct = DB::table("ba_service_products")
    ->whereIn("id",$prid)
    ->get();

    $biz_categories = DB::table("ba_business_category")
    ->where("main_module", "Shopping")
    ->where("is_active", "yes" )
    ->get();

    $business = DB::table("ba_business")
    ->where("main_module", "appointment")
    ->where("sub_module","SNS")
    ->where("is_open","open")
    ->where("is_block", "no") 
    ->paginate(4);



    $packages = DB::table("ba_service_products")
    ->whereIn("bin",  function( $query ) {

        $query->select("id")
        ->from("ba_business")
        ->where("main_module", "appointment")
        ->where("sub_module","SNS")
        ->where("is_open","open")
        ->where("is_block", "no");
    })
    ->paginate(10);


    $shopping = DB::table("ba_shopping_basket")
    ->select(DB::Raw("count(*) as totalProduct"),"prsubid as serviceProductid")
    ->groupBy("prsubid")
    ->orderBy("totalProduct",'desc')
    ->paginate(8);

    $productid = $productsubid = array();

    foreach($shopping as $items)
    {
      $productsubid[] = $items->serviceProductid;
    }

    $product_variant = DB::table("ba_product_variant")
    ->whereIn('prsubid',$productsubid)
    ->get();

    foreach($product_variant as $items)
    {
      $productid[] = $items->prid;
    }

    $products = DB::table("ba_products")
    ->whereIn('id',$productid)
    ->select("category");

    $booking = DB::table("ba_water_booking_basket")
    ->select("category_name as category")
    ->groupBy('category');

    $appointment = DB::table("ba_service_booking_details")
    ->select(DB::Raw("count(*) as totalProduct"),"service_product_id as serviceProduct")
    ->groupBy("service_product_id")
    ->orderBy("totalproduct",'desc')
    ->paginate(15);

    $serviceprid = array();

    foreach($appointment as $items)
    {
      $serviceprid[] = $items->serviceProduct;
    }

    $all_categoris = DB::table("ba_service_products")
    ->whereIn("id",$serviceprid)
    ->union($booking)
    ->union($products)
    ->select("service_category as category")
    ->get();

    $module_active = session()->get('module_active');

    $city = DB::table('ba_business')
    ->select(DB::Raw('distinct(city) as cityList'))
    ->where('main_module',$module_active)
    ->get();


    $package_categories = DB::table("ba_service_category")
    ->where("sub_module", "sns")
    ->get();



    $data = array("main_modules"=>$main_modules,'name'=>$name,"browses"=>$browse,
      'packages' =>$packages,
      'package_categories' => $package_categories,
      "product"=>$serviceProduct,"biz_categories"=>$biz_categories,"business"=>$business,"all_categoris"=>$all_categoris,'city'=>$city);

    return view("store_front.appointment.appointment_index")->with($data); 

  }


  protected function searchAppointmentServices(Request $request)
  {


    if( $request->session()->has('module_active') && $request->session()->has('module_active') == "appointment")
    {
      $module_active = session()->get('module_active');
    }
    else
    {
      $module_active ='appointment' ;
      $request->session()->put('module_active', 'appointment');
    }



    if($request->keyword == "" || $request->city == ""   )
    {
      return redirect('/appointment');
    }

    $keyword = $request->keyword;
    $cityname = $request->city;

    $businesses = DB::table("ba_business")
    ->select('id','name')
    ->where("main_module", strtoupper($module_active))
    ->where('city', $cityname )
    ->get();


    $city = DB::table('ba_business')
    ->select(DB::Raw('distinct(city) as cityList'))
    ->where("main_module", strtoupper($module_active))
    ->get();

    $bins = $businesses->pluck("id"); 


    $service_profiles =  $packages = array();
    if(count($bins) > 0)
    {

      $service_profiles = DB::table('ba_business_service_profile') 
      ->whereIn('bin', $bins)
      ->get();

      $packages = DB::table("ba_service_products")
      ->whereIn('bin', $bins)
      ->whereRaw("( srv_name like '%$keyword%' or service_category like '%$keyword%')")
      ->paginate(10);

      $package_ids = $packages->pluck("id");



        $package_specifications = DB::table('ba_business_service_profile') 
        ->whereIn('service_product_id', $package_ids)
        ->get();


          $data = array("packages"=>$packages,'businesses'=>$businesses, 'package_specifications' => $package_specifications, 'city'=>$city);
        return view('store_front.appointment.search_appointment_service_result')->with($data);
 
    }
    else
    {
      return redirect('/appointment');
    }

  }


  protected function viewPackagesByCategory(Request $request) 
  {
    $name = "appointment";
    $categoryname = $request->package_category;

    $services = DB::table('ba_service_products')
    ->whereNotIn("bin", function($query){
      $query->select("id")->from("ba_business")->where("is_block", "yes");
    })
    ->whereRaw("replace(service_category, ' ', '-') like '%$categoryname%'") 
    ->where("pricing", ">", 0)
    ->paginate(15);

    $bins = array();
    $all_services = array();
    foreach($services as $service)
    {
      $bins[] = $service->bin; 
      $all_services[] = $service;
    }
    $bins[] = 0; 

    $service_profiles = DB::table('ba_business_service_profile') 
    ->whereIn('bin', $bins)
    ->get();

    $businesses = DB::table('ba_business') 
    ->whereIn('id', $bins)
    ->get();

 
    shuffle($all_services);   
    $data = array( 
      "businesses" =>$businesses,
      "services"=>$all_services, 
      'service_profiles' =>$service_profiles,
      "categoryname"=>$categoryname
    );
    return view("store_front.appointment.packages_under_category")->with($data);

  }


  protected function bookingCategoryDetails(Request $request)
  {

    $name = $request->name;

    $maincategory = $request->categoryname;
    $mainmenu = explode("-", $maincategory );
    $category = implode(" ", $mainmenu );


    $business_category = DB::table('ba_business_category')
    ->where('name',$category)
    ->first();


    // business list
    $booking_list = DB::table("ba_bookable_category")
    ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
    ->where('sub_module',$business_category->sub_module)
    ->get();

    $submodule = $submodulename = $paa = $bin = $service_products = array();

    foreach($booking_list as $items)
    {

      $submodule[] = $items->sub_module; 
      $submodulename[] = $items->sub_module_name;

    }


    $businesses = DB::table("ba_business")
    ->whereIn("sub_module", $submodule )
    //->where('module_name',$category)
    ->where('is_open','open')
    ->select("ba_business.*",DB::Raw("'[]' products"))
    ->get();


    foreach($businesses as $item)
    {
      if ( strcasecmp($item->sub_module, "paa")== 0 ) 
      {
        $paa[] = $item->id;
      }else{
        $bin[] = $item->id;
      }
    }

    $services = DB::table('ba_service_products') 
    ->whereIn('bin', $paa)
    ->orderBy("bin" )
    ->get();

    
    $btssids = array(); 
    $i=0;
    foreach ($businesses as $items) 
    {

      $service_products[$i]['id'] = $items ;
      $prodlist = array();
      $items->products = array() ;

      foreach ($services as $sub_item)
      {
        $btssids[] = $sub_item->id; 
        if ($items->id==$sub_item->bin)
        {
          $prodlist[] = $sub_item; 
        }
      }

      $items->products = $prodlist ; 
      $i++;
    }

    $btssids[] = 0;

    $service_images = DB::table('ba_business_service_profile') 
    ->whereIn('service_product_id', $btssids)
    ->get();


    $servicecategory = DB::table('ba_service_products')
    ->select(DB::Raw("DISTINCT(service_category)","id"))
    ->whereIn('bin',$paa)
    ->get();

    $max_amount = DB::table("ba_service_products")
    ->whereIn("bin", function($query){
      $query->select("id")
      ->from("ba_business")
      ->where("service_category", 'PICNIC');
    })
    ->max("pricing");

    $min_amount = DB::table("ba_service_products")
    ->whereIn("bin", function($query){
      $query->select("id")
      ->from("ba_business")
      ->where("service_category", 'PICNIC');
    })
    ->min("pricing");

    // product list ends here
    $data = array("businesses"=>$businesses,
      "services"=>$services,
      'service_images' =>$service_images, 
      "min_amount"=>$min_amount,
      "servicecategory"=>$servicecategory,
      "max_amount"=>$max_amount,
      "name"=>$name,
      "category"=>$category,
      "mainmenu"=>$maincategory);
    return view("store_front.service.service_details")->with($data);
  } 



  public function appointmentBusiness(Request $request)
  {
    $name = $request->name;
    $bin = $request->bin;

    $Abusiness = DB::table('ba_business')
    ->where("id",$bin)
    ->first();

    $product = DB::table('ba_service_products')
    ->where("bin",$Abusiness->id)
    ->select("ba_service_products.*")
    ->get();

    $appointment = DB::table("ba_bookable_category")
    ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
    ->where('main_module',"APPOINTMENT")
    ->get();



    $submodule = $submodulename = $sns = array();

    foreach($appointment as $items)
    {
      $submodule[] = $items->sub_module;
      $submodulename[] = $items->sub_module_name;

    }

    $Amerchants = DB::table("ba_business")
    ->whereIn("sub_module", $submodule )
    ->where('is_verified','yes')
    ->get();

    // foreach($Amerchants as $item)
    // {
    // if ($item->sub_module=="SNS") {
    // $sns[] = $item->id;
    // }else{
    // $bin[] = $item->id;
    // }
    // }

    // product list for selected business
    if ($request->category=="")
    {

      $product = DB::table('ba_service_products')
      ->where('bin',$bin)
      ->get();


    }else if($request->category=="all")
    {

      $product = DB::table('ba_service_products')
      ->where("bin",$Abusiness->id)
      ->select("ba_service_products.*")
      ->get();

    }
    else
    {

      $Acategorylist = $request->category;
      $product = DB::table('ba_service_products')
      ->where('service_category',$Acategorylist)
      ->where('bin',$bin)
      ->get();

    }


    $acategory = DB::table('ba_service_products')
    ->select(DB::Raw("DISTINCT(service_category)","id"))
    ->where('bin',$bin)
    ->get();



    $data = array('product'=>$product,"acategory"=>$acategory,"name"=>$name,"Abusiness"=>$Abusiness);
    return view("store_front.service.appointment_business")->with($data);
  }




//booking business landing page
protected function appointmentCalendar(Request $request)
{

  $member_id = $request->session()->has('__member_id_' ) ?  $request->session()->get('__member_id_' ) : -1;
  $session_id = $request->session()->has('shopping_session' ) ?  $request->session()->get('shopping_session' ) : null;

  if ($request->bin =="") 
  {
    return redirect()->back();
  }
  $bin = $request->bin;
  $today = date('Y-m-d');


  
    $business_info  = DB::table("ba_business")
    ->where("id", $bin )
    ->where("is_block", "no")
    ->where("is_open", "open")
    ->first();



    if( strcasecmp($business_info->sub_module, "sns") == 0)
    {
      if( $request->package != "")
      {
        $package_info = ServiceProductModel::find($request->package);

        $temp_booking = TempBookingCartModel::where("bin",  $bin )
        ->where("package_id", $request->package  )
        ->where("member_id", $member_id )
        ->first();
        
        if( !isset($temp_booking))
        {
          $temp_booking = new TempBookingCartModel;
          $temp_booking->service_date= date('Y-m-d');
        }
        $temp_booking->bin=$request->bin;
        $temp_booking->package_id  =$request->package;
        $temp_booking->amount = isset( $package_info ) ? $package_info->actual_price : 0 ;
        $temp_booking->gst = isset( $package_info ) ? (  $package_info->cgst_pc + $package_info->sgst_pc ) *  $package_info->actual_price * 0.01 : 0 ;
        $temp_booking->member_id = $member_id;
        $temp_booking->session_id =  $session_id;
        $temp_booking->save();
      }

      if( $member_id > 0)
      {
        //updating all entries saved using session id with member id
        DB::table("ba_temp_booking_cart") 
        ->where("bin", $bin)
        ->where("session_id",  $session_id )
        ->orWhere("member_id",  $member_id )
        ->update([ 'member_id' => $member_id, 'session_id' => $session_id ]);


        $selected_packages = DB::table("ba_temp_booking_cart") 
        ->where("bin", $bin)
        ->where("member_id",  $member_id )
        ->get();

      }
      else
      {
        $selected_packages = DB::table("ba_temp_booking_cart") 
        ->where("bin", $bin)
        ->where("session_id",  $session_id )
        ->get();
      }
 


      $package_ids = $selected_packages->pluck("package_id")->toArray();
   
      if(count($package_ids) == 0)
      {
          return redirect('/appointment/business/prepare-service-appointment?bin=' . $bin )
          ->with("err_msg", "No package is selected!");
      }


    

    $service_timeslots = DB::table("ba_service_days") 
    ->where("bin", $bin )
    ->whereDate("service_date",  $today ) 
    ->select("slot_number as slotNo", "start_time as startTime", "end_time as endTime", "status") 
    ->get();


    $packages = DB::table("ba_service_products")
    ->whereIn("id",  $package_ids)
    ->get();


    $staffs = DB::table("ba_profile")
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereIn("ba_users.category", array("1", '10' ) )
    ->where("bin", $bin ) 
    ->select("ba_profile.id as staffId", "fullname as staffName", "profile_image_url as profilePicture" ) 
    ->get();


    $data = array(  
      'business'=>$business_info, 
      'timeslots' => $service_timeslots, 
      'selected_packages' => $selected_packages,
      'packages' => $packages, 
      'staffs'  => $staffs );
    return view('store_front.appointment.appointment_calendar')->with($data);
    }
    else
    {
      return redirect("/appointment")->with("err_msg" , "No matching business found!");
    }    

}


  
  public function bookingBusinessDetails(Request $request)
  {

    $name = $request->name;
    $bin = $request->bin;

    $categoryname = $request->categoryname;

    $business = DB::table('ba_business')
    ->where("id",$bin)
    ->first();

    $product = DB::table('ba_service_products')
    ->where("bin",$business->id)
    ->select("ba_service_products.*")
    ->get();

    $booking = DB::table("ba_bookable_category")
    ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
    ->where('main_module',"BOOKING")
    ->get();

    // product list for selected business
    if ($request->category=="")
    {
      $services = DB::table('ba_service_products')
      ->where('bin',$bin)
      ->get();
    }
    else
    {

      $categorylist = $request->category;

      $services = DB::table('ba_service_products')
      ->where('service_category',$categorylist)
      ->where('bin',$bin)
      ->get();
    }

    $categories = DB::table('ba_service_products')
    ->select(DB::Raw("DISTINCT(service_category)","id"))
    ->where('bin',$bin)
    ->get();


    $service_profiles = DB::table('ba_business_service_profile') 
    ->where('bin', $bin)
    ->get(); 


    $data = array("business"=>$business,
      "services"=>$services,
      'service_profiles' =>$service_profiles,
      "categories"=>$categories,
      "name"=>$name,
      "categoryname"=>$categoryname
    );
    return view("store_front.service.booking_business")->with($data);

  }


  protected function bookingViewServiceBusiness(Request $request)
  {

    if($request->serviceid=="")
    {
      return view('service/booking');
    }

    $service = DB::table('ba_service_products')
    ->where('id',$request->serviceid)
    ->first();

    if (!isset($service)) {
      return view('service/booking');
    }
    else
    {
      $name = $request->name;

      $id = $request->bin;

      $business = DB::table('ba_business')
      ->where("id",$id)
      ->first();

      // dd(request()->get('MAIN_MODULES'));
      // not found

      $product = DB::table('ba_service_products')
      ->where("bin",$business->id)
      ->select("ba_service_products.*")
      ->get();

      $bookable = DB::table("ba_bookable_category")
      ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
      ->where('main_module',"BOOKING")
      ->get();

      $submodule = $submodulename = $wtr = $bin = array();

      foreach($bookable as $items)
      {

        $submodule[] = $items->sub_module;
        $submodulename[] = $items->sub_module_name;
      }

      // $merchants = DB::table("ba_business")
      // ->whereIn("sub_module", $submodule )
      // ->where('is_verified','yes')
      // ->get();

      // foreach($merchants as $item)
      //  {
      //     if ($item->sub_module=="WTR") {
      //        $wtr[] = $item->id;

      //     }else{

      //       $bin[] = $item->id;
      //     }
      //  }
      // // product list for selected business
      //  if ($request->category=="")
      //   {
      //    $product = DB::table('ba_service_products')
      //    ->whereIn('bin',$wtr)
      //    ->get();

      //   }
      // else
      // {

      //   $categorylist = $request->category;

      //    $product = DB::table('ba_service_products')
      //     ->whereIn('service_category',$categorylist)
      //     ->whereIn('bin',$wtr)
      //     ->get();

      // }

      $category = DB::table('ba_service_products')
      ->select(DB::Raw("DISTINCT(service_category)","id"))
      ->whereIn('bin',$wtr)
      ->get();

      $data = array("merchants"=>$merchants,'product'=>$product,"category"=>$category,"name"=>$name,"business"=>$business);
      return view("store_front.service.booking_business")->with($data);
    }

  }


  // fetching service details method
  protected function getServiceDetails(Request $request)
  {

    if($request->serviceProductId == ""  || $request->bin=="")

    {
      return redirect('appointment/appointment-details');
    }


    $service_details = DB::table("ba_business_service_profile")
    ->select( "id as headingId", "service_product_id as serviceProductId", "heading", "description", "photos")
    ->where("service_product_id" ,  $request->serviceProductId)
    ->get();



    foreach($service_details  as $item)
      {  if($item->photos == "" || $item->photos == null  )
    {
      $item->photos =  "";
    }


  }

  $returnHTML = view('store_front.service.view_service_details')->with('result',$service_details)->render();

  return response()->json(array('success'=>true,'status_code'=>7003, 'html'=>$returnHTML));


}

protected function viewServiceBusiness(Request $request)
{

  if($request->serviceid=="")
  {
    return view('service/appointment');
  }

  $service = DB::table('ba_service_products')
  ->where('id',$request->serviceid)
  ->first();

  if (!isset($service)) {
    return view('service/appointment');
  }
  else
  {
    $name = $request->name;
    $bin = $service->bin;
    $service_id = $request->serviceid;


    $Abusiness = DB::table('ba_business')
    ->where("id",$bin)
    ->first();

    // product list for selected business
    if ($request->category=="")
    {
      $product = DB::table('ba_service_products')
      ->where("bin",$Abusiness->id)
      ->select("ba_service_products.*")
      ->get();

    } else if($request->category=="all")
    {
      $product = DB::table('ba_service_products')
      ->where("bin",$Abusiness->id)
      ->select("ba_service_products.*")
      ->get();
    }

    else {

      $categorylist = $request->category;
      $product = DB::table('ba_service_products')
      ->where('service_category',$categorylist)
      ->where('bin',$Abusiness->id)
      ->get();
    }
    // product section ends here

    $appointment = DB::table("ba_bookable_category")
    ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
    ->where('main_module',"APPOINTMENT")
    ->get();

    $submodule = $submodulename = $sns = array();

    foreach($appointment as $items)
    {

      $submodule[] = $items->sub_module;
      $submodulename[] = $items->sub_module_name;

    }

    $acategory = DB::table('ba_service_products')
    ->select(DB::Raw("DISTINCT(service_category)","id"))
    ->where('bin',$bin)
    ->get();



    $data = array('product'=>$product,
      "acategory"=>$acategory,
      "name"=>$name,
      "Abusiness"=>$Abusiness,
      "serviceid"=>$service_id);


    return view("store_front.service.appointment_business")->with($data);
  }

}




protected function viewByAppointmentCategoryName(Request $request)
{
  $categoryname = $request->name;

  if ($categoryname=="") {
    return redirect()->back();
  }

  $service_products = DB::table('ba_service_products')
  ->where('service_category',$categoryname)
  ->get();

  $bin = array();
  foreach($service_products as $products)
  {
    $bin[] = $products->bin;
  }

  $business_list = DB::table('ba_business')
  ->whereIn('id',array_unique($bin))
  ->get();

  $data = array('products'=>$service_products,'business'=>$business_list);
  return view('store_front.service.view_by_apppointment_menu')->with($data);
}

 



protected function prepareServiceAppointment(Request $request)
{
  if ($request->bin =="") {
    return redirect()->back();
  }

  $bin =  $request->bin;
  
  $business  = DB::table("ba_business")
  ->where("id",  $bin )
  ->where("is_block","no") 
  ->first();

  if( !isset( $business))
  {
    return redirect()->back();
  }


  $package_categories =  DB::table("ba_service_category")
      ->whereIn("category", function($query) use($bin){
        $query->select("service_category")->from("ba_service_products")->where("bin", $bin);
      })
  ->get();




  if( $request->category == "")
  {
    $first_category = $package_categories[0]->category;

    $packages = DB::table("ba_service_products")
    ->where("bin",  $bin)
    ->where('service_category', $first_category)
    ->whereNotIn('service_category',['OFFER'])
    ->orderBy("service_category","desc")
    ->get();
  }
  else
  {
    $packages = DB::table("ba_service_products")
    ->where("bin",  $bin)
    ->where('service_category', $request->category )
    ->whereNotIn('service_category',['OFFER'])
    ->orderBy("service_category","desc")
    ->get();
  }



  $package_specifications = DB::table('ba_business_service_profile') 
  ->where('bin', $bin)
  ->get();
 

  $available_slots = DB::table("ba_service_days")
  ->where("bin", $bin )
  ->where("year",  date('Y') )
  ->where("month",  date('m') )
  ->select(DB::raw('staff_id, slot_number, start_time, end_time ')) 
  ->groupBy("slot_number")
  ->groupBy("start_time")
  ->groupBy("end_time")
  ->groupBy("staff_id")
  ->get();

 
  $staff_ids = $available_slots->pluck("staff_id");

  $staff_available = DB::table("ba_profile")
    ->whereIn("id",  function($query) use (  $bin )
    {
      $query->select("profile_id") 
      ->from('ba_users')
      ->where("bin",  $bin );  
    })
    ->select("id as staffId", "fullname as staffName", "profile_image_url as profilePicture" ) 
    ->get();

 



  $data = array( "business"=>$business, 
    'packages'=> $packages ,
    'package_categories' => $package_categories, 
    'package_specifications' =>$package_specifications,
    'available_slots' => $available_slots,
    'staff_available' => $staff_available);
  return view("store_front.appointment.appointment_business_profile")->with($data); 



}


protected function reviewAppointmentSummary(Request $request)
  {

    $return_url = url()->full();
    $member_id = $request->session()->has('__member_id_' ) ?  $request->session()->get('__member_id_' ) : -1;
    $session_id = $request->session()->has('shopping_session' ) ?  $request->session()->get('shopping_session' ) : null;

    $cust_info =  CustomerProfileModel::find( $member_id);
    if(!isset($cust_info))
    {
      $request->session()->put('return_url', $return_url );
      return redirect("/shopping/login")->with("err_msg",  "Please login to complete booking process!");
    }


    $bin = $request->bin;
    if($bin == "")
    {
      return redirect('/booking');
    }
    $business= Business::find( $bin);
    if( !isset($business))
    {
        return redirect('/booking')->with("err_msg", "No matching business found!");
    }
 

    $packages = DB::table("ba_service_products")
    ->whereIn("id", function($query) use($bin, $member_id ) {
        $query->select("package_id")->from("ba_temp_booking_cart")->where("bin", $bin)->where("member_id", $member_id);

    })
    ->get();

    
    $package_specifications = DB::table('ba_business_service_profile')
    ->whereIn("service_product_id", function($query) use($bin) {
        $query->select("package_id")->from("ba_temp_booking_cart")->where("bin", $bin);
    })
    ->where('bin', $bin)
    ->get();
 

    $temp_cart_infos = DB::table("ba_temp_booking_cart") 
    ->where("bin", $bin )
    ->where("member_id", $member_id )
    ->get();

    if(isset($temp_cart_infos) && $temp_cart_infos->count() > 0)
    {
        $temp_cart_info  = $temp_cart_infos[0];
    }
    else
    {
      return redirect("/booking/business/prepare-service-booking?bin=" . $bin)
      ->with("err_msg", "Booking cart is empty. Please start by selectign a package!");
    }

    $slot_no = $temp_cart_info->starting_slot;
    $service_date = date('Y-m-d', strtotime($temp_cart_info->service_date));
    $staff_id = $temp_cart_info->staff_id;


    $available_slot = DB::table("ba_service_days") 
    ->where("bin", $bin )
    ->where("slot_number", $slot_no )
    ->whereDate("service_date", $service_date )
    ->select("slot_number as slotNo", "start_time as startTime", "end_time as endTime")
    ->first();

    //checking if slots are available
    if( !isset($available_slot))
    {
        return redirect('/booking/business/booking-calendar?bin=' . $bin )
        ->with("err_msg", "Please select an available timeslot!");
    }


    $staff_info = DB::table("ba_profile")
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereIn("ba_users.category", array("1", '10' ) )
    ->where("ba_profile.id", $staff_id )
    ->select("ba_profile.id as staffId", "fullname as staffName", "profile_image_url as profilePicture" ) 
    ->first();
 

     //checking if slots are available
    if( !isset($staff_info))
    {
        return redirect('/booking/business/booking-calendar?bin=' . $bin )
        ->with("err_msg", "Please select an available service staff!");
    }

    $data = array('bin'=>$bin, 'business' => $business, 'timeslot' => $available_slot,  'staff' => $staff_info, 
        'cust_info' =>$cust_info, 'service_date' =>$service_date, 'packages' => $packages, 
        'package_specifications' => $package_specifications );

    return view("store_front.appointment.appointment_summary")->with('data',$data);
    
    
}



  //make sns booking
  public function makeNewAppointment(Request $request)
  {

    if( !$request->session()->has('__user_id_' ))
    {
      return redirect("/shopping/login")->with("err_msg", "Please login to place an order!");
    }

    $member_id =  $cust_id = $request->session()->get('__member_id_' );
    $cust_info =  CustomerProfileModel::find( $cust_id) ;
    if(!isset($cust_info))
    {
      return redirect("/appointment")->with("err_msg",  "Missing data to perform action!");
    }

    $bin = $request->bin;
    $business = Business::find( $bin );
    if( !isset($business))
    {
      return redirect("/appointment")->with("err_msg",  "Business information not found!");
    }

    if( $request->bin == "" || $request->serviceDate == "" || $request->staffId == "" ||   $request->packageNos == ""   )
    {
      return redirect("/booking/business/review-order-summary?bin=" . $bin )->with("err_msg",  "Missing data to perform action!");
    }


    //collect customer information
    $cname =   $cust_info->fullname; 
    $cphone =   $cust_info->phone; 
    $caddress =   $cust_info->locality;
    $service_date = date('Y-m-d'  , strtotime($request->serviceDate ));

    $gateway_mode =  config('app.gateway_mode');
    $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );


    //fetching staff selected
    $selected_rows = DB::table("ba_temp_booking_cart")
    ->where("bin", $bin)
    ->where("member_id",  $member_id )
    ->get();
    
    if( !isset($selected_rows ) )
    {
      return redirect("/appointment/review-appointment-summary?bin=" . $bin )->with("err_msg",  'Your appointment order cart is empty!');
    }

    $selected_date = date('Y-m-d', strtotime(array_unique( $selected_rows->pluck("service_date")->toArray())[0] ));
    $selected_staff = array_unique( $selected_rows->pluck("staff_id")->toArray())[0];
    $slotno = array_unique( $selected_rows->pluck("starting_slot")->toArray())[0];


    $total_cost = $total_discount = $total_gst =  0.00;
    //calculating service duration
    $packages_selected = DB::table("ba_service_products")
    ->whereIn("id",  function($query) use($bin, $member_id){
      
      $query->select("package_id")->from("ba_temp_booking_cart")
      ->where("bin",  $bin )
      ->where("member_id",  $member_id );
    })
    ->get();
    
    $serviceDuration =0;
    $total_duration_required = 0;
    $serviceProductWithDuration = array();
    
    foreach($packages_selected as $item)
    {
      $serviceDuration = ( $item->duration_hr * 60 ) + $item->duration_min;
      $total_duration_required +=  ( $item->duration_hr * 60 ) + $item->duration_min;
      $serviceProductWithDuration[] = array( 'package_id' => $item->id, 'duration' => $serviceDuration );


      $total_cost +=  $item->pricing;
      $total_discount  += $item->discount;
      $actual_cost = $item->pricing   ;
      $total_gst +=  ( ( $item->cgst_pc +  $item->sgst_pc ) * $actual_cost * 0.01);


    }
     
    
    if( count($serviceProductWithDuration ) == 0 )
    {
      return redirect("/appointment/review-appointment-summary?bin=" . $bin )->with("err_msg",   'Failed to recalculate service duration.');
    }


    
    $otp = mt_rand(111111, 999999);//order OTP generation

    if( $request->homevisitservice != "")
    {
      //home visit
      //generate order sequencer
      $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
      $rp_1 = mt_rand(0, 9);
      $rp_2 = mt_rand(0, 9);
      $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;
      
      $sequencer = new OrderSequencerModel;
      $sequencer->type = "appointment";
      $sequencer->tracker_session =  $tracker;
      $save = $sequencer->save();
      //generate order sequencer ends
      
      if( !isset($save) )
      {
        $data= ["message" => "failure", "status_code" =>  997 , 'detailed_msg' => 'Failed to generate order no sequence.'  ];
        return json_encode($data);
      }
      
      //generate merchant sequencer
      $merchant_order_no =1;
      $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
      if(isset($merchant_order_info))
      {
        $merchant_order_no =  $merchant_order_info->last_order + 1;
      }
      else
      {
        $merchant_order_info = new MerchantOrderSequencer;
        $merchant_order_info->bin  = $request->bin;
      }
      //generate merchant sequencer ends
      
      //make new booking
      $newbooking = new ServiceBooking;
      $newbooking->id = $sequencer->id ;
      $newbooking->customer_name = $customer_info->fullname;
      $newbooking->customer_phone = $customer_info->phone;
      $newbooking->address = $customer_info->locality;
      $newbooking->landmark =$customer_info->landmark;
      $newbooking->city = $customer_info->city ;
      $newbooking->state = $customer_info->state;
      $newbooking->biz_order_no = $merchant_order_no;
      $newbooking->bin = $request->bin;
      $newbooking->book_category= $business->category;
      $newbooking->book_by = $user_info->member_id;
      $newbooking->staff_id = $selected_staff;
      $newbooking->book_status = "new";
      $newbooking->total_cost =  $total_cost;
      $newbooking->seller_payable =  $total_cost;
      $newbooking->discount = $total_discount;
      $newbooking->service_fee  =  0.00;
      $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
      $newbooking->cashback =  0;
      $newbooking->payment_type =  "online";
      $newbooking->latitude =  $newbooking->longitude =  0;
      $newbooking->book_date =  date('Y-m-d H:i:s');
      $newbooking->service_date =  $service_date;
      $newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
      $newbooking->transaction_no =  "na" ;
      $newbooking->reference_no =  "na"  ;
      $newbooking->preferred_time =  date('H:i:s', strtotime( $request->preferredTime));
      $newbooking->otp  =  $otp ;
      $newbooking->address = $customer_info->locality;
      $newbooking->landmark = $customer_info->landmark;
      $newbooking->city = $customer_info->city;
      $newbooking->state = $customer_info->state;
      $newbooking->pin_code  = $customer_info->pin_code;
      $save=$newbooking->save();
       $item_notes = array();
      if($save)
      {
        //update merchant order sequencer log
        $merchant_order_info->last_order  = $merchant_order_no;
        $merchant_order_info->save();
        //ending merchant order sequencer
      }
    }
    else
    {
      //check if slot is available for the day

      $day_name = date('l', strtotime( $selected_date ));
      if( $selected_staff  <=  0 ) //any staff
      {
        //find available staff
        $any_staff_available = DB::table("ba_service_days")
        ->where("slot_number", $slotno )
        ->whereDate("service_date",  $service_date )
        ->where("bin",$bin)
        ->where("status", "open")
        ->orderBy("slot_number", "asc")
        ->first();
        
        if(isset($any_staff_available) == 0)
        {
          return redirect("/appointment/review-appointment-summary?bin=". $bin)->with("err_msg", 
          'No staff available in the selected slot. Please change date or reduce sevice selected.');
        }
        $selected_staff = $any_staff_available->staff_id;
      } 

      //ascending order sort is important otherwise it will break code
      $available_slots = DB::table("ba_service_days")
      ->where("slot_number", ">=", $slotno )
      ->where("staff_id",  $selected_staff )
      ->whereDate("service_date",  $selected_date )
      ->where("bin",$bin)
      ->where("status", "open")
      ->orderBy("slot_number", "asc")
      ->get();
      
      $availableSlots = $book_slots = array();
      $total_slots_min = 0;
      $starting_slot_found = false;
      $ending_slot_found = false;
      
      
      $lastFoundSlot = $lastScannedSlot = 0;
      $scanCount =0;
      foreach($serviceProductWithDuration as $serviceProductDuration )
      {
        foreach($available_slots as $item )
        {
          if( $item->slot_number == $request->serviceTimeSlot  )
          {
            $starting_slot_found = true;
          }
          if($lastScannedSlot == 0 )
          {
            $lastScannedSlot = $item->slot_number;
          }
          //skip upto last processed slot from second scan onwards
          if( $scanCount > 0 && $lastScannedSlot >= $item->slot_number )
          {
            continue;
          }
          
          $lastScannedSlot = $item->slot_number;
          $start_time =   strtotime( $item->start_time );
          $end_time =   strtotime( $item->end_time );
          $gap =  abs( ( $end_time - $start_time) / 60);
          $serviceProductDuration['duration'] -= $gap;
          $total_slots_min += $gap;
          $total_duration_required -= $gap; //reducing total duration
          
          $availableSlots[] = array(
          'package_id' => $serviceProductDuration['package_id'],
          "slot" => $lastScannedSlot ,
          "duration" => abs($gap),
          "startTime" =>$start_time,
          );
          if( $serviceProductDuration['duration'] <= 0 )
          {
            break;
          }
        }
        $scanCount++;
      }
      
 
      
      $allowBooking = true;
      foreach($serviceProductWithDuration as $serviceDuration)
      {
        $totalDuration = $serviceDuration['duration'];
        foreach($availableSlots as $availableSlot)
        {
          if( $serviceDuration['package_id'] == $availableSlot['package_id'])
          {
            $totalDuration -= $availableSlot['duration'];
          }
        }
        
        if($totalDuration > 0  )
        {
          $allowBooking = false;
          break;
        }
      }

      // if no available slot is found or no starting slot is found or duration of the slot found is less than required
      //block booking
      //if( !$starting_slot_found ||  $total_duration_required > 0  )
      if(count($availableSlots) == 0 || !$starting_slot_found || !$allowBooking  )
      {
        return redirect("/appointment/review-appointment-summary?bin=" . $bin )->with("err_msg",  'Insufficient slots. Please change date or reduce sevice selected.' );
      }

      foreach($availableSlots as $availableSlot)
      {
        $book_slots[] = $availableSlot['slot'];
      }

      //generate order sequencer
      $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
      $rp_1 = mt_rand( 0, 9 );
      $rp_2 = mt_rand( 0, 9 );
      $tracker  = $keys[$rp_1] . $keys[$rp_2] . time();
      $sequencer = new OrderSequencerModel;
      $sequencer->type = "appointment";
      $sequencer->tracker_session =  $tracker;
      $save = $sequencer->save();
      //generate order sequencer ends
      
      if( !isset($save) )
      {
        return redirect("/appointment/review-appointment-summary?bin=" . $bin)->with("err_msg",  'Failed to generate order number sequence. Please retry again!'  );
      }

      //generate merchant sequencer
      $merchant_order_no =1;
      $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
      if(isset($merchant_order_info))
      {
        $merchant_order_no =  $merchant_order_info->last_order + 1;
      }
      else
      {
        $merchant_order_info = new MerchantOrderSequencer;
        $merchant_order_info->bin  = $request->bin;
      }
      //generate merchant sequencer ends
      
      //make new booking
      $newbooking = new ServiceBooking;
      $newbooking->id = $sequencer->id ;
      $newbooking->customer_name = $cname;
      $newbooking->customer_phone = $cphone ;
      $newbooking->address = $caddress;
      $newbooking->biz_order_no = $merchant_order_no;
      $newbooking->bin = $bin;
      $newbooking->book_category= $business->category;
      $newbooking->book_by = $cust_id;
      $newbooking->staff_id = $selected_staff;
      $newbooking->book_status = "new";
      $newbooking->total_cost =  $total_cost;
      $newbooking->seller_payable =  $total_cost;
      $newbooking->discount = $total_discount;
      $newbooking->gst =  $total_gst;
      $newbooking->service_fee  =  0.00;
      $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
      $newbooking->cashback =  0;
      $newbooking->payment_type = "online";
      $newbooking->latitude =  $newbooking->longitude =  0;
      $newbooking->book_date =  date('Y-m-d H:i:s');
      $newbooking->service_date =  $service_date;
      $newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
      $newbooking->transaction_no =  "na" ;
      $newbooking->reference_no =  "na"  ;
      $newbooking->otp  =  $otp ; 
      $save=$newbooking->save();
       $item_notes = array();
      if($save)
      {
        $newbooking = ServiceBooking::find(  $sequencer->id ); //auto increment has removed so need this line to fetch order.
        //update merchant order sequencer log
        $merchant_order_info->last_order  = $merchant_order_no;
        $merchant_order_info->save();
        //ending merchant order sequencer
        $pos = 0;
        $size = sizeof($availableSlots);
        
        foreach($packages_selected as $item)
        {

          if( $pos >= $size )
          {
            $pos = $size-1;
          }
          
          $startSlot = 0;
          $startTime = time();
          
          foreach($availableSlots as $availableSlot)
          {
            if( $availableSlot['package_id'] == $item->id  )
            {
              $startSlot = $availableSlot['slot'];
              $startTime = $availableSlot['startTime'];
              break;
            }
          }
          $newbookingDetails = new ServiceBookingDetails;
          $newbookingDetails->book_id = $sequencer->id;
          $newbookingDetails->service_product_id = $item->id    ;
          $newbookingDetails->service_name = $item->srv_name  ;
          $newbookingDetails->slot_no = $startSlot;
          $newbookingDetails->status = "new";
          $newbookingDetails->service_time = date('H:i:s', $startTime );
          $newbookingDetails->service_charge = $item->actual_price;
          $newbookingDetails->save();
          $pos++;
        }
        
        
        //updating occupied
        DB::table("ba_service_days")
        ->whereIn("slot_number" , $book_slots )
        ->whereDate("service_date", $service_date)
        ->where("bin", $bin )
        ->where("staff_id",  $selected_staff )
        ->where("status",  "open" )
        ->update(array("status" => "booked") ) ;

        //clearing temporary cart
        DB::table("ba_temp_booking_cart")
        ->where("bin", $bin )
        ->where("member_id", $cust_id )
        ->delete();

      }
    }
    
    
    //upload attachment
    if($request->attachment  != "" )
    {
      $folder_path =  config('app.app_cdn_path')   . "/assets/image/uploads/order_" . $sequencer->id .    "/";
      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0755, true, true);
      }
      $file =  $request->file('attachment');
      $originalname = $file->getClientOriginalName();
      $extension = $file->extension( );
      $filename =  "attach_" . $sequencer->id . "_" .  time() . ".{$extension}";
      $file->storeAs('uploads',  $filename );
      $imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename ,   $folder_path .  $filename );
      $newbooking->attachment = config('app.app_cdn') . "/assets/image/uploads/order_" . $sequencer->id .    "/" . $filename;
    }

 

    $return_url = URL::to('booking/order/payment-completed');
    $notifyUrl = '';
    $item_notes = array(                    
              "orderId" => $sequencer->id,
              "orderAmount" => ( $total_cost  + $total_gst ) * 100,
              "customerName" => $cname ,
              "customerPhone" => $cphone ,
              "customerEmail" => $cust_info->email =="" ? 'booktougi@gmail.com' : $cust_info->email,
              "returnUrl" => $return_url,
              "notifyUrl" => $notifyUrl,
            );

    $rzpapi = new Api(  $secure_key->key_id   , $secure_key->key_secret );
    $rzp_response =  $rzpapi->order->create(array
          ('receipt' => $sequencer->id ,
           'amount' => ( $total_cost  + $total_gst ) * 100,
           'currency' => 'INR', 
           'notes'=>  $item_notes
      ));

        if(isset($rzp_response))
        {
          //logging razorpay order ID
          $newbooking->rzp_id = $rzp_response->id;
          $newbooking->save();
          return redirect("/appointment/payment-in-progress?o=".   $sequencer->id ) ;
        }
        return redirect("/booking/view-order-details?key=" . $sequencer->id )->with('Booking placed successfully!');

 
       }

    protected function appointmentrazorpayPaymentProgress(Request $request)
    {
        $oid= $request->o;

        if($oid == "")
        {
            return redirect("/shopping/my-bookings")->with("err_msg", "Your order not found!") ;
        }
        $order_info = DB::table("ba_service_booking") 
        ->where("id", $oid)
        ->first();

        if( !isset( $order_info) )
        {
          return redirect("/shopping/my-bookings")->with("err_msg", "Your order is not found!") ;
        }
        $gateway_mode =  config('app.gateway_mode'); 
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );

        $prefill = array("name" => $order_info->customer_name , "email"=> "booktou@gmail.com", "contact" => $order_info->customer_phone);
        $notes = array("item" => $order_info->book_category, "price"=> $order_info->total_cost);
      
        $data = array(
            "orderno" => $order_info->id,
            "key" => $secure_key->key_id, 
            "amount" => ( $order_info->total_cost  + $order_info->gst ) * 100,
            "currency" => "INR",
            "name" => $order_info->customer_name, 
            "description" => $order_info->customer_name, 
            "image" => "https://booktou.in/public/store/image/logo.png",
            "order_id" => $order_info->rzp_id ,
            "callback_url" => URL::to("/shopping/payment-completed")  ,
            "prefill" =>  $prefill ,
            "notes" => $notes 
        );



        return view("store_front.appointment.appointment_checkout_progress")->with( "data", $data);
    }
      protected function appointmentrazorpayPaymentCompleted(Request $request)
      {
        $rzp_id= $request->rzid;

        if(  $rzp_id == "" )
        {
            return redirect("/shopping/checkout")->with("err_msg", "No matching online order found!") ;
        }
        //use eloquent model to allow saving any changes
        $order_info = ServiceBooking::where("rzp_id", $rzp_id)->first();

        if( !isset( $order_info) )
        {
         
          return redirect("/shopping/checkout")->with("err_msg", "Your order not found!") ;
        }
        $gateway_mode =  config('app.gateway_mode'); 
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );
        $razor_id =  $order_info->rzp_id;
        $response = $this->checkRazorPayOrderStatus( $razor_id, $gateway_mode );

        $payment_info =  $response['items'] ; 
        if(  isset(  $payment_info ) )
        {
            $payment_status = $response['items'][0]->status;
            $payment_id = $response['items'][0]->id;

            if(  strcasecmp( $payment_status , "captured") == 0 )
            {
                $order_info->rzp_pay_id =  $payment_id ;
                $order_info->payment_status  = "paid";
                $save=$order_info->save();
                return redirect("/shopping/my-bookings")->with("err_msg", "Your appointment is placed successfully.") ;
               
            }
            else
            {
                return redirect("/shopping/checkout")->with("err_msg", "Order payment pending!") ;
            }
        }
            else
            {
                return redirect("/shopping/checkout")->with("err_msg", "Order not found!") ;           
            }
        
    }




//removing one of the selected package from cart
protected function removePackageFromCart(Request $request)
{
  $member_id = $request->session()->has('__member_id_' ) ?  $request->session()->get('__member_id_' ) : -1;
  $bin = ( $request->bin == "" ? 0 : $request->bin);
  $business_info  = DB::table("ba_business")
  ->where("id", $bin )
  ->where("is_block", "no")
  ->where("is_open", "open")
  ->first();

  if (!$business_info) 
  {
    return redirect('/appointment');
  }

 
 
  if( strcasecmp($business_info->sub_module, "sns") != 0)
  {
    return redirect("/appointment")->with("err_msg" , "Business selected does not provide booking service!");
  }
  if( $request->package == "")
  {
    return redirect("/appointment/business/appointment-calendar?bin=" . $bin )->with("err_msg" , "No package selected!");
  } 
    DB::table("ba_temp_booking_cart")
    ->where("bin",  $bin )
    ->where("package_id", $request->package  )
    ->where("member_id", $member_id )
    ->delete();
  
  if( $request->return != "" && strcasecmp($request->return, "checkout") == 0)
  {
    return redirect("/appointment/review-appointment-summary?bin=" . $bin )->with("err_msg" , "Booking cart updated!");
  }
  else
  {
    return redirect("/appointment/business/appointment-calendar?bin=" . $bin )->with("err_msg" , "Booking cart updated!");
  }

    

}

}
