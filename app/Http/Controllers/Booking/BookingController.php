<?php

namespace App\Http\Controllers\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\file;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;


use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\CrsBookingDetails;
use App\User;
use App\CustomerProfileModel;
use App\CallbackEnquiryModel;
use App\QuoteRequestModel;
use App\SubscriberModel;
use App\OrderSequencerModel;
use App\ContactFormModel;
use App\Business;
use App\MerchantOrderSequencer;
use App\ServiceBookingDetails;
use App\PickAndDropRequestModel;
use App\ServiceRating;
use App\ShoppingBasketServiceProductModel;
use App\TempBookingCartModel;
use App\ServiceProductModel;


use Razorpay\Api\Api;
use App\Traits\Utilities;

class BookingController extends Controller
{
  use Utilities;
  public function bookingIndex(Request $request)
  {
    
      $name = "booking";
      //saving module selected in session
      $request->session()->put('module_active', 'booking');

      $browse = DB::table("ba_business_category")
      ->where("main_module", "Booking")
      ->where("is_public", "yes")
      ->get();

      $bookings = DB::table("ba_service_booking_details")
      ->select(DB::Raw("count(*) as productCount"),"service_product_id as productid")
      ->groupBy("service_product_id")
      ->orderBy("productCount",'desc')
      ->paginate(8);

      $prid = array();

      foreach($bookings as $items)
      {
        $prid[] = $items->productid;
      }

      $serviceProduct = DB::table("ba_service_products")
      ->whereIn("id",$prid)
      ->get();


      $biz_categories = DB::table("ba_business_category")
      ->where("main_module", "Shopping")
      ->where("is_active", "yes" )
      ->paginate(15);

      $popularservices=DB::table("ba_business")
      ->where("main_module", "Booking")
      ->where("sub_module", "PAA" )
       ->where("is_block", "no")
      ->where("is_open", "open")
      ->paginate(15);

      $bookingrelated=DB::table("ba_business")
      ->where("main_module", "Booking")
      ->where("sub_module", "paa" )
      ->where("is_block", "no")
      ->where("is_open", "open")
      ->paginate(5);

      $business = DB::table("ba_business")
      ->where("main_module", "BOOKING")
      ->where("sub_module","PAA")
      ->where("is_block", "no")
      ->where("is_open", "open")
      ->paginate(4);

      $shopping = DB::table("ba_shopping_basket")
      ->select(DB::Raw("count(*) as totalProduct"),"prsubid as serviceProductid")
      ->groupBy("prsubid")
      ->orderBy("totalProduct",'desc')
      ->paginate(8);

      $productid = $productsubid = array();

      foreach($shopping as $items)
      {
        $productsubid[] = $items->serviceProductid;
      }

      $product_variant = DB::table("ba_product_variant")
      ->whereIn('prsubid',$productsubid)
      ->get();

      foreach($product_variant as $items)
      {
        $productid[] = $items->prid;
      }

      $products = DB::table("ba_products")
      ->whereIn('id',$productid)
      ->select("category");


      $booking = DB::table("ba_water_booking_basket")
      ->select("category_name as category")
      ->groupBy('category');


      $appointment = DB::table("ba_service_booking_details")

      // ->select(DB::raw('distinct(service_product_id)as serviceProduct'))
      ->select(DB::Raw("count(*) as totalProduct"),"service_product_id as serviceProduct")
      ->groupBy("service_product_id")
      ->orderBy("totalproduct",'desc')
      ->paginate(15);

      $serviceprid = array();

      foreach($appointment as $items)
      {
        $serviceprid[] = $items->serviceProduct;
      }

      $all_categoris = DB::table("ba_service_products")
      ->whereIn("id",$serviceprid)
      ->union($booking)
      ->union($products)
      ->select("service_category as category")
      ->get();
      //$list = array_unique('category',$products) ;


      $module_active =  session()->get('module_active');

      $city = DB::table('ba_business')
      ->select(DB::Raw('distinct(city) as cityList'))
      ->where('main_module',$module_active)
      ->get();


      $package_categories = DB::table("ba_service_category")
      ->where("sub_module", "paa")
      ->whereIn("category", function( $query ) {
        $query->select("service_category")
        ->from("ba_service_products") 
        ->distinct();
      })
      ->get();


      $data = array('name'=>$name,"browses"=>$browse,"product"=>$serviceProduct, 
        'package_categories' => $package_categories,
        "biz_categories"=>$biz_categories,"bookingrelated"=>$bookingrelated,"business"=>$business, 
        "all_categoris"=>$all_categoris,"popularservices"=>$popularservices,'city'=>$city );

      return view("store_front.booking.index")->with($data); 

  }



  public function viewPackagesUnderCategory(Request $request)
  {
    $name = "booking";
    $categoryname = ( $request->category == "" ? "picnic" :  $request->category );

    $services = DB::table('ba_service_products')
    ->whereNotIn("bin", function($query){
      $query->select("id")->from("ba_business")->where("is_block", "yes");
    })
    ->whereRaw("replace(service_category, ' ', '-') like '%$categoryname%'")
    ->where("pricing", ">", 0)
    ->paginate(15);

    $bins = array();
    $all_services = array();
    foreach($services as $service)
    {
      $bins[] = $service->bin; 
      $all_services[] = $service;
    }
    $bins[] = 0; 

    $service_profiles = DB::table('ba_business_service_profile') 
    ->whereIn('bin', $bins )
    ->get();

    $businesses = DB::table('ba_business') 
    ->whereIn('id', $bins)
    ->get();

    shuffle($all_services);

    $data = array( 
      "businesses" =>$businesses,
      "services"=>$all_services, 
      'service_profiles' =>$service_profiles,
      "categoryname"=>$categoryname
    );
    return view("store_front.booking.booking_packages")->with($data);

  }



  //view packages by category name
  protected function viewPackagesByCategory(Request $request)
  {

    $name = $request->name;
    $maincategory = $request->categoryname;
    $mainmenu = explode("-", $maincategory );
    $category = implode(" ", $mainmenu );

    $business_category = DB::table('ba_business_category')
    ->where('name',$category)
    ->first();


    //business list
    $booking_list = DB::table("ba_bookable_category")
    ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
    ->where('sub_module',$business_category->sub_module)
    ->get();


    $submodule = $submodulename = $paa = $bin = $service_products = array();

    foreach($booking_list as $items)
    {

      $submodule[] = $items->sub_module; 
      $submodulename[] = $items->sub_module_name;

    }


    $businesses = DB::table("ba_business")
    ->whereIn("sub_module", $submodule )
    //->where('module_name',$category)
    ->where('is_open','open')
    ->select("ba_business.*",DB::Raw("'[]' products"))
    ->get();


    foreach($businesses as $item)
    {
      if ( strcasecmp($item->sub_module, "paa")== 0 ) 
      {
        $paa[] = $item->id;
      }else{
        $bin[] = $item->id;
      }
    }

    $services = DB::table('ba_service_products') 
    ->whereIn('bin', $paa)
    ->orderBy("bin" )
    ->get();

    
    $btssids = array(); 
    $i=0;
    foreach ($businesses as $items) 
    {
     
      $service_products[$i]['id'] = $items ;
      $prodlist = array();
      $items->products = array() ;

      foreach ($services as $sub_item)
      {
        $btssids[] = $sub_item->id; 
        if ($items->id==$sub_item->bin)
        {
          $prodlist[] = $sub_item; 
        }
      }

      $items->products = $prodlist ; 
      $i++;
    }

    $btssids[] = 0;

    $service_images = DB::table('ba_business_service_profile') 
    ->whereIn('service_product_id', $btssids)
    ->get();


    $servicecategory = DB::table('ba_service_products')
    ->select(DB::Raw("DISTINCT(service_category)","id"))
    ->whereIn('bin',$paa)
    ->get();

    $max_amount = DB::table("ba_service_products")
    ->whereIn("bin", function($query){
      $query->select("id")
      ->from("ba_business")
      ->where("service_category", 'PICNIC');
    })
    ->max("pricing");

    $min_amount = DB::table("ba_service_products")
    ->whereIn("bin", function($query){
      $query->select("id")
      ->from("ba_business")
      ->where("service_category", 'PICNIC');
    })
    ->min("pricing");

    // product list ends here
    $data = array("businesses"=>$businesses,
    "services"=>$services,
    'service_images' =>$service_images, 
    "min_amount"=>$min_amount,
    "servicecategory"=>$servicecategory,
    "max_amount"=>$max_amount,
    "name"=>$name,
    "category"=>$category,
    "mainmenu"=>$maincategory);
    return view("store_front.service.service_details")->with($data);
  } 



  public function appointmentBusiness(Request $request)
  {
    $name = $request->name;
    $bin = $request->bin;

    $Abusiness = DB::table('ba_business')
    ->where("id",$bin)
    ->first();

    $product = DB::table('ba_service_products')
    ->where("bin",$Abusiness->id)
    ->select("ba_service_products.*")
    ->get();

    $appointment = DB::table("ba_bookable_category")
    ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
    ->where('main_module',"APPOINTMENT")
    ->get();



    $submodule = $submodulename = $sns = array();

    foreach($appointment as $items)
    {
      $submodule[] = $items->sub_module;
      $submodulename[] = $items->sub_module_name;

    }

    $Amerchants = DB::table("ba_business")
    ->whereIn("sub_module", $submodule )
    ->where('is_verified','yes')
    ->get();

    // foreach($Amerchants as $item)
    // {
    // if ($item->sub_module=="SNS") {
    // $sns[] = $item->id;
    // }else{
    // $bin[] = $item->id;
    // }
    // }

    // product list for selected business
    if ($request->category=="")
    {

      $product = DB::table('ba_service_products')
      ->where('bin',$bin)
      ->get();


    }else if($request->category=="all")
    {

      $product = DB::table('ba_service_products')
      ->where("bin",$Abusiness->id)
      ->select("ba_service_products.*")
      ->get();

    }
    else
    {

      $Acategorylist = $request->category;
      $product = DB::table('ba_service_products')
      ->where('service_category',$Acategorylist)
      ->where('bin',$bin)
      ->get();

    }


    $acategory = DB::table('ba_service_products')
    ->select(DB::Raw("DISTINCT(service_category)","id"))
    ->where('bin',$bin)
    ->get();



    $data = array('product'=>$product,"acategory"=>$acategory,"name"=>$name,"Abusiness"=>$Abusiness);
    return view("store_front.service.appointment_business")->with($data);
  }

  
   


  protected function bookingViewServiceBusiness(Request $request)
  {

    if($request->serviceid=="")
    {
      return view('service/booking');
    }

    $service = DB::table('ba_service_products')
    ->where('id',$request->serviceid)
    ->first();

    if (!isset($service)) {
      return view('service/booking');
    }
    else
    {
      $name = $request->name;

      $id = $request->bin;

      $business = DB::table('ba_business')
      ->where("id",$id)
      ->first();

      // dd(request()->get('MAIN_MODULES'));
      // not found

      $product = DB::table('ba_service_products')
      ->where("bin",$business->id)
      ->select("ba_service_products.*")
      ->get();

      $bookable = DB::table("ba_bookable_category")
      ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
      ->where('main_module',"BOOKING")
      ->get();

      $submodule = $submodulename = $wtr = $bin = array();

      foreach($bookable as $items)
      {

        $submodule[] = $items->sub_module;
        $submodulename[] = $items->sub_module_name;
      }

      // $merchants = DB::table("ba_business")
      // ->whereIn("sub_module", $submodule )
      // ->where('is_verified','yes')
      // ->get();

      // foreach($merchants as $item)
      //  {
      //     if ($item->sub_module=="WTR") {
      //        $wtr[] = $item->id;

      //     }else{

      //       $bin[] = $item->id;
      //     }
      //  }
      // // product list for selected business
      //  if ($request->category=="")
      //   {
      //    $product = DB::table('ba_service_products')
      //    ->whereIn('bin',$wtr)
      //    ->get();

      //   }
      // else
      // {

      //   $categorylist = $request->category;

      //    $product = DB::table('ba_service_products')
      //     ->whereIn('service_category',$categorylist)
      //     ->whereIn('bin',$wtr)
      //     ->get();

      // }

      $category = DB::table('ba_service_products')
      ->select(DB::Raw("DISTINCT(service_category)","id"))
      ->whereIn('bin',$wtr)
      ->get();

      $data = array("merchants"=>$merchants,'product'=>$product,"category"=>$category,"name"=>$name,"business"=>$business);
      return view("store_front.service.booking_business")->with($data);
    }

  }


  // fetching service details method
  protected function getServiceDetails(Request $request)
  {

    if($request->serviceProductId == ""  || $request->bin=="")

    {
      return redirect('appointment/appointment-details');
    }


    $service_details = DB::table("ba_business_service_profile")
    ->select( "id as headingId", "service_product_id as serviceProductId", "heading", "description", "photos")
    ->where("service_product_id" ,  $request->serviceProductId)
    ->get();



    foreach($service_details  as $item)
    {  if($item->photos == "" || $item->photos == null  )
    {
      $item->photos =  "";
    }


  }

  $returnHTML = view('store_front.service.view_service_details')->with('result',$service_details)->render();

  return response()->json(array('success'=>true,'status_code'=>7003, 'html'=>$returnHTML));


}




protected function searchBookingServices(Request $request)
{
  
  $module_active = session()->get('module_active'); 
  if ($module_active=="") {
    return redirect()->back();
  }

  if($request->keyword == "" || $request->city == ""   )
  {
    return redirect('/booking');
  }

  $keyword = $request->keyword;
  $cityname = $request->city;

  $businesses = DB::table("ba_business")
  ->select('id','name')
  ->where("main_module", strtoupper($module_active))
  ->where('city', $cityname)
  ->get();

  $bins = array(0);
  foreach( $businesses  as $business)
  {
    $bins[] = $business->id;
  }


  $service_profiles = DB::table('ba_business_service_profile') 
  ->whereIn('bin', $bins)
  ->get();

  $services  = DB::table("ba_service_products")
  ->whereIn("bin", function($query) use ( $cityname ){

    $query->select("id")
    ->from("ba_business")
    ->where("city", $cityname);

  })
  ->where("srv_name", "like", "%$keyword%")
  ->orWhere("srv_details", "like", "%$keyword%")
  ->orWhere('service_category',  "like", "%$keyword%") 
  ->paginate(10); 



  $all_services = array();
  foreach( $services  as $service)
  {
    $all_services[] = $service;
  } 

  shuffle($all_services);   
  $data = array( "services"=>$all_services, 
    'service_profiles' =>$service_profiles,
    'businesses' => $businesses  
  );
  return view("store_front.booking.search_services_result")->with($data); 
}

protected function viewByBookingMenuCategoryName(Request $request)
{
  $categoryname = $request->name;

  if ($categoryname=="") {
    return redirect()->back();
  }

  $service_products = DB::table('ba_service_products')
  ->where('service_category',$categoryname)
  ->get();

  $bin = array();
  foreach($service_products as $products)
  {
    $bin[] = $products->bin;
  }

  $business_list = DB::table('ba_business')
  ->whereIn('id',array_unique($bin))
  ->get();

  $data = array('products'=>$service_products,'business'=>$business_list);
  return view('store_front.service.view_by_booking_menu')->with($data);


}

protected function viewByAppointmentCategoryName(Request $request)
{
  $categoryname = $request->name;

  if ($categoryname=="") {
    return redirect()->back();
  }

  $service_products = DB::table('ba_service_products')
  ->where('service_category',$categoryname)
  ->get();

  $bin = array();
  foreach($service_products as $products)
  {
    $bin[] = $products->bin;
  }

  $business_list = DB::table('ba_business')
  ->whereIn('id',array_unique($bin))
  ->get();

  $data = array('products'=>$service_products,'business'=>$business_list);
  return view('store_front.service.view_by_apppointment_menu')->with($data);
}


// protected function bookANewService(Request $request)
// {
//     if ($request->bin =="") 
//     {
//       return redirect()->back();
//     }
//     $bin = $request->bin;
//     $srv_id  = $request->srv_code;
//     $selected_service = DB::table("ba_service_products")
//     ->where("id",$srv_id)
//     ->first();

//     if (!$selected_service) {
//       $srv_id  = 0;
//     }

//     $business_info  = DB::table("ba_business")
//     ->where("id", $selected_service->bin)
//     ->where("is_block", "no")
//     ->where("is_open", "open")
//     ->first();

//     if (!$business_info) {
//       return redirect()->back();
//     } 

//     $request->session()->put('module_active', strtolower($business_info->main_module ) ); 

//     $packages = DB::table("ba_service_products")
//     ->where("bin", $business_info->id  )
//     ->whereNotIn('service_category',['OFFER'])
//     ->orderBy("service_category","desc")
//     ->get();

//     $service_profiles = DB::table('ba_business_service_profile') 
//     ->where('bin', $bin)
//     ->get();

//     $all_categories = array("none");
//     foreach($packages as $item)
//     {
//       if( !in_array($item->service_category, $all_categories ) )
//         $all_categories[] =  $item->service_category;
//     }

//     $package_categories = DB::table("ba_service_category") 
//     ->whereIn("category", $all_categories)
//     ->get();



//     $data = array( 'service_id' => $srv_id,
//         'business'=>$business_info,
//         'selected_service'=>$selected_service,
//         'service_products'=>$packages,
//         'package_categories' => $package_categories, 
//         'service_profiles' =>$service_profiles );
//     return view('store_front.booking.prepare_new_booking')->with($data);

// }

protected function bookANewService(Request $request)
{
    if ($request->bin =="") 
    {
      return redirect()->back();
    }
    $bin = $request->bin;
    $srv_id  = $request->srv_code;
    $selected_service = DB::table("ba_service_products")
    ->where("id",$srv_id)
    ->first();
    if (!$selected_service) {
      $srv_id  = 0;
    }

    $business_info  = DB::table("ba_business")
    ->where("id", $selected_service->bin)
    ->where("is_block", "no")
    ->where("is_open", "open")
    ->first();

    if (!$business_info) {
      return redirect()->back();
    } 

    $request->session()->put('module_active', strtolower($business_info->main_module ) ); 
    

    if( strcasecmp($business_info->sub_module, "paa") == 0)
    {
        $packages = DB::table("ba_service_products")
        ->where("bin", $business_info->id  )
        ->whereNotIn('service_category',['OFFER'])
        ->orderBy("service_category","desc")
        ->get();

        $service_profiles = DB::table('ba_business_service_profile') 
        ->where('bin', $bin)
        ->get();

        $all_categories = array("none");
        foreach($packages as $item)
        {
          if( !in_array($item->service_category, $all_categories ) )
            $all_categories[] =  $item->service_category;
        }

        $package_categories = DB::table("ba_service_category") 
        ->whereIn("category", $all_categories)
        ->get();

        $available_slots = DB::table("ba_service_days")
        ->where("bin", $bin )
        ->where("year",  date('Y') )
        ->where("month",  date('m') )
        ->select(DB::raw('staff_id, slot_number, start_time, end_time ')) 
        ->groupBy("slot_number")
        ->groupBy("start_time")
        ->groupBy("end_time")
        ->groupBy("staff_id")
        ->get();

  $staff_ids = $available_slots->pluck("staff_id");

  $staff_available = DB::table("ba_profile")
    ->whereIn("id",  function($query) use (  $bin )
    {
      $query->select("profile_id") 
      ->from('ba_users')
      ->where("bin",  $bin );  
    })
    ->select("id as staffId", "fullname as staffName", "profile_image_url as profilePicture" ) 
    ->get();



        $data = array( 'service_id' => $srv_id,
            'business'=>$business_info,
            'selected_service'=>$selected_service,
            'service_products'=>$packages,
            'package_categories' => $package_categories, 
            'available_slots' => $available_slots,
            'service_profiles' =>$service_profiles,
            'staff_available' => $staff_available );

        return view('store_front.booking.prepare_new_booking')->with($data);
    }
    else  if( strcasecmp($business_info->sub_module, "crs") == 0)
    {

      $packages = DB::table("ba_service_products")
      ->where("bin",  $bin)
      ->whereNotIn('service_category',['OFFER'])
      ->orderBy("service_category","desc")
      ->get();

      $package_specifications = DB::table('ba_business_service_profile') 
      ->where('bin', $bin)
      ->get();

      $all_categories = array("none");
      foreach($packages as $item)
      {
        if( !in_array($item->service_category, $all_categories ) )
          $all_categories[] =  $item->service_category;
      }

      $package_categories = DB::table("ba_service_category") 
      ->whereIn("category", $all_categories)
      ->get();


        $data = array( 
        'service_id' => $srv_id,    
        'selected_service'=>$selected_service,
        "business"=>$business_info, 
        'packages'=> $packages ,
        'package_categories' => $package_categories, 
        'package_specifications' =>$package_specifications);
  
        return view('store_front.booking.crs_booking_page')->with($data);
    }
    else
    {
      return redirect("/booking")->with("err_msg" , "No matching business found!");
    }    

}

public function makeCrsBooking(Request $request)
{

  if( !$request->session()->has('__user_id_' ))
  {
    return redirect("/business/prepare-booking/". $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Please login to view your profile!");
  }

  $gateway_mode =  config('app.gateway_mode');       
  $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );
  $uid = $request->session()->get('__user_id_' );
  $user_info =  json_decode($this->getUserInfo($uid));
  if(  $user_info->user_id == "na" )
  {
    return redirect("/booking/business/prepare-booking/". $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "User has no priviledge to access this feature! Please consult admin.!");
  }

  $addressCustomer = $user_info->userAddress;
  $customer_info = CustomerProfileModel::find($user_info->member_id);

  if(!isset($customer_info))
  {
    return redirect("/booking/business/prepare-booking/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg",'No matching customer found!');
  }

  if(  $request->serviceDate != "" )
  {
    $service_date = date('Y-m-d'  , strtotime($request->serviceDate )); //service on specified date
  }
  else
  {
    $service_date = date('Y-m-d');
  }



  $business = Business::find( $request->key );
  if( !isset($business))
  {
    return redirect("/booking/business/prepare-booking/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Business information not found!");
  }
      

  if(!isset($request->btnorder))
  {
    return redirect("/booking/business/prepare-booking/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "click book button to make booking");
  }

  $book_status = "pending-payment";
  $paymentType = "ONLINE";
  $salesorderno =  0;
  $serviceProductIds = $request->serviceProductIds;


      if(sizeof($serviceProductIds) == 0 )
      {
        return redirect("/booking/business/prepare-booking/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Missing data to perform action!");
      }          
      $service_products = DB::table("ba_service_products")
      ->where("bin", $request->key )
      ->whereIn("id", $serviceProductIds)
      ->get();

      if( !isset($service_products) )
      {
        return redirect("/booking/business/prepare-booking/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "No service products found!");
      }
        
      $total_duration_required = 0;
      $total_cost= $total_discount = 0.00;
      $serviceProductWithDuration = array();
      foreach($serviceProductIds as $serviceProduct)
      {
        $serviceDuration =0;
        foreach($service_products as $item)
        {
          if($serviceProduct == $item->id )
          {
            $serviceDuration += ( $item->duration_hr * 60 ) + $item->duration_min;
            $total_duration_required +=  ( $item->duration_hr * 60 ) + $item->duration_min;
            $total_cost +=  $item->actual_price;
            $total_discount  += $item->discount;
          }
        }
        $serviceProductWithDuration[] = array( 'serviceProductId' => $serviceProduct,
          'duration' => $serviceDuration );
      }

     // dd($request->subModule);
        
        $otp = mt_rand(111111, 999999);//order OTP generation
        if( strcasecmp($request->subModule ,"crs" ) ==0 )
        {

            if($request->serviceDate == "" ||   $request->serviceProductIds == ""  )
            {
              return redirect("/booking/business/prepare-booking/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Service date is missing!");
            }
          
            $staffId = 0;
            
            $booked_dates  = DB::table("ba_service_booking")
            ->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
            ->whereDate("ba_service_booking.service_date", $service_date   )
            ->whereIn("ba_service_booking_details.service_product_id",  $serviceProductIds )
            ->where("ba_service_booking.bin", $request->bin)
            ->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment') )
            ->select("ba_service_booking.id as book_id", "service_date",  "ba_service_booking_details.service_product_id",
              DB::Raw("'close' status") )
            ->get();          
            $days = array();
            $status ="open";
            $found = false;
            //if any service is close for booking the rest of the service in the selection will be blocked from booking
            foreach($serviceProductIds as $service_product_id)
            {
              foreach($booked_dates as $item)
              {
                if(  $item->service_product_id == $service_product_id)
                {
                  $found = true;
                  break;
                }
              }
              
              if( $found )
              {
                $status ="close";
                break;
              }
            }   
          
            if(  $found )
            {
              return redirect("/booking/business/prepare-booking/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Selected service is already booked. Please choose another date or change service.");
              
            }
          
            //no need to check slot as whole day is booked
            $slotno = 0; //assuming full day
            
            
            //generate order sequencer
            $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
            $rp_1 = mt_rand(0, 9);
            $rp_2 = mt_rand(0, 9);
            $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;
            
            $sequencer = new OrderSequencerModel;
            $sequencer->type = "booking";
            $sequencer->tracker_session =  $tracker;
            $save = $sequencer->save();
            //generate order sequencer ends
            if( !isset($save) )
            {
              return redirect("/booking/business/prepare-booking/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Failed to generate order no sequence.");
            }
          
            $booking_detail = new CrsBookingDetails;
            $booking_detail->id = $sequencer->id;
            $booking_detail->pickup_time = date('Y-m-d', strtotime($request->pickupTime));
            $booking_detail->pickup_from = $request->pickupFrom;
            $booking_detail->drop_at = $request->dropAt;
            $booking_detail->pickup_latitude = ($request->pickupLatitude == "" ? 0 : $request->pickupLatitude);
            $booking_detail->pickup_longitude = ($request->pickupLongitude == "" ? 0 : $request->pickupLongitude);
            $booking_detail->drop_latitude = ($request->dropLatitude == "" ? 0 : $request->dropLatitude);
            $booking_detail->drop_longitude = ($request->dropLongitude == "" ? 0 : $request->dropLongitude);
            $booking_detail->package_id = $request->serviceProductIds[0];
            $save = $booking_detail->save();
             $item_notes = array();
            
            if( !isset($save) )
            {
               return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Failed to generate order no crs.");
            }

          //generate merchant sequencer
            $merchant_order_no =1;
            $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
            if(isset($merchant_order_info))
            {
              $merchant_order_no =  $merchant_order_info->last_order + 1;
            }
            else
            {
              $merchant_order_info = new MerchantOrderSequencer;
              $merchant_order_info->bin  = $request->bin;
            }
            //generate merchant sequencer ends
            
              //make new booking
              $newbooking = new ServiceBooking;
              $newbooking->id = $sequencer->id;
              $newbooking->customer_name = $customer_info->fullname;
              $newbooking->customer_phone = $customer_info->phone;
              $newbooking->address = $customer_info->locality;
              $newbooking->landmark =$customer_info->landmark;
              $newbooking->city = $customer_info->city ;
              $newbooking->state = $customer_info->state;
              $newbooking->biz_order_no = $merchant_order_no;
              $newbooking->bin = $request->bin;
              $newbooking->book_category= $business->category;
              $newbooking->book_by = $user_info->member_id;
              $newbooking->staff_id = $staffId;
              $newbooking->book_status = "new";
              $newbooking->total_cost =  $total_cost;
              $newbooking->seller_payable =  $total_cost;
              $newbooking->discount = $total_discount;
              $newbooking->service_fee  =  0.00;
              $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
              $newbooking->cashback =  0;
              $newbooking->payment_type =   $request->payMode != "" ?  $request->payMode : "OTC";   //over the counter
              $newbooking->latitude =  $newbooking->longitude =  0;
              $newbooking->book_date =  date('Y-m-d H:i:s');
              $newbooking->service_date =  $service_date;
              $newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
              $newbooking->transaction_no =  "na" ;
              $newbooking->reference_no =  "na"  ;
              $newbooking->preferred_time =  date('H:i:s', strtotime( $request->preferredTime));
              $newbooking->otp = $otp ;
              $newbooking->address = $customer_info->locality;
              $newbooking->landmark = $customer_info->landmark;
              $newbooking->city = $customer_info->city;
              $newbooking->state = $customer_info->state;
              $newbooking->pin_code  = $customer_info->pin_code;
              $save=$newbooking->save();
               $item_notes = array();
              if($save)
              {
                //update merchant order sequencer log
                $merchant_order_info->last_order  = $merchant_order_no;
                $merchant_order_info->save();
                //ending merchant order sequencer
                
                //updatig which service were booked in this order
                $book_slots = array();
                foreach($service_products as $item)
                {
                  $newbookingDetails = new ServiceBookingDetails;
                  $newbookingDetails->book_id = $sequencer->id;
                  $newbookingDetails->service_product_id = $item->id    ;
                  $newbookingDetails->service_name = $item->srv_name  ;
                  $newbookingDetails->slot_no = $slotno ;
                  $newbookingDetails->status = "new";
                  $newbookingDetails->service_time = date('H:i:s', strtotime( $request->preferredTime));
                  $newbookingDetails->service_charge = $item->actual_price;
                  $newbookingDetails->save();
                }
                //updating occuppied
                DB::table("ba_service_days")
                ->whereDate("service_date", $service_date)
                ->where("bin",$request->bin)
                ->update(array("status" => "booked") ) ;
              }
            }
        
            //upload attachment
            if($request->attachment  != "" )
            {
              $folder_path =  config('app.app_cdn_path')   . "/assets/image/uploads/order_" . $sequencer->id .    "/";
              if( !File::isDirectory($folder_path))
              {
                File::makeDirectory( $folder_path, 0755, true, true);
              }
              
              $file =  $request->file('attachment');
              $originalname = $file->getClientOriginalName();
              $extension = $file->extension( );
              $filename =  "attach_" . $sequencer->id . "_" .  time() . ".{$extension}";
              $file->storeAs('uploads',  $filename );
              $imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename ,   $folder_path .  $filename );
              $newbooking->attachment = config('app.app_cdn') . "/assets/image/uploads/order_" . $sequencer->id .    "/" . $filename;
            }
        
        $return_url = URL::to('/payment/response') ;
             $notifyUrl = '';
             $item_notes = array(                    
              "orderId" => $sequencer->id,
              "orderAmount" => $total_cost,
              "customerName" => $customer_info->fullname,
              "customerPhone" => $customer_info->phone,
              "customerEmail" => $customer_info->email =="" ? 'booktougi@gmail.com' : $customer_info->email,
              "returnUrl" => $return_url,
              "notifyUrl" => $notifyUrl,
            );

                $rzpapi = new Api(  $secure_key->key_id   , $secure_key->key_secret );
                $rzp_response =  $rzpapi->order->create(array
                    ('receipt' => $sequencer->id ,
                     'amount' => $total_cost * 100 ,
                     'currency' => 'INR', 
                     'notes'=>  $item_notes                   
                 ));

                if(isset($rzp_response->id))
                {
                //logging razorpay order ID
                  $update_booking = ServiceBooking::find($sequencer->id);
                  $update_booking->rzp_id = $rzp_response->id;
                  $update_booking->save();

                  return redirect("/booking/business/payment-in-progress?o=".$sequencer->id ) ;
               
                }  
                 else
                 {
                    return redirect("/booking")->with('Your order could not be placed. Please contact sales');

                 }
     
    }  




//booking business landing page
protected function prepareServiceBooking(Request $request)
{
    if ($request->bin =="") 
    {
      return redirect()->back();
    }
    $bin = $request->bin;

 

 
    $business_info  = DB::table("ba_business")
    ->where("id", $bin )
    ->where("is_block", "no")
    ->where("is_open", "open")
    ->first();

    if (!$business_info) 
    {
      return redirect()->back();
    }

    $request->session()->put('module_active', strtolower($business_info->main_module ) ); 
    

    if( strcasecmp($business_info->sub_module, "paa") == 0)
    {
      if( $request->category == "")
      {
        $packages = DB::table("ba_service_products")
        ->where("bin", $bin )
        ->whereNotIn('service_category',['offer'])
        ->orderBy("service_category","desc")
        ->get();
      }
      else
      {
        $packages = DB::table("ba_service_products")
        ->where("bin", $bin )
        ->whereNotIn('service_category',['offer'])
        ->where('service_category', $request->category )
        ->orderBy("service_category","desc")
        ->get();
      }

      $service_profiles = DB::table('ba_business_service_profile') 
      ->where('bin', $bin)
      ->get();

      $package_categories =  DB::table("ba_service_category")
      ->whereIn("category", function($query) use($bin){
        $query->select("service_category")->from("ba_service_products")->where("bin", $bin);
      })
      ->get();
  

        $data = array(  
            'business'=>$business_info, 
            'packages'=>$packages, 
            'package_categories' => $package_categories,   
            'service_profiles' =>$service_profiles );

                   


        return view('store_front.booking.business_profile')->with($data);
    }
    else  if( strcasecmp($business_info->sub_module, "crs") == 0)
    {

      $packages = DB::table("ba_service_products")
      ->where("bin",  $bin)
      ->whereNotIn('service_category',['OFFER'])
      ->orderBy("service_category","desc")
      ->get();

      $package_specifications = DB::table('ba_business_service_profile') 
      ->where('bin', $bin)
      ->get();

      $all_categories = array("none");
      foreach($packages as $item)
      {
        if( !in_array($item->service_category, $all_categories ) )
          $all_categories[] =  $item->service_category;
      }

      $package_categories = DB::table("ba_service_category") 
      ->whereIn("category", $all_categories)
      ->get();


        $data = array( 
        'service_id' => $srv_id,    
        'selected_service'=>$selected_service,
        "business"=>$business_info, 
        'packages'=> $packages ,
        'package_categories' => $package_categories, 
        'package_specifications' =>$package_specifications);
  
        return view('store_front.booking.crs_booking_page')->with($data);
    }
    else
    {
      return redirect("/booking")->with("err_msg" , "No matching business found!");
    }    

}


//booking business landing page
protected function bookingCalendar(Request $request)
{

  $member_id = $request->session()->has('__member_id_' ) ?  $request->session()->get('__member_id_' ) : -1;
  $session_id = $request->session()->has('shopping_session' ) ?  $request->session()->get('shopping_session' ) : null;

  if ($request->bin =="") 
  {
    return redirect()->back();
  }
  $bin = $request->bin;


  
    $business_info  = DB::table("ba_business")
    ->where("id", $bin )
    ->where("is_block", "no")
    ->where("is_open", "open")
    ->first();

    if (!$business_info) 
    {
      return redirect()->back();
    }
    $request->session()->put('module_active', strtolower($business_info->main_module ) ); 
    

    if( strcasecmp($business_info->sub_module, "paa") == 0)
    {
      if( $request->package != "")
      {
        $package_info = ServiceProductModel::find($request->package);

        $temp_booking = TempBookingCartModel::where("bin",  $bin )
        ->where("package_id", $request->package  )
        ->where("member_id", $member_id )
        ->first();
        
        if( !isset($temp_booking))
        {
          $temp_booking = new TempBookingCartModel;
          $temp_booking->service_date= date('Y-m-d');
        }
        $temp_booking->bin=$request->bin;
        $temp_booking->package_id  =$request->package;
        $temp_booking->amount = isset( $package_info ) ? $package_info->actual_price : 0 ;
        $temp_booking->gst = isset( $package_info ) ? (  $package_info->cgst_pc + $package_info->sgst_pc ) *  $package_info->actual_price * 0.01 : 0 ;
        $temp_booking->member_id = $member_id;
        $temp_booking->session_id =  $session_id;
        $temp_booking->save();
      }

      if($member_id > 0)
      {
        //updating all entries saved using session id with member id
        DB::table("ba_temp_booking_cart") 
        ->where("bin", $bin)
        ->where("session_id",  $session_id )
        ->orWhere("member_id",  $member_id )
        ->update([ 'member_id' => $member_id, 'session_id' => $session_id ]);


        $selected_packages = DB::table("ba_temp_booking_cart") 
        ->where("bin", $bin)
        ->where("member_id",  $member_id )
        ->get();

      }
      else
      {
        $selected_packages = DB::table("ba_temp_booking_cart") 
        ->where("bin", $bin)
        ->where("session_id",  $session_id )
        ->get();
      }






      $package_ids = $selected_packages->pluck("package_id")->toArray();
   
      if(count($package_ids) == 0)
      {
          return redirect('/booking/business/prepare-service-booking?bin=' . $bin )
          ->with("err_msg", "No package is selected!");
      }


      $service_days_slot = DB::table("ba_service_days") 
      ->where("bin", $bin )
      ->whereDate("service_date",  date('Y-m-d') )
      ->whereIn("package_id",  $package_ids )
      ->select("slot_number as slotNo", "start_time as startTime", "end_time as endTime", DB::Raw(" group_concat(status) as statuses"))
      ->groupBy("slotNo")
      ->groupBy("startTime")
      ->groupBy("endTime")
      ->get();

    if( isset($service_days_slot) && $service_days_slot->count() > 0 )
    {
        $package_statuses =  explode(",",  $service_days_slot[0]->statuses );
    }
    else
    {
        $package_statuses =   array("close");
    }
    
    if(count($package_statuses) == count($package_ids))
    {
        $slot_status = ( count(array_intersect( array( 'engage', 'ended', 'booked' ) , $package_statuses))) ? 'close' : 'open';
    }
    else
    {
        $slot_status =  'close' ;
    }


    $packages = DB::table("ba_service_products")
    ->whereIn("id",  $package_ids)
    ->get();


    $staffs = DB::table("ba_profile")
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereIn("ba_users.category", array("1", '10' ) )
    ->where("bin", $bin ) 
    ->select("ba_profile.id as staffId", "fullname as staffName", "profile_image_url as profilePicture" ) 
    ->get();


    $data = array(  
      'business'=>$business_info, 
      'timeslots' => $service_days_slot,
      'slot_status' => $slot_status,
      'selected_packages' => $selected_packages,
      'packages' => $packages, 
      'staffs'  => $staffs );
    return view('store_front.booking.booking_calendar')->with($data);
    }
    else
    {
      return redirect("/booking")->with("err_msg" , "No matching business found!");
    }    

}



//removing one of the selected package from cart
protected function removePackageFromCart(Request $request)
{
  $member_id = $request->session()->has('__member_id_' ) ?  $request->session()->get('__member_id_' ) : -1;
  $bin = ( $request->bin == "" ? 0 : $request->bin);
  $business_info  = DB::table("ba_business")
  ->where("id", $bin )
  ->where("is_block", "no")
  ->where("is_open", "open")
  ->first();

  if (!$business_info) 
  {
    return redirect('/booking');
  }

 
  if( strcasecmp($business_info->sub_module, "paa") != 0)
  {
    return redirect("/booking")->with("err_msg" , "Business selected does not provide booking service!");
  }
  if( $request->package == "")
  {
    return redirect("/booking/business/booking-calendar?bin=" . $bin )->with("err_msg" , "No package selected!");
  }

    DB::table("ba_temp_booking_cart")
    ->where("bin",  $bin )
    ->where("package_id", $request->package  )
    ->where("member_id", $member_id )
    ->delete();
  
  if( $request->return != "" && strcasecmp($request->return, "checkout") == 0)
  {
    return redirect("/booking/business/review-order-summary?bin=" . $bin )->with("err_msg" , "Booking cart updated!");
  }
  else
  {
    return redirect("/booking/business/booking-calendar?bin=" . $bin )->with("err_msg" , "Booking cart updated!");
  }

    

}



  protected function reviewOrderSummary(Request $request)
  {

    $return_url = url()->full();
    $member_id = $request->session()->has('__member_id_' ) ?  $request->session()->get('__member_id_' ) : -1;
    $session_id = $request->session()->has('shopping_session' ) ?  $request->session()->get('shopping_session' ) : null;

    $cust_info =  CustomerProfileModel::find( $member_id);
    if(!isset($cust_info))
    {
      $request->session()->put('return_url', $return_url );
      return redirect("/shopping/login")->with("err_msg",  "Please login to complete booking process!");
    }


    $bin = $request->bin;
    if($bin == "")
    {
      return redirect('/booking');
    }
    $business= Business::find( $bin);
    if( !isset($business))
    {
        return redirect('/booking')->with("err_msg", "No matching business found!");
    }
 

    $packages = DB::table("ba_service_products")
    ->whereIn("id", function($query) use($bin, $member_id ) {
        $query->select("package_id")->from("ba_temp_booking_cart")->where("bin", $bin)->where("member_id", $member_id);

    })
    ->get();

    
    $package_specifications = DB::table('ba_business_service_profile')
    ->whereIn("service_product_id", function($query) use($bin) {
        $query->select("package_id")->from("ba_temp_booking_cart")->where("bin", $bin);
    })
    ->where('bin', $bin)
    ->get();
 

    $temp_cart_infos = DB::table("ba_temp_booking_cart") 
    ->where("bin", $bin )
    ->where("member_id", $member_id )
    ->get();

    if(isset($temp_cart_infos) && $temp_cart_infos->count() > 0)
    {
        $temp_cart_info  = $temp_cart_infos[0];
    }
    else
    {
      return redirect("/booking/business/prepare-service-booking?bin=" . $bin)
      ->with("err_msg", "Booking cart is empty. Please start by selectign a package!");
    }

    $slot_no = $temp_cart_info->starting_slot;
    $service_date = date('Y-m-d', strtotime($temp_cart_info->service_date));
    $staff_id = $temp_cart_info->staff_id;


    $available_slot = DB::table("ba_service_days") 
    ->where("bin", $bin )
    ->where("slot_number", $slot_no )
    ->whereDate("service_date", $service_date )
    ->select("slot_number as slotNo", "start_time as startTime", "end_time as endTime")
    ->first();

    //checking if slots are available
    if( !isset($available_slot))
    {
        return redirect('/booking/business/booking-calendar?bin=' . $bin )
        ->with("err_msg", "Please select an available timeslot!");
    }


    $staff_info = DB::table("ba_profile")
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereIn("ba_users.category", array("1", '10' ) )
    ->where("ba_profile.id", $staff_id )
    ->select("ba_profile.id as staffId", "fullname as staffName", "profile_image_url as profilePicture" ) 
    ->first();
 

     //checking if slots are available
    if( !isset($staff_info))
    {
        return redirect('/booking/business/booking-calendar?bin=' . $bin )
        ->with("err_msg", "Please select an available service staff!");
    }

    $data = array('bin'=>$bin, 'business' => $business, 'timeslot' => $available_slot,  'staff' => $staff_info, 
        'cust_info' =>$cust_info, 'service_date' =>$service_date, 'packages' => $packages, 
        'package_specifications' => $package_specifications );

    return view("store_front.booking.booking_summary")->with('data',$data);
    
    
}



//make paa booking
  public function placeBookingOrder(Request $request)
  {

    if( !$request->session()->has('__user_id_' ))
    {
      return redirect("/shopping/login")->with("err_msg", "Please login to place an order!");
    }

    $member_id =  $cust_id = $request->session()->get('__member_id_' );
    $cust_info =  CustomerProfileModel::find( $cust_id) ;
    if(!isset($cust_info))
    {
      return redirect("/booking")->with("err_msg",  "Missing data to perform action!");
    }

    $bin = $request->bin;
    $business = Business::find( $bin );
    if( !isset($business))
    {
      return redirect("/booking")->with("err_msg",  "Business information not found!");
    }

    if( $request->bin == "" || $request->serviceDate == "" || $request->staffId == "" ||  
      $request->packageNos == ""  || $request->preftime == "" )
    {
      return redirect("/booking/business/review-order-summary?bin=" . $bin )->with("err_msg",  "Missing data to perform action!");
    }
  

    //collect customer information
    $cname =   $cust_info->fullname; 
    $cphone =   $cust_info->phone; 
    $caddress =   $cust_info->locality;
    $service_date = date('Y-m-d'  , strtotime($request->serviceDate ));

    $packages_selected = DB::table("ba_temp_booking_cart")
    ->where("bin", $bin )
    ->where("member_id", $member_id )
    ->whereDate("service_date", $service_date )
    ->select("package_id", "staff_id", "starting_slot")
    ->get();

    $packages_selected_ids = $packages_selected->pluck("package_id")->toArray();

    if(count($packages_selected_ids) == 0)
    {
      return redirect("/booking/business/review-order-summary?bin=" . $bin )
      ->with("err_msg", "No package was selected to book. Please select a package to start booking!");
    }

    $staff_id = $packages_selected[0]->staff_id;
    $slotno = $packages_selected[0]->starting_slot;

    $service_days_slot = DB::table("ba_service_days") 
    ->where("bin", $bin )
    ->whereDate("service_date",  $service_date )
    ->whereIn("package_id",  $packages_selected_ids )
    ->select("slot_number as slotNo", "start_time as startTime", "end_time as endTime", DB::Raw(" group_concat(status) as statuses"))
    ->groupBy("slotNo")
    ->groupBy("startTime")
    ->groupBy("endTime")
    ->first();

    if( isset($service_days_slot)  )
    {
        $package_statuses =  explode(",",  $service_days_slot->statuses );
    }
    else
    {
        $package_statuses =   array("close");
    }

    $slot_available = false;
    if(count($package_statuses) == count($packages_selected_ids))
    {
        $slot_available = ( count(array_intersect( array( 'engage', 'ended', 'booked' ) , $package_statuses)) > 0 ) ?  false : true ;
    }
    
    if(!$slot_available) 
    {
      return redirect("/booking/business/review-order-summary?bin=" . $bin )
      ->with("err_msg", "Selected service is already booked. Please choose another date or change service.");
    }


    //proceed for booking
    $selected_packages  = DB::table("ba_service_products")
    ->where("bin", $bin)
    ->whereIn("id", $packages_selected_ids )
    ->get();

    if( !isset($selected_packages) )
    {
      return redirect("/booking/business/review-order-summary?bin=" . $bin )
      ->with("err_msg", "Selected packages are no longer available!");
    }


    $gateway_mode =  config('app.gateway_mode');
    $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );

    $total_cost = $total_discount = $total_gst =  0.00;
    foreach($selected_packages as $item)
    {
        $total_cost +=  $item->pricing;
        $total_discount  += $item->discount;
        $actual_cost = $item->pricing   ;
        $total_gst +=  ( ( $item->cgst_pc +  $item->sgst_pc ) * $actual_cost * 0.01);
    }
    
    $otp = mt_rand(111111, 999999);

    if( $request->homevisitservice != "")
    {
      //code needed to be added
    }
    else
    {

        //generate order sequencer
        $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
        $rp_1 = mt_rand(0, 9);
        $rp_2 = mt_rand(0, 9);
        $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;
        $sequencer = new OrderSequencerModel;
        $sequencer->type = "booking";
        $sequencer->tracker_session =  $tracker;
        $save = $sequencer->save();
        //generate order sequencer ends
        if( !isset($save) )
        {
          return redirect("/booking/business/review-order-summary?bin=" . $bin )
          ->with("err_msg", 'Failed to generate order number. Please retry again!'  );
        }


        //generate merchant sequencer
        $merchant_order_no =1;
        $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
        if(isset($merchant_order_info))
        {
          $merchant_order_no =  $merchant_order_info->last_order + 1;
        }
        else
        {
          $merchant_order_info = new MerchantOrderSequencer;
          $merchant_order_info->bin  = $request->bin;
        }


      //make new booking
        $newbooking = new ServiceBooking;
        $newbooking->id = $sequencer->id;
        $newbooking->customer_name =  $cname;
        $newbooking->customer_phone = $cphone ;
        $newbooking->address = $caddress ; 
        $newbooking->biz_order_no = $merchant_order_no;
        $newbooking->bin = $bin;
        $newbooking->book_category= $business->category;
        $newbooking->book_by = $cust_id;
        $newbooking->staff_id = $staff_id;
        $newbooking->book_status = "new";
        $newbooking->total_cost =  $total_cost;
        $newbooking->seller_payable =  $total_cost;
        $newbooking->discount = $total_discount;
        $newbooking->gst =  $total_gst;
        $newbooking->service_fee  =  0.00;
        $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
        $newbooking->cashback =  0;
        $newbooking->payment_type = "ONLINE";
        $newbooking->source = "booktou";
        $newbooking->origin = "customer";
        $newbooking->latitude =  $newbooking->longitude =  0;
        $newbooking->book_date =  date('Y-m-d H:i:s');
        $newbooking->preferred_time =  date('H:i:s', strtotime( $request->preftime ));
        $newbooking->service_date =  $service_date;
        $newbooking->cust_remarks = ($request->bookingRemark != "" ? $request->bookingRemark : "booked from cc portal");
        $newbooking->transaction_no =  "na" ;
        $newbooking->reference_no =  "na"  ;
        $newbooking->otp  =  $otp ;
        $save=$newbooking->save();
      
      $item_notes = array();
      if($save)
      {
        $newbooking = ServiceBooking::find(  $sequencer->id ); //auto increment has removed so need this line to fetch order.
        //update merchant order sequencer log
        $merchant_order_info->last_order  = $merchant_order_no;
        $merchant_order_info->save();
        //ending merchant order sequencer 

        $book_slots = array();
        foreach($selected_packages as $item)
        {
            $newbookingDetails = new ServiceBookingDetails;
            $newbookingDetails->book_id = $sequencer->id;
            $newbookingDetails->service_product_id = $item->id;
            $newbookingDetails->service_name = $item->srv_name;
            $newbookingDetails->slot_no = $slotno;
            $newbookingDetails->cgst_pc = $item->cgst_pc;
            $newbookingDetails->sgst_pc = $item->sgst_pc;
            $newbookingDetails->status = "new";
            $newbookingDetails->service_time = date('H:i:s', strtotime( $request->preftime));
            $newbookingDetails->service_charge = $item->actual_price;
            $newbookingDetails->save();
        }
        //updating occupied
        DB::table("ba_service_days")
        ->whereDate("service_date", $service_date)
        ->where("bin", $bin )
        ->whereIn("package_id",  $packages_selected_ids )
        ->update(array("status" => "booked") ) ;

        //clearing temporary cart
        DB::table("ba_temp_booking_cart")
        ->where("bin", $bin )
        ->where("member_id", $cust_id )
        ->delete();


        $return_url = URL::to('booking/order/payment-completed');
        $notifyUrl = '';
        $item_notes = array(
          "orderId" => $sequencer->id,
          "orderAmount" => ( $total_cost  + $total_gst ) * 100,
          "customerName" => $cname ,
          "customerPhone" =>  $cphone ,
          "customerEmail" => $cust_info->email=="" ? 'booktougi@gmail.com' : $cust_info->email,
          "returnUrl" => $return_url,
          "notifyUrl" => $notifyUrl,
        );

        $rzpapi = new Api(  $secure_key->key_id   , $secure_key->key_secret );
        $rzp_response =  $rzpapi->order->create(array
          ('receipt' => $sequencer->id ,
           'amount' => ( $total_cost  + $total_gst ) * 100,
           'currency' => 'INR', 
           'notes'=>  $item_notes                   
         ));


        if(isset($rzp_response))
        {
          //logging razorpay order ID
          $newbooking->rzp_id = $rzp_response->id;
          $newbooking->save();
          return redirect("/booking/business/payment-in-progress?o=".   $sequencer->id ) ;
        }
        return redirect("/shopping/my-order-details?key=" . $sequencer->id )->with('Booking placed successfully!');

      }
      else
      {
        return redirect("/booking/business/prepare-service-booking?bin=" . $bin )
        ->with("err_msg", "Failed to place booking!");
      }

      return redirect("/booking/business/prepare-service-booking?bin=" . $bin ) ;
    }
}



protected function bookingrazorpayPaymentInProgress(Request $request)
{ 
    
        $oid= $request->o;

        if($oid == "")
        {
            //redirect to parameter missing page
            return redirect("/shopping/my-bookings")->with("err_msg", "Your order is not found!") ;

        }
        $order_info = DB::table("ba_service_booking") 
        ->where("id", $oid)
        ->first();

        if( !isset( $order_info) )
        {
            //redirect to order not found page 
          return redirect("/shopping/my-bookings")->with("err_msg", "Your order is not found!") ;
        }
        $gateway_mode =  config('app.gateway_mode'); 
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );

        $prefill = array("name" => $order_info->customer_name , "email"=> "booktou@gmail.com", "contact" => $order_info->customer_phone);
         $notes = array("item" => $order_info->book_category, "price"=> $order_info->total_cost);
      
        $data = array(
            "orderno" => $order_info->id,
            "key" => $secure_key->key_id, 
            "amount" => ( $order_info->total_cost  + $order_info->gst ) * 100, 
            "currency" => "INR",
            "name" => $order_info->customer_name, 
            "description" => $order_info->customer_name, 
            "image" => "https://booktou.in/public/store/image/logo.png",
            "order_id" => $order_info->rzp_id ,
            "callback_url" => URL::to("/booking/order/payment-completed")  ,
            "prefill" =>  $prefill ,
            "notes" => $notes 
        );

        return view("store_front.razor.checkout_progress")->with( "data", $data);
    }



    protected function bookingrazorpayPaymentCompleted(Request $request)
    {
      $rzp_id= $request->rzid;
      if(  $rzp_id == "" )
      {
        return redirect("/shopping/checkout")->with("err_msg", "No matching online order found!") ;
      }


      //use eloquent model to allow saving any changes
      $order_info = ServiceBooking::where("rzp_id", $rzp_id)->first();

      if( !isset( $order_info) )
      {
        return redirect("/shopping/checkout")->with("err_msg", "Your order not found!") ;
      }

      $gateway_mode =  config('app.gateway_mode'); 
      $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );
      $razor_id =  $order_info->rzp_id;
      $response = $this->checkRazorPayOrderStatus( $razor_id, $gateway_mode );
 
      $payment_info =  $response['items'] ; 
      if(  isset(  $payment_info ) )
      {
        $payment_status = $response['items'][0]->status;
        $payment_id = $response['items'][0]->id;

        if(  strcasecmp( $payment_status , "captured") == 0 )
        {
          $order_info->rzp_pay_id =  $payment_id ;
          $order_info->payment_status  = "paid";
          $save=$order_info->save();

          return redirect("/shopping/my-order-details?key=" . $order_info->id )
          ->with("err_msg", "Your booking is placed successfully.") ;

        }
        else
        {
          return redirect("/shopping/my-order-details?key=" . $order_info->id )
          ->with("err_msg",  "Order payment pending!") ;
        }
      }
      else
      {
        return redirect("/shopping/my-order-details?key=" . $order_info->id )
        ->with("err_msg",  "Order payment pending!") ;        
      }

    }



    protected function myBookingOrderDetails(Request $request)
    {
        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        if( $request->key  == "")
        {
            return redirect("/shopping/my-orders")->with("err_msg","Please select an ordert to view it!");
        }

        $oid = $request->key;
        $uid = $request->session()->get('__user_id_' );

        //customer as member
        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
           return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        $ordersequence = OrderSequencerModel::find($oid);
        if(!isset($ordersequence ))
        {
           return redirect("/shopping/checkout")->with("err_msg", "Order not found!");
        }

        $ordertype = $ordersequence->type;

        if ( in_array($ordersequence->type, array("booking", "appointment" )  ) ) 
        {
            //cart details
            $order  = DB::table("ba_service_booking")
            ->where("ba_service_booking.book_by",  $member->id )
            ->where("ba_service_booking.id",  $oid )
            ->first();

            $booking_details = DB::table("ba_service_booking_details")
            ->join('ba_service_products','ba_service_booking_details.service_product_id','=','ba_service_products.id')
            ->where("book_id",$order->id)
            ->select("ba_service_booking_details.*","ba_service_products.photos","ba_service_products.srv_name as serviceName","ba_service_products.srv_details as serviceDetails")
            ->get();

            $view_name  =  'store_front.booking.booking_details';

        }else if ( $ordersequence->type=="offers" )
        {
            //cart details
            $order  = DB::table("ba_service_booking")
            ->where("ba_service_booking.book_by",  $member->id )
            ->where("ba_service_booking.id",  $oid )
            ->first();

            $booking_details = DB::table("ba_shopping_basket_srv_products")
            ->where("order_no",$order->id)
            ->get();

            $view_name  =  'store_front.orders.order_details';


        }else if($ordersequence->type=="normal"){
                //cart details
            $order  = DB::table("ba_service_booking")
            ->where("book_by",  $member->id )
            ->where("id",  $oid )
            ->first();

            $booking_details = DB::table("ba_service_booking_details")
            ->where("book_id",$order->id)
            ->get();

            $view_name  =  'store_front.orders.order_details';

        }else if ( strcasecmp($ordersequence->type,  "pnd" )  == 0 )  
        {

            $order  = DB::table("ba_pick_and_drop_order")
            ->where("request_by",  $member->id )
            ->where("id",$oid )
            ->first();

            $view_name  =  'store_front.orders.order_details';

        }else if ( strcasecmp($ordersequence->type,  "assist" )  == 0 )  
        {
          $order  = DB::table("ba_pick_and_drop_order")
          ->where("request_by",  $member->id )
          ->where("id",$oid )
          ->first();
          $view_name  =  'store_front.orders.order_details';
      } 


    if(!isset($order))
    {
        return redirect("/settings/view-my-profile")->with("err_msg", "No orders found!");
    }


    $cart_items  = DB::table("ba_shopping_basket")
    ->join('ba_product_variant','ba_shopping_basket.prsubid','=','ba_product_variant.prsubid')
    ->where("order_no",   $oid )
    ->select("ba_shopping_basket.*","ba_product_variant.sgst_pc as sgst","ba_product_variant.cgst_pc as cgst")
    ->get();

    $staff  = DB::table("ba_profile")
    ->select("id as staffId", "fullname as staffName", "profile_photo as profilePicture" )
    ->where("id",  $order->staff_id )
    ->first();


    $business = Business::find($order->bin);
    if (isset($business)) 
    {
        $rating_type = DB::table("ba_rating_types")
        ->select("ba_rating_types.*",
            DB::Raw("'' type"),
            DB::Raw("'' review"),
            DB::Raw("'0' rating"),
            DB::Raw("'' status"))
        ->where('main_module',strtolower($business->main_module))
        ->get();

        $service_rating = DB::table("ba_service_ratings")
        ->where('order_no',$oid)
        ->get();

        $rating_ = array();

        foreach ($rating_type as $items) {
            foreach($service_rating as $list)
            {
                if ($items->rating_type== $list->rating_type) {
                   $items->type = $list->rating_type;
                   $items->review = $list->review;
                   $items->rating = $list->rating;
                   $items->status = $list->status;
                   break;
               }
           }
       }
   }
 
   $data = array("order"=>$order, 'cart_items' => $cart_items ,'staff'=>$staff,
    'member' => $member, 'business' => $business,'ordertype'=>$ordertype,
    "booking_details"=>$booking_details,'ratingType'=>$rating_type);
   return view( $view_name )->with( $data );

}




  protected function cancelBookingOrder(Request $request)
  {
        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        if( $request->key  == "")
        {
            return redirect("/shopping/my-orders")->with("err_msg","Please select an ordert to view it!");
        }

        $oid = $request->key;
        $uid = $request->session()->get('__user_id_' );

        //customer as member
        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
           return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        $ordersequence = OrderSequencerModel::find($oid);
        if(!isset($ordersequence ))
        {
           return redirect("/shopping/checkout")->with("err_msg", "Order not found!");
        }

        $ordertype = $ordersequence->type;

        if ( strcasecmp($ordersequence->type,  "booking" )  != 0 )  
        {
          return redirect("/booking/view-order-details?key=" .  $oid )->with("err_msg", "Order type mismatched!");
        }
        //cart details
        $booking_info  = ServiceBooking::where("ba_service_booking.book_by",  $member->id )
        ->where("ba_service_booking.id",  $oid )
        ->first();

        if(!isset($booking_info))
        {
          return redirect("/settings/view-my-profile")->with("err_msg", "No orders found!");
        }

        $booking_items = DB::table("ba_service_booking_details")
        ->where("book_id",$booking_info->id)
        ->select("service_product_id")
        ->get();

        $package_ids = $booking_items->pluck("service_product_id")->toArray();

        if(count($package_ids) >  0)
        {
          $service_date = date("Y-m-d", strtotime( $booking_info->service_date));
          $bin = $booking_info->bin;
          $timeslots = DB::table("ba_service_days")
          ->where("bin",  $bin)
          ->whereDate("service_date",  $service_date )
          ->whereIn("package_id", $package_ids)
          ->update([ 'status' => 'open']); 

        }
  
    $booking_info->book_status = "cancelled";
    $booking_info->save();


    return redirect("/booking/view-order-details?key=" .  $oid )->with("err_msg", "Booking status updated!");
 
}



}
