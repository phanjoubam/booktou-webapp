<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Business; 
use App\ServiceDuration;
use App\ServiceBooking;
use App\Booking;
use App\User;
use App\BusinessPhoto;
use GuzzleHttp\Client;
use App\Traits\Utilities;
use Hash;
use App\BusinessOwner;
use App\BusinessReview;
use App\FavouriteBusiness;
use App\BusinessService;
use App\ServiceBookingDetails;
use App\BusinessHour;
use App\StaffAvailability;
use App\BusinessCategory;
use App\CustomerProfileModel;
use App\CustomerReviewModel;



class CustomerApiController extends Controller
{
	use Utilities;
	
   	public function saveIndividualServiceBooking(Request $request)
    {
       
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $addressCustomer = $user_info->userAddress;
        
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }

          //Fetch Business Category 
        $businessCategory=DB::table("ba_business_category")
        ->where('id',$request->categoryId)
        ->get();
         foreach ($businessCategory as $itemC)
        {
            $bookCategory=$itemC->name;
        }
    
    
        if($request->bookingDate == "" || $request->staffId == "" || $request->serviceCode == "" || $request->serviceTime == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
        
        $bookId = $bookTime = $arraySlotItem = $availableSlot = array();  

        $day = date('l', strtotime($request->bookingDate));
               
        //booking
        $booking = ServiceBooking::where('bin',$request->bin)
            ->where('book_date',date('Y-m-d', strtotime($request->bookingDate)))
            ->where('staff_id',$request->staffId)
            ->get();

        foreach ($booking as $bokitem) 
        {
            
            $bookId[] = $bokitem->id ;

            
        }  
        //bookingDetails
        $bookingDetails = ServiceBookingDetails::whereIn('book_id',$bookId)->get();

        foreach ($bookingDetails as $itemD) {
            $bookTime[] = $itemD->service_time ;
        }
        //serviceDuration
        $serviceDurationSlot = ServiceDuration::whereNotIn('start_time',$bookTime)
            ->where('bin',$request->bin)
            ->where("day_name",$day)
            ->get();
        
        foreach ($serviceDurationSlot as $serviceItem) {
            $arraySlotno = $serviceItem->slot_number;
            $arraySlotItem[] = $arraySlotno; 
            
            
        }
        $dateTimeRequest = date('H:i:s', strtotime($request->serviceTime));
        if(count($serviceDurationSlot)>0)
        {
                $serviceDuration = ServiceDuration::where('bin',$request->bin)
                ->whereIn('slot_number', $arraySlotItem)
                ->where('start_time','>=' ,$dateTimeRequest)
                ->where("day_name",$day)
                ->orderby('slot_number','ASC')
                ->get();
                foreach ($serviceDuration as $itemT) {
                
                        $availableSlot[] = $itemT->start_time ;
                    
                }
                
        }
        $otp=0;
         
     
        if(count($availableSlot)>0)
        {
        	if($availableSlot[$otp]!= $dateTimeRequest)
	        {
	        	 $data= ["message" => "failure" , "status_code" => 1028 ,  'detailed_msg' => "Failed to place your booking details." ];
	        
	         
	        	return json_encode( $data); 
	        }
        	$otp = mt_rand(1000, 9999);
            $newbooking = new ServiceBooking;
            $newbooking->bin = $request->bin;
            $newbooking->book_category= $bookCategory;
            $newbooking->book_date = date('Y-m-d', strtotime($request->bookingDate));
            $newbooking->book_by = $user_info->user_id;
            $newbooking->staff_id = $request->staffId;
            $newbooking->address= $addressCustomer;
            $newbooking->otp = $otp;
            $newbooking->book_status = "new";
            $save=$newbooking->save();
            if($save)
            {
                $serviceCodes = $request->serviceCode;
                $codeService = explode("[",$serviceCodes);
                $codeService2 = explode("]",$codeService[1]);
                $serviceCodes3 = explode(",",$codeService2[0]);
                $pos =0;
                foreach ($serviceCodes3 as $serviceCode) 
                {
                    $newbookingDetails = new ServiceBookingDetails;
                    $newbookingDetails->book_id = $newbooking->id;
                    $newbookingDetails->service_code =str_replace('"', '', $serviceCode);
                    $newbookingDetails->service_time = $availableSlot[$pos];
                    $newbookingDetails->status = "new";
                    $newbookingDetails->save();
                    $pos++;
                }
                $message = "success";
                $err_code = 1027;
                $err_msg = "Your booking is placed successfully.";
                
            
            }
            else{
                $message = "failure";
                    $err_code = 1028;
                    $err_msg = "Failed to place your booking details.";
            }
        	
        }
        else{
            $message = "failure";
                    $err_code = 1028;
                    $err_msg = "Failed to place your booking details.";
        }
        
        $data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg , 'confirmationOTP' => $otp ];
        
         
        return json_encode( $data); 
      
     
    } 
  


    //obsolete
  	public function getAllBusinessDetails(Request $request)
	{
		$binn = $binH = $binD = $binS = $binSf =array();
		//checkBy request category
		if($request->category == "")
		{
			$chkBusiness=Business::get();
		}
		else
		{
			$chkBusiness=Business::where('category',$request->category)->get();
		}
		
		foreach ($chkBusiness as $keybus) 
		{
			if($keybus->longitude && $keybus->tags && $keybus->locality && $keybus->phone_pri && $keybus->latitude && $keybus->name && $keybus->tag_line && $keybus->shop_number !=null)
			{
				$binn[]=$keybus->id;
			}
		}

		//hours table
		$businessHour = BusinessHour::whereIn('bin',$binn)
		->distinct('bin')
		->get();

	    foreach ($businessHour as $keyhour) {
	    	$binH[] = $keyhour->bin;
	    }

	    $hourList = array_unique($binH);

	    //duration table is time slot
	    $serviceDuration = ServiceDuration::whereIn('bin',$hourList)
	    ->distinct('bin')
	    ->get();
		
		foreach ($serviceDuration as $keyDur) 
		{
	    	$binD[] = $keyDur->bin;
	    }	
	    $durationList = array_unique($binD);


	    //Service table
	    $businessService = BusinessService::whereIn('bin',$durationList)
	    	->distinct('bin')
	    	->get();
	    foreach ($businessService as $keyS) {
	    	$binS[] = $keyS->bin;

	    }
	    $serviceList = array_unique($binS);
	    
	    
	    //StaffAvailability table
	    /*$staffAvailability = StaffAvailability::whereIn('bin',$serviceList)
	    	->distinct('bin')
	    	->get();
	    foreach ($staffAvailability as $keyStaff) {
	    	$binSf[] = $keyStaff->bin;
	    }
	    $staffList = array_unique($binSf);*/
	    
		$businesses = DB::table("ba_business")
		->whereIn('id',$serviceList)	 
		->select( 'id as bin',  "name", "tag_line as tagLine", 'shop_number as shopNumber',  
		  'phone_pri as primaryPhone', 'phone_alt as alternatePhone', 
		  "locality", "landmark", "city", "state", "pin", 
		  'tags','rating','latitude','longitude' , 
		  "profile_image as profileImgUrl",   "banner as bannerImgUrl" ,
		  DB::Raw("'na' promoImgUrl"), DB::Raw("'na' favourite"),"category as businessCategory", 
		  "is_open as isOpen"
		 )
		->paginate(5);

		//BusinessCategory   
	  	
	  	foreach($businesses as $item)
		{
			if($item->profileImgUrl!=null)
			{
				$item->profileImgUrl = URL::to('/public'.$item->profileImgUrl);	
			}
			else{
				$item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;	 
			}

			if($item->bannerImgUrl!=null)
			{
				$item->bannerImgUrl = URL::to('/public'.$item->bannerImgUrl);	
			}
			else{
				$item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;	 
			}

			if($item->promoImgUrl!='na')
			{
				$item->promoImgUrl = URL::to('/public'.$item->promoImgUrl);	
			}
			else{
				$item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;	 
			}
					
			$favorited = FavouriteBusiness::where('user_id',$request->userId)
			->where('bin',$item->bin)->get();

			$item->favourite = count($favorited); 
		}
		 


	  	$result = array( 'results' => $businesses ) ;

	    return json_encode($result); 
  	}




  	public function getAllRelatedBusinesses(Request $request)
	{
		$binn = $binH = $binD = $binS = $binSf =array();
		//checkBy request category
		if($request->category == "")
		{
			$chkBusiness=Business::get();
		}
		else
		{
			$chkBusiness=Business::where('category',$request->category)->get();
		}
		
		foreach ($chkBusiness as $keybus) 
		{
			if($keybus->longitude && $keybus->tags && $keybus->locality && $keybus->phone_pri && $keybus->latitude && $keybus->name && $keybus->tag_line && $keybus->shop_number !=null)
			{
				$binn[]=$keybus->id;
			}
		}

		//hours table
		$businessHour = BusinessHour::whereIn('bin',$binn)
		->distinct('bin')
		->get();

	    foreach ($businessHour as $keyhour) {
	    	$binH[] = $keyhour->bin;
	    }

	    $hourList = array_unique($binH);

	    //duration table is time slot
	    $serviceDuration = ServiceDuration::whereIn('bin',$hourList)
	    ->distinct('bin')
	    ->get();
		
		foreach ($serviceDuration as $keyDur) 
		{
	    	$binD[] = $keyDur->bin;
	    }	
	    $durationList = array_unique($binD);


	    //Service table
	    $businessService = BusinessService::whereIn('bin',$durationList)
	    	->distinct('bin')
	    	->get();
	    foreach ($businessService as $keyS) {
	    	$binS[] = $keyS->bin;

	    }
	    $serviceList = array_unique($binS);
	    
	     
	    
		$businesses = DB::table("ba_business")
		->whereIn('id',$serviceList)	 
		->where("is_block",  "no")
		->select( 'id as bin',  "name", "tag_line as tagLine", 'shop_number as shopNumber',  
		  'phone_display as primaryPhone', 'phone_alt as alternatePhone', 
		  "locality", "landmark", "city", "state", "pin", 
		  'tags','rating','latitude','longitude' , 
		  "profile_image as profileImgUrl",   "banner as bannerImgUrl" ,
		DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"),"category as businessCategory", "is_open as isOpen" )
		->paginate(5);


		//BusinessCategory   
	  	
	  	foreach($businesses as $item)
		{ 
			if($item->profileImgUrl!=null)
			{
				$item->profileImgUrl = URL::to('/public'.$item->profileImgUrl);	
			}
			else{
				$item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;	 
			}
 

			if($item->promoImgUrl!='na')
			{
				$item->promoImgUrl = URL::to('/public'.$item->promoImgUrl);	
			}
			else{
				$item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;	 
			}
 

			//compression   
			if($item->bannerImgUrl != null )
			{
				$source = public_path() .  $item->bannerImgUrl;
				$pathinfo = pathinfo( $source  );
				$new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
				$destination  = base_path() .  "/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ; 

				if(file_exists(  $source ) &&  !file_exists( $destination ))
				{

					$info = getimagesize($source ); 

				    if ($info['mime'] == 'image/jpeg')
				        $image = imagecreatefromjpeg($source);

				    elseif ($info['mime'] == 'image/gif')
				        $image = imagecreatefromgif($source);

				    elseif ($info['mime'] == 'image/png')
				        $image = imagecreatefrompng($source);


				  $original_w = $info[0];
				  $original_h = $info[1];
				  $thumb_w = 290; // ceil( $original_w / 2);
				  $thumb_h = 200; //ceil( $original_h / 2 );

				  $thumb_img = imagecreatetruecolor($original_w, $original_h);
				  imagecopyresampled($thumb_img, $image,
					                   0, 0,
					                   0, 0,
					                   $original_w, $original_h,
					                   $original_w, $original_h);

					imagejpeg($thumb_img, $destination, 20);
				}
				$item->bannerImgUrl =  URL::to("/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name );	 

			}
			else
			{
				$item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;	 
			}

 
			$favorited = FavouriteBusiness::where('user_id',$request->userId)
			->where('bin',$item->bin)->get(); 
			$item->favourite = count($favorited); 

 
		}
		 


	  	$result = array( 'results' => $businesses ) ;

	    return json_encode($result); 
  	}

	
	public function getBeautyDetails(Request $request)
	{
	  	$date = date('Y-m-d');
	   //$date = date('Y-m-d', strtotime( $request->bookingDate)); 
		$arrayStatus=["cancel_by_client","cancel_by_owner"];
	    
		$day_bookings= DB::table("ba_service_booking")
		->where("id", $request->bookingNo )
		->whereNotIn('book_status',$arrayStatus)
		->select(
			"id as bookingNo", 
			"book_date as appointmentDate", 
			DB::Raw("'na' appointmentTime"), 
			"book_by as customerId", 
			DB::Raw("'na' customerName"),
			"address as customerAddress",
			"otp as serviceOTP",
			DB::Raw("'na' bookingDetails") ,
			"bin",
			DB::Raw("'na' businessUserName"), 
			DB::Raw("'na' businessName"), 
			DB::Raw("'na' businessPhone"), 
			DB::Raw("'na' businessAltPhone"), 
			"book_status as bookingStatus",
			DB::Raw("'na' businessAddress"), 
			DB::Raw("'na' businessBannerUrl"),
			DB::Raw("'na' businesslatitude"),
			DB::Raw("'na' businesslongitude"), 
			DB::Raw("'na' businessCategory") 
		
		) 
		->orderBy('book_date', 'ASC')
		->get(); 
		
		if(count($day_bookings)<1)
		{
			$data= ["message" => "failure", "status_code" =>  1094 , 'detailed_msg' => 'Booking details information fetched has no record.', 'results' => array()];
			return json_encode($data);
		} 
		else{
			$bookStatusBoolean = true;
		}
	   
	   $bins = array();
	   $bookids = array();
	   $custids= array();
	   foreach ($day_bookings as $bitem) 
		{
			$bins[] = $bitem->bin;
			$bookids[] = $bitem->bookingNo ;
			$custids[] = $bitem->customerId ;
		}
		
		$bins[]= $bookids[] =$custids[] = 0;  
		$bins = array_unique($bins);
		
		
		$businesses = DB::table("ba_business")
		->whereRaw("id  in (" .  implode(",", $bins) .  " )") 
		->get();
		 
		
		
		$customers = DB::table("ba_users")  
		->select("id",  DB::Raw("REPLACE( CONCAT( fname , ' ', COALESCE( mname,''), ' ', lname ) , '  ', ' ') as  customerName")   ) 
		->whereRaw("id  in (" .  implode(",", $custids) .  " )") 
		->get();
	 

		$serviceBusiness = BusinessService::whereRaw("bin  in (" .  implode(",", $bins) .  " )")->get();		
				
		foreach ($day_bookings as $aitem) 
		{
			$bookings =  DB::table("ba_service_booking_details")
			->join("ba_service_booking","ba_service_booking.id","=","ba_service_booking_details.book_id")
			->whereRaw("book_id  in (" .  implode(",", $bookids) .  " )") 
			->where('ba_service_booking.otp',$aitem->serviceOTP)
			->whereNotIn('status',$arrayStatus)
			->select("ba_service_booking_details.service_code as serviceCode",DB::Raw("'na' serviceName"),"ba_service_booking_details.service_time as serviceTime", "ba_service_booking_details.status")
			->get();
			
			
			if(count($bookings)>0)
			{	
				$booleanBooking = true;
				foreach ($bookings as $keybook) 
				{

					foreach ($serviceBusiness as $keyService) 
					{
						if($keybook->serviceCode == $keyService->srv_code)
						{
							$keybook->serviceName=$keyService->srv_name;
							break;
							
						}
					}
					$keybook->serviceTime = date('h:i a',strtotime($keybook->serviceTime));

				}
			
			}	
			else{
				$booleanBooking = false;
			}	
			foreach ($businesses as $bitem) 
			{ 
				if($aitem->bin == $bitem->id)
				{
					$aitem->businessName=$bitem->name;
					$aitem->businessPhone=$bitem->phone_pri;
					$aitem->businessAltPhone=$bitem->phone_alt;
					$aitem->businesslatitude=$bitem->latitude;
					$aitem->businesslongitude=$bitem->longitude;
					$aitem->category =$bitem->category;
					//$aitem->businessBannerUrl=$bitem->banner;
					$aitem->businessBannerUrl=URL::to('/public'.$bitem->banner);
				if($bitem->landmark!=null)
				{
					$lan = ", ";
				}
				else
				{
					$lan = "";
				}
				if($bitem->city!=null)
				{
					$cty = ", ";
				}
				else
				{
					$cty = "";
				}
				if($bitem->state!=null)
				{
					$sta = ", ";
				}
				else
				{
					$sta = "";
				}
				if($bitem->pin!=null)
				{
					$pn = ", ";
				}
				else
				{
					$pn = "";
				}
				$aitem->businessAddress=$bitem->locality.$lan.$bitem->landmark.$cty.$bitem->city.$sta.$bitem->state.$pn.$bitem->pin ;	
					break;
						 	
				}
			}
			
			foreach ($customers as $citem) 
			{
				if($aitem->customerId == $citem->id)
				{
					$aitem->customerName = $citem->customerName;	
					break;
				}
			}
			
			
			$aitem->bookingDetails = $bookings; 
			if($booleanBooking) 
			{
				$aitem->appointmentTime = date('h:i a', strtotime($bookings[0]->serviceTime));
			}
			$aitem->appointmentDate = date('d-m-Y', strtotime($aitem->appointmentDate));
		}
		if($bookStatusBoolean) 
		{
			$data= ["message" => "success", "status_code" =>  1093 , 'detailed_msg' => 'Booking details information fetched.', 
		'results' => $day_bookings];
		}
				
		return json_encode($data); 
  	}
	public function addNewCustomer(Request $request)
	{
		if($request->customerName == "" || $request->customerPhone == "" || $request->customerAddress== ""||$request->bin == "" )
		{
			$err_msg = "Field missing!";
			$err_code = 9000;
						
			$data = array(
				"message" => "failure" , 'status_code' => $err_code ,
				'detailed_msg' => $err_msg) ;
			
			return json_encode( $data ); 
		}
		$userInfo = User::where("phone", $request->customerPhone)->first();
		
		if(isset($userInfo))
		{
			$err_msg = "User Account to this phone number is already exists!";
			$err_code = 1000; 
			 
			$data = array(
				"message" => "failure" , 'status_code' => $err_code ,
				'detailed_msg' => $err_msg) ;

			return json_encode( $data );
		}
		$rand = mt_rand(434343, 999999);
		$password = $rand;
		$hashed = Hash::make($password);
		$name = $request->customerName;
		$arr2 = explode(" " , $name);
		$totalname = count((array)$arr2);

		$newCustomerUser = new User;
		$newCustomerUser->bin = $request->bin;
		$newCustomerUser->phone = $request->customerPhone;
		$newCustomerUser->password = $hashed;
		if($totalname == 3)
		{
			$newCustomerUser->fname = $arr2[0];
			$newCustomerUser->mname = $arr2[1];
			$newCustomerUser->lname = $arr2[2];
		}
		else if($totalname == 2)
		{
			$newCustomerUser->fname = $arr2[0];
			$newCustomerUser->lname = $arr2[1];
		}
		
		$newCustomerUser->locality = $request->customerAddress;
		$newCustomerUser->category = 0;
		$newCustomerUser->status = 0;
		$save = $newCustomerUser->save();
		if($save)
		{
			$err_status ="success";
			$err_code =  3501;
	        $err_msg =  "Registration Completed! OTP sent as SMS to your registered mobile.";
	        $bin = $request->bin;

	        $phones = array($request->customerPhone);
	        $otp_msg = "Your Login Password OTP is " . $rand.  urlencode(". Please DO NOT SHARE this OTP with anyone. bookTou never calls you asking for OTP." ) ; 
	        $this->sendSmsAlert($phones, $otp_msg); 
	        		

		}
		else 
		{

			$err_msg = "Registration failed. Please retry!";
			$err_code = 3502;
			$err_status ="failure";
			$bin = 0;
			

		}
			 
		$data = array(
				"message" => $err_status  , "status_code"=>$err_code ,
				'detailed_msg' => $err_msg,
				'bin' => $bin) ;


		return json_encode($data); 
		

  	}
  	public function addNewSales(Request $request)
	{
		if($request->customerName == "" || $request->customerPhone == "")
		{
			$err_msg = "Field missing!";
			$err_code = 9000;
			
			
			$data = array(
				"message" => "failure" , 'status_code' => $err_code ,
				'detailed_msg' => $err_msg) ;
			
			return json_encode( $data ); 
		}
		$userInfo = User::where("phone", $request->customerPhone)->first();
		
		if(isset($userInfo))
		{
			//generate a confirmation OTP 
			$otp = mt_rand(1000, 9999);
			$newbooking = new ServiceBooking;
			$newbooking->bin = $request->bin;
			$newbooking->book_date = date('Y-m-d', strtotime($request->bookingDate));
			$newbooking->slot_no = $request->slotNo;
			$newbooking->srv_code = $request->serviceCode;
			$newbooking->book_by = $userInfo->id;
			$newbooking->otp = $otp;
			$newbooking->book_status = "new";
			$newbooking->save();
			$message = "success";
			$err_code = 1027;
			$err_msg = "Your booking is placed successfully.";
			 
			$data = array(
				"message" => $message , 'status_code' => $err_code ,
				'detailed_msg' => $err_msg) ;

			return json_encode( $data );
		}
		$business = Business::find($request->bin);
		if( !isset($business))
		{
		  
		  $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		  'detailed_msg' => 'Business information not found!'  ];
		  return json_encode( $data);
		  
		}
	
	
		if($request->bookingDate == ""  )
		{
		 $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
		}

		$randpwd = $request->customerPhone.'2001';
		$password = $randpwd;
		$hashed = Hash::make($password);
		$name = $request->customerName;
		$arr2 = explode(" " , $name);
		$totalname = count((array)$arr2);

		$newCustomerUser = new User;
		$newCustomerUser->bin = 0;
		$newCustomerUser->phone = $request->customerPhone;
		$newCustomerUser->password = $hashed;
		if($totalname == 3)
		{
			$newCustomerUser->fname = $arr2[0];
			$newCustomerUser->mname = $arr2[1];
			$newCustomerUser->lname = $arr2[2];
		}
		else if($totalname == 2)
		{
			$newCustomerUser->fname = $arr2[0];
			$newCustomerUser->lname = $arr2[1];
		}
				
		$newCustomerUser->category = 0;
		$newCustomerUser->status = 100;
		$save = $newCustomerUser->save();
		if($save)
		{
			$err_status ="success";
			$err_code =  2001;
	        $err_msg =  "Your booking is placed and new User added successfully.";
	        $otp_pwd = $randpwd;
		    //generate a confirmation OTP 
			$otp = mt_rand(1000, 9999);
			$newbooking = new ServiceBooking;
			$newbooking->bin = $request->bin;
			$newbooking->book_date = date('Y-m-d', strtotime($request->bookingDate));
			$newbooking->slot_no = $request->slotNo;
			$newbooking->srv_code = $request->serviceCode;
			$newbooking->book_by = $newCustomerUser->id;
			$newbooking->otp = $otp;
			$newbooking->book_status = "new";
			$newbooking->save();
	        
		}
		else 
		{

			$err_msg = "Registration failed. Please retry!";
			$err_code = 2002;
			$err_status ="failure";
			$otp_pwd = 0;
		
		}
				 
		$data = array(
				"message" => $err_status  , "status_code"=>$err_code ,
				'detailed_msg' => $err_msg) ;


		return json_encode($data); 
		

  	}
  	public function getBookingDetailsByDate(Request $request)
	{
	  	if($request->userId == "")
	    {
	        $err_msg = "Field missing!";
	        $err_code = 9000;
	        
	        $data = array(
	            "message" => "failure" , 'status_code' => $err_code ,
	            'detailed_msg' => $err_msg) ;
	        
	        return json_encode( $data ); 
	    }
	    $byDate = date('Y-m-d', strtotime($request->bookByDate));
	    $bin = array();
	   
	    $user_info =  json_decode($this->getUserInfo($request->userId));
	    $bin[]=$user_info->bin;
	    $businessOwner = BusinessOwner::where('user_id',$request->userId)->select("bin")->get();
	    foreach ($businessOwner as $owner) {
	    	$bin[]=$owner->bin;    		    	
	    }
	    $binList = array_unique($bin);
	    
	    $bookingService  = DB::table("ba_service_booking_details")
                ->join("ba_service_booking", "ba_service_booking_details.book_id", "=", "ba_service_booking.id")
                ->whereIn("ba_service_booking.bin", $binList)
	        	->where('ba_service_booking.book_date','=',$byDate)
	        	->where('ba_service_booking.staff_id','=',$request->staffId)
	        	->select("ba_service_booking.book_date as bookingDate",
	        		"ba_service_booking.bin",
	        		"ba_service_booking.book_by as bookingBy",
                    "ba_service_booking_details.service_code as serviceCode",
                    DB::Raw("'na' serviceName"),
	                DB::Raw("'na' serviceDetails"),
	                DB::Raw("'na' pricing"),
	                DB::Raw("'na' durationHour"),
	                DB::Raw("'na' durationMinute"),
	                DB::Raw("'na' customerName") )
	            ->get();
  
  		$businessService = BusinessService::get();

	  	$totalPrice=0.0;
		foreach ($bookingService as $item) 
		{
			foreach ($businessService as $sitem) 
			{

				if($item->serviceCode == $sitem->srv_code)
				{
					$item->serviceName = $sitem->srv_name;
					$item->serviceDetails = $sitem->srv_details;
					$item->pricing = $sitem->pricing;
					$item->durationHour = $sitem->duration_hr;
					$item->durationMinute = $sitem->duration_min;
					break;

					
				}

			}
			$userName = User::where('id',$item->bookingBy)->get();
			foreach ($userName as $name) {
                if($name->mname!=null){
                    $item->customerName=$name->fname ." ". $name->mname." ".$name->lname ;
                }
                else{
                    $item->customerName=$name->fname ." ".$name->lname ;
                }
         
                
            }
            $totalPrice += $item->pricing;
		          	
		}          
	            
	         
	    
		$result = array( 'results' => $bookingService,'totalPrice' =>$totalPrice) ;
	         	
	  	
	    return json_encode($result);
  	}

  	public function getAllTagList(Request $request)
	{
		$name = $request->title;
		$businesses = Business::select(DB::raw('GROUP_CONCAT(tags) AS tags'))
			->first();

 		$alltags = explode(",", $businesses->tags);
 
 		$tagList = array_unique($alltags);
 		$collection = collect($tagList);

		$flattened = $collection->flatten();

		$flattened->all();
 				
		$result = array( 'tags' => $flattened) ;
	     	
		$data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business Tag Line', 'results' => $result];	     		
	  	return json_encode($data); 
  	}



  	public function saveReviewsAndRating(Request $request)
  	{

  		$user_info =  json_decode($this->getUserInfo($request->userId));
	    if(  $user_info->user_id == "na" )
	    {
	      $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
	      return json_encode($data);
	    }
 


  		if($request->bin =="" ||  $request->reviewTitle== "" || $request->reviewBody == "" || $request->star == ""     )
		{
			$data=[ "message"=> "failure", "status_code"=> 9000 , "detailed_msg"=> "Field missing!" ];
				return json_encode($data);
		}	
		
		
		$business_id = $request->bin; 
		$business  = Business::find($business_id);
		if(!isset($business)) 
        {
			$data=  [
    			"message"=> "failure", "status_code"=> 9002 , "detailed_msg"=> "No matching business found!" ];
				return json_encode($data);
		}
		
		$customer_id = $user_info->member_id; 
		 
		
		//check BusinessReview
		$review = BusinessReview::where('bin',$business_id)->where('member_id',$customer_id)->first();
		if(!isset($review)){
			$review  = new BusinessReview;
			$review->bin  = $business_id ;
			$review->member_id  = $customer_id;
		}
	    $review->title  = $request->reviewTitle ;
		$review->body   =  $request->reviewBody ; 
		$review->rating  =  $request->star ;
		$review->added_on = date('Y-m-d H:i:s');		
		$save=$review->save();
		
		if($save)
		{
			$status_code= "success";
			$err_msg = "Business review saved Successfully!";
			$err_code = 2003;  

			//Get Rating From BusinessReview
    		$reviewRate = BusinessReview::where('bin',$review->bin)->get();
    		$rate = 0;
			foreach ($reviewRate as $keyitem) {
				$rate += $keyitem->rating;

			}
			
			$count = $reviewRate->count();
			if($count>1){
				$checkrate = $rate / $count;
				$countrating = $checkrate;

				

			}
			else{
				$countrating = $rate;
				
			}
			$updateBusiness = Business::where('id',$business_id)->first();
			$updateBusiness->rating = $countrating;
			$updateBusiness->save();

			
		}
		else 
		{
			$status_code= "failure";
			$err_msg = "Business review could not be saved. Please consult admin!";
			$err_code = 2004;  
		}
		
		$data = array("message" => $status_code  ,  'status_code' => $err_code, "detailed_msg" => $err_msg) ; 
		return json_encode($data); 
  	}



	public function getAvailableTimeSlot(Request $request)
    {
 
      	$business = Business::find($request->bin);  
		if( !isset($business))
        {
            $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 , 
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
        }
                
        $day = date('l', strtotime($request->bookingDate));
                      
        
        $serviceBooking = DB::table("ba_service_booking_details")
                
        ->join("ba_service_booking", "ba_service_booking_details.book_id", "=", "ba_service_booking.id")
        ->join("ba_services_duration", "ba_service_booking_details.service_time", "=", "ba_services_duration.start_time")
        ->where('ba_service_booking.book_date',date('Y-m-d', strtotime($request->bookingDate)))
        ->where('ba_service_booking.bin',$request->bin)
        ->where('ba_services_duration.bin',$request->bin)
        ->where('ba_services_duration.day_name',$day)
        ->where('ba_service_booking.staff_id',$request->staffId)
        ->select('ba_services_duration.slot_number','ba_services_duration.day_name')
        ->get();
        
        foreach ($serviceBooking as $serviceItem) {
            $arraySlotno = $serviceItem->slot_number;
            $arraySlotItem[] = $arraySlotno; 

           
        }
        if(count($serviceBooking)>0){
                $serviceDuration = ServiceDuration::where('bin',$request->bin)
                ->where('day_name',$day)
                ->whereNotIn('slot_number', $arraySlotItem)
                ->select('id as serviceId','bin','slot_number as slotNumber','day_name as dayName','start_time as startTime','end_time as endTime','status')
                ->orderby('slotNumber','ASC')
                ->get();
        }
        else{
            
            $serviceDuration = ServiceDuration::where('bin',$request->bin)
                ->where('day_name',$day)
                ->select('id as serviceId','bin','slot_number as slotNumber','day_name as dayName','start_time as startTime','end_time as endTime','status')
                ->orderby('slotNumber','ASC')
                ->get();
        }
        
        

        foreach ($serviceDuration as $keyItem) {
              $keyItem->startTime = date('h:i a', strtotime($keyItem->startTime));
              $keyItem->endTime = date('h:i a', strtotime($keyItem->endTime));
        } 

                  

        $data= ["message" => "success", "status_code" =>  2005 ,  'detailed_msg' => 'Business information.' ,  
         'results' => $serviceDuration ];
        return json_encode( $data);


    }      

	public function getCompletedBooking(Request $request)
	{
	  	$user_info =  json_decode($this->getUserInfo($request->userId));

		if(  $user_info->user_id == "na" )
		{
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
		return json_encode($data);
		}

		$requestType = $request->type;

		//$bookDate = date('Y-m-d', strtotime($request->bookingDate)); 
		//show sponsored business
		$day_bookings= DB::table("ba_service_booking_details")
                ->join("ba_service_booking", "ba_service_booking_details.book_id", "=", "ba_service_booking.id")
		->where("ba_service_booking.book_by", $user_info->user_id)
		->where("ba_service_booking.book_status","completed")
		->where("ba_service_booking_details.status","completed")
		->select("bin","book_date as appointmentDate", "service_code as services","service_time as appointmentTime", "book_by as bookingUserId", DB::Raw("'na' startTime"), DB::Raw("'na' endTime"), DB::Raw("'na' businessUserName"), DB::Raw("'na' businessName"),DB::Raw("'na' businessPhone"),DB::Raw("'na' businessAltPhone"),"book_status as bookingStatus",DB::Raw("'na' businessAddress"),DB::Raw("'na' businessBannerUrl"),DB::Raw("'na' businesslatitude"),DB::Raw("'na' businesslongitude") ) 
        ->orderBy('book_date', 'DESC')
        ->orderBy('service_time', 'DESC')
        ->get(); 
		

		if(!isset($day_bookings[0]))
	    {
	      
	      $data= ["message" => "failure",    "status_code" =>  2010 ,
	      'detailed_msg' => 'Booking information not found!' ,'results' => array() ];

	      return json_encode( $data);
	      
	    }
		foreach ($day_bookings as $bookItem) {
			$bookItem->appointmentDate = date('d-m-Y', strtotime($bookItem->appointmentDate));
			$serviceDuration = ServiceDuration::where('bin',$bookItem->bin)
                ->where('start_time', $bookItem->appointmentTime)
                ->get();

             foreach ($serviceDuration as $durationItem) {
             	$bookItem->startTime = date('h:i a', strtotime($durationItem->start_time));
             	$bookItem->endTime = date('h:i a', strtotime($durationItem->end_time));
             }

             //get bin name
			/*$businessOwner = BusinessOwner::where('bin',$bookItem->bin)->get();
			foreach ($businessOwner as $owner)
			{
				$getUserId = $owner->user_id;

			}*/
			$userName = User::where('bin',$bookItem->bin)->get();
				
			foreach ($userName as $name) 
			{
				if($name->mname!=null)
				{
					$bookItem->businessUserName=$name->fname ." ". $name->mname." ".$name->lname ;
				}
				else
				{
					$bookItem->businessUserName=$name->fname ." ".$name->lname ;
				}

			}
			$business = Business::where('id',$bookItem->bin)->get();
			foreach ($business as $businessItem) {
				$bookItem->businessName=$businessItem->name;
				$bookItem->businessPhone=$businessItem->phone_pri;
				$bookItem->businessAltPhone=$businessItem->phone_alt;
				$bookItem->businesslatitude=$businessItem->latitude;
				$bookItem->businesslongitude=$businessItem->longitude;
				$bookItem->businessBannerUrl=$businessItem->banner;
				if($businessItem->landmark!=null)
				{
					$lan = ", ";
				}
				else
				{
					$lan = "";
				}
				if($businessItem->city!=null)
				{
					$cty = ", ";
				}
				else
				{
					$cty = "";
				}
				if($businessItem->state!=null)
				{
					$sta = ", ";
				}
				else
				{
					$sta = "";
				}
				if($businessItem->pin!=null)
				{
					$pn = ", ";
				}
				else
				{
					$pn = "";
				}
				$bookItem->businessAddress=$businessItem->locality .$lan.$businessItem->landmark.$cty.$businessItem->city.$sta.$businessItem->state.$pn.$businessItem->pin ;

			}
			$businessService = BusinessService::where('srv_code',$bookItem->services)
				->select('srv_name as serviceName','id as serviceId','pricing as price',DB::Raw("'na' duration"))
				->get();
			foreach ($businessService as $servicesItem) {
				$serviceData = BusinessService::where('id',$servicesItem->serviceId)->get();
				foreach ($serviceData as $keyData) {
					if($keyData->duration_hr > 0)
					{
						$servicesItem->duration = $keyData->duration_hr. " hour" . $keyData->duration_min. " min";
					}
					else{
						$servicesItem->duration = $keyData->duration_min. " min";
					}
				}
			}	
			$bookItem->services = $businessService;
		}
		if($requestType == "ALL")
		{
			$data= ["message" => "success", "status_code" =>  2009 ,  'detailed_msg' => 'Last Booking' ,  
		'results' => $day_bookings ]; 
		}
		else if($requestType == "LAST")
		{
			$data= ["message" => "success", "status_code" =>  2009 ,  'detailed_msg' => 'Last Booking' ,  
		'results' => $day_bookings[0] ]; 
		}
		else
		{
			$data= ["message" => "failure",    "status_code" =>  2010 ,
	      'detailed_msg' => 'Please select your type correctly!' ,'results' => array() ];

	      return json_encode( $data);
		}
		
		return json_encode( $data);

  	}
  	

	public function getAllReviewsForBusiness(Request $request)
	{
			$rate =  $rateAvg = $ratingFiveCount = $ratingFourCount = 
	    	 $ratingThreeCount =  $ratingTwoCount =  $ratingOneCount ="0";  

			
			$result = DB::table("ba_business_review")
			->join("ba_profile", 'ba_profile.id', "=", "ba_business_review.member_id")
			->where("ba_business_review.bin", $request->bin)
			->select("ba_business_review.member_id as reviewAuthorId", 
				"ba_business_review.title", "ba_business_review.body", "ba_business_review.rating",
				 "ba_business_review.added_on as addedOn",
				"ba_business_review.response", "ba_business_review.responded_on as respondedOn",
				"ba_profile.fullname as reviewAuthorName",
				"ba_profile.phone as reviewAuthorPhone"  )
			->get();

		 

			foreach($result as $item)
			{
				 
				if($item->response == null)
				{
					$item->response = "na";
				}
				
				if($item->respondedOn == null)
				{
					$item->respondedOn = "na";
				} 
				//rating
							
				$rate += $item->rating;
				$ratedata = ''.$rate.'';
				
			}
			
			
			$count = $result->count();
			if($count>1){
					$checkrate = $rate / $count;
					$rateAvg = $checkrate;
					$rateAvgdata = ''.$rateAvg.'';

			
			}
			else{
				$rateAvgdata = ''.$rate.'';

			}


			$countRating = DB::table("ba_business_review")
			->where("bin", $request->bin)
			->select('rating',DB::raw('count(id)  as 
					total_count'))
			->groupby("rating")
			->get();
			foreach ($countRating as $keyitem) {
				if($keyitem->rating ==1){
					$ratingOneCount= $keyitem->total_count;
				}
				elseif($keyitem->rating ==2){
					$ratingTwoCount= $keyitem->total_count;
				}
				elseif($keyitem->rating ==3){
					$ratingThreeCount= $keyitem->total_count;
				}
				elseif($keyitem->rating ==4){
					$ratingFourCount= $keyitem->total_count;
				}
				elseif($keyitem->rating ==5){
					$ratingFiveCount= $keyitem->total_count;
				}
				else{

				}
			}
		  


			$data= ["message" => "success", "status_code" =>  2011 ,
			'detailed_msg' => 'Business reviews are fetched.' ,
			'totalRating' => $ratedata,
			'ratingAverage'=>$rateAvgdata, 
			'ratingFiveCount' => $ratingFiveCount,
			'ratingFourCount' => $ratingFourCount,
			'ratingThreeCount' => $ratingThreeCount,
			'ratingTwoCount' => $ratingTwoCount,
			'ratingOneCount' => $ratingOneCount,
			'results' => $result  ];
			return json_encode( $data);
	
  	}





  	public function customerPasswordChange(Request $request)
	{
		if($request->phoneNumber == ""|| $request->password == "")
		{
			$err_msg = "Field missing!";
			$err_code = 9000;
						
			$data = array(
				"message" => "failure" , 'status_code' => $err_code ,
				'detailed_msg' => $err_msg) ;
			
			return json_encode( $data ); 
		}
		$userInfo = User::where("phone", $request->phoneNumber)->first();
		
		if(isset($userInfo))
		{
			$hashed = Hash::make($request->password);
			$name = $request->customerName;
			$arr2 = explode(" " , $name);
			$totalname = count((array)$arr2);

			//$userInfo->phone = $request->customerPhone;
			$userInfo->password = $hashed;
			$save = $userInfo->save();
			if($save)
			{
				$err_status ="success";
				$err_code =  2017;
		        $err_msg =  "New Password is created successfully!";
		        $userId=$userInfo->id;

		       	
				

			}
			
		}
		else 
		{

			$err_msg = "Change your Password is failed. Please type your correct phone number!";
			$err_code = 2018;
			$err_status ="failure";
			$userId=0;
			

		}
		
			 
		$data = array(
				"message" => $err_status  , "status_code"=>$err_code ,
				'detailed_msg' => $err_msg, "userId"=>$userId) ;


		return json_encode($data); 
		

  	}



  	public function customerProfile(Request $request)
  	{
  		$user_info =  json_decode($this->getUserInfo($request->userId)); 
  		if(  $user_info->user_id == "na" )
		{
			$data= ["message" => "failure", "status_code" =>  901 ,
			'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
			return json_encode($data);
		}


		$profile = CustomerProfileModel::find( $user_info->member_id );

		if( !isset($profile))
		{
			$data= ["message" => "failure", "status_code" =>  999  , 'detailed_msg' => 'Profile not found.'  ];
			return json_encode($data);
		}
 

  		$complete_profile = array();
  		
  		$complete_profile['memberName'] = $profile->fullname;
  		$complete_profile['mname'] = $profile->mname;
  		$complete_profile['lname'] = $profile->lname;
  		$complete_profile['fname'] = $profile->fname;
  		$complete_profile['landmark'] = $profile->landmark;
  		$complete_profile['city'] = $profile->city;
  		$complete_profile['state'] = $profile->state;
  		$complete_profile['locality'] = $profile->locality;
  		$complete_profile['pin'] = $profile->pin_code;
  		$complete_profile['address'] = 
  		$profile->locality . ", " . $profile->landmark . " " . $profile->city . ", " . $profile->state . " - " . $profile->pin_code; 

       $complete_profile['email'] = $profile->email;

        if($profile->profile_photo != null)
        {
        	$complete_profile['profilePicture'] = URL::to('/public/assets/image/member/') . $profile->profile_photo ;
        }
        else 
        {
        	$complete_profile['profilePicture'] = URL::to('/public/assets/image/no-image.jpg' );

        }
        if($profile->cover_photo != null)
        {
        	$complete_profile['coverPhoto'] = URL::to('/public/assets/image/member/') . $profile->cover_photo ;
        }
        else 
        {
        	$complete_profile['coverPhoto'] =URL::to('/public/assets/image/no-image.jpg' ); 
        }

 	   

		$data= ["message" => "success", "status_code" =>  2013 ,
		'detailed_msg' => 'Profile fetched successfully.' , 
		'results' => $complete_profile];
		return json_encode( $data);
  	}




  	public function customerUpdateProfile(Request $request)
	{

		$user_info =  json_decode($this->getUserInfo($request->userId));
		if(  $user_info->user_id == "na" )
		{
			$data= ["message" => "failure", "status_code" =>  901 , 
			'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
			return json_encode($data);
	    } 

 
        if( $user_info->category  != 0 )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to end-users only.'  ];
          return json_encode($data);  
        }
 

      	$member_info = CustomerProfileModel::find($user_info->member_id);

        if( !isset($member_info))
        {
        	$member_info = new  CustomerProfileModel;   
        }

        $name = $request->memberName;
        $names = explode(" " , $name);

        $member_info->fname = $names[0]; 
        $member_info->mname = ( count($names) >  1 ? $names[1] : " " ); 
        $member_info->lname = ( count($names) >  2 ? $names[2] : " " );  
        $member_info->fullname= $name;

 
		  
			
			if( $request->email != "")
			{
				$member_info->email = $request->email ;
			}
			if( $request->phone != "" )
			{
				$member_info->phone = $request->phone ;
			}
			if( $request->locality != "" )
			{
				$member_info->locality = $request->locality ;
			}
			if( $request->landmark != "" )
			{
				$member_info->landmark = $request->landmark ;
			}
			if( $request->city != "" )
			{
				$member_info->city = $request->city ;
			}
			if( $request->state != "")
			{
				$member_info->state = $request->state ;
			}
			if( $request->mpin != "")
			{
				$member_info->pin_code  = $request->mpin ;
			}
			
			$member_info->latitude = ( $request->latitude  != "" ? $request->latitude : 0 );  
			$member_info->longitude = ( $request->longitude  != "" ? $request->longitude : 0 );  


			$save = $member_info->save();

			if($save)
            {
            	$data= ["message" => "success", "status_code" =>  1071 ,  'detailed_msg' => 'Your profile is successfully updated.']; 
            }

        else{
            $data= ["message" => "success", "status_code" =>  1072 ,  'detailed_msg' => 'Update profile failed.'];
        }
        return json_encode( $data);   
  	}

  	
	
	
	
  	public function otpForLogin(Request $request)
	{
		if($request->customerPhone == "")
		{
			$err_msg = "Field missing!";
			$err_code = 9000;
						
			$data = array(
				"message" => "failure" , 'status_code' => $err_code ,
				'detailed_msg' => $err_msg) ;
			
			return json_encode( $data ); 
		}
		$rand = mt_rand(434343, 999999);
			
			
		$err_status ="success";
		$err_code =  3503;
        $err_msg =  "Your OTP sent as SMS to your registered mobile.";

       	$phone = $request->customerPhone;

        $otp_msg = "Your  OTP is " . $rand.  urlencode(". Please DO NOT SHARE this OTP with anyone. bookTou never calls you asking for OTP." ) ; //staging - u8IKLyQc6v6 Prod: 8KSDYMB+AxA

        $ch = curl_init('https://www.txtguru.in/imobile/api.php?');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=ezrasorokhaiba&password=Mayol@2019&source=MES-UPR&dmobile=". $phone . "&message=" . $otp_msg);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
		
		$data = array(
				"message" => $err_status  , "status_code"=>$err_code ,
				'detailed_msg' => $err_msg) ;


		return json_encode($data); 
		

  	}
	
	
	  
 

	public function addOrRemoveFavourite(Request $request)
    {
		$user_info =  json_decode($this->getUserInfo( $request->userId ));
	    if(  $user_info->user_id == "na" )
	    {
	        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
	        return json_encode($data);
	    }


		if(    $request->bin == ""     )
	   {
		 $data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No business selected.'  ];
		 return json_encode($data);
	   }
	   
	    $business = Business::find($request->bin);  
		
		if( !isset($business))
		{
		  
		  $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		  'detailed_msg' => 'Business information not found!'  ];
		  return json_encode( $data);
		  
		}
		
		
		$fav_business =  FavouriteBusiness::where("member_id", $user_info->member_id )
		->where("bin", $request->bin)
		->first();
		
	
		if(  isset($fav_business))
		{
		   //removed from list 
		   $fav_business->delete(); 
			$err_msg = "Business removed from your favourite list.";
		}
		else 
		{
			$fav_business = new FavouriteBusiness;
			$fav_business->member_id = $user_info->member_id ;
			$fav_business->bin = $request->bin;  
			$fav_business->save(); 
			$err_msg = "Business added to your favourite list.";
		} 
		 
		
		$data= ["message" => "success", "status_code" =>   1061,  'detailed_msg' =>  $err_msg  ];
		
		return json_encode( $data);
		
	  } 
  

	  public function sendSms(Request $request)
	{
		if($request->customerPhone == "")
		{
			$err_msg = "Field missing!";
			$err_code = 9000;
						
			$data = array(
				"message" => "failure" , 'status_code' => $err_code ,
				'detailed_msg' => $err_msg) ;
			
			return json_encode( $data ); 
		}
		$message = "This is BookTou Test SMS";
		$countPhone = $request->customerPhone;
		$arr2 = explode(" " , $countPhone);
		$message = "This is BookTou Test SMS";
    
      	foreach ($arr2 as $phoneSend) {
      					
			$err_status ="success";
			$err_code =  3506;
	        $err_msg =  "This is BookTou Test SMS";

	       	$phone = $phoneSend;
        
	        $ch = curl_init('https://www.txtguru.in/imobile/api.php?');
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=ezrasorokhaiba&password=Mayol@2019&source=MES-UPR&dmobile=". $phone . "&message=" . $message);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	        $data = curl_exec($ch);
      	}
		
		
		$data = array(
				"message" => $err_status  , "status_code"=>$err_code ,
				'detailed_msg' => $err_msg) ;


		return json_encode($data); 
		

  	}
  	public function updateIndividualServiceBooking(Request $request)
   	{
	   
		$user_info =  json_decode($this->getUserInfo($request->userId));
		if(  $user_info->user_id == "na" )
		{
			$data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
			return json_encode($data);
		}


		$business = Business::find($request->bin);
		if( !isset($business))
		{
		  
		  $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		  'detailed_msg' => 'Business information not found!'  ];
		  return json_encode( $data);
		  
		}
	
	
		if($request->bookingDate == ""  )
		{
		 $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
		}
		$newbooking = ServiceBooking::where('id',$request->bookId)->first();
		if(!isset($newbooking))
		{
			$message = "failure";
			$err_code = 2020;
			$err_msg = "Failed to place your booking.";
		}
		else{
			
			$newbooking->book_date = date('Y-m-d', strtotime($request->bookingDate));
			$newbooking->book_by = $user_info->user_id;
			if($request->staffId!=null){
				$newbooking->staff_id = $request->staffId;
			}
			
			$newbooking->book_status = "new";
			$save=$newbooking->save();

			if($save)
			{
				$newbookingDetails = ServiceBookingDetails::where('book_id',$newbooking->id)->orderby('service_time', 'ASC')
					->get();

				if(isset($newbookingDetails))
				{
					foreach ($newbookingDetails as $keyidd) 
					{
						$arrayTime[]=$keyidd->service_time;
						$serviceData = ServiceBookingDetails::find($keyidd->id);
						$serviceData->delete();
					}
					
				}
					
				$serviceCodes = $request->serviceCode;
				$codeService = explode("[",$serviceCodes);
	        	$codeService2 = explode("]",$codeService[1]);
	        	$serviceCodes3 = explode(",",$codeService2[0]);
                $serviceTimes = $arrayTime;

                $pos =0;
                foreach ($serviceCodes3 as $serviceCode) 
                { 
                        
                            $newbookingDetails = new ServiceBookingDetails;
                            $newbookingDetails->book_id = $newbooking->id;
                            $newbookingDetails->service_code = $serviceCode;
                            $newbookingDetails->service_time = $serviceTimes[$pos];
                            $newbookingDetails->status = "new";
                            $newbookingDetails->save();
                            $pos++;

                    
                }      				
		        
				$message = "success";
				$err_code = 2019;
				$err_msg = "Your booking is update successfully.";
			}
			else 
			{
				$message = "failure";
				$err_code = 2020;
				$err_msg = "Failed to place your booking.";
			}
		}
        
		
		$data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg  ];
	
		return json_encode( $data);
  	}

  	public function getListOfFavouriteBusinessDetails(Request $request)
	{
		$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data); 
        }
		
		$businesses = DB::table("ba_business")
		->join("ba_fav_business","ba_business.id","=","ba_fav_business.bin") 
		->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category") 
		->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 'ba_business.shop_number as shopNumber',  
		'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone', 
		"ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin", 
		  'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' , 
		  "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
		DB::Raw("'na' promoImgUrl"),DB::Raw("'na' favourite"), 
		"category as businessCategory", 
		"ba_business.main_module as mainModule", 
		"ba_business.sub_module as subModule", 
		"is_open as isOpen",
		"ba_business_category.main_type as mainBusinessType", 
		DB::Raw("'0' distance") ,  DB::Raw("'0' duration") , 
		"is_verified as isVerified" , "ba_business.allow_self_pickup as allowSelfPickup" ) 
		  ->where('ba_fav_business.member_id',$user_info->member_id )
		  ->paginate(5);
		  
	  
	  	foreach($businesses as $item)
		{
			if($item->profileImgUrl!=null)
			{
				$item->profileImgUrl = URL::to('/public'.$item->profileImgUrl);	
			}
			else{
				$item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg' );	
			}
			if($item->bannerImgUrl!=null)
			{
				$item->bannerImgUrl = URL::to('/public'.$item->bannerImgUrl);	
			}
			else{
				$item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg' );	
			}
			if($item->promoImgUrl!='na')
			{
				$item->promoImgUrl = URL::to('/public'.$item->promoImgUrl);	
			}
			else{
				$item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg' );	
			}

			
			$favorite = FavouriteBusiness::where('member_id', $user_info->member_id  )
			->where('bin',$item->bin)->get();
			
			$item->favourite = count($favorite);


			$item->latitude = number_format($item->latitude,8,".", ""); 
			$item->longitude = number_format($item->longitude,8,".", "");  
				
		}
		
	  	$result = array( 'results' => $businesses ) ;

	    return json_encode($result); 
  	}


  	public function getAllBusinessReviewsByCustomer(Request $request)
   	{
	
		  $user_info =  json_decode($this->getUserInfo($request->userId)); 
		  if(  $user_info->user_id == "na" )
		  {
			$data= ["message" => "failure", "status_code" =>  901 ,
			'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
			return json_encode($data);
		  }

	    
		   $business_reviews = DB::table("ba_business_review")
		   ->join("ba_business", "ba_business.id", "=", "ba_business_review.bin" ) 
		   ->where("member_id",$user_info->member_id)  
		   ->select(
		   	"ba_business_review.bin as businessId", 
		   	"ba_business.name as businessName",
		   	"ba_business_review.member_id as reviewId", 
		   	"ba_business_review.title", 
		   	"ba_business_review.body", 
		   	"ba_business_review.rating", 
		   	"ba_business_review.added_on as addedOn" )
		   ->get();
	    
	    	$rate = 0;
			foreach($business_reviews as $item)
			{
							
						
				$item->addedOn = date('d-m-Y', strtotime($item->addedOn));	
				//rating
				
				
				$rate += $item->rating;

				 
				
			}

	
			$data= ["message" => "success", "status_code" =>  2021 ,
			'detailed_msg' => 'Business reviews are fetched.' ,
			'results' => $business_reviews];
			return json_encode( $data);
	
  	} 




  	
  	public function deleteUserByPhoneNumber(Request $request)
    {
        $userTable = User::where('phone',$request->phone)->get();
        if(count($userTable)>0)
        {
            foreach ($userTable as $keyUser) 
            {
                $userData = User::find($keyUser->id);

                $ProfileId = $keyUser->profile_id;

                $delete_profile = DB::table('ba_profile')
                ->where('id',$ProfileId)
                ->delete();
                
                $userData->delete();
            }
            $data= ["message" => "success", "status_code" =>  5555 ,  'detailed_msg' => 'Delete User.'  ];
    
        }
        else{
            $data= ["message" => "failure", "status_code" =>  5556 ,  'detailed_msg' => 'Delete User Fail.'  ];
        }
        return json_encode( $data);
    }
    public function getUserByPhoneNumber(Request $request)
    {
        $userTable = User::where('phone',$request->phone)
        	->select('phone','fname','mname','lname')
        	->get();
        if(count($userTable)>0)
        {
            $data= ["message" => "success", "status_code" =>  5553 ,  'detailed_msg' => 'User List fetch.','result' => $userTable  ];
    
        }
        else{
            $data= ["message" => "failure", "status_code" =>  5554 ,  'detailed_msg' => 'User List fail to fetch.'  ];
        }
        return json_encode( $data);
    }  

          
 	
 	//Get all review for customer
    public function getAllReviewsForCustomer(Request $request)
    {
       
          if ($request->customerId == "")
          {

          	$data= [ "message" => "failure", "status_code" =>  999 , 'detailed_msg ' => 'Missing data to perform action.' ];
          	return json_encode($data); 
          }

 

            $reviewDetails = CustomerReviewModel::where("customer_id",$request->customerId)
            ->select("id as reviewId","customer_id as customerId",
                "member_id as agentId","order_no as orderNo",
                "title as reviewTitle","body as reviewMessage",
                "rating as reviewRating","added_on as reviewDate")
            ->first();

 

         if (!isset($reviewDetails)) 
         {
         	$data= ["message" => "success", "status_code" =>  5020 ,  'detailed_msg' => 'No review record found.' ];
          }
          else
          {
             $data= ["message" => "success", "status_code" =>  5021 ,  'detailed_msg' => 'Review details are fetched.' , 
             'results' =>  $reviewDetails ];
          } 

        return json_encode( $data);
       
    }  



}
