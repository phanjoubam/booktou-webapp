<?php

namespace App\Http\Controllers\StoreFront;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;

use App\User;
use App\CustomerProfileModel;
use App\CallbackEnquiryModel;
use App\QuoteRequestModel;
use App\SubscriberModel;
use App\OrderSequencerModel;
use App\ContactFormModel;
use App\Business;
use App\MerchantOrderSequencer;
use App\ServiceBookingDetails;
use App\PickAndDropRequestModel;
use App\ServiceRating;
use App\ShoppingBasketServiceProductModel;
use App\Traits\Utilities;
use Razorpay\Api\Api;

class StoreFrontController extends Controller
{
    //
    use Utilities;


    public function  betaTest (Request $request)
    {


        $keyword = "chicken";
        $category = "restaurant";


        if( strcasecmp($category, "all") ==  0 )
        {
            $products = DB::table("ba_products")
            ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
            ->where("ba_business.is_block", "no")
            ->where(function($query) use ( $keyword   ){

                $query->where('ba_products.pr_name',  "like",  "%$keyword%");
                $query->orWhere("ba_products.description", "like",  "%$keyword%" ) ;

            })
            ->select("ba_products.*")
            ->paginate(30);
        }
        else
        {
            $products = DB::table("ba_products")
            ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
            ->where("ba_business.is_block", "no")
            ->where("ba_business.category",  $category )
            ->where(function($query) use ( $keyword   ){

                $query->where('ba_products.pr_name',  "like",  "%$keyword%");
                $query->orWhere("ba_products.description", "like",  "%$keyword%" ) ;
                $query->orWhere("ba_products.tags", "like",  "%$keyword%" ) ;

            })
            ->select("ba_products.*")
            ->paginate(30);

        }



        $price_range = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
        ->where("ba_business.is_block",   "no"  )
        ->where("ba_products.pr_name", "like",  "%$keyword%" )
        ->orWhere("ba_products.description", "like",  "%$keyword%" )
        ->select( DB::Raw("min(actual_price) as minimum"), DB::Raw("max(actual_price) as maximum"), DB::Raw("count(*) as totalProducts")  )
        ->first();


        $data = array(    'price_range' => $price_range , 'products' =>  $products  );
        return view("store_front.beta")->with(  $data );
    }



    public function indexBeta(Request $request)
    {

        $featured_products  = DB::table("ba_product_promotions")
        ->join("ba_products", "ba_products.pr_code", "=", "ba_product_promotions.pr_code")
        ->select("ba_products.*", DB::Raw("'na' productThumbnail") , DB::Raw("'na' productPhoto") , "ba_product_promotions.todays_deal")
        ->orderBy("ba_products.pr_name")
        ->get();



        $cdn_url =   config('app.app_cdn') ;
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public
        $cms_base_url = config('app.app_cdn')  ;
        $cms_base_path = $cdn_path  ;
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";

        $featured = array();
        foreach( $featured_products as $item)
        {
            $all_photos = array();
            $all_thumbnails  = array();
            $files =  explode(",",  $item->photos );
            if(count($files) > 0 )
            {

                $product_folder_path =  $cms_base_path .  "/assets/image/store/bin_" .   $item->bin .  "/";
                $product_folder_url = $cms_base_url . "/assets/image/store/bin_" .   $item->bin .  "/";

                $files=array_filter($files);
                foreach($files as $file )
                {
                    $source = $product_folder_path .  $file;
                    if(file_exists( $source  ))
                    {
                        $pathinfo = pathinfo( $source  );
                        $target_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
                        $target_file_path   = $cms_base_path .  "/assets/image/store/bin_" . $item->bin  .  "/"  . $target_file_name ;

                        if( file_exists( $target_file_path) )
                        {
                            $all_photos[]  =  $product_folder_url . $target_file_path;
                            $all_thumbnails[] =  $product_folder_url . $target_file_name;
                        }
                        else
                        {
                            $all_photos[]  =  $product_folder_url .   $file;
                            $all_thumbnails[] = $product_folder_url .   $file;
                        }

                    }
                    else
                    {
                        $all_photos[]   = $no_image ;
                    }

                }
            }

            $item->photos =  $all_photos;
            $item->productPhoto = count( $all_photos ) > 0 ?  $all_photos[0] : $no_image ;
            $item->productThumbnail = count($all_thumbnails) > 0 ? $all_thumbnails[0] : $no_image ;

            $featured[] =  $item;
         }

         shuffle(  $featured );



         //deals
        $deal_products  = DB::table("cms_hot_deals")
        ->join("ba_products", "ba_products.id", "=", "cms_hot_deals.prid")
        ->select("ba_products.*", DB::Raw("'na' productThumbnail") , DB::Raw("'na' productPhoto")  )
        ->orderBy("ba_products.pr_name")
        ->get();

        $deals = array();
        foreach( $deal_products as $item)
        {
            $all_photos = array();
            $all_thumbnails  = array();
            $files =  explode(",",  $item->photos );
            if(count($files) > 0 )
            {

                $product_folder_path =  $cms_base_path .  "/assets/image/store/bin_" .   $item->bin .  "/";
                $product_folder_url = $cms_base_url . "/assets/image/store/bin_" .   $item->bin .  "/";

                $files=array_filter($files);
                foreach($files as $file )
                {
                    $source = $product_folder_path .  $file;
                    if(file_exists( $source  ))
                    {
                        $pathinfo = pathinfo( $source  );
                        $target_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
                        $target_file_path   = $cms_base_path .  "/assets/image/store/bin_" . $item->bin  .  "/"  . $target_file_name ;

                        if( file_exists( $target_file_path) )
                        {
                            $all_photos[]  =  $product_folder_url . $target_file_path;
                            $all_thumbnails[] =  $product_folder_url . $target_file_name;
                        }
                        else
                        {
                            $all_photos[]  =  $product_folder_url .   $file;
                            $all_thumbnails[] = $product_folder_url .   $file;
                        }

                    }
                    else
                    {
                        $all_photos[]   = $no_image ;
                    }

                }
            }

            $item->photos =  $all_photos;
            $item->productPhoto = count( $all_photos ) > 0 ?  $all_photos[0] : $no_image ;
            $item->productThumbnail = count($all_thumbnails) > 0 ? $all_thumbnails[0] : $no_image ;
            $deals[] =  $item;
         }

         shuffle(  $deals );




        $first_group = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
        ->where("ba_business.is_block",   "no"  )
         ->where("ba_business.category",   "restaurant")
        ->where("ba_products.category",  "biryani" )
        ->select("ba_products.*", "ba_business.name")
        ->orderBy(DB::raw('RAND()'))
        ->paginate(8);

        $second_group = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
        ->where("ba_business.is_block",   "no"  )
         ->where("ba_business.category",   "appliance")
        ->where("ba_products.category",  "electrical items" )
        ->select("ba_products.*", "ba_business.name")
        ->orderBy(DB::raw('RAND()'))
        ->paginate(8);



        $recommended_list = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
        ->where("ba_business.is_block",   "no"  )
         ->where("ba_business.category",   "restaurant")
        ->where("ba_products.category",  "non-veg items" )
        ->select("ba_products.*", "ba_business.name")
        ->orderBy(DB::raw('RAND()'))
        ->paginate(12);


        $slides = DB::table('cms_banner_sliders')
                    ->select()
                    ->get();


        $data = array('featured_products' => $featured, 'deals' => $deals , 'first_group' => $first_group, 'second_group' =>$second_group,
            'recommended_list' => $recommended_list,
            'slides' =>  $slides);

        return view("store_front.index_beta")->with( $data );
    }


    public function index(Request $request)
    {

        //saving session

        $cdn_url =   config('app.app_cdn') ;
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public
        $cms_base_url = config('app.app_cdn')  ;
        $cms_base_path = $cdn_path  ;
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";

         //deals
        $openbusiness = DB::table("ba_business")
        ->where("is_open","open")
        ->where("is_block","no")
        ->get();

        $bizid =  array();
        foreach ($openbusiness as $bizlist) {
            $bizid[] = $bizlist->id;
        }

        $deal  = DB::table("ba_product_promotions")
        ->where("todays_deal","yes")
        ->get();
        $prsub  = array();

        foreach ($deal as $hotdeals) {
             $prsub[] = $hotdeals->prsubid;
        }


        $deal_product = DB::table("ba_product_variant")
        ->join("ba_products","ba_product_variant.prid","=","ba_products.id")
        ->whereIn("ba_product_variant.prsubid", $prsub )
        ->select("ba_products.*","ba_product_variant.*",DB::Raw("'[]' photos"))
        ->orderBy(DB::raw('RAND()'))
        ->paginate(12);

        $_prsubid = array();
        foreach($deal_product as $items)
        {
           $_prsubid[] =  $items->prsubid;
        }

        $hotdeals_photos = DB::table("ba_product_photos")
        ->whereIn("ba_product_photos.prsubid",$_prsubid)
        ->get();

        foreach($deal_product as $items)
        {
            foreach($hotdeals_photos as $photos)
            {
                if ($items->prsubid==$photos->prsubid) {
                    $items->photos=$photos->image_url;
                    break;
                }
            }
        }

        $business_group = DB::table("ba_business")
        ->where("ba_business.is_block",   "no")
        ->where("ba_business.is_open",   "open")
        ->where("ba_business.category",   "restaurant")
        //->where("ba_products.category",  "biryani")
        ->orderBy(DB::raw('RAND()'))
        ->get();

        // section group details for index
        $section = DB::table("cms_section_type")
        ->where("allow_view","yes")
        ->where("status",1)
        ->select("cms_section_type.*")
        ->get();

        $sectionid=array();
        foreach ($section as $items) {
            $sectionid[] = $items->id;
        }

        $sectionDetails = DB::table("cms_section_details")
        ->whereIn("section_id",$sectionid)
        ->where("status",1)
        ->select("cms_section_details.*",
                 DB::Raw("'' photos"),
                 DB::Raw("'' pr_code"),
                 DB::Raw("'' pr_name"),
                 DB::Raw("'' bin"),
                 DB::Raw("'' biz_name"),
                 DB::Raw("'' actual_price"))
        ->get();

        $prsubid = $prid = array();
        foreach ($sectionDetails as $items) {
            $prsubid[] = $items->prsubid;
        }

        $product_variant = DB::table("ba_product_variant")
        ->join("ba_products","ba_product_variant.prid","=","ba_products.id")
        ->whereIn("ba_product_variant.prsubid",$prsubid)
        ->get();

        // foreach ($product_variant as $items) {
        //     $prid[]=$items->prid;
        // }

        $product_photos = DB::table("ba_product_photos")
        ->whereIn("ba_product_photos.prsubid",$prsubid)
        ->get();

        $bin = array();

        foreach ($sectionDetails as $items) {
             foreach ($product_variant as $list) {

                if ($items->prsubid==$list->prsubid) {
                    $items->pr_code=$list->pr_code;
                    $items->pr_name=$list->pr_name;
                    $items->bin=$list->bin;
                    $items->actual_price=$list->actual_price;
                    $bin[]=$list->bin;
                    break;
                    }
             }

             foreach ($product_photos as $photos) {

                if ($items->prsubid==$photos->prsubid) {
                    $items->photos=$photos->image_url;
                    break;
                    }
             }


        }

        //section group details ends here

        $business = DB::table("ba_business")
        ->whereIn("id",$bin)
        ->where("is_open","open")
        ->where("is_block","no")
        ->select("ba_business.name","ba_business.id")
        ->get();

        foreach ($sectionDetails as $items) {
            foreach ($business as $list) {
               if ($items->bin==$list->id) {
                $items->biz_name = $list->name;
                break;
               }
            }
        }

        $recommended_list = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
        ->where("ba_business.is_open",   "open"  )
        ->where("ba_business.is_block",   "no"  )
        ->where("ba_business.category",   "restaurant")
        ->where("ba_products.category",  "non-veg items" )
        ->select("ba_products.*", "ba_business.name",
                DB::Raw("'' photos"),
                DB::Raw("'' actual_price"),
                DB::Raw("'' unit_price"),
                DB::Raw("'' prsubid")
                )
        ->orderBy(DB::raw('RAND()'))
        ->paginate(12);

        $proid = $prosubid = array();

        foreach ($recommended_list as $items) {
               $proid[]=$items->id;
        }

        $variantlist = DB::table("ba_product_variant")
        ->whereIn("prid",$proid)
        ->get();

        foreach ($variantlist as $items) {
               $prosubid[]=$items->prsubid;
        }

        $varianphotos = DB::table("ba_product_photos")
        ->whereIn("prsubid",$prosubid)
        ->get();

        foreach ($recommended_list as $items) {

            foreach ($variantlist as $list) {
                if ($items->id==$list->prid) {
                    $items->actual_price = $list->actual_price;
                    $items->unit_price = $list->unit_price;
                    $items->prsubid = $list->prsubid;
                    break;
                }
            }
            foreach ($varianphotos as $listphotos) {
                if ($items->prsubid==$listphotos->prsubid) {
                     $items->photos = $listphotos->image_url;
                     break;
                }
            }
        }


        $banner_slide = DB::table('cms_banner_sliders')
        ->where('bin',0)
        ->select()
        ->get();

        $slides = DB::table('cms_banner_sliders')
                    ->where("published","yes")
                    ->orderBy("position", "asc")
                    ->get();

        //premium business
            $permium_business= DB::table("ba_business")
            ->join("ba_premium_business", "ba_premium_business.bin", "=", "ba_business.id")
            ->where("is_open",'open')
            ->where("is_block",'no')
            ->where("frno",0)
            ->select("ba_business.id","name","tags","shop_number","banner","locality","landmark","city","state","rating","pin","is_open","category",
                DB::Raw("'YES' buttonStatus"))
            ->get();
        //premium business  ends here

        $assists_task = DB::table('ba_assist_category')
        ->orderBy('display_order','asc')
        ->get();

        $bot_system = DB::table('ba_bot_chat')
        ->get();

        $data = array( 
            'deals' => $deal_product, 
            'recommended_list' => $recommended_list,
            'permium_business'=>$permium_business,'slides' =>$slides,
            "sectionDetails"=>$sectionDetails,"section"=>$section,
            'assists'=>$assists_task,'bot'=>$bot_system); 
        return view("store_front.index")->with($data);
    }




    public function resetAll(Request $request)
    {
       $request->session()->flush();
       return redirect('/');
    }



    public function viewProductDetails(Request $request)
    {


        $cdn_url =   config('app.app_cdn')    ;
        $cdn_path =  config('app.app_cdn_path')  ; // cnd - /var/www/html/api/public

        $cms_base_url =  config('app.app_cdn')   ;
        $cms_base_path = $cdn_path  ;
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";


        try
        {

           $pid = 0;
           $prsubid =   $request->prsubid;

           $product_info  = DB::table("ba_products")
           ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
           ->where("ba_product_variant.prsubid",$prsubid)
           ->select("ba_products.*","ba_product_variant.*")
           ->first();

           $product_photos = DB::table("ba_product_photos")
           ->where("ba_product_photos.prsubid",$product_info->prsubid)
           ->get();


           if(!isset($product_info))
           {
               return redirect()->back();
           }
           $category = $product_info->category;
           $tags = $product_info->tags;
           $list_of_tags = array();



           //if product tags is not equal to null for the selected product then scan the product details
           $business = DB::table("ba_business")
           ->where("is_open","open")
           ->where("is_block","no")
           ->get();

           $bin = array();

           foreach ($business as $items) {
                $bin[] = $items->id;
           }

           if ($product_info->tags!="") {

            $list_of_tags = array_merge($list_of_tags,explode(",",$product_info->tags));
            $similar_products = DB::table("ba_products")
                ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
                ->whereIn("ba_products.tags",$list_of_tags)
                ->whereIn("ba_products.bin",$bin)
                ->select("ba_products.*","ba_product_variant.*",DB::Raw("'' photos"))
                ->orderBy(DB::raw('RAND()'))
                ->paginate(20);

           }else{

                $similar_products = DB::table("ba_products")
                ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
                ->where("ba_products.category",$category)
                ->whereIn("ba_products.bin",$bin)
                ->select("ba_products.*","ba_product_variant.*",DB::Raw("'' photos"))
                ->orderBy(DB::raw('RAND()'))
                ->paginate(20);
           }
           //product scans complete for similar items

           //if similar products for the selected product details is found
           if ($similar_products) {

                $prsubid = array();
                foreach ($similar_products as $items) {
                     $prsubid[]=$items->prsubid;
                }

                $similarproduct_photos = DB::table("ba_product_photos")
                ->whereIn("ba_product_photos.prsubid",$prsubid)
                ->select("ba_product_photos.*")
                ->get();

                foreach ($similar_products as $items) {
                  foreach ($similarproduct_photos as $photos) {
                      if ($items->prsubid==$photos->prsubid) {
                            $items->photos = $photos->image_url;
                            break;
                         }
                    }
                }
           }

           //similar products searching ends here

           $business  = Business::find( $product_info->bin );

           if(!isset( $business))
            {
               return redirect()->back();
            }

            $data = array('product' => $product_info, 'photos'=>$product_photos,'business' =>$business,
                          'similarProducts'=>$similar_products
                          //'cat_menuname'=>$cat_menuname
                      );
            return view("store_front.shopping.product_info")->with( $data );


         }
         catch(Exception $e)
         {
            $detailed_msg = 'Failed to cancel handshake!';
            return redirect("/")->with("err_msg", $detailed_msg );

         }
         catch(\Illuminate\Database\QueryException $ex)
         {
           $detailed_msg ='Failed to cancel handshake!';
           return redirect("/")->with("err_msg", $detailed_msg );
         }
         catch (DecryptException $e)
         {
           $detailed_msg ='Failed to cancel handshake!';
           return redirect("/")->with("err_msg", $detailed_msg );
         }
    }



    protected function browseByProductCategory(Request $request)
    {

        $pr_category = $bizcategory = "";
        //checking main menu session variable
        $menuitemname = $request->menuitemname;
        


        $menu_info  = DB::table("cms_menu_item")
        ->where("item_name", $menuitemname )
        ->first();

        if( !isset($menu_info))
        {
            return redirect("/shopping");
        }

        $category_name  =  $menu_info->product_category_name; 
        $products = DB::table("ba_products")
        ->join("ba_product_variant", "ba_products.id", "=", "ba_product_variant.prid")
        ->where("ba_products.category", $category_name)
        ->select("ba_products.*" , "ba_product_variant.*" )
        ->paginate(50);

        $bins = $products->pluck("bin");
        $businesses = DB::table("ba_business")
        ->where("is_block", "no")
        ->whereIn("id", $bins )
        ->get();

        $bsids = $products->pluck("prsubid");

        $photos = DB::table("ba_product_photos") 
        ->whereIn("prsubid", $bsids )
        ->get(); 
 

        return view("store_front.shopping.products_by_category")
        ->with( array( 'products' => $products , 'photos' => $photos , 'businesses' => $businesses, 'menuitemname' => $menuitemname )  );


    }


    protected function browseByBusinessCategory(Request $request)
    {
        $frno = 0;
        $member_id = $request->session()->has('__member_id_' ) ?  $request->session()->get('__member_id_' ) : -1;
        $cust_info =  CustomerProfileModel::find( $member_id) ;
        if( isset($cust_info))
        {

            $pin_code = $cust_info->pin_code;

            $franchise= DB::table('ba_franchise')
            ->whereIn('zone', function ($zone) use ( $pin_code ) {
                $zone->select("zone_name")
                ->from("ba_franchise_areas")
                ->where("pin", $pin_code);
            })
            ->select("frno")
            ->first();

            $frno = isset($franchise) ? $franchise->frno : 0;

        }
        
        $bizcategory = $request->bizcategory;
        $businesses = DB::table("ba_business")
        ->where("category", $bizcategory)
        ->where("is_block", "no")
        ->where("frno",  $frno )
        ->orderBy("is_verified", "asc" )
        ->orderBy("name", "asc" )
        ->paginate(20);
        $bins = $businesses->pluck("id");

        return view("store_front.shopping.business_listing")
        ->with( array( 'businesses' => $businesses ,  'bizcategory' => $bizcategory )  ); 
    }
 
    protected function shopByCategoryMenuList(Request $request)
    {
        $pr_category = $bizcategory = "";
        //checking main menu session variable
        $mainmenu = $request->mainmenu;

        $menu_name_parts = explode("-", $mainmenu );
        $menu_name  = count($menu_name_parts) > 0 ? implode(" ", $menu_name_parts) : $mainmenu;

        foreach( $request->session()->get('cms_main_menu') as $item)
        {
            if( strcasecmp( $item->menu_name, $menu_name) == 0)
            {
                $bizcategory = $item->biz_category;
                break;
            }
        }

        //checking main menu session variable
        $submenu = $request->submenu;


        $where_clause_product_category = " ba_products.id > 0 ";
        foreach($request->session()->get('cms_sub_menu') as $item)
        {
            if( strcasecmp($item->sub_menu_name, $submenu) == 0)
            {
                $pr_category = $item->product_category;
                $where_clause_product_category = "ba_products.category='$pr_category'";
                break;
            }
        }

        if($bizcategory == ""    )
        {
            return redirect("/");
        }



            $bin =  0;
            $where_clause = "";
            $product_categories = array();
            if($request->bin != "all")
            {
                $landing_categories  = DB::table("cms_landing_biz_category")
                ->where("biz_category", $bizcategory )
                ->select("product_category")
                ->get();


                if(isset($landing_categories))
                {
                    foreach( $landing_categories as $item)
                    {
                        $product_categories[] = $item->product_category;
                    }
                }

            }

            $businesses  = DB::table("ba_business")
            ->where("category", $bizcategory )
            ->where("is_verified", "yes")
            ->where("is_block", "no")
            ->where("is_open", "open")
            ->get();



            $product_categories_for_menu  = DB::table("ba_product_category")
            ->where("business_category", $bizcategory )
            ->get();



            if( count($product_categories ) > 0 )
            {

                $price_range = DB::table("ba_products")
                ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
                ->where("ba_business.is_block",   "no"  )
                ->where("ba_business.is_open",   "open"  )
                ->where("ba_business.category", $bizcategory )
                ->whereIn("ba_products.category", $product_categories  )
                ->whereRaw( $where_clause_product_category )
                // ->select( DB::Raw("min(actual_price) as minimum"), DB::Raw("max(actual_price) as maximum"), DB::Raw("count(*) as totalProducts")  )
                ->select(DB::Raw("count(*) as totalProducts"))
                ->first();

                $biz_categorylist = DB::table("ba_business")
                ->where("ba_business.category", $bizcategory )
                ->where("ba_business.is_block",   "no"  )
                ->where("ba_business.is_open",   "open"  )
                ->get();

                $bin_id = array();

                foreach ($biz_categorylist as $items) {
                    $bin_id[] = $items->id;
                }

                $products = DB::table("ba_products")
                ->join("ba_product_variant", "ba_products.id","=","ba_product_variant.prid")
                ->whereRaw($where_clause_product_category )
                ->whereIn("bin",$bin_id)
                ->select("ba_products.*","ba_product_variant.*",DB::Raw("'' photos"))
                ->paginate(30);

                $prsubid = array();
                foreach ($products as $items) {
                     $prsubid[]=$items->prsubid;
                }

                $product_photos = DB::table("ba_product_photos")
                ->whereIn("ba_product_photos.prsubid",$prsubid)
                ->select("ba_product_photos.*")
                ->get();

                //dd($prsubid);

                foreach ($products as $items) {
                  foreach ($product_photos as $photos) {
                      if ($items->prsubid==$photos->prsubid) {
                            $items->photos = $photos->image_url;
                            break;
                         }
                    }
                }
            }
            else
            {
                $price_range = DB::table("ba_products")
                ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
                ->where("ba_business.is_block",   "no"  )
                ->where("ba_business.is_open",   "open"  )
                ->where("ba_business.category", $bizcategory )
                ->whereRaw( $where_clause_product_category )
                // ->select( DB::Raw("min(actual_price) as minimum"), DB::Raw("max(actual_price) as maximum"), DB::Raw("count(*) as totalProducts")  )
                ->select(DB::Raw("count(*) as totalProducts"))
                ->first();

                $biz_categorylist = DB::table("ba_business")
                ->where("ba_business.category", $bizcategory )
                ->where("ba_business.is_block",   "no"  )
                ->where("ba_business.is_open",   "open"  )
                ->get();

                $bin_id = array();

                foreach ($biz_categorylist as $items) {
                    $bin_id[] = $items->id;
                }

                $products = DB::table("ba_products")
                ->join("ba_product_variant", "ba_products.id","=","ba_product_variant.prid")
                ->whereRaw($where_clause_product_category )
                ->whereIn("bin",$bin_id)
                ->select("ba_products.*","ba_product_variant.*",DB::Raw("'' photos"))
                ->paginate(30);

                $prsubid = array();
                foreach ($products as $items) {
                     $prsubid[]=$items->prsubid;
                }

                $product_photos = DB::table("ba_product_photos")
                ->whereIn("ba_product_photos.prsubid",$prsubid)
                ->select("ba_product_photos.*")
                ->get();

                //dd($prsubid);

                foreach ($products as $items) {
                  foreach ($product_photos as $photos) {
                      if ($items->prsubid==$photos->prsubid) {
                            $items->photos = $photos->image_url;
                            break;
                         }
                    }
                }
            }



        $side_menu  = DB::table("cms_side_menu")
        ->where("category", $bizcategory )
        ->first();

        $product_categories  = array();
        if( isset( $side_menu  ) )
        {
            $product_categories  = DB::table("cms_side_sub_menu")
            ->where("sub_menu_name", $bizcategory )
            ->get();
        }

        $data = array('sidebar_menu' => $side_menu,  'sidebar_sub_menu' => $product_categories,
	    'bizcategory' => $bizcategory, 'businesses' =>$businesses ,
	    'price_range' => $price_range , 'products' =>  $products,
            'product_categories_for_menu' => $product_categories_for_menu);
        return view("store_front.products.products_by_menu_list")->with(  $data );

    }


    protected function betaInnerTest(Request $request)
    {

        if($request->bin != ""  )
        {
            $bin =   $request->bin ;
            $where_clause = "";


            $business  = DB::table("ba_business")
            ->where("id", $bin )
            ->orWhere("merchant_code", $bin )
            ->first();

            if( !isset( $business))
            {
                return redirect()->back();
            }

            $price_range = DB::table("ba_products")
            ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
            ->where("ba_business.id",  $business ->id  )
            // ->select( DB::Raw("min(actual_price) as minimum"), DB::Raw("max(actual_price) as maximum"), DB::Raw("count(*) as totalProducts")  )
            ->select(DB::Raw("count(*) as totalProducts"))
            ->first();

            $products = DB::table("ba_products")
            ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
            ->where("ba_business.id",  $business ->id )
            ->select("ba_products.*")
            ->paginate(20);



            $product_categories = DB::table("ba_products")
            ->where("bin", $business ->id  )
            ->select("category")
            ->distinct()
            ->get();


        }
        else
        {
            return redirect("/");
        }

        $data = array(   'business' =>$business , 'price_range' => $price_range , 'products' =>  $products , 'product_categories' => $product_categories );
        return view("store_front.inner_beta")->with(  $data );

    }


    protected function shopByBusiness(Request $request)
    {
        if( $request->merchant_code == "") 
        {
            return redirect("/");
        }

        $merchant_code =  $request->merchant_code ;
        $list = explode("-", $merchant_code );

        if( count($list ) != 2)
        {
            return redirect("/")->with("err_msg", "No business selected!");
        }
        $bin = $list[1];

        $business  = Business::where("id", $bin)
        ->where("is_block","no")
        ->first();

        if( !isset( $business))
        {
            return redirect()->back();
        }

        $biz_url = URL::to('/business/btm-' . $bin);
        if( $business->qrcode_url == null)
        {
            $qr_url =   config('app.app_cdn') . "/assets/member/qr/" . $merchant_code . ".png" ;
            $qr_path =  config('app.app_cdn_path') . "/assets/member/qr/" . $merchant_code . ".png" ;
            QrCode::format('png')->size(500)->generate( $biz_url , $qr_path );
            $business->qrcode_url = $qr_url;
            $business->save();
        }




        $where_clause = "";
        if( $request->sf  != "" )
        {
            $new_filter = (  count($request->sf) == 0 ) ? array() : $request->sf;
            $keywords = array_unique(array_filter(  $new_filter   )) ;
        }
        else
        {
            $new_filter =  array();
            $keywords = array();
        }



        if(strcasecmp($business->main_module, "shopping") == 0)
        {

            $biz_category = $business->category;

            $main_business_category= DB::table("ba_business_category")
            ->where("name",$biz_category)
            ->first();
            $maincategoryMenu = $main_business_category->main_type;
            
            if(count($keywords) > 0)
            {
                $price_range = DB::table("ba_products")
                ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
                ->where("ba_business.id",  $business ->id  )
                ->whereIn("ba_products.category", $keywords)
                ->select(DB::Raw("count(*) as totalProducts"))
                ->first();

                $products = DB::table("ba_products")
                ->join("ba_product_variant", "ba_products.id","=","ba_product_variant.prid")
                ->where("bin",$business ->id)
                ->whereIn("ba_products.category",$keywords)
                ->select("ba_products.*","ba_product_variant.*",DB::Raw("'' photos"))
                ->paginate(20);

                $prsubid = array();
                foreach ($products as $items)
                {
                    $prsubid[]=$items->prsubid;
                }

                $product_photos = DB::table("ba_product_photos")
                ->whereIn("ba_product_photos.prsubid",$prsubid)
                ->select("ba_product_photos.*")
                ->get();


                foreach ($products as $items)
                {
                    foreach ($product_photos as $photos)
                    {
                        if ($items->prsubid==$photos->prsubid)
                        {
                            $items->photos = $photos->image_url;
                            break;
                        }
                    }
                }
            }
            else
            {
                $price_range = DB::table("ba_products")
                ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
                ->where("ba_business.id",  $business ->id  )
                ->select(DB::Raw("count(*) as totalProducts"))
                ->first();

                $products = DB::table("ba_products")
                ->where("bin",$business->id)
                ->select("ba_products.*",
                  DB::Raw("'' photos"),
                  DB::Raw("'' actual_price"),
                  DB::Raw("'' unit_price"),
                  DB::Raw("'' stock_inhand"),
                  DB::Raw("'' food_type"),
                  DB::Raw("0 prsubid"),
                  DB::Raw("'no' allow_enquiry") )
                ->paginate(20);

                $prid= array();
                foreach ($products as $items)
                {
                    $prid[]=$items->id;
                }

               $variant = DB::table("ba_product_variant")
               ->whereIn("ba_product_variant.prid",$prid)
               ->select("ba_product_variant.*")
               ->get();

               foreach ($variant as $items)
               {
                $prsubid[] = $items->prsubid;
               }

               $product_photos = DB::table("ba_product_photos")
               ->whereIn("ba_product_photos.prsubid",$prsubid)
               ->select("ba_product_photos.*")
               ->get();

               foreach ($products as $items)
               {
                    foreach ($variant as $list)
                    {
                        if ($items->id==$list->prid)
                        {
                          $items->actual_price = $list->actual_price;
                          $items->unit_price = $list->unit_price;
                          $items->stock_inhand = $list->stock_inhand;
                          $items->prsubid = $list->prsubid;
                          $items->food_type = $list->food_type;
                          $items->allow_enquiry = $list->allow_enquiry;
                          break;
                        }
                    }
                    foreach ($product_photos as $listitems)
                    {
                        if ($items->prsubid==$listitems->prsubid)
                        {
                            $items->photos = $listitems->image_url;
                            break;
                        }
                    }
                }
            }

            $product_categories = DB::table("ba_products")
            ->where("bin", $business ->id  )
            ->select("category")
            ->distinct()
            ->get();
         
            $product = DB::table("ba_products")
            ->where("bin",$business ->id)           
            ->get();
              
            $prids = $product->pluck("id");
            $amount_range = DB::select("select min(unit_price) as min_price, max(unit_price) as max_price
                from ba_product_variant 
                where prid in (" . implode(",", $prids->toArray() ) . ")");

           if( isset($amount_range) )
           {
            $min_amount = $amount_range[0]->min_price;

            $max_amount = $amount_range[0]->max_price;
      
           }
           else
           {
            $min_amount = 1;
            $max_amount = 100;
           }

            $data = array(
                        'filters' => $new_filter,
                        'business' => $business ,
                        'html_title' => $business->name,
                        'price_range' => $price_range ,
                        'products' =>  $products ,
                        'maincategoryMenu'=>$maincategoryMenu,
                        'product_categories' => $product_categories,"max_amount"=>$max_amount,"min_amount"=>$min_amount  );
                    return view("store_front.profile")->with(  $data );
        }
        else if(strcasecmp($business->main_module, "booking") == 0)
        {
            $business = DB::table('ba_business')
            ->where("id",$bin)
            ->first();

            $product = DB::table('ba_service_products')
            ->where("bin", $bin)
            ->select("ba_service_products.*")
            ->get();

            $booking = DB::table("ba_bookable_category")
            ->select(DB::Raw("DISTINCT(sub_module),main_module,sub_module_name "))
            ->where('main_module',"booking")
            ->get();

            $services = DB::table('ba_service_products')
            ->where('bin',$bin)
            ->get();
            

            $categories = DB::table('ba_service_products')
            ->select(DB::Raw("DISTINCT(service_category)","id"))
            ->where('bin',$bin)
            ->get();


            $service_profiles = DB::table('ba_business_service_profile') 
            ->where('bin', $bin)
            ->get(); 


            $data = array("business"=>$business,
            "services"=>$services,
            'service_profiles' =>$service_profiles,
            "categories"=>$categories,  
            );
            return view("store_front.booking.business_profile")->with($data);

        }
        else if(strcasecmp($business->main_module, "appointment") == 0)
        {

            $packages = DB::table("ba_service_products")
            ->where("bin",  $bin)
            ->whereNotIn('service_category',['OFFER'])
            ->orderBy("service_category","desc")
            ->get();

            $package_specifications = DB::table('ba_business_service_profile') 
            ->where('bin', $bin)
            ->get();

            $all_categories = array("none");
            foreach($packages as $item)
            {
                if( !in_array($item->service_category, $all_categories ) )
                  $all_categories[] =  $item->service_category;
            }

          $package_categories = DB::table("ba_service_category") 
          ->whereIn("category", $all_categories)
          ->get();


          $data = array( "business"=>$business, 
            'packages'=> $packages ,
            'package_categories' => $package_categories, 
            'package_specifications' =>$package_specifications);
          return view("store_front.appointment.prepare_new_appointment")->with($data); 


        }
                



}



    protected function browserMenu(Request $request)
    {
        //checking main menu session variable
        if($request->mcode != ""  )
        {
            $merchant_code = $request->mcode ;
            $where_clause = "";

            $business  = DB::table("ba_business")
            ->where("merchant_code", $merchant_code )
            ->first();

            if( !isset( $business))
            {
                return redirect()->back();
            }

            $menu_images = DB::table("ba_business_menu")
            ->where("bin",  $business->id  )
            ->orderBy("page_no", "asc")
            ->get();

        }
        else
        {
            return redirect("/");
        }


        $data = array(   'business' =>$business , 'menu_images' => $menu_images  );
        return view("store_front.menu_preview")->with(  $data );

    }


     protected function shopFeaturedProducts(Request $request)
    {

        $products = DB::table("ba_products")
        ->join("ba_product_promotions", "ba_product_promotions.pr_code", "=", "ba_products.pr_code")
        ->whereIn("ba_products.bin", function($query) {
          $query->select("id")
          ->from("ba_business")
          ->where("is_block", "no" )
          ->where("is_open", "open");
        })
        ->select("ba_products.*",
                 DB::Raw(" '' photos"),
                 DB::Raw(" '0' stock_inhand"),
                 DB::Raw(" '0' actual_price"),
                 DB::Raw(" '0' unit_price"),
                 DB::Raw(" '0' prsubid")
                )
        ->paginate(20);

        $bins = array();
        $categories = array();
        $prid = $prsubid = array();

        foreach($products as $product){
            $bins[] = $product->bin;
            $categories[] = $product->category;
            $prid[] = $product->id;
        }

        $bins[] = 0;

        $product_details = DB::table("ba_product_variant")
        ->whereIn("ba_product_variant.prid",$prid)
        ->get();

        foreach ($product_details as $list) {
                $prsubid[] = $list->prsubid;
        }

        $product_photos = DB::table("ba_product_photos")
        ->whereIn("ba_product_photos.prsubid",$prsubid)
        ->get();

        foreach ($products as $items) {
                foreach ($product_details as $variant) {
                      if ($items->id==$variant->prid) {
                            $items->stock_inhand = $variant->stock_inhand;
                            $items->actual_price = $variant->actual_price;
                            $items->unit_price = $variant->unit_price;
                            $items->prsubid = $variant->prsubid;
                            break;
                         }
                }

                foreach ($product_photos as $photos) {
                      if ($items->prsubid== $photos->prsubid) {
                             $items->photos=$photos->image_url;
                             break;
                         }
                }
        }

        $businesses  = DB::table("ba_business")
        ->whereIn("id", $bins )
        ->get();

        $data = array( 'businesses' =>$businesses ,  'products' =>  $products , 'categories' =>$categories );
        return view("store_front.products.featured_products")->with(  $data );

    }



    protected function shopByCategoryUnderBusiness(Request $request)
    {

        //checking main menu session variable
        if($request->merchant_code == "" || $request->category== ""  )
        {

            return redirect('/');

        }


        $business  = DB::table("ba_business")
        ->where("merchant_code", $request->merchant_code  )
        ->first();

        $category_hyphenated = $request->category;
        $parts = explode("-", $category_hyphenated);
        $category = implode(" ", $parts);

        $bin =   $business->id  ;

        $price_range = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
        ->where("ba_business.is_block",   "no"  )
        ->where("ba_business.id",  $bin )
        // ->select( DB::Raw("min(actual_price) as minimum"), DB::Raw("max(actual_price) as maximum"), DB::Raw("count(*) as totalProducts")  )
        ->select(DB::Raw("count(*) as totalProducts"))
        ->first();


        $products = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
        ->where("ba_business.is_block",   "no"  )
        ->where("ba_business.is_open",   "open"  )
        ->where("ba_business.id",  $bin )
        ->where("ba_products.category",  $category )
        ->select("ba_products.*")
        ->paginate(20);


        $product_categories = DB::table("ba_products")
        ->where("bin", $bin )
        ->select("category")
        ->distinct()
        ->get();


        $data = array(  'business' =>$business , 'price_range' => $price_range , 'products' =>  $products , 'product_categories' => $product_categories );
        return view("store_front.profile")->with(  $data );

    }




    protected function searchProducts(Request $request)
    {

        //checking main menu session variable
        if($request->keyword == ""   )
        {
            return redirect('/');

        }

        $keyword = $request->keyword;
        $category = $request->key_category;

        session()->put("category______",$category);

        if( strcasecmp($category, "shopping") ==  0 )
        {
            $products = DB::table("ba_products")
            ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
            ->where("ba_business.is_block", "no")
            ->where("ba_business.is_open", "open")

            ->whereRaw(" ( ( ba_products.pr_name like '%$keyword%') or
                ( ba_products.description like '%$keyword%' ) )" )
            ->select("ba_products.*",
                      DB::Raw("'' photos"),
                      DB::Raw("0 actual_price"),
                      DB::Raw("0 stock_inhand"),
                      DB::Raw("0 unit_price"),
                      DB::Raw("'' allow_enquiry"),
                      DB::Raw("0 prsubid")
                    )
            ->paginate(30);

            $prid = array();
            foreach ($products as $items) {
                 $prid[] = $items->id;
            }

            $product_variant = DB::table("ba_product_variant")
            ->join("ba_product_photos","ba_product_variant.prid","=","ba_product_photos.prid")
            ->select("ba_product_variant.*","ba_product_photos.*")
            ->whereIn("ba_product_photos.prid",$prid)
            ->get();


           foreach ($products as $items) {
                  foreach ($product_variant as $photos) {
                      if  ($items->id==$photos->prid) {
                            $items->photos = $photos->image_url;
                            $items->actual_price = $photos->actual_price;
                            $items->unit_price = $photos->unit_price;
                            $items->stock_inhand = $photos->stock_inhand;
                            $items->prsubid = $photos->prsubid;
                            $items->allow_enquiry = $photos->allow_enquiry;
                            break;
                         }
                    }
                }

        }
        else
        {


            $serviceProducts  = DB::table("ba_service_products")
            ->where(function($query) use ($keyword){

                $query->where("ba_service_products.srv_name",  "like",  "%$keyword%");
                $query->orWhere("ba_service_products.srv_details", "like",  "%$keyword%" ) ;

            })
            ->select(DB::Raw("distinct(ba_service_products.bin) as bin"))
            ->get();

            $bin = array();

            foreach ($serviceProducts as $items) {
                $bin[] = $items->bin;
            }

            $products = DB::table("ba_business")
            ->where("is_open","open")
            ->where("is_block","no")
            ->whereIn("id",$bin)
            ->paginate(30);

        }



        $price_range = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin")
        ->where("ba_business.is_block",   "no"  )
        ->where("ba_business.is_open", "open")
        ->where("ba_products.pr_name", "like",  "%$keyword%" )
        ->orWhere("ba_products.description", "like",  "%$keyword%" )
        // ->select( DB::Raw("min(actual_price) as minimum"),
        //           DB::Raw("max(actual_price) as maximum"),
        //           DB::Raw("count(*) as totalProducts")  )
        ->select(DB::Raw("count(*) as totalProducts"))
        ->first();

        /*

        $businesses  = DB::table("ba_business")
        ->where("category", $bizcategory )
        ->where("is_block", "no")
        ->get();

        */

        $data = array('price_range' => $price_range , 'products' =>  $products,'category'=>$category);
        return view("store_front.products.search_result")->with(  $data );
    }




    protected function orderCheckout(Request $request)
    {

        if ( !Auth::user()) 
        {
            return view("store_front.orders.shopping_cart_list");
        }

        

        $gsid = $request->session()->get('shopping_session'   );
        $uid = $request->session()->get('__user_id_'   );

        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
            return redirect("/shopping/login");
        }

        if($gsid != "")
        {
            //scan and update all product with matching member id
            DB::table("ba_temp_cart")
            ->where("session_id", $gsid  )
            ->orWhere("member_id", $member->id )
            ->update( ['member_id' =>  $member->id , 'session_id' => $gsid   ] ) ;

        }

        $cartitems  = DB::table("ba_temp_cart")
        ->join("ba_products", "ba_products.id", "=", "ba_temp_cart.prid")
        ->where("member_id",  $member->id )
        ->select( "ba_products.*", "ba_temp_cart.id as key",
            "ba_temp_cart.session_id", "ba_temp_cart.member_id",
            "ba_temp_cart.bin" , "ba_temp_cart.prid", 
            "ba_temp_cart.subid" , "ba_temp_cart.qty as pqty",
            DB::Raw("'' photos"),
            DB::Raw(" 0 packaging"),
            DB::Raw(" 0 actual_price"),
            DB::Raw(" 0 stock_inhand"),
            DB::Raw(" 0 cgst"),
            DB::Raw(" 0 sgst"),
            DB::Raw(" 0 gst_pc") )
        ->get();

        $cdn_url =   config('app.app_cdn') ;
        $cdn_path =  config('app.app_cdn_path');
        $cms_base_url = config('app.app_cdn')  ;
        $cms_base_path = $cdn_path  ;

        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";

        $bins = $prsubid = array();
        $bins[] = 0;

        // if cart items is found scan the product variant and photos
        if (!$cartitems->isEmpty()) {
            foreach( $cartitems as $cartitem)
            {
                $bins[] = $cartitem->bin;
                $prid[] = $cartitem->prid;
                $prsubid[] = $cartitem->subid;
            }

            $product_variants = DB::table("ba_product_variant")
            ->join("ba_product_photos","ba_product_variant.prsubid","=","ba_product_photos.prsubid")
            ->whereIn("ba_product_variant.prsubid", $prsubid)
            ->get();

            foreach ($cartitems as $cartitem) {
                foreach ($product_variants as $product_variant) {
                     if ($cartitem->subid == $product_variant->prsubid) {
                          $cartitem->photos = $product_variant->image_url;
                          $cartitem->packaging = $product_variant->packaging;
                          $cartitem->actual_price = $product_variant->actual_price;
                          $cartitem->stock_inhand = $product_variant->stock_inhand;
                          $cartitem->cgst = $product_variant->cgst_pc;
                          $cartitem->sgst = $product_variant->sgst_pc;
                          $cartitem->gst_pc = $product_variant->gst_pc;
                          break;
                     }
                }
            }
        }
        //product photos scan ends here

        $bins= array_filter($bins);
        $businesses  = DB::table("ba_business")
        ->whereIn("id", $bins )
        ->get();

        $configs  = DB::table("ba_global_settings")
        ->get();

        $data = array(  'businesses' =>$businesses , 'cart' => $cartitems , 'configs' =>$configs  );
        return view("store_front.orders.shopping_cart_list")->with(  $data );

    }


    protected function  cartSelectionForOrder(Request $request)
    {
        if($request->mid  == "" || $request->bin  == ""   )
        {
          $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
          return redirect("/shopping/checkout");
        }

        $memid = $request->mid;
        $bin  = $request->bin;

        if(isset( $request->btnorder ))
        {

            $member = DB::table("ba_profile")
            ->where("id",  $memid  )
            ->select("*")
            ->first();

            if(!isset($member ))
            {
                return redirect("/")->with("err_msg", "Please login to place an order!");
            }

            //reset
            DB::table("ba_temp_cart")
            ->where("member_id",  $memid )
            ->update( ['add_to_order' =>  'no' ] ) ;

            //update
            DB::table("ba_temp_cart")
            ->where("member_id",  $memid )
            ->where("bin",  $bin )
            ->update( ['add_to_order' =>  'yes' ] ) ;

            return redirect("/shopping/checkout/payment-delivery-address") ;
        }

        return redirect("/shopping/checkout") ;
    }



    protected function  placeOrder(Request $request)
    {
    
        //reading payment gateway mode
        $gateway_mode =  config('app.gateway_mode'); 
        //reading razorpay secret key info
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );

        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to place an order!");
        }

        $uid = $request->session()->get('__user_id_' );
        $bin = $request->bin;

        $biz = Business::find($bin);
        if (!$biz) {
             return redirect("/shopping/checkout/payment-delivery-address");
        }
        //customer as member
        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
            return redirect("/shopping/checkout/payment-delivery-address");
        }

        //cart details
        $cart  = DB::table("ba_temp_cart")
        ->join("ba_products", "ba_products.id", "=", "ba_temp_cart.prid")
        ->where("member_id",  $member->id)
        ->where("ba_temp_cart.bin",$bin)
        ->select( "ba_products.*",
            "ba_temp_cart.id as key",
            "ba_temp_cart.session_id",
            "ba_temp_cart.member_id" , "ba_temp_cart.bin" ,
            "ba_temp_cart.prid", "ba_temp_cart.subid" , "ba_temp_cart.qty as pqty",
            DB::Raw("'' photos"),
            DB::Raw("'' unit_name"),
            DB::Raw(" 0 packaging"),
            DB::Raw(" 0 actual_price"),
            DB::Raw(" 0 stock_inhand"),
            DB::Raw(" 0 sgst"),
            DB::Raw(" 0 cgst")
             )
        ->get();

        $cdn_url =   config('app.app_cdn') ;
        $cdn_path =  config('app.app_cdn_path');
        $cms_base_url = config('app.app_cdn')  ;
        $cms_base_path = $cdn_path  ;

        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";

        $bins = $prsubid = array();
        $bins[] = 0;

        // if cart items is found scan the product variant and photos
        if (!$cart->isEmpty()) {
            foreach( $cart as $item)
            {
                $bins[] = $item->bin;
                $prid[] = $item->prid;
                $prsubid[] = $item->subid;
            }

            $product_variants = DB::table("ba_product_variant")
            ->join("ba_product_photos","ba_product_variant.prsubid","=","ba_product_photos.prsubid")
            ->whereIn("ba_product_variant.prid",$prid)
            ->get();

            foreach ($cart as $cartitems) {
                foreach ($product_variants as $product_variant) {
                     if ($cartitems->subid == $product_variant->prsubid) {
                          $cartitems->photos = $product_variant->image_url;
                          $cartitems->packaging = $product_variant->packaging;
                          $cartitems->actual_price = $product_variant->actual_price;
                          $cartitems->stock_inhand = $product_variant->stock_inhand;
                          $cartitems->unit_name = $product_variant->unit_name;
                          $cartitems->sgst = $product_variant->sgst_pc;
                          $cartitems->cgst = $product_variant->cgst_pc;
                          break;
                     }
                }
            }
        }

        if( !isset($cart ))
        {
            return redirect("/shopping/checkout/payment-delivery-address");
        }
        $member_id = $cart[0]->member_id; //seller member id
        $bin = $cart[0]->bin ; //seller member id

        //business information
        $business   = DB::table("ba_business")
        ->where("id",  $bin )
        ->first();

        if( !isset($business ))
        {
            return redirect("/shopping/checkout/payment-delivery-address");
        }

        //config information
        $configs   = DB::table("ba_global_settings")
        ->get();

        if( !isset($configs ))
        {
            $delivery_charge = 100;
        }

        $normal_delivery = 80;
        $food_delivery = 50;
        $grocery_delivery = 100.00;
        $cake_delivery =  100.00;
        $delivery_charge = 0.00;

        foreach($configs as $config)
        {
            switch( $config->config_key )
            {
                case "delivery_charge":
                    $normal_delivery =  $config->config_value  ;
                    break;
                case "food_delivery_charge":
                    $food_delivery =  $config->config_value  ;
                    break;
                case "grocery_delivery_charge":
                    $grocery_delivery =  $config->config_value  ;
                    break;
                case "cake_delivery_charge":
                    $cake_delivery =  $config->config_value  ;
                    break;
            }
        }

        if( strcasecmp($business->category , "RESTAURANT") == 0)
            $delivery_charge = $food_delivery;
        elseif( strcasecmp($business->category  ,"CAKES & BAKERY") == 0)
            $delivery_charge = $cake_delivery;
        elseif( strcasecmp($business->category  , "VARIETY STORE") == 0)
            $delivery_charge = $grocery_delivery;
        elseif( strcasecmp($business->category  ,"GROCERY") == 0)
            $delivery_charge = $grocery_delivery;
        else
            $delivery_charge = $normal_delivery;

        if($request->addressSelected > 0)
        {
            $address_info   = DB::table("ba_addresses")
            ->where("id", $request->addressSelected  )
            ->first();

            if( !isset($address_info ))
            {
                return redirect("/shopping/checkout/payment-delivery-address");
            }

            $address = $address_info->address ;
            $landmark = $address_info->landmark ;
            $city= $address_info->city ;
            $state= $address_info->state ;
            $pinCode= $address_info->pin_code ;

        }
        else
        {
            $address = $member->locality ;
            $landmark = $member->landmark ;
            $city= $member->city ;
            $state= $member->state ;
            $pinCode= $member->pin_code ;

        }

        if(isset( $request->btnorder ))
        {
            if (  $request->btnorder == "online") {
                $book_status = "pending-payment";
                $paymentType = "online";
            }else{
                $book_status = "new";
                $paymentType = "cash";
            }

            //total cost
            $total_payable  = $tax = $gst = $cgst = $sgst = $total_packaging_cost = $total_to_pay = 0;
            $taxes = 2;
            foreach($cart as $item)
            {
                //$tax += $taxes*$taxable_amount / 100 ;
                $total_packaging_cost += $item->packaging * $item->pqty ;
                $total_payable += ( $item->actual_price * $item->pqty ) + ( $item->packaging * $item->pqty );
                $gst += ( ( $item->actual_price + $item->packaging) * ( $item->cgst + $item->sgst )  * $item->pqty ) / 100;
	        }

            //save sequencer
            $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
            $rp_1 = mt_rand(0, 9);
            $rp_2 = mt_rand(0, 9);
            $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

            $sequencer = new OrderSequencerModel;
            $sequencer->type = "normal";
            $sequencer->tracker_session =  $tracker;
            $save = $sequencer->save();

            if( !isset($save) )
            {
                $data= ["message" => "failure", "status_code" =>  997 , 'detailed_msg' => 'Failed to generate order no sequence.'  ];
                return json_encode($data);
            }

            $total_to_pay = $total_payable+$cgst+$sgst + $delivery_charge;
            $new_order = new ServiceBooking ;
            $new_order->id = $sequencer->id ;
            $new_order->bin = $cart[0]->bin;
            $new_order->book_category = $business->category;
            $new_order->book_by = $member_id ;
            $new_order->staff_id = 0;
            $new_order->otp  =  $new_order->delivery_otp  = mt_rand(222222, 999999);
            $new_order->customer_name = $member->fullname;
            $new_order->customer_phone = $member->phone;
            $new_order->address = $address;
            $new_order->landmark = $landmark;
            $new_order->city = $city;
            $new_order->state = $state;
            $new_order->pin_code  = $pinCode;
            $new_order->book_status = $book_status;
            $new_order->item_total =  $total_payable ;
            $new_order->seller_payable =  $total_payable ;
            $new_order->delivery_charge =  $delivery_charge ;
            $new_order->total_cost =  $total_payable ;
            $new_order->agent_payable  =  ( 0.6  *  $delivery_charge  ) ;
            $new_order->service_fee  =   $delivery_charge  - $new_order->agent_payable ;
            $new_order->gst =  $gst;
            $new_order->cashback =  0;
            $new_order->discount =  0 ;
 	        $new_order->payment_type =  $paymentType;
	        $new_order->latitude = 0;
            $new_order->longitude =  0;
            $new_order->book_date =  date('Y-m-d H:i:s');
            if( date("H")  <  15 )
            {
                $new_order->service_date = date('Y-m-d'); //delivery on specified date
            }
            else
            {
                $new_order->service_date = date("Y-m-d", time() + 86400) ;  //delivery next day
            }

            $new_order->cust_remarks =  ($request->orderRemarks != "" ? $request->orderRemarks : null);
            $new_order->transaction_no =  ($request->transactionId  != "" ? $request->transactionId : "na");
            $new_order->reference_no =  ($request->referenceNo  != "" ? $request->referenceNo : "na");
            $save=$new_order->save();

            $item_notes = array();

            if($save)
            {
                $new_order = ServiceBooking::find(  $sequencer->id ); //auto increment has removed so need this line to fetch order.
                $orderStatus = new OrderStatusLogModel;
                $orderStatus->order_no = $sequencer->id;
                $orderStatus->order_status = $new_order->book_status;
                $orderStatus->log_date = date('Y-m-d H:i:s');
                $orderStatus->save();

                foreach($cart as $item)
                {
                    //ading items
                    $items = new ShoppingOrderItemsModel;
                    $items->order_no = $new_order->id;
                    $items->pr_code = $item->pr_code  ;
                    $items->pr_name = $item->pr_name;
                    $items->prsubid = $item->subid;
                    $items->description = $item->description;
                    $items->qty  = $item->pqty;
                    $items->price  = $item->actual_price;
                    $items->cgst  = $item->cgst;
                    $items->sgst  = $item->sgst;
                    $items->unit  = $item->unit_name;
                    $items->category_name  = $item->category;
                    $items->package_charge =  $item->packaging;
                    $items->custom_text  = null;
                    $items->save();
                }
                //clear cart
                DB::table("ba_temp_cart")
                ->where("member_id", $member_id )
                ->where("bin",  $bin )
                ->where("add_to_order", "yes")
                ->delete();
            }           
        switch ($request->btnorder) {
            case 'online':

                     $return_url = URL::to('/shopping/payment-completed') ;
                     $notifyUrl = '';
                     $item_notes = array(                    
                      "orderId" => $sequencer->id,
                      "orderAmount" => ( $total_payable + $delivery_charge + $gst ),
                      "customerName" => $member->fullname,
                      "customerPhone" => $member->phone,
                      "customerEmail" => $member->email=="" ? 'booktougi@gmail.com' : $member->email,
                      "returnUrl" => $return_url,
                      "notifyUrl" => $notifyUrl,
                    );

                $rzpapi = new Api(  $secure_key->key_id   , $secure_key->key_secret );
                $rzp_response =  $rzpapi->order->create(array
                    ('receipt' => $sequencer->id ,
                     'amount' => ( $total_payable + $delivery_charge + $gst )  * 100 ,
                     'currency' => 'INR', 
                     'notes'=>  $item_notes                   
                 ));
                    
                 if(isset($rzp_response))
                 {
                    //logging razorpay order ID
                    $new_order->rzp_id = $rzp_response->id;
                    $new_order->save();

                    return redirect("/shopping/payment-in-progress?o=".$sequencer->id ) ;
                    break;
                 }
                 else
                 {
                    //open payment failure page

                 }

                break;
            default:
            return redirect("/shopping/checkout")->with("err_msg", "You order has been  placed!") ;
                break;
            }            
        //switch statement for checking signature generation ends here
            return redirect("/shopping/checkout")->with("err_msg", "You order has been  placed!") ;
        }

        return redirect("/shopping/checkout") ;
    }

    //to open razorpay payment gateway screens
    protected function razorpayPaymentProgress(Request $request)
    {
        $oid= $request->o;

        if($oid == "")
        {
            //redirect to parameter missing page
            return redirect("/shopping/checkout")->with("err_msg", "Your order not found!") ;

        }
        $order_info = DB::table("ba_service_booking") 
        ->where("id", $oid)
        ->first();

        if( !isset( $order_info) )
        {
            //redirect to order not found page 
          return redirect("/shopping/checkout")->with("err_msg", "Your order not found!") ;
        }
        $gateway_mode =  config('app.gateway_mode'); 
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );

        $prefill = array("name" => $order_info->customer_name , "email"=> "booktou@gmail.com", "contact" => $order_info->customer_phone);
         $notes = array("item" => $order_info->book_category, "price"=> $order_info->total_cost);
      
        $data = array(
            "orderno" => $order_info->id,
            "key" => $secure_key->key_id, 
            "amount" => $order_info->total_cost * 100 ,
            "currency" => "INR",
            "name" => $order_info->customer_name, 
            "description" => $order_info->customer_name, 
            "image" => "https://booktou.in/public/store/image/logo.png",
            "order_id" => $order_info->rzp_id ,
            "callback_url" => URL::to("/shopping/payment-completed")  ,
            "prefill" =>  $prefill ,
            "notes" => $notes 
        );

        return view("store_front.razor.checkout_progress")->with( "data", $data);
    }

    protected function razorpayPaymentCompleted(Request $request)
    {
        $rzp_id= $request->rzid;

        if(  $rzp_id == "" )
        {
            return redirect("/shopping/checkout")->with("err_msg", "No matching online order found!") ;
        }


        //use eloquent model to allow saving any changes
        $order_info = ServiceBooking::where("rzp_id", $rzp_id)->first();

        if( !isset( $order_info) )
        {
            return redirect("/shopping/checkout")->with("err_msg", "Your order not found!") ;
        }

        $gateway_mode =  config('app.gateway_mode'); 
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );
        $razor_id =  $order_info->rzp_id;
        $rzp_payment_response = $this->checkRazorPayOrderStatus( $razor_id, $gateway_mode );

 
        $payment_info =  $rzp_payment_response['items'] ;


        if(  isset(  $payment_info ) )
        {
            $payment_status = $rzp_payment_response['items'][0]->status;
            $payment_id = $rzp_payment_response['items'][0]->id;

            if(  strcasecmp( $payment_status , "captured") == 0 )
            {
                $order_info->rzp_pay_id =  $payment_id ;
                $order_info->payment_status  = "paid";
                $save=$order_info->save();
                return redirect("/shopping/checkout")->with("err_msg", "Order payment succeeded!") ;
               
            }
            else
            {
                return redirect("/shopping/checkout")->with("err_msg", "Order payment pending!") ;
            }
        }
            else
            {
                return redirect("/shopping/checkout")->with("err_msg", "Order not found!") ;           
            }
        
    }
 

    protected function orderPaymentAndDeliveryDetails(Request $request)
    {

        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to place an order!");
        }
        $uid = $request->session()->get('__user_id_'   );

        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member))
        {
            return redirect("/join-and-refer");
        }

        // $cart  = DB::table("ba_temp_cart")
        // ->join("ba_products", "ba_products.id", "=", "ba_temp_cart.prid")
        // ->where("member_id",  $member->id   )
        // ->where("add_to_order", "yes")
        // ->select( "ba_products.*", "ba_temp_cart.session_id",
        //     "ba_temp_cart.member_id" , "ba_temp_cart.bin" ,
        //     "ba_temp_cart.prid", "ba_temp_cart.subid" , "ba_temp_cart.qty as pqty" )
        // ->get();


        $cart  = DB::table("ba_temp_cart")
                ->join("ba_products", "ba_products.id", "=", "ba_temp_cart.prid")
                ->where("member_id",  $member->id   )
                ->where("add_to_order", "yes")
                ->select( "ba_products.*",
                    "ba_temp_cart.id as key",
                    "ba_temp_cart.session_id",
                    "ba_temp_cart.member_id" , "ba_temp_cart.bin" ,
                    "ba_temp_cart.prid", "ba_temp_cart.subid" , "ba_temp_cart.qty as pqty",
                    DB::Raw("'' photos"),
                    DB::Raw(" 0 packaging"),
                    DB::Raw(" 0 actual_price"),
                    DB::Raw(" 0 stock_inhand"),
                    DB::Raw(" 0 cgst"),
                    DB::Raw(" 0 sgst"),
                    DB::Raw(" 0 gst_pc")

                     )
                ->get();

        $cdn_url =   config('app.app_cdn') ;
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public
        $cms_base_url = config('app.app_cdn')  ;
        $cms_base_path = $cdn_path  ;

        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";

        $bins = $prsubid = array();
        $bins[] = 0;

        // if cart items is found scan the product variant and photos
        if (!$cart->isEmpty()) {
            foreach( $cart as $item)
            {
                $bins[] = $item->bin;
                $prid[] = $item->prid;
                $prsubid[] = $item->subid;
            }

            $product_variant = DB::table("ba_product_variant")
            ->join("ba_product_photos","ba_product_variant.prsubid","=","ba_product_photos.prsubid")
            ->whereIn("ba_product_variant.prid",$prid)
            ->get();

            foreach ($cart as $cartitems) {
                foreach ($product_variant as $product_items) {
                     if ($cartitems->prid == $product_items->prid) {
                          $cartitems->photos = $product_items->image_url;
                          $cartitems->packaging = $product_items->packaging;
                          $cartitems->actual_price = $product_items->actual_price;
                          $cartitems->stock_inhand = $product_items->stock_inhand;
                          $cartitems->cgst = $product_items->cgst_pc;
                          $cartitems->sgst = $product_items->sgst_pc;
                          $cartitems->gst_pc = $product_items->gst_pc;
                          break;
                     }
                }
            }
        }

        $bin = 0;
        if(count($cart) > 0)
        {
            $bin = $cart[0]->bin;
        }

        $business = Business::find( $bin) ;
        if(!isset($business ))
        {
            return redirect("/shopping/checkout")->with("err_msg", "No business information found.");
        }

        $addresses = DB::table("ba_addresses")
        ->join("ba_member_address", "ba_member_address.address_id", "=", "ba_addresses.id")
        ->where("ba_member_address.member_id", $member->id  )
        ->select("ba_addresses.*")
        ->get();

        $configs  = DB::table("ba_global_settings")
        ->get();

        $data = array(
            'member' =>$member ,
            'cart' => $cart,
            'addresses' =>  $addresses ,
            'business' =>$business,
            'configs' =>$configs  );
        return view("store_front.payment_and_address")->with(  $data );


    }

    protected function removeCart(Request $request)
    {

        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to place an order!");
        }


        if($request->key  == ""    )
        {
          $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
          return redirect("/shopping/checkout");
        }


        $uid = $request->session()->get('__user_id_'   );

        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
            return redirect("/")->with("err_msg", "Please login to place an order!");
        }


        DB::table("ba_temp_cart")
        ->where("member_id",  $member->id   )
        ->where("bin",  $request->key  )
        ->delete();

        return redirect("/shopping/checkout") ;
    }




protected function addDeliveryAddress(Request $request)
    {

        if(isset($request->btnadd) &&  $request->btnadd == "addaddress")
        {

            $uid = $request->session()->get('__user_id_'   );

            $member = DB::table("ba_profile")
            ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
            ->where("ba_users.id",  $uid  )
            ->select("ba_profile.*")
            ->first();


             $address = new AddressModel ;
             $address->address = $request->address;
             $address->landmark = $request->landmark;
             $address->city = $request->city;
             $address->state = $request->state;
             $address->pin_code  = $request->pin;
             $address->address_type =  "secondary";
             $save=$address->save();

           if($save)
           {

            $member_address = new MemberAddressModel;
            $member_address->member_id =$member->id;
            $member_address->address_id =$address->id;
            $member_address->save();

            return back()->with("err_msg", "Address saved successfully!"  );

           }


        }
    }



    protected function viewMyProfile(Request $request)
    {

        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to view your profile!");
        }


        $uid = $request->session()->get('__user_id_'   );


        $customerProfile = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.id as cusProfileId","ba_profile.fullname as cusFullName",
                 "ba_profile.dob as cusDateOfBirth", "ba_profile.locality as cusLocality",
                 "ba_profile.landmark as cusLandmark", "ba_profile.city as cusCity",
                 "ba_profile.state as cusState", "ba_profile.pin_code as cusPin",
                 "ba_profile.phone as cusPhone", "ba_profile.email as cusEmail",
                  "ba_profile.profile_photo as cusImage")
        ->first();

        if(!isset($customerProfile ))
        {
            return redirect("/");
        }


        $addresses = DB::table("ba_addresses")
        ->join("ba_member_address", "ba_member_address.address_id", "=", "ba_addresses.id")
        ->where("ba_member_address.member_id", $customerProfile->cusProfileId  )
        ->select("ba_addresses.*")
        ->get();

        $orders = DB::table("ba_service_booking")
        ->where("book_by",$customerProfile->cusProfileId)
        ->orderBy("id","desc")
        ->first();

       $rate = $ratedata =  $rateAvg = $ratingFiveCount = $ratingFourCount =
        $ratingThreeCount =  $ratingTwoCount =  $ratingOneCount ="0";


      $reviews = DB::table("ba_customer_reviews")
      ->where("customer_id", $customerProfile->cusProfileId)
      ->get();



      foreach($reviews as $item)
      {

        $rate += $item->rating;
        $ratedata = ''.$rate.'';

      }

     // return ($ratedata);

      $count = $reviews->count();
      if($count>1){
          $checkrate = $rate / $count;
          $rateAvg = $checkrate;
          $rateAvgdata = ''.$rateAvg.'';


      }
      else{

           $rateAvgdata = ''.$rate.'';

      }


      $countRating = DB::table("ba_customer_reviews")
      ->where("customer_id", $customerProfile->cusProfileId)
      ->select('rating',DB::raw('count(id)  as
          total_count'))
      ->groupBy("rating")
      ->get();
      foreach ($countRating as $keyitem) {
        if($keyitem->rating ==1){
          $ratingOneCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==2){
          $ratingTwoCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==3){
          $ratingThreeCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==4){
          $ratingFourCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==5){
          $ratingFiveCount= $keyitem->total_count;
        }
        else{

        }
      }


        $data = array('profiles' => $customerProfile, 'addresses' => $addresses,
                      'orders' => $orders, 'reviews' => $reviews,
                      'totalRating' => $ratedata,
                      'ratingAverage'=>$rateAvgdata,
                      'ratingFiveCount' => $ratingFiveCount,
                      'ratingFourCount' => $ratingFourCount,
                      'ratingThreeCount' => $ratingThreeCount,
                      'ratingTwoCount' => $ratingTwoCount,
                      'ratingOneCount' => $ratingOneCount);

     return view("store_front.member.view_customer_profile")->with("data",$data);
    }




  public function showSignupForm(Request $request)
  {

    $referrer = null;
    if(   $request->code  != ""   )
    {
        $referrer  = DB::table('ba_profile')
        ->where('referral_code',  $request->code  )
        ->first();
    }



    $data = array('title' => 'Join bookTou, refer friends and earn commission on every purchase they make', 'referrer' => $referrer );

    return view("store_front.member.join_and_refer")->with("data",$data);
  }

    public function joinAndGenerateReferralLink(Request $request)
    {

        if(   $request->phone == "" || $request->firstname == ""  || $request->lastname == "" || $request->address == "" ||
            $request->landmark == ""  || $request->pin == ""   || $request->state == ""   || $request->city == ""  )
       {

        return redirect("/join-and-refer")->with("err_msg",  "Please fill all the important fields!");
       }

       if(  strlen($request->phone) < 10 )
       {
            return redirect("/join-and-refer")->with("err_msg",  "Phone number should be valid 10 digit number!");
        }

        if(   $request->password !=  $request->confpassword )
       {
            return redirect("/join-and-refer")->with("err_msg",  "Password mismatch!");
        }



        $phone = $request->get('phone');

        $count = DB::table('ba_users')
        ->where('phone',  $phone)
        ->where("category","0")
        ->count();

        if($count > 0)
        {
            return redirect("/join-and-refer")->with("err_msg",  "Seems like you are already a member. Please login instead.");
        }


        $refercode = "";
        $profile = new  CustomerProfileModel;
        $profile->fname =  $request->firstname ;
        $profile->lname = $request->lastname;
        $profile->fullname = $request->firstname  . " " .  $request->lastname;
        $profile->phone = $request->phone;
        $profile->locality = $request->address;
        $profile->landmark  = $request->landmark;
        $profile->city = $request->city;
        $profile->state = $request->state;
        $profile->pin_code  = $request->pin;

        $save  = $profile->save();
        if(  $save  )
        {
            $refercode ='bt' . date('y') . 'm' . $profile->id;
            DB::table("ba_profile")
            ->where('id', $profile->id )
            ->update(['referral_code' => $refercode  ]);

        }



        //generate a confirmation OTP
        $otp = mt_rand(232323,999999); //this is also the new password
        $otpdate =  date('Y-m-d H:i:s');
        $currentDate = strtotime($otpdate);
        $futureDate = $currentDate+(60*5);
        $otp_expiresOn = date("Y-m-d H:i:s", $futureDate);

        $newuser = new User;
        $newuser->profile_id =$profile->id;
        $newuser->bin = 0;
        $newuser->phone = $request->phone;
        $newuser->password =  Hash::make( $request->password )  ;
        $newuser->category =  0 ;
        $newuser->status =  1;
        $newuser->otp =  $otp;
        $newuser->otp_expires =  $otp_expiresOn;
        $save =$newuser->save();


        if(  $save  )
        {
            //alerting the user
            $message = "Thank you for joining bookTou. Please use ".  $otp  . " as password to login. This is automatically generated password. Please update with a new password soon.";
            //$this->sendSmsAlert($phone, $message);
            return redirect("/refer/" .  $refercode );
        }
        else
        {
            return redirect("/join-and-refer")->with("err_msg",  "An error occurred while generating referral code. Please retry.");
        }

  }



    public function viewReferralInformation(Request $request)
    {

        if(   $request->code == ""   )
       {
            return redirect("/join-and-refer")->with("err_msg",  "No referral code provided.");
       }

       $profile  = DB::table('ba_profile')
        ->where('referral_code',  $request->code  )
        ->first();

       if(!isset( $profile ) )
       {
        return redirect("/join-and-refer")->with("err_msg",  "No referral code found.");
       }

       $data = array('title' => 'Your referral code is ready.', 'profile' => $profile );

        return view("store_front.member.referral_card")->with("data",$data);


    }




    protected function  makeOrderEnquiry(Request $request)
    {


        return view("store_front.customer_request_form");
    }


    protected function  booktouAssistWizard(Request $request)
    {


        if($request->stage == 1  || $request->stage  == "")
        {
            if ($request->session()->has('stage1')!="") {

                return redirect('/shopping-enquiry-wizard?stage=2' );

            }
            elseif (isset( $request->name) && isset( $request->phone)  && isset( $request->otp) )
            {
                $stage1 =[$request->stage=>[ 'name'=> $request->name, 'phone'=> $request->phone_no,'otp'=> $request->otp, ] ];
                session()->put('stage1', $stage1 );
                if ($request->session()->has('stage1')) {
                    return redirect('/shopping-enquiry-wizard?stage=2' );
                }else{
                    return view("store_front.assist.assist_confirm_identity");
                     }
                }else{

                    return view("store_front.assist.assist_confirm_identity");
             }
        }
        elseif ($request->stage==2) {

            $field_name = array();
            $field_value = array();


            if($request->orderitemslist!="")
            {
                switch ($request->orderitemslist) {
                    case 'cake':

                    if ($request->orderitemslist=="") {
                         return redirect('/shopping-enquiry-wizard?stage=2' );
                    }

                    if ($request->cake_title=="" && $request->size=="" && $request->cake_flavour=="") {

                        return redirect('/shopping-enquiry-wizard?stage=2' );
                    }


                    if ($request->file('photo')!="") {

                            $imageName =  time() . '.' .$request->photo->getClientOriginalExtension();

                            $url =   'public/webapp/upload/' . $imageName ;

                            $request->photo->move(public_path('webapp/upload'), $imageName);
                     }
                     $item_id="item";
                     array_push($field_name,$item_id,
                            $request->cake_title_id ,$request->cake_weight_id,$request->cake_flavour_id,
                            $request->cake_description_id,$request->cake_photo_id);

                        array_push($field_value,$request->orderitemslist,
                            $request->cake_title ,$request->size,$request->cake_flavour,
                            $request->cake_description,$url);


                        $stage2=array_combine($field_name,$field_value);




                    session()->put('stage2', $stage2 );



                    return redirect('/shopping-enquiry-wizard?stage=3' );

                        break;

                    case 'grocery':

                         if ($request->orderitemslist=="") {
                         return redirect('/shopping-enquiry-wizard?stage=2' );
                        }
                        if ($request->grocery_needs=="") {

                        return redirect('/shopping-enquiry-wizard?stage=2' );
                         }


                         if ($request->file('photo')!="") {

                            $imageName =  time() . '.' .$request->photo->getClientOriginalExtension();

                            $url =   'public/webapp/upload/' . $imageName ;

                            $request->photo->move(public_path('webapp/upload'), $imageName);
                        }
                        if($request->file('photo')=="")
                        {
                             $url ="";
                        }

                        //adding items to array
                        $item_id="item";

                        array_push($field_name,$item_id, $request->grocery_photo_id,$request->grocery_needs_desc);

                        array_push($field_value,$request->orderitemslist,$url,$request->grocery_needs);


                        $stage2=array_combine($field_name,$field_value);

                        //photo upload ends here


                        session()->put('stage2', $stage2 );
                        return redirect('/shopping-enquiry-wizard?stage=3' );

                        break;

                    case 'medicine':
                         if ($request->orderitemslist=="") {
                         return redirect('/shopping-enquiry-wizard?stage=2' );
                        }

                        if ($request->doctor_prescription=="") {

                        return redirect('/shopping-enquiry-wizard?stage=2' );
                         }


                         if ($request->file('photo')!="") {

                            $imageName =  time() . '.' .$request->photo->getClientOriginalExtension();

                            $url =   'public/webapp/upload/' . $imageName ;

                            $request->photo->move(public_path('webapp/upload'), $imageName);
                        }

                        if($request->file('photo')=="")
                        {
                             $url ="";
                        }

                        //adding items to array
                        $item_id="item";

                        array_push($field_name,$item_id, $request->prescription_photo,$request->doctor_pres_desc);

                        array_push($field_value,$request->orderitemslist,$url,$request->doctor_prescription);


                        $stage2=array_combine($field_name,$field_value);

                        //photo upload ends here


                        session()->put('stage2', $stage2 );
                        return redirect('/shopping-enquiry-wizard?stage=3' );

                        break;

                    case 'gifts':
                         if ($request->orderitemslist=="") {
                         return redirect('/shopping-enquiry-wizard?stage=2' );
                        }

                        if ($request->gift_itemtype=="") {

                        return redirect('/shopping-enquiry-wizard?stage=2' );
                         }


                         if ($request->file('photo')!="") {

                            $imageName =  time() . '.' .$request->photo->getClientOriginalExtension();

                            $url =   'public/webapp/upload/' . $imageName ;

                            $request->photo->move(public_path('webapp/upload'), $imageName);
                        }


                        if($request->file('photo')=="")
                        {
                             $url ="";
                        }

                        //adding items to array
                        $item_id="item";

                        array_push($field_name,
                            $item_id,
                            $request->gift_items,
                            $request->gifts_photo_id,
                            $request->gift_items_desc);

                        array_push($field_value,
                            $request->orderitemslist,
                            $request->gift_itemtype,
                            $url,
                            $request->item_description);


                        $stage2=array_combine($field_name,$field_value);


                        //photo upload ends here


                        session()->put('stage2', $stage2 );


                        return redirect('/shopping-enquiry-wizard?stage=3' );

                        break;

                    case 'others':
                        if ($request->orderitemslist=="") {
                         return redirect('/shopping-enquiry-wizard?stage=2' );
                        }

                        if ($request->others_description=="") {

                        return redirect('/shopping-enquiry-wizard?stage=2' );
                         }


                        //adding items to array
                        $item_id="item";

                        array_push($field_name,
                            $item_id,
                            $request->other_description);

                        array_push($field_value,
                            $request->orderitemslist,$request->others_description);


                        $stage2=array_combine($field_name,$field_value);


                        session()->put('stage2', $stage2 );
                        return redirect('/shopping-enquiry-wizard?stage=3' );
                        break;
                }
            }
            return view("store_front.assist.assist_order_type");
        }

    }

    protected function  placeOrderEnquiry(Request $request)
    {

        if( $request->phone == ""  ||  $request->name == ""   ||  $request->otp == "" )
        {
            return redirect('/shopping/make-order-enquiry')->with("err_msg", "Please fill your name and phone number!");
        }


        if(isset( $request->btnsend ))
        {
            $phone = $request->phone;
            $name = $request->name ;
            $otp = $request->otp;
            $enquiry = $request->enquiry ;


            $callback =  CallbackEnquiryModel::where("phone",  $phone)
            ->where("otp", $otp)
            ->whereDate("enquiry_date", date('Y-m-d'))
            ->first();

            if( !isset($callback))
            {
                return redirect('/shopping/make-order-enquiry')->with("err_msg", "Please regenerate OTP and try again!");
            }


            if($callback->is_verified == "yes"  )
            {
                return redirect('/shopping/make-order-enquiry')
                ->with("err_msg", "Your request is in queue. One of our customer care agent will call you back. Please bear with us!");
            }

            $callback->body = $enquiry;
            $callback->name = $name;
            $callback->save();

        }

        return redirect('/shopping/make-order-enquiry')->with("err_msg", "Thank you. Your request is submitted and we will call you back!");
    }

    protected function assistOrderType(Request $request)
    {
        if (isset($request->items)) {

            if ($request->items=="cake") {


                $returnHTML = view('store_front.assist_cake_items')->render();


                return response()->json(array('success'=>true,'html'=>$returnHTML));

            }elseif ($request->items=="grocery") {
                $returnHTML = view('store_front.assist_grocery_items')->render();
                return response()->json(array('success'=>true,'html'=>$returnHTML));

            }elseif ($request->items=="medicine") {
                $returnHTML = view('store_front.assist_medicine_items')->render();
                return response()->json(array('success'=>true,'html'=>$returnHTML));

            }elseif ($request->items=="gifts") {
                $returnHTML = view('store_front.assist_gifts_items')->render();
                return response()->json(array('success'=>true,'html'=>$returnHTML));

            }elseif ($request->items=="others") {
                $returnHTML = view('store_front.assist_other_items')->render();
                return response()->json(array('success'=>true,'html'=>$returnHTML));

            }else{

                return redirect('/shopping/booktou-assist-wizard?stage=2' );
            }

        }else{
            return redirect('/shopping/booktou-assist-wizard?stage=2' );
        }

    }





    protected function  whereIsMyOrder(Request $request)
    {

        $order_info = $agent_info  = $timeline = array();

        $trackcode = $request->trackcode;

        if( $trackcode  != "")
        {
            $order_type_info  = DB::table("ba_order_sequencer")
            ->where("tracker_session", $trackcode )
            ->first();

            if( !isset($order_type_info))
            {
                $order_type_info  = DB::table("ba_order_sequencer")
                ->where("id", $trackcode )
                ->first();
            }


            if( !isset($order_type_info))
            {
                return redirect("/");
            }


            if(isset($order_type_info))
            {

                if( $order_type_info->type == "normal" )
                {
                    $order_info = DB::table("ba_service_booking")
                    ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
                    ->where("ba_service_booking.id", $order_type_info->id )
                    ->select("ba_service_booking.id", "ba_service_booking.bin", "ba_profile.fullname", "ba_service_booking.payment_type",
                        "ba_service_booking.address", "ba_service_booking.book_status",  DB::Raw("'na' business"), DB::Raw("'na' locality") )
                    ->first();


                    if(isset($order_info))
                    {

                        $business_info = DB::table("ba_business")
                        ->where("id", $order_info->bin )
                        ->first();

                        if(isset($business_info))
                        {
                            $order_info->business = $business_info->name ;
                            $order_info->locality = $business_info->locality ;
                        }


                        $agent_info =  DB::table("ba_delivery_order")
                        ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
                        ->where("ba_delivery_order.order_no",  $order_info->id)
                        ->first();
                    }
                }
                else
                {
                    $order_info = DB::table("ba_pick_and_drop_order")
                    ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
                    ->where("ba_pick_and_drop_order.id", $order_type_info->id )
                    ->select("ba_pick_and_drop_order.*", "ba_business.name as business", "ba_business.locality", "ba_business.landmark")
                    ->first();

                    if(isset($order_info))
                    {
                        $agent_info =  DB::table("ba_delivery_order")
                        ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
                        ->where("ba_delivery_order.order_no",  $order_info->id)
                        ->first();
                    }
                }

            }
            else
            {
                //remove this code later
                $order_info = DB::table("ba_pick_and_drop_order")
                ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
                ->where("ba_pick_and_drop_order.id", $trackcode )
                ->select("ba_pick_and_drop_order.*", "ba_business.name as business", "ba_business.locality", "ba_business.landmark")
                ->first();


                if(isset($order_info))
                {
                    $agent_info =  DB::table("ba_delivery_order")
                    ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
                    ->where("ba_delivery_order.order_no",  $order_info->id)
                    ->first();
                }
            }
        }

        $slides = DB::table("ba_slides")
        ->where("status", 1 )
        ->get();

        foreach($slides as $item)
        {
            $item->path =  config('app.app_cdn')  . $item->path;
        }

        $data = array('order_info' => $order_info, 'agent_info' => $agent_info, 'timeline' => $timeline , 'slides' =>$slides, 'htmlTitle' => 'Track Your bookTou Order');
        return view("store_front.order_live_location")->with($data);
    }



     protected function  submitInstantQuote(Request $request)
    {

        if( isset($request->btnsend))
        {
            if( $request->body == "" ||  $request->name == ""   ||  $request->phone == ""   ||  $request->qty == "" ||  $request->unit == "" )
            {
                return redirect('/')->with("err_msg", "Please fill in your request details so that we can process it better!");
            }

            $user_request = new QuoteRequestModel;
            $user_request->request_body = $request->body;
            $user_request->qty = $request->qty;
            $user_request->name = $request->name;
            $user_request->phone = $request->phone;
            $user_request->unit = $request->unit;
            $user_request->request_date = date('Y-m-d H:i:s');
            $user_request->save();

            return redirect('/')->with("err_msg", "Your request is submitted. We will call back soon to update!");
        }
    }



     protected function  subscribeEmailMarketing(Request $request)
    {

        if( isset($request->btnsubscribe))
        {
            if( $request->email == ""   )
            {
                return redirect('/') ;
            }

            $subscribe = new SubscriberModel;
            $subscribe->email = $request->email;
            $subscribe->save();

            return redirect('/') ;
        }
    }


    public function  contactUs (Request $request)
    {
        return view("contact_us") ;
    }



    public function  saveContactForm (Request $request)
    {
        $cform = new ContactFormModel;


        if( $request->name == "" || $request->email == "" || $request->phone == "" || $request->message == ""    )
        {
            return redirect("/contact-us")->with("err_msg", "You have not provided enough details. Please fill your details correctly.");
        }

        if( strlen($request->phone) < 10 &&  strlen($request->phone) > 13  )
        {
            return redirect("/contact-us")->with("err_msg", "Please provided valid phone number.");
        }


        $cform->name = $request->name;
        $cform->email = $request->email;
        $cform->phone = $request->phone;
        $cform->message = $request->message;
        $cform->save();


        return redirect("/contact-us")->with("err_msg", "Your form is submitted. We will contact you soon!");
    }



    protected function bookADoctor(Request $request)
    {
        $professions = DB::table("ba_professionals")
        ->distinct("profession")->get();

        $professionals= DB::table("ba_professionals")
        ->get();






        return view("store_front.booking.book_a_doctor")->with( array('professions' => $professions, 'professionals' => $professionals )) ;

    }


    protected function confirmAndCheckout(Request $request)
    {



        $secretKey = "8173786f158d80f86ffc4b4ec382f022883a9dd9";
        $postData = array(
          "appId" => '92213d92ddd6010b563914a5031229' ,
          "orderId" => $request->orderId,
          "orderAmount" => $request->orderAmount,
          "orderCurrency" => "INR",
          "orderNote" => $request->orderNote,
          "customerName" =>$request->customerName,
          "customerPhone" => $request->customerPhone,
          "customerEmail" => $request->customerEmail,
          "returnUrl" => "https://booktou.in/appointment/checkout-completed",
          "notifyUrl" => "https://booktou.in/appointment/notify-checkout-status",
        );
         // get secret key from your config
         ksort($postData);
         $signatureData = "";
         foreach ($postData as $key => $value){
              $signatureData .= $key.$value;
         }
         $signature = hash_hmac('sha256', $signatureData, $secretKey,true);
         $signature = base64_encode($signature);


        $data = array(
            'category' => $request->category,
            'professional' => $request->professional,
            'orderAmount' => $request->orderAmount,
            'customerName' => $request->customerName,
            'customerEmail' => $request->customerEmail,
            'customerPhone' => $request->customerPhone,
            'orderNote' => $request->orderNote,
            'orderId' => $request->orderId,
            'customerPhone' => $request->customerPhone,
            'signature' => $signature

        );

        return view("store_front.booking.confirm_checkout")->with($data)  ;

    }


    protected function checkoutCompleted(Request $request)
    {


      return view("store_front.booking.checkout_completed");

    }


    protected function donateToCovidFund(Request $request)
    {



    if ($request->profile==2) {

            if ($request->donation_select_type=="donate") {

                if ($request->person_toinclude=="") {
                    $agrees_terms = "no";
                }else{
                    $agrees_terms = $request->person_toinclude;
                }

            $donate =[$request->profile=>[
                                                "name" => $request->person_name,
                                                "phone" =>$request->person_phone,
                                                "address" =>$request->person_address,
                                                "category" =>$request->person_category,
                                                "show_name"=>$agrees_terms ]
                                                ];
            session()->put('donor', $donate);

            return redirect('/shopping/booktou-assist-donation_wizard?profile=3');
            //dd(session()->get('profile'));die();

            }elseif ($request->donation_select_type=="add_beneficiary") {

            $beneficiary =[$request->profile=>[
                                                "name" => $request->person_name,
                                                "phone" =>$request->person_phone,
                                                "address" =>$request->person_address,
                                                "category" =>$request->person_category
                                                ]
                                                ];

            session()->put('donor', $beneficiary);

            return redirect('/shopping/booktou-assist-donation_wizard?profile=5');
            //dd(session()->get('profile'));die();
        }
            return view('store_front.donation.assist_donation_wizard');

    }elseif ($request->profile==3) {

           //dd(session()->get('donor'));die();


        if (session()->has("donor")=="")
        {
            return redirect('/shopping/booktou-assist-donation_wizard?profile=2');
        }


        if ($request->donation_type=="money") {
                $amount=$request->amount;
                $desc="na";
            }elseif ($request->donation_type=="item") {
                $amount=0;
                $desc=$request->item_description;
        }


        if ($request->donateto=="to_a_person") {




            $person =[$request->profile=>[
                                                "name" => $request->doneename,
                                                "phone" =>$request->doneephone,
                                                "address" =>$request->doneeaddress,
                                                "mode" =>$request->donation_type,
                                                "type" =>$request->donateto,
                                                "amount"=>$amount,
                                                "description"=>$desc
                                                ]];
            session()->put('donations', $person);

            return redirect('/shopping/booktou-assist-donation_wizard?profile=4');
            //dd(session()->get('profile'));die();

        }elseif ($request->donateto=="to_donation_fund") {

            $item_or_cash =[$request->profile=>[ "name" => "NA",
                                                "phone" =>"NA",
                                                "address" =>"NA",
                                                "mode" =>$request->donation_type,
                                                "type" =>$request->donateto,
                                                "amount"=>$amount,
                                                "description"=>$desc
                                                ]];

            session()->put('donations', $item_or_cash);
            return redirect('/shopping/booktou-assist-donation_wizard?profile=4');
            //dd(session()->get('profile'));die();
        }

           return view('store_front.donation.donation_second_step');
    }elseif ($request->profile==4) {
        //dd(session()->get('donations'));die();
        if (session()->has("donor")=="" && session()->has("donations")=="")
                {
                    return redirect('/shopping/booktou-assist-donation_wizard?profile=2');
                }

        if ($request->btn_confirm=="confirm") {

             $donationList = new DonationModel;
             $donationBeneficiary = new DonationBeneficiaryModel;
             $donationItems = new DonationItemsModel;
             $donation = new DonationDonorModel;



             if (session()->has("donor") & session()->has("donations")) {
                //declaring variables
                 //dd(session()->get("donations"));
                $donorname;$donorphone;$donoraddress;$terms;$category;
                $beneficiaryname;$beneficiaryphone;$beneficiaryaddress;
                $mode;$type_of_donation;$amount;
                //
                foreach (session()->get("donor") as $key => $d) {
                    $donorname = $d['name'];
                    $donorphone = $d['phone'];
                    $donoraddress = $d['address'];
                    $terms= $d['show_name'];
                    $category = $d['category'];
                }

                foreach (session()->get("donations") as $key => $don) {
                    $beneficiaryname = $don['name'];
                    $beneficiaryphone = $don['phone'];
                    $beneficiaryaddress = $don['address'];
                    $mode = $don['mode'];
                    $type_of_donation = $don['type'];
                    $amount = $don['amount'];
                    $item_desc = $don['description'];
                }

                  //saving into ba_donation_donor
                $donation->name = $donorname;
                $donation->phone = $donorphone;
                $donation->address = $donoraddress;
                $donation->category = $category;
                $donation->save();
                //
                $str = str_replace('_', ' ', $type_of_donation);
                //
                $donationList->donor_id  = $donation->id;
                $donationList->donation_mode=$mode;
                $donationList->donation_type= $str;
                $donationList->agrees_terms  = $terms;

                $donationList->save();

                //

                //
                if ($str=="to a person") {
                    $donationBeneficiary->entered_by  = $donation->id;
                    $donationBeneficiary->name   = $beneficiaryname;
                    $donationBeneficiary->phone  = $beneficiaryphone;
                    $donationBeneficiary->address  = $beneficiaryaddress;
                    $donationBeneficiary->save();
                }else{

                    $donationBeneficiary->entered_by  = $donation->id;
                    $donationBeneficiary->name   = $beneficiaryname;
                    $donationBeneficiary->phone  = $beneficiaryphone;
                    $donationBeneficiary->address  = $beneficiaryaddress;
                    $donationBeneficiary->save();
                }



                //

                //
                if ($mode=="money") {
                    $donationItems->donation_id = $donationList->id;
                    $donationItems->donor_id =$donation->id;
                    $donationItems->beneficiary_id =$donationBeneficiary->id;
                    $donationItems->amount = $amount;

                    $save = $donationItems->save();

                }elseif ($mode=="item") {

                    $donationItems->donation_id = $donationList->id;
                    $donationItems->donor_id =$donation->id;
                    $donationItems->beneficiary_id =0 ;
                    $donationItems->items = $item_desc ;
                    $save = $donationItems->save();
                }

                    if ($save) {

                        $showdonor = DonationDonorModel::find($donation->id);
                        $donorList = DB::table('ba_donations')
                        ->where('donor_id',$donation->id)
                        ->first();

                        if ($showdonor && $donorList) {
                             $donor_id = $showdonor->id;
                             $terms_name = $donorList->agrees_terms;

                            session()->forget('donor');
                            session()->forget('donations');

                             session()->put('donorid',$donor_id);
                             session()->put('terms',$terms_name);
                        }


                        return redirect('/shopping/booktou-assist-donation_wizard?l=7');
                    }



             }

        }

        return view('store_front.donation.assist_donation_final');
    }elseif ($request->profile==5) {

            //dd(session()->get("donor"));die();
            if (session()->has("donor")=="" && session()->has("beneficiary")=="")
            {
                return redirect('/shopping/booktou-assist-donation_wizard?profile=2');
            }


            if($request->btn_add=="Addbeneficiary") {
                $beneficiary =[$request->profile=>[
                                                "name" => $request->doneename,
                                                "phone" =>$request->doneephone,
                                                "address" =>$request->doneeaddress

                                                ]
                                                ];

            session()->put('beneficiary', $beneficiary);
            return view('store_front.donation.assist_donation_final');
        }


        //saving record section
        if ($request->btn_confirm_beneficiary=="confirmBeneficiary") {



                  if (session()->has("donor") && session()->has("beneficiary")) {
                    //declaring variables

                    $donationBeneficiary = new DonationBeneficiaryModel;
                    $donation = new DonationDonorModel;
                    $donorname;$donorphone;$donoraddress;$category;
                    $beneficiaryname;$beneficiaryphone;$beneficiaryaddress;

                    foreach (session()->get("donor") as $key=>$d) {
                    $donorname = $d['name'];
                    $donorphone = $d['phone'];
                    $donoraddress = $d['address'];
                    $category = $d['category'];
                    }

                    foreach (session()->get("beneficiary") as $key => $b) {
                    $beneficiaryname = $b['name'];
                    $beneficiaryphone = $b['phone'];
                    $beneficiaryaddress = $b['address'];
                    }
                    //saving donor in table
                    $donation->name=$donorname;
                    $donation->phone=$donorphone;
                    $donation->address=$donoraddress;
                    $donation->category=$category;

                    $donation->save();
                    //donor table record save successfull

                    $donationBeneficiary->entered_by=$donation->id;
                    $donationBeneficiary->name=$beneficiaryname;
                    $donationBeneficiary->phone=$beneficiaryphone;
                    $donationBeneficiary->address=$beneficiaryaddress;

                    $save  =  $donationBeneficiary->save();

                    if ($save) {

                        session()->forget('donor');
                        session()->forget('beneficiary');
                        return redirect('/shopping/booktou-assist-donation_wizard?l=7');
                    }

                }


            }

        //



        return view('store_front.donation.add_beneficiary_form');

    }elseif ($request->l==7) {

        if (session()->get('donorid')=="") {
             return redirect('/shopping/booktou-assist-donation_wizard?profile=2');
         }



        return view('store_front.donation.confirm_donation');


    }

    return view('store_front.donation.assist_donation_wizard');



    }



    protected function covidFundDonors(Request $request)
    {

        $donor = array();
        $donorlist = DB::table('ba_donation_donor')
        ->select()
        ->get();

        $data = array("donor"=>$donorlist);

            return view('store_front.donation.view_donation_list')->with('data',$data);
            //return view('store_front.donation.view_donation_list');


        if ($request->ListBtn=="d2") {
           $beneficiaryList = DB::table('ba_donation_beneficiery')
            //->join('ba_donation_items','ba_donation_beneficiery.entered_by','=','ba_donation_items.donor_id')
            ->select()
            ->get();

            //dd($beneficiaryList);

            $data = array("beneficiary"=>$beneficiaryList);
            return view('store_front.donation.view_beneficiary_list')->with('data',$data);
        }
    }

    protected function covidFundBeneficiaries(Request $request)
    {


           $beneficiaryList = DB::table('ba_donation_beneficiery')
            ->select()
            ->get();

            //dd($beneficiaryList);

            $data = array("beneficiary"=>$beneficiaryList);
            return view('store_front.donation.view_beneficiary_list')->with('data',$data);

    }




    protected function viewMyOrders(Request $request)
    {

        if( $request->month  == "")
        {
            $month   =  date('m');
        }
        else
        {
          $month   =  $request->month ;
        }


        if( $request->year   == "")
        {
            $year = date('Y');
        }
        else
        {
          $year   =  $request->year;
        }


        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        $uid = $request->session()->get('__user_id_' );

        //customer as member
        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();


        if(!isset($member ))
        {
           return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        //cart details
        $normal  = DB::table("ba_service_booking")
        ->where("book_by",  $member->id )
        //->orderBy("book_date", "desc")
        ->select(
            'id',
            'book_date',
            'book_status',
            'payment_type',
            'bin',
            'cust_remarks as orderdescription',
            DB::Raw("'' type")
            );
        //->get();

        $orders  = DB::table("ba_pick_and_drop_order")
        ->where("request_by",  $member->id )
        ->where('source','customer')
        ->union($normal)
        ->select(
            'id',
            'book_date',
            'book_status',
            'pay_mode as payment_type',
            DB::Raw('0 bin'),
            'pickup_details as orderdescription',
            DB::Raw("'' type")
            )
        ->orderBy("book_date", "desc")
        ->get();

        if( !isset($orders ))
        {
            return redirect("/settings/view-my-profile")->with("err_msg", "No orders found!");
        }



      $all_bins = $order_id = array();
      foreach($orders as $bitem )
      {
        $all_bins[] = $bitem->bin;
        $order_id[] = $bitem->id;
      }

      $sequence = DB::table('ba_order_sequencer')
      ->whereIn('id',$order_id)
      ->get();

      foreach($orders as $bitem )
      {
        foreach($sequence as $items)
        {
            if ($bitem->id == $items->id) {
                $bitem->type = $items->type;
                break;
            }
        }
      }

      $all_bins = array_filter($all_bins);

      $all_businesses = DB::table("ba_business")
      ->whereIn("id", $all_bins)
      ->get();



    $data = array("orders"=>$orders, 'month' => $month, 'year' => $year, 'all_businesses' => $all_businesses );
    return view('store_front.orders.all_orders')->with( $data );

    }

    protected function myOrderDetails(Request $request)
    {
        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        if( $request->key  == "")
        {
            return redirect("/shopping/my-orders")->with("err_msg","Please select an ordert to view it!");
        }

        $oid = $request->key;
        $uid = $request->session()->get('__user_id_' );

        //customer as member
        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
           return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        $ordersequence = OrderSequencerModel::find($oid);
        if(!isset($ordersequence ))
        {
           return redirect("/shopping/checkout")->with("err_msg", "Order not found!");
        }

        $ordertype = $ordersequence->type;

        if ( in_array($ordersequence->type, array("booking", "appointment" )  ) ) 
        {
            //cart details
            $order  = DB::table("ba_service_booking")
            ->where("ba_service_booking.book_by",  $member->id )
            ->where("ba_service_booking.id",  $oid )
            ->first();

            $booking_details = DB::table("ba_service_booking_details")
            ->join('ba_service_products','ba_service_booking_details.service_product_id','=','ba_service_products.id')
            ->where("book_id",$order->id)
            ->select("ba_service_booking_details.*","ba_service_products.photos","ba_service_products.srv_name as serviceName","ba_service_products.srv_details as serviceDetails")
            ->get();

            $view_name  =  'store_front.booking.booking_details';

        }else if ( $ordersequence->type=="offers" )
        {
            //cart details
            $order  = DB::table("ba_service_booking")
            ->where("ba_service_booking.book_by",  $member->id )
            ->where("ba_service_booking.id",  $oid )
            ->first();

            $booking_details = DB::table("ba_shopping_basket_srv_products")
            ->where("order_no",$order->id)
            ->get();

            $view_name  =  'store_front.orders.order_details';


        }else if($ordersequence->type=="normal"){
                //cart details
            $order  = DB::table("ba_service_booking")
            ->where("book_by",  $member->id )
            ->where("id",  $oid )
            ->first();

            $booking_details = DB::table("ba_service_booking_details")
            ->where("book_id",$order->id)
            ->get();

            $view_name  =  'store_front.orders.order_details';

        }else if ( strcasecmp($ordersequence->type,  "pnd" )  == 0 )  
        {

            $order  = DB::table("ba_pick_and_drop_order")
            ->where("request_by",  $member->id )
            ->where("id",$oid )
            ->first();

            $view_name  =  'store_front.orders.order_details';

        }else if ( strcasecmp($ordersequence->type,  "assist" )  == 0 )  
        {
          $order  = DB::table("ba_pick_and_drop_order")
          ->where("request_by",  $member->id )
          ->where("id",$oid )
          ->first();
          $view_name  =  'store_front.orders.order_details';
      } 


    if(!isset($order))
    {
        return redirect("/settings/view-my-profile")->with("err_msg", "No orders found!");
    }


    $cart_items  = DB::table("ba_shopping_basket")
    ->join('ba_product_variant','ba_shopping_basket.prsubid','=','ba_product_variant.prsubid')
    ->where("order_no",   $oid )
    ->select("ba_shopping_basket.*","ba_product_variant.sgst_pc as sgst","ba_product_variant.cgst_pc as cgst")
    ->get();

    $staff  = DB::table("ba_profile")
    ->select("id as staffId", "fullname as staffName", "profile_photo as profilePicture" )
    ->where("id",  $order->staff_id )
    ->first();


    $business = Business::find($order->bin);
    if (isset($business)) 
    {
        $rating_type = DB::table("ba_rating_types")
        ->select("ba_rating_types.*",
            DB::Raw("'' type"),
            DB::Raw("'' review"),
            DB::Raw("'0' rating"),
            DB::Raw("'' status"))
        ->where('main_module',strtolower($business->main_module))
        ->get();

        $service_rating = DB::table("ba_service_ratings")
        ->where('order_no',$oid)
        ->get();

        $rating_ = array();

        foreach ($rating_type as $items) {
            foreach($service_rating as $list)
            {
                if ($items->rating_type== $list->rating_type) {
                   $items->type = $list->rating_type;
                   $items->review = $list->review;
                   $items->rating = $list->rating;
                   $items->status = $list->status;
                   break;
               }
           }
       }
   }
 
   $data = array("order"=>$order, 'cart_items' => $cart_items ,'staff'=>$staff,
    'member' => $member, 'business' => $business,'ordertype'=>$ordertype,
    "booking_details"=>$booking_details,'ratingType'=>$rating_type);
   return view( $view_name )->with( $data );

}

    protected function myBookings(Request $request)
    {
        if( $request->month  == "")
        {
            $month   =  date('m');
        }
        else
        {
          $month   =  $request->month ;
        }


        if( $request->year   == "")
        {
            $year = date('Y');
        }
        else
        {
          $year   =  $request->year;
        }


        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        $uid = $request->session()->get('__user_id_' );

        //customer as member
        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
           return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        $bookings =DB::table('ba_service_booking')
        ->join('ba_business','ba_service_booking.bin','=','ba_business.id')
        ->whereIn('ba_business.main_module',array('BOOKING','APPOINTMENT'))
        ->where("book_by", $member->id )
        ->select(
            'ba_service_booking.id as orderid',
            'book_date',
            'service_date',
            'book_status',
            'payment_type',
            'preferred_time',
            'bin',
            'cust_remarks as orderdescription',
            DB::Raw("'' type"),
            "ba_business.*")
        ->orderBy("service_date", "desc")
        ->paginate(10);

        if (!$bookings) {
             return redirect('shopping/my-bookings')->with('err_msg','no booking orders found!');
        }

        $data = array('orders'=>$bookings);

        return view('store_front.orders.all_bookings')->with($data);

    }

    protected function updateOrderReview(Request $request)
    {
           if($request->orderno=="")
           {
                return redirect('/shopping/my-orders');
           }
           $user_info = User::find($request->session()->get('__user_id_' ));

           if (!$user_info) {
                return redirect('shopping/login');
           }

           $orderno = $request->orderno;

           $ratinglist = array();
           $i=0;
           foreach($request->ratingType as $type)
           {
                $ratinglist[$i]['type'] = $type;
                $ratinglist[$i]['rating'] = $request->rating[$i];
                $ratinglist[$i]['review'] = $request->review[$i];
                $i++;
           }

           foreach ($ratinglist as $items) {

                        $checkRating = DB::table('ba_service_ratings')
                        ->where('order_no',$orderno)
                        ->where('rating_type',$items['type'])
                        ->first();

                        // checking rating type and order no exist or not
                        if (isset($checkRating)) {
                            $servicerating = ServiceRating::find($checkRating->id);
                        }else{
                            $servicerating = new ServiceRating;
                        }
                        // checking ends here

                        $servicerating->member_id = $user_info->profile_id;
                        $servicerating->order_no = $orderno;
                        $servicerating->rating_type =  $items['type'];
                        $servicerating->rating  =$items['rating'];
                        $servicerating->review  = $items['review'];
                        $servicerating->rated_on = date('Y-m-d');
                        $servicerating->status = 'new' ;
                        $save = $servicerating->save();

            }

            if ($save) {
                return redirect('shopping/my-order-details?key='.$orderno);
            }else{
                return redirect('shopping/my-order-details?key='.$orderno);
            }




    }

     protected function forgotPassword(Request $request)
    {




        if(isset($request->phoneno))
        {
            $userProfile = User::where('phone',$request->phoneno)
            ->where('category',0)
            ->first();

            if ($userProfile) {


                //generate OTP
                $rand = mt_rand(111111, 999999);
                $userProfile->otp = $rand;
                $userProfile->save();

                $message_row = DB::table("ba_sms_templates")
                ->where("name", "otp_generate")
                ->first();

                  if(isset($message_row))
                  {
                      $otp_msg =  urlencode( str_replace(  "###", $rand,  $message_row->message ) );
                      $this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id ,   array($userProfile->phone)   ,  $otp_msg  );
                  }


                 return view('store_front.check_otp');
            }else
            {
                $msg = "No matching phone no found!";
                return view('store_front.reset_password')->with('err_msg',$msg);
            }
        }else
        {
            return view('store_front.reset_password');
        }



    }

    protected function passwordOtpcheck(Request $request)
    {

        if(isset($request->otp))
        {
            $otp = User::where('otp',$request->otp)
            ->where('status',1)
            ->where('category',0)
            ->first();

            if ($otp) {

                return view('store_front.change_password')->with('userslist',$otp);
            }else{
                $msg="Otp verification mismatch! please try again.";
                return view('store_front.check_otp')->with('err_msg',$msg);
            }

        }else{
             return view('store_front.check_otp');
        }

    }

    protected function changePassword(Request $request)
    {

        if(isset($request->newpassword) && isset($request->cpassword))
        {
            if(isset($request->userlistid))
            {

                $update = DB::table('ba_users')
                ->where('id', $request->userlistid)
                ->update(['password' => Hash::make($request->newpassword)]);

                return redirect('/shopping/login');

            }


        }

    }



 protected function checkoutInProcess(Request $request)
    {
        return view("store_front.payment.checkout_in_progress");
    }


    public function cashfreeResponse(Request $request)
    {
            if ($request->referenceId=="" || $request->orderId=="") {
                   return redirect("https://booktou.in");
            }

            $ordersequence = OrderSequencerModel::find($request->orderId);


             //FAILED SUCCESS
             //$secretkey = '8173786f158d80f86ffc4b4ec382f022883a9dd9';
             $secretkey ="b22f705beefcbb32183a74c74715a88319966887";

             $orderId = $request->orderId;
             $orderAmount = $request->orderAmount;
             $referenceId = $request->referenceId;
             $txStatus = $request->txStatus;
             $paymentMode = $request->paymentMode;
             $txMsg = $request->txMsg;
             $txTime = $request->txTime;
             $signature = $request->signature;

             $data = $orderId.$orderAmount.$referenceId.$txStatus.$paymentMode.$txMsg.$txTime;
             $hash_hmac = hash_hmac('sha256', $data, $secretkey, true) ;
             $computedSignature = base64_encode($hash_hmac);

            switch ($ordersequence->type) {
                case 'booking':
                     if ($signature == $computedSignature) {
                                //signature check is valid it will process further


                                if ($txStatus=="SUCCESS") {
                                    $ServiceBooking = ServiceBooking::find($orderId);
                                    $ServiceBooking->reference_no = $referenceId;
                                    $ServiceBooking->transaction_no = $referenceId;
                                    $ServiceBooking->book_status = "new";
                                    $ServiceBooking->clerance_status = "paid";
                                    $ServiceBooking->save();

                                    $BookingDetailsByOrderID = ServiceBookingDetails::where("book_id",$request->orderId)->first();

                                    $serviceBookDetails = ServiceBookingDetails::find($BookingDetailsByOrderID->id);

                                    $serviceBookDetails->status="new";
                                    $serviceBookDetails->save();
                                }
                                //for cancelling booking appointment or drop by customer itself
                                else if($txStatus=="FAILED"|| $txStatus=="CANCELLED" ||
                                     $txStatus=="USER_DROPPED"){

                                    $ServiceBooking = ServiceBooking::find($orderId);
                                    $ServiceBooking->reference_no = $referenceId;
                                    $ServiceBooking->transaction_no = $referenceId;
                                    $ServiceBooking->book_status = "cancelled";
                                    $ServiceBooking->clerance_status = "un-paid";
                                    $ServiceBooking->save();

                                    $BookingDetailsByOrderID = ServiceBookingDetails::where("book_id",$request->orderId)->first();

                                    $serviceBookDetails = ServiceBookingDetails::find($BookingDetailsByOrderID->id);

                                    $serviceBookDetails->status="cancel_by_client";
                                    $serviceBookDetails->save();

                                }
                                //cancelled code ends here
                                return redirect("/shopping/my-order-details?key=".$orderId."");
                                //response to my order details view

                              } else {
                                return redirect('/shopping/checkout');
                             }
                    break;

                default:
                     // normal order update part

                             if ($signature == $computedSignature) {
                                //signature check is valid it will process further


                            if ($txStatus=="SUCCESS"|| $txStatus=="PENDING") {
                                    $ServiceBooking = ServiceBooking::find($orderId);
                                    $ServiceBooking->reference_no = $referenceId;
                                    $ServiceBooking->transaction_no = $referenceId;
                                    $ServiceBooking->book_status = "new";
                                    $ServiceBooking->clerance_status = "paid";
                                    $ServiceBooking->save();
                                }//for failed, cancelled and user droped normal orders
                                else if($txStatus=="FAILED"|| $txStatus=="CANCELLED" ||
                                     $txStatus=="USER_DROPPED"){

                                    $ServiceBooking = ServiceBooking::find($orderId);
                                    $ServiceBooking->reference_no = $referenceId;
                                    $ServiceBooking->transaction_no = $referenceId;
                                    $ServiceBooking->book_status = "cancelled";
                                    $ServiceBooking->clerance_status = "un-paid";
                                    $ServiceBooking->save();
                            }

                                return redirect("/shopping/my-order-details?key=".$orderId."");
                                //response to my order details view

                              } else {
                                return redirect('/shopping/checkout');
                             }
                    break;
            }


    }


    protected function showWishListProduct(Request $request)
    {

        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to view your profile!");
        }

        if($request->session()->get('__user_id_' ))
        {
            $UserObj = User::find($request->session()->get('__user_id_' ));

            if ($UserObj) {
               $profileid = $UserObj->profile_id;
            }

            $wishlist = DB::table("ba_member_wishlist")
            ->where("member_id",$UserObj->profile_id)
            ->first();

            if (!$wishlist) {
                return redirect("/shopping/checkout");
            }

            $products =  DB::table('ba_products')
            ->join('ba_product_variant','ba_products.id','=','ba_product_variant.prid')
            ->where("ba_product_variant.prid",$wishlist->prid)
            ->select("ba_products.*","ba_product_variant.*",DB::Raw("'' photos"))
            ->get();

            $photos = DB::table("ba_product_photos")
            ->where("prid",$wishlist->prid)
            ->select()
            ->get();

            $data = array("products"=>$products,"photos"=>$photos);

            return view('store_front.view_wishlist')->with($data);


        }

}
   //SNS Service Booking
public function makeServiceBooking(Request $request)
{

    $gateway_mode =  config('app.gateway_mode'); 

    $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );

    if( !$request->session()->has('__user_id_' ))
    {
        return redirect("/booking/slot-selector/". $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Please login to view your profile!");
    }
    $uid = $request->session()->get('__user_id_' );
    $user_info =  json_decode($this->getUserInfo($uid));
    if(  $user_info->user_id == "na" )
    {
      return redirect("/booking/slot-selector/". $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "User has no priviledge to access this feature! Please consult admin.!");
  }

  $addressCustomer = $user_info->userAddress;
  $customer_info = CustomerProfileModel::find($user_info->member_id);

  if(!isset($customer_info))
  {
     return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg",'No matching customer found!');
 }

 if(  $request->serviceDate != "" )
 {
        $service_date = date('Y-m-d'  , strtotime($request->serviceDate )); //service on specified date
    }
    else
    {
        $service_date = date('Y-m-d');
    }

    $business = Business::find( $request->key );

    if( !isset($business))
    {
        return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Business information not found!");
    }
    

    if(isset($request->btnorder))
    {

      if ($request->paymode=="online")
       {
        $book_status = "pending-payment";
        $paymentType = "Pay online (UPI)";
        }
        else
        {
            $book_status = "new";
            $paymentType = "Cash on delivery";

        }

    $serviceProductIds = $request->serviceProductIds;

    if(sizeof($serviceProductIds) == 0 )
    {
        return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Missing data to perform action.!");
    }


    $service_products = DB::table("ba_service_products")
    ->where("bin", $request->key )
    ->whereIn("id", $serviceProductIds)
    ->get();

    if( !isset($service_products) )
    {
       return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "No service products found!");
    }

   $total_duration_required = 0;
   $total_cost= $total_discount = 0.00;
   $serviceProductWithDuration = array();
   foreach($serviceProductIds as $serviceProduct)
   {
        $serviceDuration =0;
        foreach($service_products as $item)
        {
            if($serviceProduct == $item->id )
            {
                $serviceDuration += ( $item->duration_hr * 60 ) + $item->duration_min;
                $total_duration_required +=  ( $item->duration_hr * 60 ) + $item->duration_min;
                $total_cost +=  $item->actual_price;
                $total_discount  += $item->discount;
            }
        }
        $serviceProductWithDuration[] = array( 'serviceProductId' => $serviceProduct,
            'duration' => $serviceDuration );
    }

    $otp = mt_rand(111111, 999999);//order OTP generation
    if( strcasecmp($business->sub_module ,"paa" ) ==0 )
    {
        if($request->serviceDate == "" ||   $request->serviceProductIds == ""  )
        {
           return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Service date is missing!");
        }

       $staffId = 0;

       $booked_dates  = DB::table("ba_service_booking")
       ->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
       ->whereDate("ba_service_booking.service_date", $service_date   )
       ->whereIn("ba_service_booking_details.service_product_id",  $serviceProductIds )
       ->where("ba_service_booking.bin", $request->bin)
        //->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment') )
       ->whereIn("book_status",  array('new','confirmed','engaged') )
       ->select("ba_service_booking.id as book_id", "service_date",  "ba_service_booking_details.service_product_id",
        DB::Raw("'close' status") )
       ->get();

       $days = array();
       $status ="open";
       $found = false;
        //if any service is close for booking the rest of the service in the selection will be blocked from booking
       foreach($serviceProductIds as $service_product_id)
       {
        foreach($booked_dates as $item)
            {
                if(  $item->service_product_id == $service_product_id)
                {
                    $found = true;
                    break;
                }
            }

            if( $found )
            {
                $status ="close";
                break;
            }
        }

    if(  $found )
    {
       return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Selected service is already booked. Please choose another date or change service.");
    }

        //no need to check slot as whole day is booked
        $slotno = 0; //assuming full day

        //generate order sequencer
        $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
        $rp_1 = mt_rand(0, 9);
        $rp_2 = mt_rand(0, 9);
        $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

        $sequencer = new OrderSequencerModel;
        $sequencer->type = "booking";
        $sequencer->tracker_session =  $tracker;
        $save = $sequencer->save();
        //generate order sequencer ends

        if( !isset($save) )
        {

           return redirect("/booking/slot-selector/" .  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Failed to generate order no sequence.");
        }

        //generate merchant sequencer
       $merchant_order_no =1;
       $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
       if(isset($merchant_order_info))
       {
        $merchant_order_no =  $merchant_order_info->last_order + 1;
       }
        else
        {
            $merchant_order_info = new MerchantOrderSequencer;
            $merchant_order_info->bin  = $request->bin;
        }
        //generate merchant sequencer ends

        //make new booking
        $newbooking = new ServiceBooking;
        $newbooking->id = $sequencer->id ;
        $newbooking->customer_name = $customer_info->fullname;
        $newbooking->customer_phone = $customer_info->phone;
        $newbooking->address = $customer_info->locality;
        $newbooking->landmark =$customer_info->landmark;
        $newbooking->city = $customer_info->city ;
        $newbooking->state = $customer_info->state;
        $newbooking->biz_order_no = $merchant_order_no;
        $newbooking->bin = $request->bin;
        $newbooking->book_category= $business->category;
        $newbooking->book_by = $user_info->member_id;
        $newbooking->staff_id = $staffId;
        $newbooking->book_status = $book_status;
        $newbooking->total_cost =  $total_cost;
        $newbooking->seller_payable =  $total_cost;
        $newbooking->discount = $total_discount;
        $newbooking->service_fee  =  0.00;
        $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
        $newbooking->cashback =  0;
        $newbooking->payment_type =   $paymentType;   //over the counter
        $newbooking->latitude =  $newbooking->longitude =  0;
        $newbooking->book_date =  date('Y-m-d H:i:s');
        $newbooking->service_date =  $service_date;
        $newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
        $newbooking->transaction_no =  "na" ;
        $newbooking->reference_no =  "na"  ;

        $newbooking->preferred_time =  date('H:i:s', strtotime($request->preferredTime));
        $newbooking->otp = $otp ;
        $newbooking->address = $customer_info->locality;
        $newbooking->landmark = $customer_info->landmark;
        $newbooking->city = $customer_info->city;
        $newbooking->state = $customer_info->state;
        $newbooking->pin_code  = $customer_info->pin_code;
        $save=$newbooking->save();
        $item_notes = array();
        if($save)
        {
            //update merchant order sequencer log
            $merchant_order_info->last_order  = $merchant_order_no;
            $merchant_order_info->save();
            //ending merchant order sequencer

            //updatig which service were booked in this order
            $book_slots = array();
            foreach($service_products as $item)
            {
                $newbookingDetails = new ServiceBookingDetails;
                $newbookingDetails->book_id = $sequencer->id;
                $newbookingDetails->service_product_id = $item->id    ;
                $newbookingDetails->service_name = $item->srv_name  ;
                $newbookingDetails->slot_no = $slotno ;
                $newbookingDetails->status = "new";
                $newbookingDetails->service_time = date('H:i:s', strtotime( $request->preferredTime));
                $newbookingDetails->service_charge = $item->actual_price;
                $newbookingDetails->save();
            }
            //updating occuppied
            DB::table("ba_service_days")
            ->whereDate("service_date", $service_date)
            ->where("bin",$request->bin)
            ->update(array("status" => "booked") ) ;
        }


    } // SNS part
    else
    {
        if($request->serviceDate == "" || $request->staffId == "" || $request->serviceProductIds == ""  )
        {
            return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Missing data to perform action!");
        }
        $staffId =   $request->staffId;



        if( $request->preferredTime != "")
        {
            //home visit
            //generate order sequencer
            $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
            $rp_1 = mt_rand(0, 9);
            $rp_2 = mt_rand(0, 9);
            $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

            $sequencer = new OrderSequencerModel;
            $sequencer->type = "booking";
            $sequencer->tracker_session =  $tracker;
            $save = $sequencer->save();
            //generate order sequencer ends

            if( !isset($save) )
            {
                $data= ["message" => "failure", "status_code" =>  997 , 'detailed_msg' => 'Failed to generate order no sequence.'  ];
                return json_encode($data);
            }

            //generate merchant sequencer
            $merchant_order_no =1;
            $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
            if(isset($merchant_order_info))
            {
                $merchant_order_no =  $merchant_order_info->last_order + 1;
            }
            else
            {
                $merchant_order_info = new MerchantOrderSequencer;
                $merchant_order_info->bin  = $request->bin;
            }
            //generate merchant sequencer ends

            //make new booking
            $newbooking = new ServiceBooking;
            $newbooking->id = $sequencer->id ;
            $newbooking->customer_name = $customer_info->fullname;
            $newbooking->customer_phone = $customer_info->phone;
            $newbooking->address = $customer_info->locality;
            $newbooking->landmark =$customer_info->landmark;
            $newbooking->city = $customer_info->city ;
            $newbooking->state = $customer_info->state;
            $newbooking->biz_order_no = $merchant_order_no;
            $newbooking->bin = $request->bin;
            $newbooking->book_category= $business->category;
            $newbooking->book_by = $user_info->member_id;
            $newbooking->staff_id = $staffId;
            $newbooking->book_status = $book_status;
            $newbooking->total_cost =  $total_cost;
            $newbooking->seller_payable =  $total_cost;
            $newbooking->discount = $total_discount;
            $newbooking->service_fee  =  0.00;
            $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
            $newbooking->cashback =  0;
            $newbooking->payment_type =  $paymentType;   //over the counter
            $newbooking->latitude =  $newbooking->longitude =  0;
            $newbooking->book_date =  date('Y-m-d H:i:s');
            $newbooking->service_date =  $service_date;
            $newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
            $newbooking->transaction_no =  "na" ;
            $newbooking->reference_no =  "na"  ;
            $newbooking->preferred_time =  date('H:i:s', strtotime( $request->preferredTime));
            $newbooking->otp  =  $otp ;
            $newbooking->address = $customer_info->locality;
            $newbooking->landmark = $customer_info->landmark;
            $newbooking->city = $customer_info->city;
            $newbooking->state = $customer_info->state;
            $newbooking->pin_code  = $customer_info->pin_code;
            $save=$newbooking->save();
            $item_notes = array();
            if($save)
            {
                //update merchant order sequencer log
                $merchant_order_info->last_order  = $merchant_order_no;
                $merchant_order_info->save();
                //ending merchant order sequencer
            }
        }
        else
        {
            //check if slot is available for the day
            $slotno= $request->serviceTimeSlot;

            //ascending order slot is very important
            $day_name = date('l', strtotime( $service_date ));
            $staff_id = $request->staffId ;
            if($request->staffId  == -1) //any staff
            {
                //find available staff
                $any_staff_available = DB::table("ba_service_days")
                ->where("slot_number", $slotno )
                ->whereDate("service_date",  $service_date )
                ->where("bin",$request->bin)
                ->where("status", "open")
                ->orderBy("slot_number", "asc")
                ->first();

                if(isset($any_staff_available) == 0)
                {

                    return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "No staff available in the selected slot. Please change date or reduce sevice selected.");
                }

                $staff_id = $any_staff_available->staff_id;
            }

            //ascending order sort is important otherwise it will break code
            $available_slots = DB::table("ba_service_days")
            ->where("slot_number", ">=", $slotno )
            ->where("staff_id",  $staff_id )
            ->whereDate("service_date",  $service_date )
            ->where("bin",$request->bin)
            ->where("status", "open")
            ->orderBy("slot_number", "asc")
            ->get();

            $availableSlots = $book_slots = array();
            $total_slots_min = 0;
            $starting_slot_found = false;
            $ending_slot_found = false;


            //$serviceProductWithDuration[] = array('serviceProductId' =>  1, 'duration' =>  4 );
            $lastFoundSlot = $lastScannedSlot = 0;
            $scanCount =0;
            foreach($serviceProductWithDuration as $serviceProductDuration )
            {
                foreach($available_slots as $item )
                {
                    if( $item->slot_number == $request->serviceTimeSlot  )
                    {
                        $starting_slot_found = true; //user specified slot is found empty
                    }
                    if($lastScannedSlot == 0 )
                    {
                        $lastScannedSlot = $item->slot_number;
                    }
                    //skip upto last processed slot from second scan onwards
                    if( $scanCount > 0 && $lastScannedSlot >= $item->slot_number )
                    {
                        continue;
                    }

                    $lastScannedSlot = $item->slot_number; //logging last scanne slot
                    $start_time =   strtotime( $item->start_time );
                    $end_time =   strtotime( $item->end_time );
                    $gap =  abs( ( $end_time - $start_time) / 60);
                    $serviceProductDuration['duration'] -= $gap;
                    $total_slots_min += $gap;
                    $total_duration_required -= $gap; //reducing total duration

                    $availableSlots[] = array(
                        'serviceProductId' => $serviceProductDuration['serviceProductId'],
                        "slot" => $lastScannedSlot ,
                        "duration" => abs($gap),
                        "startTime" =>$start_time,
                    );
                    if( $serviceProductDuration['duration'] <= 0 )
                    {
                        break;
                    }
                }
                $scanCount++;
            }


            $allowBooking = true;

            if(count($availableSlots) == 0 || !$starting_slot_found || !$allowBooking  )
            {

               return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Insufficient slots. Please change date or reduce sevice selected.");

            }


           foreach($availableSlots as $availableSlot)
           {
            $book_slots[] = $availableSlot['slot'];
           }


            //generate order sequencer
        $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
        $rp_1 = mt_rand(0, 9);
        $rp_2 = mt_rand(0, 9);
        $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

        $sequencer = new OrderSequencerModel;
        $sequencer->type = "booking";
        $sequencer->tracker_session =  $tracker;
        $save = $sequencer->save();
            //generate order sequencer ends

        if( !isset($save) )
        {

          return redirect("/booking/slot-selector/".  $request->key  . "/" . $request->serviceProductIds[0])->with("err_msg", "Failed to generate order no sequence.");

        }

            //generate merchant sequencer
      $merchant_order_no =1;
      $merchant_order_info = MerchantOrderSequencer::where("bin", $request->bin )->first();
      if(isset($merchant_order_info))
      {
        $merchant_order_no =  $merchant_order_info->last_order + 1;
      }
        else
        {
            $merchant_order_info = new MerchantOrderSequencer;
            $merchant_order_info->bin  = $request->bin;
        }
            //generate merchant sequencer ends

            //make new booking
            $newbooking = new ServiceBooking;
            $newbooking->id = $sequencer->id ;
            $newbooking->customer_name = $customer_info->fullname;
            $newbooking->customer_phone = $customer_info->phone;
            $newbooking->address = $customer_info->locality;
            $newbooking->landmark =$customer_info->landmark;
            $newbooking->city = $customer_info->city ;
            $newbooking->state = $customer_info->state;
            $newbooking->biz_order_no = $merchant_order_no;
            $newbooking->bin = $request->bin;
            $newbooking->book_category= $business->category;
            $newbooking->book_by = $user_info->member_id;
            $newbooking->staff_id = $staffId;
            $newbooking->book_status = $book_status;
            $newbooking->total_cost =  $total_cost;
            $newbooking->seller_payable =  $total_cost;
            $newbooking->discount = $total_discount;
            $newbooking->service_fee  =  0.00;
            $newbooking->delivery_charge  = $newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
            $newbooking->cashback =  0;
            $newbooking->payment_type =   $paymentType;   //over the counter
            $newbooking->latitude =  $newbooking->longitude =  0;
            $newbooking->book_date =  date('Y-m-d H:i:s');
            $newbooking->service_date =  $service_date;
            $newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
            $newbooking->transaction_no =  "na" ;
            $newbooking->reference_no =  "na"  ;
            $newbooking->otp  =  $otp ;
            $newbooking->address = $customer_info->locality;
            $newbooking->landmark = $customer_info->landmark;
            $newbooking->city = $customer_info->city;
            $newbooking->state = $customer_info->state;
            $newbooking->pin_code  = $customer_info->pin_code;
            $save=$newbooking->save();
            $item_notes = array();
            if($save)
            {
                //update merchant order sequencer log
                $merchant_order_info->last_order  = $merchant_order_no;
                $merchant_order_info->save();
                //ending merchant order sequencer
                $pos = 0;
                $size = sizeof($availableSlots);

                foreach($service_products as $item)
                {
                    if( $pos >= $size )
                    {
                        $pos = $size-1;
                    }

                    $startSlot = 0;
                    $startTime = time();

                    foreach($availableSlots as $availableSlot)
                    {
                        if( $availableSlot['serviceProductId'] == $item->id  )
                        {
                            $startSlot = $availableSlot['slot'];
                            $startTime = $availableSlot['startTime'];
                            break;
                        }
                    }
                    $newbookingDetails = new ServiceBookingDetails;
                    $newbookingDetails->book_id = $sequencer->id;
                    $newbookingDetails->service_product_id = $item->id    ;
                    $newbookingDetails->service_name = $item->srv_name  ;
                    $newbookingDetails->slot_no = $startSlot; //$availableSlot[$pos]["slot"]  ;
                    $newbookingDetails->status = "new";
                    $newbookingDetails->service_time = date('H:i:s', $startTime );
                    $newbookingDetails->service_charge = $item->actual_price;
                    $newbookingDetails->save();
                    $pos++;
                }


                //updating occupied
                DB::table("ba_service_days")
                ->whereIn("slot_number" , $book_slots )
                ->whereDate("service_date", $service_date)
                ->where("bin", $request->bin )
                ->where("staff_id",  $staff_id )
                ->where("status",  "open" )
                ->update(array("status" => "booked") ) ;
            }
        }
    }

    //upload attachment
    if($request->attachment  != "" )
    {
        $folder_path =  config('app.app_cdn_path')   . "/assets/image/uploads/order_" . $sequencer->id .    "/";
        if( !File::isDirectory($folder_path)){
            File::makeDirectory( $folder_path, 0755, true, true);
        }

        $file =  $request->file('attachment');
        $originalname = $file->getClientOriginalName();
        $extension = $file->extension( );
        $filename =  "attach_" . $sequencer->id . "_" .  time() . ".{$extension}";
        $file->storeAs('uploads',  $filename );
        $imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename ,   $folder_path .  $filename );
        $newbooking->attachment = config('app.app_cdn') . "/assets/image/uploads/order_" . $sequencer->id .    "/" . $filename;
    }
    $return_url = URL::to('/payment/response') ;
    $notifyUrl = '';
    $item_notes = array(                    
      "orderId" => $sequencer->id,
      "orderAmount" => $total_cost,
      "customerName" => $customer_info->fullname,
      "customerPhone" => $customer_info->phone,
      "customerEmail" => $customer_info->email =="" ? 'booktougi@gmail.com' : $customer_info->email,
      "returnUrl" => $return_url,
      "notifyUrl" => $notifyUrl,
  );

    $rzpapi = new Api(  $secure_key->key_id   , $secure_key->key_secret );
    $rzp_response =  $rzpapi->order->create(array
        ('receipt' => $sequencer->id ,
           'amount' => $total_cost * 100 ,
           'currency' => 'INR', 
           'notes'=>  $item_notes                   
       ));

    if(isset($rzp_response->id))
    {
                //logging razorpay order ID
        $update_booking = ServiceBooking::find($sequencer->id);
        $update_booking->rzp_id = $rzp_response->id;
        $update_booking->save();

        return redirect("/booking/payment-in-progress?o=".$sequencer->id ) ;
                // break;
    }
    else
    {
                //open payment failure page

    }
}

return redirect("/shopping/checkout")->with("err_msg", "Your booking is placed successfully.") ;


}


 
    public function updateOrderRemarks(Request $request)
    {

        if($request->key=="" || $request->key==null) {
            return redirect('/shopping/my-orders');
        }
        $orderId = $request->key;
        $booking = ServiceBooking::find($orderId);
        if (!$booking) {
            return redirect("/shopping/my-order-details?key=".$orderId."");
        }

        $booking->cust_remarks = $request->customer_remarks;
        $update = $booking->save();

        if ($update) {
            return redirect("/shopping/my-order-details?key=".$orderId."");
        }
    }


    public function loadChatLogin(Request $request)
  {
    if( $request->login == "")
         {
         $err_msg = "Field missing!";
         $err_code = 9001;
         $data = array( "message" => "failure" , 'status_code' => $err_code , 'detailed_msg' => $err_msg) ;
         return json_encode( $data ); 
         } 
          
                //upload attachment
                        
                        if($request->photo == "undefined" ){
                            $url="";
                        }else
                        {
                            $folder_path =  config('app.app_cdn_path')   . "/assets/image/uploads/order_" . $request->pickupPhone .    "/";
                            if( !File::isDirectory($folder_path)){
                                File::makeDirectory( $folder_path, 0755, true, true);
                            }
                            $file =  $request->file('photo');
                            $originalname = $file->getClientOriginalName();
                            $extension = $file->extension();
                            $filename =  "assist_".time().".{$extension}";
                            $file->storeAs('uploads',  $filename );
                            $imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename,$folder_path .  $filename );
                            $url = config('app.app_cdn')."/assets/image/uploads/order_".$request->pickupPhone."/" . $filename;
                        }
                //end attachment
             

                $assists_task = [$request->login=>[
                                                "taskid"=>$request->taskId,
                                                "pickup_name" => $request->pickupName,
                                                "pickup_phone" =>$request->pickupPhone,
                                                "pickup_address" =>$request->pickupAddress,
                                                "destination_name" =>$request->destName,
                                                "destination_phone"=>$request->destPhone ,
                                                "destination_address"=>$request->destAddress,
                                                "preferred_time"=>$request->preferredTime,
                                                "photo"=>$url ,
                                                "service_fee"=>$request->serviceFee,
                                                "description"=>$request->taskDetails
                                                 ]
                                                ];

                session()->put('assist_log', $assists_task);
              

        //         if ($request->session()->has('login')) {
        //            return redirect("/shopping/my-order-details?key=""")
        //         }else{
        //             return view("store_front.orders.chat_login");
        //              }
        //         }
        //         else{

        //             return view("store_front.orders.chat_login");
        //      }
        // }

         $returnHTML = view('store_front.assist.chat_login')->render();
         $err_status ="success";
         $err_msg = "Action perform";
         $err_code =  7000;
         $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg,
         'html'=>$returnHTML); 
         return json_encode($data);  
  }

    protected function assistCompleteYourRequest(Request $request)
    {


        if ($request->btnTaskWizard=="btnsave") {


                    if($request->session()->has('__user_id_' ) )
                         {
                            $userId  =  $request->session()->get('__user_id_' );
                         }
                        $user_info =  json_decode($this->getUserInfo($userId));


                        $pickupName = $request->pickupName;
                        $pickupPhone = $request->pickupPhone;
                        $pickupAddress = $request->pickupAddress;

                        $destinationName = $request->destinationName;
                        $destinationPhone = $request->destinationPhone;
                        $destinationAddress = $request->destinationAddress;
                        $landmark = "";

                        $assist_fee = DB::table('ba_global_settings')
                        ->where('config_key','assist_fee')
                        ->first();

                        if ($request->servicefee < $assist_fee->config_value) {
                        return redirect("/")->with("err_msg",'Failed to request assist order.');
                        }

                        //echo $request->taskid;die();
                        //saving into the pick and drop database

                        $orderSequencer = new OrderSequencerModel;
                        $new_order = new PickAndDropRequestModel;

                        //order sequencer generating section
                        $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
                        $rp_1 = mt_rand(0, 9);
                        $rp_2 = mt_rand(0, 9);
                        $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;
                        $orderSequencer->type = "assist";
                        $orderSequencer->tracker_session =  $tracker;
                        $save = $orderSequencer->save();

                        if( !isset($save))
                        {
                           return redirect("/")->with("err_msg",  'Failed to generate order no sequence.'  );
                        }
                        //order sequencer ends here

                        //upload attachment
                        
                        if($request->photo!= "" )
                        {
                            $folder_path =  config('app.app_cdn_path')   . "/assets/image/uploads/order_" . $orderSequencer->id .    "/";
                            if( !File::isDirectory($folder_path)){
                                File::makeDirectory( $folder_path, 0755, true, true);
                            }
                            $file =  $request->file('photo');
                            $originalname = $file->getClientOriginalName();
                            $extension = $file->extension();
                            $filename =  "assist_".time().".{$extension}";
                            $file->storeAs('uploads',  $filename );
                            $imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename,$folder_path .  $filename );
                            $url = config('app.app_cdn')."/assets/image/uploads/order_"."/" . $filename;
                            $new_order->pickup_image = $url;
                        }
                        //end attachment


                         $new_order->id = $orderSequencer->id ;
                         $new_order->request_by = $user_info->member_id;
                         $new_order->assist_id = $request->taskid;
                         $new_order->request_from = "customer";
                         $new_order->otp  =   mt_rand(222222, 999999);


                         $new_order->pickup_name = ( $pickupName  != "" ?  $pickupName  : null);
                         $new_order->pickup_phone = ($pickupPhone  != "" ?  $pickupPhone  : null);
                         $new_order->pickup_address = ($pickupAddress  != "" ?  $pickupAddress  : null);
                         $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);



                         $new_order->address = $destinationAddress;
                         $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
                         $new_order->latitude =   0.00 ;
                         $new_order->longitude =  0.00 ;


                         $new_order->drop_name =  preg_replace('/[^A-Za-z0-9\-]/', '', $destinationName);
                         $new_order->drop_phone =   preg_replace('/[^A-Za-z0-9\-]/', '',$destinationPhone);
                         $new_order->drop_address =  $destinationAddress;
                         $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
                         $new_order->drop_latitude =   0.00 ;
                         $new_order->drop_longitude =  0.00 ;

                         $new_order->fullname  = $pickupName; //obsolete
                         $new_order->phone  = $pickupPhone ; //obsolete

                         $new_order->book_status = "new";
                         $new_order->service_fee = $new_order->requested_fee =  $request->servicefee ;
                         $new_order->packaging_cost = ($request->packcost != "" ? $request->packcost : 0.00);
                         $new_order->total_amount = 0 ;
                         $new_order->book_date =  date('Y-m-d H:i:s');
                         $new_order->service_date = date("Y-m-d", strtotime(  $request->itemservicedate));

                         $new_order->pickup_details =  ($request->remarks != "" ? preg_replace('/[^A-Za-z0-9\-]/', ' ',  $request->remarks )   : null);
                         $new_order->source =  ( $request->source != "" ? $request->source : "customer" );


                         $new_order->tracking_session = $tracker;
                         $save=$new_order->save();

                         if($save)
                         {
                            session()->remove('category');
                            $msg = "Order place successfully!";
                            return redirect("/shopping/my-orders")->with('err_msg',$msg);

                         }
                         else
                         {
                            $msg = "failed to place assist order!";
                            return redirect("/shopping/my-orders")->with('err_msg',$msg);
                         }
                    //saving ends here
                    }
    }
    //destination methods ends here
    // .........start..............
    protected function booktouAssist(Request $request)
    {
        $assists_task = DB::table('ba_assist_category')
        ->orderBy('display_order','asc')
        ->get();
         $bot_system = DB::table('ba_bot_chat')
        ->get();
        $data = array('assists'=>$assists_task,'bot'=>$bot_system);
        return view("store_front.booktou_assist")->with($data);

    }
    public function updateAssistOrderRemarks(Request $request)
    {

        if($request->assistkey=="" || $request->assistkey==null) {
            return redirect('/shopping/my-orders');
        }
        $orderId = $request->assistkey;
        $assist = PickAndDropRequestModel::find($orderId);
        if (!$assist) {
            return redirect("/shopping/my-order-details?key=".$orderId."");
        }

        $assist->cust_remarks = $request->customer_remarks;
        $update = $assist->save();

        if ($update) {
            return redirect("/shopping/my-order-details?key=".$orderId."");
        }
    }


    // offer details



    //purchase service offers
    protected function purchaseServiceOffers(Request $request)
    {

         $gateway_mode =  config('app.gateway_mode'); 
      
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );

        if( $request->bin  == "" )
        {
            return redirect("/booking/slot-selector/".$request->serviceProductIds)->with("err_msg", "missing data to perform action!");
        }

        if($request->session()->has('__user_id_' ) )
        {
            $userId  =  $request->session()->get('__user_id_' );
        }
        $user_info =  json_decode($this->getUserInfo($userId));
        

        $customer = CustomerProfileModel::find($user_info->member_id);
        if(!isset($customer))
        {
              return redirect("/booking/slot-selector/".$request->serviceProductIds)->with("err_msg", "NO Customer record found!");
        }

        $address  =  $customer->locality;
        $landmark  =  $customer->landmark;
        $city  =  $customer->city ;
        $state   =  $customer->state;
        $pinCode  =  $customer->pin_code;
        $latitude   =  $customer->latitude;
        $longitude  =  $customer->longitude;



      //save sequencer
            $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
            $rp_1 = mt_rand(0, 9);
            $rp_2 = mt_rand(0, 9);
            $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;

        $business_info = Business::find( $request->bin  ) ;
        if( !isset($business_info) )
        {
             return redirect("/booking/slot-selector/".$request->serviceProductIds)->with("err_msg", "no business info found!");
        }

        $salesorderno = 0;
        $paymode = "online";

            $sequencer = new OrderSequencerModel;
            $sequencer->type = "offers";
            $sequencer->tracker_session =  $tracker;
            $save = $sequencer->save();
            if( !isset($save) )
            {
                 return redirect("/booking/slot-selector/".$request->serviceProductIds)->with("err_msg", "failed to generate order sequence!");
            }
            $salesorderno = $sequencer->id ;


        $new_order = new ServiceBooking ;
        $new_order->id =  $salesorderno ;
        $new_order->bin = $request->bin;
        $new_order->book_category = $business_info->category;
        $new_order->book_by = $user_info->member_id;
        $new_order->staff_id = 0;
        $new_order->otp =  $new_order->delivery_otp =  mt_rand(222222, 999999);
        $new_order->customer_name = $customer->fullname;
        $new_order->customer_phone = $customer->phone;
        $new_order->address = $address;
        $new_order->landmark = $landmark;
        $new_order->city = $city;
        $new_order->state = $state;
        $new_order->pin_code  = $pinCode;
        $new_order->book_status = "new";
        $new_order->total_cost =  $request->totalCost ;
        $new_order->delivery_charge = 0.00;
        $new_order->agent_payable  =  0.00;
        $new_order->service_fee  = 0.00;
        $new_order->cashback =  0;
        $new_order->seller_payable =  $request->totalCost ;
        $new_order->payment_type =  $paymode;
        //$new_order->transaction_no =  ( $request->transactionId == "" ) ? $request->transactionId :  null;
        $new_order->latitude = $latitude;
        $new_order->longitude =  $longitude;
        $new_order->book_date =  date('Y-m-d H:i:s');
        $new_order->service_date =   date('Y-m-d H:i:s');   ; //since it is offer we don't have service date
        $new_order->preferred_time =  null; //since it is offer we don't have service date
        $new_order->cust_remarks =  ($request->custRemarks != "" ? $request->custRemarks : null);
        $new_order->transaction_no =  ($request->transactionId  != "" ? $request->transactionId : "na");
        $new_order->reference_no =  ($request->referenceNo  != "" ? $request->referenceNo : "na");
        $new_order->tracking_session = $tracker ;
        $save=$new_order->save();
        
        $item_notes = array();
        if($save)
        {
            $new_order = ServiceBooking::find($salesorderno); //auto increment has removed so need this line to fetch order.
            $orderStatus = new OrderStatusLogModel;
            $orderStatus->order_no = $salesorderno;
            $orderStatus->order_status = $new_order->book_status;
            $orderStatus->log_date = date('Y-m-d H:i:s');
            $orderStatus->save();
          //logs ends here
          $serviceProductIds  = $request->serviceProductIds;
          $productQty  = $request->productQty;


          for($i=0; $i < count($serviceProductIds) ; $i++ )
          {
            $serviceProductIds[$i] = trim( $serviceProductIds[$i] );
          }

          //Fetch product details
          $service_products = DB::table("ba_service_products")
          ->whereIn("id", $serviceProductIds)
          ->get();

          $totalItems= count( $serviceProductIds );
          $productName=$productPrice=$productCategory=array();
          $item_cost = 0;

          foreach ($service_products as $value)
          {

             for($i=0; $i < $totalItems ; $i++ )
             {
                if($value->id ==  $serviceProductIds[$i] )
                {
                    //ading items
                      $items = new ShoppingBasketServiceProductModel;
                      $items->order_no =  $salesorderno;
                      $items->srvprid = $value->id;
                      $items->srv_name = $value->srv_name;
                      $items->srv_details = $value->srv_details;
                      $items->qty  = $productQty[$i];
                      $items->price  = $value->actual_price;
                      $items->remarks  =  $request->custRemarks == "" ? null : $request->custRemarks;
                      $items->category_name  = $value->service_category;
                      $items->save();
                      $item_cost += $value->actual_price * $productQty[$i];
                      break;
                }
             }
          }

            $new_order->seller_payable   = $item_cost  ;
            $new_order->total_cost  =  $item_cost  ;
            $new_order->save();

              
                     $notifyUrl = '';
                     $item_notes = array(                    
                      "orderId" => $salesorderno,
                      "orderAmount" => $item_cost,
                      "customerName" => $member->fullname,
                      "customerPhone" => $member->phone,
                      "customerEmail" => $member->email=="" ? 'booktougi@gmail.com' : $member->email,
                      "returnUrl" => $return_url,
                      "notifyUrl" => $notifyUrl,
                    );

                $rzpapi = new Api(  $secure_key->key_id   , $secure_key->key_secret );
                $rzp_response =  $rzpapi->order->create(array
                    ('receipt' => $sequencer->id ,
                     'amount' => $total_to_pay * 100 ,
                     'currency' => 'INR', 
                     'notes'=>  $item_notes                   
                 ));
                    
                 if(isset($rzp_response))
                 {
                    //logging razorpay order ID
                    $new_order->rzp_id = $rzp_response->id;
                    $new_order->save();

                    return redirect("/booking/payment-in-progress?o=".$salesorderno ) ;
                    
                 }
                 else
                 {
                    return redirect("/booking/slot-selector/".$request->serviceProductIds)->with('Your order could not be placed. Please contact sales');
                 }
        }
         return redirect("/shopping/my-bookings")->with("err_msg", "Your booking is placed successfully.") ;
    }


protected function paymentPending(Request $request)
{  
  $pending_orders = ServiceBooking::whereDate("service_date",  date('Y-m-d') )
    ->where("payment_status",  "pending" )
    ->whereIn("payment_type", array('online', 'Pay online (UPI)') )
    ->whereNotIn("book_status", array('canceled','cancelled'  ))
    ->get();

   $order_key = json_decode($this->pendingPayments($pending_orders));

    foreach($pending_orders as $order)
        {
         
            if($order->payment_status == "pending")
            {      
                $message_row = DB::table("ba_sms_templates")
                ->where("name", "payment_link_sms")
                ->first();

                if(isset($message_row))
                {

                $pending_msg = urlencode(str_replace("#param1",$order->id,$message_row->message));
                $this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id,array($order->id),$pending_msg  ); 

                $pending_msg = urlencode(str_replace("#param2","URL::to('/booking/payment-in-progress?o='.$order->id)",$pending_msg));
                $this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id,array($order->id),$pending_msg  );
 
                } 
            }
        }
    }


protected function filter(Request $request)
{
    return view("store_front.filter");
}
    




  protected function cancelShoppingOrder(Request $request)
  {
        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        if( $request->key  == "")
        {
            return redirect("/shopping/my-orders")->with("err_msg","Please select an ordert to view it!");
        }

        $oid = $request->key;
        $uid = $request->session()->get('__user_id_' );

        //customer as member
        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_users.id",  $uid  )
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
           return redirect("/shopping/login")->with("err_msg", "Please login to proceed!");
        }

        $ordersequence = OrderSequencerModel::find($oid);
        if(!isset($ordersequence ))
        {
           return redirect("/shopping/checkout")->with("err_msg", "Order not found!");
        }

        $ordertype = $ordersequence->type;

        if ( strcasecmp($ordersequence->type,  "normal" )  != 0 )  
        {
          return redirect("/shopping/my-order-details?key=" .  $oid )->with("err_msg", "Order type mismatched!");
        }
        //cart details
        $booking_info  = ServiceBooking::where("book_by",  $member->id )
        ->where("id",  $oid )
        ->first();

        if(!isset($booking_info))
        {
          return redirect("/shopping/my-orders")->with("err_msg", "No orders found!");
        } 
  
        $booking_info->book_status = "cancelled";
        $booking_info->save();
        return redirect("/booking/view-order-details?key=" .  $oid )->with("err_msg", "Booking status updated!");

    }


}
