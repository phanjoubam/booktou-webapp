<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\BusinessTaxDetails;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function beta()
    {
        echo "here";
    }

     public function aboutUs()
    {
       return view("about_booktou") ;
    }

     public function webAboutUs()
    {
       return view("web_about_booktou") ;
    }

    public function joinUs()
    {
       return view("join_booktou") ;
    }

    public function privacyPolicy()
    {
       return view("privacy_policy") ;
    }
    public function webPrivacyPolicy()
    {
       return view("web_privacy_policy");
    }

    public function termsOfUse()
    {
       return view("terms_conditions") ;
    }

    public function webTermsOfUse()
    {
       return view("web_terms_conditions") ;
    }

    public function cancellationAndRefund()
    {
       return view("cancellation_and_refund");
    }
    public function webCancellationAndRefund()
    {
       return view("web_cancellation_and_refund");
    }

    public function shippingAndDelivery()
    {
       return view("shipping_and_delivery");
    }
    public function webShippingAndDelivery()
    {
       return view("web_shipping_and_delivery");
    }

    public function downloadPrinterDriver()
    {

        ///var/www/html/booktou.in/public_html/public/driver
       return view("download_driver") ;
    }




    public function merchantAgreementForm()
    {
       return view("merchant_agreement") ;
    }

    public function saveMerchantAgreementForm(Request $request)
    { 

        if(isset($request->submit) && $request->agree == "on")
        {

            $loginState =   Auth::attempt([ 
                'phone' => $request->priphone , 
                'password' => $request->password  , 
                'category' => 1 ]); 


             if($loginState)
             {

                $user = Auth::user();

                if( $user->category ==  1 )  
                {

                    //update business profile 
                    $biz_tax_info = BusinessTaxDetails::where("bin", $user->bin ) 
                    ->first();

                    if( !isset( $biz_tax_info )  )
                    {
                        $biz_tax_info = new BusinessTaxDetails; 
                    }

                   $biz_tax_info->bin  =$user->bin;
                   $biz_tax_info->biz_name  =$request->business;
                   $biz_tax_info->owner_name  =$request->name;
                   $biz_tax_info->biz_address  =$request->address;
                   $biz_tax_info->fssai  = ($request->fssai == "" ? null : $request->fssai );   
                   $biz_tax_info->gstin  = ($request->gstin == "" ? null : $request->gstin );  
                   $biz_tax_info->pan  = $request->pan;
                   $biz_tax_info->regdno  = ($request->regdno == "" ? null : $request->regdno );
                   $biz_tax_info->save();

                   Auth::logout();
                   $request->session()->flush();
                   return redirect("/merchant-agreement#taxdetails")->with("err_msg",  "Merchant taxation information updated.");

                } 
                else 
                { 
                  Auth::logout();
                  $request->session()->flush();
                  return redirect("/merchant-agreement#taxdetails")->with("err_msg",  "This page is only for registered merchant.");
                }
      
              }
              else
              {
                return redirect("/merchant-agreement#taxdetails")
                ->with("err_msg",  "To update merchant agreenment, please provide registered account correctly.");
              } 

          }  
    }



     public function pageNotFound()
    {
       return view("error.404") ;
    }

}
