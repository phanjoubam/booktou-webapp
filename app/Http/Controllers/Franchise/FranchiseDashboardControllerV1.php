<?php

namespace App\Http\Controllers\Franchise; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;


use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\OrderRemarksModel;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\SalesTargetModel;
use App\ProductPhotosModel;

use App\Exports\FranchiseMonthlyAgentsOrdersExport;
use App\Exports\FranchiseDailyAgentsOrdersExport;
use App\Theme;

use App\Traits\Utilities; 
use PDF;

class FranchiseDashboardControllerV1  extends Controller
{
    use Utilities;
  
    public function dashboardOld(Request $request)
    {

      $frno = $request->session()->get('_frno');
        
      $status = array('new', 'confirmed','in_queue',  'order_packed', 'in_route', 'delivered', 'completed',  'package_picked_up','delivery_scheduled' ) ; 
      $today = date('Y-m-d'); 
 
       


      $sales_summary = DB::table("ba_service_booking") 
      ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
      ->where("ba_business.frno",  $frno  )
      ->whereRaw("date(ba_service_booking.service_date) = '$today' " )  
      ->whereIn("ba_service_booking.book_status",  array( 'delivered', 'completed' )  )    
      ->select( DB::Raw("sum(ba_service_booking.total_cost) as totalSold"),  
        DB::Raw("sum(ba_service_booking.delivery_charge) as conveyance"), 
        DB::Raw("sum(ba_service_booking.agent_payable) as agentPayable"), 
        DB::Raw("sum(ba_service_booking.service_fee) as serviceFee")   )
      ->first(); 

  
    
 
       
      $total_pick_drop = DB::table("ba_pick_and_drop_order") 
      ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
      ->whereRaw("date(service_date) >= '$today' " )  
      ->whereIn("book_status", $status)
      ->where("ba_pick_and_drop_order.request_from", 'business')
      ->select("ba_pick_and_drop_order.*") 
      ->get(); 
     
      $new_signup = DB::table("ba_profile") 
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->whereRaw("date(ba_users.created_at) = '$today' " ) 
      ->where("ba_users.franchise",    $frno )
      ->select("ba_profile.*", "ba_users.id as userId", "ba_users.created_at", "ba_users.otp")
      ->get(); 


      $new_business = DB::table("ba_business") 
      ->join("ba_users", "ba_users.bin", "=", "ba_business.id")
      ->where("ba_business.frno",  $frno )
      ->whereRaw("date(ba_users.created_at) = '$today' " ) 
      ->select("ba_business.*", "ba_users.id", "ba_users.created_at")
      ->get();


      $active_users =  DB::table("ba_profile") 
      ->join("ba_active_user_log", "ba_active_user_log.member_id", "=", "ba_profile.id")
      ->whereRaw("date(ba_active_user_log.log_date) = '$today'" )  
      ->select("ba_profile.id", "ba_profile.fullname", DB::Raw("count(*) as openCount") )
      ->groupBy("ba_profile.id")
      ->get();


      $active_agents  = DB::table("ba_business") 
      ->join("ba_users", "ba_users.bin", "=", "ba_business.id")
      ->whereRaw("date(ba_users.created_at) = '$today' " ) 
      ->where("ba_business.frno",  $frno )
      ->select("ba_business.*", "ba_users.id", "ba_users.created_at")
      ->get();
      
 
      $report_date =  date('Y-m-d', strtotime("-10 days")); 
 
      $pnd_sales_chart_data = DB::table("ba_pick_and_drop_order")
      ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
      ->whereDate("service_date", ">=", $report_date )
      ->where("book_status", "delivered" ) 
      ->where("ba_pick_and_drop_order.request_from", 'business')
      ->where("ba_business.frno",  $frno  )
      ->select( DB::Raw("date( service_date ) as serviceDate"), DB::Raw("count(*) as pndOrders")  )
      ->groupBy("serviceDate")
      ->get(); 
  

      $normal_sales_chart_data = DB::table("ba_service_booking")
      ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
      ->whereDate("service_date", ">=", $report_date ) 
      ->where("book_status", "delivered" )
      ->where("ba_business.frno",    $frno  ) 
      ->select( DB::Raw("date( service_date ) as serviceDate"), DB::Raw("count(*) as normalOrder")  )
      ->groupBy("serviceDate")
      ->get(); 
 

      $pnd_sales_earning_data = DB::table("ba_pick_and_drop_order")
      ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
      ->whereDate("service_date", ">=", $report_date )
      ->where("book_status", "delivered" )
      ->where("request_from", "business" ) 
      ->where("ba_business.frno",  $frno )
      ->select( DB::Raw("date(ba_pick_and_drop_order.service_date ) as serviceDate"), DB::Raw("sum(ba_pick_and_drop_order.service_fee) as totalRevenue")  )
      ->groupBy("serviceDate")
      ->get();
 

      $migrated_users   = DB::table("ba_profile") 
      ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.phone", "=", "ba_profile.phone") 
      ->select("ba_profile.*" )
      ->orderBy("id", "desc")
      ->paginate(10);


      $sales_target   = DB::table("ba_sales_target")   
      ->where("month", date('m'))
      ->where("year", date('Y'))
      ->where("frno", $frno) 
      ->first();
 
  

      if( $request->session()->get('_user_role_' ) == "1000" )
      {
        return view("franchise.dashboard_cc")->with("data",
        array(
        
        "active_users" => $active_users,
        "sales" => $sales_summary,   
        'new_customers' => $new_signup, 
        'new_business' => $new_business,
        'pnd_sales_chart_data' => $pnd_sales_chart_data,
        'normal_sales_chart_data' => $normal_sales_chart_data,
        'pnd_sales_earning_data' => $pnd_sales_earning_data,
        'migrated_users' => $migrated_users ,
        'sales_target' =>$sales_target ,
        'pnd_income_achieved' =>  0
      ) ); 
      }
      else 
      {
        return view("franchise.dashboard_franchise")->with("data",
        array( 
        "active_users" => $active_users,
        "sales" => $sales_summary,   
        'new_customers' => $new_signup, 
        'new_business' => $new_business,
        'pnd_sales_chart_data' => $pnd_sales_chart_data,
        'normal_sales_chart_data' => $normal_sales_chart_data,
        'pnd_sales_earning_data' => $pnd_sales_earning_data,
        'migrated_users' => $migrated_users ,
        'sales_target' => $sales_target,
        'pnd_income_achieved' => 0
      ) ); 
    }     
  }
  
  
  
  public function dashboard(Request $request)
  {
  
 

    $frno = $request->session()->get('_frno'); 
    $status = array('new', 'confirmed','in_queue',  'order_packed', 'in_route', 'delivered', 'completed',  'package_picked_up','delivery_scheduled' ) ; 

    $today = date('Y-m-d'); 
    $report_date =  date('Y-m-d', strtotime("-10 days")); 

    $sales_summary = DB::table("ba_service_booking") 
    ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
    ->where("ba_business.frno",  $frno  )
    ->whereDate("ba_service_booking.service_date", $today )    
    ->select( "ba_service_booking.*")
    ->get();

    $actual_pnd_orders = DB::table("ba_pick_and_drop_order") 
    ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
    ->whereDate("service_date",  $today )   
    ->whereIn("request_from", array ('business'))
    ->where("ba_business.frno",$frno)
    ->select("ba_pick_and_drop_order.*")  ;

    $pnd_orders = DB::table("ba_pick_and_drop_order") 
    ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
    ->whereDate("service_date",  $today )   
    ->whereIn("request_from", array ('business'))
    ->where("ba_pick_and_drop_order.route_to_frno",$frno)
    ->union($actual_pnd_orders)
    ->select("ba_pick_and_drop_order.*") 
    ->get();




    $assist_orders = DB::table("ba_pick_and_drop_order") 
    ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
    ->whereDate("ba_pick_and_drop_order.service_date", $today )   
    ->whereIn("request_from", array ('customer' ) )

    ->whereIn("ba_profile.id", function($query) use($frno){
        $query->select("profile_id")->from("ba_users")
        ->where("franchise", $frno);
      })
    ->select("ba_pick_and_drop_order.*") 
    ->get();


    $new_signup = DB::table("ba_profile") 
    ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
    ->whereDate("ba_users.created_at", $today )  
    ->where("ba_users.franchise",$frno)
    ->select("ba_profile.*", "ba_users.id as userId", "ba_users.created_at", "ba_users.otp")
    ->get(); 


    $new_business = DB::table("ba_business") 
    ->join("ba_users", "ba_users.bin", "=", "ba_business.id")
    ->where("ba_business.frno",  $frno )
    ->whereDate("ba_users.created_at", $today )   
    ->select("ba_business.*", "ba_users.id", "ba_users.created_at")
    ->get();
    

    $pnd_sales_chart_data = DB::table("ba_pick_and_drop_order")
    ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")  
    ->whereDate("service_date", ">=", $report_date )
    ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
    ->whereIn("request_from", array ('business', 'customer'))
    ->where("ba_business.frno", $frno)
    ->select( DB::Raw("date(service_date) as serviceDate"), DB::Raw("count(*) as pndOrders"), DB::Raw("sum(service_fee) as totalRevenue")  )
    ->groupBy("serviceDate")
    ->get();

    
    $normal_sales_chart_data = DB::table("ba_service_booking") 
    ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
    ->where("ba_business.frno",  $frno  )
    ->whereDate("service_date", ">=", $report_date )  
    ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
    ->select( DB::Raw("date(service_date) as serviceDate"), DB::Raw("count(*) as normalOrder")  )
    ->groupBy("serviceDate")
    ->get(); 

    
    $active_users =  DB::table("ba_profile") 
    ->join("ba_active_user_log", "ba_active_user_log.member_id", "=", "ba_profile.id") 
    ->whereDate("ba_active_user_log.log_date", $today )  
    ->whereIn("ba_profile.id", function($query) use($frno){ 
       $query->select("profile_id")
       ->from("ba_users")
       ->where("franchise", $frno);

    })
    ->select("ba_profile.id", "ba_profile.fullname", DB::Raw("count(*) as openCount") )
    ->groupBy("ba_profile.id")
    ->get();


    $pnd_income_achieved = DB::table("ba_pick_and_drop_order")
    ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
    ->whereIn("request_from", array('business'))
    ->where("ba_business.frno",$frno)   
    ->whereMonth("service_date", date('m'))  
    ->whereYear("service_date", date('Y'))   
    ->where("book_status", "delivered") 
    ->select(DB::Raw("sum(total_amount) as totalAmount")) 
    ->first();


    $assits_income_achieved = DB::table("ba_pick_and_drop_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
      ->whereIn("request_from", array ('customer' ))
      ->whereIn("ba_profile.id", function($query) use($frno){
            $query->select("profile_id")->from("ba_users")
            ->where("franchise", $frno);

    })
      ->whereIn("book_status", array('completed','delivered')) 
      ->whereMonth("service_date", date('m'))  
      ->whereYear("service_date", date('Y'))  
      ->select(DB::Raw("sum(total_amount) as totalAmount")) 
      ->first();

      $normal_income_achieved = DB::table("ba_service_booking") 
      ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
      ->where("ba_business.frno",  $frno  )  
      ->whereMonth("service_date", date('m') )  
      ->whereYear("service_date", date('Y') )   
      ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
      ->select(DB::Raw("sum(total_cost) as totalAmount")) 
      ->first();

      $sales_target   = DB::table("ba_sales_target")   
      ->where("month", date('m'))
      ->where("year", date('Y'))
      ->where("frno",$frno)
      ->first();


      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

      
  return view("franchise.dashboard_franchise")->with( 
      array( 
      'new_customers' => $new_signup, 
      'new_business' => $new_business,
      'normal_orders' => $sales_summary,
      'pnd_orders' => $pnd_orders,
      'assits_orders' => $assist_orders,
      'active_users' => $active_users,
      'pnd_sales_chart_data' => $pnd_sales_chart_data,
      'normal_sales_chart_data' => $normal_sales_chart_data,
      'sales_target' =>$sales_target,
      'pnd_income_achieved'=>($pnd_income_achieved == null) ? 0.00 : $pnd_income_achieved->totalAmount,
      'normal_income_achieved'=>($normal_income_achieved == null) ? 0.00 : $normal_income_achieved->totalAmount,
      'assits_income_achieved'=>($assits_income_achieved == null) ? 0.00 : $assits_income_achieved->totalAmount,
      "theme"=>$theme,
      "report_date"=>$report_date,
      "todayDate"=>$today
    )); 
      
}



  public function logoutFranchise(Request $request)
  {
      Auth::logout(); 
      $request->session()->flush(); 
      return redirect('/franchise/login');
  }

  


  protected function viewListedBusinesses(Request $request)
  {

      $frno = $request->session()->get('_frno'); //reading session variable where franchise number is stored

      $business= array();
      if( isset($request->btnSearch) )
      {

          $business= DB::table("ba_business")
          ->whereRaw("( frno = '" . $frno  .  "' ) and " . 
          " (city like '%" . $request->search_key . "%'  or name like '%" . $request->search_key . "%' or tags like '%" . $request->search_key . "%' ) ") 
          ->paginate(20); 
 
       }
       else 
       {

         $business= DB::table("ba_business") 
         ->where("frno", $frno )
        ->paginate(20); 

       }

      $bins = array();
      foreach( $business as $item)
      {
        $bins[] =  $item->id;
      }

      $bins[]=0;

      $promo_businesses= DB::table("ba_premium_business")
      ->whereIn("bin",$bins) 
      ->get();

      $qr_menu= DB::table("ba_business_menu")
      ->whereIn("bin", $bins) 
      ->select("bin", DB::Raw("count(*) as pageCount"))
      ->groupBy("bin")
      ->get();

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();
 

       $data = array('results' => $business, 'promo_businesses'=>$promo_businesses, 'qr_menu' => $qr_menu,'theme'=>$theme);
       return view("franchise.business.business_list")->with('data',$data)->with('theme',$theme);
    
  }


   public function saveBusinessPromotion(Request $request) 
   {
      if( isset($request->btnSave) && $request->btnSave =="save")
      {  
      
       $businessId  = $request->bin;
      // return json_encode($businessId);

       $business_info =  Business::find($businessId);

       if (!$business_info) 
       {

         return redirect("/franchise/businesses/view-all")->with("err_msg", "No business record found!." ); 
       }

       $prem_busi_info = PremiumBusiness::where('bin', $business_info->id)
       ->first();

      
        if( !isset($prem_busi_info))
        {
          $prem_busi_info = new PremiumBusiness; 
        } 
        
        $prem_busi_info->bin  = $business_info->id; 
        $prem_busi_info->start_from  = date('Y-m-d H:i:s'); 
        $prem_busi_info->ended_on = date("Y-m-d H:i:s",strtotime("+30 day"));
        $prem_busi_info->save();
        return redirect("/franchise/businesses/view-all")->with("err_msg", "Business promo updated successfully." );

    }
    else
    {
      return redirect("/franchise/businesses/view-all")->with("err_msg", "Business promo update failed!." );

    }


   }


  public function updateBusinessVisibility(Request $request) 
  {
      if( isset($request->btnSave) && $request->btnSave =="save")
      {  
        $businessId  = $request->bin; 
        $business_info =  Business::find($businessId);
        if (!$business_info) 
        {
          return redirect("/franchise/businesses/view-all")->with("err_msg", "No business record found!." ); 
        }
        $business_info->limit_by_distance  = $request->dbvisibility;  
        $business_info->save();
        return redirect("/franchise/businesses/view-all")->with("err_msg", "Business visibility updated successfully." );

    }
    else
    {
      return redirect("/franchise/businesses/view-all")->with("err_msg", "Business visibility update failed!." ); 
    } 

   }



  protected function viewAllOrders(Request $request)
  {

      $frno = $request->session()->get('_frno'); 


      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)) ; 
      }
      else 
      {
        $orderDate = date('Y-m-d' , strtotime("7 days ago"))  ; 
      }
        

      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );


        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new','confirmed', 'cancel_by_client', 'canceled', 'delivered', 
                'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
                'package_picked_up','delivery_scheduled' ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
            $status = array( 'new','confirmed', 'cancel_by_client', 'canceled', 'delivered', 
            'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
            'package_picked_up','delivery_scheduled' ) ; 
        }

      }
      else 
      {

        $request->session()->remove('_last_search_date' );

        $status = array( 'new','confirmed',  'order_packed','pickup_did_not_come','in_route',  'package_picked_up','delivery_scheduled' ) ; 
      }

      
     $first_query =  DB::table("ba_service_booking")
      ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
      ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
          $query->select("id")->from("ba_business")
          ->where("frno",  $frno  );

      }) 
      ->whereRaw("date(ba_service_booking.service_date) >= '$orderDate'")
      ->whereIn("ba_service_booking.book_status",$status)  
      ->orderBy("ba_service_booking.service_date", "asc")
      ->select("ba_service_booking.*", "ba_profile.fullname", "ba_profile.phone" ) ;


   

       $day_bookings=  DB::table("ba_service_booking")
        ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
        ->where("ba_service_booking.route_to_frno",  $frno  )  
        ->whereRaw("date(ba_service_booking.service_date) >= '$orderDate'")
        ->whereIn("ba_service_booking.book_status",$status)  
        ->orderBy("ba_service_booking.service_date", "asc")
        ->union($first_query)
        ->select("ba_service_booking.*", "ba_profile.fullname", "ba_profile.phone" )
        ->get();


 

      $all_bins = array();
      $book_by = array();
      $phone_nos = array();
      foreach($day_bookings as $bitem )
      {
        $all_bins[] = $bitem->bin; 
        $book_by[] = $bitem->book_by;
        $phone_nos[] =$bitem->customer_phone;
      }

       $flag_customers = DB::table("ba_ban_list")
      ->whereIn("phone", $phone_nos)    
      ->get();

      $all_bins = array_filter($all_bins);

      $all_businesses = DB::table("ba_business")
      ->whereIn("id", $all_bins)    
      ->get();


      $order_statuses = array(   'completed', 'order_packed' , 'in_route','delivered' , 'package_picked_up','delivery_scheduled' );

      $book_by = array_filter($book_by);

       $book_status = array( 'cancel_by_owner','pickup_did_not_come', 'cancel_by_client',  'canceled','order_packed' ) ; 
 
 
      $book_counts = DB::table("ba_service_booking")
      ->whereIn("book_by", $book_by)    
      ->whereNotIn("book_status", $book_status)  
      ->select("book_by", DB::raw('count(*) as totalOrders') )
      ->groupBy("book_by")
      ->get(); 
 
     
        //finding free agents  
        $today = date('Y-m-d'); 

        $all_agents = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 
        ->where("ba_users.category", 100)
        ->where("ba_users.status", "<>" , "100") 
        ->where("ba_users.franchise", $frno)
        ->where("ba_profile.fullname", "<>", "" )
        ->get();


        $delivery_orders = DB::table("ba_delivery_order")
        ->whereRaw(" date(accepted_date) = '$today'" )
        ->where("completed_on", null )
         ->where("order_type",  'client order'  )
        ->select("member_id", DB::Raw("count(*) as cnt") )
        ->groupBy('member_id')
        ->get();

        $free_agents = array();


        //Fetching assigned orders
        $delivery_orders_list = DB::table("ba_delivery_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
        ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
        ->whereRaw(" date(accepted_date) ='$today'" )
        ->get(); 
 
   
        foreach ($all_agents as $item)
        {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           }



           //image compression
            if($item->profile_photo != null )
            {
              $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

              if(file_exists(  $source ) &&  !file_exists( $destination ))
              {
                  $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();


       $data = array( 
        'title'=>'Manage Orders' ,
        'results' => $day_bookings, 
        'date' => $orderDate,
        'flag_customers' => $flag_customers, 
        'all_agents' => $all_agents , 
        'delivery_orders_list' => $delivery_orders_list, 
        'all_businesses' =>$all_businesses, 
        'book_counts' => $book_counts,
        'last_keyword' => $last_keyword, 
        'last_keyword_date' =>  $last_keyword_date , 
        'refresh' => 'true');

         return view("franchise.orders.normal_orders_list")
         ->with('data',$data)
         ->with('theme',$theme);
    }





    protected function viewOrderDetails(Request $request)
    {
        $cdn_url =   config('app.app_cdn')    ;  
        $cdn_path =  config('app.app_cdn_path')  ; 
        $cms_base_url =  config('app.app_cdn')   ;  
        $cms_base_path = $cdn_path  ;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";
        $orderno = $request->orderno;
        $userid= $request->session()->get('__user_id_');
        $frno = $request->session()->get('_frno'); 
        if($orderno == "")
        {
          return redirect("/franchise/orders/normal-orders"); 
       }
 
      $order_info   = DB::table("ba_service_booking") 
      ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
      ->select("ba_service_booking.*", "ba_business.name as businessName","ba_business.locality as businessLocality", 
        "ba_business.landmark as businessLandmark", "ba_business.city as businessCity", 
        "ba_business.state as businessState", "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",  "ba_business.banner" )
      ->where("ba_service_booking.id", $orderno )    
      ->first() ;

      if( !isset($order_info))
      {
        return redirect("/franchise/orders/normal-orders"); 
      }

      $order_items   = DB::table("ba_shopping_basket")  
      ->join("ba_products", "ba_products.pr_code", "=", "ba_shopping_basket.pr_code")
      ->where("ba_shopping_basket.order_no", $orderno )    
      ->select("ba_shopping_basket.*" , DB::Raw("'na' photos"))      
      ->get() ;
     

      if( count ($order_items) == 0)
      {
        $order_items   = DB::table("ba_shopping_basket")   
        ->join("ba_product_variant", "ba_product_variant.prsubid",  "=", "ba_shopping_basket.prsubid")
        ->where("ba_shopping_basket.order_no", $orderno )    
        ->select("ba_shopping_basket.*" , DB::Raw("'na' photos"))
        ->get() ;
      }  

      $customer_info   = DB::table("ba_profile") 
      ->where("id", $order_info->book_by )    
      ->first() ;

      
      if( !isset($customer_info))
      {
        return redirect("/franchise/orders/normal-orders"); 
      }


      $agent_info=  DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
      ->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
      ->where("order_no", $orderno  )
      ->where("order_type",  'client order'  )
      ->first();


      $bin = $order_info->bin;
      $today = date('Y-m-d');
      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        }) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", $frno)
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();
      /* processing photos */
      
      $subids =  array(); 
      foreach( $order_items as $item )
      {
          $subids[] = $item->prsubid;
      }
      $subids[] =0; 



      $product_images =  DB::table('ba_product_photos') 
      ->whereIn('prsubid', $subids)
      ->select( "prsubid", "image_url" )   
      ->get();

      $image_url = "https://cdn.booktou.in/assets/image/no-image.jpg"; 
      foreach($order_items as $order_item)
      {
        foreach($product_images as $image )
        {
          if( $order_item->prsubid == $image->prsubid)
          {
            $image_url = $image->image_url ; 
            break;
          } 
        }
        $order_item->photos =  $image_url ; 
      }  

      /* photo processing ends */

       $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();
        
      $result = array(
        'order_info' => $order_info , 
        'customer' =>  $customer_info, 
        'order_items' => $order_items, 
        'agent_info' => $agent_info,
        'all_agents' =>$all_agents );
      return view("franchise.orders.normal_order_detailed_view")->with('data',$result)->with("theme",$theme);


     }

     // added by sanatomba
     protected function updateNormalOrderPrice(Request $request)
     {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/franchise/orders/normal-orders"); 
        }

        if(   $request->remarks == ""  )
        {
          return redirect("/franchise/order/view-details/" . $orderno )->with("err_msg", "Remarks missing!"); 
        }


        if( $request->price == ""   )
        {
          return redirect("/franchise/order/view-details/" . $orderno )->with("err_msg", "Order amount missing!"); 
        }


        $order_info   = ServiceBooking::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/franchise/order/view-details")->with("err_msg", "No matching order found!"); 
        }

        $order_info->seller_payable =  $request->price ;  
        $order_info->total_cost = $request->price  ;  
        $order_info->save();


        $remark = new OrderRemarksModel();
        $remark->order_no =$orderno;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "Price Correction." ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();  
 
   
         return redirect("/franchise/order/view-details/" . $orderno )->with("err_msg", "Order status updated!"); 


     }

     protected function viewCompletedOrders(Request $request)
    { 

      $frno = $request->session()->get("_frno");


      $status = array(  'completed', 'delivered', 
        'cancel_by_client','cancel_by_owner', 
        'pickup_did_not_come', 'delivered', 'returned', 'canceled') ; 
      
      if( isset($request->btn_search) )
      {
        $orderDate = date('Y-m-d', strtotime($request->orderdate)); 
      }
      else 
      {
        $orderDate = date('Y-m-d'); 
      }

      
      $day_bookings= DB::table("ba_service_booking")
      ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
      ->whereRaw("date(ba_service_booking.service_date)  ='$orderDate'") 
      ->whereIn("ba_service_booking.book_status",$status) 
      ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
          $query->select("id")->from("ba_business")
          ->where("frno",  $frno  );

      })
      ->orderBy("ba_service_booking.service_date", "asc")
      ->select("ba_service_booking.*", "ba_profile.fullname", "ba_profile.phone", 
       DB::Raw("'na'  agentName"),
       DB::Raw("'na'  agentPhone"),
       DB::Raw("'na'  profile_photo")  ,

        DB::Raw("'na'  businessName"),
       DB::Raw("'na'  businessPhone"),
       DB::Raw("'na'  businessBanner")  
       )
      ->get();

     
        //finding free agents  
        $all_agents = DB::table("ba_profile")
        ->join("ba_delivery_order", "ba_delivery_order.member_id", "=", "ba_profile.id")
        ->select( "ba_profile.id", "ba_profile.fullname",   "ba_profile.phone", "ba_profile.profile_photo" , "ba_delivery_order.order_no" )  
        ->where("order_type", "client order")
        ->whereIn("ba_profile.id", function($query) use ($frno) {
          $query->select("profile_id")->from("ba_users")
          ->where("franchise",  $frno  );

        })
        ->get();
 
         
   
        foreach ($all_agents as $item)
        {
            
           //image compression
            if($item->profile_photo != null )
            {
              $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

              if(file_exists(  $source ) &&  !file_exists( $destination ))
              {
                  $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
          

      $bins = array(0);
      foreach($day_bookings as $bitem)
      {
        $bins[] = $bitem->bin;

        foreach ($all_agents as $item)
        {    
            if($bitem->id == $item->order_no)
            { 
               $bitem->agentName = $item->fullname;
               $bitem->agentPhone = $item->phone;
               $bitem->profile_photo =    $item->profile_photo;

              break;
            }

          }
         }

         $businesses = DB::table("ba_business") 
        ->whereIn( "id", $bins)  
        ->get(); 

 

       $data = array( 'title'=>'Manage Orders' ,'results' => $day_bookings, 'date' => $orderDate, 'all_agents' => $all_agents, 'businesses' =>$businesses  );

       $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();
         return view("franchise.orders.completed_normal_orders_table")->with('data',$data)
         ->with('theme',$theme);
    }
 
 


    protected function viewPickAndDropOrders(Request $request)
    {

      $frno = $request->session()->get("_frno");

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)); 
        $todayDate = date('Y-m-d');
      }
      else 
      {
  
        $todayDate = date('Y-m-d'); 
        $orderDate = date('Y-m-d' , strtotime("15 days ago"))  ;  
      }
        
  

      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );
        $dbstatus = $request->filter_status; 
        if( $dbstatus != "")
        {
              if( $dbstatus == "-1")
              {
                $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled',
                  'order_packed','pickup_did_not_come','in_route', 'package_picked_up','delivery_scheduled' ) ; 
              }
              else 
              {
                $status[] =  $dbstatus;
              }
        }  
        else 
        {
          $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner','order_packed','pickup_did_not_come', 
            'delivered', 'cancelled', 'in_route', 'package_picked_up','delivery_scheduled' ) ; 
        }

         
         
 
        $first_query = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
        ->where("ba_business.frno",  $frno )  
        ->where("ba_pick_and_drop_order.request_from",  "business" )  
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone" ,  DB::Raw("'pnd' orderType")  )  ;

        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")  
        ->where("ba_pick_and_drop_order.route_to_frno",  $frno  )  
        ->where("ba_pick_and_drop_order.request_from",  "business" )  
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->union($first_query)
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone" ,  DB::Raw("'pnd' orderType") ) 
        ->get();
 

        $pnd_orders  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->where("ba_pick_and_drop_order.request_from", "business" )  
        ->where("ba_business.frno",  $frno )
 
        
       ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ")  
       ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
       ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ")


        ->select("ba_pick_and_drop_order.*",  
          
          DB::Raw("'0' mid") ,
          "ba_business.id as bin", 
          "ba_business.name", 
          "ba_business.phone_pri as primaryPhone", 
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")  

        ) 
        
        ->orderBy("ba_pick_and_drop_order.id", "desc")

        ->get();

         

         $assist_orders = DB::table("ba_pick_and_drop_order") 
          ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
          ->whereRaw("date(service_date) >= '$orderDate'") 
          ->whereIn("ba_pick_and_drop_order.book_status", $status)    
          ->whereIn("request_from", array ('customer' ))
          ->whereIn("ba_profile.id", function($query) use($frno){
                  $query->select("profile_id")->from("ba_users")
                  ->where("franchise", $frno);

           })

        ->orderByRaw("date(ba_pick_and_drop_order.service_date)= '$todayDate' desc ") 
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)< '$todayDate' desc ")
        ->orderByRaw("date(ba_pick_and_drop_order.service_date)> '$todayDate' asc ")
        
        ->select("ba_pick_and_drop_order.*",  
         
         "ba_profile.id as mid", 
         DB::Raw("'0' bin") ,
         "ba_profile.fullname as name", 
         "ba_profile.phone as primaryPhone", 
         "ba_profile.locality" ,
          DB::Raw("'assist' orderType")  

        ) 
        
       ->orderBy("ba_pick_and_drop_order.id", "desc")  
        ->get();

 
      }
      else 
      {
 
 
        $request->session()->remove('_last_search_date' );
        $status = array( 'new','confirmed', 'cancel_by_client', 
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route', 
        'package_picked_up','delivery_scheduled' ) ;  

        $first_query  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)   
        ->where("ba_pick_and_drop_order.request_from", "business" )  
        ->where("ba_business.frno",  $frno )  
        ->select("ba_pick_and_drop_order.*",
          DB::Raw("'0' mid") ,  
         "ba_business.id as bin", "ba_business.name", 
         "ba_business.phone_pri as primaryPhone" ,  
         DB::Raw("'pnd' orderType") )   ;
         

         $pnd_orders  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)   
        ->where("ba_pick_and_drop_order.request_from", "business" )  
        ->where("ba_pick_and_drop_order.route_to_frno",  $frno )  
        ->union($first_query )
        ->select("ba_pick_and_drop_order.*",
          DB::Raw("'0' mid") ,  
         "ba_business.id as bin", "ba_business.name", 
         "ba_business.phone_pri as primaryPhone" ,  
         DB::Raw("'pnd' orderType") )  
        ->get();


        $assist_orders = DB::table("ba_pick_and_drop_order") 
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
          ->whereRaw("date(service_date) >= '$orderDate'") 
          ->whereIn("ba_pick_and_drop_order.book_status", $status)    
          ->where("request_from", 'customer' ) 
        ->select("ba_pick_and_drop_order.*",  
         "ba_profile.id as mid", 
         DB::Raw("'0' bin") ,
         "ba_profile.fullname as name", 
         "ba_profile.phone as primaryPhone", 
         "ba_profile.locality" ,
          DB::Raw("'assist' orderType")   )  
        ->where("ba_pick_and_drop_order.route_to_frno",  $frno )  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->get(); 

      }

      $merged = $pnd_orders->merge($assist_orders);
      $day_bookings = $merged->all(); 

      //finding free agents  
      $today = date('Y-m-d'); 

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_users.franchise",  $frno )  
      ->where("ba_profile.fullname", "<>", "" )
      ->get();

       $all_staffs = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname",  "ba_users.id as user_id" ) 
      ->where("ba_users.category", ">", "100")
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_users.franchise",  $frno ) 
      ->get();

      $delivery_orders = DB::table("ba_delivery_order")
      ->whereRaw(" date(accepted_date) = '$today'" )
      ->where("completed_on", null )
      ->where("order_type", 'pick and drop' )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();

      $free_agents = array();

      //Fetching assigned orders
      $delivery_orders_list = DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
      ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
      ->whereRaw(" date(accepted_date) ='$today'" ) 
      ->get(); 
 
   
      foreach ($all_agents as $item)
      {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           } 


          //image compression
          if($item->profile_photo != null )
          {
            
            $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

            if(file_exists(  $source ) &&  !file_exists( $destination ))
            {
              $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 

      $customerphones = $merchants = array();
      foreach($day_bookings as $item)
      {
        $merchants[] = $item->bin;
        $customerphones[] = $item->phone;
      }
      $merchants[] = 0;

      $merchants = array_unique($merchants);

      $businesses  = DB::table("ba_business") 
      ->whereIn("id", $merchants ) 
      ->get(); 

      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->where("frno",  $frno ) 
      ->get(); 

      $requests_to_process  = DB::table("ba_order_process_request")  
      ->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
      ->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
      ->whereDate("log_date", $today ) 
      ->get(); 

      $banned_list = DB::table("ba_ban_list")
      ->whereIn("phone",  $customerphones) 
      ->get();
 
      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents , 'staffs'=>$all_staffs, 
        'delivery_orders_list' => $delivery_orders_list, 
        'results' => $day_bookings,  
        'requests_to_process' =>$requests_to_process,
        'all_business' =>$all_business,
        'todays_business' => $businesses,
        'last_keyword' => $last_keyword,
        'last_keyword_date' =>  $last_keyword_date , 
        'banned_list' =>$banned_list,
        'refresh' => 'true',
        'breadcrumb_menu' => 'pnd_franchise_sub_menu'
      );
       return view("franchise.orders.pick_and_drop_orders_list")
       ->with('data',$data)
       ->with('theme',$theme);
    }



  
  //View completed pick and drop
    protected function viewPickAndDropCompletedOrders(Request $request)
    {

      $frno = $request->session()->get("_frno");

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date));  
        $today  = date('Y-m-d', strtotime($request->filter_date)); 
      }
      else 
      {
        $today = date('Y-m-d'); 
        $orderDate = date('Y-m-d'); 
      }


      if( isset($request->btn_search) )
      {

        $request->session()->put('_last_search_', $request->filter_status); 
        $request->session()->put('_last_search_date',  $request->filter_date );


        $dbstatus = $request->filter_status; 

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
            }
            else 
            {
              $status[] =  $dbstatus;
            }
        }  
        else 
        {
          $status =  array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
        }


        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->where("ba_business.frno",  $frno ) 
        ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone" ) 
        ->get();


      }
      else 
      {

        $request->session()->remove('_last_search_date' );
        $status = array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ; 
 
        $day_bookings = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->where("ba_business.frno",  $frno ) 
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")  
        ->whereIn("ba_pick_and_drop_order.book_status", $status)  
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",  
         "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone" ) 
        ->get();

      }  

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" ) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_users.franchise",  $frno ) 
      ->where("ba_profile.fullname", "<>", "" )
      ->get();

      $delivery_orders = DB::table("ba_delivery_order")
      ->whereRaw(" date(accepted_date) = '$today'" )
      ->where("completed_on", null )
      ->where("order_type", 'pick and drop' )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();

      $free_agents = array();

      //Fetching assigned orders
      $delivery_orders_list = DB::table("ba_delivery_order")
      ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
      ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
      ->whereRaw(" date(accepted_date) ='$today'" ) 
      ->get(); 
 
   
      foreach ($all_agents as $item)
      {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           } 


          //image compression
          if($item->profile_photo != null )
          {
            
            $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

            if(file_exists(  $source ) &&  !file_exists( $destination ))
            {
              $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
 

      $all_business  = DB::table("ba_business")  
      ->where("is_block", "no" ) 
      ->where("frno", $frno)
      ->get(); 

      $merchants = array();
      foreach($day_bookings as $item)
      {
        $merchants[] = $item->bin; 
      }
      $merchants[] = 0; 
      $merchants = array_unique($merchants); 
      $businesses  = DB::table("ba_business") 
      ->whereIn("id", $merchants ) 
      ->get(); 

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

       $data = array( 'title'=>'Manage Pick-And-Drop Orders' , 
        'date' => $orderDate, 'all_agents' => $all_agents ,  
        'delivery_orders_list' => $delivery_orders_list, 
        'results' => $day_bookings,  
        'todays_business' => $businesses,
        'all_business' =>$all_business,
        'last_keyword' => $last_keyword, 'last_keyword_date' =>  $last_keyword_date , 
        'refresh' => 'true',
        'breadcrumb_menu' => 'pnd_franchise_sub_menu'
      );

       return view("franchise.orders.pick_and_drop_old_orders_table")->with('data',$data)
       ->with('theme',$theme);
  
  }


  protected function viewPnDOrderDetails(Request $request)
  {
      
      $frno = $request->session()->get("_frno");
      $orderno = $request->orderno;

      if($orderno == "")
      {
        return redirect("/franchise/pick-and-drop-orders/view-all"); 
      } 

      $order_info   = DB::table("ba_pick_and_drop_order") 
      ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
      ->select("ba_pick_and_drop_order.*", 
        "ba_business.name as businessName","ba_business.locality as businessLocality", 
        "ba_business.landmark as businessLandmark", "ba_business.city as businessCity", 
        "ba_business.state as businessState", "ba_business.pin as businessPin", 
        "ba_business.phone_pri as businessPhone",  "ba_business.banner" )
      ->where("ba_pick_and_drop_order.id", $orderno )    
      ->where("request_from", "business")
      ->whereRaw(" ( ba_business.frno = '$frno' or  ba_pick_and_drop_order.route_to_frno='$frno' )" ) 
      ->first() ;

       if( !isset($order_info))
      {
        return redirect("/franchise/pick-and-drop-orders/view-all"); 
      }

      $agent_info=  DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
         ->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
         ->where("order_no", $orderno  )
         ->where("order_type",  "pick and drop" )
         ->first(); 
      
      $today = date('Y-m-d'); 

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", $frno ) 
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();


      $banned_list    = DB::table("ba_ban_list")   
      ->where("phone", $order_info->pickup_phone )  
      ->orWhere("phone", $order_info->pickup_phone )     
      ->get() ;

       $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ;

      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("is_booktou_staff", "yes" )
      ->where("ba_users.status", "<>", "100" )  
      ->where("ba_users.category",   "100" )  
      ->where("franchise", 0)
      ->select("ba_profile.*", "ba_users.category" ) 
      ->get();

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

      $result = array('order_info' => $order_info ,  
       'agent_info' => $agent_info  ,
      'banned_list' => $banned_list,  
       'all_agents' => $all_agents ,  
       'staffs' => $all_staffs, 
       'remarks' => $remarks ,

     );
      return view("franchise.orders.pnd_order_detailed_view")->with('data',$result)->with('theme',$theme);


     }

    



      protected function normalOrderStatsLog(Request $request)
    {

       $frno = $request->session()->get("_frno");

      if( isset($request->filter_date))
      {
        $orderDate = date('Y-m-d', strtotime($request->filter_date)) ; 
      }
      else 
      {
        $orderDate = date('Y-m-d' , strtotime("10 days ago"))  ; 
      }

      $status = array( 'new','confirmed', 'delivered', 'order_packed','delivery_scheduled','task_accepted',  'waiting_package','package_picked_up','in_route', 'delivered' ) ; 


      if( isset($request->btn_search) )
      {
 
        $request->session()->put('_last_search_date',  $request->filter_date ); 
      }
      else 
      {

        $request->session()->remove('_last_search_date' ); 
      }

      
      $day_bookings= DB::table("ba_service_booking")
      ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
      ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
          $query->select("id")->from("ba_business")
          ->where("frno",  $frno  );

      })
      ->whereRaw("date(ba_service_booking.service_date) >= '$orderDate'")
      ->whereIn("ba_service_booking.book_status",$status)   
      ->select("ba_service_booking.*", "ba_profile.fullname", "ba_profile.phone" )
      ->orderBy("ba_service_booking.id", "desc")
      ->get();

 
      $bookid  = array();
      $all_bins = array();
      $book_by = array();
      foreach($day_bookings as $bitem )
      {
        $all_bins[] = $bitem->bin; 
        $book_by[] = $bitem->book_by;
        $bookid[] = $bitem->id;
      }
 
      $order_logs = DB::table("ba_order_status_log")
      ->whereIn("order_no", $bookid)    
      ->orderBy("order_no", "asc" )
      ->orderBy("seq_no", "desc" )
      ->get();



      $all_bins = array_filter($all_bins); 
      $all_businesses = DB::table("ba_business")
      ->whereIn("id", $all_bins)    
      ->get();


      $order_statuses = array(   'completed', 'order_packed' , 'in_route','delivered' , 'package_picked_up','delivery_scheduled' );

      $book_by = array_filter($book_by);

       $book_status = array( 'cancel_by_owner','pickup_did_not_come', 'cancel_by_client',  'canceled','order_packed' ) ; 
 
 
      $book_counts = DB::table("ba_service_booking")
      ->whereIn("book_by", $book_by)    
      ->whereNotIn("book_status", $book_status)  
      ->select("book_by", DB::raw('count(*) as totalOrders') )
      ->groupBy("book_by")
      ->get(); 
 
     
        //finding free agents   
        $all_agents = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo"  ) 
        ->where("ba_users.category", 100)
        ->where("ba_users.status", "<>" , "100") 
        ->where("ba_users.franchise",  $frno ) 
        ->where("ba_profile.fullname", "<>", "" )
        ->get();


        $delivery_orders = DB::table("ba_delivery_order")
        ->whereRaw(" date(accepted_date) >= '$orderDate'" )
        ->where("completed_on", null )
         ->where("order_type",  'client order'  )
        ->select("member_id", DB::Raw("count(*) as cnt") )
        ->groupBy('member_id')
        ->get();

        $free_agents = array();


        //Fetching assigned orders
        $delivery_orders_list = DB::table("ba_delivery_order")
        ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
        ->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
        ->whereRaw(" date(accepted_date) ='$orderDate'" )
        ->get(); 
 
   
        foreach ($all_agents as $item)
        {
           $is_free = true  ;
           $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
           foreach($delivery_orders as $ditem)
           {
              if($item->id == $ditem->member_id )
              {
                $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)"; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           }



           //image compression
            if($item->profile_photo != null )
            {
              $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

              if(file_exists(  $source ) &&  !file_exists( $destination ))
              {
                  $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         }
         

         $last_keyword = "0";
         if($request->session()->has('_last_search_' ) ) 
         {
          $last_keyword = $request->session()->get('_last_search_' );
         }


         $last_keyword_date = date('d-m-Y') ;
         if($request->session()->has('_last_search_date' ) ) 
         {
            $last_keyword_date = $request->session()->get('_last_search_date' );
         }
      
            $theme = DB::table("ba_theme")
          ->where("frno",$frno)
          ->where("theme_for","bookTou-franchise")
          ->first();



       $data = array( 'title'=>'Manage Orders' ,'results' => $day_bookings, 'date' => $orderDate, 'all_agents' => $all_agents , 
        'delivery_orders_list' => $delivery_orders_list, 'all_businesses' =>$all_businesses, 'book_counts' => $book_counts,
        'last_keyword' => $last_keyword, 'last_keyword_date' =>  $last_keyword_date , 'refresh' => 'true', 'order_logs' => $order_logs);

         return view("franchise.orders.stats")->with('data',$data)->with('theme',$theme);
    }




  public function viewBestPerformers(Request $request)
  {


    $frno = $request->session()->get("_frno");
    $month = $request->month == "" ?  date('m') : $request->month  ;
    $year = $request->year == "" ?  date('Y')  : $request->year  ;  

 
      
    $sales = DB::table("ba_service_booking")  
     ->whereRaw("month(service_date)= '$month'")  
     ->whereRaw("year(service_date)= '$year'")  
     ->where("book_status", "delivered")
     ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
          $query->select("id")->from("ba_business")
          ->where("frno",  $frno  );

      }) 
     ->select( "bin",  DB::Raw("count(*) as saleCount"), DB::Raw("sum(seller_payable) as totalSales") )
     ->groupBy("bin")
     ->orderBy("totalSales", "desc")
     ->get();

     $bins = array();
     foreach($sales as $item)
     {
      $bins[] = $item->bin;
     }
      $bins[]=0;


    $businesses= DB::table("ba_business")   
    ->whereIn("id" ,  $bins) 
    ->where("frno",  $frno  ) 
    ->get( );

    $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

    $data = array('results' => $businesses, 'sales' => $sales  ); 
          //return json_encode( $data);
       return view("franchise.business.best_performers")->with('data',$data)->with('theme',$theme);
    }



  public function viewPndBestPerformers(Request $request)
  {

    $frno = $request->session()->get("_frno"); 
    $month = $request->month == "" ?  date('m') : $request->month  ;
    $year = $request->year == "" ?  date('Y')  : $request->year  ;  
    $sales = DB::table("ba_pick_and_drop_order")  
    ->whereRaw("month(service_date)= '$month'")  
    ->whereRaw("year(service_date)= '$year'")  
    ->where("book_status", "delivered")
    ->where("request_from", "business")
    ->whereIn("ba_pick_and_drop_order.request_by", function($query) use ($frno) {
          $query->select("id")->from("ba_business")
          ->where("frno",  $frno  );

      }) 

    ->select( "request_by as bin", DB::Raw("count(*) as saleCount"), DB::Raw("sum(total_amount-service_fee) as totalSales") )
    ->groupBy("request_by")
    ->orderBy("totalSales", "desc")
    ->get();

    
    $bins = array();
    foreach($sales as $item)
    {
      $bins[] = $item->bin;
    }
    $bins[]=0;

    $businesses= DB::table("ba_business")    
    ->whereIn("id" ,  $bins) 
    ->where("frno",  $frno  ) 
    ->get();

    $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

    $data = array('results' => $businesses, 'sales' => $sales  ); 
          //return json_encode( $data);
       return view("franchise.business.pnd_best_performers")->with('data',$data)->with('theme',$theme);
  }


 


  public function manageAttendance(Request $request)
  {
      
      $date =  date('Y-m-d');  
       $frno = $request->session()->get("_frno");
 


      $all_staffs =  DB::table("ba_profile")   
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->where("ba_users.status", "<>", "100" )  
      ->where("ba_users.franchise", $frno  )  
      ->select("ba_profile.*", "ba_users.category" )
      ->get(); 

      $active_staffs = DB::table("ba_staff_attendance")    
      ->whereDate("log_date",  $date )   
      ->get();

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

     $data = array(  'date' => $date , 'staffs' => $all_staffs , 'active_staffs' => $active_staffs );  
     return view("franchise.staffs.staffs_attendance")->with('data', $data)->with('theme',$theme); 
    }


  public function saveAttendance(Request $request)
  {

    $date =  date('Y-m-d');  
    $active_staff = StaffAttendanceModel::where("staff_id", $request->key )
    ->whereDate("log_date",  $date )    
    ->first();

    if( !isset($active_staff))
    {
      $active_staff = new StaffAttendanceModel; 
    }

    $active_staff->staff_id = $request->key;
    $active_staff->shift= $request->shift;
    $active_staff->staff_status= $request->status;
    $active_staff->log_date= $date; 
    $active_staff->start_log= date('Y-m-d H:i:s', strtotime($request->start));
    $active_staff->save();
    return redirect("/franchise/staffs/manage-attendance")->with("err_msg", "Attendance log taken"); 

  }
  


  protected function getDeliveryAgents(Request $request)
  {

      $today = date('Y-m-d');
      $frno = $request->session()->get("_frno");

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->where("ba_users.category", 100)
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", $frno  )  
      ->select("ba_profile.*", DB::Raw("'0' taskCount"), DB::Raw("'no' isFree"))
      ->orderBy("fullname", "asc")
      ->get();

      $delivery_orders = DB::table("ba_delivery_order")
      ->whereRaw(" date(accepted_date) = '$today'" )
      ->where("completed_on", null )
      ->select("member_id", DB::Raw("count(*) as cnt") )
      ->groupBy('member_id')
      ->get();


      $free_agents = array(); 
      foreach ($all_agents as $item)
      {
          $is_free = true  ;
          $item->taskCount =    " ( 0 tasks )";
          foreach($delivery_orders as $ditem)
          {
              if($item->id == $ditem->member_id )
              {
                $item->taskCount =    $ditem->cnt  ; 
                if( $ditem->cnt >= 50 )
                {
                    $is_free = false ; 
                    break;
                } 
              } 
           }
 
           if(  !$is_free )
           {
              $item->isFree = "no";
           }



           //image compression
            if($item->profile_photo != null )
            {
              $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ; 

              if(file_exists(  $source ) &&  !file_exists( $destination ))
              {
                  $info = getimagesize($source ); 

                  if ($info['mime'] == 'image/jpeg')
                      $image = imagecreatefromjpeg($source);

                  elseif ($info['mime'] == 'image/gif')
                      $image = imagecreatefromgif($source);

                  elseif ($info['mime'] == 'image/png')
                      $image = imagecreatefrompng($source);


                $original_w = $info[0];
                $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                                   0, 0,
                                   0, 0,
                                   $original_w, $original_h,
                                   $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
              }
              $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );   

            }
            else
            {
              $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;  
            }

         } 

         $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

        $data = array("agents" =>   $all_agents );
        return view("franchise.agents.agents_list")->with('data',$data)->with('theme',$theme);

    }



    protected function getDeliveryAgentsLocations(Request $request)
    {

       $frno = $request->session()->get("_frno");


      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>", 100)
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise",  $frno )
      ->select("ba_profile.*", DB::Raw("'0' latitude"), DB::Raw("'0' longitude"),
      DB::Raw("'NIL' orderNo") )
      ->orderBy("fullname", "asc")
      ->get();

      $agentids=array();

      foreach ($all_agents as $item)
      { 
        $agentids[] = $item->id;
      }
       $agentids[] = 0;

      $livelocations = DB::table("ba_agent_locations") 
      ->join("ba_staff_attendance", "ba_staff_attendance.staff_id", "=", "ba_agent_locations.staff_id")
      ->whereIn("ba_agent_locations.staff_id", $agentids) 
      ->where("seq_no", 1)  
      ->whereDate("created_at",  date('Y-m-d') ) 
      ->whereDate("ba_staff_attendance.log_date",  date('Y-m-d') ) 
      ->get();

 

      $all_tasks = DB::table("ba_order_sequencer") 
      ->whereDate("entry_date",  date('Y-m-d') ) 
      ->get();

      $pndIds  =  $normalIds = array();

      $pndIds[] =  $normalIds[] = 0;
      foreach($all_tasks as $item)
      {
        if( $item->type == "pnd")
        {
          $pndIds[]  =  $item->id;
        }
        else 
        {
          $normalIds[] =  $item->id; 
        }
      }



      $pnd_task_lists = DB::table("ba_pick_and_drop_order") 
      ->join("ba_delivery_order", "ba_delivery_order.order_no", "=", "ba_pick_and_drop_order.id")
      ->whereIn("ba_pick_and_drop_order.id", $pndIds) 
      ->whereNotIn("book_status" ,  
        array( 'completed','cancel_by_client','cancel_by_owner', 'pickup_did_not_come', 'delivered','returned', 'canceled'))
      ->select("ba_pick_and_drop_order.id", "ba_delivery_order.member_id")  ;
 
      $all_task_lists = DB::table("ba_service_booking") 
      ->join("ba_delivery_order", "ba_delivery_order.order_no", "=", "ba_service_booking.id")
      ->union($pnd_task_lists) 
      ->whereIn("ba_service_booking.id", $normalIds) 
      ->whereNotIn("book_status" ,  
        array( 'completed','cancel_by_client','cancel_by_owner', 'pickup_did_not_come', 'delivered','returned', 'canceled'))
      ->select("ba_service_booking.id", "ba_delivery_order.member_id") 
      ->get();


      foreach ($all_agents as $item)
      {
        foreach ($livelocations as $litem)
        { 
          if( $item->id  == $litem->staff_id )
          { 
            $item->latitude = $litem->latitude;
            $item->longitude = $litem->longitude;
            break;
          } 
        }

        $taskid = array();
        foreach ($all_task_lists as $titem)
        {
          if( $item->id  == $titem->member_id )
          { 
            $taskid[] =   $titem->id;  
          } 
        }

        $item->orderNo = count($taskid) > 0 ? implode(",",  $taskid) : "No Task" ;
      }

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();
 
      $data = array("agents" =>   $all_agents, 'livelocations' => $livelocations );
      return view("franchise.agents.live_locations")->with('data',$data)->with('theme',$theme);

    }



  
    protected function fetchDeliveryAgentsPerformance(Request $request)
    {
$frno = $request->session()->get("_frno");
       $date =  date('Y-m-d');  

        $all_staffs =  DB::table("ba_profile")   
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("is_booktou_staff", "yes" )
        ->where("ba_users.status", "<>", "100" )  
        ->where("ba_users.category",   "100" )  
        ->where("ba_users.franchise",  $frno )
        ->select("ba_profile.*", "ba_users.category" )
        ->get(); 


        $agent_ids = array(0);
        foreach( $all_staffs as $item)
        {
          $agent_ids[] = $item->id;
        }

        $normal_deliveries = DB::table("ba_service_booking")
        ->where('book_status', 'delivered')
        ->select("id", "delivery_charge", DB::Raw("'normal' order_type")) ;

        $all_orders = DB::table("ba_pick_and_drop_order")
        ->where('book_status', 'delivered')
        ->select("id", "service_fee as delivery_charge" , DB::Raw("'pnd' order_type"))
        ->union( $normal_deliveries)
        ->get();


        $active_staffs = DB::table("ba_staff_attendance")    
        ->whereDate("log_date",  $date )   
        ->get();

        $total_count = array();
          

        $normalDeliveries = Db::table('ba_service_booking')
        ->join('ba_delivery_order','ba_delivery_order.order_no',"=", 'ba_service_booking.id')
        ->whereIn("ba_delivery_order.member_id", $agent_ids )
        ->where("ba_service_booking.book_status", "delivered")
        ->select("member_id", DB::Raw('count(*) as total_count'), DB::Raw('sum(delivery_charge) as total_earned') )
        ->groupBy('ba_delivery_order.member_id') ;


        $allDeliveries = Db::table('ba_pick_and_drop_order')
        ->join('ba_delivery_order','ba_delivery_order.order_no',"=", 'ba_pick_and_drop_order.id')
        ->whereIn("ba_delivery_order.member_id", $agent_ids )
        ->where("ba_pick_and_drop_order.book_status", "delivered")
        ->select("member_id", DB::Raw('count(*) as total_count'), DB::Raw('sum(service_fee) as total_earned') )  
        ->groupBy('ba_delivery_order.member_id')
        ->union( $normalDeliveries )
        ->get();

        $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

      $data = array(  'date' => $date , 'staffs' => $all_staffs , 'active_staffs' => $active_staffs ,'all'=>$total_count, 'allDeliveries' => $allDeliveries  );  
       return view("franchise.agents.delivery_agent_performance")->with('data', $data)
       ->with('theme',$theme); 

    }



    // Get Delivery Agent Earning Report for daily
    protected function allDeliveryAgentDailyOrdersReport(Request $request)
    {
      
      $frno = $request->session()->get("_frno");
      if(isset($request->filter_date))
       {
          $today =   date('Y-m-d', strtotime($request->filter_date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }
        
        $totalEarning = 0;


        $agents  = DB::table("ba_profile")
        ->join("ba_staff_attendance", "ba_staff_attendance.staff_id", "=", "ba_profile.id")
        ->whereIn("ba_profile.id", function($query) use ($frno){
          $query->select("profile_id")
          ->from("ba_users") 
          ->where("category", '100' )
          ->where("status", '<>', '100' )
          ->where("franchise",  $frno ) ;

        })
        ->whereDate("log_date",    $today  )
        ->select("ba_profile.id", "ba_profile.fullname")
        ->get(); 

        $agentids = array(0);
        foreach($agents as $agent)
        {
          $agentids[] =$agent->id;
        }


      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->whereIn("ba_delivery_order.member_id", $agentids )
        ->whereDate('ba_service_booking.service_date', $today)  
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            "ba_service_booking.service_date",
            "ba_service_booking.preferred_time as delivery_time",
            'book_status as orderStatus',
            'seller_payable as totalAmount',
            'delivery_charge as serviceFee',
            "payment_type as payMode",
            DB::Raw("'normal' orderType"),
            "ba_delivery_order.member_id"
        ) ;
 


        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->whereIn("ba_delivery_order.member_id", $agentids )
        ->whereDate('ba_pick_and_drop_order.service_date', $today)   
        ->union( $normalorders )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            "ba_pick_and_drop_order.service_date",
            "ba_pick_and_drop_order.delivery_time",
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            DB::Raw("'pnd' orderType"),
            "ba_delivery_order.member_id"
        )
        ->get();
  

        $merchantIds =array();
        $customerIds =array();
        $toalamount =0.00;
        foreach ($all_orders as $item) 
        {
            $toalamount += $item->totalAmount + $item->serviceFee;

          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }
        $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;
                    break;
                }

            }
        }
                 
       }

       $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

       $data = array(  'date' =>$today  ,  "total_amount" => $toalamount,  'agents' =>  $agents,  'all_orders' =>  $all_orders );

      return view("franchise.earning_clearance.all_agents_daily_orders")->with('data',$data)->with('theme',$theme);

     }


      // Get Delivery Agent Earning Report for daily
    protected function getDeliveryAgentDailyOrdersReport(Request $request)
    {

        $id=$request->agent;
        if($request->agent == "" )
        {
          return redirect("/franchise/customer-care/delivery-agents")->with("err_msg", "No agent selected.");
        }

        if(isset($request->filter_date))
        {
          $today =   date('Y-m-d', strtotime($request->filter_date));  
        }
        else 
        {
          $today =   date('Y-m-d');  

        }
        
        $totalEarning = 0;

      
        $normalorders  = DB::table("ba_delivery_order")
        ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $id )
        ->whereDate('ba_service_booking.service_date', $today)  
        ->select( 
            "ba_service_booking.id as orderNo", 
            "book_by as orderBy",  
            'book_status as orderStatus',
            'seller_payable as totalAmount',
            'delivery_charge as serviceFee',
            "payment_type as payMode",
            DB::Raw("'normal' orderType")
        ) ;
 


        $all_orders  = DB::table("ba_delivery_order")
        ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
        ->where("ba_delivery_order.member_id", $id )
        ->whereDate('ba_pick_and_drop_order.service_date', $today)   
        ->union( $normalorders )
        ->select( 
            "ba_pick_and_drop_order.id as orderNo", 
            "ba_pick_and_drop_order.request_by as orderBy",  
            'book_status as orderStatus',
            'total_amount as totalAmount',
            'service_fee as serviceFee',
            "pay_mode as payMode",
            DB::Raw("'pnd' orderType")
        )
        ->get();
 
 

        $merchantIds =array();
        $customerIds =array();
        $totalPnDAmount =0.00;
        foreach ($all_orders as $item) 
        {
          $totalPnDAmount += $item->totalAmount + $item->serviceFee;

          if($item->orderType == "pnd")
            $merchantIds[] = $item->orderBy;
          else 
            $customerIds[] =  $item->orderBy;
        }
        $customerIds[] = $merchantIds[] =0;
       
        $merchants  = DB::table("ba_business")
        ->whereIn("id", $merchantIds )
        ->select("id as orderBy","name")
        ->get();

        $customers  = DB::table("ba_profile")
        ->whereIn("id", $customerIds )
        ->select("id as orderBy","fullname")
        ->get();


        
       foreach ($all_orders as $item) 
       {
        if($item->orderType == "pnd")
        {
          foreach ($merchants as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->name;
                    break;
                }

            }
        }
        else 
        {
          foreach ($customers as $citem) 
            {
                if($item->orderBy == $citem->orderBy )
                {
                    $item->orderBy= $citem->fullname;
                    break;
                }

            }
        }
             

               
       }

       
       $profile = CustomerProfileModel::find($id);

       $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

      $data = array( "agentName" =>  $profile->fullname ,   'agent' => $id, 'date' =>$today  , 
        "total_amount" => $totalPnDAmount,  'all_orders' =>  $all_orders );

      return view("franchise.earning_clearance.agent_daily_orders")->with('data',$data)->with('theme',$theme);

     }


  protected function manageCustomers(Request $request)
  {

    $frno = $request->session()->get("_frno");
    //franchise search starts
    $franchise_pincodes = DB::table("ba_franchise_areas")
    ->join("ba_franchise", "ba_franchise.zone", "=", "ba_franchise_areas.zone_name")
    ->where("ba_franchise.frno",  $frno )
    ->select("ba_franchise_areas.pin")
    ->get();
      
    if(!isset($franchise_pincodes))
    {
      return redirect("/franchise")->with("err_msg", "No franchise pin codes found!");
    }

    $pincodes = array("na");
    foreach($franchise_pincodes as $item)
    {
      $pincodes[] = $item->pin;
    } 
 


    $cloud_message= DB::table("ba_cloud_message") 
    ->get();
     

     $keyname = $request->search_key;

     if($keyname != "") 
        $key_name_where = " fullname like '%$keyname%'";
    else
      $key_name_where = "  id > 0";



      if( isset($request->btn_search) )
      {
        if($request->filter_key=="Present") 
        {
           $customers= DB::table("ba_profile")
           ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
           ->select("ba_profile.*", "ba_users.firebase_token", "ba_users.category", "ba_users.otp")
           ->where("category", "0")
           ->whereRaw( $key_name_where  )
           ->where("ba_users.firebase_token","!=", "") 
           ->whereIn("ba_profile.pin_code",  $pincodes)
           ->orderBy("fullname")
           ->paginate(50);
        }
        else if ($request->filter_key=="Absent") 
        {
           $customers= DB::table("ba_profile")
           ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
           ->select("ba_profile.*", "ba_users.firebase_token", "ba_users.category", "ba_users.otp")
           ->where("category", "0")
           ->whereRaw( $key_name_where  )
           ->whereNull("ba_users.firebase_token")
           ->whereIn("ba_profile.pin_code",  $pincodes)
           ->orderBy("fullname")
           ->paginate(50);
        }
        else   
        {
           $customers= DB::table("ba_profile")
          ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
          ->select("ba_profile.*", "ba_users.firebase_token", "ba_users.category", "ba_users.otp")
          ->where("category", "0")
          ->whereIn("ba_profile.pin_code",  $pincodes)
          ->whereRaw( $key_name_where  )
          ->orderBy("fullname")
          ->paginate(50); 
        }
        
       }
       else 
       {

        $customers= DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->select("ba_profile.*", "ba_users.firebase_token" , "ba_users.category", "ba_users.otp")
        ->where("category", "0")
        ->whereIn("ba_profile.pin_code",  $pincodes)
        ->orderBy("fullname")
        ->paginate(50); 

       }


       $cid = array(0);
       foreach($customers as $item)
       {
         $cid[] = $item->id;
       }

      $orders= DB::table("ba_service_booking") 
      ->select("book_by",  "book_status")
      ->whereIn("book_by",  $cid ) 
      ->get();

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();
     
      $data = array( 'title'=>'Manage All Customers' ,'results' => $customers, 'messages' => $cloud_message, 'orders' =>$orders  );
      return view("franchise.customers.view_all")->with('data',$data)->with('theme',$theme);
    }



    public function viewCustomerProfile(Request $request)
    {

      $frno = $request->session()->get("_frno");
      //franchise search starts
      $franchise_pincodes = DB::table("ba_franchise_areas")
      ->join("ba_franchise", "ba_franchise.zone", "=", "ba_franchise_areas.zone_name")
      ->where("ba_franchise.frno",  $frno )
      ->select("ba_franchise_areas.pin")
      ->get();
        
      if(!isset($franchise_pincodes))
      {
        return redirect("/franchise")->with("err_msg", "No franchise pin codes found!");
      }

      $pincodes = array("na");
      foreach($franchise_pincodes as $item)
      {
        $pincodes[] = $item->pin;
      } 



        $profileId=$request->cus_id;

        $customerProfile = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
        ->where("ba_profile.id",$profileId)
        ->whereIn("ba_profile.pin_code",  $pincodes)
        ->select("ba_profile.id as cusProfileId","fullname as cusFullName", 
                 "ba_profile.dob as cusDateOfBirth", "ba_profile.locality as cusLocality", 
                 "ba_profile.landmark as cusLandmark", "ba_profile.city as cusCity",
                 "ba_profile.state as cusState", "ba_profile.pin_code as cusPin",
                 "ba_profile.phone as cusPhone", "ba_profile.email as cusEmail",
                  "ba_profile.profile_photo as cusImage", "ba_users.flagged")
        ->first();


        $orders = DB::table("ba_service_booking")
        ->where("book_by",$profileId)
        ->orderBy("id","desc")
        ->get();

       $rate = $ratedata =  $rateAvg = $ratingFiveCount = $ratingFourCount = 
        $ratingThreeCount =  $ratingTwoCount =  $ratingOneCount ="0";  

      
      $reviews = DB::table("ba_customer_reviews")
      ->where("customer_id", $profileId)
      ->get();

     

      foreach($reviews as $item)
      {
        $rate += $item->rating;
        $ratedata = ''.$rate.''; 
      }
      
     // return ($ratedata);
      
      $count = $reviews->count();
      if($count>1){
          $checkrate = $rate / $count;
          $rateAvg = $checkrate;
          $rateAvgdata = ''.$rateAvg.'';

      
      }
      else{

           $rateAvgdata = ''.$rate.'';

      }


      $countRating = DB::table("ba_customer_reviews")
      ->where("customer_id", $profileId)
      ->select('rating',DB::raw('count(id)  as 
          total_count'))
      ->groupBy("rating")
      ->get();
      foreach ($countRating as $keyitem) {
        if($keyitem->rating ==1){
          $ratingOneCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==2){
          $ratingTwoCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==3){
          $ratingThreeCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==4){
          $ratingFourCount= $keyitem->total_count;
        }
        elseif($keyitem->rating ==5){
          $ratingFiveCount= $keyitem->total_count;
        }
        else{

        }
      }
        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

        $data = array('profiles' => $customerProfile, 
            'orders' => $orders,
            'reviews' => $reviews,
                      'totalRating' => $ratedata,
                      'ratingAverage'=>$rateAvgdata, 
                      'ratingFiveCount' => $ratingFiveCount,
                      'ratingFourCount' => $ratingFourCount,
                      'ratingThreeCount' => $ratingThreeCount,
                      'ratingTwoCount' => $ratingTwoCount,
                      'ratingOneCount' => $ratingOneCount);
    
     return view("franchise.customers.customer_profile")->with("data",$data)->with('theme',$theme);  
    }
    

  public function viewAdminProfile(Request $request)
  {

      $frno = $request->session()->get("_frno");
      $user_id = $request->session()->get("__user_id_");

      

      //franchise search starts
      $user_info = DB::table("ba_users") 
      ->where("id",  $user_id ) 
      ->first();
        
      if(!isset($user_info))
      {
        Auth::logout();
        $request->session()->flush();
        return redirect("/franchise/login")->with("err_msg",  "Franchise information not found!");
      }

      $profile_id=$user_info->profile_id; 
      $customer_profile = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->where("ba_profile.id",$profile_id) 
      ->select("ba_profile.id as cusProfileId","fullname as cusFullName", 
                 "ba_profile.dob as cusDateOfBirth", "ba_profile.locality as cusLocality", 
                 "ba_profile.landmark as cusLandmark", "ba_profile.city as cusCity",
                 "ba_profile.state as cusState", "ba_profile.pin_code as cusPin",
                 "ba_profile.phone as cusPhone", "ba_profile.email as cusEmail",
                  "ba_profile.profile_photo as cusImage", "ba_users.flagged")
      ->first(); 

      $data = array('profiles' => $customer_profile, );
      return view("franchise.customers.admin_profile")->with("data",$data);  
  }

    public function viewFrequentlyBrowsedProducts(Request $request)
{
      
      $month = $request->month == "" ?  date('m') : $request->month  ;
      $year = $request->year == "" ?  date('Y')  : $request->year  ;

      $frno = $request->session()->get('_frno');

      $MostViewProducts = DB::table("ba_tracker_product_visit")
      ->join("ba_products","ba_products.id","=","ba_tracker_product_visit.pid")
      ->whereMonth("visit_date",$month )
      ->whereYear("visit_date",$year )
      ->select("ba_products.*",  DB::Raw("count(*) as viewCount") )
      ->groupBy("ba_products.id" )
      ->orderBy("viewCount","desc")
      ->paginate(50); 
        

          $bins = array();
          foreach($MostViewProducts as $item)
          {
            $bins[] = $item->bin;
          }
          $bins[] = 0;

          $businesses = DB::table("ba_business")  
          ->whereIn("id", $bins )
          ->where("frno",1)
          ->get();


          $theme = DB::table("ba_theme")
          ->where("frno",$frno)
          ->where("theme_for","bookTou-franchise")
          ->first();

          
          $data = array(  'title'=>'Most Frequent Browsed Products' ,  
          'mostViewProducts' => $MostViewProducts, 'businesses' =>$businesses);
          return view("franchise.products.view_frequently_browsed_products")->with('data',$data)->with('theme',$theme); 
}


    
    public function monthlyEarningReport(Request $request)
  {

       $frno = $request->session()->get('_frno');

      $endmonth =12;
      $year = $request->year == "" ?  date('Y')  : $request->year  ;  

      $report_data = array( );

      for($i=1; $i <= $endmonth  ; $i++){


        $income = DB::table("ba_service_booking")   
         ->whereRaw("year(service_date)= '$year'")  
         ->where("book_status", "delivered")
         ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
            $query->select("id")->from("ba_business")
            ->where("frno",  $frno  ); 
          })   
         ->groupBy("currentMonth")
         ->select( DB::Raw("month(service_date) as currentMonth"),  DB::Raw("sum(seller_payable) as totalSale"), DB::Raw("sum(delivery_charge) as deliveryCommission")  )  
         ->get();

         $pnd_income = DB::table("ba_pick_and_drop_order")   
         ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
          ->where("ba_business.frno",  $frno )  

         ->whereRaw("year(service_date)= '$year'")  
         ->where("book_status", "delivered")
         ->groupBy("currentMonth")
         ->select(DB::Raw("month(service_date) as currentMonth"),  DB::Raw("sum(total_amount) as totalSale"), DB::Raw("sum(service_fee) as deliveryCommission")  )  
         ->get();
  

        $report_data[$i] = array(  'month' =>  date("F", mktime(0, 0, 0, $i, 10)), 'year' => $year , 
          'normalSale' => 0,  'normalOrderCommission' => 0 ,
          'pndSale' => 0,  'pndOrderCommission' => 0  );
 
        foreach($income as $item)
        {
           if($item->currentMonth == $i) {
             $report_data[$i]["normalSale"] += $item->totalSale;
             $report_data[$i]["normalOrderCommission"] += $item->deliveryCommission;
             break;
           }

        }
 
        foreach($pnd_income as $item)
        {
           if($item->currentMonth == $i) {
             $report_data[$i]["pndSale"] += $item->totalSale;
             $report_data[$i]["pndOrderCommission"] += $item->deliveryCommission;
             break;
           }

        }  

      } 
 

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

     $data = array(   'year' =>  $year, 'report_data' => $report_data  );  
     return view("franchise.reports.income_report")->with('data', $data)->with('theme',$theme); 
    }



    public function dailySalesReport(Request $request )
    { 

       $frno = $request->session()->get("_frno");

        if($request->todayDate != "")
        {
            $todayDate  =  date('Y-m-d' , strtotime($request->todayDate)) ; 
        }
        else
        {
            $todayDate  = date('Y-m-d' );
        }

        $totalEarning = 0;


        $first = DB::table('ba_service_booking')
        ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
        ->wheredate('ba_service_booking.service_date', $todayDate)
        ->where("ba_business.frno",  $frno )  
        ->select('ba_service_booking.id', 
          'total_cost as orderCost', 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', 
          'ba_business.name', "ba_business.category", DB::Raw("'normal' orderType")) ;

        $result = DB::table('ba_pick_and_drop_order')
        ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
        ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
        ->where("request_from", "business")
        ->where("ba_business.frno",  $frno )  
        ->union($first) 
        ->select('ba_pick_and_drop_order.id', 
          'total_amount as orderCost', 
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'ba_business.name', "ba_business.category", DB::Raw("'pnd' orderType"))
        ->orderBy("id", "asc")  
        ->get();

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

        $data = array(  'sales' =>  $result , 'date' =>  $todayDate ,'theme'=>$theme );  

        return view("franchise.reports.daily_sale_report")->with(  $data);  
    }



 
  protected function pendingPayments(Request $request )
  {

    $frno = $request->session()->get("_frno");

    if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }
 

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }

        $inner_query = DB::table('ba_pick_and_drop_order')
        ->whereRaw("month(service_date) = '". $month . "'")
        ->whereRaw("year(service_date) = '". $year . "'") 
        ->whereRaw("clerance_status = 'un-paid'")
        ->whereRaw("book_status in  (  'completed', 'delivered') " ) 
        ->select('request_by as bin', DB::raw('sum(total_amount) as totalDue'))
        ->groupBy('bin');

        $pnds = DB::table('ba_business') 
        ->join(DB::raw('(' . $inner_query->toSql() . ') sales'), 'ba_business.id', '=', 'sales.bin') 
        ->where("ba_business.frno",  $frno  )
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "sales.totalDue" ) 
        ->orderBy("ba_business.name",'asc');

 

        $inner_query_2 = DB::table('ba_service_booking')
        ->whereRaw("month(service_date) = '". $month . "'")
        ->whereRaw("year(service_date) = '". $year . "'") 
        ->whereRaw("clerance_status = 'un-paid'")
        ->whereRaw("book_status in  (  'completed', 'delivered') " )
        ->select('bin', DB::raw('sum(seller_payable) as totalDue'))
        ->groupBy('bin');

        $result = DB::table('ba_business') 
        ->join(DB::raw('(' . $inner_query_2->toSql() . ') sales'), 'ba_business.id', '=', 'sales.bin') 
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "sales.totalDue" ) 
        ->where("ba_business.frno",  $frno  )
        ->union($pnds)  
        ->get();

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();



        $data = array(  'result' => $result , 'month' =>  $month ,  'year' => $year,'theme'=>$theme ); 
        return view("franchise.reports.pending_payments")->with(  $data);



    }

protected function salesTarget(Request $request)
    {
        $frno = $request->session()->get('_frno');

        if ($request->minimum!="" && $request->maximum!="") {
          
            if ($request->maximum<$request->minimum) {
                return redirect('/franchise/sytem/config/sales-target')->with('err_msg','minimum target cannot be greater than maximum target!');
            }

             $targetlist = DB::table('ba_sales_target')
             ->where('frno',$frno)
             ->select()
             ->first();

             if ($targetlist) {
                $sales = SalesTargetModel::find($targetlist->id);
             }else{
                $sales = new SalesTargetModel;
             }
            

             if ($sales) {

               $sales->min = $request->minimum;
               $sales->max = $request->maximum;
               $sales->year = date('Y');
               $sales->month = date('m');
               $sales->bin = 0;
               $sales->difference = 0;
               $sales->frno = $frno;
               $saveTarget = $sales->save();
               
               if ($saveTarget) {
                return redirect('/franchise/sytem/config/sales-target')->with('err_msg','target set');
               }
                      }
        }

        $targetlist = DB::table('ba_sales_target')
        ->where('frno',$frno)
        ->select()
        ->first();

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

        $data = array('target'=>$targetlist,'theme'=>$theme);

        return view('franchise.settings.config')->with($data);
    }

     protected function franchiseOrderDeliveredByMonth(Request $request)
     {
       
      $today = date('Y-m-d');
      
      if ($request->month!="") {

        $month = $request->month;
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }else{

        $month = date('m');
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }

      if ($request->year!="") {

        $year = $request->year;

      }else{

        $year = date('Y');
        
      }

      $frno = $request->session()->get('_frno');
      
      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->where("ba_users.category", 100)
      ->where("ba_users.franchise",$frno)
      ->where("ba_users.status","<>",100)
      ->where("ba_profile.fullname", "<>", "" )
      ->select("ba_profile.*")
      ->orderBy("id", "asc")
      ->get(); 



      //dd($all_agents);

      $memberid = array();
      foreach ($all_agents as $profile) {
         $memberid[] = $profile->id;
      }



       $normal_orders = DB::table("ba_delivery_order")
       ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_service_booking.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_service_booking.service_date", $month )
       ->whereYear("ba_service_booking.service_date", $year )        
       ->select(
        "ba_delivery_order.member_id as member_id", 

        DB::Raw(" 'normal' as type"),
        DB::Raw("count(ba_service_booking.id) as orderCount"),
        DB::Raw("sum(ba_service_booking.delivery_charge) as total_earned")

       )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
       ->get();

        

       $pnd_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereIn("request_from", array ('business', 'merchant') )
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
        "ba_delivery_order.member_id as member_id",
         
         DB::Raw(" 'pnd' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned")


        )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
        ->get();
        
       $assist_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered') )
       ->whereIn('ba_delivery_order.member_id',$memberid)
       ->whereIn("request_from", array ('customer' ) )
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
        "ba_delivery_order.member_id as member_id",
         
         DB::Raw(" 'assist' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned") 


        )
       ->orderBy("ba_delivery_order.member_id","asc")
       ->groupBy("ba_delivery_order.member_id")
        ->get();


        $merged = $normal_orders->merge($pnd_orders,$assist_orders);
         
        
        $all_orders = $merged->all();
        
        $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();
         
        $data = array(
          "agents" =>$all_agents, 
          'orders'=>$all_orders ,
          'month'=>$monthname,
          'year'=>$year,
          'mont'=>$month 
        );
        return view('franchise.agents.delivery_report_by_agents_months')->with('data',$data)->with('theme',$theme);

     }


     public function activeUsersActivityHistory(Request $request)
    {
      $frno = $request->session()->get('_frno');
      
      $today = date('Y-m-d');
      $activeUsers =  DB::table("ba_profile") 
      ->join("ba_active_user_log", "ba_active_user_log.member_id", "=", "ba_profile.id")
      ->whereDate("ba_active_user_log.log_date",  $today ) 
      ->whereIn("ba_profile.id", function($query) use ( $frno ) { 
          $query->select("profile_id")
          ->from("ba_users")
          ->where("ba_users.category", 0 )
          ->where("ba_users.franchise",$frno) ;
        }) 
      ->select(  "ba_profile.*", DB::Raw("count(*) as openCount"), DB::Raw("'unknown' category") )
      ->groupBy("ba_profile.id")
      ->paginate(50);
    
        $mids = array();

        foreach($activeUsers as $item)
        {
          $mids[]  = $item->id;
        }

        $mids[] =0;
        
      $visitlogs =  DB::table("ba_active_user_log")  
      ->whereIn("member_id" , $mids ) 
      ->whereDate("ba_active_user_log.log_date",  $today ) 
      ->select( "member_id", "category" ) 
      ->get(); 

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

      $data = array( 'title'=>'Analytics of Active Users' ,'results' => $activeUsers , 'visitlogs' => $visitlogs);
      return view("franchise.analytics.user_activity")->with('data' , $data )->with('theme',$theme);
        
    }


     protected function getDailyOrdersReportAgentWise(Request $request,$memberid)
     {

       $frno = $request->session()->get('_frno');
        
       if ($request->month!="") {

        $month = $request->month;
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }else{

        $month = date('m');
        $monthname = date('F', mktime(0, 0, 0, $month, 10));

      }

      if ($request->year!="") {

        $year = $request->year;

      }else{

        $year = date('Y');
        
      }

        

       $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
      ->where("ba_users.category", 100)
      ->where("ba_users.franchise",$frno)
      ->where("ba_users.status","<>",100)
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_profile.id",$memberid)
      ->select("ba_profile.*")
      ->first();


       $normal_orders = DB::table("ba_delivery_order")
       ->join("ba_service_booking", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
       //->where("ba_service_booking.book_status","delivered")
       ->where('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_service_booking.service_date", $month )
       ->whereYear("ba_service_booking.service_date", $year )        
       ->select(
        DB::Raw("date(service_date) as serviceDate") , 
        DB::Raw(" 'normal' as type"),
        DB::Raw("count(ba_service_booking.id) as orderCount"),
        DB::Raw("sum(ba_service_booking.delivery_charge) as total_earned")
       )
       ->groupBy("serviceDate")
        
       
       ->get();



       $pnd_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
       ->whereIn("request_from", array ('business', 'merchant') )
       //->where("ba_pick_and_drop_order.book_status","delivered")
       ->where('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
         DB::Raw("date(service_date) as serviceDate"),
         DB::Raw(" 'pnd' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned") 
        )
       ->groupBy("serviceDate")
        
        ->get();

        $assist_orders = DB::table("ba_delivery_order")
       ->join("ba_pick_and_drop_order", "ba_pick_and_drop_order.id", "=", "ba_delivery_order.order_no")
       ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
       ->whereIn("request_from", array ('customer' ) )
       //->where("ba_pick_and_drop_order.book_status","delivered")
       ->where('ba_delivery_order.member_id',$memberid)
       ->whereMonth("ba_pick_and_drop_order.service_date", $month ) 
       ->whereYear("ba_pick_and_drop_order.service_date", $year ) 
       ->select(
         DB::Raw("date(service_date) as serviceDate"),
         DB::Raw(" 'assist' as type"),
         DB::Raw("count(ba_pick_and_drop_order.id) as orderCount"),
         DB::Raw("sum(ba_pick_and_drop_order.service_fee) as total_earned") 
        )
       ->groupBy("serviceDate")
        
        ->get();

         

        $merged = $normal_orders->merge($pnd_orders,$assist_orders);
        
        $all_orders = $merged->all();

        $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();
         
        $data = array(
          "agents" =>$all_agents, 
          'orders'=>$all_orders,
          'month'=>$monthname,
          'month_no'=>$month,
          'year'=>$year
        );


        return view('franchise.agents.daily_order_delivery')->with('data',$data)->with('theme',$theme);
     }



    public function franchiseAgentsMonthlyOrdersExportToExcel(Request $request) 
    
    {
          
          $frno = $request->session()->get('_frno');
           
          if( $request->month   == "")
          { 
              $month = date('m');
          }
          else 
          {
            $month   =  $request->month; 
          }

          if( $request->year   == "")
          { 
              $year = date('Y');
          }
          else 
          {
            $year   =  $request->year; 
          }
          
          //echo $year;die();
           
       return Excel::download(new FranchiseMonthlyAgentsOrdersExport($frno,$month,$year), 'franchise_agents_monthly_orders_count.xlsx');
    }

      
    public function franchiseAgentsDailyOrdersExportToExcel(Request $request) 
    
    {
          
        $frno = $request->session()->get('_frno');
           
          if( $request->month   == "")
          { 
              $month = date('m');
          }
          else 
          {
            $month   =  $request->month; 
          }

          if( $request->year   == "")
          { 
              $year = date('Y');
          }
          else 
          {
            $year   =  $request->year; 
          }

          $memberid = $request->memberid;
          
          
          return Excel::download(new FranchiseDailyAgentsOrdersExport($frno,$memberid,$month,$year),'franchise_agents_daily_orders_count.xlsx');
    }

//17-08-2021 new parts for pick and drop and assist for Franchise Controller

    protected function viewAssistOrderDetails(Request $request)
     {
        
       $orderno = $request->orderno;

      if($orderno == "")
      {
        return redirect("/franchise/customer-care/business/view-all"); 
      }

      $pndOrder = PickAndDropRequestModel::find($orderno);
       
      $from = $pndOrder->request_from;

      $order_info   = DB::table("ba_pick_and_drop_order") 
      ->join("ba_profile", "ba_pick_and_drop_order.request_by", "=", "ba_profile.id")

      ->select("ba_pick_and_drop_order.*")
      ->where("ba_pick_and_drop_order.id", $orderno )    
      //->where("request_from", "business")
      ->where("request_from", $from)
      ->first() ;

       if( !isset($order_info))
      {
        return redirect("/franchise/customer-care/business/view-all"  ); 
      }


      $agent_info=  DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
         ->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
         ->where("order_no", $orderno  )
         ->where("order_type",  "pick and drop" )
         ->first(); 

      $remarks    = DB::table("ba_order_remarks")   
      ->where("order_no", $orderno )  
      ->orderBy("id", "asc")   
      ->get() ;


      $today = date('Y-m-d'); 
      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();

      $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

      $result = array('order_info' => $order_info ,  
       'agent_info' => $agent_info , 
       'all_agents' => $all_agents ,  
       'remarks' => $remarks );
      return view("franchise.orders.assist_order_detailed_view")->with('data',$result)->with('theme',$theme);


     }

     protected function newPickAndDropRequest(Request $request)
    { 

    if(   $request->cname == "" ||  $request->cphone  == ""  ||  $request->address   == "" ||   
          $request->sfee   == "" ||  $request->total   == "" ||  $request->servicedate    == "" || $request->bin     == "" )
    {

      return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg", "Important fields are missing!");
    }
      
     
     $business_info = Business::find(  $request->bin   );
     if( !isset($business_info) )
     {
        return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'No matching business found.'  );
     }  
  
     $address  =  $request->address;
     $landmark  =  $request->landmark; 
     $latitude   =  $request->latitude;
     $longitude  =  $request->longitude;
 
    //save sequencer  
    $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G'); 
    $rp_1 = mt_rand(0, 9);
    $rp_2 = mt_rand(0, 9); 
    $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ; 

    $sequencer = new OrderSequencerModel;
    $sequencer->type = "pnd";
    $sequencer->tracker_session =  $tracker; 
    $save = $sequencer->save();
    
    if( !isset($save) )
    { 
       return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'Failed to generate order no sequence.'  );
    }
 

     $new_order = new PickAndDropRequestModel ; 
     $new_order->id = $sequencer->id ; 
     $new_order->request_by =  $request->bin ; 
     $new_order->request_from = "business";   
     $new_order->otp  =   mt_rand(222222, 999999);  
     $new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  : null);
     $new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : null);
     $new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : null);
     $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null); 
     $new_order->address = $address;
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ;  
     $new_order->drop_name = $request->cname; 
     $new_order->drop_phone =  $request->cphone ; 
     $new_order->drop_address =  $address;
     $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->drop_latitude =   0.00 ;
     $new_order->drop_longitude =  0.00 ;  
     $new_order->fullname  = $request->cname; //obsolete
     $new_order->phone  = $request->cphone ; //obsolete 
     $new_order->book_status = "new";
     $new_order->service_fee = $new_order->requested_fee =  $request->sfee ;  
     $new_order->packaging_cost = ($request->packcost != "" ? $request->packcost : 0.00); 
     $new_order->total_amount = $request->total ; 
     $new_order->book_date =  date('Y-m-d H:i:s');   
     $new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));

     $new_order->pickup_details =  ($request->remarks != "" ? $request->remarks : null);  
     $new_order->source =  ( $request->source != "" ? $request->source : "booktou" );  

     if($request->session()->has('__user_id_' ) ) 
     {
        $new_order->staff_user_id  =  $request->session()->get('__user_id_' );
     }  
     $new_order->tracking_session = $tracker; 
     $save=$new_order->save();  
     if($save)
     {
      return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'New pick-and-drop order placed!'  );  
     }
     else
     {
       return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'Your pick-and-drop order could not be placed. Please retry'  );
     } 
  }


  protected function convertReceiptToPickAndDropRequest(Request $request)
  {

      if(   $request->cname == "" ||  $request->cphone  == ""  ||  $request->address   == "" ||   
            $request->sfee   == "" ||  $request->total   == "" ||  $request->servicedate    == "" || $request->erono  == "" )
      {

        return redirect("/franchise/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "Important fields are missing!");
      }

      $new_order = PickAndDropRequestModel::find(   $request->erono  );
      if( !isset($new_order) )
      {
        return redirect("/franchise/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching receipt found.'  );
      }  
  

     $address  =  $request->address;
     $landmark  =  $request->landmark; 
     $latitude   =  $request->latitude;
     $longitude  =  $request->longitude;  

     $new_order->otp  =  mt_rand(222222, 999999); 
     $new_order->fullname  = $request->cname;
     $new_order->phone  = $request->cphone ; 
     $new_order->address = $address;
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ;


  
     $new_order->request_by =  $request->ebin ;    
     $new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  : null);
     $new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : null);
     $new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : null);
     $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);
      
     $new_order->address = $address;
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ; 
   

     $new_order->drop_name = $request->cname; 
     $new_order->drop_phone =  $request->cphone ; 
     $new_order->drop_address =  $address;
     $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->drop_latitude =   0.00 ;
     $new_order->drop_longitude =  0.00 ; 


     //////

     $new_order->service_fee = $new_order->requested_fee =  $request->sfee ;  
     $new_order->total_amount = $request->total  ;    
     $new_order->packaging_cost = ($request->packcost != "" ? $request->packcost : 0.00); 
     $new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));
     $new_order->pickup_details =  ($request->remarks != "" ? $request->remarks : null);  

     $new_order->pay_mode =   $request->paymode ;  
     $save=$new_order->save(); 

     if($save)
     {
      return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'Receipt converted to pick-and-drop order!'  );  
     }
     else
     {
      return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'Pick-and-drop order could not be saved. Please retry'  );
     } 
  }

   protected function removeAgentFromPnDOrder(Request $request)
  {
    
      if( $request->key == ""   )
      {
        return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg", 'Missing data to perform action.'  ); 
      }
  

      $orderinfo = DeliveryOrderModel::where('order_no',$request->key)->first();

 

      $delete =  DB::table('ba_delivery_order')
      ->where('order_no', $request->key)
      ->where('order_type', "pnd")
      ->delete();


      if ($delete) {
          DB::table('ba_pick_and_drop_order')
          ->where('id', $request->key)
          ->update(["book_status" => 'new'  ]); 
      } 


      return redirect("/franchise/pick-and-drop-orders/view-all/" .  $request->key )->with("err_msg", 'Agent freed from PnD Order. Please reassign another free agent!'  ); 
  }


  protected function updatePnDOrderRemarks(Request $request)
  {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/franchise/pick-and-drop-orders/view-all"); 
        }
    
        if( $request->remarks == "" || $request->type == "" )
        {
          if($request->turl == "list")
          {
            return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg", "Order remarks fields missing!"); 
          }
          else 
          {
            return redirect("/franchise/pnd-order/view-details/" . $orderno )->with("err_msg", "Order remarks fields missing!"); 
          }
          
        }

        $order_info   =  PickAndDropRequestModel::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $remark = new OrderRemarksModel();
        $remark->order_no = $orderno;
        $remark->remarks = ($request->remarks == "" ? "payment info update" :  $request->remarks ) ;
        $remark->rem_type = $request->type ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->remark_by = 0;
         
        $remark->save();   

         if($request->turl == "list")
          {
            return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg", "Order remarks updated!");   
          }
          else 
          {
            return redirect("/franchise/pnd-order/view-details/" . $orderno )->with("err_msg", "Order remarks updated!");   
          }

     
  }



   protected function updateAssistOrder(Request $request)
  {

      if(   $request->cname == "" ||  $request->cphone  == ""  ||  $request->address   == "" ||   
            $request->sfee   == "" ||  $request->total   == "" ||  $request->servicedate    == "" || $request->erono  == "" )
      {

        return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg", "Important fields are missing!");
      }

      $new_order = PickAndDropRequestModel::find(   $request->erono  );
      if( !isset($new_order) )
      {
        return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'No matching receipt found.'  );
      }  
   
     $address  =  $request->address;
     $landmark  =  $request->landmark; 
     $latitude   =  $request->latitude;
     $longitude  =  $request->longitude;  

     $new_order->otp  =  mt_rand(222222, 999999); 
     $new_order->fullname  = $request->cname;
     $new_order->phone  = $request->cphone ; 
     $new_order->address = $address;
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ;

  
     $new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  : null);
     $new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : null);
     $new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : null);
     $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);
      
     $new_order->address = $address;
     $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->latitude =   0.00 ;
     $new_order->longitude =  0.00 ; 
   

     $new_order->drop_name = $request->cname; 
     $new_order->drop_phone =  $request->cphone ; 
     $new_order->drop_address =  $address;
     $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
     $new_order->drop_latitude =   0.00 ;
     $new_order->drop_longitude =  0.00 ; 


     //////

     $new_order->service_fee = $new_order->requested_fee =  $request->sfee ;  
     $new_order->total_amount = $request->total  ;   
     $new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));
     $new_order->pickup_details =  ($request->remarks != "" ? $request->remarks : null);  

     $new_order->pay_mode =   $request->paymode ;  
     $save=$new_order->save(); 

     if($save)
     {
      return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'Assist order updated!'  );  
     }
     else
     {
      return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg",  'Assist order updated!'  );
     } 
  }


   protected function redirectToOrderDetailsView(Request $request)
     {
        
       $orderno = $request->orderno;

      if($orderno == "")
      {
        return redirect("/franchise"); 
      }

      $order_info   = DB::table("ba_order_sequencer")
      ->where("id", $orderno)  
      ->first() ;

      if( !isset($order_info))
      {
        return redirect("/franchise")->with("err_data", "No order information found!"); 
      }


      if( $order_info->type == "pnd")
      {
        return redirect("/franchise/pnd-order/view-details/" .  $orderno ); 
      }
      else if( $order_info->type == "assist")
      {
        return redirect("/franchise/customer-care/assist-order/view-details/" .  $orderno ); 
      }
      else
      {
        return redirect("/franchise/order/view-details/" .  $orderno );  
      }
 
     }



    protected function updatePnDOrderStatus(Request $request)
    {
        $orderno = $request->orderno;
        if($orderno == "")
        {
          return redirect("/franchise/pick-and-drop-orders/view-all"); 
        }

        if( $request->status == ""   )
        {
          return redirect("/franchise/pnd-order/view-details/" . $orderno )
          ->with("err_msg", "Order status missing!"); 
        }
        $order_info   = PickAndDropRequestModel::find( $orderno) ;

        if( !isset($order_info))
        {
          return redirect("/franchise/pick-and-drop-orders/view-all")->with("err_msg", "No matching order found!"); 
        }

        $order_info->book_status =  $request->status ;  
        $order_info->save();  
    

        //save remarks 
        $remark = new OrderRemarksModel();
        $remark->order_no =  $orderno ;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "order status update" ;  
        $remark->remark_by = $request->session()->get('__member_id_' );
        $remark->save();  


        $orderStatus = new OrderStatusLogModel;
        $orderStatus->order_no =  $orderno ;
        $orderStatus->order_status =  $request->status ;
        $orderStatus->remarks =  $request->remarks ;  
        $orderStatus->log_date = date('Y-m-d H:i:s');
        $orderStatus->save(); 

      if($order_info->request_from == "business" || $order_info->request_from == "merchant" )
        {
            return redirect("/franchise/pnd-order/view-details/" .  $orderno )
            ->with("err_msg", "Order remarks updated!");   
        }
        else  if(  $order_info->request_from == "customer")
        {
            return redirect("/franchise/pnd-order/view-details/" . $orderno )
            ->with("err_msg", "Order remarks updated!");   
        } 
        else 
        {
          return redirect("/franchise/pick-and-drop-orders/view-all"); 
        }
  

     }


 protected function updatePnDOrderPaymentUpdate(Request $request)
  {

      $orderno = $request->orderno;
      if($orderno == "")
      {
        return redirect("/franchise/pick-and-drop-orders/view-all"); 
      }

      if( $request->paytarget == "" || $request->paymode == "" )
      {
        return redirect("/franchise/pnd-order/view-details/" . $orderno )
        ->with("err_msg", "PnD payment info missing!"); 
      }

      $order_info   = PickAndDropRequestModel::find( $orderno) ;
      if( !isset($order_info))
      {
        return redirect("/franchise/pick-and-drop-orders/view-all"  )
        ->with("err_msg", "No matching order found!"); 
      }

      $paytarget =   $request->paytarget ;
      $paymode =  $request->paymode ;  

      if( date('d-m-Y', strtotime($request->servicedate)) != date('d-m-Y', strtotime($order_info->service_date))  )
      {
        $order_info->service_date =  date('Y-m-d', strtotime($request->servicedate)) ;  
      } 

      $order_info->source = $request->ordersource;
      $order_info->pickup_details = $request->orderdetails;
      $order_info->payment_target =$paytarget;
      $order_info->pay_mode  =  $paymode ; 

        if($request->total != "" )
        {
          $order_info->total_amount =  $request->total ;  
        }
        
        if($request->delivery != "" )
        {
          $order_info->requested_fee  =  $order_info->service_fee = $request->delivery;  
        }

        if($request->packcost != "" )
        {
          $order_info->packaging_cost  =  $request->packcost;  
        }

        $order_info->save();  


        //save remarks 
        $remark = new OrderRemarksModel();
        $remark->order_no =  $orderno ;
        $remark->remarks = $request->remarks ;
        $remark->rem_type = "payment update" ;  
        $remark->remark_by =  $request->session()->get('__member_id_' );
        $remark->save();  
  
        if($order_info->request_from == "business" || $order_info->request_from == "merchant" )
        {
            return redirect("/franchise/pnd-order/view-details/" .  $orderno )
            ->with("err_msg", "Order remarks updated!");   
        }
        else 
        {
            return redirect("/franchise/assist-order/view-details/" . $orderno )
            ->with("err_msg", "Order remarks updated!");   
        }
}
  
  //route order to franchise
  protected function routeOrderToHeadquarter(Request $request)
  {

      if(   $request->remarks == "" ||  $request->frno  == ""  ||  $request->orderno   == ""  )
      {

        return redirect("/franchise/pnd-order/view-details" )
        ->with("err_msg", "Important fields are missing!"); 
      }


      $order_seq = OrderSequencerModel::find($request->orderno); 

      if( !isset($order_seq) )
      {
        return redirect("/franchise/pnd-order/view-details" )
        ->with("err_msg", "No matching order found!"); 
      }  

      $redirect_url ="/franchise";

      $order_seq = OrderSequencerModel::find($request->orderno); 

      if( !isset($order_seq) )
      {
        return redirect("/franchise/pnd-order/view-details" )
        ->with("err_msg", "No matching order found!"); 
      }  

      $redirect_url ="/franchise";


      if($order_seq->type == "normal" || $order_seq->type == "booking")
      {
        $order_info = ServiceBooking::find( $request->orderno ) ;
        $redirect_url ="/franchise/order/view-details/" .  $request->orderno ;

      }
      else if($order_seq->type == "pnd" || $order_seq->type == "assist")
      {
        $order_info = PickAndDropRequestModel::find( $request->orderno ) ;
        $redirect_url ="/franchise/pnd-order/view-details/" .  $request->orderno ;

      }else 
      {
        return redirect("/franchise/pnd-order/view-details" )
        ->with("err_msg", "No matching order found!"); 
      }

      if( !isset($order_info) )
      {
        return redirect("/franchise/pnd-order/view-details" )
        ->with("err_msg", "No order information found!"); 
      }

      if ($order_info->route_to_frno==0) {
        return redirect( $redirect_url )
        ->with("err_msg", "Cannot route to headquarter!"); 
      }  

      $order_info->route_to_frno = $request->frno;
      $order_info->save(); 

      //save remarks
      $remark = new OrderRemarksModel();
      $remark->order_no = $request->orderno ;
      $remark->remarks =  $request->remarks == "" ? "order migration"  : $request->remarks  ;
      $remark->rem_type = "CC Remarks";  
      $remark->remark_by = $request->session()->get('__member_id_' );
      $remark->save(); 

      return redirect( $redirect_url )->with("err_msg",  'Order migrated successfully!'  );  
      
  }



  //route order to franchise
  protected function updateOrderSourceZone(Request $request)
  {
      if(     $request->frno  == ""  ||  $request->orderno   == ""  )
      {

        return redirect("/franchise/orders/normal-orders" )
        ->with("err_msg", "Important fields are missing!"); 
      }


      $order_seq = OrderSequencerModel::find($request->orderno); 

      if( !isset($order_seq) )
      {
        return redirect("/franchise/orders/normal-orders" )
        ->with("err_msg", "No matching order found!"); 
      }  

      $redirect_url ="/franchise";

       

      if($order_seq->type == "normal" || $order_seq->type == "booking")
      {
        $order_info = ServiceBooking::find( $request->orderno ) ;
        $redirect_url ="/franchise/order/view-details/" .  $request->orderno ;

      }
      else if($order_seq->type == "pnd" || $order_seq->type == "assist")
      {
        $order_info = PickAndDropRequestModel::find( $request->orderno ) ;
        $redirect_url ="/franchise/pnd-order/view-details/" .  $request->orderno ;

      }else 
      {
        return redirect("/franchise/pnd-order/view-details" )
        ->with("err_msg", "No matching order found!"); 
      }

      if( !isset($order_info) )
      {
        return redirect("/franchise/orders/normal-orders" )
        ->with("err_msg", "No order information found!"); 
      }

      if ($order_info->route_to_frno==0) {
        return redirect( $redirect_url )
        ->with("err_msg", "Cannot route to headquarter!"); 
      }  

      $order_info->original_franchise = $request->frno;
      $order_info->save(); 

      //save remarks
      $remark = new OrderRemarksModel();
      $remark->order_no = $request->orderno ;
      $remark->remarks =  $request->remarks == "" ? "order zone update"  : $request->remarks  ;
      $remark->rem_type = "Order Remarks";  
      $remark->remark_by = $request->session()->get('__member_id_' );
      $remark->save();  
      return redirect( $redirect_url )->with("err_msg",  'Order zone updated successfully!'  );  
      
  }


   protected function updateTheme(Request $request)
    {

          if ($request->name=="") {
             return redirect("/");
          }
          $frno = $request->session()->get('_frno');
          $theme =  DB::table("ba_theme")  
            ->where('frno',$frno)
            ->where('theme_for',"bookTou-franchise")
            ->first();

          if ($theme) {

             $getTheme = Theme::find($theme->id);

          }else{

            $getTheme = new Theme;

          }

          $getTheme->frno = $frno;
          $getTheme->theme_name = $request->name;
          $getTheme->code = 0;
          $getTheme->theme_for = "bookTou-franchise";

          $save = $getTheme->save();

          if ($save) {
           return response()->json(['success'=>true]);
          }

 

      if($order_seq->type == "normal" || $order_seq->type == "booking")
      {
        $order_info = ServiceBooking::find( $request->orderno ) ;
        $redirect_url ="/franchise/order/view-details/" .  $request->orderno ;

      }
      else if($order_seq->type == "pnd" || $order_seq->type == "assist")
      {
        $order_info = PickAndDropRequestModel::find( $request->orderno ) ;
        $redirect_url ="/franchise/pnd-order/view-details/" .  $request->orderno ;

      }else 
      {
        return redirect("/franchise/pnd-order/view-details" )
        ->with("err_msg", "No matching order found!"); 
      }

      if( !isset($order_info) )
      {
        return redirect("/franchise/pnd-order/view-details" )
        ->with("err_msg", "No order information found!"); 
      }

      if ($order_info->route_to_frno==0) {
        return redirect( $redirect_url )
        ->with("err_msg", "Cannot route to headquarter!"); 
      }  

      $order_info->route_to_frno = $request->frno;
      $order_info->save(); 

      //save remarks
      $remark = new OrderRemarksModel();
      $remark->order_no = $request->orderno ;
      $remark->remarks =  $request->remarks == "" ? "order migration"  : $request->remarks  ;
      $remark->rem_type = "CC Remarks";  
      $remark->remark_by = $request->session()->get('__member_id_' );
      $remark->save(); 

      return redirect( $redirect_url )->with("err_msg",  'Order migrated successfully!'  );  
      
  }



  protected function viewBusinessProfile(Request $request)
  {

    $frno = $request->session()->get('_frno'); 

    $id = $request->busi_id;
    $businessInfo= DB::table("ba_business")
    ->where("id", $id ) 
    ->first();
     
     $businessPayment = DB::table('ba_billing_cycle')
      ->where('bin',$id)
      ->where('month',date('m'))
      ->where('year', date('Y') ) 
    ->first();

     
    $productCategory= DB::table("ba_product_category")
    ->get();
    
    $data = array('business' => $businessInfo, 'category' => $productCategory, 
                  'bin' => $id, 'biz_pay_cycle'=>$businessPayment);
    
    
    return view("franchise.business.business_profile")->with('data',$data);
    
  }


  public function uploadBannerImage(Request $request) 
  {
   
     if(isset($request->btnupload))
     {
          $key = $request->key;
  
          if(  $key == ""  || $request->photo == "")
          {
             return redirect("/franchise/businesses/view-all")->with('err_msg', 'Missing data to perform action.' );
          }
  
          $business = Business::find(   $key) ; 
          if(   !isset($business))
          {
            return redirect("/franchise/businesses/view-all")->with('err_msg',  "No matching business found!");
          }
  
  
          $cdn_url =   config('app.app_cdn')    ; 
          $cdn_path =  config('app.app_cdn_path')  ; // cnd - /var/www/html/api/public
           
  
          $folder_url  =  $cdn_url . '/assets/image/business/bin_'. $key   ;
          $folder_path =  $cdn_path  . '/assets/image/business/bin_'.   $key  ;
  
  
          if( !File::isDirectory($folder_path))
          {
              File::makeDirectory( $folder_path, 0777, true, true);
          }
   
  
          $file = $request->photo; 
          $originalname = $file->getClientOriginalName();
          $extension = $file->extension( );
          $filename = "bb_". time() . ".{$extension}";
          $file->storeAs('uploads',  $filename ); 
          $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename,   $folder_path . "/" . $filename);
  
    
          $business->banner =  '/assets/image/business/bin_'. $key . "/" . $filename ;
          $save=$business->save();
  
          return redirect("/franchise/business/view-profile/" . $key )->with('err_msg',  "Banner image uploaded!");
  
        }
        
        return redirect("/franchise/businesses/view-all");
    
   
     }
    

    protected function viewBusinessProducts(Request $request)
    {
    
      $bin = $request->bin;

      if(isset( $bin ))
      {
          $request->session()->put('_bin_', $request->busi_id ); 
          $id = $request->busi_id;
      }
      else if( $request->session()->has('_bin_' ) )
      {
        $id = $request->session()->get('_bin_' ); 
      }
      else 
      {
         return redirect('franchise/businesses/view-all');
      }

 

      $productCategory= DB::table("ba_product_category")->get();

      if( isset($request->btnSearch) )
      {

        $request->session()->put('_key_', $request->search_key ); 
        $key = $request->search_key  ;
        $products= DB::table("ba_products")
        ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
        ->where("ba_products.bin",$bin)
        ->where("ba_products.category",  $key )
        ->select("ba_products.*", "ba_product_variant.prsubid as productSubId", DB::Raw("'na' image") )
        ->paginate(20);

      }
      else 
      {
        $products= DB::table("ba_products")
        ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
        ->where("ba_products.bin",$bin)
        ->select("ba_products.*", "ba_product_variant.prsubid as productSubId", DB::Raw("'na' image") )
        ->paginate(20);
      }
 

      $prsubids = array(); 
      foreach($products as $item)
      {
        $prsubids[] = $item->productSubId;
      }
      $prsubids[] =0;

      $promotion_products = DB::table("ba_product_promotions")
      ->whereIn("prsubid", $prsubids )
      ->get();


      $product_images =  DB::table("ba_product_photos")  
      ->whereIn("prsubid", $prsubids )
      ->select("id", "prsubid", "image_url as imageUrl" )
      ->get();


      foreach($products as $item)
      {
        $item->image = "https://cdn.booktou.in/assets/image/no-image.jpg";
        foreach( $product_images as $images)
        {
          if( $item->productSubId ==  $images->prsubid )
          {
            $item->image = $images->imageUrl;
            break;
          }
        }
      }  

      $business= Business::find( $bin) ; 
      $data = array('products' => $products, 'business' => $business, 'category' => $productCategory,  'promotion_products' =>$promotion_products, 'bin' => $bin);
      return view("franchise.business.business_products")->with('data',$data); 
  }
 



  public function viewProductImages(Request $request) 
  {

      $bin = $request->bin; 
      $prsubid = $request->prsubid; 

      if(isset( $bin ))
      {
          $request->session()->put('_bin_', $request->bin ); 
          $id = $request->busi_id;
      }
      else if( $request->session()->has('_bin_' ) )
      {
        $id = $request->session()->get('_bin_' ); 
      }
      else 
      {
         return redirect('franchise/businesses/view-all');
      } 

      $business=  Business::find( $bin);

      $product= DB::table("ba_products")
      ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
      ->where("ba_products.bin", $bin )
      ->where("ba_product_variant.prsubid",  $prsubid )
      ->select("ba_products.*", "ba_product_variant.prsubid as productSubId" )
      ->first(); 


      if( !isset($business)  )
      {
        return redirect('franchise/businesses/view-all')->with("err_mgs", "No business found!");
      }

      if(  !isset($product))
      {
        return redirect('franchise/business/view-products/' . $bin )->with("err_mgs", "No matching product found!");
      }

      $product_photos= DB::table("ba_product_photos")
     ->where("prsubid",  $prsubid )
     ->get();


      $category= DB::table("ba_product_category")  
      ->get();

      $data = array( 
      'title'  => 'View Product Image',
      'product' => $product, 
      'category' => $category, 
      'product_photos'=>$product_photos,
      'bin' => $bin, 
      'business' => $business, 
      'prsubid' => $prsubid
     ); 

    return view("franchise.products.view_product_image")->with('data',$data);

   }

  

    public function uploadProductImage(Request $request) 
    {

      $bin = $request->bin; 
      $prsubid = $request->prsubid; 

      if(isset( $bin ))
      {
          $request->session()->put('_bin_', $request->bin ); 
          $id = $request->busi_id;
      }
      else if( $request->session()->has('_bin_' ) )
      {
        $id = $request->session()->get('_bin_' ); 
      }
      else 
      {
         return redirect('franchise/businesses/view-all');
      } 

      $business=  Business::find( $bin);

      $product_info= DB::table("ba_products")
      ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
      ->where("ba_products.bin", $bin )
      ->where("ba_product_variant.prsubid",  $prsubid )
      ->select("ba_products.*", "ba_product_variant.prsubid as productSubId" )
      ->first(); 


      if( !isset($business)  )
      {
        return redirect('franchise/businesses/view-all')->with("err_mgs", "No business found!");
      }

      if(  !isset($product_info))
      {
        return redirect('franchise/business/view-products/' . $bin )->with("err_mgs", "No matching product found!");
      }

      $cdn_url =   config('app.app_cdn')    ;
      $cdn_path =  config('app.app_cdn_path')  ;  
      $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";


      $folder_url  =  $cdn_url . '/assets/image/store/bin_'. $product_info->bin;
      $folder_path =  $cdn_path  . '/assets/image/store/bin_'.   $product_info->bin  ;
      if( !File::isDirectory($folder_path))
      {
          File::makeDirectory( $folder_path, 0777, true, true);
      }

      $i=1;
      foreach($request->file('photos') as $file)
      {

          $originalname = $file->getClientOriginalName();
          $extension = $file->extension( );
          $filename = "prim_" .  $product_info->id . $i . time()  . ".{$extension}";
          $file->storeAs('uploads',  $filename );
          $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);

          $product_photos = new ProductPhotosModel ;
          $product_photos->prid = $product_info->id;
          $product_photos->prsubid = $product_info->productSubId;
          $product_photos->image_url  = $folder_url . "/" . $filename;
          $product_photos->image_path = $filename;
          $product_photos->save();

          $i++;
       }

       return redirect("/franchise/business/view-product-photo/$bin/$prsubid");

   }



  public function removeProductImage(Request $request)
  {

      $bin = $request->bin;  

      if(isset( $bin ))
      {
          $request->session()->put('_bin_', $request->bin ); 
          $id = $request->busi_id;
      }
      else if( $request->session()->has('_bin_' ) )
      {
        $id = $request->session()->get('_bin_' ); 
      }
      else 
      {
          return redirect("/franchise/businesses/view-all")->with('detailed_msg', 'Business information not found.' );
      }
      

      $prsubid = $request->key;
      $imageid = $request->imageid;
      if(  $prsubid == ""   ||  $imageid == "" )
      {
          return redirect("/franchise/business/view-product-photo/$bin/$prsubid")->with('detailed_msg', 'Missing data to perform action.' );
      }

      $product_photo = ProductPhotosModel::find(  $imageid) ;
      if(   !isset($product_photo))
      {
          return redirect("/franchise/business/view-product-photo/$bin/$prsubid")->with("err_mgs", "No product photo found!");
      }

      $product_info =  DB::table("ba_product_photos")
       ->join("ba_products", "ba_products.id", "=", "ba_product_photos.prid")
       ->where("ba_product_photos.id",  $imageid )
       ->select("ba_products.*")
       ->first();

      if(   !isset($product_info))
      {
          return redirect("/all-products")->with("err_mgs", "No product information found!");
      }

      //removing from database
       $product_photo->delete();

      //removing from folder
      $cdn_path =  config('app.app_cdn_path')  ;
      $file_path  = $cdn_path  . '/assets/image/store/bin_'.   $product_info->bin . "/" . $product_photo->image_path  ;



      if($product_photo->image_path  != "" && File::exists($file_path))
      {
          File::delete($file_path);
      }
      else
      {
          $file_path  = $cdn_path  .   $product_photo->image_path  ;
          File::delete($file_path);
      }
      return redirect("/franchise/business/view-product-photo/$bin/$prsubid")->with("err_mgs", "Selected image removed!");
   }

  
}

