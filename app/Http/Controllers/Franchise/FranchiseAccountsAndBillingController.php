<?php

namespace App\Http\Controllers\Franchise; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;
 



use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\DailyDepositModel;
use App\ComplaintModel;

use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\AccountsDailyLogModel;

use App\OrderVoucherModel; 
use App\OrderJournalEntryModel;




use App\Traits\Utilities; 
use PDF;

class FranchiseAccountsAndBillingController  extends Controller
{
    use Utilities;


    protected function dailyAccountLedgerEntry(Request $request)
    {

    	if($request->btnsearch != "")
    	{
    		$date = date('Y-m-d', strtotime($request->datefilter));
    	}
    	else 
    	{
    		$date = date('Y-m-d');
    	}
    	

    	$ledgers = DB::table("ba_accounts_daily_log")
    	->whereDate("account_date", $date  )
    	->get();
 

    	if(isset($request->btnsave))
    	{
    		$dailylog = AccountsDailyLogModel::where("account_name", $request->type)
    		->whereDate("account_date", date('Y-m-d', strtotime( $request->date) ))
    		->first();

    		if( !isset($dailylog))
    		{
    			$dailylog = new AccountsDailyLogModel;
    		} 

    		$dailylog->account_name = $request->type;
    		$dailylog->amount = $request->amount;
    		$dailylog->details = $request->details;
    		$dailylog->entered_by = Session::get('__user_id_') ;
    		$dailylog->account_date = date('Y-m-d', strtotime( $request->date) );
    		$dailylog->entry_date = date('Y-m-d');
    		$dailylog->save();
 
    		return redirect('/admin/accounts/account-ledger-entry')->with("err_msg", "Ledger entry saved!");
    	}


    	$data = array( 
        'title'=>'Account ledger entry' ,  
        'date' => $date,
        'ledgers' =>$ledgers );

         return view("admin.accounts.add_daily_closing")->with( $data);

    }
    


    protected function weeklyPaymentDueMerchants(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }


         $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 2;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                return redirect("/franchise/billing-and-clearance/payment-due-merchants" )->with("err_msg", "Selected month has only 4 weeks of payment."); 
            } 
        }

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "request_by", DB::Raw("sum(total_amount) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "bin", DB::Raw("sum(total_cost) as totalAmount") )
          ->groupBy("bin")
          ->get(); 

          $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

        $data = array(  'businesses' => $all_businesses , 'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales, 'endDay' => $endDay , 'month' =>  $month ,  'year' => $year,'theme'=>$theme ); 
        return view("franchise.billing.payment_due_report")->with(  $data);


    }

 

    protected function weeklySalesAndServiceReport(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }


         $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 2;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                return redirect("/franchise/billing-and-clearance/payment-due-merchants" )->with("err_msg", "Selected month has only 4 weeks of payment."); 
            } 
        }
 
        $frno = $request->session()->get('_frno');
        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", $frno ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" , "ba_business.phone_pri") 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", $frno ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "ba_business.phone_pri"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "request_by", DB::Raw("sum(total_amount) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "bin", DB::Raw("sum(total_cost) as totalAmount") )
          ->groupBy("bin")
          ->get();   

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

        $data = array(  'businesses' => $all_businesses ,
         'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales, 'endDay' => $endDay , 
         'month' =>  $month ,  'year' => $year ,'theme'=>$theme); 
        return view("franchise.billing.weekly_sales_service_report")->with(  $data);


    }


    protected function monthlyServiceReport(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }
  

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "request_by", DB::Raw("sum(total_amount) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "bin", DB::Raw("sum(total_cost) as totalAmount") )
          ->groupBy("bin")
          ->get();   
          $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

        $data = array(  'businesses' => $all_businesses , 
            'pnd_sales' =>$pnd_sales, 
         'normal_sales' => $normal_sales, 
         'month' =>  $month ,  'year' => $year,'theme'=>$theme ); 
        return view("admin.billing.monthly_service_report")->with(  $data);


    }


     // View Business Normal and PnD Sales Report for monthly
     protected function salesAndClearanceReport(Request $request )
     {
        $frno = $request->session()->get('_frno');
        $cycles =  array(1);

        if( isset($request->cycle))
        {
            $cycles =  $request->cycle;
        }
        $bin = $request->bin;  

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        } 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

        $allowed_cycles = true;
        if(count($cycles) > 0)
        {
            //check if adjoining cycles are selected
            
            for($i=0; $i<count($cycles)-1; $i++)
            {
                for($j= $i+1; $j <count($cycles) ; $j++)
                {
                    if( $cycles[$i] + 1 != $cycles[$j]  )
                    {
                        $allowed_cycles = false; 
                    }
                    break;

                } 
            }

            if( count($cycles)  == 1 )
            {
                if($cycles[0]  == 1 )
                {
                    $startDay   =  1 ;  
                    $endDay   =    7;
                    $cycle = 1;
                }
                else if($cycles[0]  == 2 )
                {
                    $startDay   = 8 ;  
                    $endDay   =    14;
                    $cycle = 2;
                }else if($cycles[0]  == 3 )
                {
                    $startDay   = 15 ;  
                    $endDay   =    21;
                    $cycle = 3;
                }else if($cycles[0]  == 4 )
                {
                    $startDay   = 22 ;  
                    $endDay   =    28;
                    $cycle = 4;
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $startDay   = 29;  
                        $endDay   =  $totalDays; 
                    }
                }

            }
            else  //count($cycles)  > 1
            {
                $size = count($cycles)-1;
                
                if($cycles[0]  == 1 )
                {
                    $startDay   =  1 ;  
                }
                else if($cycles[0]  == 2 )
                {
                    $startDay   = 8 ;  
                }else if($cycles[0]  == 3 )
                {
                    $startDay   = 15 ;  
                }else if($cycles[0]  == 4 )
                {
                    $startDay   = 22 ; 
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $startDay   = 29;   
                    }
                    else 
                    {
                        $startDay   = 0;   
                    }
                }
 

                if($cycles[$size]  == 1 )
                { 
                    $endDay   =    7; 
                }
                else if($cycles[$size]  == 2 )
                { 
                    $endDay   =    14; 
                }else if($cycles[$size]  == 3 )
                { 
                    $endDay   =    21; 
                }else if($cycles[$size]  == 4 )
                {  
                    $endDay   =    28; 
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $endDay   =  $totalDays; 
                    }
                    else 
                    {
                        $endDay   = 0;   
                    }
                }

            } 

        }
  

        if(  !$allowed_cycles  )
        {
            return redirect("/franchise/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
            ->with("err_msg", "Billing cycles selected are not adjacent. Report generation blocked."); 
        }
        
        
        $business  =  DB::table("ba_business")  
        ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id") 
        ->where("ba_business_commission.month", $month )
        ->where("ba_business_commission.year", $year ) 
           ->where("ba_business.id", $bin ) 
           ->select("ba_business.id", "ba_business.name",    "ba_business_commission.commission") 
           ->first(); 

 
        if( !isset($business)  )
        {
            return redirect("/franchise/billing-and-clearance/weekly-sales-and-services-report"  )
            ->with("err_msg", "Business not selected. Report generation blocked."); 
        }

 

        $first = DB::table('ba_service_booking')  
        ->where("ba_service_booking.bin", $bin )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 

       $all_business  = DB::table("ba_business") 
       ->get(); 

       $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

       $data =  array(  'business' => $business, 
        'businesses' => $all_business,
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart,
        'bin' => $bin,   
        'cycles' => $cycles,
        'start' =>  $startDay , 
        'end' =>  $endDay , 
        'month' =>  $month , 
        'year' => $year ,
        'theme'=>$theme );
       return view("franchise.billing.sales_report_per_cycle_for_merchant")->with( $data ); 
     }

    
    // View Business Normal and PnD Sales Report for monthly
     protected function migratedOrderSalesAndClearanceReport(Request $request )
     {
        $frno = $request->session()->get('_frno');
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        } 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }
        $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

      

        $startDay   = 1;   
        $endDay   =  $totalDays;  

                     
        $first = DB::table('ba_service_booking') 
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->where("route_to_frno",  $frno  )
        ->where("original_franchise",  0)  
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
            'ba_service_booking.bin', 
          'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business") 
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->where("route_to_frno",  $frno  )
         ->where("original_franchise",  0) 
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select( 'ba_pick_and_drop_order.id',  
            'ba_pick_and_drop_order.request_by as bin', 
            'ba_pick_and_drop_order.service_date',
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

 


        $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 

       $all_business  = DB::table("ba_business") 
       ->join("ba_franchise", "ba_franchise.frno", "=", "ba_business.frno" )
       ->select("ba_business.*", "ba_franchise.zone" )
       ->get(); 

       $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first();

       $data =  array('businesses' => $all_business,
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart,  
        'start' =>  $startDay , 
        'end' =>  $endDay , 
        'month' =>  $month , 
        'year' => $year ,
        'theme'=>$theme );
       return view("franchise.billing.migrated_sales_report_per_cycle_for_merchant")->with( $data ); 
     }


    protected function dailySalesAndServiceReport(Request $request )
    {

        if($request->reportDate == "")
        {
               $date   =  date('Y-m-d');  
        }
        else
        {
            $date   =  date('Y-m-d', strtotime($request->reportDate)); 
        }
         
        $frno = $request->session()->get('_frno');
        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (  $date   ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business")
          ->whereDate("service_date", "=",   $date  )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", $frno ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use ( $date  ) { 

          $query->select('bin' )
          ->from('ba_service_booking') 
          ->whereDate("service_date", "=",   $date  )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", $frno) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business")
        ->whereDate("service_date", "=",   $date  )
        ->whereIn("book_status",  array ( 'completed', 'delivered') )
        ->select( "ba_pick_and_drop_order.*" ) 
        ->get(); 
    

         $normal_sales = DB::table('ba_service_booking')   
        ->whereDate("service_date",   $date  )
        ->whereIn("book_status",  array ( 'completed', 'delivered') )
        ->get(); 

        $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first(); 

        $theme = DB::table("ba_theme")
      ->where("frno",$frno)
      ->where("theme_for","bookTou-franchise")
      ->first(); 

        $data = array(  'businesses' => $all_businesses , 'pnd_sales' =>$pnd_sales,  
            'normal_sales' => $normal_sales,  
            'reportDate' => $date ,'theme'=>$theme); 
        return view("franchise.billing.daily_sales_and_service")->with(  $data);


    }




    // View Business Normal and PnD Sales Report for monthly
     protected function dailySalesAndServiceMerchantReport(Request $request )
     {

        $bin = $request->bin;
        $frno = $request->session()->get('_frno'); 
        
        if($request->reportDate == "")
        {
            $date   =  date('Y-m-d');  
        }
        else
        {
            $date   =  date('Y-m-d', strtotime($request->reportDate)); 
        }
 


        if( $bin == 0 || $bin == "")
        {
            $bin == 0;
            $normals = DB::table('ba_service_booking')   
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $assists = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "customer") 
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id")  
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'assist' orderType"))   ;


            $result = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "business") 
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->union($normals)  
            ->union($assists)  
            ->select('ba_pick_and_drop_order.id', 
            "request_by as requestBy",
            'ba_pick_and_drop_order.service_date',
            "source",
            'total_amount as orderCost', 
            'service_fee as deliveryCharge', 
            DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
            'book_status  as bookingStatus',             
            "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get(); 
        }
        else 
        {
            $first = DB::table('ba_service_booking')  
            ->where("ba_service_booking.bin", $bin )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $result = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "business")
            ->where("request_by", $bin )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->union($first)  
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();

        } 
     
        
        $memberids = $bins = $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;
                $bins[] = $item->requestBy ;

            }
            if($item->orderType == "pnd")
            {
                $bins[] = $item->requestBy ;
            }
            if($item->orderType == "assist")
            {
                $memberids[] = $item->requestBy ;
            }
            
            
        }
        $oids[]= $bins[]= $memberids[]=0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 


        $businesses  =  DB::table("ba_business") 
        ->whereIn("id",  $bins  )  
        ->get();

        $profiles  =  DB::table("ba_profile") 
        ->whereIn("id",  $memberids  )  
        ->get();  

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

       $data =  array(  'businesses' =>$businesses, 
        'profiles' => $profiles,
        'results' =>  $result, 
        'cart_items' => $normal_sales_cart,
        'bin' => $bin ,'reportDate' => $date,'theme'=>$theme );
       return view("franchise.billing.detailed_sales_report_for_day")->with( $data ); 
     }





    public function generateClearanceBill( Request $request)
    {
        $bin = $request->bin; 
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }


        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        if( $request->start !=  "" )
        {
            $startDay   =  $request->start; 
        }

        if( $request->end !=  "" )
        {
            $endDay   =  $request->end; 
        }

        if(   $startDay <=  0 || $endDay <   $startDay  )
        {
            return redirect("/franchise/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )->with("err_msg", 
                    "Selected date range is invalid.");

        }
   


        $first = DB::table('ba_service_booking')  
        ->where("ba_service_booking.bin", $bin )
         ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
            'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ; 

        $orders = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
          "source",
          'total_amount as orderCost', 
          "packaging_cost as packingCost" , 
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();

        $oids = array();
        foreach($orders as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 
 

       $business  = Business::find(  $bin)  ;
       $data =  array(  'business' =>$business,   
        'orders' =>  $orders, 
       'cart_items' =>$normal_sales_cart,
       'bin' => $bin,  
       'startDate' => $startDay,   
       'endDate' => $endDay, 
       'month' => $month, 'year' => $year ); 

       $pdf_file =  "booktou_mcbill_" .time(). '_' . $bin . ".pdf";
       $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ;

         

        if($request->format == "roll")
        {
            $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('admin.billing.clearance_bill_roll_paper' ,  $data  )   ; 
            return view('admin.billing.clearance_bill_roll_paper')->with( $data );
        }
        else  
        { 

            if( strcasecmp($request->download  , "yes" ) == 0)
            { $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('admin.billing.clearance_bill_a4' ,  $data  )   ;
               return $pdf->download( $pdf_file  ); 
               
            }
            else 
            { 
                 $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('admin.billing.clearance_bill_a4' ,  $data  )   ;
                return view('admin.billing.clearance_bill_a4')->with( $data );
            } 
        }
  

    }
    

    protected function monthlySalesServiceEarningReport(Request $request )
    {
        
        $frno = $request->session()->get('_frno');

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

       
        $first = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
            $query->select("id")->from("ba_business")
            ->where("frno",  $frno ); 
          })
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          "bin",
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order') 
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
        ->where("ba_business.frno",  $frno )  
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
            "request_by as bin",
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

       $data =  array(   
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart, 
        'businesses' => $all_business, 
        'month' =>  $month , 
        'year' => $year ,'theme'=>$theme );
       return view("franchise.billing.sales_report_per_month_for_merchant")->with( $data ); 
     }


    public function dailySalesReport(Request $request )
    {
        if($request->todayDate != "")
        {
            $todayDate  =  date('Y-m-d' , strtotime($request->todayDate)) ; 
        }
        else
        {
            $todayDate  = date('Y-m-d' );
        }
         $totalEarning = 0;

        if( strcasecmp($request->payTarget, "merchant")  == 0   )
        {
            $paytarget_where_pnd  = "payment_target  in ('merchant', 'business')";  

        }
        elseif( strcasecmp($request->payTarget, "booktou")  == 0   )
        {
            $paytarget_where_pnd  = "payment_target  in ('booktou' , 'Cash using Agent UPI', 'Cash to Agent')";  

        }
        else   
            {
               $paytarget_where_pnd  = "payment_target  in  ('merchant', 'business', 'booktou' , 'Cash using Agent UPI', 'Cash to Agent')";  
            }


 


        if( strcasecmp($request->payType, "online")  == 0)
        {
            $paytype_where_normal  = "payment_type in ('Pay online (UPI)', 'online')"; 
            $paytype_where_pnd = "pay_mode = 'online'"; 

        }
        else if( strcasecmp($request->payType, "cash")  == 0)
            {
               $paytype_where_normal  = "payment_type in ('Cash on delivery', 'COD', 'POD', 'OTC')"; 
               $paytype_where_pnd = "pay_mode = 'cash'"; 
            }
            else 
            {
                $paytype_where_normal  = "payment_type in ('Pay online (UPI)', 'online', 'Cash on delivery', 'OTC', 'COD', 'POD')"; 
                $paytype_where_pnd = "pay_mode in ('online', 'cash', 'Cash on delivery', 'OTC')"; 
            }
 
            $frno = $request->session()->get('_frno');
       
            $first = DB::table('ba_service_booking')
                ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
                ->wheredate('ba_service_booking.service_date', $todayDate)
                ->whereIn("book_status", array('completed', 'delivered') ) 
                ->where("ba_business.frno", $frno) 
                ->whereRaw($paytype_where_normal )
                ->select('ba_service_booking.id', 
                  DB::Raw("'customer' source"), 
                  "ba_business.id as bin" ,   
                  'total_cost as orderCost', 
                  DB::Raw("'0.00' packingCost"), 
                  'delivery_charge as deliveryCharge', 
                  'discount',  'payment_type  as paymentType',
                  'book_status as bookingStatus', 
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",
                  DB::Raw("'booktou' paymentTarget"),
                  DB::Raw("'normal' orderType")) ;

                $second = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
                ->whereIn("book_status", array('completed', 'delivered') ) 
                ->where("request_from", "customer")
                ->where("ba_users.franchise", $frno) 
                ->whereRaw($paytype_where_pnd )
                ->whereRaw($paytarget_where_pnd )
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"))
                ->orderBy("id", "asc")   ;
         

                $result = DB::table('ba_pick_and_drop_order')
                ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
                ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
                ->whereIn("book_status", array('completed', 'delivered') ) 
                ->where("request_from", "business")
                ->where("ba_business.frno", $frno) 
                ->whereRaw($paytype_where_pnd )
                ->whereRaw($paytarget_where_pnd )
                ->union($first) 
                ->union($second) 
                ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"))
                ->orderBy("id", "asc")  
                ->get();

        


        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 
  
        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();


        $data = array(  'sales' =>  $result , 
            'cart_items' =>$normal_sales_cart,  
            'date' =>  $todayDate,
            'businesses' => $all_business,
            'theme'=>$theme );  

        return view("franchise.billing.sales_report_per_day")->with(  $data);  
    }



  public function monthlyTaxableSalesReport(Request $request) 
  {
        if( $request->month  == "")
        { 
            $month   =  date('m');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
        else 
        {
          $month   =  $request->month ;
          $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
 

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }
 


        $first = DB::table('ba_service_booking')
        ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
        ->whereMonth('ba_service_booking.service_date', $month)
        ->whereYear('ba_service_booking.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("ba_business.frno", "0") 
        ->select('ba_service_booking.id', 
          DB::Raw("'customer' source"), 
          "ba_business.id as bin" ,   
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', 
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",
          DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType"), "book_date", "service_date") ;

        $second = DB::table('ba_pick_and_drop_order')
        ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
        ->whereMonth('ba_pick_and_drop_order.service_date', $month)
        ->whereYear('ba_pick_and_drop_order.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "customer")
        ->where("ba_users.franchise", "0") 
        ->select('ba_pick_and_drop_order.id',  
          "source",
          DB::Raw("'0' bin"),
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  
          'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'pickup_name as name', 
          DB::Raw("'customer' category"), 
          DB::Raw("'0' commission"),       
          "payment_target as paymentTarget",
          DB::Raw("'assist' orderType"), "book_date", "service_date")
        ->orderBy("id", "asc")   ;



        $result = DB::table('ba_pick_and_drop_order')
        ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
        ->whereMonth('ba_pick_and_drop_order.service_date', $month)
        ->whereYear('ba_pick_and_drop_order.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "business")
        ->where("ba_business.frno", "0") 
        ->union($first) 
        ->union($second) 
        ->select('ba_pick_and_drop_order.id',  
          "source","ba_business.id as bin",
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",          
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"), "book_date", "service_date")
        ->orderBy("id", "asc")  
        ->get();


        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 
   
        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

        $data = array(  'sales' =>  $result , 
            'cart_items' =>$normal_sales_cart,  
            'monthname' =>  $monthname,
            'businesses' => $all_business,
            'theme'=>$theme );  

        return view("franchise.reports.monthly_tax_report")->with(  $data);  
 
  }


  public function orderTaxDetails(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("/franchise/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }
 
        
        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
         

 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business,
            'theme'=>$theme );  

        return view("franchise.reports.order_tax_report")->with(  $data);  
 
  }


  public function viewVoucherDetails(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("/franchise/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }
 
        
        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/franchise/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
         

 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business,'theme'=>$theme );  

        return view("admin.reports.order_tax_report")->with(  $data); 
 
  }



  public function prepareVoucherEntry(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("//admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }

        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
          
 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  

        $month = date('m', strtotime($order_info->book_date));
        $year  = date('Y', strtotime($order_info->book_date));

        $account_types = DB::table('bta_accounts')
        ->get() ;


        $balance_sheet = DB::table('bta_order_journal')
        ->join('bta_journal_entry', 'bta_journal_entry.vno', '=', 'bta_order_journal.vno') 
        ->whereMonth("voucher_date", $month  )
        ->whereYear("voucher_date", $year )
        ->where("bta_order_journal.vno", "<=",  $orderno )
        ->select( "bta_journal_entry.ac_key_name" , "bta_journal_entry.rule",
            DB::Raw(" sum(debit) as debit") , 
            DB::Raw(" sum(credit) as credit")  ) 
        ->groupBy("bta_journal_entry.ac_key_name" ) 
        ->groupBy("bta_journal_entry.rule" ) 
        ->get() ;
         
        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business,
            'account_types' =>$account_types ,
            'balance_sheet' => $balance_sheet,
            'theme'=>$theme);  
 

        return view("admin.reports.order_to_voucher_entry")->with(  $data); 
 
  }


 

  public function updateVoucherEntry(Request $request) 
  {
    if( $request->o  == "")
    {
        return redirect("//franchise/accounting/monthly-taxable-sales-report")
        ->with("err_msg", "No order selected!"); 
    }
    else 
    {
        $orderno   =  $request->o ; 
    }
    
    if($orderno < 13876     )
    {
        return redirect("/franchise/accounting/monthly-taxable-sales-report")
        ->with("err_msg", "No order selected!");  
    }

    //finding order
     $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"),  
                      "customer_name as customer",
                      "address",
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source",
                   "drop_name as customer",
                   "drop_address as address",
                  "ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                   "drop_name as customer",
                   "drop_address as address",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
    if( !isset($order_info) )
    {
        return redirect("/franchise/accounting/monthly-taxable-sales-report")->with("err_msg", "No order selected!"); 
    }

    //saving new voucher or updating existing one
    $order_journal = OrderVoucherModel::find( $orderno) ;
    if( !isset($order_journal))
    {
        $order_journal = new OrderVoucherModel ;
    }
    $order_journal->vno  = $orderno ;
    $order_journal->voucher_date = $order_info->book_date;
    $order_journal->cust_name = $order_info->customer;
    $order_journal->cust_address = $order_info->address;
    $order_journal->related_order =$orderno;
    $order_journal->save();


    $cash_dr  = $request->tbcasset;
    $merchant_cr  = $request->tbmerlbt;
    $comm_cr  = $request->tbcommin; 
    $delivery_cr  = $request->tbdelin; 
    $cgst_cr  = $request->tbcgstlbt;
    $sgst_cr  = $request->tbsgstlbt;

    //making voucher entries
    //finding existing entry for cash_asset
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "cash_asset")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno =  $orderno ;
    $voucher_entry->ac_title = "Cash A/c";
    $voucher_entry->ac_key_name = "cash_asset";
    $voucher_entry->ac_type = "asset";
    $voucher_entry->debit = $cash_dr;  
    $voucher_entry->rule= "dr"; 
    $voucher_entry->save();

    //finding existing entry for merchant_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "merchant_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Merchant A/c";
    $voucher_entry->ac_key_name = "merchant_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $merchant_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();


    //finding existing entry for comm_income
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "comm_income")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Commission Income A/c";
    $voucher_entry->ac_key_name = "comm_income";
    $voucher_entry->ac_type = "income"; 
    $voucher_entry->credit= $comm_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();


    //finding existing entry for delivery_income
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "delivery_income")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Delivery Charge A/c";
    $voucher_entry->ac_key_name = "delivery_income";
    $voucher_entry->ac_type = "income"; 
    $voucher_entry->credit= $delivery_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    //finding existing entry for sgst_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "sgst_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Ouput SGST";
    $voucher_entry->ac_key_name = "sgst_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $sgst_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    
    //finding existing entry for cgst_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "cgst_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Output CGST";
    $voucher_entry->ac_key_name = "cgst_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $cgst_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    return redirect("/franchise/accounting/prepare-voucher-entry?o=" . $orderno )
    ->with("err_msg", "Voucher entry updated!");

  }

    protected function cyclewiseSalesServiceReport(Request $request )
    {
         

        $frno = $request->session()->get('_frno');
        $bin = $request->bin; 
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 3;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                $startDay   =  1 ;  
                $endDay   =    7;
                $cycle = 1;
            } 
        }


         $first = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed'))
        ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
            $query->select("id")->from("ba_business")
            ->where("frno",  $frno  ); 
          })  
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          "bin",
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order') 
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
        ->where("ba_business.frno",  $frno )   
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
         ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
            "request_by as bin",
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();

        //report data for weekly cycle


        
        $normal_orders = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed'))
        ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
            $query->select("id")->from("ba_business")
            ->where("frno",  $frno  ); 
          })  
        //->orderBy("ba_service_booking.service_date")
        ->select( 
          DB::Raw("date(service_date) as serviceDate"),
          DB::Raw("sum(total_cost) as salesAmount")
          )
        ->groupBy("serviceDate")
        ->get();

         $pnd_orders = DB::table('ba_pick_and_drop_order') 
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
        ->where("ba_business.frno",  $frno )   
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        //->orderBy("ba_pick_and_drop_order.service_date")
        ->select(
          DB::Raw("date(service_date) as serviceDate"),
          DB::Raw("sum(total_amount) as salesAmount"))
          ->groupBy("serviceDate")  
        ->get(); 

        $assist_orders = DB::table("ba_pick_and_drop_order") 
        ->join("ba_profile", "ba_profile.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereIn("request_from", array ('customer' ) )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed'))
        ->whereIn("ba_profile.id", function($query) use($frno){
            $query->select("profile_id")->from("ba_users")
            ->where("franchise", $frno);
          })
        ->select(
          DB::Raw("date(service_date) as serviceDate"),
          DB::Raw("sum(total_amount) as salesAmount"))
          ->groupBy("serviceDate")  
        ->get(); 
        
        

        $day =  $normal_sales_chart  = $pnd_sales_chart = $assist_sales_chart = array();
         

        for($i=$startDay; $i <= $endDay  ; $i++){
              
              array_push($day, date('Y-m-d', strtotime($year."-".$month."-".$i)));



              foreach($normal_orders as $item)
                {
                   if($item->serviceDate == date('Y-m-d', strtotime($year."-".$month."-".$i))) {
                     $normal_sales_chart[$i] = $item->salesAmount;
                     break;
                   }else{

                    $normal_sales_chart[$i] = 0;
                   }

                }

              foreach($pnd_orders as $item)
                {
                   if($item->serviceDate == date('Y-m-d', strtotime($year."-".$month."-".$i))) {
                     $pnd_sales_chart[$i] = $item->salesAmount;
                     break;
                   }else{

                    $pnd_sales_chart[$i] = 0;
                   }
                }




              foreach($assist_orders as $item)
                {
                   if($item->serviceDate == date('Y-m-d', strtotime($year."-".$month."-".$i))) {
                     $assist_sales_chart[$i] = $item->salesAmount;
                     break;
                   }else{

                    $assist_sales_chart[$i] = 0;
                   }
                }


 
        }

        //dd($normal_sales_chart); 

        //weekly cycle ends here

        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get();  
 
        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

       $data =  array(   
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart, 
        'businesses' => $all_business, 
        'cycle' => $cycle,
        'month' =>  $month , 
        'year' => $year,
        "normal_sales_chart"=> $normal_sales_chart,
        "pnd_sales_chart"=>$pnd_sales_chart ,
        "assist_sales_chart"=>$assist_sales_chart,
        "day"=>$day ,
        "theme"=>$theme);  

       return view("franchise.billing.sales_report_per_cycle")->with( $data ); 
     }

    protected function viewAccountsDashboard(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $month = ($request->month == "") ? date('m') : $request->month;
      $year = ($request->year == "") ? date('Y') : $request->year;

      $all_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->whereIn("ba_profile.id", function($query) use ( $month,$year ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereMonth("log_date",   $month)
          ->whereYear("log_date",   $year); 
        }) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", $frno)
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();

      $profileId = array();
      foreach ($all_agents as $item) {
         
        $profileId[]= $item->id;

      }
      

      $dailyExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  ) 
      ->whereIn("use_by",$profileId)
      ->orWhere("use_by",$frno)
      ->orderBy("use_date", "asc")
      ->select(DB::Raw("sum(amount) as expensesAmount"))
      ->first();

      $totalAdvance = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  )
      ->whereIn("use_by",$profileId)
      ->where("category","advance") 
      ->orderBy("use_date", "asc")
      ->select(DB::Raw("sum(amount) as totalAdvance"))
      ->first();

      $totalReimbursement = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  ) 
      ->whereIn("use_by",$profileId)
      ->where("category","fuel") 
      ->orderBy("use_date", "asc")
      ->select(DB::Raw("sum(amount) as Totalreimbursement"))
      ->first();

      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  ) 
      ->whereIn("deposit_by",$profileId)
      ->orderBy("deposit_date", "asc")
      ->select(
                DB::Raw("sum(amount) as depositAmount"),
                DB::Raw("sum(due_amount) as dueAmount"),
                DB::Raw("sum(extra_amount) as extraAmount"))
      ->first();


      $cash_in_upi =  DB::table("ba_deposit_target")   
        ->whereMonth('verified_date', $month )
        ->whereYear('verified_date', $year  ) 
        ->select(DB::Raw("sum(verified_amount) as upiAmount"))
        ->first();


      $normal_orders= DB::table("ba_service_booking")
        ->whereMonth("ba_service_booking.service_date",$month)
        ->whereYear("ba_service_booking.service_date",$year)
        ->whereIn("ba_service_booking.bin", function($query) use ($frno) {
            $query->select("id")->from("ba_business")
            ->where( "frno",$frno ); 
        })
        ->select("ba_service_booking.id as orderNo",
          "ba_service_booking.bin as bin",
                  "ba_service_booking.book_date as orderDate",
                  "ba_service_booking.total_cost as orderCost",
                  "ba_service_booking.delivery_charge as deliveryCharge",
                  "ba_service_booking.payment_type as paymentType",
                  "ba_service_booking.source",
                  "ba_service_booking.service_date as receiptDate",
                  "ba_service_booking.service_date as payDate",
                  "ba_service_booking.book_status as bookingStatus",
                  DB::Raw("'booktou' paymentTarget"),  
                  DB::Raw("'normal' orderType")); 



        $all_orders = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by") 
        ->where("ba_business.frno",  $frno ) 
        ->whereMonth("ba_pick_and_drop_order.service_date",$month)
        ->whereYear("ba_pick_and_drop_order.service_date",$year)
        ->unionAll($normal_orders)
        ->select("ba_pick_and_drop_order.id as orderNo",
                "ba_pick_and_drop_order.request_by as bin",
                "ba_pick_and_drop_order.book_date as orderDate",
                "ba_pick_and_drop_order.total_amount as orderCost",
                "ba_pick_and_drop_order.service_fee as deliveryCharge",
                "ba_pick_and_drop_order.pay_mode as paymentType",
                "ba_pick_and_drop_order.source",
                "ba_pick_and_drop_order.service_date as receiptDate",
                "ba_pick_and_drop_order.service_date as payDate",
                "ba_pick_and_drop_order.book_status as bookingStatus",
                "ba_pick_and_drop_order.payment_target as paymentTarget",
                DB::Raw("'pnd' orderType")
              )

        ->orderBy('orderNo','asc')
        ->get();

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array(
        'expense'=>$dailyExpense, 
        'reimbursement' => $totalReimbursement, 
        'deposits'=>$all_deposits, 
        'advance'=>$totalAdvance,
        'upi'=>$cash_in_upi,
        'all_orders'=>$all_orders,
        'month'=>$month, 
        'year'=>$year,
        "theme"=>$theme);          

       return view('franchise.accounts.accounts_dashboard')->with($data);
    }




    protected function addCashDeposit(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $businesses = DB::table('ba_business') 
      ->where("frno",$frno)
      ->where('is_block','no')
      ->select()
      ->get();  

       $staffs =  DB::table("ba_profile")   
                  ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
                  ->where("category", "100" )
                  ->where("franchise", $frno )
                  ->where("ba_users.status", "<>", "100" )  
                  ->select("ba_profile.id","ba_profile.fullname" ) 
                  ->get(); 

        $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();  


      return view('franchise.accounts.daily_deposit_entry')->with("data", array("staffs" => $staffs, 'businesses' => $businesses) )->with('theme',$theme);
    }

    protected function saveCashDeposit(Request $request)
    {
      $frno = $request->session()->get('_frno');

      if ($request->btn_save=="save") {
         
        

        $deposit = new DailyDepositModel; 
        $deposit->deposit_from = $request->depositfrom;
        $deposit->deposit_by = $request->depositby;
        $deposit->bin = $request->bin;
        $deposit->details = ( $request->details == "" ? "Deposit log entry" : $request->details);
        $deposit->amount = $request->amount;
        $deposit->deposit_date =  date('Y-m-d', strtotime($request->depodate));
        $deposit->category = $request->category; 
        $save= $deposit->save();


         if ($save) {
          
            return redirect('/franchise/accounts/add-deposit')->with('err_msg','Deposi information saved!');

         }

      }

      return redirect('/franchise/accounts/add-deposit');
    }



    protected function enterDailyExpenses(Request $request)
    {

       $frno = $request->session()->get('_frno');
       $staffs =  DB::table("ba_profile")   
                  ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
                  ->where("category", "100" )
                  ->where("franchise", $frno )
                  ->where("ba_users.status", "<>", "100" )  
                  ->select("ba_profile.id","ba_profile.fullname" ) 
                  ->get();  

      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      return view('franchise.accounts.daily_expenses')->with("data", array("staffs" => $staffs))->with('theme',$theme);
    }

    protected function saveDailyExpenses(Request $request)
    {
      if ($request->btn_save=="save") {
        
         $frno = $request->session()->get('_frno');
         $dailyExpense = new DailyExpenseModel;
         $dailyExpense->expense_for = $request->expensesby;

         if ($request->expensesby=="staff") {
          $dailyExpense->use_by = $request->useby;
         }else{
          $dailyExpense->use_by = $frno;
         }
         

         $dailyExpense->details = $request->details;
         $dailyExpense->amount = $request->amount;
         $dailyExpense->use_date = date('Y-m-d', strtotime($request->expensedate));
         $dailyExpense->category = $request->category; 
         $save = $dailyExpense->save();
         if ($save) {
          
            return redirect('/franchise/accounts/add-expenditure')->with('err_msg','Expense information saved!');

         }

      }

      return redirect('/franchise/accounts/add-expenditure');
    }

    protected function viewExpenditureReport(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $month = ($request->month == "") ? date('m') : $request->month;
      $year = ($request->year == "") ? date('Y') : $request->year;


      $all_staffs = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
      ->whereIn("ba_profile.id", function($query) use ( $month,$year ) { 
          $query->select("staff_id")
          ->from("ba_staff_attendance")
          ->whereMonth("log_date",   $month)
          ->whereYear("log_date",   $year); 
        }) 
      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", $frno)
      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();

      $profileId = array();
      foreach ($all_staffs as $item) {
         
        $profileId[]= $item->id;

      }

      $dailyExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  )
      ->whereIn("use_by",$profileId) 
      ->orderBy("use_date", "asc")
      ->get(); 

      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  ) 
      ->whereIn("deposit_by",$profileId)
      ->orderBy("deposit_date", "asc")
      ->get();

      $bins = array();
      foreach($all_deposits as $deposit)
      {
        $bins[] = $deposit->bin;
      }

      $all_businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->whereIn("id", $bins )
      ->select("id", "name", "locality")
      ->get();

      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array(
        'expense'=>$dailyExpense, 
        'all_staffs' => $all_staffs, 
        'deposits'=>$all_deposits, 
        'all_businesses' =>$all_businesses,
        'month'=>$month, 
        'year'=>$year,
        'theme'=>$theme);          

       return view('franchise.accounts.deposit_expense_report')->with($data);
    }

    protected function viewDepositsDetails(Request $request)
    {
      $frno = $request->session()->get('_frno');
      $month = date('m');
      $year = date('Y');

      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  $frno )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

      $bins = $profile_id = array();

      foreach ($all_staffs as $staffs) {
         $profile_id[] = $staffs->id;
      } 

       $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  ) 
      ->whereIn("deposit_by",$profile_id)
      ->orderBy("deposit_date", "asc")
      ->get();

       foreach($all_deposits as $deposit)
      {
        $bins[] = $deposit->bin;

      }

      $all_businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->whereIn("id", $bins )
      ->select("id", "name", "locality")
      ->get();

      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array(
         
        'deposits'=>$all_deposits, 
        'all_staffs'=>$all_staffs,
        'all_businesses'=>$all_businesses,
        'month'=>$month, 
        'year'=>$year,
        'theme'=>$theme);          


       return view('franchise.accounts.view_all_deposits')->with($data);
    }



    protected function viewExpensesDetails(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $month = date('m');
      $year = date('Y');

     


      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  $frno )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

      $profile_id = array();

      foreach ($all_staffs as $staffs) {
         $profile_id[] = $staffs->id;
      } 

       $dailyExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  ) 
      ->where("use_by",$frno)
      ->orWhereIn("use_by",$profile_id) 
      ->orderBy("use_date", "asc")
      ->get();

      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array(
         
        'expense'=>$dailyExpense, 
        'all_staffs'=>$all_staffs,
        'month'=>$month, 
        'year'=>$year,
        'theme'=>$theme);          


       return view('franchise.accounts.view_all_expenses')->with($data);
    }


    protected function viewAdvanceDetails(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $month = date('m');
      $year = date('Y'); 

      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise", $frno )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

      $profile_id = array();

      foreach ($all_staffs as $item) {
        
        $profile_id[] = $item->id;
      }

      $advanceExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  )
      ->whereIn('use_by', $profile_id  )
      ->where("category","advance") 
      ->orderBy("use_date", "asc")
      ->get();

      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array( 
        'advance'=>$advanceExpense, 
        'all_staffs'=>$all_staffs,
        'month'=>$month, 
        'year'=>$year,
        'theme'=>$theme);          


       return view('franchise.accounts.view_all_advance_or_expenses')->with($data);
    }

    protected function viewReimbursementDetails(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $month = date('m');
      $year = date('Y'); 

      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  $frno )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

      $profile_id = array();

      foreach ($all_staffs as $item) {
        
        $profile_id[] = $item->id;
      }

       $fuelExpense = DB::table('ba_daily_expense') 
      ->whereMonth('use_date', $month )
      ->whereYear('use_date', $year  ) 
      ->where("category","fuel")
      ->WhereIn("use_by",$profile_id) 
      ->orderBy("use_date", "asc")
      ->get();

      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array( 
         'fuelexpense'=>$fuelExpense,  
        'all_staffs'=>$all_staffs,
        'month'=>$month, 
        'year'=>$year,
        'theme'=>$theme);          


       return view('franchise.accounts.view_all_advance_or_expenses')->with($data);
    }

    protected function viewCashInUPIDetails(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $month = date('m');
      $year = date('Y');

      $all_deposits_target = DB::table('ba_deposit_target') 
      ->whereMonth('verified_date', $month )
      ->whereYear('verified_date', $year  )  
      ->get() ;
       
      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array( 'cash_upi'=>$all_deposits_target,  
                     'month'=>$month, 
                     'year'=>$year,
                     'theme'=>$theme);          


      return view('admin.accounts.view_all_deposit_target')->with($data);
    }

    protected function viewDepositsExtraDetails(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $month = date('m');
      $year = date('Y');

      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  $frno )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

     $profile_id = array();

      foreach ($all_staffs as $item) {
        
        $profile_id[] = $item->id;
      }

      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  )
      ->whereIn("deposit_by",$profile_id)
      ->where("category","agent tip")
      ->whereRaw("extra_amount > 0") 
      ->orderBy("deposit_date", "asc")
      ->get();

      //dd($all_deposits);

      $bins = array();
      foreach($all_deposits as $deposit)
      {
        $bins[] = $deposit->bin;
      }

      $all_businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->whereIn("id", $bins )
      ->select("id", "name", "locality")
      ->get();

      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array(
         
        'deposits'=>$all_deposits, 
        'all_staffs'=>$all_staffs,
        'all_businesses'=>$all_businesses,
        'month'=>$month, 
        'year'=>$year,
        'theme'=>$theme);          


       return view('franchise.accounts.view_deposit_extra')->with($data);
    }


    protected function viewDepositsDueDetails(Request $request)
    {

      $frno = $request->session()->get('_frno');
      $month = date('m');
      $year = date('Y'); 

      $all_staffs =  DB::table("ba_profile")   
         ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
         ->where("category", "100" )
         ->where("ba_users.status", "<>", "100" )  
         ->where("ba_users.franchise",  $frno )  
         ->select("ba_profile.id","ba_profile.fullname" )
         ->get();

      $profile_id = array();

      foreach ($all_staffs as $item) {
        
        $profile_id[] = $item->id;
      }

      $all_deposits = DB::table('ba_daily_deposit') 
      ->whereMonth('deposit_date', $month )
      ->whereYear('deposit_date', $year  )
      ->whereIn("deposit_by",$profile_id)
      ->whereRaw("due_amount > 0") 
      ->orderBy("deposit_date", "asc")
      ->get();

      $bins = array();
      foreach($all_deposits as $deposit)
      {
        $bins[] = $deposit->bin;
      }

      $all_businesses = DB::table('ba_business') 
      ->where('is_block','no')
      ->whereIn("id", $bins )
      ->select("id", "name", "locality")
      ->get();

      $theme = DB::table("ba_theme")
        ->where("frno",$frno)
        ->where("theme_for","bookTou-franchise")
        ->first();

      $data = array(
         
        'deposits'=>$all_deposits, 
        'all_staffs'=>$all_staffs,
        'all_businesses'=>$all_businesses,
        'month'=>$month, 
        'year'=>$year,
        'theme'=>$theme);          


       return view('franchise.accounts.view_deposit_due_amount')->with($data);
    }


     
    protected function salesAndClearanceHistory(Request $request )
    {
 
        if($request->year != "")
        {
          $year  = $request->year ; 
        }
        else
        {
          $year  = date('Y' );
        }

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $payment_history =  DB::table("ba_payment_dealings") 
        ->whereMonth('cleared_on', $month )
        ->whereYear('cleared_on', $year )   
        ->orderBy("id", "desc")
        ->paginate(20);

        $bins = array();
        $order_no_str = "";
        foreach($payment_history as $item)
        {
          $order_no_str .=  $item->order_nos . ",";

          $bins[] = $item->bin;
        }

        $order_nos = explode(",", $order_no_str); 
        $order_nos = array_unique(array_filter($order_nos));
 

        $booking_list  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
        ->whereIn("ba_pick_and_drop_order.id", $order_nos )  
        ->select("ba_pick_and_drop_order.*",  "ba_business.name as orderBy" , DB::Raw("'na'  deliverBy") )
        ->get();
  
        $totalEarning= 0;
        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {

           if( $item->book_status != "cancel_by_client" && $item->book_status != "cancel_by_owner" && 
            $item->book_status != "canceled"  && $item->book_status != "returned")
           {
              $totalEarning += $item->total_amount;
           } 
            
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->id; 
        }

        $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                } 
            }   
       }
       $businesses  = DB::table("ba_business")
        ->whereIn("id", $bins ) 
        ->get();


        $data = array("totalEarning" => $totalEarning,  
        'results' =>  $booking_list, 'businesss' =>   $businesses , 'payment_history' => $payment_history);
       return view("franchise.earning_clearance.merchant_clearance_report")->with('data',$data);

     }


     protected function viewClearanceReportMerchantwise(Request $request)
     { 

      if ($request->bin=="") {
                 
        return view('/franchise/businesses/view-all')->with('err_msg','No business selected!');;
    }

    $bin =$request->bin;
    if ($request->paymentid=="")
    {
      return view('/franchise/business/sales-and-clearance-history?bin=' . $bin)->with('err_msg','mismatch information!');;
    }
    
    $id = $request->paymentid;
    $clearance = DB::table('ba_payment_dealings')
    ->where('id',$id)
    ->select()
    ->get();


     
             if ($clearance->isEmpty()) {
               
                  return view('/franchise/sales-and-clearance-history')->with('err_msg','No payment info found!');
     
             }else{
               
               foreach ($clearance as $clearPayment) {
                       
                 $orderNo = explode(',', $clearPayment->order_nos);
                 
               } 
     
             }
            
             $pnd_list  = DB::table("ba_pick_and_drop_order")
             ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
             ->whereIn("ba_pick_and_drop_order.id",$orderNo) 
             ->select( "ba_pick_and_drop_order.id",   
               "source",
               "book_date", 
               "clerance_status",
               "total_amount",
               "service_fee",
               "pay_mode",
               "book_status",
               "fullname as customerName", 
               "payment_target",
               "ba_business.commission",  
               DB::Raw("'na'  deliverBy"), 
               DB::Raw("'pnd' orderType"));
     
             $booking_list  = DB::table("ba_service_booking")
             ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id") 
             ->whereIn("ba_service_booking.id",$orderNo)  
             ->select("ba_service_booking.id",   
                       "source",
                       "book_date", 
                       "clerance_status",
                       "seller_payable as total_amount",
                       "delivery_charge as service_fee",
                       "payment_type as pay_mode",
                       "book_status",
                       "customer_name as customerName" ,
                       DB::Raw("'booktou'  payment_target"),
                       "ba_business.commission",
                       DB::Raw("'na'  deliverBy"), 
                       DB::Raw("'normal' orderType"))
            ->union($pnd_list)
            ->orderBy('id','asc')
            ->get();
      
     
             $data = array('clearanceReport'=>$booking_list,'clearanceid'=>$id,'paid'=>$clearance);
     
             return view('franchise.earning_clearance.sales_clearance_report_merchantwies')->with($data);
     
     }


     public function salesAndServicePerformance(Request $request)
{
    $bin = $request->bin; 

    if($bin == "")
    {
      return redirect("/franchise/businesses/view-all")->with("err_msg", "No business selected!");
    }


    $year =  date('Y');
    $month = date('m');

    $result = DB::table('ba_shopping_basket')
    ->select("ba_shopping_basket.pr_name", 
        DB::Raw('sum(ba_shopping_basket.qty) as quantity ,
                 sum(ba_shopping_basket.qty * ba_shopping_basket.price) as actualPrice, 
                 
                 sum(ba_shopping_basket.cgst) as cGST,
                 sum(ba_shopping_basket.sgst) as sGST '))
    ->join('ba_products',"ba_products.pr_code","=","ba_shopping_basket.pr_code")
    ->join('ba_service_booking',"ba_service_booking.id","=","ba_shopping_basket.order_no")
    ->whereYear('service_date',$year)
    ->whereMonth('service_date',$month)
    ->where('ba_products.bin',"=",$bin)
    ->groupBy('ba_shopping_basket.pr_name')
    //->groupBy('ba_shopping_basket.price')
    ->get();

    $dataResult =  array('serv_list' => $result );

    $months=array(); 
    $sales=0;

    foreach ($result as  $value) {
       $sales+= $value->actualPrice;

    }

     $revenueMonthResult = DB::table('ba_service_booking')
    ->select(  
        DB::Raw('sum(if(month(service_date) = 1, seller_payable,0)) as Jan,
                 sum(if(month(service_date) = 2, seller_payable,0)) as Feb ,
                 sum(if(month(service_date) = 3,seller_payable,0)) as Mar ,
                 sum(if(month(service_date) = 4, seller_payable,0)) as  Apr ,
                 sum(if(month(service_date) = 5, seller_payable,0)) as  May ,
                 sum(if(month(service_date) = 6, seller_payable,0)) as  Jun ,
                 sum(if(month(service_date) = 7, seller_payable,0)) as   Jul ,
                 sum(if(month(service_date) = 8, seller_payable,0)) as  Aug ,
                 sum(if(month(service_date) = 9, seller_payable,0)) as  Sep ,
                 sum(if(month(service_date) = 10, seller_payable,0)) as  Oct ,
                 sum(if(month(service_date) = 11, seller_payable,0)) as  Nov ,
                 sum(if(month(service_date) = 12, seller_payable,0)) as  "Dec" '))
        ->whereYear('service_date',$year)
        ->where('bin',"=",$bin)
        ->get();

        $revdata = array();
        
        foreach ($revenueMonthResult as $rev) {
         array_push(
          $revdata,$rev->Jan,$rev->Feb,$rev->Mar,$rev->Apr,$rev->May,
          $rev->Jun,$rev->Jul,$rev->Aug,$rev->Sep,$rev->Oct,$rev->Nov,
          $rev->Dec);
        }


//pnd commission calculation logic

    $PNDCommisionMonthWiseResult = DB::table('ba_pick_and_drop_order')
    ->join('ba_business','ba_pick_and_drop_order.request_by','=','ba_business.id')
    ->select(  
    DB::Raw(
    'sum(if(month(service_date) = 1,total_amount*commission/100,0)) as Jan,
    sum(if(month(service_date) = 2,total_amount*commission/100,0)) as Feb,
    sum(if(month(service_date) = 3,total_amount*commission/100,0)) as Mar,
    sum(if(month(service_date) = 4,total_amount*commission/100,0)) as Apr,
    sum(if(month(service_date) = 5,total_amount*commission/100,0)) as May,
    sum(if(month(service_date) = 6,total_amount*commission/100,0)) as Jun,
    sum(if(month(service_date) = 7,total_amount*commission/100,0)) as Jul,
    sum(if(month(service_date) = 8,total_amount*commission/100,0)) as Aug,
    sum(if(month(service_date) = 9,total_amount*commission/100,0)) as Sep,
    sum(if(month(service_date) = 10,total_amount*commission/100,0))as Oct,
    sum(if(month(service_date) = 11,total_amount*commission/100,0))as Nov,
    sum(if(month(service_date) = 12,total_amount*commission/100,0)) as "Dec" '))
    ->whereYear('service_date',$year)
    ->where('request_by',$bin)
    ->where(DB::Raw('source="booktou" or source="customer" '))
    ->get();

//setting the variable for commision
    $jantotal=0;$febtotal=0;$martotal=0;$aprtotal=0;
    $maytotal=0;$juntotal=0;$jultotal=0;$augtotal=0;
    $septotal=0;$octtotal=0;$novtotal=0;$dectotal=0;
//variable setting ends here

    foreach ($PNDCommisionMonthWiseResult as $commission) {
      $jantotal = $commission->Jan;
      $febtotal = $commission->Feb;
      $martotal = $commission->Mar;
      $aprtotal = $commission->Apr;
      $maytotal = $commission->May;
      $juntotal = $commission->Jun;
      $jultotal = $commission->Jul;
      $augtotal = $commission->Aug;
      $septotal = $commission->Sep;
      $octtotal = $commission->Oct;
      $novtotal = $commission->Nov;
      $dectotal = $commission->Dec;      
    }

    //ends here


       //pnd revenue months
        $PNDrevenueMonthWiseResult = DB::table('ba_pick_and_drop_order')
    ->select(  
        DB::Raw('sum(if(month(service_date) = 1, total_amount,0)) as Jan,
                 sum(if(month(service_date) = 2, total_amount,0)) as Feb ,
                 sum(if(month(service_date) = 3,total_amount,0)) as Mar ,
                 sum(if(month(service_date) = 4, total_amount,0)) as  Apr ,
                 sum(if(month(service_date) = 5, total_amount,0)) as  May ,
                 sum(if(month(service_date) = 6, total_amount,0)) as  Jun ,
                 sum(if(month(service_date) = 7, total_amount,0)) as   Jul ,
                 sum(if(month(service_date) = 8, total_amount,0)) as  Aug ,
                 sum(if(month(service_date) = 9, total_amount,0)) as  Sep ,
                 sum(if(month(service_date) = 10, total_amount,0)) as  Oct ,
                 sum(if(month(service_date) = 11, total_amount,0)) as  Nov ,
                 sum(if(month(service_date) = 12, total_amount,0)) as  "Dec"  '))
        ->whereYear('service_date',$year)
        ->where('request_by',"=",$bin)
        ->get();

       //echo json_encode($revenueMonthResult);die();

    
    $pnddata=array();
    
    foreach ($PNDrevenueMonthWiseResult as  $pnd) {
      array_push(
      $pnddata,
      $pnd->Jan-$jantotal,
      $pnd->Feb-$febtotal,
      $pnd->Mar-$martotal,
      $pnd->Apr-$aprtotal,
      $pnd->May-$maytotal,
      $pnd->Jun-$juntotal,
      $pnd->Jul-$jultotal,
      $pnd->Aug-$augtotal,
      $pnd->Sep-$septotal,
      $pnd->Oct-$octtotal,
      $pnd->Nov-$novtotal,
      $pnd->Dec-$dectotal);
    }

     

    $PNDrevenueMonthResult = DB::table('ba_pick_and_drop_order')
        ->select(  
        DB::Raw('sum(total_amount) as totalAmount'))
        ->whereYear('service_date',$year)
        ->whereMonth('service_date',$month)
        ->where('request_by',"=",$bin)
        ->get();

    $pndAmount = 0;
        foreach ($PNDrevenueMonthResult as  $value) {
        $pndAmount+= $value->totalAmount;

    }  

     $PNDResult = DB::table('ba_pick_and_drop_order')
     ->join('ba_business','ba_pick_and_drop_order.request_by', '=' ,'ba_business.id')
     ->whereYear('service_date',$year)
     ->whereMonth('service_date',$month)
     ->where('request_by',"=",$bin)
     ->select('cust_remarks','source','commission','total_amount')
     ->get();
    //dd($PNDrevenueMonthResult);die();
    
    $datarevenueResult =  array('rev_list' => $revenueMonthResult );
    $dataPNDResult =  array('pnd_list' => $PNDResult );
    
    return view('franchise.reports.sales_and_service_performance',
        ['revdata'=>$revdata,'pnddata'=>$pnddata])
    ->with($dataResult)
    ->with($dataPNDResult)
    ->with("sales",$sales)
    ->with("pndsales",$pndAmount)
    ->with($datarevenueResult);

    }


    public function eClearanceReport( Request $request)
    {

      
      if( $request->paymentid  != ""  )
      {
         
         $id = $request->paymentid;

              $clearance = DB::table('ba_payment_dealings')
              ->where('id',$id)
              ->select()
              ->get();

             

            if ($clearance->isEmpty()) {
              
                 // return view('admin.earning_clearance.sales_clearance_report_merchantwies')
                 // ->with('err_msg','No payment info found!');

            }else{
             
            
             
              foreach ($clearance as $clearPayment) {
                      
                $orderNo= explode(',', $clearPayment->order_nos);

               }

            
            
            }
     
             
            

            $pnd_list  = DB::table("ba_pick_and_drop_order")
            ->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id") 
            ->whereIn("ba_pick_and_drop_order.id",$orderNo) 
            ->select( "ba_pick_and_drop_order.id",   
              "source",
              "book_date", 
              "clerance_status",
              "total_amount",
              "service_fee",
              "pay_mode",
              "book_status",
              "fullname as customerName", 
              "payment_target",
              "ba_business.commission",  
              DB::Raw("'na'  deliverBy"), 
              DB::Raw("'pnd' orderType"));

            $booking_list  = DB::table("ba_service_booking")
            ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id") 
            ->whereIn("ba_service_booking.id",$orderNo)  
            ->select("ba_service_booking.id",   
                      "source",
                      "book_date", 
                      "clerance_status",
                      "seller_payable as total_amount",
                      "delivery_charge as service_fee",
                      "payment_type as pay_mode",
                      "book_status",
                      "customer_name as customerName" ,
                      DB::Raw("'booktou'  payment_target"),
                      "ba_business.commission",
                      DB::Raw("'na'  deliverBy"), 
                      DB::Raw("'normal' orderType"))
           ->union($pnd_list)
           ->orderBy('id','asc')
           ->get();




      }
      else
      {
        return; 
      }
      
      $public_path = public_path();
      
      $folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $request->bin .  "/"     ;
      $folder_path =   $public_path  .  '/assets/bills/btm-' .  $request->bin .  "/"     ;
      
      if( !File::isDirectory($folder_path)){
            File::makeDirectory( $folder_path, 0777, true, true);
      }
      
      $pdf_file =  "btr-" . $request->bin . ".pdf";
      $save_path = $folder_path .  $pdf_file  ;
      
      $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
        ->setPaper('a4', 'portrait')
        ->loadView('docs.ebill_clearance' ,  array('data' => $booking_list,'clearanceid'=>$id,
        'paid'=>$clearance));


      return $pdf->download('ebill-' . $request->bin  .   mt_rand(0,20)  .    '.pdf');

      // return view('docs.ebill_clearance',  array('data' => $booking_list,'clearanceid'=>$id,'paid'=>$clearance));
    }




    public function generateDailyClearanceBill( Request $request)
    {

        $bin = $request->bin;  
        $frno = $request->session()->get('_frno');

        if($request->reportDate !="" )
        {
          $today = $request->reportDate ;
        }
        else
        {
            $today = date('Y-m-d' );
        }
 
        $business  = Business::find($bin);


        
        if ($frno==0) { //headquarter query
            
            $first = DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("service_date", $today ) 
            ->where("ba_business.frno",$frno)
            ->where("ba_service_booking.route_to_frno",$frno)   
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_service_booking.service_date")
            ->select('ba_service_booking.id', 
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              DB::Raw("'0.00' packingCost"), 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $orders = DB::table('ba_pick_and_drop_order')
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
            ->where("ba_pick_and_drop_order.request_by", $bin )  
            ->where("request_from", "business")
            ->where("service_date", $today )
            ->where("ba_business.frno",$frno)
            ->where("ba_pick_and_drop_order.route_to_frno",$frno)  
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_pick_and_drop_order.service_date")
            ->union($first)  
            ->select('ba_pick_and_drop_order.id', 
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              "packaging_cost as packingCost" , 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();
        }else{  //franchise query with route to franchise orders for normal and pnd
             
            $first = DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("service_date", $today ) 
            ->where("ba_business.frno",$frno) 
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_service_booking.service_date")
            ->select('ba_service_booking.id', 
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              DB::Raw("'0.00' packingCost"), 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ;

            $normal_route_to =  DB::table('ba_service_booking') 
            ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")  
            ->where("ba_service_booking.bin", $bin )
            ->where("service_date", $today ) 
            ->where("ba_service_booking.route_to_frno",$frno) 
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_service_booking.service_date")
            ->select('ba_service_booking.id', 
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              DB::Raw("'0.00' packingCost"), 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $pnd_route_to = DB::table('ba_pick_and_drop_order')
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
            ->where("ba_pick_and_drop_order.request_by", $bin )  
            ->where("request_from", "business")
            ->where("service_date", $today )
            ->where("ba_pick_and_drop_order.route_to_frno",$frno) 
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_pick_and_drop_order.service_date")
            ->select('ba_pick_and_drop_order.id', 
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              "packaging_cost as packingCost" , 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"));

            $orders = DB::table('ba_pick_and_drop_order')
            ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
            ->where("ba_pick_and_drop_order.request_by", $bin )  
            ->where("request_from", "business")
            ->where("service_date", $today )
            ->where("ba_business.frno",$frno) 
            ->where("book_status", "delivered" ) 
            ->orderBy("ba_pick_and_drop_order.service_date")
            ->union($first)  
            ->union($normal_route_to)
            ->union($pnd_route_to)
            ->select('ba_pick_and_drop_order.id', 
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              "packaging_cost as packingCost" , 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();

        }

        $oids = array();
        foreach($orders as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 
 
 
       $franchise = DB::table("ba_franchise")
       ->where("frno",$frno)
       ->first();

       $data =  array(  'business' =>$business,   
        'orders' =>  $orders, 
       'cart_items' =>$normal_sales_cart,
       'bin' => $bin,  
       'reportDate' => $today ,
       'franchise'=>$franchise
       ); 

       $pdf_file =  "booktou_mcbill_" .time(). '_' . $bin . ".pdf";
       $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ;

      
          
                if($request->format == "roll")
                {
                    $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('franchise.billing.daily_clearance_bill_roll_paper' ,  $data  )   ; 
                    return view('franchise.billing.daily_clearance_bill_roll_paper')->with( $data );
                }
                else  
                {


                    if( strcasecmp($request->download  , "yes" ) == 0)
                    { $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('franchise.billing.daily_clearance_bill_a4' ,  $data  )   ;
                       return $pdf->download( $pdf_file  ); 
                       
                    }
                    else 
                    { 
                         $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                        ->setPaper('a4', 'portrait')
                        ->loadView('franchise.billing.daily_clearance_bill_a4' ,  $data  )   ;
                        return view('franchise.billing.daily_clearance_bill_a4')->with( $data );
                    } 
                }

         
    }
}

