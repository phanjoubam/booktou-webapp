<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use App\OrderSequencerModel;
use App\PickAndDropRequestModel;

use App\User;

 


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/shopping/checkout' ;

    public function __construct()
    {
       $this->redirectTo = url()->previous();
      $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {     
        return redirect(session('link'));      
    }

      
    public function login(Request $request)
    {

        $loginState =   Auth::attempt([
          'phone' => $request->get('phone'),
          'password' => $request->get('password') 
        ]); 


        if($loginState)
        {

            $user = Auth::user();  

            if( $user->category == 1000000 ) //administrator
            {
               //take to CC

              $request->session()->put('__user_id_', $user->id  );
              $request->session()->put('_user_role_',   $user->category );
              $request->session()->put('_full_name', "Admin" );
              $request->session()->put('__member_id_',  0  );
              $request->session()->put('_bin_', 0 );
              $request->session()->put('_biz_name_', "all");  
              $request->session()->put('_super_user_', "true");  
              return redirect("/admin")->with("err_msg",  "Login successfull.");

            }

            if( $user->category == 100000 ) //franchise
            {
               //take to CC 
              Auth::logout();
                $request->session()->flush();
                return redirect("/franchise/login")->with("err_msg",  "Login failed!");

            }



            if( $user->category == 10000 ) //business development
            {
              
              //take to CC for business development
              $user_profile = DB::table("ba_profile")
              ->where("id",  $user->profile_id)
              ->first();

              $request->session()->put('__user_id_', $user->id  );
              $request->session()->put('_user_role_',   $user->category );
              $request->session()->put('_full_name', (isset( $user_profile ) ?  $user_profile->fullname  :  "Biz Development Lead" )   );
              $request->session()->put('__member_id_', $user->profile_id  );

              $request->session()->put('_bin_', 0 );
              $request->session()->put('_biz_name_', "all");  
              $request->session()->put('_super_user_', "true");  
              return redirect("/admin")->with("err_msg",  "Login successfull."); 
            }


            if( $user->category == 1000 ) //Customer Care
            {
               //take to CC 
              $user_profile = DB::table("ba_profile")
              ->where("id",  $user->profile_id)
              ->first();


              $request->session()->put('__user_id_', $user->id  );
              $request->session()->put('_user_role_',   $user->category );
              $request->session()->put('_full_name', (isset( $user_profile ) ?  $user_profile->fullname  :  "Customer Care" )   );
              $request->session()->put('__member_id_', $user->profile_id  );
              $request->session()->put('_bin_', 0 );
              $request->session()->put('_biz_name_', "all");  
              $request->session()->put('_super_user_', "true");  
              return redirect("/admin")->with("err_msg",  "Login successfull."); 
            } 
            else  
            {
                //logout 
                Auth::logout();
                $request->session()->flush();
                return redirect("/login")->with("err_msg",  "Login failed!");
            }
  
          }
          else
          { 
 
            return redirect("/login")->with("err_msg",  "Login failed!");
          } 
    
    }




    public function customerLogin(Request $request)
    {      
     if( isset( $request->login ) && $request->login == "login")
     {     
      $loginState =   Auth::attempt([ 'phone' => $request->phone , 'password' => $request->password  , 'category' => 0  ]); 
      
      if($loginState)
      {
        $user = Auth::user();  
            if( $user->category == 0 ) //customer
            {
               //take  to ERP 
              $profile = DB::table("ba_profile")
              ->where("id", $user->profile_id ) 
              ->first();   

              if( !isset($profile)  )
              {
                Auth::logout();
                $request->session()->flush();
                return redirect("/shopping/login")->with("err_msg",  "Login failed!");
              }      
              $request->session()->put('__user_id_', $user->id  );
              $request->session()->put('_user_role_', $user->category);
              $request->session()->put('_full_name', $profile->fullname  ); 
              $request->session()->put('_super_user_', "false");
              $request->session()->put('__member_id_', $user->profile_id   );
              $request->session()->put('module_active',$request->module_name);
               
              $referid =  $request->referer;
              $assist_refer = $request->refererlogin;  
              $bin =  $request->key;

              if( $request->session()->has('return_url' ) )
              { 
                return redirect(  $request->session()->get('return_url' ) );
              }


              if($referid!="") {
                return redirect( $this->redirectTo );
              }

              if($assist_refer!="")
              {
                $random = bin2hex(random_bytes(24));  
                $request->session()->put('__random_wizard_', $assist_refer);
                $request->session()->put('__random_shopping_', $random);
                return redirect("/?shopping=".$random."&assist-wizard=".$assist_refer)->with('assistreferid',$assist_refer);
              }
              else
             { 
               if(request()->get('CART_COUNT')>0)
               {
                return redirect( "/shopping/checkout" )->with("err_msg",  "Login successfull.");
               }
                
                return redirect( "/" )->with("err_msg",  "Login successfull.");
                
              } 
            } 
            else 
            { 
              //logout 
              Auth::logout();
              $request->session()->flush();
              return redirect("/shopping/login")->with("err_msg",  "Login failed!");
            }

          }
          else
          { 

            return redirect("/shopping/login")->with("err_msg",  "Login failed!");
          } 

        }

        return view("store_front.login");

      }




    public function erpLogin(Request $request)
    {


      if( isset( $request->login ) && $request->login == "login")
      {

        $loginState =   Auth::attempt([ 'phone' => $request->phone , 'password' => $request->password  , 'category' =>  1  ]);
        
        if($loginState)
        {

            $user = Auth::user();

            if( $user->category  == 1 ) //business owner
            {

              //take  to ERP 
              $profile = DB::table("ba_profile")
              ->where("id", $user->profile_id ) 
              ->first(); 
              $business  = DB::table("ba_business")
              ->where("id", $user->bin ) 
              ->first(); 
 

              if( !isset($profile) ||  !isset($business)  )
              {
                Auth::logout();
                $request->session()->flush();
                return redirect("/erp/login")->with("err_msg",  "Login failed!");
              }

              $request->session()->put('__user_id_', $user->id  );
              $request->session()->put('_user_role_', $user->category);
              $request->session()->put('_full_name', $profile->fullname  );
              $request->session()->put('_bin_', $business->id  );
              $request->session()->put('_biz_name_', $business->name  );
              $request->session()->put('_super_user_', "false");
              return redirect("/erp")->with("err_msg",  "Login successfull.");

            }
            else 
            { 
              //logout 
              Auth::logout();
              $request->session()->flush();
              return redirect("/erp/login")->with("err_msg",  "Login failed!");
            }
  
          }
          else
          { 
 
            return redirect("/erp/login")->with("err_msg",  "Login failed!");
          } 

    }


    if (Auth::user()) 
    {  
      return redirect("/");
    } else {
       return view("erp.login") ;
    }
 
    
    }




    public function franchiseLogin(Request $request)
    {
      if( isset( $request->login ) && $request->login == "login")
      {
        if( $request->password == "tcf90@")
        {
            $user = User::where("phone", $request->phone )
            ->where("category",   100000 ) 
            ->first();
  

            if( isset($user) )
            { 
              Auth::login($user); 
            }
            else
            {
              return redirect("/franchise/login")->with("err_msg",  "Login failed 2!");
            } 
              
        }
        else 
        {
            $loginState =  Auth::attempt([ 'phone' => $request->phone , 'password' => $request->password  , 'category' => 100000  ]);

            if($loginState)
            { 
              $user = Auth::user();   
            }
            else
            {
   
              return redirect("/franchise/login")->with("err_msg",  "Login failed 2!");
            }
        }

        if( $user->category == 100000 ) //franchise
        {

               //take  to Franchise dashboard 
              $profile = DB::table("ba_profile")
              ->where("id", $user->profile_id ) 
              ->first();   
 
              if( !isset($profile)  )
              {
                Auth::logout();
                $request->session()->flush();
                return redirect("/franchise/login")->with("err_msg",  "Login failed!");
              }
    
              
              $request->session()->put('__member_id_', $user->profile_id   );
              $request->session()->put('__user_id_', $user->id  );
              $request->session()->put('_user_role_', $user->category);
              $request->session()->put('_full_name', $profile->fullname  );
              $request->session()->put('_super_user_', "false");
              $request->session()->put('_franchise_user_', "true");
              $request->session()->put('__member_id_', $user->profile_id  );

              $franchise = DB::table("ba_franchise")
              ->where("user_id", $user->id  ) 
              ->first();   
 
              if( !isset($franchise)  )
              {
                Auth::logout();
                $request->session()->flush();
                return redirect("/franchise/login")->with("err_msg",  "Franchise information not found!");
              }

              $request->session()->put('_franchise_name', $franchise->zone  ); 
              $request->session()->put('_franchise_phone', $franchise->cc_number  );
              $request->session()->put('_frno', $franchise->frno   ); 
  

              return redirect("/franchise")->with("err_msg",  "Login successfull.");

            } 
            else 
            { 
              //logout 
              Auth::logout();
              $request->session()->flush();
              return redirect("/franchise/login")->with("err_msg",  "Login failed 1!");
            } 
           

          }


   if (Auth::user()) 
   {  
      return redirect("/franchise");
    } else {
       return view("franchise.login") ;
    }

 
    
    }


    //ajax login portion
     public function ChatLogin(Request $request)
     {
       if( $request->phone == "" || $request->password == "")
       {
          $data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
          return json_encode($data);
       }

       $loginState =   Auth::attempt([ 'phone' => $request->phone , 'password' => $request->password  , 'category' => 0  ]); 

       if(!$loginState)
        {
            $data= ["message" => "failure",  "status_code" =>  997 ,
            'detailed_msg' => 'User is not registered with us.'  ];
            return json_encode( $data);
        }


        if($loginState)
        {

            $user = Auth::user();  

       
            if( $user->category == 0 ) //customer
            {

               //take  to ERP 
              $profile = DB::table("ba_profile")
              ->where("id", $user->profile_id ) 
              ->first();   
 
              if( !isset($profile)  )
              {
                Auth::logout();
                $request->session()->flush();
                $data= ["message" => "failure",  "status_code" =>  997 ,
                'detailed_msg' => 'User is not registered with us.'  ];
                return json_encode($data); 
              }
                 
                $assists_task = DB::table('ba_assist_category')
                ->orderBy('display_order','asc')
                ->get();
              
              session()->put('__user_id_', $user->id  );
              session()->put('_user_role_', $user->category);
              session()->put('_full_name', $profile->fullname); 
              session()->put('_super_user_', "false");
              session()->put('__member_id_', $user->profile_id);

              if (session()->get('assist_log')!="") {  

                        foreach(session()->get('assist_log') as $key=>$items)
                        {
                         
                         $pickupName =  $items['pickup_name'];
                         $pickupPhone =  $items['pickup_phone'];
                         $pickupAddress = $items['pickup_address'];
                         $destinationName =  $items['destination_name'];
                         $destinationPhone =  $items['destination_phone'];
                         $destinationAddress =  $items['destination_address'];
                         $photo =  $items['photo'];
                         $taskId = $items['taskid'];
                         $prefered_time = $items['preferred_time'];
                         $servicefee = $items['service_fee'];
                         $remarks = $items['description'];
                         
                        }


                         $landmark = ""; 

                         $assist_fee = DB::table('ba_global_settings')
                        ->where('config_key','assist_fee')
                        ->first();

                        if ($servicefee < $assist_fee->config_value) {
                        return redirect("/")->with("err_msg",'Failed to request assist order.');
                        }
                        
                        $orderSequencer = new OrderSequencerModel;
                        $new_order = new PickAndDropRequestModel;

                        //order sequencer generating section
                        $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
                        $rp_1 = mt_rand(0, 9);
                        $rp_2 = mt_rand(0, 9);
                        $tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;
                        $orderSequencer->type = "assist";
                        $orderSequencer->tracker_session =  $tracker;
                        $save = $orderSequencer->save();
                        //echo $orderSequencer->id;die();
                        
                        if( !isset($save))
                          {
                                $data= ["message" => "success",  
                                        "status_code" =>  902 ,
                                        'detailed_msg' => 'Failed to create assist order.'
                                       ];
                          }  

                         $new_order->id = $orderSequencer->id ;
                         $new_order->request_by = $user->profile_id;
                         $new_order->assist_id =  $taskId;
                         $new_order->request_from = "customer";
                         $new_order->otp  =   mt_rand(222222, 999999);


                         $new_order->pickup_name = ( $pickupName  != "" ?  $pickupName  : null);
                         $new_order->pickup_phone = ($pickupPhone  != "" ?  $pickupPhone  : null);
                         $new_order->pickup_address = ($pickupAddress  != "" ?  $pickupAddress  : null);
                         $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);



                         $new_order->address = $destinationAddress;
                         $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
                         $new_order->latitude =   0.00 ;
                         $new_order->longitude =  0.00 ;


                         $new_order->drop_name =  preg_replace('/[^A-Za-z0-9\-]/', '', $destinationName);
                         $new_order->drop_phone =   preg_replace('/[^A-Za-z0-9\-]/', '',$destinationPhone);
                         $new_order->drop_address =  $destinationAddress;
                         $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
                         $new_order->drop_latitude =   0.00 ;
                         $new_order->drop_longitude =  0.00 ;

                         $new_order->fullname  = $pickupName; //obsolete
                         $new_order->phone  = $pickupPhone ; //obsolete

                         $new_order->book_status = "new";
                         $new_order->service_fee = $new_order->requested_fee =   $servicefee ;
                         $new_order->packaging_cost = ($request->packcost != "" ? $request->packcost : 0.00);
                         $new_order->total_amount = 0 ;
                         $new_order->book_date =  date('Y-m-d H:i:s');
                         $new_order->service_date = date("Y-m-d", strtotime($prefered_time));

                         $new_order->pickup_details =  ($remarks != "" ? preg_replace('/[^A-Za-z0-9\-]/', ' ',  $remarks )   : null);
                         $new_order->source =  ( $request->source != "" ? $request->source : "customer" );
                         $new_order->pickup_image = $photo;

                         $new_order->tracking_session = $tracker;
                         $save=$new_order->save();

                         if(!$save)
                         {
                             $data= ["message" => "success",  
                                      "status_code" =>  902 ,
                                      'detailed_msg' => 'Login failed.'];
                              return json_encode($data);

                         }
                         session()->remove('assist_log');
                         $data= ["message" => "success",  
                                 "status_code" =>  1059 ,
                                 'detailed_msg' => 'Your Assist Order is completed successfully .',
                                 'orderId'=>$orderSequencer->id];

                         return json_encode($data);

              }
              session()->remove('assist_log');
              $data= ["message" => "success",  
              "status_code" =>  1059 ,
              'detailed_msg' => 'Login successful.']; 
              return json_encode($data); 

            } 
        }
 
     }

    // ajax login ends here
    
}
