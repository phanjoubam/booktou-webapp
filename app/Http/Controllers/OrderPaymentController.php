<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

use App\Libraries\FirebaseCloudMessage;



use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;

use App\Traits\Utilities; 

class OrderPaymentController  extends Controller
{
	 
  use Utilities;
 

    public function processOrderPaymentStatus(Request $request)
    {
        $gateway_mode =  config('app.gateway_mode');
        $razorpay_payment_id = $request->razorpay_payment_id;
        $razorpay_payment_link_id = $request->razorpay_payment_link_id;
        $payment_status = $request->razorpay_payment_link_status;
        $razorpay_payment_id = $request->razorpay_payment_id;

        if( strcasecmp($payment_status, "paid") == 0)
        {
          $order_info = ServiceBooking::where( "rzp_paylink_id", $razorpay_payment_link_id)->first();
          if( !isset( $order_info)  )
          {
            $data['msg']   ='No order with the payment link found. Please contact bookTou customer care.';
          }

          $order_items  = DB::table("ba_service_booking_details")
          ->where("book_id", $order_info->id )
          ->get();

          //check if this is already paid 
         
            //fetching real data from razorpay
            $gateway_response = $this->fetchRazorPaymentLinkDetails( $razorpay_payment_link_id, $gateway_mode );
            if(  isset($gateway_response) )
            {
              $rzp_order_id  = $gateway_response->order_id; 
              if( isset($gateway_response->payments) && count($gateway_response->payments) > 0)
              {
                $payment_status = $gateway_response['payments'][0]->status;
                $payment_id = $gateway_response['payments'][0]->payment_id; 

                if(  strcasecmp( $payment_status , "captured") == 0 )
                {
                  $order_info->rzp_id = $rzp_order_id;
                  $order_info->rzp_pay_id = $payment_id ;
                  $order_info->book_status = "confirmed";
                  $order_info->payment_status = "paid";
                  $order_info->payment_type ="ONLINE";
                  $order_info->save();
                }
              }
            } 
          
          $data = array('order_info' => $order_info, 'order_items'=> $order_items , 'htmlTitle' => 'Track Your bookTou Order');
          return view("store_front.payment_summary")->with($data);
        }
        else
        {
          return redirect("/page-invalid");
        }

}   
  
}
