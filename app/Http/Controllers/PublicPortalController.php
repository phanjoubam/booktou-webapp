<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class PublicPortalController  extends Controller
{
    
     //Privacy Policy
     public function viewPrivacyPolicy(Request $request)
    {
		
		 return view("privacy_policy");	
    }  

      //Terms & Conditions
     public function viewTermsConditions(Request $request)
    {
		
		 return view("terms_conditions");	
    }  

      //About Us
     public function viewAboutUs(Request $request)
    {
        return view("about_booktou");	
    }       

}
