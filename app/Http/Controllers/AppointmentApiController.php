<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Business; 
use App\ServiceHours;
use App\Traits\Utilities;
use App\User;
use App\BusinessOwner;
use App\ServiceDuration;
use App\BusinessService;
use App\BusinessCategory;
 
use App\CustomerProfileModel;





class AppointmentApiController extends Controller
{
	use Utilities;
	
   public function getDayBooking(Request $request)
   {
	    
	   $user_info =  json_decode($this->getUserInfo($request->userId));
	   
	  if(  $user_info->user_id == "na" )
	  {
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
		return json_encode($data);
	  }
	     
	  $business = Business::find($request->bin);
	  if( !isset($business))
	  {
		
		$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		'detailed_msg' => 'Business information not found!'  ];
		return json_encode( $data);
		
	  }

	if(    $request->bookingDate == ""  )
   {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
   }
 
	$bookDate = date('Y-m-d', strtotime($request->bookingDate)); 


	//show sponsored business
	$day_bookings= DB::table("ba_service_booking")
	->join("ba_users", "ba_users.id", "=", "ba_service_booking.book_by")
	->whereRaw("date(ba_service_booking.book_date) = '" . $bookDate . "'")
	->where("ba_service_booking.bin", $business->id)
	->select("ba_service_booking.id as bookingNo", "book_by as bookingUserId", 
	DB::Raw("'na' bookingUserName"),
	"profile_photo as userProfilePhoto" ,  
	"ba_service_booking.otp as confirmationOTP", "book_status as bookingStatus", 
   	DB::Raw( "'na' appointmentTime"),
	  DB::Raw("'[]' bookedServices") ,
	  "ba_service_booking.book_date as appointmentDate",
	  "ba_service_booking.book_category as categoryId")
	->get();
	
	$custids= array();
	foreach ($day_bookings as $bitem) 
    {
        $custids[] = $bitem->bookingUserId ;
    }

	$userName = CustomerProfileModel::whereIn('id',$custids)->get(); 
 
   foreach($day_bookings as $item)
   {
		if($business->category==1)
		{
			$allBookings =  DB::table("ba_service_booking_details") 
			->join("ba_business_services", "ba_business_services.srv_code", "=", "ba_service_booking_details.service_code")
			->where("book_id", $item->bookingNo) 
			->select("service_time as serviceTime", "srv_name as serviceName", "ba_service_booking_details.status"  )
			->orderBy("service_time")
			->get();
			foreach ($allBookings as $itemTime) 
			{
				$itemTime->serviceTime = date('h:i a',strtotime($itemTime->serviceTime));
			}
			$item->bookedServices =$allBookings;
			$item->appointmentTime = isset( $allBookings ) ? date('h:i a',strtotime($allBookings[0]->serviceTime))  : 'na'  ; 


		}
		else if($business->category==2)		
		{
			//fetch ServiceBooking
		    $cateringBooking = DB::table("ba_catering_booking")
		    ->where("book_id", $item->bookingNo )
		    ->select('book_id as bookingNo','plate','other_request as customerRequest')
		    ->get();
		    
		    $item->bookedServices =$cateringBooking;
		    
            $bookedTimeC = date('h:i a', strtotime($item->appointmentDate));
            $item->appointmentTime = $bookedTimeC;

		}
	   	$bookedDateC = date('d-m-Y', strtotime($item->appointmentDate));
	   	$item->appointmentDate = $bookedDateC;
		
		$item->userProfilePhoto =url('/public/assets/image/member/').   $item->userProfilePhoto ;
		

		foreach ($userName as $name) 
	    {
	        if($item->bookingUserId == $name->id)
	        {
	        	$item->bookingUserName=$name->fullname ;
	        	break;
	        }
	    }
   }
    

    $data= ["message" => "success", "status_code" =>  1021 ,  'detailed_msg' => 'Day booking list fetched.' ,  
	  'results' => $day_bookings ]; 
    return json_encode( $data);
	
  }



public function getDayBookingForCustomer(Request $request)
{
	$user_info =  json_decode($this->getUserInfo($request->userId));
	if(  $user_info->user_id == "na" )
	{
		$data= ["message" => "failure", "status_code" =>  901 , 
		'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
		return json_encode($data);
	}


	$date = date('Y-m-d'); 

	$start_date  =   date('c', strtotime('-60 days'));


	$arrayStatus=["cancel_by_client","cancel_by_owner"]; 

	$day_bookings= DB::table("ba_service_booking")
	->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
	->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")
	->where("ba_service_booking.book_by", $user_info->member_id )
	->where(DB::Raw("date(ba_service_booking.book_date)"),'>=', $start_date) 
	->where(DB::Raw("date(ba_service_booking.book_date)"),'<=', $date) 
	->where(DB::Raw("date(ba_service_booking.book_date)"),'<=', $date) 
	->whereNotIn('ba_service_booking.book_status',$arrayStatus)
	->select(
			"ba_service_booking.id as bookingNo", 
			"ba_service_booking.book_date as appointmentDate", 
			DB::Raw("'na' appointmentTime"), 
			"ba_service_booking.book_by as customerId", 
			DB::Raw("'na' customerName"),
			"ba_service_booking.address as customerAddress",
			"ba_service_booking.otp as serviceOTP",
			DB::Raw("'na' bookingDetails") ,
			"ba_service_booking.bin",
			DB::Raw("'na' businessUserName"), 
			DB::Raw("'na' businessName"), 
			DB::Raw("'na' businessPhone"), 
			DB::Raw("'na' businessAltPhone"), 
			"ba_service_booking.book_status as bookingStatus",
			DB::Raw("'na' businessAddress"), 
			DB::Raw("'na' businessBannerUrl"),
			DB::Raw("'na' businesslatitude"),
			DB::Raw("'na' businesslongitude"), 
			"ba_service_booking.book_category  as businessCategory",
			"ba_business_category.main_type as mainBizType"
		 ) 
		->orderBy('ba_service_booking.book_date', 'desc')
		->get();
			
		
		if(count($day_bookings)<1)
		{
			$data= ["message" => "failure", "status_code" =>  1092 , 'detailed_msg' => 'Booking information fetched has no record.', 
		'results' => array()];
			return json_encode($data);
		} 
		else{
			$bookStatusBoolean = true;
		}
	   
	   $bins = array();
	   $bookids = array();
	   $custids= array();
	   foreach ($day_bookings as $bitem) 
		{
			$bins[] = $bitem->bin;
			$bookids[] = $bitem->bookingNo ;
			$custids[] = $bitem->customerId ;


			$status_parts = explode("_", $bitem->bookingStatus);


			$bitem->bookingStatus = implode(" ", $status_parts);


		}
		
		$bins[]= $bookids[] =$custids[] = 0;  
		$bins = array_unique($bins);
		
		
		$businesses = DB::table("ba_business")
		->whereRaw("id  in (" .  implode(",", $bins) .  " )") 
		->get();
		 
		
		
		$customers = DB::table("ba_profile")  
		->select("id",  "fullname as customerName"  ) 
		->whereRaw("id  in (" .  implode(",", $custids) .  " )") 
		->get();
	
	 
		$serviceBusiness = BusinessService::whereRaw("bin  in (" .  implode(",", $bins) .  " )")->get();		
			 
		foreach ($day_bookings as $aitem) 
		{
			$bookings =  DB::table("ba_service_booking_details")
			->join("ba_service_booking","ba_service_booking.id","=","ba_service_booking_details.book_id")
			->whereRaw("book_id  in (" .  implode(",", $bookids) .  " )") 
			->where('ba_service_booking.otp',$aitem->serviceOTP)
			->whereNotIn('status',$arrayStatus)
			->select("ba_service_booking_details.service_code as serviceCode",DB::Raw("'na' serviceName"), 
			"ba_service_booking_details.service_time as serviceTime", "ba_service_booking_details.status")
			->get();
			
			
			if(count($bookings)>0)
			{	
				$booleanBooking = true;
				foreach ($bookings as $keybook) 
				{
					foreach ($serviceBusiness as $keyService) 
					{
						if($keybook->serviceCode == $keyService->srv_code)
						{
							$keybook->serviceName=$keyService->srv_name;
							break;
							
						}
					}
					$keybook->serviceTime = date('h:i a',strtotime($keybook->serviceTime));

				}
			
			}	
			else{
				$booleanBooking = false;
			}	
			foreach ($businesses as $bitem) 
			{
			 
				if($aitem->bin == $bitem->id)
				{
					$aitem->businessName=$bitem->name;
					$aitem->businessPhone=$bitem->phone_pri;
					$aitem->businessAltPhone=$bitem->phone_alt;
					$aitem->businesslatitude=$bitem->latitude;
					$aitem->businesslongitude=$bitem->longitude;
					//$aitem->businessBannerUrl=$bitem->banner;
					$aitem->businessBannerUrl=URL::to('/public'.$bitem->banner);
				if($bitem->landmark!=null)
				{
					$lan = ", ";
				}
				else
				{
					$lan = "";
				}
				if($bitem->city!=null)
				{
					$cty = ", ";
				}
				else
				{
					$cty = "";
				}
				if($bitem->state!=null)
				{
					$sta = ", ";
				}
				else
				{
					$sta = "";
				}
				if($bitem->pin!=null)
				{
					$pn = ", ";
				}
				else
				{
					$pn = "";
				}
				$aitem->businessAddress=$bitem->locality.$lan.$bitem->landmark.$cty.$bitem->city.$sta.$bitem->state.$pn.$bitem->pin ;	
					break;
						 	
				}
			}
			
			foreach ($customers as $citem) 
			{
				if($aitem->customerId == $citem->id)
				{
					$aitem->customerName = $citem->customerName;	
					break;
				}
			}
			
			
			$aitem->bookingDetails = $bookings; 
			if($booleanBooking) 
			{
				$aitem->appointmentTime = date('h:i a', strtotime($bookings[0]->serviceTime));
			}
			else{
				$aitem->appointmentTime = date('h:i a', strtotime($aitem->appointmentDate));
			}
			
			$aitem->appointmentDate = date('d-m-Y', strtotime($aitem->appointmentDate));
		}

		if($bookStatusBoolean) 
		{
			$data= ["message" => "success", "status_code" =>  1091 , 'detailed_msg' => 'Booking information fetched.', 
		'results' => $day_bookings];
		} 		
		return json_encode($data);  

	}
	  
 
}
