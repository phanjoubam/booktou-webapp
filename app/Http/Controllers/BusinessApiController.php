<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Business;
use App\ServiceDuration;
use App\BusinessService;
use App\BusinessHour;
use App\BusinessPhoto;
use App\Traits\Utilities;
use App\StaffAvailability;
use App\User;
use Hash;
use App\ServiceBooking;
use App\BusinessProduct;
use App\BusinessOwner;
use App\BusinessSales;
use App\ServiceBookingDetails;
use App\BusinessCategory;
use App\MarketingSms;
use App\MarketingSmsDetails;
use App\CustomerProfileModel;
use App\FavouriteBusiness;
use App\ActiveUserLog;
use App\Franchise;



class BusinessApiController extends Controller
{

	use Utilities;
	public function registerBusiness(Request $request)
    {
    $tagg = explode(",",$request->tags);
	foreach ($tagg as $keytag) {
		$tagger[]=ltrim($keytag);
	}
	$taged = implode(",", $tagger);

	$user_info =  json_decode($this->getUserInfo($request->userId));
	if($request->category=="")
	{
		$cat=1;
	}
	else{
		$cat=$request->category;
	}

	$business =Business::find($user_info->bin);
    $business->name = $request->businessName ;
	$business->shop_number = $request->shopNoOrLicenseNo;
    $business->tag_line = $request->tagLine ;
    $business->phone_pri = $request->phonePrimary ;
    $business->phone_alt = $request->phoneAlternate;
    $business->locality = $request->locality;
    $business->landmark = $request->landmark ;
    $business->city = $request->city;
    $business->state = $request->state  ;
    $business->pin = $request->pin ;
	$business->tags = $taged;
	$business->rating =  0;
	$business->latitude =  0;
	$business->longitude =  0;
	$business->category = $cat;
    $business->save();


    $data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business registered successfully.' ];

    return json_encode( $data);

  }


  public function registerBusinessV2(Request $request)
  {
	$tagg = explode(",",$request->tags);
	foreach ($tagg as $keytag) {
		$tagger[]=ltrim($keytag);
	}
	$taged = implode(",", $tagger);

	$user_info =  json_decode($this->getUserInfo($request->userId)); 
	
	
	$frno = 0; 
	if($request->pin != "")
	{
		//franchise search starts
		$franchise_area = DB::table("ba_franchise_areas")
		->join("ba_franchise", "ba_franchise.zone", "=", "ba_franchise_areas.zone_name")
		->where("ba_franchise_areas.pin", $request->pin )
		->select("ba_franchise.frno")
		->first();
			
		if(isset($franchise_area))
		{
			$frno = $franchise_area->frno;
		}
		//franchise search ends 
	}
	
	
	$business = Business::where('phone_pri',$request->phonePrimary)->first();

	if( !isset($business))
	{
		$business = new Business; 
		$business->pin = $request->pin ;
	}
	else 
	{
		//check if pin is present 
		if( $business->pin == null ) $business->pin = $request->pin ; 
	}
	
	if( $request->categoryName  == "" )
	{
		$cat='VARIETY STORE'; 
	}
	else
	{
		$cat= $request->categoryName;
	}
	
	
	$business->latitude =  0;
	$business->longitude =  0;
	$business->rating =  0;
	
	//read cc phone 
	$config_data = DB::table("ba_global_settings")
	->where("config_key", "helpdesk")
	->first();
	
	if( isset($config_data))
	{
		$helpdesk_phone =  $config_data->config_value;
	}
	else 
	{
		$helpdesk_phone =  "918787306375";
	}
	
    $business->name = $request->businessName ;
	$business->shop_number = $request->shopNoOrLicenseNo; 
	$business->sub_module = ( $request->subModule != "" ? $request->subModule : "" )  ; 
	$business->main_module = ( $request->mainModule  != "" ? $request->mainModule : "" )  ;  
    $business->tag_line = $request->tagLine ;
    $business->phone_pri =  $request->phonePrimary ; 
	$business->phone_display = $helpdesk_phone ;
    $business->phone_alt = $request->phoneAlternate;
    $business->locality = $request->locality;
    $business->landmark = $request->landmark ;
    $business->city = $request->city;
	$business->pin = $request->pin;
    $business->state = $request->state  ; 
	$business->tags = $taged;
	$business->category = $cat; 
	$business->frno = $frno;
    $save=$business->save();
	
	if($save)
    {
		//update merchant code
		$business->merchant_code = "btm-" . $business->id ;
		$business->save();
		
		$newuser = DB::table("ba_users")
    	->where("id", $request->userId)
    	->update(['bin' =>  $business->id, 'category' => 1 ]);
		
		$businessOwner = new BusinessOwner;
		$businessOwner->bin=$business->id;
		$businessOwner->user_id=$request->userId;
		$businessOwner->save();

		//get business main type
		$main_module = DB::table("ba_business_category")
    	->where("name", $cat )
    	->first();
		

    	$data= ["message" => "success",
    	"status_code" =>  1011 ,
    	'detailed_msg' => 'Business profile updated.',
    	'bin' => $business->id,
    	'mainModule' =>  isset($main_module) ? $main_module->main_module:  "Shopping"  ];
    }
    else
    {
    	$data= ["message" => "failure", "status_code" =>  1012 ,  'detailed_msg' => 'Business profile update failed. Please retry!', "bin" => 0 ];
    }

    return json_encode( $data); 
  }


  public function checkBusinessStatus(Request $request)
  {

  	$user_info =  json_decode($this->getUserInfo($request->userId));
  	if(  $user_info->user_id == "na" )
  	{
  		$data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
  		return json_encode($data);
    }

    //block if  user is not store owner code # 1 is store owner or business
    if( $user_info->category  != 1  )
    {
    	$data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to business owner.'  ];
    	return json_encode($data);
    }

  	$business = Business::find(  $request->bin  );
    if( !isset($business))
    {

       $setupComplete = "no";
    }
    else
    {
    	if( $business->name == "" ||  $business->phone_pri == "" || $business->locality == ""  ||
    	$business->landmark== "" ||  $business->city  == "" || $business->state == "" ||  $business->pin  == "" ||
    	$business->banner  == ""   )
    	{

    		$setupComplete = "no";
    	}
    	else
    	{

    		$setupComplete = "yes";
    		$show_hours_count  = DB::table("ba_shop_hours")
		    ->where("bin",  $request->bin)
		    ->count();


		    if($show_hours_count == 0 )
		    {
		    	 $setupComplete = "no";
		    }

    	}
    }
    $data= ["message" => "success", "status_code" =>  1011 ,
      'detailed_msg' => 'Business profile completeness fetched.' ,
    'setupComplete' => $setupComplete ];

    return json_encode( $data);

  }

  public function updateProfile(Request $request)
  {
    $business = Business::find($request->bin);
    if( !isset($business))
    {

      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
      'detailed_msg' => 'Business information not found!'  ];
      return json_encode( $data);

    }

    $business->name = $request->businessName ;
    $business->tag_line = $request->tagLine ;
	$business->shop_number = $request->shopNoOrLicenseNo;
    $business->phone_pri = $request->phonePrimary ;
    $business->phone_alt = $request->phoneAlternate;
    $business->locality = $request->locality;
    $business->landmark = $request->landmark ;
    $business->city = $request->city;
    $business->state = $request->state  ;
    $business->pin = $request->pin ;
	$business->tags = $request->tags;
    $business->save();

    $data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business profile completeness.'  ];

    return json_encode( $data);

  }


  public function updateBusinessLocationGPS(Request $request)
  {
    $business = Business::find($request->bin);
    if( !isset($business))
    {

      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
      'detailed_msg' => 'Business information not found!'  ];
      return json_encode( $data);

    }

	if($request->latitude != "" && $request->longitude != "" && $request->latitude != 0 && $request->longitude !=  0  )
	{
		$business->latitude = $request->latitude ;
		$business->longitude = $request->longitude ;
		$business->save();
	}


    $data= ["message" => "success", "status_code" =>  1013 ,  'detailed_msg' => 'Business location updated.'   ];

    return json_encode( $data);

  }






public function addBusinessHour(Request $request)
{

	$business = Business::find($request->bin);
	if( !isset($business))
	{

		$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		'detailed_msg' => 'Business information not found!'  ];
		return json_encode( $data);

	} 

	//$shifts = $request->shifts ; 
	$shifts = (explode(",", $request->shifts)) ;
  


	foreach($shifts as $item)
	{
		$day_parts = explode("-", $item); 

		if(count($day_parts) != 2   )
		{
			continue;
		}

		$exist_count = DB::table("ba_shop_hours")
		->where("bin", $request->bin)
		->where("day_name", $request->dayName)
		->where("day_open", $day_parts[0] )
		->count();

		if( $exist_count > 0)
		{
			continue;
		} 

		$business_hour = new BusinessHour;
		$business_hour->bin = $request->bin;
		$business_hour->day_name = $request->dayName;
		$business_hour->day_open = $day_parts[0];
		$business_hour->day_close = $day_parts[1];
		$business_hour->save();

	}

	$data= ["message" => "success", "status_code" =>  1015 ,  'detailed_msg' => 'Business hour saved.'  ];

	return json_encode( $data);

}



  public function addBusinessPhoto(Request $request)
  {

	$user_info =  json_decode($this->getUserInfo($request->userId));
    if(  $user_info->category == 1 )
    {
    	$folder_url  = '/assets/image/business/binOwner_'. $request->bin ;
		$folder_path = public_path(). '/assets/image/business/binOwner_'. $request->bin;
    }
    else
    {
    	$folder_url  = '/assets/image/business/bin_'. $request->bin ;
		$folder_path = public_path(). '/assets/image/business/bin_'. $request->bin;

    }

    $business = Business::find($request->bin);
    if( !isset($business))
    {
		$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		'detailed_msg' => 'Business information not found!'  ];
		return json_encode( $data);
    }

	if( !File::isDirectory($folder_path)){
        File::makeDirectory( $folder_path, 0777, true, true);
	}
	$businessPhotoId=array();
	$i=1;

	foreach($request->file('photos') as $file)
	{
		$originalname = $file->getClientOriginalName();
		$extension = $file->extension( );

		$filename = "bp" .  $request->bin . $i . time()  . ".{$extension}";
		$file->storeAs('uploads',  $filename );
		$imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
		$file_names[] =  $filename ;


		$business_photo = new BusinessPhoto;
		$business_photo->bin = $request->bin;
		$business_photo->photo_path =  $folder_url . "/" . $filename ;
		$save=$business_photo->save();
		$i++;
		if($save)
		{
			$businessPhotoId[] = $business_photo->id;
		}

    }


	$business_photos  = DB::table("ba_business_photos")
	->select( "photo_path as photoUrl", 'display_order as displaySequence'  )
	->where("bin", $request->bin)
	->whereIn("id",$businessPhotoId)
	->get();
	foreach ($business_photos as $item) {
		$item->photoUrl = URL::to('/public')   . $item->photoUrl;
	}


    $data= ["message" => "success", "status_code" =>  1017 ,  'detailed_msg' => 'Business  photo saved.' ,
    'results' => $business_photos ];

    return json_encode( $data);

  }



  public function viewByServiceType(Request $request)
  {

  	if( $request->bizCategory == "" )
	{
		 $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
	}


	$bizCategory =  $request->bizCategory;
	//search business
	$businesses = DB::table("ba_business")
	->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")
	->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine",
		'ba_business.shop_number as shopNumber',
		'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone',
		"ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin",
		'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' ,
		"ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
		DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"),
		"ba_business.category as businessCategory",  DB::Raw("'0' distance") ,
		"ba_business_category.main_type as mainBusinessType",   "is_open as isOpen" )
	->where("ba_business.category", $bizCategory )
	->paginate(20);


	$final_result = $matched_businesses = array();

	foreach($businesses as $item)
	{
		if($item->profileImgUrl!=null)
	      {
	          $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl);
	      }
	      else
	      {
	          $item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;
	      }

		if($item->bannerImgUrl != null )
		{
			$source = public_path() .  $item->bannerImgUrl;
			$pathinfo = pathinfo( $source  );
				$new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
				$destination  = base_path() .  "/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ;

				if(file_exists(  $source )  )
				{

					$info = getimagesize($source );

				    if ($info['mime'] == 'image/jpeg')
				        $image = imagecreatefromjpeg($source);

				    elseif ($info['mime'] == 'image/gif')
				        $image = imagecreatefromgif($source);

				    elseif ($info['mime'] == 'image/png')
				        $image = imagecreatefrompng($source);


				  $original_w = $info[0];
				  $original_h = $info[1];
				  $thumb_w = 290; // ceil( $original_w / 2);
				  $thumb_h = 200; //ceil( $original_h / 2 );

				  $thumb_img = imagecreatetruecolor($original_w, $original_h);
				  imagecopyresampled($thumb_img, $image,
					                   0, 0,
					                   0, 0,
					                   $original_w, $original_h,
					                   $original_w, $original_h);

					imagejpeg($thumb_img, $destination, 20);
				}
				$item->bannerImgUrl =  URL::to("/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name );

		}
		else
		{
			$item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;
		}


		if($request->userId != "")
		{
			$favorited = FavouriteBusiness::where('user_id',$request->userId)
			->where('bin',$item->bin)->get();
			$item->favourite = count($favorited);
		}
		else
		{
			$item->favourite =  0;
		}


		$matched_businesses[] = $item;
	}



	$final_result['last_page'] = $businesses->lastPage( );
	$final_result['data'] = $matched_businesses  ;


	if(count($final_result)>0)
	{
		$data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business profile fetch.' ,
    'results' => $final_result ];
	}
	else{
		$data= ["message" => "failure", "status_code" =>  1012 ,  'detailed_msg' => 'Business profile could not fetch.' ,
    'results' => array() ];
	}

    return json_encode( $data);

  }
  
  
  public function viewByServiceSearchTags(Request $request)
  {

	if( $request->tags == "" )
	{
 
		//show sponsored business
		$businesses = DB::table("ba_business")
		->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")
		->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine",
                        'ba_business.shop_number as shopNumber',
                      'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone',
                      "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin",
                      'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' ,
                      "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
                      DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"),
                      "ba_business.category as businessCategory",  DB::Raw("'0' distance") , 
					    "ba_business.sub_module as subModule",  
						"ba_business.main_module as mainModule",   
                      "ba_business_category.main_type as mainBusinessType", "is_open as isOpen", 
					  "is_verified as isVerified", "ba_business.allow_self_pickup as allowSelfPickup" )
       	->get();
	}
	else
	{
		$tags = str_replace("'", "\\'", $request->tags);

		$tags = explode(",",  $tags );
		$where_clause = $pr_names =  array();
		if( count($tags) > 0)
		{
			foreach($tags as $item)
			{
				$where_clause[] = " find_in_set('". $item . "', tags ) > 0";

				$pr_names[] = "pr_name like '%" .$item. "%'";
			}

			$bins_result =  DB::table("ba_products")
			->selectRaw( "group_concat(bin) as bin" )
			->whereRaw( implode(" or ", $pr_names) )
			->first();

			$bins = explode(",",  $bins_result->bin);
			$bins[]=0;
			array_unique($bins);



			//search business
			$businesses = DB::table("ba_business")
			->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")
			->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine",
	                        'ba_business.shop_number as shopNumber',
	                      'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone',
	                      "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin",
	                      'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' ,
	                      "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
	                      DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"),
	                      "ba_business.category as businessCategory",  DB::Raw("'0' distance") ,
						   "ba_business.sub_module as subModule",  
						"ba_business.main_module as mainModule",   
	                      "ba_business_category.main_type as mainBusinessType",
	                       "is_open as isOpen", "is_verified as isVerified", "ba_business.allow_self_pickup as allowSelfPickup" )
			->whereRaw( implode(" or ", $where_clause) )
			->orWhereIn("ba_business.id", $bins)
			->orWhere("ba_business.name", $request->tags)
			->orWhere("ba_business.category",  $tags)
	       	->get();
		}

	}

	foreach($businesses as $item)
	{
		if($item->profileImgUrl!=null)
	      {
	          $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl);
	      }
	      else
	      {
	          $item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;
	      }

		if($item->bannerImgUrl != null )
		{
			$source = public_path() .  $item->bannerImgUrl;
			$pathinfo = pathinfo( $source  );
				$new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
				$destination  = base_path() .  "/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ;

				if(file_exists(  $source )  )
				{

					$info = getimagesize($source );

				    if ($info['mime'] == 'image/jpeg')
				        $image = imagecreatefromjpeg($source);

				    elseif ($info['mime'] == 'image/gif')
				        $image = imagecreatefromgif($source);

				    elseif ($info['mime'] == 'image/png')
				        $image = imagecreatefrompng($source);


				  $original_w = $info[0];
				  $original_h = $info[1];
				  $thumb_w = 290; // ceil( $original_w / 2);
				  $thumb_h = 200; //ceil( $original_h / 2 );

				  $thumb_img = imagecreatetruecolor($original_w, $original_h);
				  imagecopyresampled($thumb_img, $image,
					                   0, 0,
					                   0, 0,
					                   $original_w, $original_h,
					                   $original_w, $original_h);

					imagejpeg($thumb_img, $destination, 20);
				}
				$item->bannerImgUrl =  URL::to("/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name );

		}
		else
		{
			$item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;
		}


		if($request->userId != "")
		{
			$favorited = FavouriteBusiness::where('user_id',$request->userId)
			->where('bin',$item->bin)->get();
			$item->favourite = count($favorited);
		}
		else
		{
			$item->favourite =  0;
		}

	}


	if(count($businesses)>0)
	{
		$data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business profile fetch.' ,
    'results' => $businesses ];
	}
	else{
		$data= ["message" => "failure", "status_code" =>  1012 ,  'detailed_msg' => 'Business profile could not fetch.' ,
    'results' => array() ];
	}
    return json_encode( $data);

  }


  public function getServiceDuration(Request $request)
  {

  	$business = Business::find($request->bin);
	if( !isset($business))
	{
		$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,  'detailed_msg' => 'Business information not found!'  ];
		return json_encode( $data);
	}

	//show sponsored business
	$businesshours = DB::table("ba_services_duration")
	->select("id",  "bin","slot_number as slotNo",
	"start_time as startTime", "end_time as endTime" , "status" )
	->get();

    $data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business profile updated.' ,
	 'results' => $businesshours ];
	return json_encode( $data);

  }



   public function getBusinessServices(Request $request)
   {

	  if(    $request->bin == ""  )
	   {
	     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => "No business selected to it's services." ];
	     return json_encode($data);
	   }

	   $business = DB::table("ba_business")
	   ->where("id", $request->bin )
	   ->select("id as bin", "name", "tag_line as tagLine", "phone_pri as phone",
		"locality", "landmark", "city", "state", "pin", "profile_image as profileImgUrl", "banner as bannerImgUrl" , "rating" ,
		DB::Raw("'[]' services")
		)
	   ->first();

	    if( !isset($business))
	    {

	      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
	      'detailed_msg' => 'Business information not found!'  ];
	      return json_encode( $data);

	    }
	    if($business->profileImgUrl!=null)
	    {
	    	$business->profileImgUrl = URL::to('/public'.$business->profileImgUrl);
	    }
	    if($business->bannerImgUrl!=null)
	    {
	    	$business->bannerImgUrl = URL::to('/public'.$business->bannerImgUrl);
	    }


	//show services
	$business_services  = DB::table("ba_business_services")
	->where('bin',$request->bin)
	->select("id as usid", "srv_code as serviceCode",  "srv_name as serviceName" , "srv_details as serviceDetails",
	"pricing", "duration_hr as durationHour" , "duration_min as durationMinute","srv_status as canBeBook" )
	->get();


	$business->services = 	$business_services;

    $data= ["message" => "success", "status_code" =>  1025 ,
	  'detailed_msg' => 'Business profile fetched.' ,
	  'results' => $business ];


    return json_encode( $data);

  }

  public function addTimeSlot(Request $request)
  {

    $user_info =  json_decode($this->getUserInfo($request->userId));
    if(  $user_info->user_id == "na" )
    {
    $data= ["message" => "failure", "status_code" =>  901 ,
    'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
    return json_encode($data);
    }

    $business = Business::find($request->bin);
    if( !isset($business))
    {

      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
      'detailed_msg' => 'Business information not found!'  ];
      return json_encode( $data);

    }


    if($request->shift == "" )
    {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
    }
	$properties=$request['shift'];
	for($i=0;$i<sizeof($properties);$i++)
	{
		$shift=$properties[$i];
		$timeSlot = $shift['shiftBreakUps'];
		$timeHour = $shift['shifts'];
		$chkSlotDuration = ServiceDuration::where('bin',$request->bin)
                ->where('day_name',ucfirst($shift['dayName']))
                ->get();

        if(isset($chkSlotDuration))
        {

            foreach ($chkSlotDuration as $keyidd)
            {
                $serviceData = ServiceDuration::find($keyidd->id);
                $serviceData->delete();
            }


        }

		$chkHour = BusinessHour::where('bin',$request->bin)
                ->where('day_name',ucfirst($shift['dayName']))
                ->get();

	    if(isset($chkHour))
	    {

	        foreach ($chkHour as $khridd)
	        {
	            $hourData = BusinessHour::find($khridd->id);
	            $hourData->delete();
	        }


	    }
	    $inc=1;
     	for($k=0;$k<sizeof($timeSlot);$k++)
     	{
     		$slotTime = $timeSlot[$k];
     		$slot = explode("-",$slotTime);
     		$startTime = date('H:i:s', strtotime($slot[0]));
			$endTime = date('H:i:s', strtotime($slot[1]));
     		$slot = new ServiceDuration;
        	$slot->bin  = $request->bin;
	        $slot->slot_number = $inc;
	        $slot->day_name = ucfirst($shift['dayName']);
	        $slot->start_time = $startTime;
	        $slot->end_time = $endTime;
	        $save =$slot->save();
	        $inc++;
     	}
	    $pos=1;
	    for($j=0;$j<sizeof($timeHour);$j++)
     	{
     		$hourTime=$timeHour[$j];
     		$hour = explode("-",$hourTime);
     		$open = date('H:i:s', strtotime($hour[0]));
			$close = date('H:i:s', strtotime($hour[1]));
			$business_hour = new BusinessHour;
			$business_hour->bin = $request->bin;
			$business_hour->day_name = ucfirst($shift['dayName']);
			$business_hour->shift_name = 'shift'.$pos;
			$business_hour->day_open = $open;
			$business_hour->day_close = $close;
			$business_hour->save();
			$pos++;

     	}

	}



    if(  $save  )
    {
        $message = "success";
        $err_code = 1061;
        $err_msg = "Service slot saved.";
    }
    else
    {
        $message = "failure";
        $err_code = 1062;
        $err_msg = "Service slot could not be saved!";
    }

    $data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg  ];
    return json_encode( $data);

  }

  public function addTimeSlotV2(Request $request)
  {

    $user_info =  json_decode($this->getUserInfo($request->userId));
    if(  $user_info->user_id == "na" )
    {
    $data= ["message" => "failure", "status_code" =>  901 ,
    'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
    return json_encode($data);
    }

    $business = Business::find($request->bin);
    if( !isset($business))
    {

      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
      'detailed_msg' => 'Business information not found!'  ];
      return json_encode( $data);

    }


    if($request->shift == "" )
    {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
    }


   $properties=$request['shift'];
   for($i=0;$i<sizeof($properties);$i++)
   {
     $shift=$properties[$i];
     $shiftNames = $shift['shiftNames'];
     $chkSlotDuration = ServiceDuration::where('bin',$request->bin)
                ->where('day_name',ucfirst($shift['day']))
                ->get();

	        if(isset($chkSlotDuration))
	        {

	            foreach ($chkSlotDuration as $keyidd)
	            {
	                $serviceData = ServiceDuration::find($keyidd->id);
	                $serviceData->delete();
	            }


	        }

		$chkHour = BusinessHour::where('bin',$request->bin)
                ->where('day_name',ucfirst($shift['day']))
                ->get();

	    if(isset($chkHour))
	    {

	        foreach ($chkHour as $khridd)
	        {
	            $hourData = BusinessHour::find($khridd->id);
	            $hourData->delete();
	        }


	    }

     for($j=0;$j<sizeof($shiftNames);$j++)
     {
        $shiftName=$shiftNames[$j];
        $timeSlot = $shiftName['time'];
        $pos=0;
        for($k=0;$k<sizeof($timeSlot);$k++)
        {
        	$shiftTimeSlot=$timeSlot[$k];


        	$slot = new ServiceDuration;
        	$inc = ServiceDuration::where('bin',$request->bin)
                ->where('day_name',ucfirst($shift['day']))->max("slot_number");
	        $slot->bin  = $request->bin;
	        $slot->slot_number = $inc+1;
	        $slot->day_name = ucfirst($shift['day']);
	        $slot->start_time = date('H:i', strtotime($shiftTimeSlot['startTime']));
	        $slot->end_time =  date('H:i', strtotime($shiftTimeSlot['endTime']));
	        $save =$slot->save();
			$open[$pos] = date('H:i', strtotime($shiftTimeSlot['startTime']));
			$close = date('H:i', strtotime($shiftTimeSlot['endTime']));



        }

		$hours = new BusinessHour;
		$hours->bin = $request->bin;
		$hours->day_name = ucfirst($shift['day']);
		$hours->shift_name = ucfirst($shiftName['shiftName']);
		$hours->day_open = $open[0];
		$hours->day_close = $close;
		$hours->save();

     }




   }


    if(  $save  )
    {
        $message = "success";
        $err_code = 1061;
        $err_msg = "Service slot saved.";
    }
    else
    {
        $message = "failure";
        $err_code = 1062;
        $err_msg = "Service slot could not be saved!";
    }

    $data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg  ];
    return json_encode( $data);

  }



  public function addServiceDetails(Request $request)
   {

   $user_info =  json_decode($this->getUserInfo($request->userId));
	  if(  $user_info->user_id == "na" )
	  {
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
		return json_encode($data);
	  }

	$business = Business::find($request->bin);
    if( !isset($business))
    {

      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
      'detailed_msg' => 'Business information not found!'  ];
      return json_encode( $data);

    }


	if(    $request->serviceCode == "" || $request->serviceName == "" ||
	$request->details == ""|| $request->pricing == ""  ||
	$request->durationHour == "" || $request->durationMinute == ""	)
   {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
   }

	//check duplicates
	$exist_count  = DB::table("ba_business_services")
	->where("bin", $request->bin)
	->where("srv_code", $request->serviceCode)
	->select(DB::Raw("count(*) as cnt" ))
	->first();

	if(isset($exist_count) && $exist_count->cnt > 0)
	{
		$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Service details already exists.'  ];
		return json_encode($data);
	}


	$service = new BusinessService;
	$service->bin  = $request->bin;
	$service->srv_code = $request->serviceCode ;
	$service->srv_name = $request->serviceName ;
	$service->srv_details = $request->details ;
	$service->pricing = $request->pricing ;
	$service->duration_hr =  $request->durationHour ;
	$service->duration_min =  $request->durationMinute ;
	$save =$service->save();


	if(	 $save  )
	{
		$message = "success";
		$err_code = 1063;
		$err_msg = "Service information saved.";
	}
	else
	{
		$message = "failure";
		$err_code = 1064;
		$err_msg = "Service information could not be saved!";
	}


	$data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg   ];
    return json_encode( $data);

  }




  public function getAllServiceDetails(Request $request)
   {

   $user_info =  json_decode($this->getUserInfo($request->userId));
	  if(  $user_info->user_id == "na" )
	  {
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
		return json_encode($data);
	  }

	$business = Business::find($request->bin);
    if( !isset($business))
    {

      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
      'detailed_msg' => 'Business information not found!'  ];
      return json_encode( $data);

    }


	$services= DB::table("ba_business_services")
	->where( "bin", $request->bin )
	->select("id as usid",
	"srv_code as serviceCode",
	"srv_name as serviceName",
	"srv_details as details",
	"pricing",
	"duration_hr as durationHour",
	"duration_min as durationMinute" )
	->get();


	$data= ["message" => "success" , "status_code" => "1090" ,  'detailed_msg' => "Services information fetched." , 'results' =>  $services ];
    return json_encode( $data);

  }



   public function removeService(Request $request)
   {

	$user_info =  json_decode($this->getUserInfo($request->userId));
	  if(  $user_info->user_id == "na" )
	  {
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
		return json_encode($data);
	  }


	$business = Business::find($request->bin);
    if( !isset($business))
    {

      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
      'detailed_msg' => 'Business information not found!'  ];
      return json_encode( $data);

    }


	if(    $request->usid  == ""  	)
   {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
   }

	$service = BusinessService::find($request->usid);

	if(isset($service))
	{
		$message = "succcess";
		$err_code = 1065;
		$err_msg = "Service information removed!";
		$service->delete();
	}
	else
	{

		$message = "failure";
		$err_code = 999;
		$err_msg =  'Service information not found!'  ;

	}

	$data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg  ];
    return json_encode( $data);

  }




	public function getDayAppointments(Request $request)
   {

	$user_info =  json_decode($this->getUserInfo($request->userId));
	  if(  $user_info->user_id == "na" )
	  {
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
		return json_encode($data);
	  }

	$business = Business::find($request->bin);
    if( !isset($business))
    {

      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
      'detailed_msg' => 'Business information not found!'  ];
      return json_encode( $data);

    }

	$bookings = DB::table("ba_service_booking")
	->join("ba_users", "ba_users.id", "=", "ba_service_booking.book_by")
	->whereRaw( "date(book_date) ='" . date('Y-m-d', strtotime($request->serviceDate))  . "'")
	->where("ba_service_booking.bin", $request->bin)
	->select("slot_no as slotNo",
	DB::Raw("'na' serviceStart"),
	DB::Raw("'na' serviceEnd"),
	DB::Raw("'na' currentStatus"),
	"srv_code as serviceName",
	DB::Raw("REPLACE( CONCAT( fname , ' ', COALESCE( mname,''), ' ', lname ) , '  ', ' ') as  customerName ")  )
	->get();



	$services  = DB::table("ba_business_services")
	->where("bin", $request->bin)
	->get();



	$services_slots  = DB::table("ba_services_duration")
	->where("bin", $request->bin)
	->get();


	foreach($bookings as $item)
	{
		foreach($services as $sitem)
		{
			if($item->serviceName == $sitem->srv_code)
			{
				$item->serviceName =$sitem->srv_name;
				break;
			}
		}

		foreach($services_slots as $slitem)
		{
			if($item->slotNo == $slitem->slot_number)
			{
				$item->serviceStart =$slitem->start_time;
				$item->serviceEnd =$slitem->end_time;
				$item->currentStatus =$slitem->status;
				break;
			}
		}

	}


	$data= ["message" => "success" , "status_code" => "1091" ,  'detailed_msg' => "Booking information fetched." ,
	'results' =>  $bookings ];
    return json_encode( $data);

  }



  protected function  getLandingPageAds(Request $request)
  {

	  $businesses =  DB::table("ba_business")
	  ->join("ba_ads", "ba_ads.bin", "=", "ba_business.id")
	  ->select(  "name", "tag_line", "locality", "landmark", "city", "state", "pin",
	  "profile_image as profileImgUrl",   "banner as bannerImgUrl"  )
	  ->whereRaw("date(ba_ads.valid_till) >= '" . date('Y-m-d') . "'"  )
	  ->get();


		foreach($businesses as $item)
		{
			$item->profileImgUrl = URL::to('/public'.$item->profileImgUrl);
			$item->bannerImgUrl = URL::to('/public'.$item->bannerImgUrl);
		}

		$data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Sponsored ads fetched.' ,
		'results' => $businesses ];

		return json_encode( $data);

  }

  public function getServiceDays(Request $request)
   {


	  if(    $request->bin == ""  )
   {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => "No business selected to it's services." ];
     return json_encode($data);
   }

	   $business = DB::table("ba_business")
	   ->where("id", $request->bin )
	   ->select("id as bin", "name", "tag_line as tagLine", "phone_pri as phone",
		"locality", "landmark", "city", "state", "pin", "profile_image as profileImgUrl", "banner as bannerImgUrl" , "rating" ,
		DB::Raw("'[]' services")
		)
	   ->first();

    if( !isset($business))
    {
		$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
	      'detailed_msg' => 'Business information not found!'  ];
		  return json_encode( $data);
    }

	//show services

	$fbh = array(
		array("dayName" => "Monday", "shiftBreakUps" => array() , "shifts" => array()),
		array("dayName" => "Tuesday", "shiftBreakUps" => array() , "shifts" => array()),
		array("dayName" => "Wednesday", "shiftBreakUps" => array() , "shifts" => array()),
		array("dayName" => "Thursday", "shiftBreakUps" => array() , "shifts" => array()),
		array("dayName" => "Friday", "shiftBreakUps" => array() , "shifts" => array()),
		array("dayName" => "Saturday", "shiftBreakUps" => array() , "shifts" => array()),
		array("dayName" => "Sunday", "shiftBreakUps" => array() , "shifts" => array())
		);


	$business_duration = DB::table("ba_services_duration")
	->where("bin", $request->bin)
	->select( "day_name as dayName", DB::Raw("  CONCAT( start_time , '-',  end_time) as  shiftBreakUps") )
	->orderby('slot_number','ASC')
	->get();


	for($i=0; $i < sizeof($fbh) ; $i++)
	{
		$j = 0;
		foreach($business_duration as $item)
		{
			if(strcasecmp($fbh[$i]['dayName'], $item->dayName) == 0 )
			{
				$first[$i] = explode("-", $item->shiftBreakUps);
                $open = date('h:i A', strtotime($first[$i][0]));
                $close = date('h:i A', strtotime($first[$i][1]));
                $joinTwoItem = $open. " - " .$close;
				$fbh[$i]['shiftBreakUps'][$j] = $joinTwoItem;
				$j++;


			}
		}
	}
	$business_hours = DB::table("ba_shop_hours")
	->where("bin", $request->bin)
	->select( "day_name as dayName", DB::Raw("  CONCAT( day_open , '-',  day_close) as  shifts") )
	->get();


	for($i=0; $i < sizeof($fbh) ; $i++)
	{
		$j = 0;
		foreach($business_hours as $item)
		{
			if(strcasecmp($fbh[$i]['dayName'], $item->dayName) == 0 )
			{
				$first[$i] = explode("-", $item->shifts);
                $open = date('h:i A', strtotime($first[$i][0]));
                $close = date('h:i A', strtotime($first[$i][1]));
                $joinTwoItem = $open. " - " .$close;
				$fbh[$i]['shifts'][$j] = $joinTwoItem;
				$j++;


			}
		}
	}




	$data= ["message" => "success", "status_code" =>  1029 ,
	'detailed_msg' => 'Business hours fetched.' ,
	'results' => $fbh ];
	return json_encode( $data);

  }
  public function getBusinessShift(Request $request)
   {


	  if(    $request->bin == ""  )
   {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => "No business selected to it's services." ];
     return json_encode($data);
   }

	   $business = DB::table("ba_business")
	   ->where("id", $request->bin )
	   ->select("id as bin", "name", "tag_line as tagLine", "phone_pri as phone",
		"locality", "landmark", "city", "state", "pin", "profile_image as profileImgUrl", "banner as bannerImgUrl" , "rating" ,
		DB::Raw("'[]' services")
		)
	   ->first();

    if( !isset($business))
    {
		$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
	      'detailed_msg' => 'Business information not found!'  ];
		  return json_encode( $data);
    }

	//show services

	$fbh = array(
		array("dayName" => "Monday", "shifts" => array() ),
		array("dayName" => "Tuesday", "shifts" => array() ),
		array("dayName" => "Wednesday", "shifts" => array() ),
		array("dayName" => "Thursday", "shifts" => array() ),
		array("dayName" => "Friday", "shifts" => array() ),
		array("dayName" => "Saturday", "shifts" => array() ),
		array("dayName" => "Sunday", "shifts" => array() )
		);


	$business_hours = DB::table("ba_shop_hours")
	->where("bin", $request->bin)
	->select( "day_name as dayName", DB::Raw("  CONCAT( day_open , '-',  day_close) as  shifts") )
	->get();


	for($i=0; $i < sizeof($fbh) ; $i++)
	{
		$j = 0;
		foreach($business_hours as $item)
		{
			if(strcasecmp($fbh[$i]['dayName'], $item->dayName) == 0 )
			{
				$first[$i] = explode("-", $item->shifts);
                $open = date('h:i A', strtotime($first[$i][0]));
                $close = date('h:i A', strtotime($first[$i][1]));
                $joinTwoItem = $open. " - " .$close;
				$fbh[$i]['shifts'][$j] = $joinTwoItem;
				$j++;


			}
		}
	}




	$data= ["message" => "success", "status_code" =>  1029 ,
	'detailed_msg' => 'Business hours fetched.' ,
	'results' => $fbh ];
	return json_encode( $data);

  }




	 public function getAllReviews(Request $request)
   {



	   $business_reviews = DB::table("ba_business_review")
	   ->select("bin", "title", "body", "rating", "added_on as addedOn", "response", "responded_on as respondedOn" )
	   ->get();

		foreach($business_reviews as $item)
		{
			if($item->response == null)
			{
				$item->response = "na";
			}

			if($item->respondedOn == null)
			{
				$item->respondedOn = "na";
			}

		}

	$data= ["message" => "success", "status_code" =>  1041 ,
	'detailed_msg' => 'Business reviews are fetched.' ,
	'results' => $business_reviews];
	return json_encode( $data);

  }


  public function getAllPortfolioImages(Request $request)
  {



	   $business_reviews = DB::table("ba_business_photos")
	   ->where("bin",$request->bin )
	   ->select("id",   "photo_path as photo", "caption", "display_order as displayOrder"   )
	   ->get();
	   foreach ($business_reviews as $keyphoto) {
	   		$keyphoto->photo =  url('/public')  .  $keyphoto->photo;
	   }
	   if(count($business_reviews)>0)
	   {
	   		$data= ["message" => "success", "status_code" =>  1045 ,
	'detailed_msg' => 'Business portfolio fetched.' ,
	'results' => $business_reviews];
	   }
	   else{
	   	$data= ["message" => "failure", "status_code" =>  1046 ,
	'detailed_msg' => 'Business portfolio fetched has no record.'  ];
	   }


	return json_encode( $data);

  }



  //get list of staffs under a business
  public function getAllStaffs(Request $request)
  {
		$staffidd=array();
		$staffs = DB::table("ba_users")
		->select("id"  )
		->where("bin", $request->bin)
		->where("category", "1")
		->first();

		$staffidd[]=$staffs->id;

		$staff_available = StaffAvailability::where("bin",  $request->bin )
		    ->select('staff_id as staffId')
		  ->get();

		foreach ($staff_available as $item) {

		    $staffidd[]=$item->staffId;

		}

		$userData = User::whereIn('id',$staffidd)
		->select("id","fname","mname","lname")->get();
		foreach ($userData as $itemU) {
			$staffId=$itemU->id;
			if($itemU->mname!=null)
            {
                $staffName=$itemU->fname ." ". $itemU->mname." ".$itemU->lname ;
            }
            else
            {
                $staffName=$itemU->fname ." ".$itemU->lname ;
            }
            $collect=array("staffId"=>$staffId,"staffName"=>$staffName);
            $listUserStaff[]=$collect;

		}




	$data= ["message" => "success", "status_code" =>  1075 ,
	'detailed_msg' => 'All staffs are fetched.' ,
	'results' => $listUserStaff ];
	return json_encode( $data);

  }


  //add staffs under a business
  public function saveStaffAvailability(Request $request)
  {

	$user_info =  json_decode($this->getUserInfo($request->userId));
	if($user_info->user_id == "na" )
	{
		$data= ["message" => "failure", "status_code" =>  901 ,
		'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
		return json_encode($data);
	}
	if($user_info->bin == 0 )
	{
		$data= ["message" => "failure", "status_code" =>  999 ,
	      'detailed_msg' => 'Business information not found!'  ];
	      return json_encode( $data);
	}
	else if(  $user_info->category == 1 )
    {
    	$folder_url  = '/m_'. $user_info->bin ;
        $folder_path = public_path(). '/assets/image/member/m_'. $user_info->bin;
    }
	if($request->name == "" || $request->address == "" || $request->phone == "")
	{
		$err_msg = "Field missing!";
		$err_code = 9000;


		$data = array(
			"message" => "failure" , 'status_code' => $err_code ,
			'detailed_msg' => $err_msg) ;

		return json_encode( $data );
	}
	$userInfo = User::where("phone", $request->phone)->first();
	if(isset($userInfo))
    {
    	$data= ["message" => "failure", "status_code" =>  1000 , 'detailed_msg' => 'User Account is already exists!'  ];
     	return json_encode($data);
    }
    if( !File::isDirectory($folder_path)){
        File::makeDirectory( $folder_path, 0777, true, true);
    }
    $file = $request->file('photo');
    if($file==null)
    {
        $data= ["message" => "failure", "status_code" =>  1018 ,  'detailed_msg' => 'Please select your file.'];
        return json_encode( $data);
    }
    $originalname = $file->getClientOriginalName();
    $extension = $file->extension( );
    $filename = "pSf_" .  $request->userId .  time()  . ".{$extension}";
    $file->storeAs('uploads',  $filename );
    $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
    $file_names[] =  $filename ;

    $randpwd = $request->phone.'2001';
	$password = $randpwd;
	$hashed = Hash::make($password);
	$staffName = $request->name;
	$arr2 = explode(" " , $staffName);
	$totalname = count((array)$arr2);

	$newStaffUser = new User;
	$newStaffUser->bin = $user_info->bin;
	$newStaffUser->phone = $request->phone;
	$newStaffUser->password = $hashed;
	if($totalname == 3)
	{
		$newStaffUser->fname = $arr2[0];
		$newStaffUser->mname = $arr2[1];
		$newStaffUser->lname = $arr2[2];
	}
	else if($totalname == 2)
	{
		$newStaffUser->fname = $arr2[0];
		$newStaffUser->lname = $arr2[1];
	}
	else{
		$newStaffUser->fname = $arr2[0];
	}
	$newStaffUser->locality = $request->address;
	$newStaffUser->profile_photo = $folder_url . "/" . $filename ;
	$newStaffUser->category = 10;
	$newStaffUser->status = 0;
	$save = $newStaffUser->save();
	if(isset($save))
	{
		$day = date("l");
		$date =  date('Y-m-d');
		$staff_available = StaffAvailability::where("bin",  $user_info->bin)
	      ->where("staff_id",   $newStaffUser->id)
	      ->first();

	      if( !isset($staff_available))
	      {
	        $staff_available = new StaffAvailability;
	      }

	     $staff_available->bin = $user_info->bin;
	     $staff_available->staff_id = $newStaffUser->id; //staff ID is same as user id
	     $staff_available->service_date = $date;
	     $staff_available->shift = 'morning';
	     $staff_available->save();
		 $data= ["message" => "success", "status_code" =>  1077 ,
		'detailed_msg' => 'Staff availability saved.',"ps" => $randpwd ];
	}
	else
	{
		$data= ["message" => "success", "status_code" =>  1078 ,
		'detailed_msg' => 'Staff availablity could not be saved.'  ];
	}

    return json_encode( $data);




  }

  //fetch staffs under a business
  public function fetchStaffAvailability(Request $request)
  {
	  	$user_info =  json_decode($this->getUserInfo($request->userId));
		if($user_info->user_id == "na" )
		{
			$data= ["message" => "failure", "status_code" =>  901 ,
			'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
			return json_encode($data);
		}
		if($request->bin == "")
		{
			$data= ["message" => "failure", "status_code" =>  999 ,
		      'detailed_msg' => 'Business information not found!'  ];
		      return json_encode( $data);
		}
		$staff_available = StaffAvailability::where("bin",  $request->bin )
			->select('bin','staff_id as staffId','service_date as serviceDate','shift',DB::Raw("'na' staffName"),DB::Raw("'na' staffPhone"),DB::Raw("'na' staffPhotoUrl"),DB::Raw("'na' staffCoverPhotoUrl"))
	      ->get();

	    $staffidd = array();
	    foreach ($staff_available as $item) {

	    	$staffidd[]=$item->staffId;

	    }
	    $userData = User::whereIn('id',$staffidd)->get();

	    foreach ($staff_available as $itemStaff) {
	    	foreach ($userData as $keyName) {
	    		if($itemStaff->staffId == $keyName->id)
	    		{
	    			if($keyName->mname!=null)
	    			{
	  					$itemStaff->staffName=$keyName->fname ." ". $keyName->mname." ".$keyName->lname ;
					}
					else
					{
						$itemStaff->staffName=$keyName->fname ." ".$keyName->lname ;
					}
					$itemStaff->staffPhone = $keyName->phone;

					$itemStaff->staffPhotoUrl = URL::to('/public/assets/image/member/').$keyName->profile_photo;
					$itemStaff->staffCoverPhotoUrl = URL::to('/public/assets/image/member/').$keyName->cover_photo;
	    		}


	    	}
	    }
	    $data= ["message" => "success", "status_code" =>  1083 ,
		'detailed_msg' => 'Fetch Staff availablity.' ,'results' => $staff_available  ];


	    return json_encode($data);
  }

  //remove staffs under a business
  public function removeStaffAvailability(Request $request)
  {
	  	$user_info =  json_decode($this->getUserInfo($request->userId));
		if($user_info->user_id == "na" )
		{
			$data= ["message" => "failure", "status_code" =>  901 ,
			'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
			return json_encode($data);
		}
		if($user_info->bin == 0 )
		{
			$data= ["message" => "failure", "status_code" =>  999 ,
		      'detailed_msg' => 'Business information not found!'  ];
		      return json_encode( $data);
		}
		if($request->staffId == "" )
		{
			$err_msg = "Field missing!";
			$err_code = 9000;


			$data = array(
				"message" => "failure" , 'status_code' => $err_code ,
				'detailed_msg' => $err_msg) ;

			return json_encode( $data );
		}
		$staff_available = StaffAvailability::where("bin",  $user_info->bin )
			->where('staff_id',$request->staffId)
	      	->get();

	    if(count($staff_available)>0)
        {
            foreach ($staff_available as $keyidd)
            {
                $serviceData = StaffAvailability::find($keyidd->id);
                $serviceData->delete();
            }


			$data = array(
				"message" => "success" , 'status_code' => 1081 ,
				'detailed_msg' => "Staff availability is deleted successfully!") ;

        }
        else{
        	$data = array(
				"message" => "failure" , 'status_code' => 1082 ,
				'detailed_msg' => "Please try Again!") ;
        }


	    return json_encode($data);
  }


  public function sendSmsTest()
  {
	  $phones = array('9612824492', '7005894963');
	  $message = "This is BookTou Test SMS";
	  $this->sendSmsAlert($phones, $message);
  }

  public function addBusinessProfilePhoto(Request $request)
  {
		$user_info =  json_decode($this->getUserInfo($request->userId));
		if(  $user_info->category == 1 )
		{
		    $folder_url  = '/assets/image/business/bin_'. $request->bin ;
		    $folder_path = public_path(). '/assets/image/business/bin_'. $request->bin;
		}
		$business = Business::find($request->bin);
		if( !isset($business))
		{
		    $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		    'detailed_msg' => 'Business information not found!'  ];
		    return json_encode( $data);
		}

		if( !File::isDirectory($folder_path)){
		    File::makeDirectory( $folder_path, 0777, true, true);
		}

		$file = $request->file('photos');
		if($file==null)
		{
			$data= ["message" => "failure", "status_code" =>  1018 ,  'detailed_msg' => 'Please select your file.'];
			return json_encode( $data);
		}
		$originalname = $file->getClientOriginalName();
		$extension = $file->extension( );
		$filename = "profile". ".{$extension}";
		$file->storeAs('uploads',  $filename );
		$imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
		$file_names[] =  $filename ;
		$business->profile_image =  $folder_url . "/" . $filename ;
		$save=$business->save();

		if($save)
		{
		    $data= ["message" => "success", "status_code" =>  1017 ,  'detailed_msg' => 'Business profile photo is uploads successfully'];
		}
		else{

		}

		return json_encode( $data);
  }



  public function addBusinessBannerPhoto(Request $request)
  {
		$user_info =  json_decode($this->getUserInfo($request->userId));
		if(  $user_info->category == 1 )
		{
		    $folder_url  = '/assets/image/business/bin_'. $request->bin ;
		    $folder_path = public_path(). '/assets/image/business/bin_'. $request->bin;
		}


		$business = Business::find($request->bin);


		if( !isset($business))
		{
		    $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		    'detailed_msg' => 'Business information not found!'  ];
		    return json_encode( $data);
		}

		if( !File::isDirectory($folder_path)){
		    File::makeDirectory( $folder_path, 0777, true, true);
		}

		$file = $request->file('photo');
		if($file==null)
		{
			$data= ["message" => "failure", "status_code" =>  1018 ,  'detailed_msg' => 'Please select your file.'];
			return json_encode( $data);
		}
		$originalname = $file->getClientOriginalName();
		$extension = $file->extension( );
		$filename = "banner_". time() . ".{$extension}";
		$file->storeAs('uploads',  $filename );
		$imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
		$file_names[] =  $filename ;
		$business->banner =  $folder_url . "/" . $filename ;
		$save=$business->save();

		if($save)
		{
		    $data= ["message" => "success", "status_code" =>  1017 ,  'detailed_msg' => 'Business banner photo uploaded successfully.'];
		}
		else{

		}

		return json_encode( $data);
  }




  public function getBannerImage(Request $request)
  {
		$business = Business::find($request->bin);
		if( !isset($business))
		{
		    $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		    'detailed_msg' => 'Business information not found!'  ];
		    return json_encode( $data);
		}
		$bannerdata = $business->banner;
		$banner = URL::to('/public'.$bannerdata);
		$result = array('bannerImgUrl' => $banner);

		$data= ["message" => "success", "status_code" =>  1019 ,  'detailed_msg' => 'Business banner photo is fetch successfully.','result' => $result];


		return json_encode( $data);
  }

  public function getBusinessProfile(Request $request)
  {
		$business = Business::find($request->bin);
		if( !isset($business))
		{
		    $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		    'detailed_msg' => 'Business information not found!'  ];
		    return json_encode( $data);
		}
		$businesses = DB::table("ba_business")
		->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")
		 ->where('ba_business.id',$request->bin)
		  ->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 'ba_business.shop_number as shopNumber',
		  'phone_pri as primaryPhone', 'phone_alt as alternatePhone',
		  "locality", "landmark", "city", "state", "pin",
		  'tags','rating','latitude','longitude' ,
		  "profile_image as profileImgUrl",   "banner as bannerImgUrl" ,
		DB::Raw("'na' promoImgUrl"),  "category as businessCategory",
		"ba_business_category.main_type as mainType",
		 
		  "is_open as isOpen" )
		  ->get();

	  	foreach($businesses as $item)
		{
			if($item->profileImgUrl!=null)
			{
				$item->profileImgUrl = URL::to('/public'.$item->profileImgUrl);
			}
			else{
				$item->profileImgUrl = 'na';
			}
			if($item->bannerImgUrl!=null)
			{
				$item->bannerImgUrl = URL::to('/public'.$item->bannerImgUrl);
			}
			else{
				$item->bannerImgUrl = 'na';
			}
			if($item->promoImgUrl!='na')
			{
				$item->promoImgUrl = URL::to('/public'.$item->promoImgUrl);
			}
			else{
				$item->promoImgUrl = 'na';
			}



		}
		//$profiledata = $business->profile_image;
		//$profile = URL::to('/public'.$profiledata);
		//$result = array('profileImgUrl' => $profile);

		$data= ["message" => "success", "status_code" =>  1021 ,  'detailed_msg' => 'Business profile photo is fetch successfully.','results' => $businesses];


		return json_encode( $data);
  }
  public function getBusinessGpsLocation(Request $request)
  {
		$business = Business::find($request->bin);
		if( !isset($business))
		{
		    $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		    'detailed_msg' => 'Business information not found!'  ];
		    return json_encode( $data);
		}

		$latitude = $business->latitude;
		$longitude = $business->longitude;

		$result = array('latitude' => $latitude , 'longitude' => $longitude);

		$data= ["message" => "success", "status_code" =>  1023 ,  'detailed_msg' => 'Business Gps location is fetch successfully.','result' => $result];


		return json_encode( $data);
  }



  	public function getCustomerDetails(Request $request)
	{

	    $business = Business::find($request->bin);
        if( !isset($business))
        {
            $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
        }
		
		 
		
		$listCustomer = DB::table("ba_profile")
		->join("ba_service_booking", "ba_service_booking.book_by", "=", "ba_profile.id")
		->where("ba_service_booking.bin", $request->bin ) 
		->orderBy("ba_service_booking.customer_name", "asc") 
		->distinct()		
		->select(  "book_by as customerId", 
			"customer_name as customerName" , 
			"customer_phone as customerPhone",  
			"profile_image_url  as customerProfilePhoto" )
		->get() ;
		
		foreach($listCustomer as $item)
		{
			if($item->customerProfilePhoto == "" || $item->customerProfilePhoto == null )
			{
				$item->customerProfilePhoto = config('app.app_cdn') .  "/assets/image/no-image.jpg" ;  
			}
		}
		
		
		if(count($listCustomer)>0)
		{
			$data= ["message" => "success", "status_code" =>  1037 ,  'detailed_msg' => 'Fetch Customer Details','results' => $listCustomer];
		}
		else{
			$data= ["message" => "failure", "status_code" =>  1038 ,  'detailed_msg' => 'Business has no Customer','results' => array()];
		}


	    return json_encode($data);
  	}
	
	
  	public function addBusinessProduct(Request $request)
	{
		$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        else if(  $user_info->category == 1 )
        {
            $folder_url  = '/assets/image/business/product/bin_'. $request->bin ;
            $folder_path = public_path(). '/assets/image/business/product/bin_'. $request->bin;
        }
		$business = Business::find($request->bin);
		if( !isset($business))
		{
		    $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		    'detailed_msg' => 'Business information not found!'  ];
		    return json_encode( $data);
		}

		if($request->name == "" || $request->productCode == "" || $request->price == "")
        {
	         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	         return json_encode($data);
        }

		$businessProduct = BusinessProduct::where('bin',$request->bin)
			->where('product_name',$request->name)
			->where('pd_code',$request->productCode)
			->first();
		if(isset($businessProduct))
		{
			$data= ["message" => "failure", "status_code" =>  1000 , 'detailed_msg' => 'Bussiness Product is already exists!'  ];
     		return json_encode($data);
		}

		if( !File::isDirectory($folder_path)){
            File::makeDirectory( $folder_path, 0777, true, true);
        }
        $file = $request->file('image');
        if($file==null)
        {
            $data= ["message" => "failure", "status_code" =>  1018 ,  'detailed_msg' => 'Please select your file.'];
            return json_encode( $data);
        }
        $originalname = $file->getClientOriginalName();
        $extension = $file->extension( );
        $filename = "pd_" .  $request->userId .  time()  . ".{$extension}";
        $file->storeAs('uploads',  $filename );
        $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
        $file_names[] =  $filename ;

		$businessProduct = new BusinessProduct;
		$businessProduct->bin = $request->bin;
		$businessProduct->product_name = $request->name;
		$businessProduct->image = $folder_url . "/" . $filename ;
		$businessProduct->pd_code = $request->productCode;
		$businessProduct->price = $request->price;
		$save = $businessProduct->save();
		if($save)
		{
			$data= ["message" => "success", "status_code" =>  1031 ,  'detailed_msg' => 'Business Product placed successfully.','productId' => $businessProduct->id];
		}
		else{
			$data= ["message" => "success", "status_code" =>  1032 ,  'detailed_msg' => 'Please Try Again.'];
		}
		return json_encode( $data);
	}
	public function removeBusinessProduct(Request $request)
	{
		$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
		if( !isset($business))
		{
		    $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		    'detailed_msg' => 'Business information not found!'  ];
		    return json_encode( $data);
		}

		if($request->productCode == "")
        {
	         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	         return json_encode($data);
        }

		$businessProduct = BusinessProduct::where('bin',$request->bin)
			->where('pd_code',$request->productCode)
			->get();
		if(count($businessProduct)>0)
        {
            foreach ($businessProduct as $item)
            {
            	File::delete('public'.$item->image);
                $productData = BusinessProduct::find($item->id);
                $productData->delete();
            }
            $data= ["message" => "success", "status_code" =>  1033 ,  'detailed_msg' => 'Business Product deleted successfully.'];
        }
        else{
        	$data= ["message" => "failure", "status_code" =>  1034 ,  'detailed_msg' => 'Business Product could not find Please try Again.'];
        }

		return json_encode( $data);
	}
	public function getBusinessProduct(Request $request)
	{
		$business = Business::find($request->bin);
		if( !isset($business))
		{
		    $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
		    'detailed_msg' => 'Business information not found!'  ];
		    return json_encode( $data);
		}
		$businessProduct = BusinessProduct::where('bin',$request->bin)
			->select('bin','product_name as productName','image as productImgUrl','pd_code as productCode','price')
			->get();
		if(count($businessProduct)>0)
        {
        	foreach ($businessProduct as $item)
            {
            	$item->productImgUrl = URL::to('public'.$item->productImgUrl);
            }
            $data= ["message" => "success", "status_code" =>  1035 ,  'detailed_msg' => 'Business Product is fetch successfully.',"results" => $businessProduct];
        }
        else{
        	$data= ["message" => "failure", "status_code" =>  1036 ,  'detailed_msg' => 'Business Product not found!',"results" => array()];
        }

		return json_encode( $data);
	}

	//MakeProductSalesReceipt
	public function productSales(Request $request)
	{
		$bin=$request->bin;
		$phnumber=$request->phone;
		if($request->bookingId==0)
		{
			if($request->phone == "" || $request->customerName == "")
        	{
	         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	         return json_encode($data);
        	}
			$userInfo = User::where('phone',$request->phone)->first();
			if( !isset($userInfo))
	        {
	          	$rand = mt_rand(434343, 999999);
				$password = $rand;
				$hashed = Hash::make($password);
				$name = $request->customerName;
				$arr2 = explode(" " , $name);
				$totalname = count((array)$arr2);

				$newCustomerUser = new User;
				$newCustomerUser->bin = $bin;
				$newCustomerUser->phone = $phnumber;
				$newCustomerUser->password = $hashed;
				if($totalname == 3)
				{
					$newCustomerUser->fname = $arr2[0];
					$newCustomerUser->mname = $arr2[1];
					$newCustomerUser->lname = $arr2[2];
				}
				else if($totalname == 2)
				{
					$newCustomerUser->fname = $arr2[0];
					$newCustomerUser->lname = $arr2[1];
				}
				$newCustomerUser->category = 0;
				$newCustomerUser->status = 0;
				$saveCust = $newCustomerUser->save();
				if($saveCust)
				{
					$phones = array($phnumber);
			        $otp_msg = "Your Login Password OTP is " . $rand.  urlencode(". Please DO NOT SHARE this OTP with anyone. bookTou never calls you asking for OTP." ) ;
			        $this->sendSmsAlert($phones, $otp_msg);
			        $custId = $newCustomerUser->id;

				}
	        }
	        else{
	        	$custId = $userInfo->id;
	        }
	        $bookId = 0;

		}
		else{
			$booking = ServiceBooking::find($request->bookingId);
	        if( !isset($booking))
	        {

	          $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
	          'detailed_msg' => 'Booking information not found!'  ];
	          return json_encode( $data);

	        }
	        $bookId = $booking->id;
	        $custId = $booking->book_by;

		}
        if($request->bookingId == "")
        {
	         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	         return json_encode($data);
        }

         if( $request->serviceCode != "")
         {

             $serviceCode = $request->serviceCode;


        $where_clause=array();
        foreach ($serviceCode as $keyCoder) {
        	$where_clause[]= " find_in_set('". $keyCoder . "', srv_code ) > 0";
        }

        $service = DB::table("ba_business_services")
        ->whereRaw( implode(" or ", $where_clause) )->get();
		if(count($service)==0)
        {

          $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Service information not found!'  ];
          return json_encode( $data);

        }

        foreach ($serviceCode as $itemS)
        {
        	foreach ($service as $keyS)
        	{

        		if($itemS==$keyS->srv_code)
        		{
        			$serviceId=$keyS->id;
        			$serviceName=$keyS->srv_name;
        			$pricing=$keyS->pricing;
        			//add product sales in receipt
		            $businessSales = new BusinessSales;
			        $businessSales->book_id=$request->bookingId;
			        $businessSales->bin=$request->bin;
			        $businessSales->customer_id=$custId;
			        $businessSales->bill_date=date('Y-m-d');
			        $businessSales->name=$serviceName;
			        $businessSales->pricing=$pricing;
			        $save=$businessSales->save();

        		}
        	}


	        if($save)
	        {
	        	 $data= ["message" => "success", "status_code" =>  101 ,  'detailed_msg' => 'Business Sales Receipt is placed successfully.',"results" => $businessSales->id];
	        }
	        else{
	        	 $data= ["message" => "failure", "status_code" =>  102 ,  'detailed_msg' => 'Business Sales Receipt failed. Please retry!',"results" => array()];

	        }


    	}

    }

     if( $request->productCode != "")
     {
        $productCode = $request->productCode;

        $where_clause=array();
        foreach ($productCode as $keyCoder) {
        	$where_clause[]= " find_in_set('". $keyCoder . "', pd_code ) > 0";
        }

        $product = DB::table("ba_business_product")
        ->whereRaw( implode(" or ", $where_clause) )->get();
		if(count($product)==0)
        {

          $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Product information not found!'  ];
          return json_encode( $data);

        }

        foreach ($productCode as $itemP)
        {
        	foreach ($product as $keyP)
        	{

        		if($itemP==$keyP->pd_code)
        		{
        			$prodId=$keyP->id;
        			$productName=$keyP->product_name;
        			$pricing=$keyP->price;
        			//add product sales in receipt
		            $businessSales = new BusinessSales;
			        $businessSales->book_id=$request->bookingId;
			        $businessSales->bin=$request->bin;
			        $businessSales->customer_id=$custId;
			        $businessSales->bill_date=date('Y-m-d');
			        $businessSales->name=$productName;
			        $businessSales->pricing=$pricing;
			        $save=$businessSales->save();

        		}
        	}


	        if($save)
	        {
	        	 $data= ["message" => "success", "status_code" =>  1039 ,  'detailed_msg' => 'Business Sales Receipt is placed successfully.',"results" => $businessSales->id];
	        }
	        else{
	        	 $data= ["message" => "failure", "status_code" =>  1040 ,  'detailed_msg' => 'Business Sales Receipt failed. Please retry!',"results" => array()];

	        }


    	}

      }
    	return json_encode( $data);



	}
	//MakeSalesReceipt
	public function bookingComplete(Request $request)
	{
		$booking = ServiceBooking::find($request->bookingId);
        if( !isset($booking))
        {

          $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Booking information not found!'  ];
          return json_encode( $data);

        }
        if($request->serviceCode == "" || $request->bookingId == "")
        {
	         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	         return json_encode($data);
        }

        $custId = $booking->book_by;
        $bin = $booking->bin;
        //BusinessService
        $businessService =  BusinessService::where('bin',$bin)
        	->where('srv_code',$request->serviceCode)
        	->get();
        $bookId = $booking->id;
        foreach ($businessService as $itemS)
        {

			$serviceName=$itemS->srv_name;
			$pricing=$itemS->pricing;
			//add product sales in receipt
            $businessSales = new BusinessSales;
	        $businessSales->book_id=$request->bookingId;
	        $businessSales->customer_id=$custId;
	        $businessSales->bill_date=date('Y-m-d');
	        $businessSales->name=$serviceName;
	        $businessSales->pricing=$pricing;
	        $save=$businessSales->save();



	        if($save)
	        {
	        	 $data= ["message" => "success", "status_code" =>  1047 ,  'detailed_msg' => 'Business Sales Receipt is placed successfully.',"results" => $businessSales->id];
	        }
	        else{
	        	 $data= ["message" => "failure", "status_code" =>  1048 ,  'detailed_msg' => 'Business Sales Receipt failed. Please retry!',"results" => array()];

	        }
        }


        return json_encode( $data);


	}

	//FetchProductSalesWithBooking
	public function receiptBusiness(Request $request)
	{
		if($request->bin == "" || $request->bookingDate == "")
        {
	         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	         return json_encode($data);
        }
        $dateBooking=date('Y-m-d',strtotime($request->bookingDate));


        $product=DB::table('ba_service_booking')
        ->join("ba_business_sales","ba_business_sales.bill_date","=","ba_service_booking.book_date")
        ->where('ba_service_booking.bin',$request->bin)
        ->where('ba_service_booking.book_date',$dateBooking)
        ->select('ba_business_sales.book_id as bookingId',
        	     'ba_business_sales.customer_id as customerId',
        	     'ba_business_sales.bill_date as billDate',
        	     'ba_business_sales.name',
        	     'ba_business_sales.pricing as pricing')
        ->get();

        $totalprice=0;
        foreach ($product as $item) {
        	$totalprice+=$item->pricing;
        }



        $data= ["message" => "success", "status_code" =>  1043 ,  'detailed_msg' => 'Business Product receipt is fetch successfully.',"results" => $product,"totalprice" =>$totalprice];

        return json_encode( $data);


	}
	//BusinessBookingStatus

	public function bookingSystem(Request $request)
	{
		$day = date('l', strtotime($request->bookingDate));
		//booking
        $booking = ServiceBooking::where('bin',$request->bin)
            ->where('book_date',date('Y-m-d', strtotime($request->bookingDate)))
            ->where('staff_id',$request->staffId)
            ->get();

        foreach ($booking as $bokitem)
        {

            $bookId[] = $bokitem->id ;


        }
        //bookingDetails
        $bookingDetails = ServiceBookingDetails::whereIn('book_id',$bookId)->get();

        foreach ($bookingDetails as $itemD) {
            $bookTime[] = $itemD->service_time ;
        }

	}
	public function getBusinessCategory(Request $request)
	{
		$busCategory = BusinessCategory::select( 'name as category')
		->orderBy("name", "asc")
		->get();


		 foreach($busCategory as   $item)
        {

		 $item->category = ucfirst($item->category);

		}


		$data= ["message" => "success", "status_code" =>  1049 ,  'detailed_msg' => 'Fetch Business Category',"results" => $busCategory];
		return json_encode( $data);
	}



	//obsolte and replaced by sendBusinessMarketingSms
	public function sendMarketingSms(Request $request)
	{
	  	$user_info =  json_decode($this->getUserInfo($request->userId));

        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.' ,'results' => array() ];
            return json_encode($data);
        }
        if($user_info->category==0)
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.','results' => array()  ];
            return json_encode($data);
        }
	    $business = Business::find($request->bin);
        if( !isset($business))
        {
            $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
        }
	   	$binCategory=strtoupper($business->category);
        $cid = $listCustomer=$sendPhone =array();

        if($request->months == ""|| $request->months == 0 || $request->months > 12)
        {
	         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	         return json_encode($data);
        }
        $month= '-'.$request->months.' month';
        $todays = date('Y-m-d', strtotime($month));


    	$bookingCustomer = ServiceBooking::where('bin',$request->bin)
    	->where('book_date','>=',$todays)
    	->select("book_by as customerId",DB::Raw("'na' customerName"),DB::Raw("'na' customerPhone"))
    	->distinct('customerId')
    	->get();

	    foreach($bookingCustomer as $item)
	  	{
	  		$cid[]=$item->customerId;

	  	}
	  	if($binCategory==config('global.BUSINESS_BEAUTY'))
	  	{
	  		$businessSales = BusinessSales::where('bin',$request->bin)
		  	->where('bill_date','>=',$todays)
		  	->distinct('customer_id')
	    	->get();

	    	foreach($businessSales as $itemSa)
		  	{
		  		$cid[]=$itemSa->customer_id;

		  	}
	  	}


	  	$userData = User::whereIn('id',$cid)
		->select("id","fname","mname","lname","phone")->get();
		foreach ($userData as $itemU) {
			$customerId=$itemU->id;
			$custId[]=$itemU->id;
			if($itemU->mname!=null)
            {
                $customerName=$itemU->fname ." ". $itemU->mname." ".$itemU->lname ;
            }
            else
            {
                $customerName=$itemU->fname ." ".$itemU->lname ;
            }
            $customerPhone=$itemU->phone;
            $sendPhone[]=$itemU->phone;
            $collect=array("customerId"=>$customerId,"customerName"=>$customerName,"customerPhone"=>$customerPhone);
            $listCustomer[]=$collect;

		}
		if(count($listCustomer)>0)
		{
			$userCustomer=$custId;
			$phones = $sendPhone;
			$otp_msg = urlencode($request->content);
	        $this->sendSmsAlert($phones, $otp_msg);

	       	$marketingSms = new MarketingSms;
	        $marketingSms->bin=$request->bin;
	        $marketingSms->month=$request->months;
	        $marketingSms->content=$request->content;
	        $save = $marketingSms->save();
	        if($save)
	        {
	        	for($i=0;$i<sizeof($userCustomer);$i++)
				{
					$customerIdd=$userCustomer[$i];
					$marketingSmsDetails = new MarketingSmsDetails;
					$marketingSmsDetails->sms_id = $marketingSms->id;
					$marketingSmsDetails->customer_id = $customerIdd;
					$marketingSmsDetails->save();


				}
				$data= ["message" => "success", "status_code" =>  1071 ,  'detailed_msg' => 'Send Sms to Customer ','results' => $listCustomer ];

	        }


		}
		else{
			$data= ["message" => "failure", "status_code" =>  1072 ,  'detailed_msg' => 'Could not Send Sms to Customer','results' => array()];
		}

	    return json_encode($data);
  	}





  	public function sendBusinessMarketingSms(Request $request)
    {
          $user_info =  json_decode($this->getUserInfo($request->userId));



        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.' ,'results' => array() ];
            return json_encode($data);
        }
        if($user_info->category==0)
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.','results' => array()  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
            $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
        }
           $binCategory=strtoupper($business->category);
        $cid = $listCustomer=$sendPhone =array();



        if($request->months == ""|| $request->months == 0 || $request->months > 12)
        {
             $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
             return json_encode($data);
        }
        $month= '-'.$request->months.' month';
        $todays = date('Y-m-d', strtotime($month));




        $bookingCustomer = ServiceBooking::where('bin',$business->id)
        ->where('book_date','>=',$todays)
        ->select("book_by as customerId",DB::Raw("'na' customerName"),DB::Raw("'na' customerPhone"))
        ->distinct('customerId')
        ->get();

        foreach($bookingCustomer as $item)
          {
              $cid[]=$item->customerId;

          }
          if($binCategory==config('global.BUSINESS_BEAUTY'))
          {
              $businessSales = BusinessSales::where('bin',$business->id)
              ->where('bill_date','>=',$todays)
              ->distinct('customer_id')
            ->get();



            foreach($businessSales as $itemSa)
              {
                  $cid[]=$itemSa->customer_id;

              }
          }




          $userData = CustomerProfileModel::whereIn('id',$cid)
        ->select("id","fname","mname","lname","phone")->get();
        foreach ($userData as $itemU) {
            $customerId=$itemU->id;
            $custId[]=$itemU->id;
            if($itemU->mname!=null)
            {
                $customerName=$itemU->fname ." ". $itemU->mname." ".$itemU->lname ;
            }
            else
            {
                $customerName=$itemU->fname ." ".$itemU->lname ;
            }
            $customerPhone=$itemU->phone;
            $sendPhone[]=$itemU->phone;
            $collect=array("customerId"=>$customerId,"customerName"=>$customerName,"customerPhone"=>$customerPhone);
            $listCustomer[]=$collect;

        }
        if(count($listCustomer)>0)
        {
            $userCustomer=$custId;
            $phones = $sendPhone;
            $otp_msg = urlencode($request->content);
            $this->sendSmsAlert($phones, $otp_msg);



               $marketingSms = new MarketingSms;
            $marketingSms->bin=$request->bin;
            $marketingSms->month=$request->months;
            $marketingSms->content=$request->content;
            $save = $marketingSms->save();
            if($save)
            {
                for($i=0;$i<sizeof($userCustomer);$i++)
                {
                    $customerIdd=$userCustomer[$i];
                    $marketingSmsDetails = new MarketingSmsDetails;
                    $marketingSmsDetails->sms_id = $marketingSms->id;
                    $marketingSmsDetails->customer_id = $customerIdd;
                    $marketingSmsDetails->save();




                }
                $data= ["message" => "success", "status_code" =>  1071 ,  'detailed_msg' => 'Send Sms to Customer ','results' => $listCustomer ];

            }


        }
        else{
            $data= ["message" => "failure", "status_code" =>  1072 ,  'detailed_msg' => 'Could not Send Sms to Customer','results' => array()];
        }

        return json_encode($data);
      }


      //obsolete
  	public function sendMarketingSmsAll(Request $request)
	{
	  	$user_info =  json_decode($this->getUserInfo($request->userId));

        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.' ,'results' => array() ];
            return json_encode($data);
        }
        if($user_info->category==0)
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.','results' => array()  ];
            return json_encode($data);
        }
	    $business = Business::find($request->bin);
        if( !isset($business))
        {
            $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
        }
	   	$binCategory=strtoupper($business->category);
        $cid = $listCustomer=$sendPhone =array();


    	$bookingCustomer = ServiceBooking::where('bin',$request->bin)
    	->select("book_by as customerId",DB::Raw("'na' customerName"),DB::Raw("'na' customerPhone"))
    	->distinct('customerId')
    	->get();

	    foreach($bookingCustomer as $item)
	  	{
	  		$cid[]=$item->customerId;

	  	}
	  	if($binCategory==config('global.BUSINESS_BEAUTY'))
	  	{
	  		$businessSales = BusinessSales::where('bin',$request->bin)
		  	->distinct('customer_id')
	    	->get();

	    	foreach($businessSales as $itemSa)
		  	{
		  		$cid[]=$itemSa->customer_id;

		  	}
	  	}

	  	$userData = User::whereIn('id',$cid)
		->select("id","fname","mname","lname","phone")->get();
		foreach ($userData as $itemU) {
			$customerId=$itemU->id;
			$custId[]=$itemU->id;
			if($itemU->mname!=null)
            {
                $customerName=$itemU->fname ." ". $itemU->mname." ".$itemU->lname ;
            }
            else
            {
                $customerName=$itemU->fname ." ".$itemU->lname ;
            }
            $customerPhone=$itemU->phone;
            $sendPhone[]=$itemU->phone;
            $collect=array("customerId"=>$customerId,"customerName"=>$customerName,"customerPhone"=>$customerPhone);
            $listCustomer[]=$collect;

		}
		if(count($listCustomer)>0)
		{
			$userCustomer=$custId;
			$phones = $sendPhone;
			$otp_msg = urlencode($request->content);
	        $this->sendSmsAlert($phones, $otp_msg);

	       	$marketingSms = new MarketingSms;
	        $marketingSms->bin=$request->bin;
	        $marketingSms->month=0;
	        $marketingSms->content=$request->content;
	        $save = $marketingSms->save();
	        if($save)
	        {
	        	for($i=0;$i<sizeof($userCustomer);$i++)
				{
					$customerIdd=$userCustomer[$i];
					$marketingSmsDetails = new MarketingSmsDetails;
					$marketingSmsDetails->sms_id = $marketingSms->id;
					$marketingSmsDetails->customer_id = $customerIdd;
					$marketingSmsDetails->save();


				}
				$data= ["message" => "success", "status_code" =>  1073 ,  'detailed_msg' => 'Send Sms to Customer ','results' => $listCustomer ];

	        }


		}
		else{
			$data= ["message" => "failure", "status_code" =>  1074 ,  'detailed_msg' => 'Could not Send Sms to Customer','results' => array()];
		}

	    return json_encode($data);
  	}



  	public function sendBusinessMarketingSmsAll(Request $request)
    {
          $user_info =  json_decode($this->getUserInfo($request->userId));



        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.' ,'results' => array() ];
            return json_encode($data);
        }
        if($user_info->category==0)
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.','results' => array()  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
            $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
        }
           $binCategory=strtoupper($business->category);
        $cid = $listCustomer=$sendPhone =array();




        $bookingCustomer = ServiceBooking::where('bin',$business->id)
        ->select("book_by as customerId",DB::Raw("'na' customerName"),DB::Raw("'na' customerPhone"))
        ->distinct('customerId')
        ->get();

        foreach($bookingCustomer as $item)
          {
              $cid[]=$item->customerId;

          }
          if($binCategory==config('global.BUSINESS_BEAUTY'))
          {
              $businessSales = BusinessSales::where('bin',$business->id)
              ->distinct('customer_id')
            ->get();



            foreach($businessSales as $itemSa)
              {
                  $cid[]=$itemSa->customer_id;

              }
          }

          $userData = CustomerProfileModel::whereIn('id',$cid)
        ->select("id","fname","mname","lname","phone")->get();
        foreach ($userData as $itemU) {
            $customerId=$itemU->id;
            $custId[]=$itemU->id;
            if($itemU->mname!=null)
            {
                $customerName=$itemU->fname ." ". $itemU->mname." ".$itemU->lname ;
            }
            else
            {
                $customerName=$itemU->fname ." ".$itemU->lname ;
            }
            $customerPhone=$itemU->phone;
            $sendPhone[]=$itemU->phone;
            $collect=array("customerId"=>$customerId,"customerName"=>$customerName,"customerPhone"=>$customerPhone);
            $listCustomer[]=$collect;

        }
        if(count($listCustomer)>0)
        {
            $userCustomer=$custId;
            $phones = $sendPhone;
            $otp_msg = urlencode($request->content);
            $this->sendSmsAlert($phones, $otp_msg);



               $marketingSms = new MarketingSms;
            $marketingSms->bin=$request->bin;
            $marketingSms->month=0;
            $marketingSms->content=$request->content;
            $save = $marketingSms->save();
            if($save)
            {
                for($i=0;$i<sizeof($userCustomer);$i++)
                {
                    $customerIdd=$userCustomer[$i];
                    $marketingSmsDetails = new MarketingSmsDetails;
                    $marketingSmsDetails->sms_id = $marketingSms->id;
                    $marketingSmsDetails->customer_id = $customerIdd;
                    $marketingSmsDetails->save();




                }
                $data= ["message" => "success", "status_code" =>  1073 ,  'detailed_msg' => 'Send Sms to Customer ','results' => $listCustomer ];

            }


        }
        else{
            $data= ["message" => "failure", "status_code" =>  1074 ,  'detailed_msg' => 'Could not Send Sms to Customer','results' => array()];
        }

        return json_encode($data);
      }

  	//Analaytics and growth
  	public function getAnalyticsGrowth(Request $request)
	{
	  	$user_info =  json_decode($this->getUserInfo($request->userId));

        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.' ,'results' => array() ];
            return json_encode($data);
        }
        if($user_info->category==0)
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.','results' => array()  ];
            return json_encode($data);
        }
	    $business = Business::find($request->bin);
        if( !isset($business))
        {
            $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
        }
        $binCategory=strtoupper($business->category);
        if($binCategory==config('global.BUSINESS_CATERING'))
        {


        }

        $todays = date('Y-m-d');

        if($request->years!=null)
        {
        	$years = $request->years;
        }
        else
        {
        	$years = date('Y',strtotime($todays));
        }
        if($request->months!=null)
        {
        	$month = $years.'-'.$request->months.'-1';
        	$monthly = date('m',strtotime($month));
        	$monthlyName = date('F',strtotime($month));
        }
        else
        {
        	$month = date('Y-m-d',strtotime($todays));
        	$monthly = date('m',strtotime($todays));
        	$monthlyName = date('F',strtotime($todays));
        }
        //Customer Propect date range
        $dateMonth = strtotime($month);
      	$twoMonth = strtotime("-2 month",$dateMonth);
      	$fourMonth = strtotime("-4 month",$dateMonth);
      	$start_2Month = date("Y-m-d",$twoMonth);
      	$start_4Month = date("Y-m-d",$fourMonth);


        $analaytics=array("month"=>$monthlyName,
    		"salesOverView" => array(), "orderOverView" => array(), "customerProscpects" => array(), "recurringCustomer" => array());

        $salesTotal = $salesAppointment = $salesRandom = $orderAppoinment = $orderRandom = $orderTotal=  $countAppointment = $countRandom = $appoinment= $random = $appointmentCount = $randomCount = $count2MonthCustomer = $count4MonthCustomer = $countOnline = $countOffline = 0.0;

        if($binCategory==config('global.BUSINESS_BEAUTY'))
        {

		    $businessSalesAppointment = BusinessSales::where('bin',$request->bin)
	        ->whereMonth('bill_date',$monthly)
	        ->whereYear('bill_date',$years)
	        ->where('book_id','>',0)
	        ->get();
	        //dd($businessSalesAppointment);
	        foreach ($businessSalesAppointment as $itemSalesA)
	        {
	            $salesAppointment += $itemSalesA->pricing;
	            $bookId[] =$itemSalesA->book_id;
	            $recuringCus[] = $itemSalesA->customer_id;


	        }

	        if(count($businessSalesAppointment)>0)
	        {
	        	$bookList = array_unique($bookId);
		        $countAppointment = count($bookList);
		        $appoinment = $salesAppointment;
		        $appointmentCount = $countAppointment;
		        $recList = array_unique($recuringCus);
		        $countOnline = count($recList);
	        }


	        $businessSalesRamdom = BusinessSales::where('bin',$request->bin)
	        ->whereMonth('bill_date',$monthly)
	        ->whereYear('bill_date',$years)
	        ->where('book_id',0)
	        ->get();

	        foreach ($businessSalesRamdom as $itemSalesR)
	        {
	            $salesRandom += $itemSalesR->pricing;
	            $customerId[] = $itemSalesR->customer_id;

	        }
	        if(count($businessSalesRamdom)>0)
	        {
		        $ramdomList = array_unique($customerId);
		        $countRandom = count($ramdomList);
		        $random = $salesRandom;
		        $randomCount = $countRandom;
		        $countOffline = count($ramdomList);
		    }
	        //Total
	        $salesTotal = $salesAppointment + $salesRandom;
	        $countTotal = $countAppointment + $countRandom;
	        $recuringTotal = $countOnline + $countOffline;
	        $total = $salesTotal;
	        $totalCount = $countTotal;

	        $sales[] = array("total" => $total,"appoinment" => $appoinment,"random" => $random);

	        $order[] = array("totalCount" => $totalCount,"appointmentCount" => $appointmentCount,"randomCount" => $randomCount);

	        $recuring[] = array("totalRecurring"=>$recuringTotal,"online" => $countOnline,"nonOnline" => $countOffline);

	        //4Month
	        $businessSalesCustomer4 = BusinessSales::where('bin',$request->bin)
            ->where('bill_date','>=',$start_4Month)
            ->distinct('customer_id')
            ->get();
            foreach($businessSalesCustomer4 as $itemSa4)
            {
                $cid4month[]=$itemSa4->customer_id;

            }
            if(count($businessSalesCustomer4)>0)
            {
            	$customerList4 = array_unique($cid4month);

            	$count4MonthCustomer = count($customerList4);
            }


	        //2Month
	        $businessSalesCustomer = BusinessSales::where('bin',$request->bin)
            ->where('bill_date','>=',$start_2Month)
            ->distinct('customer_id')
            ->get();
            foreach($businessSalesCustomer as $itemSa)
            {
                $cid2month[]=$itemSa->customer_id;

            }
            if(count($businessSalesCustomer)>0)
            {
            	$customerList = array_unique($cid2month);

            	$count2MonthCustomer = count($customerList);
            }




            $totalCustomer = $count4MonthCustomer + $count2MonthCustomer;

            $customer[] = array("totalCustomer" => $totalCustomer,"visitFourMonth" => $count4MonthCustomer, "visitTwoMonth" => $count2MonthCustomer);

	        for($i=0; $i < sizeof($analaytics) ; $i++)
	        {




	            $analaytics['salesOverView'] = $sales;
	            $analaytics['orderOverView'] = $order;
	            $analaytics['customerProscpects'] = $customer;
	            $analaytics['recurringCustomer'] = $recuring;

	        }
	   	}

	    return json_encode($analaytics);
  	}



  	public function updateBusinessOpenStatus(Request $request)
   {

   		$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }

        //block if  user is not store owner code # 1 is store owner or business
        if( $user_info->category  != 1  )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to business owner.'  ];
          return json_encode($data);
        }

        if( $request->bin  == ""  ||    ( $request->businessStatus != "open" && $request->businessStatus != "close"     ) )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


	    $business = Business::find($request->bin);
	    if( !isset($business))
	    {

	      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
	      'detailed_msg' => 'Business information not found!'  ];
	      return json_encode( $data);

	    }


	    if( $user_info->bin   != $business->id  )
        {
          	$data= ["message" => "failure", "status_code" =>  901 ,
          	'detailed_msg' =>  'You seem to not own the selected business!' ];
          	return json_encode($data);
        }

		$business->is_open = $request->businessStatus;

    	$business->save();

    	$data= ["message" => "success", "status_code" =>  5075 ,  'detailed_msg' => 'Business is updated as '. $request->businessStatus. "."  ];

    	return json_encode( $data);

  }



  public function getBusinessOpenStatus(Request $request)
   {
	

   		if( $request->bin  == "" )
        {
        	$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
        	return json_encode($data);
        }

        $business = Business::find($request->bin);
	    if( !isset($business))
	    {
	    	$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
	    	'detailed_msg' => 'Business information not found!'  ];
	    	return json_encode( $data);
	    }


		$status = $business->is_open;
		$name = $business->name;
		$address  = $business->locality . ", ". $request->landmark . " ". $request->city . " ". $request->state . " - ". $request->pin  ;


		$bannerImgUrl = $business->banner;

		if( $bannerImgUrl !=null)
		{
			$bannerImgUrl= URL::to('/public'. $bannerImgUrl );
		}
		else
		{
			$bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;
		}


    	$data= [
    		"message" => "success",
    		"status_code" =>  5075 ,
    		'detailed_msg' => 'Current business status fetched.',
    		'businessName' => $name ,
    		'businessStatus' => $status,
    		'locality' => ( $business->locality != "" ? $business->locality : "not provided"),
    		'landmark' => ( $business->landmark!= "" ? $business->landmark : "not provided") ,
    		'city' => ( $business->city!= "" ? $business->city : "not provided") ,
    		'state' => ( $business->state != "" ? $business->state : "not provided"),
    		'pin' => ( $business->pin != "" ? $business->pin : "not provided")  ,
    		'bannerImgUrl' => $bannerImgUrl
    	];

    	return json_encode( $data);

  }




   public function getBusinessCategories(Request $request)
   {
	   $pin_code = ""; 
	   
	   if($request->pinCode != "")
	   {
		  $pin_code=  $request->pinCode; 
	   }
	   else 
	   {
		   $user_info =  json_decode($this->getUserInfo($request->userId));
		   if(  $user_info->user_id != "na" )
		   {
			   $pin_code=  $user_info->pinCode;
		   }
	   }
	   $frno = 0;
	   if( $pin_code != "")
	   {
		   $franchise = DB::table("ba_franchise")
		   ->join("ba_franchise_areas", "ba_franchise_areas.zone_name", "=", "ba_franchise.zone")
		   ->select("ba_franchise.frno"  )
		   ->where("ba_franchise_areas.pin", $pin_code )  
		   ->first(); 
		   
		   $frno = isset($franchise) ? $franchise->frno : 0; 
		   
	   }
	
		
		
	
	
       $categories = DB::table("ba_business_category")
   		->select("id", "name", "icon_url as iconUrl" ,
   			"main_type as mainType", "total_biz as totalBiz",
   			"bin",  DB::Raw("'na' bizName"),  "display_type as uiLayoutMode"  )
   		->where("is_active", "yes")
   		->where("bin",  0 ) 
		->orderBy("display_order", "asc")
   		->get();
		
		
		
		$categories2  = DB::table("ba_business_category")
		->join("ba_business", "ba_business.id", "=", "ba_business_category.bin")
		->select("ba_business_category.id", "ba_business_category.name", "ba_business_category.icon_url as iconUrl" ,
				"main_type as mainType", "total_biz as totalBiz",
				"bin",  "ba_business.name as bizName" , "display_type as uiLayoutMode"  )
		->where("is_active", "yes")
		->where("bin", "<>" , 0)
		->where("ba_business.frno", $frno)
		->orderBy("display_order", "asc")
		->get();
		 
		


   		$final_result =array();
   		foreach($categories as   $item)
        {
        	$item->name = ucfirst( strtolower($item->name) );
        	$final_result[] = $item;

		}

 		foreach($categories2 as   $item)
        {
        	$item->name = ucfirst( strtolower($item->name) );
        	$final_result[] = $item;

		}


    	$data= [
    		"message" => "success",
    		"status_code" =>  5079 ,
    		'detailed_msg' => 'Business categories are fetched.',
    		'results' => $final_result
    	];

    	return json_encode( $data);

  }

}
