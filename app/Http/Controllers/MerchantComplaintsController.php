<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Traits\Utilities;
use App\MerchamtComplaintModel;
use App\Business;
use App\User;


class MerchantComplaintsController extends Controller
{  
   use Utilities;
    public function saveMerchantComplaint(Request $request)
    { 

       $biz_name=$request->bizname;
       $phone=$request->phone;
       $comp_nature=$request->nature;
       $comp_mgs=$request->complaint;
 



       if(isset($request->submit))
       {

        if(  $biz_name=="" || $phone=="" || $comp_nature=="" || $comp_mgs=="")
        {
           return redirect("/merchant-complaints-forms" )->with("err_msg", "Important field is missing!");
        } 

          if(strlen($phone!=10 && $phone>10 && $phone<10  ))
          {
            return redirect("/merchant-complaints-forms" )->with("err_msg", "You need to specify a valid phone number!");
          }




       /*
       $otp1=$request->number1;
       $otp2=$request->number2;
       $otp3=$request->number3;
       $otp4=$request->number4;
       $otp5=$request->number5;
       $otp6 =$request->number6;


       $otp = $otp1 . $otp2 . $otp3 . $otp4 . $otp5 . $otp6;
       */
       $otp_array[] =$request->number1;
       $otp_array[]=$request->number2;
       $otp_array[]=$request->number3;
       $otp_array[]=$request->number4;
       $otp_array[]=$request->number5;
       $otp_array[] =$request->number6;

       $otp = implode("",  $otp_array);

       // echo $otp;
       // return;


        $user_info = User::where("phone", $phone)->where("category", 1)->first();

        //$user_info = User::where("phone", $phone)->where("bin", "<>",  0 )->first();
        
        if(!isset($user_info))
        {
            return redirect("/merchant-complaints-forms" )->with("err_msg", "No business found with the specified phone!");
        }

        if( $user_info->otp != $otp  )
        {
            return redirect("/merchant-complaints-forms" )->with("err_msg", "OTP mismatched or expired!");
        }

 
          $merchant_complaint=new MerchamtComplaintModel; 
          $merchant_complaint->biz_name=$biz_name;
          $merchant_complaint->reg_phone_no=$phone;
          $merchant_complaint->complaint_nature=$comp_nature;
          $merchant_complaint->text_area=$comp_mgs;
          $merchant_complaint->complaint_date = date('Y-m-d H:i:s');
           $save = $merchant_complaint->save();
           if($save)
        {
          return redirect("/merchant-complaints-forms" )->with("err_msg", "Your complaint is successfully send!");   
        }
             

        }


        return view("admin/merchants.merchant_complaint_form_view");
    }



    public function viewAllComplaints(Request $request)
    {
        $complaint_table=DB::table("ba_merchant_complaints")
        ->where("status", "new")
        ->paginate(10);
        return view("admin.merchants.merchant_complaint_reports")->with('complaint_table',$complaint_table);
    }

    public function viewMerchantComplaints(Request $request)
    {   
     $session_user_id = session('__user_id_');   
       $complaint_table=DB::table("ba_merchant_complaints")
       ->where('id',$request->cid)
       ->get();

       $profile=DB::table("ba_profile")
       ->where('id',$session_user_id)
       ->select('fullname','phone')
       ->get();

       return view("admin.merchants.merchant_complaint_details")->with(array('complaint_table'=>$complaint_table,'profile'=>$profile)); 
    }


    protected function addRemarksMerchantComplaints(Request $request)
    {
        $session_user_id = session('__user_id_'); 
       $cid = $request->id;
       if($cid == "")
       {
         return redirect("/admin/merchants/view-merchant-complaints?cid= $cid"); 
       }

     if(isset($request->saveRemark)=="save")
     {
        if( strcasecmp($request->remarks,null) == 0 )
        {
           return redirect("/admin/merchants/view-merchant-complaints?cid= $cid")->with("err_msg", "Order remarks fields missing!"); 
        }
        
       $remarks=MerchamtComplaintModel::find($cid );
       $remarks->remarks= $request->remarks;
       $remarks->status= $request->status;
       $remarks->last_updated=date('Y-m-d H:i:s');
       $remarks->user_id= $session_user_id;
       $save = $remarks->save();
       if ($save)
        {
        return redirect("/admin/merchants/view-merchant-complaints?cid=$cid")
        ->with("err_msg","Add remarks successfully!");
        }

    else{
        return redirect("/admin/merchants/view-merchant-complaints?cid= $cid")
        ->with("err_msg","failed!");
        }

    }
}


      protected function showRemarksMerchantComplaints(Request $request)
      {

      }
   
}
