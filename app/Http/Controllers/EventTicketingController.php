<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

use App\EventTicketModel;
use App\EventTicketPurchase;
use App\EventTicketPurchaseDetails;
use App\EventWhatsAppCampaignLogModel;


use PDF;

use Razorpay\Api\Api;
use App\Traits\Utilities;

class EventTicketingController extends Controller
{

    use Utilities;

    
    protected function entryRegistration (Request $request)
    {
        echo "Page under construction";
    }


    protected function freePassRegistration (Request $request)
    {
        echo "Page under construction";
    }

    protected function ticketSaleForm (Request $request)
    { 

        return view('events.ticketing.ticket_sale_form');
    }
    
     
    protected function makeTicketPurchase (Request $request)
    {
        $cname = $request->cname;
        $cphone = $request->cphone;
        $tickettypes = $request->tickettype;
        
        $gateway_mode =  config('app.gateway_mode');
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );
        $ticket_cost = 200;
        $total_gst = 0;
        

        $purchase = new EventTicketPurchase;
        $purchase->visitor_name = $cname;
        $purchase->phone = $cphone;
        $save = $purchase->save();
        $purchase_no =0;
        $ticket_cost = 0 ;
        if($save)
        {
            $purchase_no = $purchase->id;
            foreach($tickettypes as $tickettype)
            {
                $pricing  = 0 ;
                $ticket_type = $tickettype;
                switch($tickettype)
                {
                    case "free":
                        $pricing = 0 ;
                        break;
                    case "first-day":
                        $pricing = 100;
                        break;
                    case "second-day":
                        $pricing = 100;
                        break;
                }
                
                $ticket_cost += $pricing;
                $purchase_detail = new EventTicketPurchaseDetails;
                $purchase_detail->purchase_id = $purchase_no;
                $purchase_detail->ticket_type = $ticket_type;
                $purchase_detail->pricing = $pricing;
                $purchase_detail->save();
            }
        } 

        $total_gst = 0;
        $gateway_charge = 0;
        $total_to_pay = 0;
        if( $ticket_cost > 0 )
        {
            $total_gst =  0 ;
            $gateway_charge = 0 ;
            $total_to_pay = $ticket_cost + $total_gst + $gateway_charge;


            $return_url = URL::to('events/ticketing/payment-completed');
            $notifyUrl = '';
            $item_notes = array(
            "orderId" => $purchase_no ,
            "orderAmount" => $total_to_pay * 100,
            "customerName" => $cname ,
            "customerPhone" =>  $cphone ,
            "customerEmail" =>   'booktougi@gmail.com' ,
            "returnUrl" => $return_url,
            "notifyUrl" => $notifyUrl,
            );

            $rzpapi = new Api(  $secure_key->key_id   , $secure_key->key_secret );
            $rzp_response =  $rzpapi->order->create(array
            ('receipt' =>  $purchase_no ,
            'amount' =>  $total_to_pay * 100,
            'currency' => 'INR', 
            'notes'=>  $item_notes                   
            ));
            

            if(isset($rzp_response))
            {
                //log razorpay order ID
                $rzp_order_id = $rzp_response->id;
                $purchase->rzp_id = $rzp_order_id ;
                $purchase->save();


                return redirect("/events/ticketing/payment-in-progress?o=". $purchase_no . "&rzpid=" . $rzp_order_id ) ;
            }
        }
        
        return redirect("/events/ticketing/view-ticket?purchase=" . Crypt::encrypt( $purchase_no  )  )->with("err_msg", 'Ticket purchased successfully!');
        

    }

    
    

protected function paymentInProgress(Request $request)
{ 
    
        $o= $request->o;

        if($o == "")
        {
            //redirect to parameter missing page
            return redirect("/events/ticketing/no-ticket-found")->with("err_msg", "Your ticket does not exist!") ;

        }
  
        $purchase_info = EventTicketPurchase::find( $o );
        if( !isset( $purchase_info) )
        {
          return redirect("/events/ticketing/ticket-sale-form")->with("err_msg", "Your ticket registration record not found!") ;
        }



        $gateway_mode =  config('app.gateway_mode'); 
        $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );

        $cname = $purchase_info->visitor_name;
        $cphone =  $purchase_info->phone;
        $cemail =  'booktougi@gmail.com' ;
        $ticket_cost  =100;
        $total_gst = 0;
        $total_payable = ( $ticket_cost  + $total_gst ) * 100;
        $rzpid  = $request->rzpid;

        $prefill = array("name" =>  $cname  , "email"=>  $cemail , "contact" =>  $cphone );
        $notes = array("item" => "bookTouX Film Festival Ticket" ,  "price"=> $total_payable );
      
        $data = array(
            "orderno" => $o ,
            "key" => $secure_key->key_id, 
            "amount" =>  $total_payable * 100, 
            "currency" => "INR",
            "name" =>  $cname  , 
            "description" => $cname , 
            "image" => "https://booktou.in/public/store/image/logo.png",
            "order_id" =>  $rzpid ,
            "callback_url" => URL::to("/events/ticketing/payment-completed")  ,
            "prefill" =>  $prefill ,
            "notes" => $notes 
        );

        return view("events.ticketing.payment_in_progress")->with( "data", $data);
    }



    protected function paymentCompleted(Request $request)
    {
      $razor_id= $request->rzid;
      if(  $razor_id == "" )
      {
        return redirect("/events/ticketing/no-ticket-found")->with("err_msg", "Your ticket does not exist!") ;
      }
      

      $purchase_info = EventTicketPurchase::where("rzp_id", $razor_id )->first();
      if( !isset( $purchase_info) )
      {
        return redirect("/events/ticketing/ticket-sale-form")->with("err_msg", "Your ticket registration record not found!") ;
      }


      $purchase_no = $purchase_info->id;
      $gateway_mode =  config('app.gateway_mode'); 
      $secure_key = json_decode(  $this->fetchRazorPaySecureKey( $gateway_mode ) );
      $response = $this->checkRazorPayOrderStatus( $razor_id, $gateway_mode );
 
      $payment_info =  $response['items'] ; 
      if(  isset(  $payment_info ) )
      {
        $payment_status = $response['items'][0]->status;
        $payment_id = $response['items'][0]->id;

        if(  strcasecmp( $payment_status , "captured") == 0 )
        {
          $purchase_info->rzp_pay_id =  $payment_id ;
          $purchase_info->payment_status  = "paid";
          $save=$purchase_info->save();


          /*
          //send sms
          $message_row = DB::table("ba_sms_templates")
          ->where("name", "event_ticket_download_link")
          ->first();

          if(isset($message_row))
          {
            $ticket_sms =  urlencode( str_replace(  "#param1", $rand,  $message_row->message ) );

            $this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id ,   array($userProfile->phone)   ,  $otp_msg  );
          }
          */
          
          return redirect("/events/ticketing/view-ticket?purchase=" .   Crypt::encrypt( $purchase_no  )   )->with("err_msg", 'Ticket purchased successfully!');
        }
        else
        {
            return redirect("/events/ticketing/view-ticket?purchase=" .  Crypt::encrypt( $purchase_no  )  )->with("err_msg", 'Payment is pending for your purchase!');
        }
      }
      else
      {
        return redirect("/events/ticketing/view-ticket?purchase=" .  Crypt::encrypt( $purchase_no  )   )->with("err_msg", 'Payment is pending for your purchase!');      
      }

    }
    
    
    
    protected function viewTicket(Request $request)
    {
        $enc_pid = $request->purchase;
        $purchaseid = 0;
        try
        {
           $purchaseid =  Crypt::decrypt( $enc_pid );
           
           $ticket_purchase =EventTicketPurchase::find($purchaseid );

           $purchased_items  = DB::table("event_ticket_purchase_details")
           ->where("purchase_id",  $purchaseid)
           ->get();

           if(   !isset($ticket_purchase ) || !isset( $purchased_items )   )
           {
                return redirect("/events/ticketing/ticket-sale-form" )->with("err_msg", 'Purchase ticket not found!');
           }
           
           $event_id = $ticket_purchase->event_id;

           $cdn_url = config('app.app_cdn');
           $cdn_path =  config('app.app_cdn_path');

           $folder_path =  $cdn_path  . '/assets/events/tickets/';
           if( !File::isDirectory($folder_path))
           {
             File::makeDirectory( $folder_path, 0777, true, true);
           }


           if( $ticket_purchase->ticket_generated  == 'no')
           {
                foreach($purchased_items as $purchased_item)
                {
                    if(!in_array( $purchased_item->ticket_type, array('free', 'first-day', 'second-day')  ) )
                    {
                        continue;
                    }
                    
                    $date = '2023-2-3';

                    if( strcasecmp( $purchased_item->ticket_type, "free" )  == 0 )
                    {
                        $ticket_no = "FP". time();
                        $ticket_format = "FREE";
                    }else if( strcasecmp($purchased_item->ticket_type, "first-day" )  == 0 )
                    {
                        $ticket_no = "FD". time();
                        $date = '2023-2-3';
                        $ticket_format = "ONL";
                    }
                    else
                    {
                        $ticket_no = "SD". time();
                        $date = '2023-2-4';
                        $ticket_format = "ONL";
                    }
                    
                    //generate qr codes
                    $qr_url =   $cdn_url . "/public/assets/events/tickets/" . $ticket_no . ".png" ;
                    $qr_path =  $cdn_path . "/assets/events/tickets/" . $ticket_no . ".png" ;
                    QrCode::format('png')->size(500)->generate( $ticket_no , $qr_path );
                    
                    $new_ticket = new EventTicketModel;
                    $new_ticket->ticket_no  = $ticket_no ;
                    $new_ticket->purchase_id  = $purchaseid;
                    $new_ticket->valid_date = $date ;
                    $new_ticket->price = $purchased_item->pricing;
                    $new_ticket->visitor_name  = $ticket_purchase->visitor_name;
                    $new_ticket->visitor_phone  = $ticket_purchase->phone;
                    $new_ticket->qrcode_url  =  $qr_url ;
                    $new_ticket->qrcode_path  =  "/assets/events/tickets/" . $ticket_no . ".png" ;

                    $new_ticket->ticket_format  =  $ticket_format ;
                    $new_ticket->current_status  =  0;
                    $new_ticket->event_id  = $event_id;
                    $new_ticket->save();

                }

                $ticket_purchase->ticket_generated  = 'yes';
                $ticket_purchase->save();
            }


            $ticket_files  = DB::table("event_ticket")
            ->where("purchase_id",  $purchaseid)
            ->get();
            
            $data = array('ticket_files'=> $ticket_files  );
            return view('events.ticketing.ticket_success')->with($data);


        }
        catch (DecryptException $e)
        {
            return redirect("/events/ticketing/ticket-sale-form" )->with("err_msg", 'Purchase ticket not found!');
        }
    }

 

    
    public function downloadTicket( Request $request)
    {
        $enc_pid = $request->purchase;
        $purchaseid = 0;
        $slno  = $request->slno;

        if($slno== "")
        {
            return redirect("/events/ticketing/ticket-sale-form" )->with("err_msg", 'Purchase ticket not found!');
        }
        try
        {
            $purchaseid =  Crypt::decrypt( $enc_pid );

            $ticket_file_info   = DB::table("event_ticket")
            ->where("id",   $slno)
            ->where("purchase_id",  $purchaseid)
            ->first();
           
            if( !isset( $ticket_file_info )  )
            {
                return redirect("/events/ticketing/ticket-sale-form" )->with("err_msg", 'Purchase ticket not found!');
            }
           
           $cdn_url = config('app.app_cdn');
           $cdn_path =  config('app.app_cdn_path');

           $folder_path =  $cdn_path  . '/assets/events/tickets/pdf/';
           if( !File::isDirectory($folder_path))
           {
                File::makeDirectory( $folder_path, 0777, true, true);
           }
           
           $qr_path = $ticket_file_info->qrcode_path;
           $ticket_format = $ticket_file_info->ticket_format;
           $valid_date = $ticket_file_info->valid_date;
           
           $pdf_file =  "venuepass" . $ticket_file_info->id . ".pdf";
           $save_path = $folder_path . '/assets/events/tickets/pdf/' .  $pdf_file;
            

           $pdf = PDF::setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true  ])
           ->setPaper('a4', 'portrait')
           ->loadView('events.ticketing.vertical_ticket' ,  array( 'qr_path' =>   $qr_path, "ticket_format" => $ticket_format,  'valid_date' => $valid_date )  );

        }
        catch (DecryptException $e)
        {
            return redirect("/events/ticketing/ticket-sale-form" )->with("err_msg", 'Purchase ticket not found!');
        }
        
        //return view('events.ticketing.vertical_ticket')->with( array( 'qr_path' =>   $qr_path )  );
        return $pdf->download( $pdf_file  ); 

     
    } 




    protected function phoneVerification(Request $request)
    {
        if(isset( $request->phoneno ))
        {
            $purchases = DB::table("event_ticket_purchase")
            ->where("phone", $request->phoneno)  
            ->where("payment_status",  "paid" )
            ->get();

            if ( isset( $purchases ))
            {
                //generate OTP
                $rand = mt_rand(111111, 999999);

                DB::table("event_ticket_purchase")
                ->where("phone", $request->phoneno)  
                ->where("payment_status",  "paid" )
                ->update( array('otp' =>  $rand) );

                $message_row = DB::table("ba_sms_templates")
                ->where("name", "otp_generate")
                ->first();

                  if(isset($message_row))
                  {
                      $otp_msg =  urlencode( str_replace(  "###", $rand,  $message_row->message ) );
                      $this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id ,   array( $request->phoneno )   ,  $otp_msg  );
                  }
                return view('events.ticketing.verify_otp');
            }
            else
            {
                $msg = "No matching purchase information found!";
                return redirect("/events/ticketing/ticket-sale-form")->with('err_msg',$msg);
            }
        }else
        {
            return view('events.ticketing.phone_verification');
        }

    }


    protected function otpVerification(Request $request)
    {
        if( $request->otp  == "" || $request->phoneno  == "" )
        {
            return redirect("/events/ticketing/phone-verification")->with('err_msg', "OTP not provided.");

        }


        $purchases = DB::table("event_ticket_purchase")
        ->where("phone", $request->phoneno)  
        ->where("payment_status",  "paid" )
        ->get();

        $otp_matched = true;
        foreach($purchases as $purchase)
        {
            if( $purchase->otp !=   $request->otp )
            {
                $otp_matched = false;
                break;
            }
        }

        if (  $otp_matched )
        {
            return redirect("/events/ticketing/ticket-purchases?i=" . Crypt::encrypt( $request->phoneno )  )
            ->with("err_msg", "OTP mismatched!");
        }
        else
        {
            return redirect("/events/ticketing/phone-verification?phoneno=" . $request->phoneno )->with("err_msg", "OTP mismatched!");
        }

    }

    protected function ticketPurchaseList(Request $request)
    {
        $i = $request->i;

        if( $i == "" )
        {
            return redirect("/events/ticketing/ticket-sale-form")->with('err_msg', "OTP not provided.");
        }

        try
        {
           $phoneno =  Crypt::decrypt( $i );

           $purchases = DB::table("event_ticket_purchase")
            ->where("phone",  $phoneno )  
            ->where("payment_status",  "paid" )
            ->get();


            return view('events.ticketing.purchases_list')->with( array( 'purchases' => $purchases ) );


        }
        catch (DecryptException $e)
        {
            return redirect("/events/ticketing/ticket-sale-form" )->with("err_msg", 'Purchase ticket not found!');
        }

    }

    protected function sendWhatsappMessage(Request $request)
    {

        $this->sendWhatsAppMessageToRegisteredPhone( "917005894963");

    }

    protected function sendWhatsappInvitationMessage(Request $request)
    {

        if(isset($request->submit))
        {
            $max_profile_id  = DB::table("ba_profile")->max("id");
            $last_sent_id  = DB::table("event_whatsapp_cpn_log")->min("profile_id");
            $last_sent_id = isset($last_sent_id) == "" ? $max_profile_id: $last_sent_id;
 


            $profiles = DB::table("ba_profile")
            ->where("id", "<=", $last_sent_id )
            ->whereNotIn("phone", function( $query) {
                $query->select("phone")
                ->from("event_whatsapp_cpn_log")
                ->whereDate("sent_date", "<>",  date('Y-m-d') );
            })
            ->where("locality", "like", "%imphal%")
            ->orWhere("landmark", "like", "%imphal%") 
            ->orWhere("city", "like", "%imphal%") 
            ->orderBy("id",  "desc" )
            ->paginate(100);

            foreach($profiles as $profile)
            {
                $sent_log = new EventWhatsAppCampaignLogModel;
                $sent_log->phone = $profile->phone;
                $sent_log->profile_id = $profile->id;
                $sent_log->sent_date = date('Y-m-d H:i:s');
                $sent_log->save();

                $this->sendWhatsAppEventInvitation( "91" .  $profile->phone );

            }

        }
        else
        {
            $profiles = DB::table("ba_profile")
            ->join("event_whatsapp_cpn_log", "event_whatsapp_cpn_log.phone", "=", "ba_profile.phone")
            ->select("ba_profile.*")
            ->orderBy("id",  "desc" )
            ->paginate(100);
        }

        $total_sent  = DB::table("event_whatsapp_cpn_log")->whereDate("sent_date",  date('Y-m-d') )->count();

        return view('events.ticketing.campaign_receipent')->with( array( 'profiles' => $profiles, 'total_sent' =>$total_sent  ) );


    }


}
