<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Traits\Utilities;
use App\User;
use App\Business;
use App\CateringMainMenu;
use App\CateringSubMenu;
use App\CateringBooking;
use App\CateringBookingDetails;
use App\CateringSales;
use App\ServiceBooking;
use App\BusinessCategory;
use App\BusinessService;
use App\CateringCategory;
use App\FavouriteBusiness;


class CateringApiController extends Controller
{
    use Utilities;
    //Fetch Main menu for Catering
    public function getMainMenu(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        $arrayBin[]=0;
        $catering = CateringMainMenu::where('bin',$request->bin)
        ->whereNotIn('bin',$arrayBin)
        ->select('id as menuId','name','description')
        ->get();
        if( count($catering) > 0)
        {
            $arrayBin[]=$request->bin;
            $cateringMainMenu = CateringMainMenu::whereIn('bin',$arrayBin)
                ->select('id as menuId','name','description')
                ->get();
        }
        else{

            $cateringMainMenu = CateringMainMenu::whereIn('bin',$arrayBin)
            ->select('id as menuId','name','description')
            ->get();
        }
    	//$cateringMainMenu = CateringMainMenu::select('id as menuId','name','description')->get();
    	$data= ["message" => "success", "status_code" =>  1051 ,  'detailed_msg' => 'Fetch Business Catering Main Menu',"results" => $cateringMainMenu];

        return json_encode( $data);
    	
    }
    //Add Sub menu for Catering
    public function addSubMenu(Request $request)
    {
    	$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",    "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        $mainMenu = CateringMainMenu::find($request->mainId);
        if( !isset($mainMenu))
        {
          
          $data= ["message" => "failure",    "status_code" =>  903 ,
          'detailed_msg' => 'Main menu information not found!'  ];
          return json_encode( $data);
          
        }
        if($request->bin == "" || $request->name == "" || $request->description == "" || $request->mainId == ""|| $request->price == ""|| $request->categoryId == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
        $cateringSubMenu = CateringSubMenu::where('bin',$request->bin)
        ->where('name',$request->name)
        ->where('main_id',$request->mainId)
        ->first();
        if(isset($cateringSubMenu))
	    {
	    	$data= ["message" => "failure", "status_code" =>  1002 , 'detailed_msg' => 'Sub menu is already exists!'  ];
	     	return json_encode($data);
	    }
        $newSubMenu = new CateringSubMenu;
        $newSubMenu->bin = $request->bin;
        $newSubMenu->name = $request->name;
        $newSubMenu->description = $request->description;
        $newSubMenu->main_id = $request->mainId;
        $newSubMenu->category = $request->categoryId;
        $newSubMenu->price = $request->price;
        $save=$newSubMenu->save();
        if($save)
        {
        	$data= ["message" => "success", "status_code" =>  1053 ,  'detailed_msg' => 'Catering Sub Menu'];
        }
        else
        {
        	$data= ["message" => "failure", "status_code" =>  1054 ,  'detailed_msg' => 'Catering Sub Menu'];
        }
    	return json_encode( $data);
    	
    }
    //Update Sub menu for Catering
    public function updateSubMenu(Request $request)
    {
    	$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        if($request->subMenuId == ""||$request->mainId== "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
        
        $cateringSubMenu = CateringSubMenu::find($request->subMenuId);
        if(!isset($cateringSubMenu))
        {
        	$data= ["message" => "failure", "status_code" =>  1056 ,  'detailed_msg' => 'Update Catering Sub Menu failed'];
        	return json_encode( $data);
        }
        if($request->bin!=null)
        {
        	$cateringSubMenu->bin = $request->bin;
        }
        if($request->name!=null)
        {
        	$cateringSubMenu->name = $request->name;
        }    	
        if($request->description!=null)
        {
        	$cateringSubMenu->description = $request->description;
        }
        $cateringSubMenu->main_id = $request->mainId;
        if($request->categoryId!=null)
        {
            $cateringSubMenu->category = $request->categoryId;
        }
        if($request->price!=null)
        {
        	$cateringSubMenu->price = $request->price;
        }
        $save=$cateringSubMenu->save();
	   
	    if($save)
        {
        	$data= ["message" => "success", "status_code" =>  1055 ,  'detailed_msg' => 'Update Catering Sub Menu'];
        }
    	return json_encode( $data);
        
    }
    //Delete Sub menu for Catering
    public function deleteSubMenu(Request $request)
    {
    	$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
    	$subMenuTable = CateringSubMenu::where('id',$request->subMenuId)->get();
        if(count($subMenuTable)>0)
        {
            foreach ($subMenuTable as $keySub) 
            {
                $serviceData = CateringSubMenu::find($keySub->id);
                $serviceData->delete();
            }
            $data= ["message" => "success", "status_code" =>  555 ,  'detailed_msg' => 'Delete Sub Menu Item.'  ];
    
        }
        else{
            $data= ["message" => "failure", "status_code" =>  556 ,  'detailed_msg' => 'Delete Sub Menu Item Fail.'  ];
        }
        return json_encode( $data);
    }
    //Fetch Menu for Catering to their business admin
    public function fetchSubMenu(Request $request)
    {
    	$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        if($request->bin == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.',"results" => array()  ];
         return json_encode($data);
        }
        $mainMenuId = array();
        $cateringMenu = DB::table("ba_catering_sub_menu")
            ->join("ba_catering_main_menu","ba_catering_sub_menu.main_id","=","ba_catering_main_menu.id") 
            ->where('ba_catering_sub_menu.bin',$request->bin)
            ->select('ba_catering_main_menu.id as mainMenu','ba_catering_main_menu.name','ba_catering_main_menu.description',DB::Raw("'[]' submenu"))
            ->distinct('ba_catering_main_menu.id')
            ->get();
        
        if(!isset($cateringMenu))
        {
        	$data= ["message" => "failure", "status_code" =>  1058 ,  'detailed_msg' => 'Fetch Catering Sub Menu failed',"results" => array()];
        	return json_encode( $data);
        }
        foreach ($cateringMenu as $itemMain) {
        	$mainMenuId[]=$itemMain->mainMenu;
        }
        $cateringSubMenu = DB::table("ba_catering_sub_menu")
        ->where('bin',$request->bin)
        ->whereIn('main_id',$mainMenuId)
		->select('id as subMenuId','bin','name','description','main_id as mainMenuId','price')
		->orderBy("main_id", "asc")
        ->get() ;

        //return $cateringSubMenu;
        
        foreach ($cateringMenu as $itemM) 
        {
        	$pos=0;  
        	$temp = array();      	
        	foreach ($cateringSubMenu as $itemS) 
        	{
        		if($itemM->mainMenu == $itemS->mainMenuId )
        		{
        			$temp[] = $itemS;	
        			 
        		} 	
        	}
        	$itemM->submenu= $temp; 
        } 

        $data= ["message" => "success", "status_code" =>  1057 ,  'detailed_msg' => 'Fetch Catering Menu',"results" => $cateringMenu];
        
    	return json_encode( $data);
        
    }
    //Booking a Catering Service by customer
    public function saveCateringServiceBooking(Request $request)
    {
    	$user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",   "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        if($request->bookingDate == "" || $request->bookingTime == "" || $request->menu == "" || $request->cusAddress == "" || $request->serviceCode == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
        //booked ServiceBooking
        $bookDate = date('Y-m-d', strtotime($request->bookingDate));
        $bookTime = date('H:i:s', strtotime($request->bookingTime));
        $otp = mt_rand(1000, 9999);
        $cateringBooking=new ServiceBooking;
        $cateringBooking->bin= $request->bin;
        $cateringBooking->book_date= $bookDate.' '.$bookTime;
        $cateringBooking->book_by= $user_info->user_id;
        $cateringBooking->staff_id= 0;
        $cateringBooking->address= $request->cusAddress;
        $cateringBooking->category= 2;//catering
        $cateringBooking->otp= $otp;
        $cateringBooking->book_status= "new";
        $cateringBooking->save();
        $cateringBookingPlate = new CateringBooking;
    	$cateringBookingPlate->book_id = $cateringBooking->id;
        $cateringBookingPlate->service_code = $request->serviceCode;
    	$cateringBookingPlate->plate = $request->plate;
    	if($request->otherRequest!=null)
    	{
    		$cateringBookingPlate->other_request = $request->otherRequest;
    	}
    	$save=$cateringBookingPlate->save();

        $subMenuArray  = array();
        if($save)
        {
        	$properties=$request['menu'];
        	for($i=0;$i<sizeof($properties);$i++)
			{
				$menu=$properties[$i];
                $subMenuArray[] = $menu['subMenuId'];
            }
            //call submenu
            $cateringSubMenu=DB::table("ba_catering_sub_menu")
            ->join("ba_catering_main_menu","ba_catering_sub_menu.main_id","=","ba_catering_main_menu.id")
            ->where('ba_catering_sub_menu.bin',$request->bin)
            ->whereIn('ba_catering_sub_menu.id',$subMenuArray)
            ->select('ba_catering_sub_menu.*','ba_catering_main_menu.id as mainMenuId','ba_catering_main_menu.name as mainMenuName')
            ->get();
            
            foreach ($cateringSubMenu as $item) {

                //booked CateringBookingDetails 
                $cateringBookingDetails=new CateringBookingDetails;
                $cateringBookingDetails->book_id= $cateringBooking->id;
                $cateringBookingDetails->name= $item->name;
                $cateringBookingDetails->description= $item->description;
                $cateringBookingDetails->main_name= $item->mainMenuName;
                $cateringBookingDetails->price= $item->price;
                $cateringBookingDetails->status= "new";
                $cateringBookingDetails->save();
                $cateringDetails[]=$cateringBookingDetails;
                $catering = array("booked" => $cateringBooking ,"bookedCatering" => $cateringBookingPlate,"bookedDetails" => $cateringDetails);
                $subMenu[] = array("subMenuId"=>$cateringBookingDetails->id);
                # code...
            }

                            
			$data= ["message" => "success", "status_code" =>  1059 ,  'detailed_msg' => 'Your booking for catering is placed successfully.'];

        }
        else{
        	$data= ["message" => "success", "status_code" =>  1059 ,  'detailed_msg' => 'Failed to place your booking details.'];
        }
  		return json_encode( $data); 	

    }
    
    //Fetch Catering Booking Service for customer
    public function getCateringDetails(Request $request)
    {
    	
        $arrayStatus=["cancel_by_client","cancel_by_owner"];
     	//fetch ServiceBooking
        $cateringBooking = DB::table("ba_service_booking")
        ->join("ba_catering_booking","ba_catering_booking.book_id","=","ba_service_booking.id")
        ->where("ba_service_booking.id", $request->bookingNo )
		->whereNotIn('ba_service_booking.book_status',$arrayStatus)
        ->select('ba_service_booking.id as bookingNo','ba_service_booking.bin' , 
        'ba_service_booking.book_date as appointmentDate',DB::Raw("'--' appointmentTime"),'ba_service_booking.book_by as customerId', 
        'ba_service_booking.address as customerAddress','ba_service_booking.otp as serviceOTP', 
        'ba_service_booking.book_status as cateringStatus','ba_catering_booking.plate','ba_catering_booking.other_request as otherRequest', 
        'ba_catering_booking.service_code as serviceCode',DB::Raw("'' serviceName"),DB::Raw("'' serviceDetails"), 
        DB::Raw("'' servicePricing"), DB::Raw("'catering' category") , DB::Raw("'[]' bookingDetails"))
        ->orderBy("ba_service_booking.book_date", "asc")
        ->get();
       
        
        $bookedId=array();
        foreach ($cateringBooking as $itemC) {
        	$itemC->bookingDetails = array();
        	$bookedId[]=$itemC->bookingNo;
            $serviceCoder=$itemC->serviceCode;
            $bin=$itemC->bin;

        }
        //service code 
        $services= DB::table("ba_business_services")
        ->where("bin",  $bin )
        ->where("srv_code",$serviceCoder)
        ->select("srv_code as serviceCode",  
            "srv_name as serviceName", 
            "srv_details as details", 
            "pricing", 
            "duration_hr as durationHour",
            "duration_min as durationMinute" )
            ->get();
           
        //fetch CateringBookingDetails
        $cateringBookingDetails = CateringBookingDetails::whereIn('book_id',$bookedId)
        ->select('id as detailsId','book_id as bookId','name as itemName','description as itemDescription','main_name as mainName','price','status')
        ->orderBy("main_name", "asc")
        ->get();
        //BusinessCategory  
        $busCategory = BusinessCategory::select('id as categoryId','name')->get();

        foreach ($cateringBooking as $item) 
        {
            $pos=0;  
            $temp = array();        
            foreach ($cateringBookingDetails as $itemD) 
            {
                if($item->bookingNo == $itemD->bookId )
                {
                    $temp[] = $itemD;   
                     
                }   
            }
            $item->bookingDetails= $temp; 
            $bookedDateC = date('d-m-Y', strtotime($item->appointmentDate));
            $bookedTimeC = date('h:i a', strtotime($item->appointmentDate));
            $item->appointmentDate = $bookedDateC;
            $item->appointmentTime = $bookedTimeC;
 

            //Service Code replace to its name
            foreach($services as $itemS)
            {
                if($item->serviceCode == $itemS->serviceCode)
                {
                    $item->serviceName = $itemS->serviceName;
                    $item->serviceDetails = $itemS->details;
                    $item->servicePricing = $itemS->pricing;

                }
            }


        } 

        $data= ["message" => "success", "status_code" =>  1061 ,  'detailed_msg' => 'Fetch Catering Menu',"results" => $cateringBooking];

        return json_encode( $data);

    }
    //Update or Edit Catering Details
    public function updateCateringDetails(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->customerId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",   "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        if($business->category != $request->categoryId)
        {
            $data= ["message" => "failure",   "status_code" =>  997 ,
          'detailed_msg' => 'Business information for Catering is not found!'  ];
          return json_encode( $data);
        }
        if($request->bookingNo == ""||$request->bin == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
        $arrayStatus=["cancel_by_client","cancel_by_owner","engaged"];
        //ServiceBooking table
        $serviceBooking = ServiceBooking::where('id',$request->bookingNo)
        ->whereNotIn('book_status',$arrayStatus)
        ->first();
        if( !isset($serviceBooking))
        {
          
          $data= ["message" => "failure",  "status_code" =>  1064 ,
          'detailed_msg' => 'Failed to place your booking'  ];
          return json_encode( $data);
          
        }
        $bookDate = date('Y-m-d', strtotime($request->appointmentDate));
        $bookTime = date('H:i:s', strtotime($request->appointmentTime));
        $serviceBooking->book_date= $bookDate.' '.$bookTime;
        if($request->customerAddress!=null)
        {
           $serviceBooking->address= $request->customerAddress; 
        }
        $serviceBooking->save();
        //CateringBooking table
        $cateringBooking = CateringBooking::where('book_id',$serviceBooking->id)->first();
        if($request->serviceCode!=null)
        {
            $cateringBooking->service_code=$request->serviceCode;
        }
        if($request->plate!=null)
        {
            $cateringBooking->plate=$request->plate;
        }
        if($request->otherRequest!=null)
        {
            $cateringBooking->other_request = $request->otherRequest;
        }
        $cateringBooking->save();
        //CateringBookingDetails table
        $cateringBookingDetails = CateringBookingDetails::where('book_id',$serviceBooking->id)->get();
        if(isset($cateringBookingDetails))
        {
            foreach ($cateringBookingDetails as $keyidd) 
            {
                $detailId[]=$keyidd->id;
                $serviceData = CateringBookingDetails::find($keyidd->id);
                $serviceData->delete();
            }
            
        }
        $arrayId = $detailId;
        $pos =0;
        $properties=$request['bookingDetails'];
        for($i=0;$i<sizeof($properties);$i++)
        {
            $menu=$properties[$i];
            //booked CateringBookingDetails 
            $cateringBookingDetails=new CateringBookingDetails;
            $cateringBookingDetails->book_id= $serviceBooking->id;
            $cateringBookingDetails->name= $menu['itemName'];
            $cateringBookingDetails->description= $menu['itemDescription'];
            $cateringBookingDetails->main_name= $menu['mainName'];
            $cateringBookingDetails->price= $menu['price'];
            $cateringBookingDetails->status= $menu['status'];
            $cateringBookingDetails->save();
            $pos++;
            $cateringDetails[]=$cateringBookingDetails;
            $catering = array("booked" => $serviceBooking ,"bookedCatering" => $cateringBooking,"bookedDetails" => $cateringDetails);
            
        }
        $data= ["message" => "success", "status_code" =>  1063 ,  'detailed_msg' => 'Your booking is update successfully'];

        return json_encode( $data);

    }
    //Add Main menu for Catering
    public function addMainMenu(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));

        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        if($user_info->category==0)
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",   "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        if($business->category!=2)
        {
            $data= ["message" => "failure",   "status_code" =>  997 ,
          'detailed_msg' => 'Business information for Catering is not found!'  ];
          return json_encode( $data);
        }
        if($request->catName == "" || $request->catDesp == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
        $arrayBin[]=0;
        $catering = CateringMainMenu::where('name',$request->catName)
        ->where('description',$request->catDesp)
        ->whereIn('bin',$arrayBin)
        ->first();
        
        if(isset($catering))
        {
            $data= ["message" => "failure", "status_code" =>  1066 ,  'detailed_msg' => 'Main Menu is already exists!'];
            return json_encode( $data);
        }
        $catering1 = CateringMainMenu::where('name',$request->catName)
        ->where('description',$request->catDesp)
        ->where('bin',$request->bin)
        ->whereNotIn('bin',$arrayBin)
        ->first();
        
        if(isset($catering1))
        {
            $data= ["message" => "failure", "status_code" =>  1066 ,  'detailed_msg' => 'Main Menu is already exists!'];
            return json_encode( $data);
        }
        $cateringMainMenu = new CateringMainMenu;
        $cateringMainMenu->name = $request->catName;
        $cateringMainMenu->description = $request->catDesp;
        $cateringMainMenu->bin = $request->bin;
        $save = $cateringMainMenu->save();
        if($save)
        {
            $data= ["message" => "success", "status_code" =>  1065 ,  'detailed_msg' => 'Add Catering Main Menu'];
        }
        else{
            $data= ["message" => "failure", "status_code" =>  1066 ,  'detailed_msg' => 'Add Catering Main Menu could not be saved!'];
        }
        return json_encode( $data);
        
    }
    //Update Main menu for Catering
    public function updateMainMenu(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));

        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        if($user_info->category==0)
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",    "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        if($business->category!=2)
        {
            $data= ["message" => "failure",    "status_code" =>  997 ,
          'detailed_msg' => 'Business information for Catering is not found!'  ];
          return json_encode( $data);
        }
        if($request->mainId == "" || $request->catName == "" || $request->catDesp == "")
        {
             $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.' ];
             return json_encode($data);
        }
        $arrayBin[]=0;
        $catering = CateringMainMenu::where('name',$request->catName)
        ->where('description',$request->catDesp)
        ->where('bin',$request->bin)
        ->whereNotIn('bin',$arrayBin)
        ->first();
        
        if(isset($catering))
        {
            $data= ["message" => "failure", "status_code" =>  1068 ,  'detailed_msg' => 'Main Menu is already exists!'];
            return json_encode( $data);
        }

        
        $cateringMainMenu = CateringMainMenu::where('id',$request->mainId)
        ->where('bin',$request->bin)
        ->whereNotIn('bin',$arrayBin)
        ->first();
        
        if(!isset($cateringMainMenu))
        {
            $data= ["message" => "failure", "status_code" =>  1068 ,  'detailed_msg' => 'Update Catering Main Menu could not be saved!'];
            return json_encode( $data);

        }
        $cateringMainMenu->name = $request->catName;
        $cateringMainMenu->description = $request->catDesp;
        $save = $cateringMainMenu->save();
        if($save)
        {
            $data= ["message" => "success", "status_code" =>  1067 ,  'detailed_msg' => 'Update Catering Main Menu'];
        }
        return json_encode( $data);
        
    }
    //Delete Main menu for Catering
    public function deleteMainMenu(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));

        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        if($user_info->category==0)
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }
        $business = Business::find($request->bin);
        if( !isset($business))
        {
          
          $data= ["message" => "failure",  "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
          return json_encode( $data);
          
        }
        if($business->category!=2)
        {
            $data= ["message" => "failure",  "status_code" =>  997 ,
          'detailed_msg' => 'Business information for Catering is not found!'  ];
          return json_encode( $data);
        }
        if($request->mainId == "")
        {
             $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
             return json_encode($data);
        }
        $arrayBin[]=0;
          
        $cateringMainMenu = CateringMainMenu::where('id',$request->mainId)
        ->where('bin',$request->bin)
        ->whereNotIn('bin',$arrayBin)
        ->first();
        
        if(!isset($cateringMainMenu))
        {
            $data= ["message" => "failure", "status_code" =>  1070 ,  'detailed_msg' => 'Delete Main Menu Item fail.!'];
            return json_encode( $data);

        }
        $save = $cateringMainMenu->delete();
        if($save)
        {
            $data= ["message" => "success", "status_code" =>  1069 ,  'detailed_msg' => 'Delete Main Menu Item.'];
        }
        return json_encode( $data);
        
    }

    //Fetch Business Catering veg or non veg
    public function getCateringBusiness(Request $request)
    {
        $binn = $binCa = $binS =array();
        
        $chkBusiness=Business::where('category', "catering")->get();
        foreach ($chkBusiness as $keybus) {
            if($keybus->longitude && $keybus->tags && $keybus->locality && $keybus->phone_pri && $keybus->latitude && $keybus->name && $keybus->tag_line && $keybus->shop_number !=null)
            {
                $binn[]=$keybus->id;
            }
            
        }
        $durationList = array_unique($binn);

        //
        if($request->cateringCategory=="")
        {
            $cateringMenu = DB::table("ba_catering_sub_menu")
            ->join("ba_catering_main_menu","ba_catering_sub_menu.main_id","=","ba_catering_main_menu.id") 
            ->whereIn('ba_catering_sub_menu.bin',$durationList)
            ->select('ba_catering_main_menu.id as mainMenuId','ba_catering_sub_menu.bin')
            ->distinct('ba_catering_main_menu.id')
            ->get();
        }
        else{
            $cateringMenu = DB::table("ba_catering_sub_menu")
            ->join("ba_catering_main_menu","ba_catering_sub_menu.main_id","=","ba_catering_main_menu.id") 
            ->whereIn('ba_catering_sub_menu.bin',$durationList)
            ->where('ba_catering_sub_menu.category',$request->cateringCategory)
            ->select('ba_catering_main_menu.id as mainMenuId','ba_catering_sub_menu.bin')
            ->distinct('ba_catering_main_menu.id')
            ->get();
        }
        

        foreach ($cateringMenu as $keyC) {
            $binCa[] = $keyC->bin;

        }    
        $cateringList = array_unique($binCa);
        //Service table
        $businessService = BusinessService::whereIn('bin',$cateringList)
            ->distinct('bin')
            ->get();
        foreach ($businessService as $keyS) {
            $binS[] = $keyS->bin;

        }
        $serviceList = array_unique($binS);

        $businesses = DB::table("ba_business")
            ->whereIn('id',$serviceList)     
          ->select( 'id as bin',  "name", "tag_line as tagLine", 'shop_number as shopNumber',  
          'phone_pri as primaryPhone', 'phone_alt as alternatePhone', 
          "locality", "landmark", "city", "state", "pin", 
          'tags','rating','latitude','longitude' , 
          "profile_image as profileImgUrl",   "banner as bannerImgUrl" ,
        DB::Raw("'na' promoImgUrl"),DB::Raw("'na' favourite"),"category as businessCategory" )
          ->paginate(5);

        //BusinessCategory  
        $busCategory = BusinessCategory::select('id as categoryId','name')->get();  
        
        foreach($businesses as $item)
        {
            if($item->profileImgUrl!=null)
            {
                $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl); 
            }
            else{
                $item->profileImgUrl =  URL::to('/public/assets/image/no-image.jpg' );
            }
            if($item->bannerImgUrl!=null)
            {
                $item->bannerImgUrl = URL::to('/public'.$item->bannerImgUrl);   
            }
            else{
                $item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg' );
            }
            if($item->promoImgUrl!='na')
            {
                $item->promoImgUrl = URL::to('/public'.$item->promoImgUrl); 
            }
            else{
                $item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg' );
            }
                    
            $favorite = FavouriteBusiness::where('user_id',$request->userId)
                ->where('bin',$item->bin)->get();

                $item->favourite = count($favorite);

            foreach($busCategory as $itemCat)
            {
                if($item->businessCategory == $itemCat->categoryId)
                {
                    $item->businessCategory = $itemCat->name;
                }
            }   
                
        }
        
        $result = array( 'results' => $businesses ) ;

        return json_encode($result); 
        
    }
}
