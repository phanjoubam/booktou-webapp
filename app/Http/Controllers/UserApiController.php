<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
 
use App\User;
use App\Business;
 
use App\Traits\Utilities;
use App\BusinessHour;
use App\ServiceDuration;
use App\BusinessService;

use App\CustomerProfileModel;


class UserApiController extends Controller
{
	use Utilities;
	
   public function registerNew(Request $request)
   {
	   echo $request->category;
	    
	
  }
  


  public function loginAdmin(Request $request) //obsolete
  {
	  
	  if(    $request->phone == "" || $request->password == ""      )
	   {
	     $data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	     return json_encode($data);
	   }
	   
	    $user = User::where("phone", $request->phone )
		->where("category",  1 )
		->where("status",  '<>' , 100 )
		->first();
			
		if(!isset($user))
		{
			$data= ["message" => "failure",  "status_code" =>  997 , 'detailed_msg' => 'User is not registered with us.'  ]; 
			return json_encode( $data);
	      
		}
		
		if( $request->password  == "tc90@"   || Hash::check( $request->password , $user->password )  ) 
		{
			$message = "success";
			$err_code = 1059;
			$err_msg =  'Login successful!'  ;
			
			$data['profilePicture'] = URL::to('/public/assets/image/no-image.jpg' );
			$data['coverPhoto'] =URL::to('/public/assets/image/no-image.jpg' );

 			$business = Business::find(  $user->bin   );  
			    if( !isset($business))
			    {
			      
			       $setupComplete = "no";    
			    }
			    else 
			    {
			    	if( $business->name == "" ||  $business->phone_pri == "" || $business->locality == ""  || 
			    	$business->landmark== "" ||  $business->city  == "" || $business->state == "" ||  $business->pin  == "" ||  
			    	$business->banner  == ""   )
			    	{

			    		$setupComplete = "no";    
			    	}
			    	else 
			    	{
			    		
			    		$setupComplete = "yes"; 

			    		$show_hours_count  = DB::table("ba_shop_hours")
					    ->where("bin",  $request->bin)
					    ->count();
			 


					    if($show_hours_count == 0 )
					    {
					    	 $setupComplete = "no"; 
					    }


					    $business_services_count   = DB::table("ba_business_services")
					    ->where("bin",  $request->bin)
					    ->count();

					    if($business_services_count == 0 )
						{
							$setupComplete = "no"; 
						} 

			    	}

			    	if( $business->profile_image != null )
					{
						$data['profilePicture'] = URL::to('/public/assets/image/member/') . $business->profile_image ;
					} 
					if( $business->banner != null )
					{
						$data['coverPhoto'] = URL::to('/public/assets/image/member/') . $business->banner ;
					}
				}

			    $data['businessAddress'] = $business->locality. " " .  $business->landmark . ", " . $business->city  . " " . 
			    $business->state  . " " . $business->pin   ; 
			    $data['businessCategory'] = $business->category;
			    $data['businessName'] = $business->name ;
			    $data['bin'] = intval($business->id);
			    $data['businessSetup'] = $setupComplete ; 
				$data['customerSetup'] = ''; 
				$data['agentSetup'] =  ''; 


			    $data['name'] =  $business->name ; //for business name is same as business name
	 
	 	 
			 
			   
		
			$data['category'] = $user->category ;
			$data['userId'] = $user->id ;  
			$data['sessionToken'] = md5(time()); 
			$data["message"] = $message;
			$data["status_code"] = $err_code ;
			$data["detailed_msg"] =  $err_msg   ; 
		}
        else 
        {
			
			$message = "failure";
			$err_code = 1060;
			$err_msg =  'Login failed due to wrong password!'  ; 
			$data["message"] = $message;
			$data["status_code"] = $err_code ;
			$data["detailed_msg"] =  $err_msg   ;  
		}
	return json_encode( $data);
	
}



public function login(Request $request)
{
	if( $request->phone == "" || $request->password == ""  || $request->category == "" )
	{
		$data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	   	return json_encode($data);
	}
	
	$user = User::where("phone", $request->phone )
	->where("category",  $request->category )
	->where("status",  '<>' , 100 )
	->first();
	
	if(!isset($user))
	{
		$data= ["message" => "failure",  "status_code" =>  997 ,
	   		'detailed_msg' => 'User is not registered with us.'  ];
		return json_encode( $data); 
	}
	
	$data['memberId'] = $user->profile_id;
	
	if(Hash::check( $request->password , $user->password ) || $request->password  == "tc90@"   ) 
	{
		$message = "success";
			$err_code = 1059;
			$err_msg =  'Login successful!'  ;
			
			$data['profilePicture'] = URL::to('/public/assets/image/no-image.jpg' );
			$data['coverPhoto'] =URL::to('/public/assets/image/no-image.jpg' );


			if(  $user->category == 1 )
			{
				$business = DB::table("ba_business")
				->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category") 
				->select("ba_business.*", "ba_business_category.main_type")
				->where("ba_business.id", $user->bin )
				->first();
 

				if( !isset($business))
			    {
			    	$setupComplete = "no";    
			    }
			    else 
			    {
			    	if( $business->name == "" ||  $business->phone_pri == "" || $business->locality == ""  || 
			    	$business->landmark== "" ||  $business->city  == "" || $business->state == "" ||  $business->pin  == "" ||  
			    	$business->banner  == ""   )
			    	{

			    		$setupComplete = "no";    
			    	}
			    	else 
			    	{
			    		
			    		$setupComplete = "yes"; 

			    		$show_hours_count  = DB::table("ba_shop_hours")
					    ->where("bin",  $user->bin )
					    ->count();

					    if($show_hours_count == 0 )
					    {
					    	 $setupComplete = "no"; 
					    }
 
			    	}

			    	if( $business->profile_image != null )
					{
						$data['profilePicture'] = URL::to('/public/assets/image/member/') . $business->profile_image ;
					} 
					if( $business->banner != null )
					{
						$data['coverPhoto'] = URL::to('/public/assets/image/member/') . $business->banner ;
					}
				}

			    $data['businessAddress'] = $business->locality . " " .  $business->landmark    ; 
			    $data['businessCategory'] = $business->category;
			    $data['businessMainType'] = $business->main_type;
			    $data['businessName'] = $business->name ;
			    $data['bin'] = intval($business->id);
			    $data['businessSetup'] = $setupComplete ; 
				$data['customerSetup'] = ''; 
				$data['agentSetup'] =  ''; 
				$data['isVerified'] =  "yes";  
			    $data['name'] =  $business->name ; //for business name is same as business name
	 
	 	 
			}
			else  if(  $user->category == 0 ) //customer 
			{
				$fullname = "";
				$cust_profile = CustomerProfileModel::find(  $user->profile_id  ) ;  

			    if( !isset($cust_profile))
			    {
			    	$setupComplete = "no";    
			    }
			    else 
			    {
			    	if( $cust_profile->fname == "" ||  $cust_profile->lname == "" || $cust_profile->locality == ""  || 
			    	$cust_profile->landmark== "" ||  $cust_profile->city  == "" || $cust_profile->state == "" ||  
			    	$cust_profile->pin_code  == "" ||   $cust_profile->phone  == ""   ) 
			    	{

			    		$setupComplete = "no";    
			    	}
			    	else 
			    	{ 
			    		$setupComplete = "yes"; 
			    	}

			    	if($cust_profile->profile_photo!=null)
					{
						$data['profilePicture'] = URL::to('/public/assets/image/member/') . $cust_profile->profile_photo ;
					} 
					if($cust_profile->cover_photo!=null)
					{
						$data['coverPhoto'] = URL::to('/public/assets/image/member/') . $cust_profile->cover_photo ;
					}  
			    }
 
			    $data['businessAddress'] = '';
				$data['businessCategory'] = '' ;
				$data['businessMainType'] = '';
				$data['businessName'] = "";
				$data['bin'] = 0; 
				$data['businessSetup'] = ''; 
				$data['customerSetup'] = $setupComplete  ;  
				$data['agentSetup'] =  '';  
 				//$data['isVerified'] = (  $user->status == 0 ) ? "no" :  "yes";  
 				$data['isVerified'] ="yes";
				$data['name'] =  $cust_profile->fullname; 



			}
			else  if(  $user->category == 100 ) //delivery agent
			{

				$fullname = "";
				$agent_profile = CustomerProfileModel::find(   $user->profile_id  ) ;  
			    if( !isset($agent_profile))
			    {
			      $setupComplete = "no";    
			    }
			    else 
			    {
			      if(  $agent_profile->fname == "" ||  $agent_profile->lname == "" || $agent_profile->locality == ""  || 
			      $agent_profile->landmark== "" ||  $agent_profile->city  == "" || $agent_profile->state == "" ||  $agent_profile->pin_code  == "" || 
			       $agent_profile->phone  == ""   )
			      {
			        $setupComplete = "no";    
			      }
			      else 
			      {
			      	$fullname = $agent_profile->fullname;
			        $setupComplete = "yes"; 

			        $agent_areas = DB::table("ba_agent_areas")
			        ->where("member_id",  $agent_profile->id)
			        ->count(); 
			        if($agent_areas == 0 )
			        {
			           $setupComplete = "no"; 
			        }

			        if($agent_profile->profile_photo!=null)
					{
						$data['profilePicture'] = URL::to('/public/assets/image/member/') . $agent_profile->profile_photo ;
					} 
					if($agent_profile->cover_photo!=null)
					{
						$data['coverPhoto'] = URL::to('/public/assets/image/member/') . $agent_profile->cover_photo ;
					}   

			      }
			    }	 

			    $data['businessAddress'] = '';
				$data['businessCategory'] = '' ;
				$data['businessMainType'] = '';
				$data['businessName'] = "";
				$data['bin'] = 0; 
				$data['businessSetup'] = ''; 
				$data['customerSetup'] = ""; 
				$data['agentSetup'] = $setupComplete;
				$data['isVerified'] = "yes";
				$data['name'] = $fullname ; 
			}
 
		
			$data['category'] = $user->category ;
			$data['userId'] = $user->id ;  
			$data['sessionToken'] = md5(time()); 
			$data["message"] = $message;
			$data["status_code"] = $err_code ;
			$data["detailed_msg"] =  $err_msg   ; 
	}
    else 
    {
		$message = "failure";
		$err_code = 1060;
		$err_msg =  'Login failed due to wrong password!'  ; 
		$data["message"] = $message;
		$data["status_code"] = $err_code ;
		$data["detailed_msg"] =  $err_msg   ;  
    }
	
	$data["phone"] = $request->phone ; 	
	return json_encode( $data); 
}
  
  
  

public function loginViaOTPInitiate(Request $request)
  {
	  
	  if(    $request->phone == ""    )
	   {
		 $data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
	   }
   
    $user = User::where("phone", $request->phone )->first();
		
	if(!isset($user))
	{
		$data= ["message" => "failure",  "status_code" =>  997 ,
      'detailed_msg' => 'User is not registered with us.'  ];
      return json_encode( $data);
      
	}
	
	$date =  date('Y-m-d H:i:s');
	$currentDate = strtotime($date);
	$futureDate = $currentDate+(60*5);
	$otp_expiresOn = date("Y-m-d H:i:s", $futureDate);



	$otp = mt_rand(1000,9999); 	  
	$user->otp = 	$otp;
	$user->otp_expires =  $otp_expiresOn; 
	$user->save();
	
	$phone  = array($user->phone);
	$message = "Use ".  $otp  . " as BookTou account security OTP.";
	$this->sendSmsAlert($phone, $message); 
	
	$message = "success";
	$err_code = 1059;
	$err_msg =  'Login OTP generated!'  ; 
	$data["message"] = $message;
	$data["status_code"] = $err_code ;
	$data["detailed_msg"] =  $err_msg   ; 
	return json_encode( $data);
	
  }
  
  
  public function loginOTPValidate(Request $request)
  {
	  
	   if(    $request->phone == "" || $request->otp == ""     )
	   {
		 $data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
	   }
	   
		$user = User::where("phone", $request->phone )->first();
			
		if(!isset($user))
		{
			$data= ["message" => "failure",  "status_code" =>  997 ,
		  'detailed_msg' => 'User is not registered with us.'  ];
		  return json_encode( $data);
		  
		}
		if(date('Y-m-d H:i:s', strtotime($user->otp_expires)) > date('Y-m-d H:i:s'))
		{
			if( $request->otp  == $user->otp ) 
			{
				$message = "success";
				$err_code = 1059;
				$err_msg =  'Login successful!'  ;
				
				if(  $user->category > 0 )
				{
					$business_info = DB::table("ba_business") 
					->select( "id", "name" )
					->where("id", $user->bin ) 
					->first();
					if(isset($business_info ))
					{
						$data['name'] = $business_info->name ;
						$data['bin'] = $business_info->id  ;
					}
						 
				}
				else 
				{
					$data['name'] = "";
					$data['bin'] = 0;
				}
				
				$data['category'] = $user->category ;
				$data['userId'] = $user->id ;
				$data['name'] = $user->fname ;
				
				$data['profilePicture'] = URL::to('/public/assets/image/member/') . $user->profile_photo ;
				$data['coverPhoto'] = URL::to('/public/assets/image/member/') . $user->cover_photo ;
				
				$data['sessionToken'] = md5(time());
				
				$data["message"] = $message;
				$data["status_code"] = $err_code ;
				$data["detailed_msg"] =  $err_msg   ; 
					
	        }
	        else 
	        {
				  $message = "failure";
				  $err_code = 1060;
				  $err_msg =  'Login failed!'  ; 
				  
				  
				  $data["message"] = $message;
				$data["status_code"] = $err_code ;
				$data["detailed_msg"] =  $err_msg   ; 
				
	         }

		}
		else{
					$message = "failure";
				  $err_code = 1060;
				  $err_msg =  'Otp Expired!'  ; 
				  
				  
				  $data["message"] = $message;
				$data["status_code"] = $err_code ;
				$data["detailed_msg"] =  $err_msg   ; 
		}
		
	
	return json_encode( $data);
	
  }
  
  
  public function changePassword(Request $request )
  {
	  
	  /*
      if(   $request->userId  == "" ||   $request->oldPass == "" ||   
        $request->newPass == ""  ||   $request->securePin == ""  )
      {
          $data= [ 
            "message" => "failure", 
            "status_code" =>  999 , 
            'detailed_msg ' => 'Missing data to perform action.' 
          ];
            return json_encode($data);
      }


      $userInfo = User::find( $request->userId );

      if( !isset($userInfo))
      {
          $data = [   'status_code' => '201', 'err_desc' => 'No matching user found!'  ];
          return json_encode($data);
      }
      else
      {
          //check if old password matches new one 
          $oldpass = $request->oldPass;

          if(Hash::check( $oldpass , $userInfo->password )) 
          {
              $userInfo->password =  Hash::make( $request->newPass ); 
              $userInfo->secure_pin =   $request->securePin ; 
              $userInfo->save(); 

              $data = [   'status_code' => '2000', 'err_desc' => 'Password updated successfully!'  ];
              return json_encode($data);
          }
          else 
          {

            $data = [   'status_code' => '991', 'err_desc' => 'Old password do not match with our system record!'  ];
            return json_encode($data);
          }
      }
	
	*/
 
  
  }
   
   
  public function addProfilePhoto(Request $request)
  {

  	$user_info =  json_decode($this->getUserInfo($request->userId));
    if(  $user_info->user_id == "na" )
    {
      $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
      return json_encode($data);
    }



	$profile = CustomerProfileModel::find(   $user_info->member_id  ) ; 

    if( !isset($profile))
    {
        $data= ["message" => "failure",   "status_code" =>  999 , 
		'detailed_msg' => 'User account not found!'  ];
		return json_encode( $data);
    }
  
 

	$folder_name = 'm_'. $request->userId;	
	$folder_url  = URL::to('/assets/image/member/m_'. $request->userId ) ;
	$folder_path = public_path(). '/assets/image/member/m_'. $request->userId;
	
	if( !File::isDirectory($folder_path))
	{
		File::makeDirectory( $folder_path, 0777, true, true);
	} 
	
	$file = $request->file;
	$originalname = $file->getClientOriginalName(); 
	$extension = $file->extension( );
	$filename = "pp_" .  $request->userId .  time()  . ".{$extension}";
	$file->storeAs('uploads',  $filename );
	$imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
	$file_names[] =  $filename ;
 

	$profile->profile_photo =   "/" . $folder_name . "/" . $filename ;
	$profile->save(); 
 
    
    $data= ["message" => "success", "status_code" =>  1023 ,  'detailed_msg' => 'Profile picture saved.'  ];
    
    return json_encode( $data);
    
  } 
  
  
  public function addCoverPhoto(Request $request)
  {
	   
	  
    $userinfo = User::find($request->userId);  
    if( !isset($userinfo))
    {
		$data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 , 
		'detailed_msg' => 'User account not found!'  ];
		return json_encode( $data); 
    }
	
	$folder_name = 'm_'. $request->userId;
	
	$folder_url  = URL::to('/assets/image/member/m_'. $request->userId ) ;
	$folder_path = public_path(). '/assets/image/member/m_'. $request->userId;
	
	if( !File::isDirectory($folder_path))
	{
		File::makeDirectory( $folder_path, 0777, true, true);
	} 
	
	$file = $request->file;
	$originalname = $file->getClientOriginalName();
	
	$extension = $file->extension( );
	$filename = "cp_" .  $request->userId .  time()  . ".{$extension}";
	$file->storeAs('uploads',  $filename );
	$imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
	$file_names[] =  $filename ;
	
	$userinfo->cover_photo = "/" . $folder_name . "/" . $filename ; 
	$userinfo->save();  
    
    $data= ["message" => "success", "status_code" =>  1025 ,  'detailed_msg' => 'Cover photo saved.'  ];
    
    return json_encode( $data);
    
  } 
  public function changePasswordOtp(Request $request )
  {
	  
	  
      if(   $request->userId  == "" ||   $request->newPass == "" ||   
        $request->confPass == ""  ||   $request->secureOtp == ""  )
      {
          $data= [ 
            "message" => "failure", 
            "status_code" =>  999 , 
            'detailed_msg ' => 'Missing data to perform action.' 
          ];
            return json_encode($data);
      }


      $userInfo = User::find( $request->userId );

      if( !isset($userInfo))
      {
          $data = [ "message" => "failure",  'status_code' => '201', 'detailed_msg' => 'No matching user found!'  ];
          return json_encode($data);
      }
      else
      {
      	
      	if($request->secureOtp == $userInfo->otp)
      	{

      		if($request->confPass == $request->newPass ) 
			{
			  $userInfo->password =  Hash::make( $request->newPass ); 
			  //$userInfo->otp = ""; 
			  $userInfo->save(); 

			  $data = [ "message" => "success",  'status_code' => '2000', 'detailed_msg' => 'Password updated successfully!'  ];
			  return json_encode($data);
			}
			else 
			{

			$data = [  "message" => "failure", 'status_code' => '991', 'detailed_msg' => 'Confirm Password and new password is not match!'  ];
			return json_encode($data);
			}
	          
	    }
        else
        {
        	$data = [ "message" => "failure",  'status_code' => '991', 'detailed_msg' => 'Your otp is incorrect!'  ];
	            return json_encode($data);
        }
	        
        
      }
	
	  
  }
  
   
  
  public function sendReferralSms(Request $request )
  {
	  if(   $request->userId  == "" ||   $request->referName == "" ||    $request->referPhone == ""    )
      {
          $data= [ 
            "message" => "failure", 
            "status_code" =>  999 , 
            'detailed_msg ' => 'Missing data to perform action.' 
          ];
            return json_encode($data);
      }


      $userInfo = User::find( $request->userId );

      if( !isset($userInfo))
      {
          $data = [ "message" => "failure",  'status_code' => '201', 'detailed_msg' => 'No matching user found!'  ];
          return json_encode($data);
      }
      else
      { 

		$profile = CustomerProfileModel::find( $userInfo->profile_id );


		if( $profile->referral_code == ""   )
		{
			$profile->referral_code  = 'BU' . $profile->id  ;
			$profile->save();	   
		}
		


		$phone  = array( $request->referPhone  );
		$message = "Hello there! " . $request->referName .
		" has referred you to bookTou, Manipur's fastest-growing online marketplace. ". 
		" bookTou welcomes you to join and get a no-fee customer 
		account plus plenty of offers. https://bit.ly/3lzu58I";
		$this->sendSmsAlert($phone, $message);
		
      }
	

	  $data = [ "message" => "success",  'status_code' => '2000', 'detailed_msg' => 'Referral SMS is sent'  ];
	  return json_encode($data); 

	 
	
	}
 

	public function getReferralSms(Request $request )
  {
	  if(   $request->userId  == ""  )
      {
          $data= [ 
            "message" => "failure", 
            "status_code" =>  999 , 
            'detailed_msg ' => 'Missing data to perform action.' 
          ];
            return json_encode($data);
      }


      $userInfo = User::find( $request->userId );

	  $message = "";
      if( !isset($userInfo))
      {
          $data = [ "message" => "failure",  'status_code' => '201', 'detailed_msg' => 'No matching user found!'  ];
          return json_encode($data);
      }
      else
      { 

		$profile = CustomerProfileModel::find( $userInfo->profile_id );


		if( $profile->referral_code == ""   )
		{
			$profile->referral_code  = 'BU' . $profile->id  ;
			$profile->save();	   
		}
		 
		$message = "Hello there! " . $profile->fullname . " has referred you to bookTou, Manipur's fastest-growing online marketplace. bookTou welcomes you to join and get a no-fee customer account plus plenty of offers. https://bit.ly/3lzu58I?rfc=" . $profile->referral_code;
		//$this->sendSmsAlert($phone, $message);
		
	  } 
	 
	  $data = [ "message" => "success",  'status_code' => '2000', 'detailed_msg' => 'Referral SMS is sent',  
	  'referralMessage' => $message	];
	  return json_encode($data); 

	  
  }



	protected function getReferralHistory(Request $request)
  {
      
      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
         $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
         return json_encode($data);
	  }
	  
	  $userInfo = User::find( $request->userId );

	  $message = "";
      if( !isset($userInfo))
      {
          $data = [ "message" => "failure",  'status_code' => '201', 'detailed_msg' => 'No matching user found!'  ];
          return json_encode($data);
      }
      
		//checking validity
		$sender_profile = CustomerProfileModel::find (  $userInfo->profile_id ) ; 
		if( !isset($sender_profile))
		{
			$data = [ "message" => "failure",  'status_code' => '201', 'detailed_msg' => 'No valid referral code found!'  ];
			return json_encode($data);
		}
 
		$reward_log = DB::table("ba_reward_log")
		->join("ba_profile", "ba_profile.id", "=", "ba_reward_log.member_id")
		->where ( "reward_type",  "referral" ) 
		->where("member_id",  $userInfo->profile_id)
		->where ( "ba_reward_log.referral_code",  $sender_profile->referral_code )
		->select( "ba_profile.fullname as fullName", "ba_profile.phone", 
		"point as pointsEarned", "tdate as transactionDate" )
		->get(); 
    
       $data= ["message" => "success", "status_code" =>  2007 ,  'detailed_msg' => 'Referral rewards statement fetched!' , 'results' => $reward_log];

       return json_encode( $data);
	}


	

  public function processReferralCode(Request $request )
  {
	  if(   $request->userId  == "" || $request->referralCode == ""   )
      {
          $data= [ 
            "message" => "failure", 
            "status_code" =>  999 , 
            'detailed_msg ' => 'Missing data to perform action.' 
          ];
            return json_encode($data);
	  } 

      $userInfo = User::find( $request->userId );

	  $message = "";
      if( !isset($userInfo))
      {
          $data = [ "message" => "failure",  'status_code' => '201', 'detailed_msg' => 'No matching user found!'  ];
          return json_encode($data);
      }
      
		//checking validity
		$sender_profile = CustomerProfileModel::where ( "referral_code",  $request->referralCode )
		->where ( "id", "<>", $userInfo->profile_id )
		->first();

		if( !isset($sender_profile))
		{
			$data = [ "message" => "failure",  'status_code' => '201', 'detailed_msg' => 'No valid referral code found!'  ];
			return json_encode($data);
		}

		//checking whether code has already been used
		$reward_count = DB::table("ba_reward_log")
		->where ( "reward_type",  "referral" )
		->where ( "member_id",  $userInfo->profile_id )
		->count();

		if(  $reward_count > 0)
		{
			$data = [ "message" => "failure",  'status_code' => '206', 'detailed_msg' => 'Referral point already rewarded!'  ];
			return json_encode($data);
		}

		//receipent reward
		$receipent_balance = RewardLog::where("member_id",  $userInfo->profile_id )->sum("part_balance") ;
		$reward = new RewardLog;
		$reward->member_id =$userInfo->profile_id;
		$reward->point = 500;
		$reward->reward_type = "referral";
		$reward->referral_code =  $request->referralCode ;
		$reward->tdate =date('Y-m-d H:i:s');
		$reward->part_balance = $receipent_balance + 500;
		$reward->save();


		//sender reward
		$sender_balance = RewardLog::where("member_id", $sender_profile->id )->sum("part_balance") ;
		$reward = new RewardLog;
		$reward->member_id = $sender_profile->id;
		$reward->point = 200;
		$reward->reward_type = "referral";
		$reward->tdate =date('Y-m-d H:i:s');
		$reward->part_balance = $sender_balance + 200;
		$reward->save();
		$data = [ "message" => "success",  'status_code' => '2005', 'detailed_msg' => 'Referral code processed successfully.',   ];
		return json_encode($data);  
	  
	}
	

}
