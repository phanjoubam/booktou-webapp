<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request; 

use App\CloudMessageModel;
use App\User;

use App\Libraries\FirebaseCloudMessage;

use Config;  

class SystemsController  extends Controller
{

	public function sendIndividualPushMessage(Request $request) 
	{
 
		if( $request->cmid == "" || $request->mid == "")
		{
			$data= ["message" => "failure", "status_code" =>  999 ,  'detailed_msg' => 'Invalid message ID'  ] ;
            return json_encode($data);
		}

		$msg = CloudMessageModel::find( $request->cmid) ; 

		if( !isset($msg) )
		{
			$data= ["message" => "failure", "status_code" =>  902 ,  'detailed_msg' => 'No message found.'  ] ;
            return json_encode($data);
		}

		 $response = array();
		 $response['title']     = $msg->title;
		 $response['message']   = $msg->body; 
		 $response['image'] = $msg->image_url ; 

		 if($msg->bin != null || $msg->bin != "")
		 {
		 	$response['bin']   = $msg->bin;
		 	$response['bizName']   = $msg->business_name;  
		 }
 
 
		 $receipent = User::where("profile_id",  $request->mid)->first()  ; 
		 if( !isset($receipent ) )
		{
			$data= ["message" => "failure", "status_code" =>  902 ,  'detailed_msg' => 'No receipent found.'  ] ;
            return json_encode($data);
		}

  
 	
 		 $firebase = new FirebaseCloudMessage();
 		 $regId    = $receipent->firebase_token;
 		 $response = $firebase->sendToIndividual( $regId, $response );

 		 if ( $response === false ) 
	     {
	     	$msg =  'Cloud message could not be send.'  ;
	     	$status = "failure";
	     	$code = 7076;
	     }
	     else 
	     {
	     	$status = "success";
	     	$code = 7077;
	     	$msg ="Cloud message send.";
	     }

	     $data= ["message" => $status , "status_code" =>  $code ,  'detailed_msg' => $msg  ] ;
	     return json_encode($data);


	}
 
	 

}
