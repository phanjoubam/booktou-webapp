<?php

namespace App\Http\Middleware;

use Closure;

class LogBookingMainModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->put('module_active', 'booking');
        return $next($request);
    }
}
