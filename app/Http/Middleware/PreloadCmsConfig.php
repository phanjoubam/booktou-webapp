<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\Traits\Utilities;
use App\ShoppingCartTemporary;


use Closure;

class PreloadCmsConfig
{
    use Utilities;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 

        $cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path');  
        $cms_base_url = config('app.app_cdn')  ;  
        $cms_base_path = $cdn_path  ;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";


        //saving the currently active module based on URL 
        $url_segments = request()->segments();
        
        if( count($url_segments ) > 0)
        {
            $main_module = $url_segments[0];
        }
        else
        {
            $main_module = "shopping";
        }
        $request->session()->put('_active_module_', $main_module  );


        $web_main_menu  = array();
        if( strcasecmp($main_module, "booking") == 0  || strcasecmp($main_module, "appointment")  ==  0  )
        {
            $web_main_menu  = DB::table("cms_main_menu") 
            ->where("main_module",   $main_module)
            ->get(); 
        }



        if( !$request->session()->has('shopping_session' ))
        {
            $uuid =  uniqid(); 
            $request->session()->put('shopping_session', $uuid  ); 
            $session_id =  $uuid;  
        }  
        else 
        {
            $session_id = $request->session()->get('shopping_session' );
        }

        //cart item count 
        $cart_count = ShoppingCartTemporary::where( "session_id",   $session_id ) 
        ->count();
        $request->session()->put('cart_count',  $cart_count   );  

        if( !$request->session()->has('cms_main_menu' ))
        {
            $cms_main_menu  = DB::table("cms_main_menu")->get(); 
            $request->session()->put('cms_main_menu', $cms_main_menu   );    

        } 



        if( !$request->session()->has('cms_sub_menu' ))
        {
           
            //loading sub-menu
            $cms_sub_menu  = DB::table("cms_sub_menu")->get();  
            $request->session()->put('cms_sub_menu', $cms_sub_menu   );  

        }  

        if( !$request->session()->has('cms_side_menu' ))
        {
            //loading sub-menu
            $cms_side_menu  = DB::table("cms_side_menu")->get();  
            $request->session()->put('cms_side_menu', $cms_side_menu   );  

        } 


        if( !$request->session()->has('ba_business_category' ))
        {
            //loading sub-menu
            $business_category  = DB::table("ba_business_category")->get();  
            $request->session()->put('business_category', $business_category   );   
            $product_category  = DB::table("ba_product_category")->get();  
            $request->session()->put('product_category', $product_category   );   
        } 
     





        if( $request->session()->has('__user_id_' ))
        {
            //cart item count
            $uid = $request->session()->get('__user_id_' ); 
            $user_info =  json_decode ( $this->getUserInfo( $uid  ) );  
            
              
              if(  $user_info->user_id == "na" ||  $user_info->category  != 0  )
              {
                $cart_count = 0;
              }
              else 
              {
                $cart_count = ShoppingCartTemporary::where( "member_id",  $user_info->member_id )
                ->count();
              }
              $request->session()->put('cart_count',  $cart_count   ); 


            $cart  = DB::table("ba_temp_cart")   
            ->join("ba_products", "ba_products.id", "=", "ba_temp_cart.prid") 
            ->where("member_id",  $user_info->member_id   ) 
            ->select( "ba_products.*", "ba_temp_cart.session_id", 
                    "ba_temp_cart.member_id" , "ba_temp_cart.bin" , 
                    "ba_temp_cart.prid", "ba_temp_cart.subid" , "ba_temp_cart.qty as pqty" )
            ->get();
        }
        else 
        {

            $cart  = DB::table("ba_temp_cart")   
            ->join("ba_products", "ba_products.id", "=", "ba_temp_cart.prid") 
            ->where("session_id",  $session_id   ) 
            ->select( "ba_products.*", "ba_temp_cart.session_id", 
                    "ba_temp_cart.member_id" , "ba_temp_cart.bin" , 
                    "ba_temp_cart.prid", "ba_temp_cart.subid" , "ba_temp_cart.qty as pqty" )
            ->get();
        }
 

        $bins = array(); 
        $bins[] = 0;
        foreach($cart as $item)
        {
            $bins[] = $item->bin;
        } 
          
        $bins= array_filter($bins); 
        $businesses  = DB::table("ba_business")
        ->whereIn("id", $bins )
        ->get();

    
        $request->session()->put('__cart_items', $cart   ); 
        $request->session()->put('__businesses', $businesses   );  


        //new code 
        $biz_categories  = DB::table("ba_business_category")
        ->where("is_active", "yes" )
        ->where("is_public", "yes" ) 
        ->orderBy("display_order")
        ->get(); 


        $main_menu  = DB::table("cms_main_menu") 
        ->select(
            "cms_main_menu.*",
            DB::Raw("'[]' submenu")
        )
        ->get();

        $biz_category_submenu = DB::table("cms_sub_menu")
        ->get();

        $submenu=array();
        
        $i=0;
        foreach ($main_menu as $items) {
                 $submenu[$i]['id'] = $items ;
                 $menu = array();
                 $items->submenu = array() ;
                foreach ($biz_category_submenu as $sub_items) {

                        if ($items->id==$sub_items->menu_id) {

                            $menu[] = $sub_items;
                            $items->submenu = $menu ;
                             
                         }
                }           
                $i++;
        } 


        $sub_menu  = DB::table("cms_sub_menu") 
        ->get(); 


        $menu_location  = DB::table("cms_menu_location") 
        ->get(); 

        $menus  = DB::table("cms_menus") 
        ->get(); 

        $menu_items  = DB::table("cms_menu_item") 
        ->where("main_module", "SHOPPING") 
        ->get(); 

        //loading menu items for business categories

        $menu_category = DB::table("ba_bookable_category")
        ->select(DB::Raw("distinct(main_module) as menuCategory"))
        ->where('main_module','SHOPPING')
        ->get(); 

        $main_modules = DB::table("ba_bookable_category")
        ->select(DB::raw('distinct(main_module)as main'))
        ->get();

        $categories = DB::table("ba_business_category")
        ->where("main_module", "Shopping")
      ->where("is_active", "yes" )
      ->paginate(15);

        $browse_booking = DB::table("ba_business_category")
        ->where("main_module", "Booking")
        ->where("is_public", "yes")
        ->get();

        $bookings = DB::table("ba_service_booking_details")
          ->select(DB::Raw("count(*) as productCount"),"service_product_id as productid") 
          ->groupBy("service_product_id")
          ->orderBy("productCount",'desc')
          ->paginate(8); 
          
      
          $serviceProduct = DB::table("ba_business_category")
          ->where("main_module","appointment")
          ->where("is_public","yes")
          ->get();

          $booking = DB::table("ba_water_booking_basket")
          ->select("category_name as category")
          ->groupBy('category');


          $shopping = DB::table("ba_shopping_basket")
          ->select(DB::Raw("count(*) as totalProduct"),"prsubid as serviceProductid") 
          ->groupBy("prsubid")
          ->orderBy("totalProduct",'desc')
          ->paginate(8); 

          $productid = $productsubid = array();
          $serviceprid = array();

          foreach($shopping as $items)
          {
            $productsubid[] = $items->serviceProductid;
          }

          $product_variant = DB::table("ba_product_variant")
          ->whereIn('prsubid',$productsubid)
          ->get();

          foreach($product_variant as $items)
          {
            $productid[] = $items->prid;
          }
           
          $products = DB::table("ba_products")
          ->whereIn('id',$productid) 
          ->select("category");

          $all_categories = DB::table("ba_service_products")
          ->whereIn("id",$serviceprid)
          ->union($booking)
          ->union($products)
          ->select("service_category as category")
          ->get(); 
 

          $bookingModuleCategory = DB::table("cms_menu_item")
                ->where("main_module", "BOOKING") 
                ->select('item_name as mainMenu','navigate_to as menulink')
                ->get(); 
                 
        $mainmenu_items = DB::table("cms_menu_item")
                ->whereIn("main_module", array("booking", "appointment"))
                ->select('item_name as mainMenu','navigate_to as menulink', "main_module")
                ->get();

        $request->merge(
            array(
                "BUSINESS_CATEGORIES" =>  $biz_categories, 
                'MAIN_MENU' => $main_menu,  
                'SUB_MENU' => $sub_menu,  
                'CART_COUNT' =>  $cart_count,
                'MENU_CATEGORY' =>  $menu_category,
                'MENU_LOCATION' => $menu_location , 
                'MENU' => $menus , 
                'MENU_ITEMS' => $menu_items , 
                'MAIN_MODULES' => $main_modules,
                'CATEGORIES' => $categories,
                'BROWSE_BOOKING' => $browse_booking,
                'PRODUCT'=>$serviceProduct,
                'ALL_CATEGORIES'=>$all_categories, 
                'BOOKING_MODULE_CATEGORY'=>$bookingModuleCategory,
                'ACTIVE_MAIN_MODULE' => $main_module  ,
                'WEB_MAIN_MENU' => $web_main_menu ,
                'WEB_MAIN_MENU_ITEMS'=> $mainmenu_items,

            )
        );

        return $next($request);
    }
}
