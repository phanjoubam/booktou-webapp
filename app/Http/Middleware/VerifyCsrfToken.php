<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'payment/response',
        'shopping/payment-completed',
        'appointment/payment-completed',
        'booking/payment-completed',
        'booking/order/payment-completed',
        'events/ticketing/payment-completed'
    ];
}
