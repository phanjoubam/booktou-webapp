<?php

namespace App\Http\Middleware;

use Closure;

class LogAppointmentMainModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->put('module_active', 'appointment');

        echo $request->session()->get('module_active');

        return $next($request);
    }
}
