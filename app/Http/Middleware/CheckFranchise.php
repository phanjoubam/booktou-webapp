<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Closure;

class CheckFranchise
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if( !$request->session()->has('__user_id_' ))
        {
            Auth::logout();
            $request->session()->flush();
            return redirect()->intended('login');
        }

        
        $uid = $request->session()->get('__user_id_' ); 
        
        $user = DB::table("ba_users")
        ->where("id",  $uid ) 
        ->first();

        if( !isset($user))
        {
            Auth::logout();
            $request->session()->flush();
            return redirect()->intended('login');
        } 

    
      
        if( $user->category != 100000  )
        {
            Auth::logout();
            $request->session()->flush();
            return redirect()->intended('login');
        }

        return $next($request);
    }
}
