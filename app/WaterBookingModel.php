<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaterBookingModel extends Model
{
	protected $table = 'ba_water_booking';
	public $timestamps = false;
}
