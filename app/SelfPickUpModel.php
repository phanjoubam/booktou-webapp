<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelfPickUpModel  extends Model
{
	protected $table = 'ba_business_selfpickup';
	public $timestamps = false; 
	
}
