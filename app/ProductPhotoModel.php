<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPhotoModel extends Model
{  
	protected $table = 'ba_product_photos';
    public $timestamps = false;  

}
