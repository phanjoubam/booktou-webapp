<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationBeneficiaryModel extends Model
{
    protected $table = 'ba_donation_beneficiery';
    public $timestamps = false;
}
