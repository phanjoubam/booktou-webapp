<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationDonorModel extends Model
{
    protected $table = 'ba_donation_donor';
    public $timestamps = false;
}
