<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingSms extends Model
{
    protected $table = 'ba_marketing_sms';
}
