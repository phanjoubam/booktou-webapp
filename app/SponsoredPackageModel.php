<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SponsoredPackageModel extends Model
{
    protected $table = 'ba_sponsored_package';
    
    public $timestamps = false;
}
