<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallbackEnquiryDetailsModel  extends Model
{
	protected $table = 'ba_callbak_enquiry_details';
	public $timestamps = false;
}
