<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackMerchant extends Model
{
	protected $table = 'ba_feedback_merchant';
	public $timestamps = false;
}
