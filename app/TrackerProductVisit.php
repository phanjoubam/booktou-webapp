<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackerProductVisit extends Model
{
    protected $table = 'ba_tracker_product_visit';
	public $timestamps = false;

}
