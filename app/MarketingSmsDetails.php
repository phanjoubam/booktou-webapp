<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingSmsDetails extends Model
{
     protected $table = 'ba_marketing_sms_details';
}
