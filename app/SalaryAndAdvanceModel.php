<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryAndAdvanceModel  extends Model
{
	protected $table = 'ba_salary_and_advance';
	public $timestamps = false;
}
