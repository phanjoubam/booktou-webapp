<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductWishListModel extends Model
{
    protected $table = 'ba_wish_list';
	public $timestamps = false;

}
