<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDeliveryTerms  extends Model
{
    protected $table = 'ba_product_delivery_terms';
    public $timestamps = false;
}
