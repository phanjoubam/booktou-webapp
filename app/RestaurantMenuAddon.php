<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantMenuAddon extends Model
{
    protected $table = 'ba_restaurant_menu_addon';
    public $timestamps = false;
}
