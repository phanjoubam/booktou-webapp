<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageMetaModel extends Model
{
	protected $table = 'ba_package_meta_data';
	
    public $timestamps = false;
}