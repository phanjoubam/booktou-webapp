<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackerMerchantVisit extends Model
{
    protected $table = 'ba_tracker_merchant';
	public $timestamps = false;

}
