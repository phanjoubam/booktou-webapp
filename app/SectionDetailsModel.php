<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionDetailsModel extends Model
{
	protected $table = 'cms_section_details';
	public $timestamps = false;
}
