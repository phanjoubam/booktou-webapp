<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTicketPurchase   extends Model
{
    protected  $table='event_ticket_purchase';
    public $timestamps = false;

}
