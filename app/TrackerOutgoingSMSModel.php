<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackerOutgoingSMSModel extends Model
{
    protected $table = 'ba_tracker_outgoing_sms';
	public $timestamps = false;

}
