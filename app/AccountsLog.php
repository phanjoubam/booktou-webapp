<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountsLog  extends Model
{
	protected $table = 'ba_accounts_log';
    public $timestamps = false;
}
