<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;

use Illuminate\Console\Command;
use App\Traits\Utilities; 


class PendingOrderProcessingNotification extends Command
{
    use Utilities;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a notification every 5 minute untill an order is processed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        //get list of order place acknoledge by merchants
        $unprocessed_orders = DB::table("ba_service_booking")
        ->whereDate("service_date",  date('Y-m-d') )
        ->where("has_business_accepted",  "no" )
        ->whereIn("book_status", array('new','confirmed'))
        ->get();


        //send cloud message to merchant to confirm order



        //section to auto assign ordres
        //get list of order place acknoledged by merchants
        $merchant_accepted_orders = DB::table("ba_service_booking")
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
        ->where("ba_order_sequencer.type", "normal")
        ->whereDate("ba_service_booking.service_date",  date('Y-m-d') )
        ->whereIn("ba_service_booking.book_status", array('new','confirmed'))
        ->where("ba_service_booking.has_business_accepted",  "yes" )
        ->get();

        //retrieved active agents
        $active_agents = DB::table("ba_staff_attendance")
        ->whereIn("staff_id", function($query){
            $query->select("profile_id")
            ->from("ba_users")
            ->where("category", 100)
            ->where("status", '<>', 100);
        })
        ->whereDate("log_date", date("Y-m-d") )
        ->where("staff_status", "present")
        ->get();


        $agent_ids = array(0);
        foreach($active_agents as $active_agent)
        {
            $agent_ids[] = $active_agent->staff_id;
        }

        //change only to ready-for-task in final code
        //add GPS comparison between agent and order delivery
        $agents_ready = DB::table("ba_agent_locations")
        ->where("agent_status",  array('ready-for-task','engaged' ) ) 
        ->whereDate("created_at", date("Y-m-d") )
        ->get();


        //assigning normal order to agent
        foreach($merchant_accepted_orders as $order)
        {
            if($order->book_status == "delivery_scheduled" )
            {
                continue;
            }

            foreach($agents_ready as $active_agent)
            {
                $this->autoAssignNormalOrderToAgent($order->id, $active_agent->staff_id);
            }

        }

    }
}
