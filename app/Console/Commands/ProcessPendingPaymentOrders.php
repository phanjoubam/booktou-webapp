<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;

use Illuminate\Console\Command;
use App\Traits\Utilities; 

use App\ServiceBooking;


class ProcessPendingPaymentOrders extends Command
{
    use Utilities;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:updatecfstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Cashfree gateway status for an online paid order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = date('Y-m-d');
        $gateway_mode =config('app.gateway_mode');

        $pending_orders = ServiceBooking::whereDate("book_date",  date('Y-m-d') )
        ->where("payment_status",  "pending" )
        ->whereIn("payment_type", array('online', 'Pay online (UPI)') )
        ->whereNotIn("book_status", array('canceled','cancelled'  ))
        ->get();

 

        foreach($pending_orders as $order_info)
        {

            $gateway_response = json_decode($this->checkPayGatewayStatus($order_info->id,  $gateway_mode ));

            if( isset($gateway_response) && 
                array_key_exists("orderStatus", $gateway_response ) && strcasecmp( $gateway_response->orderStatus, "PAID" ) == 0 && 
                array_key_exists("txStatus", $gateway_response ) &&  strcasecmp($gateway_response->txStatus, "SUCCESS" ) == 0 )
            {
                $order_info->payment_status = "paid";
                $order_info->save();
            }
        }

    }
}
