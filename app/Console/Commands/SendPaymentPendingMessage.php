<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\ServiceBooking;
use App\TrackerOutgoingSMSModel;
use App\Traits\Utilities;

class SendPaymentPendingMessage extends Command
{
    use Utilities;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendpaymentlink';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send message containing payment link for every payment pending order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //fetch sms templates
        $template_info = DB::table("ba_sms_templates")
        ->where("name",  "payment_link_sms" )
        ->first();

        if( !isset($template_info) )
        {  
            return;
        }

        $dlt_entity_id = $template_info->dlt_entity_id;
        $dlt_header_id =  $template_info->dlt_header_id;
        $dlt_template_id = $template_info->dlt_template_id;
        $sms_body = $template_info->message;

        $today = date('Y-m-d');

        $pending_orders = DB::table("ba_service_booking")
        ->whereDate("service_date",  $today  )
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
        ->where("payment_status",  "pending" )
        ->whereIn("payment_type", array('online') )
        ->whereNotIn("book_status", array( 'cancelled') )
        ->select("ba_service_booking.*", "ba_order_sequencer.type")
        ->get();
        $order_nos =  $pending_orders->pluck("id");
        
        //calculating SMS count
        $sms_logs = DB::table("ba_tracker_outgoing_sms")
        ->whereIn("order_no",  $order_nos) 
        ->get();


        $trackerids = array();

        if(count($sms_logs) > 0)
        {
            foreach($pending_orders as $order)
            {
                $phones =  array();
                foreach($sms_logs as $sms_log)
                {
                    if($sms_log->order_no == $order->id )
                    {
                        if($sms_log->sms_count < 3)
                        {
                            $trackerids[] = $sms_log->id; //this is required to update the outgoing sms counter
                            $phones[] = $order->customer_phone;

                            //preparing payment URL
                            if( strcasecmp($order->type, "normal") == 0 )
                            {
                              $payment_url = URL::to("/shopping/payment-in-progress?o=" . $order->id);
                              $sms_body = str_replace( "#param1", $order->id, $sms_body);
                              $sms_body = str_replace( "#param2",$payment_url, $sms_body);
                              $this->sendSmsFromTemplate(  $dlt_entity_id,  $dlt_header_id,  $dlt_template_id , $phones, $sms_body );
                            }
                            elseif( strcasecmp($order->type, "booking") == 0 ) 
                            {
                              $payment_url = URL::to("/booking/payment-in-progress?o=" . $order->id);
                              $sms_body = str_replace( "#param1", $order->id, $sms_body);
                              $sms_body = str_replace( "#param2",$payment_url, $sms_body);
                              $this->sendSmsFromTemplate(  $dlt_entity_id,  $dlt_header_id,  $dlt_template_id , $phones, $sms_body );
                            }
                            else
                            {
                              $payment_url = URL::to("/appointment/payment-in-progress?o=" . $order->id);
                              $sms_body = str_replace( "#param1", $order->id, $sms_body);
                              $sms_body = str_replace( "#param2",$payment_url, $sms_body);
                              $this->sendSmsFromTemplate(  $dlt_entity_id,  $dlt_header_id,  $dlt_template_id , $phones, $sms_body );
                            }
          
                        }
                    }
                }
            }

        }
        else
        {
            foreach($pending_orders as $order)
            {
                $phones = array();
                $phones[] = $order->customer_phone;

                //preparing payment URL
                 if( strcasecmp($order->type, "normal") == 0 )
                    {
                      $payment_url = URL::to("/shopping/payment-in-progress?o=" . $order->id);
                      $sms_body = str_replace( "#param1", $order->id, $sms_body);
                      $sms_body = str_replace( "#param2",$payment_url, $sms_body);
                      $this->sendSmsFromTemplate(  $dlt_entity_id,  $dlt_header_id,  $dlt_template_id , $phones, $sms_body );
                    }
                    elseif( strcasecmp($order->type, "booking") == 0 ) 
                    {
                      $payment_url = URL::to("/booking/payment-in-progress?o=" . $order->id);
                      $sms_body = str_replace( "#param1", $order->id, $sms_body);
                      $sms_body = str_replace( "#param2",$payment_url, $sms_body);
                      $this->sendSmsFromTemplate(  $dlt_entity_id,  $dlt_header_id,  $dlt_template_id , $phones, $sms_body );
                    }
                    else
                    {
                      $payment_url = URL::to("/appointment/payment-in-progress?o=" . $order->id);
                      $sms_body = str_replace( "#param1", $order->id, $sms_body);
                      $sms_body = str_replace( "#param2",$payment_url, $sms_body);
                      $this->sendSmsFromTemplate(  $dlt_entity_id,  $dlt_header_id,  $dlt_template_id , $phones, $sms_body );
                    }
                //send sms here
                $sms_body = str_replace( "#param1", $order->id, $sms_body);
                $sms_body = str_replace( "#param2",$payment_url, $sms_body);
                $this->sendSmsFromTemplate(  $dlt_entity_id,  $dlt_header_id,  $dlt_template_id , $phones, $sms_body );

                //create new tracker object and save 
                $sms_tracker = new TrackerOutgoingSMSModel;
                $sms_tracker->cust_id = $order->book_by;
                $sms_tracker->order_no = $order->id;
                $sms_tracker->sms_count = 1;
                $sms_tracker->save();
            } 
        } 

        //send the actual SMS
        if( count($trackerids) > 0)
        {
            //update outgoing SMS counter for the above order
            DB::table("ba_tracker_outgoing_sms")
            ->whereIn("id",  $trackerids) 
            ->increment("sms_count");
        }
    }
}
 