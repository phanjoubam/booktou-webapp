<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentDeliveryLogsModel extends Model
{
     protected $table ="ba_agent_delivery_logs";
    public $timestamps = false;
}
