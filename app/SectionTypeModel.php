<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionTypeModel extends Model
{
	protected $table = 'cms_section_type';
	public $timestamps = false;
}
