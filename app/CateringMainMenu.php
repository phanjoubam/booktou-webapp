<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringMainMenu extends Model
{
    protected $table = 'ba_catering_main_menu';
}
