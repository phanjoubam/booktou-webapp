<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessBankAccountModel extends Model
{
	protected $table = 'ba_business_bank_account';
    public $timestamps = false;
}
