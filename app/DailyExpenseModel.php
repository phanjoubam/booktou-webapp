<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyExpenseModel extends Model
{
	protected $table = 'ba_daily_expense';
	public $timestamps = false;
}
