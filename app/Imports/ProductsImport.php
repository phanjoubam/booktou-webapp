<?php

namespace App\Imports;

use App\ProductImportedModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */ 
    public function model(array $row)
    {
        return new ProductImportedModel([
           'barcode' => $row[0], 
           'pr_name'   => ($row[1] == "" ? $row[0] : $row[1] ) , 
           'description'    => ($row[2] == "" ? $row[0] : $row[2] ) , 
           'unit_price'    => $row[3],  
           'stock' => ($row[4] == "" ? 0: $row[4] )  , 
        ]);
    }  
}


 