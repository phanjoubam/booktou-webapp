<?php

namespace App\Imports;

use App\ProductImportedModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;



class ProductImport implements ToModel 
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ( is_numeric( $row[0] )    ) 
        {
            if( $row[1] == "Store" == $row[1] == "") 
            {
                return;
            }
            else 
            {
                 return new ProductImportedModel([
                        'store' => $row[1],
                        'pr_name' => $row[2],
                        'description'    => $row[3],
                        'unit_value' => $row[4],
                        'unit_name' => $row[5],
                        'unit_price' => $row[6],
                        'stock' => $row[7],
                        'category' => $row[8]

                      ]);
            } 
        }  

    }
}
