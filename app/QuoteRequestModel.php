<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteRequestModel extends Model
{
	protected $table = 'ba_quote_request';
	public $timestamps = false; 
	
}
