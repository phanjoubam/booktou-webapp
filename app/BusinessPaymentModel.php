<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPaymentModel extends Model
{
	protected $table = 'ba_business_repayment';
	public $timestamps = false;
}
