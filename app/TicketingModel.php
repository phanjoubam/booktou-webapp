<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketingModel extends Model
{
    protected $table = 'event_ticket'; 
    public $timestamps = false; 
}
