<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTicketModel extends Model
{
	protected $table = 'event_ticket';
}
