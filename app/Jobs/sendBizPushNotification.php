<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


use App\Libraries\FirebaseCloudMessage;


use App\ServiceBooking;
use App\Business;



class sendBizPushNotification implements ShouldQueue
{
    use Utilities, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
         //$this->order_id = $order_id ;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $orders = ServiceBooking::where('alert_merchant','no')
         ->where('book_status','new')
         ->get();


      $bins = $oids = array(0);
      foreach($orders as $order_info)
      {
        $oids[] =  $order_info->id;
        $bins[] = $order_info->bin;
      } 


      $response = array(); 
      $response['title']     =  "New Order"  ;
      $response['message']   = "You have a new order" ; 
      
      $firebase = new FirebaseCloudMessage();

      $target_tokens  = DB::table("ba_business")
      ->join("ba_users", "ba_users.bin", "=", "ba_business.id")
      ->whereIn("ba_business.id",  $bins)
      ->select( "bin", "firebase_token")
      ->get();

      for($orders as $order_info)
      {
        foreach($target_tokens as $target_token)
        {
            if($order_info->bin == $target_token->bin )
            {
                $response['orderId'] = $order_info->id; 
                $response = $firebase->sendToIndividualBizOwner( $target_token, $response );
                break;
            }
            
        }
      }
      

       //assuming all messages are successfully sent
      DB::table("ba_service_booking") 
      ->whereIn("id",  $oids)
      ->update(  [ "alert_merchant" => "yes" ] ) ;

    }
}
