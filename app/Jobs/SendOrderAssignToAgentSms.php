<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Traits\Utilities;

use App\ServiceBooking;
use App\CustomerProfileModel;
use App\Business;



class SendOrderAssignToAgentSms implements ShouldQueue
{
    use Utilities, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $order_id;
    protected $agent_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $order_id, $agent_id )
    {
        //  
        $this->order_id = $order_id ;
        $this->agent_id = $agent_id;

    } 

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( )
    {

        $order_info = ServiceBooking::find( $this->order_id ); 

        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }

        $agent_info = CustomerProfileModel::find( $this->agent_id );

        $business_owner  = Business::find( $order_info->bin ) ; 

        
          //sms to buyer   
          if(isset($business_owner))
          {
            $msg = 'Order #'. $order_info->id . ' is assigned to BookTou agent for delivery. ' .  
            'Our delivery agent - '. $agent_info->fullname . ' ( ID #: ' .  $agent_info->id .  ') will pick the package'; 
            $this->sendSmsAlert( array( $business_owner->phone_pri  ),  $msg );  
          } 


          //sms to agent  
          if(isset( $agent_info ))
          {
            
            $msg = 'You have a new delivery task for order #'. $order_info->id . '. Please check your delivery list.'; 
            $this->sendSmsAlert( array(  $agent_info->phone ),  $msg ); 

          }
          
 
  
    }
}
