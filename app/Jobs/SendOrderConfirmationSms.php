<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Traits\Utilities;

use App\ServiceBooking;
use App\CustomerProfileModel;
use App\Business;



class SendOrderConfirmationSms implements ShouldQueue
{
    use Utilities, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $order_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $order_id )
    {
        //  
        $this->order_id = $order_id ;

    } 

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( )
    {

        $order_info = ServiceBooking::find( $this->order_id ); 

        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }


         $cust_info = CustomerProfileModel::find($order_info->book_by);
          if(isset($cust_info))
          {
            $msg = 'Your order #' . $order_info->id . ' is confirmed and will be processed by the store. ' .
            'Please use OTP ' .  $order_info->otp  .' to receive ordered items from our bookTou delivery agent.'; 
            //sms to buyer
            $this->sendSmsAlert( array( $cust_info->phone  ),  $msg ); 
          } 


          $business_owner  = Business::find( $order_info->bin ) ;  


          if(isset($business_owner))
          {
            $msg = 'You have a new order with # ' . $order_info->id . 
            '. Please make it ready for our bookTou agent to pickup and deliver. Earn more selling in bookTou'; 
            //sms to buyer
            $this->sendSmsAlert( array( $business_owner->phone_pri  ),  $msg ); 
          } 

  
    }
}
