<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Traits\Utilities;

 

class SendEmaOrderSms implements ShouldQueue
{
    use Utilities, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         $date = date('Y-m-d'); 

        $order_list = DB::table("ba_service_booking")
        ->whereRaw("date(book_date) ='$date'")
        ->where("book_status", "<>", "cancelled")
        ->where("sms_sent", "no")
        ->get();

        $oids= array();  

        foreach ($order_list as $item)
        {
            $oids[] = $item->id;

            $customer_phone = DB::table("ba_profile")
            ->select("phone") 
            ->where("id",  $item->book_by )
            ->first();
    
            if(isset($customer_phone))
            {
                $message = 'Your order is placed successfully through BookTou. Order # is ' . $item->id . '. Our delivery partner will contact you soon.';
                $this->sendSmsAlert( array($customer_phone->phone) ,   $message );
            }

            //sms bookTou Team for new orders
            $message = 'There is a new order with reference # ' . $item->id . '. Please process it from the CC Portal.';
            $this->sendSmsAlert( array(  '9612824492', '7005894963', '9740012097' ) ,   $message );
        }

        DB::table("ba_service_booking") 
        ->whereIn("id", $oids)
        ->update(['sms_sent' => 'yes']); 
    }
}
