<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Traits\Utilities;

use App\ServiceBooking;
use App\CustomerProfileModel;
use App\Business;



class SendAgentOrderProcessingSms  implements ShouldQueue
{
    use Utilities, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $order_info;
    protected $agent_id;
    protected $order_status;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $order_info , $agent_id, $order_status)
    {
        //  
        $this->order_info = $order_info  ;
        $this->agent_id = $agent_id ;
        $this->order_status = $order_status;

    } 

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( )
    {

        //$order_info = ServiceBooking::find( $this->order_id ); 

        $order_info = $this->order_info ; 
 

        if( $this->order_status  == "delivered")
        {
                  $cust_info = CustomerProfileModel::find($order_info->book_by);
                  if(isset($cust_info))
                  {
                      $msg = 'Your order #' . $order_info->id . ' is delivered successfully. Thank you for using BookTou for shopping.'; 
                      //sms to buyer
                      $this->sendSmsAlert( array( $cust_info->phone  ),  $msg ); 
                  }

                  $business_owner  = Business::find( $order_info->bin ) ;   

                  if(    isset( $business_owner)  )
                  {
                     $msg = 'Order #'. $order_info->id . ' has been successfully delivered by our BookTou agent. Thank you for selling via BookTou.';
                     $this->sendSmsAlert( array( $business_owner->phone_pri  ),  $msg );
                  } 

                  $agent_info = CustomerProfileModel::find(  $this->agent_id  );
                  if(isset($agent_info))
                  {
                      $msg = 'You have earned commission for your successful delivery to customer. Deliver more using BookTou to keep earning.'; 
                      //sms to buyer
                      $this->sendSmsAlert( array( $agent_info->phone  ),  $msg ); 
                  }
 
          }


  
    }
}
