<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Traits\Utilities;

use App\ServiceBooking;
use App\CustomerProfileModel;
use App\Business;



class SendSellerOrderProcessingSms  implements ShouldQueue
{
    use Utilities, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $order_id;
    protected $order_status;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $order_id,   $order_status)
    {
        //  
        $this->order_id = $order_id ;
        $this->order_status = $order_status;

    } 

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( )
    {

        $order_info = ServiceBooking::find( $this->order_id ); 

        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }

        $cust_info = CustomerProfileModel::find($order_info->book_by);


        if(   !isset( $cust_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching customer found.'  ];
         return json_encode($data);
        }
  

        //sending sms
        if( $this->order_status == "order_packed")
        {
          $msg = 'Your order #' . $order_info->id    .' is packed and ready for delivery. BookTou delivery partner will start delivery shortly.'; 
          //sms to buyer
          $this->sendSmsAlert( array( $cust_info->phone  ),  $msg ); 
            
        }
 

        if( $this->order_status == "completed" || $this->order_status == "delivered" )
        {
          $msg = 'Your order #' . $order_info->id    .' is delivered. Thank you for using BookTou. '; 
          //sms to buyer
          $this->sendSmsAlert( array( $cust_info->phone  ),  $msg );    
        }
 
  
    }
}
