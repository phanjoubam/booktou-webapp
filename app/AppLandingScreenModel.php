<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppLandingScreenModel extends Model
{
	protected $table = 'app_landing_screens';
	public $timestamps = false;
}
