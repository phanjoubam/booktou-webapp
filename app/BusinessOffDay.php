<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessOffDay extends Model
{
	protected $table = 'ba_shop_off_day';
	public $timestamps = false;
}
