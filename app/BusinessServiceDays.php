<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessServiceDays extends Model
{
    protected $table = 'ba_service_days';
	public $timestamps = false;
}
