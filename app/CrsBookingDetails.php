<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrsBookingDetails extends Model
{
	protected $table = 'ba_crs_booking_details';
    public $timestamps = false;
}