<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringCategory extends Model
{
    protected $table = 'ba_catering_category';
}
