<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistsPndItemsModel extends Model
{
    protected $table = 'ba_assists_pnd_items';
    public $timestamps = false;
}
