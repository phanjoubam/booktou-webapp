<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRequestStatusModel  extends Model
{
	protected $table = 'ba_order_process_request';
    public $timestamps = false;
}
