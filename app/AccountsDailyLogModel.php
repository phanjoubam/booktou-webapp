<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountsDailyLogModel extends Model
{
	protected $table = 'ba_accounts_daily_log';
    public $timestamps = false;
}
