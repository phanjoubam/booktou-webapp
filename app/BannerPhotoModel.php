<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerPhotoModel extends Model
{
    protected $table = 'app_banner_photo';
    public $timestamps = false;  

}
