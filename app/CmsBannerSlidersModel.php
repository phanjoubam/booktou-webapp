<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsBannerSlidersModel extends Model
{
    protected $table = 'cms_banner_sliders'; 
    public $timestamps = false; 
}
