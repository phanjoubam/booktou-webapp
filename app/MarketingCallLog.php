<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingCallLog extends Model
{
    protected $table = 'ba_marketing_call_log';
	public $timestamps = false;
}
