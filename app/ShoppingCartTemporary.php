<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCartTemporary extends Model
{
    protected $table = 'ba_temp_cart'; 
}
