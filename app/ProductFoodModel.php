<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFoodModel extends Model
{
    protected $table = 'ba_product_food';
    public $timestamps = false;

}
