<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantOrderSequencer extends Model
{
    protected $table = 'ba_merchant_order_sequencer';
    public $timestamps = false;
}
