<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringSales extends Model
{
    protected $table = 'ba_catering_sales';
}
