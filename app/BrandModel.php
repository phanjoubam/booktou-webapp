<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
	protected $table = 'ba_water_brand';
	public $timestamps = false;
}
