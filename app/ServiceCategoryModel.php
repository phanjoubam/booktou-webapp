<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategoryModel extends Model
{
	protected $table = 'ba_service_category';
	public $timestamps = false;
}
