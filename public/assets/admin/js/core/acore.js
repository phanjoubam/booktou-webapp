var d='/';
var siteurl =  "//" + window.location.hostname + d  ;
var api =  "//alpha.booktou.in/api/" ;


 
	 
var ChartColor = ["#5D62B4", "#54C3BE", "#EF726F", "#F9C446", "rgb(93.0, 98.0, 180.0)", "#21B7EC", "#04BCCC"];
var primaryColor = getComputedStyle(document.body).getPropertyValue('--primary');
var secondaryColor = getComputedStyle(document.body).getPropertyValue('--secondary');
var successColor = getComputedStyle(document.body).getPropertyValue('--success');
var warningColor = getComputedStyle(document.body).getPropertyValue('--warning');
var dangerColor = getComputedStyle(document.body).getPropertyValue('--danger');
var infoColor = getComputedStyle(document.body).getPropertyValue('--info');
var darkColor = getComputedStyle(document.body).getPropertyValue('--dark');
var lightColor = getComputedStyle(document.body).getPropertyValue('--light');
(function ($) {
  'use strict';
  $(function () {
    var body = $('body');
    var contentWrapper = $('.content-wrapper');
    var scroller = $('.container-scroller');
    var footer = $('.footer');
    var sidebar = $('#sidebar');

    //Add active class to nav-link based on url dynamically
    //Active class can be hard coded directly in html file also as required
    if (!$('#sidebar').hasClass("dynamic-active-class-disabled")) {
      var current = location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');
      $('#sidebar >.nav > li:not(.not-navigation-link) a').each(function () {
        var $this = $(this);
        if (current === "") {
          //for root url
          if ($this.attr('href').indexOf("index.html") !== -1) {
            $(this).parents('.nav-item').last().addClass('active');
            if ($(this).parents('.sub-menu').length) {
              $(this).addClass('active');
            }
          }
        } else {
          //for other url
          if ($this.attr('href').indexOf(current) !== -1) {
            $(this).parents('.nav-item').last().addClass('active');
            if ($(this).parents('.sub-menu').length) {
              $(this).addClass('active');
            }
            if (current !== "index.html") {
              $(this).parents('.nav-item').last().find(".nav-link").attr("aria-expanded", "true");
              if ($(this).parents('.sub-menu').length) {
                $(this).closest('.collapse').addClass('show');
              }
            }
          }
        }
      })
    }

    //Close other submenu in sidebar on opening any
    $("#sidebar > .nav > .nav-item > a[data-toggle='collapse']").on("click", function () {
      $("#sidebar > .nav > .nav-item").find('.collapse.show').collapse('hide');
    });

   

    //checkbox and radios
    $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');

  });
  $('.dropdown-toggle').dropdown()




  $.toastDefaults = {
    position: 'bottom-right', 
    dismissible: true, 
    stackable: true, 
    pauseDelayOnHover: true, 
    style: {
      toast: '',
      info: '',
      success: '',
      warning: '',
      error: '',
    }
  
  };


   


})(jQuery);




$(document).ready(  function()
  { 
    setInterval(checkNewOrders, 10000);
  });


function checkNewOrders()
  {
     

    if (typeof $.cookie('lid') === 'undefined'){
      var lid  = 0;
    } else {
       var lid = $.cookie("lid");
    } 
   
    
      var json = {};
      json['lastScanId'] =  lid;

      $.ajax({
        type: 'post',
        url: api + "v3/web/orders/scan-new-orders" ,
        data:   json ,
        success: function(data)
        {
          data = $.parseJSON(data);
          $.cookie('lid',  data.lastId ); 
          if( data.lastId > lid )
          {
            $.toast({
          position:'bottom-right', 
        title: 'bookTou Notification', 
        content: 'There is a new order coming in.',
        type: 'info',
        delay: 120000,
        dismissible: true,
          img: {
            src: '<?php echo URL::to("/public/assets/image/notification.png"); ?>',
            class: 'rounded',
            title: 'BTN',
            alt: 'BTN'
          }
      }); 

          }
        }
      });
  }





// var d='/';
// var siteurl =  "//" + window.location.hostname + d  ;
// var api =  siteurl + "api/" ;
var cdn =  siteurl + "public/" ;
var erp =  "//alpha.booktou.in/";
//var erp =  "//localhost/bt/" ;

$(".redirect").click(function(){

     
    var code = $(this).attr("data-pgc");
    var key  = $(this).attr("data-key");
    


    switch(code)
    { 
        case "1":
            
            var win = window.open( erp + "pos/orders/view-bill?o=" + key , '_blank');
            if (win) { 
                win.focus();
            } else { 
                alert('Please allow popups for this website');
            } 
            break;

            

            case "2":
            window.open(erp +"services/booking-appointment?o=" + key ,'_blank');
            break;

            case "3":
            var dat  = $(this).attr("data-keydate");
            var win = window.open( erp + "business/franchise/print-receipt?filter_date="+dat+"&o=" + key , '_blank');
            if (win) { 
                win.focus();
            } else { 
                alert('Please allow popups for this website');
            } 
            break;

            case "4":
            var win = window.open( erp + "services/download-water-receipt?o=" + key , '_blank');
            if (win) { 
                win.focus();
            } else { 
                alert('Please allow popups for this website');
            }
            break;

    }


})