<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// Route::get('/beta', 'StoreFront\StoreFrontController@indexBeta')->middleware('preloadcmsconfig' );

Route::get('/reset', 'StoreFront\StoreFrontController@resetAll');
Route::get('/', 'StoreFront\StoreFrontController@index')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::match(array('get', 'post'), '/shopping/login', 'Auth\LoginController@customerLogin')->middleware('preloadcmsconfig');
Route::post('/shopping/customer/chat-login', 'Auth\LoginController@ChatLogin')->middleware('preloadcmsconfig','logshoppingmainmodule');
Route::post('/shopping/show-chat-login', 'StoreFront\StoreFrontController@loadChatLogin');
Route::get('/logout', 'UserController@logout');
Route::get('/shopping/logout', 'UserController@logoutCustomer');

Route::post('/shopping/select-cart-for-order', 'StoreFront\StoreFrontController@cartSelectionForOrder');
Route::get('/shopping/my-orders', 'StoreFront\StoreFrontController@viewMyOrders')->middleware('preloadcmsconfig' );
Route::post('/shopping/update-order-remarks', 'StoreFront\StoreFrontController@updateOrderRemarks')->middleware('preloadcmsconfig');
Route::post('/shopping/update-assist-order-remarks', 'StoreFront\StoreFrontController@updateAssistOrderRemarks')->middleware('preloadcmsconfig');


Route::post('/shopping/select-cart-for-order', 'StoreFront\StoreFrontController@cartSelectionForOrder')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::get('/shopping/my-orders', 'StoreFront\StoreFrontController@viewMyOrders')->middleware('preloadcmsconfig','logshoppingmainmodule'  );
Route::get('/shopping/my-order-details', 'StoreFront\StoreFrontController@myOrderDetails')->middleware('preloadcmsconfig','logshoppingmainmodule'  );
Route::get('/shopping/my-bookings', 'StoreFront\StoreFrontController@myBookings')->middleware('preloadcmsconfig','logshoppingmainmodule'  );

Route::post('/shopping/update-order-remarks', 'StoreFront\StoreFrontController@updateOrderRemarks')->middleware('preloadcmsconfig');
Route::post('/shopping/update-assist-order-remarks', 'StoreFront\StoreFrontController@updateAssistOrderRemarks')->middleware('preloadcmsconfig');
Route::post('/shopping/update-order-review', 'StoreFront\StoreFrontController@updateOrderReview')->middleware('preloadcmsconfig' );

Route::post('/shopping/remove-cart', 'StoreFront\StoreFrontController@removeCart');
Route::get('/shopping/checkout/payment-delivery-address', 'StoreFront\StoreFrontController@orderPaymentAndDeliveryDetails')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');
Route::post('/payment/response', 'StoreFront\StoreFrontController@cashfreeResponse');
Route::post('/shopping/add-delivery-address', 'StoreFront\StoreFrontController@addDeliveryAddress')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::get('/settings/view-my-profile', 'StoreFront\StoreFrontController@viewMyProfile')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::get('/shopping/show-wishlist-product', 'StoreFront\StoreFrontController@showWishListProduct')->middleware('preloadcmsconfig','logshoppingmainmodule');

Route::match(array('get', 'post'),'/shopping/place-order', 'StoreFront\StoreFrontController@placeOrder')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');
Route::get('/shopping/checkout-in-process', 'StoreFront\StoreFrontController@checkoutInProcess')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');


Route::get('/shopping/payment-in-progress', 'StoreFront\StoreFrontController@razorpayPaymentProgress')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');


Route::match(array('get', 'post'),'/shopping/payment-completed', 'StoreFront\StoreFrontController@razorpayPaymentCompleted')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');

Route::match(array('get', 'post'),'/appointment/payment-completed', 'Appointment\AppointmentControllerWeb@appointmentrazorpayPaymentCompleted')->middleware('preloadcmsconfig' ,'logappointmentmainmodule');


Route::get('/shopping/make-order-enquiry', 'StoreFront\StoreFrontController@makeOrderEnquiry')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');
Route::post('/shopping/place-order-enquiry', 'StoreFront\StoreFrontController@placeOrderEnquiry')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');
Route::get('/shopping/where-is-my-order/{trackcode}', 'StoreFront\StoreFrontController@whereIsMyOrder')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');
Route::get('/order/track/{trackcode}', 'StoreFront\StoreFrontController@whereIsMyOrder')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');

Route::get('/merchant-agreement', 'HomeController@merchantAgreementForm')->middleware( 'preloadcmsconfig' );
Route::post('/merchant-agreement', 'HomeController@saveMerchantAgreementForm')->middleware( 'preloadcmsconfig' );
Route::match(array('GET', 'POST'), '/shopping-enquiry-wizard', 'StoreFront\StoreFrontController@booktouAssistWizard')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');
//assits wizard booktou
Route::get('/shopping/assist-pickup/{code}', 'StoreFront\StoreFrontController@assistPickup')->middleware('preloadcmsconfig','logshoppingmainmodule');
Route::post('/shopping/assist-pickup-save', 'StoreFront\StoreFrontController@assistPickupSave')->middleware('preloadcmsconfig','logshoppingmainmodule');
Route::get('/shopping/assist-destination', 'StoreFront\StoreFrontController@assistDestination')->middleware('preloadcmsconfig','logshoppingmainmodule');
Route::post('/shopping/assist-save-destination', 'StoreFront\StoreFrontController@assistDestinationSave')->middleware('preloadcmsconfig','logshoppingmainmodule');
Route::get('/shopping/assist-task-description', 'StoreFront\StoreFrontController@assistTaskDescription')->middleware('preloadcmsconfig','logshoppingmainmodule');
Route::post('/shopping/assist-task-complete', 'StoreFront\StoreFrontController@assistSaveTaskDescription')->middleware('preloadcmsconfig','logshoppingmainmodule');


Route::match(array('GET', 'POST'), '/shopping-enquiry-wizard', 'StoreFront\StoreFrontController@booktouAssistWizard');

//assits wizard booktou
Route::get('/shopping/assist-pickup/{code}', 'StoreFront\StoreFrontController@assistPickup')
->middleware('preloadcmsconfig');
Route::post('/shopping/assist-pickup-save', 'StoreFront\StoreFrontController@assistPickupSave')
->middleware('preloadcmsconfig');
Route::get('/shopping/assist-destination', 'StoreFront\StoreFrontController@assistDestination')
->middleware('preloadcmsconfig');
Route::post('/shopping/assist-save-destination', 'StoreFront\StoreFrontController@assistDestinationSave')
->middleware('preloadcmsconfig');
Route::get('/shopping/assist-task-description', 'StoreFront\StoreFrontController@assistTaskDescription')
->middleware('preloadcmsconfig');
Route::post('/shopping/assist-task-complete', 'StoreFront\StoreFrontController@assistSaveTaskDescription')
->middleware('preloadcmsconfig');

Route::post('/shopping/assist-complete-your-request', 'StoreFront\StoreFrontController@assistCompleteYourRequest')
->middleware('preloadcmsconfig');
Route::get('/shopping/my-assist-order', 'StoreFront\StoreFrontController@myAssistOrder')
->middleware('preloadcmsconfig');
// .............meena......
Route::get('/shopping/booktou-assist', 'StoreFront\StoreFrontController@booktouAssist')
->middleware('preloadcmsconfig');
// ........end...........................
// assist wizard ends here
//donation
Route::get('/donate-to-covid-fund', 'StoreFront\StoreFrontController@donateToCovidFund')->middleware('preloadcmsconfig' );
Route::post('/donate-to-covid-fund', 'StoreFront\StoreFrontController@donateToCovidFund')->middleware('preloadcmsconfig' );

Route::get('/covid-donation-form', 'StoreFront\StoreFrontController@covidDonationForm')->middleware('preloadcmsconfig' );

Route::get('/covid-fund-donors', 'StoreFront\StoreFrontController@covidFundDonors')->middleware('preloadcmsconfig' );

Route::get('/covid-fund-beneficiaries', 'StoreFront\StoreFrontController@covidFundBeneficiaries')->middleware('preloadcmsconfig' );

//donation ends here


Route::post('/booktou-assist-donation_wizard-post', 'StoreFront\StoreFrontController@booktouAssistDonationWizard')->middleware('preloadcmsconfig' );
Route::get('/assist-donation-form', 'StoreFront\StoreFrontController@assistDonationForm')->middleware('preloadcmsconfig' );


Route::get('/covid-fund/beneficiaries', 'StoreFront\StoreFrontController@covidFundBeneficiaries')->middleware('preloadcmsconfig' );
Route::get('/covid-fund/donors', 'StoreFront\StoreFrontController@covidFundDonors')->middleware('preloadcmsconfig' );



Route::get('/business/{merchant_code}', 'StoreFront\StoreFrontController@shopByBusiness')->middleware('preloadcmsconfig','logshoppingmainmodule' );





Route::get('/shop-by-product-category/{sdfsdf}/{category}/{submenu?}', 'StoreFront\StoreFrontController@shopByCategory')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');
Route::get('/shop-by-menulist/{mainmenu}/{submenu}', 'StoreFront\StoreFrontController@shopByCategoryMenuList')->middleware('preloadcmsconfig','logshoppingmainmodule');

Route::get('/shopping/search-products', 'StoreFront\StoreFrontController@searchProducts')->middleware('preloadcmsconfig','logshoppingmainmodule'); 
Route::get('/join-and-refer/{code?}', 'StoreFront\StoreFrontController@showSignupForm')->middleware('preloadcmsconfig','logshoppingmainmodule' );  
Route::post('/generate-refer-code', 'StoreFront\StoreFrontController@joinAndGenerateReferralLink')->middleware('preloadcmsconfig','logshoppingmainmodule' );  
Route::get('/refer/{code}', 'StoreFront\StoreFrontController@viewReferralInformation')->middleware('preloadcmsconfig','logshoppingmainmodule' );  


Route::post('/shopping/submit-instant-quote', 'StoreFront\StoreFrontController@submitInstantQuote')->middleware('preloadcmsconfig','logshoppingmainmodule' );  
Route::post('/shopping/subscribe', 'StoreFront\StoreFrontController@subscribeEmailMarketing')->middleware('preloadcmsconfig','logshoppingmainmodule' );  

Route::get('/shopping/search-products', 'StoreFront\StoreFrontController@searchProducts')->middleware('preloadcmsconfig','logshoppingmainmodule');
Route::get('/join-and-refer/{code?}', 'StoreFront\StoreFrontController@showSignupForm')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::post('/generate-refer-code', 'StoreFront\StoreFrontController@joinAndGenerateReferralLink')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::get('/refer/{code}', 'StoreFront\StoreFrontController@viewReferralInformation')->middleware('preloadcmsconfig','logshoppingmainmodule' );
//Route::post('/shopping/place-order', 'StoreFront\StoreFrontController@placeOrder')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::post('/shopping/submit-instant-quote', 'StoreFront\StoreFrontController@submitInstantQuote')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::post('/shopping/subscribe', 'StoreFront\StoreFrontController@subscribeEmailMarketing')->middleware('preloadcmsconfig','logshoppingmainmodule' );

Route::get('/about-us', 'HomeController@aboutUs');
Route::get('/privacy-policy', 'HomeController@privacyPolicy');
Route::get('/terms-and-conditions', 'HomeController@termsOfUse');
Route::get('/cancellation-and-refund', 'HomeController@cancellationAndRefund');
Route::get('/shipping-and-delivery', 'HomeController@shippingAndDelivery');
/*===============Terms and conditions ================================*/
Route::get('/terms/about-us', 'HomeController@webAboutUs')->middleware('preloadcmsconfig');
Route::get('/terms/privacy-policy', 'HomeController@webPrivacyPolicy')->middleware('preloadcmsconfig');
Route::get('/terms/terms-and-conditions', 'HomeController@webTermsOfUse')->middleware('preloadcmsconfig');
Route::get('/terms/cancellation-and-refund', 'HomeController@webCancellationAndRefund')->middleware('preloadcmsconfig');
Route::get('/terms/shipping-and-delivery', 'HomeController@webShippingAndDelivery')->middleware('preloadcmsconfig');


Route::get('/about-us', 'HomeController@aboutUs');
Route::get('/privacy-policy', 'HomeController@privacyPolicy');
Route::get('/terms-and-conditions', 'HomeController@termsOfUse');
Route::get('/cancellation-and-refund', 'HomeController@cancellationAndRefund');
Route::get('/shipping-and-delivery', 'HomeController@shippingAndDelivery');



Route::get('/business-app/about-us', 'HomeController@aboutUs');
Route::get('/business-app/privacy-policy', 'HomeController@privacyPolicy');
Route::get('/business-app/terms-and-conditions', 'HomeController@termsOfUse');


Route::get('/agent-app/about-us', 'HomeController@aboutUs');
Route::get('/agent-app/privacy-policy', 'HomeController@privacyPolicy');
Route::get('/agent-app/terms-and-conditions', 'HomeController@termsOfUse');
/*========================================================================*/

Route::get('/careers', 'HomeController@joinUs')->middleware('preloadcmsconfig' );
Route::get('/contact-us', 'StoreFront\StoreFrontController@contactUs')->middleware('preloadcmsconfig' );
Route::post('/submit-contact-us-form', 'StoreFront\StoreFrontController@saveContactForm')->middleware('preloadcmsconfig' );
Route::get('/downloads/drivers/pos/windows', 'HomeController@downloadPrinterDriver');

Route::get('/inner-beta', 'StoreFront\StoreFrontController@betaInnerTest')->middleware('preloadcmsconfig' );
Route::get('/shopping/browse-menu', 'StoreFront\StoreFrontController@browserMenu')->middleware('preloadcmsconfig','logshoppingmainmodule' );




Route::get('/appointment/book-a-doctor', 'StoreFront\StoreFrontController@bookADoctor')->middleware('preloadcmsconfig', 'logappointmentmainmodule'  );
Route::post('/appointment/confirm-and-checkout', 'StoreFront\StoreFrontController@confirmAndCheckout')->middleware('preloadcmsconfig' , 'logappointmentmainmodule');

Route::get('/appointment/checkout-completed', 'StoreFront\StoreFrontController@checkoutCompleted')->middleware('preloadcmsconfig' , 'logappointmentmainmodule');
Route::post('/appointment/checkout-completed', 'StoreFront\StoreFrontController@checkoutCompleted')->middleware('preloadcmsconfig' , 'logappointmentmainmodule');
Route::get('/appointment/notify-checkout-status', 'StoreFront\StoreFrontController@checkoutCompleted')->middleware('preloadcmsconfig', 'logappointmentmainmodule' );
Route::post('/appointment/notify-checkout-status', 'StoreFront\StoreFrontController@checkoutCompleted')->middleware('preloadcmsconfig' , 'logappointmentmainmodule');
// appointment routes starts from here for public portal by sanatomba
Route::get('/booking/slot-selector/{bin}/{srv_code?}', 'Booking\BookingController@bookANewService')->middleware('preloadcmsconfig' );

Route::get('/booking/cab-slot-selector/{bin}/{srv_code?}', 'Booking\BookingController@bookCABNewService')->middleware('preloadcmsconfig' );
Route::post('/booking/customer/services/place-booking','StoreFront\StoreFrontController@makeServiceBooking')->middleware('preloadcmsconfig' );
// Route::get('/shopping/get-offer-details/{productid}', 'StoreFront\StoreFrontController@getOfferDetails')->middleware('preloadcmsconfig' );
Route::post('/booking/purchase-service-offers', 'StoreFront\StoreFrontController@purchaseServiceOffers')->middleware('preloadcmsconfig' );


//booking and shopping common 







Route::get('/{name}/service-details/{categoryname}',"Booking\BookingController@bookingCategoryDetails")->middleware('preloadcmsconfig');
Route::get('/{name}/appointment-details/{categoryname}/{category?}',"Booking\BookingController@appointmentServiceDetails")->middleware('preloadcmsconfig' );
Route::post('/appointment/get-service-details',"Booking\BookingController@getServiceDetails")->middleware('preloadcmsconfig');
Route::get('/{name}/appointment/view-service/{serviceid}/{category?}',"Booking\BookingController@viewServiceBusiness")->middleware('preloadcmsconfig' );


Route::get('/booking/btm-{bin}', "Booking\BookingController@bookingBusinessDetails")->middleware('preloadcmsconfig');


Route::get('/{name}/business/btm-{bin}/{category?}',"Booking\BookingController@appointmentBusiness")->middleware('preloadcmsconfig' );





Route::get('/service/booking/{name}',"Booking\BookingController@viewByBookingMenuCategoryName")->middleware('preloadcmsconfig' );
Route::get('/service/appointment/{name}',"Booking\BookingController@viewByAppointmentCategoryName")->middleware('preloadcmsconfig');


//Route::get('/booking/business/search-booking',"Booking\SearchController@searchBooking")->middleware('preloadcmsconfig' );
//Route::get('/admin/rating', 'Admin\RatingController@selectRating')->middleware('auth', 'checkifadmin' );
// ...............end............

/* ****************************************** */
//
//
//   ADMIN BLOCK
//
//
/* ****************************************** */
Route::get('/admin', 'Admin\AdminDashboardController@dashboard')->middleware('auth', 'checkifadmin' );
Route::get('/admin/profileView', 'Admin\AdminDashboardController@profileView')->middleware('auth', 'checkifadmin' );
Route::get('/admin/View', 'CustomerCareController@viewCustomerCare')->middleware('auth', 'checkifadmin' );
Route::get('/order/get-customer-details', 'CustomerCareController@getCustomerDetails')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer-care/orders/view-all', 'Admin\AdminDashboardController@viewAllOrders')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/orders/view-all', 'Admin\AdminDashboardController@viewAllOrders')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/order/view-details/{orderno}', 'Admin\AdminDashboardController@viewOrderDetails')->middleware('auth', 'checkifadmin' );
Route::get('/admin/order/view-information', 'Admin\AdminDashboardController@redirectToOrderDetailsView')->middleware('auth', 'checkifadmin' );


Route::post('/admin/customer-care/orders/update-delivery-date', 'Admin\AdminDashboardController@updateDeliveryDate')->middleware('auth' , 'checkifadmin');

Route::post('/admin/customer-care/orders/remove-assigned-agent', 'Admin\AdminDashboardController@removeAgentFromNormalOrder')->middleware('auth' , 'checkifadmin');



Route::get('/admin/order/view-information', 'Admin\AdminDashboardController@redirectToOrderDetailsView')->middleware('auth', 'checkifadmin' );

Route::get('/admin/normal-orders/stats', 'Admin\AdminDashboardController@normalOrderStatsLog')->middleware('auth' , 'checkifadmin');

//banner slide for website

Route::get('/admin/slide/enter-image-slide', 'Admin\AdminDashboardController@EnterImageSlide')->middleware('auth' , 'checkifadmin');
Route::post('/admin/save/photo-slide', 'Admin\AdminDashboardController@photoSlide')->middleware('auth' , 'checkifadmin');
Route::post('/admin/delete/photo-slide', 'Admin\AdminDashboardController@deleteSlide')->middleware('auth' , 'checkifadmin');

Route::post('/admin/cms/web-slider/update-position', 'Admin\AdminDashboardController@updateSlidePosition')->middleware('auth' , 'checkifadmin');
Route::post('/admin/cms/web-slider/update-slider-status', 'Admin\AdminDashboardController@toggleSliderStatus')->middleware('auth' , 'checkifadmin');
Route::get('/admin/system/water-booking/create-brand-name', 'Admin\AdminDashboardController@createBrandName')->middleware('auth' , 'checkifadmin');
Route::post('/admin/system/water-booking/save-brand-name', 'Admin\AdminDashboardController@saveBrandName')->middleware('auth' , 'checkifadmin');

//
Route::get('/forgot-password', 'StoreFront\StoreFrontController@forgotPassword')->middleware('preloadcmsconfig');

Route::post('/password-otpcheck', 'StoreFront\StoreFrontController@passwordOtpcheck')->middleware('preloadcmsconfig');
Route::post('/change-password', 'StoreFront\StoreFrontController@changePassword')->middleware('preloadcmsconfig');

//pick and drop ordres
Route::post('/admin/customer-care/pick-and-drop-orders/new-request', 'Admin\AdminDashboardController@newPickAndDropRequest')->middleware('auth' , 'checkifadmin');


Route::get('/admin/customer-care/pick-and-drop-orders/view-all', 'Admin\AdminDashboardController@viewPickAndDropOrders')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/pick-and-drop-orders/view-all', 'Admin\AdminDashboardController@viewPickAndDropOrders')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/pick-and-drop-orders/view-completed', 'Admin\AdminDashboardController@viewPickAndDropCompletedOrders')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/pick-and-drop-orders/view-completed', 'Admin\AdminDashboardController@viewPickAndDropCompletedOrders')->middleware('auth' , 'checkifadmin');

Route::get('/admin/customer-care/pick-and-drop-orders/view-future-orders',
	'Admin\AdminDashboardController@viewPndFutureOrders')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/pick-and-drop-orders/view-future-orders',
	'Admin\AdminDashboardController@viewPndFutureOrders')->middleware('auth' , 'checkifadmin');

Route::get('/admin/customer-care/pick-and-drop-orders/sortedlist-pick-and-drop', 'Admin\AdminDashboardController@SortPickandDropOrders')->middleware('auth' , 'checkifadmin');

Route::post('/admin/orders/pnd-orders/remove-assigned-agent', 'Admin\AdminDashboardController@removeAgentFromPnDOrder')->middleware('auth' , 'checkifadmin');

Route::post('/admin/orders/assist/remove-assigned-agent', 'Admin\AdminDashboardController@removeAgentFromAssist')->middleware('auth' , 'checkifadmin');

Route::get('/admin/orders/assists/view-all', 'Admin\AdminDashboardController@viewAllAssistOrders')->middleware('auth' , 'checkifadmin');
Route::get('/admin/orders/assists/view-all/view-completed', 'Admin\AdminDashboardController@viewAllAssistOrdersCompleted')->middleware('auth' , 'checkifadmin');



Route::post('/admin/assist-order/update-assist-details', 'Admin\AdminDashboardController@updateAssistOrderPaymentUpdate')->middleware('auth' );


Route::get('/admin/pick-and-drop-orders/prepare-allocation-table', 'Admin\AdminDashboardController@prepareAllocationTablePnD')->middleware('auth' , 'checkifadmin');


Route::get('/admin/orders/route-with-tasks-table', 'Admin\AdminDashboardController@routeAndTasksTable')->middleware('auth' , 'checkifadmin');

Route::post('/admin/update-agent-location', 'Admin\AdminDashboardController@updateAgentLocation')->middleware('auth' , 'checkifadmin');


Route::post('/admin/pick-and-drop-orders/approve-self-assign-request', 'Admin\AdminDashboardController@processAgentSelfAssignRequest')->middleware('auth' , 'checkifadmin');

Route::get('/admin/services/pnd-print-receipt',
'Admin\AdminDashboardController@generatePNDServiceBill')->middleware('auth', 'checkifadmin' );


//callback orders
Route::get('/admin/customer-care/callback-requests', 'Admin\AdminDashboardController@viewCallbackRequests')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/quotation-requests', 'Admin\AdminDashboardController@viewQuotationRequests')->middleware('auth' , 'checkifadmin');



//convert receipt to order
Route::post('/admin/customer-care/convert-receipt-to-pick-and-drop-order',  'Admin\AdminDashboardController@convertReceiptToPickAndDropRequest')->middleware('auth' , 'checkifadmin');

Route::post('/admin/order/assist/update-information',  'Admin\AdminDashboardController@updateAssistOrder')->middleware('auth' , 'checkifadmin');

Route::post('/admin/order/migrate-to-franchise',  'Admin\AdminDashboardController@routeOrderToFranchise')->middleware('auth' , 'checkifadmin');





Route::get('/admin/orders/remarks-and-feedback', 'Admin\AdminDashboardController@remarksAndFeedback')->middleware('auth' , 'checkifadmin' );



Route::post('/admin/customer-care/order/search-details', 'Admin\AdminDashboardController@viewOrderDetails')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer-care/orders/view-completed', 'Admin\AdminDashboardController@viewCompletedOrders')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/orders/view-completed', 'Admin\AdminDashboardController@viewCompletedOrders')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/view-importable-product/{bin}', 'Admin\AdminDashboardController@viewImportableProducts')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/import-products', 'Admin\AdminDashboardController@saveImportableProducts')->middleware('auth' , 'checkifadmin');
Route::get('/admin/products/category/manage-categories', 'Admin\AdminDashboardController@manageProductCategory')->middleware('auth' , 'checkifadmin');
Route::post('/admin/products/category/manage-categories', 'Admin\AdminDashboardController@manageProductCategory')->middleware('auth' , 'checkifadmin');
Route::post('/admin/products/category/save-category', 'Admin\AdminDashboardController@saveProductCategory')->middleware('auth' , 'checkifadmin');
Route::post('/admin/products/category/remove', 'Admin\AdminDashboardController@destroyCategory')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/view-all', 'Admin\AdminDashboardController@viewListedBusinesses')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/view-profile/{busi_id}','Admin\AdminDashboardController@viewBusinessProfile')->middleware('auth' , 'checkifadmin');

Route::get( '/admin/services/manage-category', 'Admin\AdminDashboardController@manageServiceCategory')->middleware('auth' , 'checkifadmin');
Route::post( '/admin/services/update-category-details', 'Admin\AdminDashboardController@updateServiceCategory')->middleware('auth' , 'checkifadmin');
Route::post('/admin/services/category/remove', 'Admin\AdminDashboardController@deleteServiceCategory')->middleware('auth' , 'checkifadmin');

Route::get('/admin/customer-care/business/{day}',
	'Admin\AdminDashboardController@viewListedBusinessesByDay')->middleware('auth' , 'checkifadmin');

Route::post('/admin/business/upload-profile-image','Admin\AdminDashboardController@uploadBannerImage')->middleware('auth' , 'checkifadmin');

Route::post('/admin/business/update-pan-gst-fssai','Admin\AdminDashboardController@updatePanGstFssai')->middleware('auth', 'checkifadmin' );
Route::post('/admin/business/update-comission','Admin\AdminDashboardController@updateCommission')->middleware('auth', 'checkifadmin' );
Route::post('/admin/business/archive-active-comission','Admin\AdminDashboardController@archiveActiveCommission')->middleware('auth', 'checkifadmin' );
Route::post('/admin/business/update-bank-info','Admin\AdminDashboardController@updateBankAccountInfo')->middleware('auth', 'checkifadmin' );





Route::post('/admin/business/update-payment-cycle','Admin\AdminDashboardController@updatePaymentCycle')->middleware('auth', 'checkifadmin' );

Route::post('/admin/business/update-phone-no','Admin\AdminDashboardController@updateBusinessPhoneNo')->middleware('auth', 'checkifadmin' );

Route::post('/admin/business/update-verification-tick','Admin\AdminDashboardController@updateVerificationTick')->middleware('auth', 'checkifadmin' );

Route::post('/admin/business/update-pos-merchant','Admin\AdminDashboardController@updatePosMerchant')
->middleware('auth', 'checkifadmin' );
Route::post('/admin/business/update-pos-payment','Admin\AdminDashboardController@updatePosPayment')
->middleware('auth', 'checkifadmin' );
Route::post('/admin/business/enable-self-pickup-service','Admin\AdminDashboardController@enableSelfPickupService')
->middleware('auth', 'checkifadmin' );

Route::get('/admin/customer-care/business/view-products/{bin}', 'Admin\AdminDashboardController@viewBusinessProducts')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/product/add-to-promotion', 'Admin\AdminDashboardController@addToPromotion')->middleware('auth', 'checkifadmin' );
// ...................start
Route::post('/admin/customer-care/business/product/update-price', 'Admin\AdminDashboardController@updatePrice')->middleware('auth', 'checkifadmin' );
Route::post('/admin/customer-care/business/product/add-popular-products', 'Admin\AdminDashboardController@addPopularProducts')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer-care/business/view-discount-products/{bin}', 'Admin\AdminDashboardController@viewDiscountProducts')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/update-discount-products', 'Admin\AdminDashboardController@updateDiscountProducts')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/delete-discount-products', 'Admin\AdminDashboardController@deleteDiscountProducts')->middleware('auth' , 'checkifadmin');

Route::post('/admin/customer-care/business/change-user-password', 'Admin\AdminDashboardController@changeUserPassword')->middleware('auth' , 'checkifadmin');

Route::post('/admin/customer-care/business/product/add-discounted-products', 'Admin\AdminDashboardController@addDiscountedProducts')->middleware('auth', 'checkifadmin' );

 //..........................end
Route::get('/admin/customer-care/business/view-product-photo/{bin}/{pid}', 'Admin\AdminDashboardController@viewProductImages')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/save-uploaded-photo', 'Admin\AdminDashboardController@uploadProductImage')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/search-products', 'Admin\AdminDashboardController@searchBusinessProducts')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/search-products', 'Admin\AdminDashboardController@searchBusinessProducts')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/get-daily-earning/{bin}', 'Admin\AdminDashboardController@viewBusinessDailyEarning')->middleware('auth' , 'checkifadmin');


Route::post('/admin/customer-care/business/search-daily-earning', 'Admin\AdminDashboardController@viewBusinessDailyEarning')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/save-earning-clearance', 'Admin\AdminDashboardController@saveBusinessDailyEarning')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/monthly-sales-report', 'Admin\AdminDashboardController@viewBusinessMonthlySales')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/monthly-pnd-report', 'Admin\AdminDashboardController@viewBusinessMonthlyPnD')->middleware('auth' , 'checkifadmin');



Route::post('/admin/order/update-remarks', 'Admin\AdminDashboardController@updateNormalOrderRemarks')->middleware('auth' , 'checkifadmin');
Route::post('/admin/order/update-pnd-remarks', 'Admin\AdminDashboardController@updatePnDOrderRemarks')->middleware('auth' , 'checkifadmin');
Route::post('/admin/order/update-normal-order-status', 'Admin\AdminDashboardController@updateNormalOrderStatus')->middleware('auth' , 'checkifadmin');
Route::post('/admin/order/update-normal-order-price', 'Admin\AdminDashboardController@updateNormalOrderPrice')->middleware('auth' , 'checkifadmin');
Route::post('/admin/order/update-normal-order-refund-amount', 'Admin\AdminDashboardController@updateRefundAmount')->middleware('auth' , 'checkifadmin');
Route::post('/admin/order/delete-normal-order-item', 'Admin\AdminDashboardController@removeItemFromOrderBasket')->middleware('auth' , 'checkifadmin');



Route::get('/admin/business/sales-and-clearance-report', 'Admin\AdminDashboardController@salesAndClearanceReport')->middleware('auth' , 'checkifadmin');

Route::get('/admin/business/sales-and-clearance-report-by-days/{bin}', 'Admin\AdminDashboardController@salesAndClearanceReportByDays')->middleware('auth' , 'checkifadmin');

Route::post('/admin/business/clearance/prepare-clearance-dues-form', 'Admin\AdminDashboardController@salesClearanceForm')->middleware('auth' , 'checkifadmin');
Route::post('/admin/business/clearance/save-sales-clearance', 'Admin\AdminDashboardController@saveSalesClearance')->middleware('auth' , 'checkifadmin');

Route::get('/admin/business/sales-and-clearance-history', 'Admin\AdminDashboardController@salesAndClearanceHistory')->middleware('auth' , 'checkifadmin');
Route::get('/admin/sales-and-clearance-history', 'Admin\AccountsAndBillingController@salesAndClearanceHistory')->middleware('auth', 'checkifadmin');
Route::get('/admin/customer-care/pnd-order/view-details/{orderno}', 'Admin\AdminDashboardController@viewPnDOrderDetails')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/pnd-order/change-to-franchise', 'Admin\AdminDashboardController@changeToFranchise')->middleware('auth' , 'checkifadmin');
Route::post('/admin/order/update-order-zone', 'Admin\AdminDashboardController@updateOrderSourceZone')->middleware('auth' , 'checkifadmin');


Route::get('/admin/customer-care/assist-order/view-details/{orderno}', 'Admin\AdminDashboardController@viewAssistOrderDetails')->middleware('auth' , 'checkifadmin');

Route::post('/admin/pnd-order/update-cc-remarks', 'Admin\AdminDashboardController@updatePnDOrderCCRemarks')->middleware('auth' , 'checkifadmin');
Route::post('/admin/pnd-order/update-payment-mode', 'Admin\AdminDashboardController@updatePnDOrderPaymentUpdate')->middleware('auth' , 'checkifadmin');
Route::post('/admin/pnd-order/update-order-information', 'Admin\AdminDashboardController@updatePnDOrderInformation')->middleware('auth' , 'checkifadmin');

Route::post('/admin/pnd-order/update-order-status', 'Admin\AdminDashboardController@updatePnDOrderStatus')->middleware('auth' , 'checkifadmin');

Route::get('/admin/business/add-premium-business', 'Admin\PopularcategoryController@addPremiumBusiness')->middleware('auth' , 'checkifadmin');
Route::post('/admin/business/save-premium-business', 'Admin\PopularcategoryController@savePremiumBusiness')->middleware('auth' , 'checkifadmin');
Route::post('/admin/business/edit-premium-business', 'Admin\PopularcategoryController@editPremiumBusiness')->middleware('auth' , 'checkifadmin');
Route::post('/admin/business/delete-premium-business', 'Admin\PopularcategoryController@deletePremiumBusiness')->middleware('auth' , 'checkifadmin');
Route::get('/admin/business/view-premium-business-list',
'Admin\PopularcategoryController@viewPremiumBusinessList')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/business/view-premium-business', 'Admin\PopularcategoryController@viewPremiumBusiness')->middleware('auth' , 'checkifadmin');

Route::get('/admin/customer-care/business/view-promotion-businesses', 'Admin\AdminDashboardController@viewPromotionBusinesses')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/add-as-promotion', 'Admin\AdminDashboardController@saveBusinessPromotion')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/remove-from-promotion', 'Admin\AdminDashboardController@deleteFromPromoList')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/update-visibility', 'Admin\AdminDashboardController@updateBusinessVisibility')->middleware('auth' , 'checkifadmin');
Route::get('/admin/delivery-agents', 'Admin\AdminDashboardController@getDeliveryAgents')->middleware('auth' , 'checkifadmin');
Route::get('/admin/delivery-agent/monthly-collection-report',  'Admin\AdminDashboardController@deliveryAgentMonthlyCollectionReport')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/delivery-agents-live-locations', 'Admin\AdminDashboardController@getDeliveryAgentsLocations')->middleware('auth' , 'checkifadmin');
Route::get('/admin/delivery-agent/performance-snapshot', 'Admin\AdminDashboardController@fetchDeliveryAgentsPerformance')->middleware('auth' , 'checkifadmin');



Route::get('/admin/delivery-agent/daily-orders-report', 'Admin\AdminDashboardController@getDeliveryAgentDailyOrdersReport')->middleware('auth' , 'checkifadmin');


Route::get('/admin/delivery-agent/combined-daily-collection-report', 'Admin\AdminDashboardController@allDeliveryAgentsDailyCollectionReport')->middleware('auth' , 'checkifadmin');

Route::get('/admin/delivery-agent/daily-collection-report', 'Admin\AdminDashboardController@deliveryAgentDailyCollectionReport')->middleware('auth' , 'checkifadmin');
Route::get('/admin/orders/update-payment-target', 'Admin\AdminDashboardController@updatePaymentTarget')->middleware('auth' , 'checkifadmin');
Route::get('/admin/delivery-agent/printable-daily-collection-report', 'Admin\AdminDashboardController@printableDeliveryAgentDailyCollectionReport')->middleware('auth' , 'checkifadmin');

Route::match(array('get', 'post'), '/admin/delivery-agent/update-daily-delivery-commission', 'Admin\AdminDashboardController@updateDeliveryAgentDailyCommission')->middleware('auth' , 'checkifadmin');


Route::get('/admin/delivery-agent/monthly-collection-report', 'Admin\AdminDashboardController@deliveryAgentMonthlyCollectionReport')->middleware('auth' , 'checkifadmin');

Route::get('/admin/delivery-agent/save-monthly-collection-report', 'Admin\AdminDashboardController@saveAgentMonthlyCollectionReport')->middleware('auth' );

Route::get('/admin/delivery-agent/save-daily-collection-report',  'Admin\AdminDashboardController@saveAgentDailyCollectionReport')->middleware('auth' , 'checkifadmin');


Route::get('/admin/customer-care/delivery-agents/monthly-report',  'Admin\AdminDashboardController@orderDeliveredByMonth')->middleware('auth' , 'checkifadmin');
Route::get('/admin/delivery-agents/monthly-commission-report',  'Admin\AdminDashboardController@monthlyCommissionReport')->middleware('auth' , 'checkifadmin');
Route::get('/admin/export-agent-commission-report/{year}/{month}',  'Admin\AdminDashboardController@exportCommissionReport')->middleware('auth' , 'checkifadmin');


Route::get('/admin/customer-care/delivery-agents/daily-report/{memberid}', 'Admin\AdminDashboardController@getDailyOrdersReportAgentWise')->middleware('auth' , 'checkifadmin');



Route::get('/admin/customer-care/delivery-agent/get-daily-earning/{aid}', 'Admin\AdminDashboardController@viewAgentDailyEarningReport')->middleware('auth', 'checkifadmin' );
Route::post('/admin/customer-care/delivery-agent/search-daily-earning', 'Admin\AdminDashboardController@searchAgentDailyEarningReport')->middleware('auth', 'checkifadmin' );
Route::post('/admin/customer-care/delivery-agent/save-earning-clearance', 'Admin\AdminDashboardController@saveAgentDailyEarning')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/delivery-agent/get-monthly-earning/{a_id}', 'Admin\AdminDashboardController@viewMonthlyEarningReport')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/delivery-agent/get-monthly-earning', 'Admin\AdminDashboardController@viewMonthlyEarningReport')->middleware('auth' , 'checkifadmin');
Route::get('/admin/systems/prepare-product-import', 'Admin\AdminDashboardController@prepareProductImportExcel')->middleware('auth' , 'checkifadmin');
Route::post('/admin/systems/save-product-import', 'Admin\AdminDashboardController@uploadProductImportExcel')->middleware('auth' , 'checkifadmin');
Route::get('/admin/systems/view-imported-products', 'Admin\AdminDashboardController@viewProductImportExcel')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/products/view-all-products', 'Admin\AdminDashboardController@viewAllProducts')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/products/save-as-feature-product', 'Admin\AdminDashboardController@saveAsFeatureProduct')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/products/view-featured-products', 'Admin\AdminDashboardController@viewFeaturedProducts')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/products/update-todays-deal', 'Admin\AdminDashboardController@updateTodaysDeal')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/business/products/delete-featured-product', 'Admin\AdminDashboardController@deleteFeaturedProduct')->middleware('auth' , 'checkifadmin');



Route::get('/admin/analytics/products/view-frequently-browsed-products', 'Admin\AdminDashboardController@viewFrequentlyBrowsedProducts')->middleware('auth' , 'checkifadmin');
Route::get('/admin/analytics/products/most-viewed-customers', 'Admin\AdminDashboardController@mostViewedCustomers')->middleware('auth' , 'checkifadmin');
Route::get('/admin/analytics/merchants/page-view', 'Admin\AdminDashboardController@merchantPageViews')->middleware('auth' , 'checkifadmin');

Route::get('/admin/customer-care/business/normal-orders/best-performers', 'Admin\AdminDashboardController@viewBestPerformers')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/normal-orders/non-performers', 'Admin\AdminDashboardController@viewLeastPerformers')->middleware('auth', 'checkifadmin' );

Route::get('/admin/customer-care/business/pnd-orders/best-performers', 'Admin\AdminDashboardController@viewPndBestPerformers')->middleware('auth' , 'checkifadmin');
Route::get('/admin/customer-care/business/pnd-orders/non-performers', 'Admin\AdminDashboardController@viewLeastPerformers')->middleware('auth', 'checkifadmin' );
Route::get('/admin/analytics/business/normal-orders-break-down', 'Admin\AdminDashboardController@viewNorderOrderSalesBreakDown')->middleware('auth' , 'checkifadmin');

Route::post('/admin/customer-care/business/normal-orders/non-performers', 'Admin\AdminDashboardController@viewLeastPerformers')->middleware('auth' , 'checkifadmin');

Route::get('/admin/feedback/feedback-from-merchant','Admin\AdminDashboardController@feedbackFromMerchant')->middleware('auth', 'checkifadmin' );

Route::get('/admin/feedback/save-merchant-feedback', 'Admin\AdminDashboardController@saveMerchantFeedback')->middleware('auth', 'checkifadmin' );

//taxation
Route::get('/admin/accounting/prepare-monthly-taxes', 'Admin\AdminDashboardController@prepareMonthlyTaxes')->middleware('auth' , 'checkifadmin');
Route::get('/admin/business/all-order-list-report', 'Admin\AdminDashboardController@allOrderListReport')->middleware('auth' , 'checkifadmin');
Route::get('/export-to-excel/{key}', 'Admin\AdminDashboardController@exportToExcel')->middleware('auth' , 'checkifadmin');
Route::get('/admin/accounting/monthly-sales-report-exporter', 'Admin\AdminDashboardController@monthlySalesReportExporter')->middleware('auth' , 'checkifadmin');
Route::get('/admin/accounting/monthly-taxable-sales-report', 'Admin\AccountsAndBillingController@monthlyTaxableSalesReport')->middleware('auth' , 'checkifadmin');
Route::get('/admin/accounting/order-tax-details', 'Admin\AccountsAndBillingController@orderTaxDetails')->middleware('auth' , 'checkifadmin');
//preparing vouchers
Route::get('/admin/accounting/prepare-voucher-entry', 'Admin\AccountsAndBillingController@prepareVoucherEntry')->middleware('auth' , 'checkifadmin');
Route::post('/admin/accounting/update-voucher-entry', 'Admin\AccountsAndBillingController@updateVoucherEntry')->middleware('auth' , 'checkifadmin');
Route::get('/admin/accounting/view-voucher-details', 'Admin\AccountsAndBillingController@viewVoucherDetails')->middleware('auth' , 'checkifadmin');



Route::get('/monthly-taxes-export-to-excel/{bin}/{month}/{year}', 'Admin\AdminDashboardController@monthlyTaxesExportToExcel')->middleware('auth' , 'checkifadmin');
Route::get('/admin/accounting/export-merchant-taxes', 'Admin\AdminDashboardController@exportMerchantTaxes')->middleware('auth' , 'checkifadmin');
//taxation ends here

Route::match(array('GET', 'POST'), '/admin/accounts/account-ledger-entry', 'Admin\AccountsAndBillingController@dailyAccountLedgerEntry')->middleware('auth' , 'checkifadmin');

//export order delivery agents wise
Route::get('/agents-monthly-orders-export-to-excel/{year}/{month}',  'Admin\AdminDashboardController@agentsMonthlyOrdersExportToExcel')->middleware('auth' , 'checkifadmin');

Route::get('/agents-daily-orders-export-to-excel/{memberid}/{year}/{month}',
	'Admin\AdminDashboardController@agentsDailyOrdersExportToExcel')->middleware('auth' , 'checkifadmin');

//**************Staff Agent Salary page*****************
Route::get('/admin/staff/enter-salary-page', 'Admin\AdminDashboardController@enterSalaryPage')->middleware('auth' , 'checkifadmin');
Route::post('/admin/staff/save-staff-salary-entry', 'Admin\AdminDashboardController@saveStaffSalaryEntry')->middleware('auth' , 'checkifadmin');
Route::get('/admin/staff/view-staff-salary', 'Admin\AdminDashboardController@viewStaffSalary')->middleware('auth' , 'checkifadmin');
Route::get('/admin/staff/e-staff-salary-slip',
'Admin\AdminDashboardController@eStaffSalarySlip')->middleware('auth', 'checkifadmin' );

Route::get('/admin/accounts/add-expenditure', 'Admin\AdminDashboardController@enterDailyExpenses')->middleware('auth', 'checkifadmin' );
Route::post('/admin/accounts/save-expenditure', 'Admin\AdminDashboardController@saveDailyExpenses')->middleware('auth', 'checkifadmin' );
Route::get('/admin/accounts/view-expenditure', 'Admin\AdminDashboardController@viewExpenditureReport')->middleware('auth', 'checkifadmin' );
Route::get('/admin/accounts/view-accounts-dashboard', 'Admin\AdminDashboardController@viewAccountsDashboard')->middleware('auth', 'checkifadmin' );
Route::get('/admin/accounts/view-deposits-details', 'Admin\AdminDashboardController@viewDepositsDetails')->middleware('auth', 'checkifadmin' );
Route::get('/admin/accounts/view-expenses-details', 'Admin\AdminDashboardController@viewExpensesDetails')->middleware('auth', 'checkifadmin' );
Route::get('/admin/accounts/view-advance-details', 'Admin\AdminDashboardController@viewAdvanceDetails')->middleware('auth', 'checkifadmin' );
Route::get('/admin/accounts/view-reimbursement-details','Admin\AdminDashboardController@viewReimbursementDetails')->middleware('auth', 'checkifadmin' );
Route::get('/admin/feedback/feedback-from-merchant','Admin\AdminDashboardController@feedbackFromMerchant')->middleware('auth', 'checkifadmin' );
Route::get('/admin/feedback/save-merchant-feedback','Admin\AdminDashboardController@saveMerchantFeedback')->middleware('auth' , 'checkifadmin' );
Route::get('/daily-sales-and-service-export-to-excel','Admin\AdminDashboardController@DailyOrdersExportToExcel')->middleware('auth' , 'checkifadmin' );



//by sanatomba accounts routes and others routes
Route::get('/admin/agents/agent-advance-amount-entry',
'Admin\AdminDashboardController@agentAdvanceAmountEntry')->middleware('auth','checkifadmin');
Route::post('/admin/agents/add-agent-advance-amount-entry',
'Admin\AdminDashboardController@addAgentAdvanceAmountEntry')->middleware('auth','checkifadmin');
Route::get('/admin/agents/view-agent-advance-history',
'Admin\AdminDashboardController@viewAgentAdvanceHistory')->middleware('auth','checkifadmin');
Route::get('/admin/accounts/view-accounts-dashboard',
'Admin\AdminDashboardController@viewAccountsDashboard')->middleware('auth', 'checkifadmin' );

Route::get('/admin/accounts/view-deposits-details',
'Admin\AdminDashboardController@viewDepositsDetails')->middleware('auth', 'checkifadmin' );

Route::get('/admin/accounts/view-expenses-details',
'Admin\AdminDashboardController@viewExpensesDetails')->middleware('auth', 'checkifadmin' );

Route::get('/admin/accounts/view-advance-details',
'Admin\AdminDashboardController@viewAdvanceDetails')->middleware('auth', 'checkifadmin' );

Route::get('/admin/accounts/view-reimbursement-details',
'Admin\AdminDashboardController@viewReimbursementDetails')->middleware('auth', 'checkifadmin' );

Route::get('/admin/feedback/feedback-from-merchant','Admin\AdminDashboardController@feedbackFromMerchant')->middleware('auth' );
Route::get('/admin/feedback/save-merchant-feedback',
	'Admin\AdminDashboardController@saveMerchantFeedback')->middleware('auth' );
Route::get('/daily-sales-and-service-export-to-excel','Admin\AdminDashboardController@DailyOrdersExportToExcel')->middleware('auth' );



Route::get('/admin/accounts/add-deposit', 'Admin\AdminDashboardController@addCashDeposit')->middleware('auth', 'checkifadmin' );
Route::post('/admin/accounts/save-deposit', 'Admin\AdminDashboardController@saveCashDeposit')->middleware('auth', 'checkifadmin' );
Route::get('/admin/staff/get-booktou-and-staff', 'Admin\AdminDashboardController@getBooktouAndStaff')->middleware('auth', 'checkifadmin' );
Route::get('/admin/staff/e-daily-expense', 'Admin\AdminDashboardController@eDailyExpense')->middleware('auth', 'checkifadmin' );
Route::get('/admin/staff/get-staff-leave-daily-details','Admin\AdminDashboardController@getStaffLeaveDailyDetails')->middleware('auth', 'checkifadmin' );//merchant payment clearance report
Route::get('/admin/payment/view-clearance-report-merchantwise/{paymentid}','Admin\AdminDashboardController@viewClearanceReportMerchantwise')->middleware('auth', 'checkifadmin' );
Route::get('/admin/payment/e-clearance-report/{paymentid}','Admin\AdminDashboardController@eClearanceReport')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer/view-feedback', 'Admin\AdminDashboardController@viewFeedback')->middleware('auth', 'checkifadmin' );
Route::post('/admin/customer/reply-from-bookTou', 'Admin\AdminDashboardController@replyFromBookTou')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer/view-customer-analytics', 'Admin\CustomerController@viewCUstomerAnalytics')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer/view-customer-three-months-back', 'Admin\CustomerController@viewCustomerThreeMonthsBack')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer/customer-three-months-back-to-excel', 'Admin\CustomerController@CustomerThreeMonthsBackToExcel')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer/view-customer-shopping-two-months-back', 'Admin\CustomerController@viewCustomerShoppingTwoMonthsBack')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer/customer-shopping-two-months-back-to-excel', 'Admin\CustomerController@customerShoppingtwoMonthsBackToExcel')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer/view-customer-browse-last-week', 'Admin\CustomerController@viewCustomerBrowseLastWeek')->middleware('auth', 'checkifadmin' );
Route::get('/admin/customer/customer-browse-last-week-to-excel', 'Admin\CustomerController@CustomerBrowseLastWeekToExcel')->middleware('auth', 'checkifadmin' );
//merchant payment clearance report ends here
//displaying franchise order transaction details
Route::get('/admin/view-all-franchise', 'Admin\AdminDashboardController@showAllFranchise')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/show-franchise/franchise-daily-orders-details', 'Admin\AdminDashboardController@franchiseDailyOrdersDetails')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/show-franchise/franchise-monthly-orders-details','Admin\AdminDashboardController@franchiseMonthlyOrdersDetails')->middleware('auth' , 'checkifadmin' );


Route::get('/admin/show-franchise/view-routed-order',
'Admin\AdminDashboardController@viewRoutedOrderDashboard')->middleware('auth' , 'checkifadmin' );

Route::get('/admin/show-franchise/routed-order-details',
'Admin\AdminDashboardController@routedOrderDetails')->middleware('auth' , 'checkifadmin' );

//franchise transaction ends here
//System
Route::get('/admin/system/arrange-icon', 'Admin\AdminDashboardController@arrangeIcon')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/system/update-categories-by-drag', 'Admin\AdminDashboardController@updateCategoriesByDrag')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/manage-cloud-messages', 'Admin\AdminDashboardController@manageCloudMessages')->middleware('auth' , 'checkifadmin' );
Route::match(  array('GET', 'POST'), '/admin/systems/list-business-product', 'Admin\AdminDashboardController@fetchBusinessProductList');

Route::post('/admin/systems/save-cloud-message', 'Admin\AdminDashboardController@saveCloudMessage')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/broadcast-cloud-message', 'Admin\AdminDashboardController@broadcastCloudMessage')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/broadcast-cloud-message-delete', 'Admin\AdminDashboardController@broadcastCloudMessageDelete')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/manage-customers', 'Admin\AdminDashboardController@manageCustomers')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/search-customers', 'Admin\AdminDashboardController@manageCustomers')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/customer/view-complete-profile/{cus_id}', 'Admin\AdminDashboardController@viewCustomerProfile')->middleware('auth' , 'checkifadmin' );

Route::get('/admin/system/arrange-icon',
'Admin\AdminDashboardController@arrangeIcon')->middleware('auth' );

Route::get('/admin/system/update-categories-by-drag',
'Admin\AdminDashboardController@updateCategoriesByDrag')->middleware('auth' );

Route::get('/admin/systems/manage-cloud-messages', 'Admin\AdminDashboardController@manageCloudMessages');
//Route::post('/admin/systems/save-cloud-message', 'Admin\AdminDashboardController@saveCloudMessage');
//Route::post('/admin/systems/broadcast-cloud-message', 'Admin\AdminDashboardController@broadcastCloudMessage');  
Route::get('/admin/systems/manage-customers', 'Admin\AdminDashboardController@manageCustomers')->middleware('auth' ); 
Route::get('/admin/systems/search-customers', 'Admin\AdminDashboardController@manageCustomers')->middleware('auth' );
Route::get('/admin/customer/view-frequent-purchase-customer', 'Admin\AdminDashboardController@viewFrequentPurchaseCustomer')->middleware('auth' );
Route::get('/admin/customer/view-migration-readylist', 'Admin\AdminDashboardController@viewMigrationReadylist')->middleware('auth' );
Route::get('/admin/customer/view-complete-profile/{cus_id}', 'Admin\AdminDashboardController@viewCustomerProfile')->middleware('auth','checkifadmin' );

Route::get('/admin/systems/manage-voucher-codes', 'Admin\AdminDashboardController@manageVouchers')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/save-voucher-code', 'Admin\AdminDashboardController@saveCoupon')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/update-voucher-code', 'Admin\AdminDashboardController@updateCoupon')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/apply-voucher-code', 'Admin\AdminDashboardController@applyCoupon')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/order/remove-coupon-code', 'Admin\AdminDashboardController@removeCouponDiscount')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/save-delivery-charge', 'Admin\AdminDashboardController@saveDeliveryCharge')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/manage-business-categories', 'Admin\AdminDashboardController@mangeBusinessCategories')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/save-business-categories', 'Admin\AdminDashboardController@saveBusinessCategories')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/upload-business-categories-photo', 'Admin\AdminDashboardController@uploadBusinessCategoriesPhoto')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/remove-business-categories', 'Admin\AdminDashboardController@removeBusinessCategories')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/active-users-activity-history', 'Admin\AdminDashboardController@activeUsersActivityHistory')->middleware('auth', 'checkifadmin' );
Route::get('/admin/marketing/campaign-area', 'Admin\AdminDashboardController@viewCampaignAreas')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/marketing/campaign-area/view-audiences', 'Admin\AdminDashboardController@viewCampaignAudiences')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/manage-app-banner', 'Admin\AdminDashboardController@manageAppBanners')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/upload-app-banner', 'Admin\AdminDashboardController@uploadAppBanner')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/disable-app-banner', 'Admin\AdminDashboardController@disableAppBanner')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/delete-app-banner', 'Admin\AdminDashboardController@deleteAppBanner')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/add-qr-menu', 'Admin\AdminDashboardController@addQrMenu')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/upload-qr-menu-item', 'Admin\AdminDashboardController@updateWebMenuImage')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/remove-qr-menu-item', 'Admin\AdminDashboardController@removeQrMenuItem')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/staffs/manage-attendance', 'Admin\AdminDashboardController@manageAttendance')->middleware('auth', 'checkifadmin' );
Route::post('/admin/staffs/save-attendance', 'Admin\AdminDashboardController@saveAttendance')->middleware('auth', 'checkifadmin' );
Route::post('/admin/staffs/remove-attendance', 'Admin\AdminDashboardController@removeAttendance')->middleware('auth', 'checkifadmin' );
Route::post('/admin/staffs/block-user-account', 'Admin\AdminDashboardController@blockUserAccount')->middleware('auth', 'checkifadmin' );
Route::get('/admin/accounting/prepare-daily-collection', 'Admin\AdminDashboardController@prepareDailyCollection')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/accounting/save-daily-collection', 'Admin\AdminDashboardController@saveDailyCollection')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/edit-configuration', 'Admin\AdminDashboardController@editConfiguration')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/save-configuration', 'Admin\AdminDashboardController@saveConfiguration')->middleware('auth' , 'checkifadmin' );

Route::get('/admin/systems/save-section-items', 'Admin\AdminDashboardController@saveSectionItems')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/save-section-items', 'Admin\AdminDashboardController@saveSectionItems')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/delete-section-items', 'Admin\AdminDashboardController@deleteSectionItems')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/product-section_details','Admin\AdminDashboardController@productSectionDetails')
->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/product-section_details',
'Admin\AdminDashboardController@saveSectionDetails')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/edit-product-section_details',
'Admin\AdminDashboardController@editProductSectionDetails')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/delete-product-section_details',
'Admin\AdminDashboardController@deleteProductSectionDetails')->middleware('auth' , 'checkifadmin' );
// app section settings
Route::get('/admin/systems/app-section',
'Admin\AdminDashboardController@appSection')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/save-app-section',
'Admin\AdminDashboardController@saveAppSection')->middleware('auth' , 'checkifadmin' );

Route::get('/admin/systems/add-popular-category',
'Admin\PopularcategoryController@addPopularCategory')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/save-popular-category',
'Admin\PopularcategoryController@savePopularCategory')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/view-submodule-category',
'Admin\PopularcategoryController@viewSubmoduleCategory')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/popular-category-record',
'Admin\PopularcategoryController@popularCategoryRecord')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/update-category',
'Admin\PopularcategoryController@updateCategory')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/delete-category',
'Admin\PopularcategoryController@deleteCategory')->middleware('auth' , 'checkifadmin' );

Route::get('/admin/systems/view-app-section',
'Admin\AdminDashboardController@viewAppSection')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/delete-app-section',
'Admin\AdminDashboardController@deleteSection')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/app-section-details',
'Admin\AdminDashboardController@appSectionDetails')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/systems/save-app-section-details',
'Admin\AdminDashboardController@saveAppSectionDetails')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/view-app-section-details',
'Admin\AdminDashboardController@viewAppSectionDetails')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/systems/delete-app-section-details',
'Admin\AdminDashboardController@deleteSectionDetails')->middleware('auth' , 'checkifadmin' );
/* ****************************************** */
//
//
//   ERP BLOCK
//
//
/* ****************************************** */
Route::get('/erp/login', 'Auth\LoginController@erpLogin');
Route::post('/erp/login', 'Auth\LoginController@erpLogin');

Route::get('/erp', 'Erp\ErpToolsController@erpDashboard')->middleware('auth' );
Route::post('/erp/product-categories/{bin}', 'Erp\ErpToolsController@viewBusinessProductCategory');
Route::post('/erp/product-categories/all-products/{bin}/{category}', 'Erp\ErpToolsController@viewBusinessProducts');
//View Business Product Category
Route::get('/erp/product-categories', 'Erp\ErpToolsController@viewBusinessProductCategory')->middleware('auth' );
Route::get('/erp/product-categories/all-products/{bin}/{category}', 'Erp\ErpToolsController@viewBusinessProducts')->middleware('auth' );
//Delete/Destroy Business products
Route::post('/erp/business/product-delete', 'Erp\ErpToolsController@destroyBusinessProducts')->middleware('auth' );
//Edit Business Product
Route::get('/erp/business/product-edit/{pr_id}', 'Erp\ErpToolsController@editBusinessProducts')->middleware('auth' );
//Edit Business Product save
Route::post('/erp/business/product-update', 'Erp\ErpToolsController@editBusinessProductSave')->middleware('auth' );
//View business earning report for daily
Route::get('/erp/business/get-daily-earning', 'Erp\ErpToolsController@viewBusinessDailyEarning')->middleware('auth' );
//Search business earning report for daily
Route::post('/erp/business/search-daily-earning', 'Erp\ErpToolsController@viewBusinessDailyEarning')->middleware('auth' );
//View business earning report for monthly
Route::get('/erp/business/get-monthly-earning', 'Erp\ErpToolsController@viewBusinessMonthlyEarning')->middleware('auth' );
//Search business earning report for monthly
Route::post('/erp/business/search-monthly-earning', 'Erp\ErpToolsController@viewBusinessMonthlyEarning')->middleware('auth' );
//View new orders
Route::get('/erp/orders/view-all', 'Erp\ErpToolsController@viewNewOrders')->middleware('auth' );
//Search orders
Route::post('/erp/orders/view-new-orders', 'Erp\ErpToolsController@viewNewOrders')->middleware('auth' );
//View order details
Route::get('/erp/orders/view-details/{orderno}', 'Erp\ErpToolsController@viewOrderDetails')->middleware('auth' );
//Update order status
Route::post('/erp/orders/update-status', 'Erp\ErpToolsController@updateOrderStatus')->middleware('auth' );
//Generate pdf file for orders
Route::get('/erp/orders/sales/pdf/{order_no}', 'Erp\PDFController@pdfSalesDetails')->middleware('auth' );
Route::get('/erp/orders/add-new-order', 'Erp\ErpToolsController@addNewOrder')->middleware('auth' );
Route::post('/erp/orders/save-new-sales', 'Erp\ErpToolsController@saveNewOrder')->middleware('auth' );


Route::get('/pos/orders/sales/view-bill', 'Erp\PDFController@downloadReceipt')->middleware('auth' );
Route::get('/pos/orders/view-bill', 'Erp\PDFController@downloadReceipt') ;

Route::get('/admin/pos/merchant-wise-report', 'Admin\POSAdminController@monthlyMerchantWiseReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/pos/monthly-sales-report', 'Admin\POSAdminController@monthlySalesReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/pos/top-selling-product', 'Admin\POSAdminController@topSellingProduct')->middleware('auth' , 'checkifadmin' );
//CMS
Route::get('/admin/cms', 'Admin\CMSController@cmsDashboard')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/cms/menus', 'Admin\CMSController@viewExistingCMSMenu')->middleware('auth' , 'checkifadmin' );
Route::get('/grid/agent-live-position', 'Admin\AdminDashboardController@agentLivePosition')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/report/monthly-earning', 'Admin\AdminDashboardController@monthlyEarningReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/report/pending-payments', 'Admin\AdminDashboardController@pendingPayments')->middleware('auth' , 'checkifadmin' );
//billing and clearance
Route::get('/admin/billing-and-clearance/daily-sales', 'Admin\AccountsAndBillingController@dailySalesReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/billing-and-clearance/weekly-sales-and-services-report', 'Admin\AccountsAndBillingController@weeklySalesAndServiceReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/billing-and-clearance/weekly-report', 'Admin\AccountsAndBillingController@weeklyPaymentDueMerchants')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/billing-and-clearance/monthly-service-report', 'Admin\AccountsAndBillingController@monthlyServiceReport')->middleware('auth' , 'checkifadmin' );
Route::get( '/admin/billing-and-clearance/sales-and-clearance-report', 'Admin\AccountsAndBillingController@salesAndClearanceReport')->middleware('auth' , 'checkifadmin' );
Route::post( '/admin/billing-and-clearance/sales-and-clearance-report', 'Admin\AccountsAndBillingController@salesAndClearanceReport')->middleware('auth' , 'checkifadmin' );
Route::post( '/admin/billing-and-clearance/save-clearance-information', 'Admin\AccountsAndBillingController@saveClearanceInformation')->middleware('auth' , 'checkifadmin' );

Route::get( '/admin/billing-and-clearance/migrated-order-sales-and-clearance-report', 'Admin\AccountsAndBillingController@migratedOrderSalesAndClearanceReport')->middleware('auth' , 'checkifadmin' );


Route::match( array('get', 'post'), '/admin/billing-and-clearance/generate-sales-and-clearance-report', 'Admin\AccountsAndBillingController@salesAndClearanceReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/billing-and-clearance/generate-clearance-bill', 'Admin\AccountsAndBillingController@generateClearanceBill')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/billing-and-clearance/generate-daily-clearance-bill', 'Admin\AccountsAndBillingController@generateDailyClearanceBill')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/report/daily-sales-and-service', 'Admin\AccountsAndBillingController@dailySalesAndServiceReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/report/daily-sales-and-service-for-merchant', 'Admin\AccountsAndBillingController@dailySalesAndServiceMerchantReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/report/daily-sales-and-service-for-merchant', 'Admin\AccountsAndBillingController@dailySalesAndServiceMerchantReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/billing-and-clearance/daily-due-merchantwise', 'Admin\AdminDashboardController@dailyDueMerchantwise')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/report/daily-sales-and-service-merchantwise', 'Admin\AccountsAndBillingController@dailySalesAndServiceMerchantWiseReport')->middleware('auth' , 'checkifadmin' );

Route::get('/admin/add-new-booking','Admin\ServiceBookingController@addServiceBooking')->middleware('auth' , 'checkifadmin' );
Route::post('/save-booking','Admin\ServiceBookingController@saveServiceBooking')->middleware('auth' , 'checkifadmin' );
Route::post('/admin/show-service-product','Admin\ServiceBookingController@showServiceProduct')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/report/daily-sales-report','Admin\ServiceBookingController@dailySalesReport')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/booking-appointment','Admin\ServiceBookingController@bookingAppointment')->middleware('auth' , 'checkifadmin' );
Route::get('/services/view-bookings', 'Admin\ServiceBookingController@manageBooking')->middleware('auth' , 'checkifadmin');
Route::get('/services/view-bookings-offer', 'Admin\ServiceBookingController@viewBookingOffer')->middleware('auth' , 'checkifadmin');
Route::get('/services/view-water-bookings', 'Admin\ServiceBookingController@viewWaterBooking')->middleware('auth' , 'checkifadmin');
Route::get('/services/remove-bookings','Admin\ServiceBookingController@removeBooking')->middleware('auth' , 'checkifadmin' );
Route::get('/admin/business/sales-and-service-performance','Admin\ServiceBookingController@salesAndServicePerformance')->middleware('auth' , 'checkifadmin' );
Route::post('/update-remark','Admin\ServiceBookingController@updateRemark')->middleware('auth' , 'checkifadmin' );
Route::get('/services/view-bookings-by-bin', 'Admin\ServiceBookingController@viewBookingsByBin')->middleware('auth' );
Route::get('/services/get-bookings-by-bin', 'Admin\ServiceBookingController@getBookingsByBin')->middleware('auth' );
Route::get('/services/auto-businees-name', 'Admin\ServiceBookingController@autoBusineesName')->middleware('auth' );
Route::get('/services/remove-bookings-by-bin','Admin\ServiceBookingController@removeBookingsByBin')->middleware('auth' );
Route::post('/services/update-bookings-status','Admin\ServiceBookingController@updateBookingStatus');
Route::post('/services/update-water-bookings-status','Admin\ServiceBookingController@updateWaterBookingStatus');
Route::get('/services/booking/view-details/{orderno}', 'Admin\ServiceBookingController@viewOrderDetails')->middleware('auth' );
Route::post('/services/update-customer-remarks','Admin\ServiceBookingController@updateCustomerRemarks')->middleware('auth');
Route::get('/services/print-receipt', 'Admin\ServiceBookingController@generateServiceBill')->middleware('auth' );
Route::get('/services/download-water-receipt', 'Admin\ServiceBookingController@downloadWaterBookingReceipt')->middleware('auth' );
Route::get('/services/view-merchant-wise-booking', 'Admin\ServiceBookingController@viewMerchantWiseBooking')->middleware('auth' );
Route::post('/services/view-merchant-wise-booking', 'Admin\ServiceBookingController@viewMerchantWiseBooking')->middleware('auth' );
Route::get('/services/sns-business-list', 'Admin\ServiceBookingController@snsBusinessList')->middleware('auth' );
Route::post('/services/sns-category-service-photo', 'Admin\ServiceBookingController@saveServiceCategoryPhoto')->middleware('auth' );
//ba-service booking route ends here
//purchase routing starts from here
Route::get('/purchase/enter-purchase-details','PurchaseController@enterPurchaseDetails')->middleware('auth' );
Route::get('/purchase/show-product','PurchaseController@showlistofProduct')->middleware('auth' );
Route::post('/purchase/update-quantity','PurchaseController@updateQuantity')->middleware('auth' );
Route::post('/save-order-details','PurchaseController@saveOrderDetails')->middleware('auth' );

//purchase ends here



/******************************************
*
*  FRANCHISE BLOCK
*
*
***********************************************/


Route::get('/franchise/login', 'Auth\LoginController@franchiseLogin') ;
Route::post('/franchise/login', 'Auth\LoginController@franchiseLogin') ;
Route::get('/franchise/logout', 'Franchise\FranchiseDashboardControllerV1@logoutFranchise');
Route::get('/franchise',   'Franchise\FranchiseDashboardControllerV1@dashboard')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/view-admin-profile', 'Franchise\FranchiseDashboardControllerV1@viewAdminProfile')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/merchants/view-all',   'Franchise\FranchiseDashboardControllerV1@dashboard')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/businesses/view-all', 'Franchise\FranchiseDashboardControllerV1@viewListedBusinesses')->middleware('auth', 'checkiffranchise'  );
Route::get('/franchise/business/view-profile/{busi_id}','Franchise\FranchiseDashboardControllerV1@viewBusinessProfile')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/business/upload-profile-image','Franchise\FranchiseDashboardControllerV1@uploadBannerImage')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/business/view-products/{bin}', 'Franchise\FranchiseDashboardControllerV1@viewBusinessProducts')->middleware('auth' , 'checkiffranchise');


Route::post('/franchise/business/search-products', 'Franchise\FranchiseDashboardControllerV1@viewBusinessProducts')->middleware('auth' , 'checkiffranchise');

Route::get('/franchise/business/view-product-photo/{bin}/{prsubid}', 'Franchise\FranchiseDashboardControllerV1@viewProductImages')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/business/save-uploaded-photo', 'Franchise\FranchiseDashboardControllerV1@uploadProductImage')->middleware('auth' , 'checkiffranchise');

Route::post('/franchise/business/product/remove-product-photo', 'Franchise\FranchiseDashboardControllerV1@removeProductImage')->middleware('auth' );





Route::get('/franchise/business/sales-and-clearance-history', 'Franchise\FranchiseAccountsAndBillingController@salesAndClearanceHistory')->middleware('auth', 'checkiffranchise');
Route::get('/franchise/sales-and-clearance-history', 'Franchise\FranchiseAccountsAndBillingController@salesAndClearanceHistory')->middleware('auth', 'checkiffranchise');


Route::get('/franchise/payment/view-clearance-report-merchantwise/{bin}/{paymentid}','Franchise\FranchiseAccountsAndBillingController@viewClearanceReportMerchantwise')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/business/sales-and-service-performance','Franchise\FranchiseAccountsAndBillingController@salesAndServicePerformance')->middleware('auth' , 'checkiffranchise' );


Route::get('/franchise/orders/normal-orders', 'Franchise\FranchiseDashboardControllerV1@viewAllOrders')->middleware('auth' , 'checkiffranchise'  );
Route::post('/franchise/orders/normal-orders', 'Franchise\FranchiseDashboardControllerV1@viewAllOrders')->middleware('auth' , 'checkiffranchise'  );
Route::post('/franchise/businesses/update-visibility', 'Franchise\FranchiseDashboardControllerV1@updateBusinessVisibility')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/business/add-as-promotion', 'Franchise\FranchiseDashboardControllerV1@saveBusinessPromotion')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/order/view-details/{orderno}', 'Franchise\FranchiseDashboardControllerV1@viewOrderDetails')->middleware('auth', 'checkiffranchise' );
Route::post('/franchise/order/update-normal-order-price', 'Franchise\FranchiseDashboardControllerV1@updateNormalOrderPrice')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/order/view-information', 'Franchise\FranchiseDashboardControllerV1@redirectToOrderDetailsView')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/orders/view-completed', 'Franchise\FranchiseDashboardControllerV1@viewCompletedOrders')->middleware('auth', 'checkiffranchise' );
Route::post('/franchise/orders/view-completed', 'Franchise\FranchiseDashboardControllerV1@viewCompletedOrders')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/pick-and-drop-orders/view-all', 'Franchise\FranchiseDashboardControllerV1@viewPickAndDropOrders')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/pick-and-drop-orders/view-all', 'Franchise\FranchiseDashboardControllerV1@viewPickAndDropOrders')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/pick-and-drop-orders/view-completed', 'Franchise\FranchiseDashboardControllerV1@viewPickAndDropCompletedOrders')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/pick-and-drop-orders/view-completed', 'Franchise\FranchiseDashboardControllerV1@viewPickAndDropCompletedOrders')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/order/migrate-to-headquarter','Franchise\FranchiseDashboardControllerV1@routeOrderToHeadquarter')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/order/update-order-zone', 'Franchise\FranchiseDashboardControllerV1@updateOrderSourceZone')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/pnd-order/view-details/{orderno}', 'Franchise\FranchiseDashboardControllerV1@viewPnDOrderDetails')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/normal-orders/stats', 'Franchise\FranchiseDashboardControllerV1@normalOrderStatsLog')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/business/normal-orders/best-performers', 'Franchise\FranchiseDashboardControllerV1@viewBestPerformers')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/business/pnd-orders/best-performers', 'Franchise\FranchiseDashboardControllerV1@viewPndBestPerformers')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/staffs/manage-attendance', 'Franchise\FranchiseDashboardControllerV1@manageAttendance')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/staffs/save-attendance', 'Franchise\FranchiseDashboardControllerV1@saveAttendance')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/delivery-agents', 'Franchise\FranchiseDashboardControllerV1@getDeliveryAgents')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/delivery-agents-live-locations', 'Franchise\FranchiseDashboardControllerV1@getDeliveryAgentsLocations')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/delivery-agent/performance-snapshot', 'Franchise\FranchiseDashboardControllerV1@fetchDeliveryAgentsPerformance')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/delivery-agent/all-daily-orders-report',
	'Franchise\FranchiseDashboardControllerV1@allDeliveryAgentDailyOrdersReport')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/delivery-agent/daily-orders-report', 'Franchise\FranchiseDashboardControllerV1@getDeliveryAgentDailyOrdersReport')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/manage-customers', 'Franchise\FranchiseDashboardControllerV1@manageCustomers')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/search-customers', 'Franchise\FranchiseDashboardControllerV1@manageCustomers')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/view-complete-profile/{cus_id}', 'Franchise\FranchiseDashboardControllerV1@viewCustomerProfile')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/analytics/businesses/products/view-frequently-browsed-products', 'Franchise\FranchiseDashboardControllerV1@viewFrequentlyBrowsedProducts')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/report/monthly-earning', 'Franchise\FranchiseDashboardControllerV1@monthlyEarningReport')->middleware('auth' , 'checkiffranchise');
Route::get('/admin/report/monthly-sales-and-service-earning', 'Admin\AccountsAndBillingController@monthlySalesServiceEarningReport')->middleware('auth' );
Route::get('/admin/report/sales-and-service-per-cycle', 'Admin\AccountsAndBillingController@cyclewiseSalesServiceReport')->middleware('auth' );
Route::get('/franchise/report/daily-sales', 'Franchise\FranchiseAccountsAndBillingController@dailySalesReport')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/report/pending-payments', 'Franchise\FranchiseDashboardControllerV1@pendingPayments')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/sytem/config/sales-target','Franchise\FranchiseDashboardControllerV1@salesTarget')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/sytem/config/sales-target','Franchise\FranchiseDashboardControllerV1@salesTarget')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/delivery-agent/monthly-orders-report','Franchise\FranchiseDashboardControllerV1@franchiseOrderDeliveredByMonth')->middleware('auth' );
Route::get('/franchise/active-users-activity-history', 'Franchise\FranchiseDashboardControllerV1@activeUsersActivityHistory')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/delivery-agents/daily-report/{memberid}',
'Franchise\FranchiseDashboardControllerV1@getDailyOrdersReportAgentWise')->middleware('auth', 'checkiffranchise' );
//export order delivery agents wise
Route::get('/franchise/agents-monthly-orders-export-to-excel/{year}/{month}', 'Franchise\FranchiseDashboardControllerV1@franchiseAgentsMonthlyOrdersExportToExcel')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/agents-daily-orders-export-to-excel/{memberid}/{year}/{month}',  'Franchise\FranchiseDashboardControllerV1@franchiseAgentsDailyOrdersExportToExcel')->middleware('auth', 'checkiffranchise' );
//17-08-2021 by sanatomba for franchise pick and drop and assist orders
Route::post('/franchise/customer-care/convert-receipt-to-pick-and-drop-order',
	'Franchise\FranchiseDashboardControllerV1@convertReceiptToPickAndDropRequest')->middleware('auth' , 'checkiffranchise');

Route::post('/franchise/order/update-pnd-remarks',
	'Franchise\FranchiseDashboardControllerV1@updatePnDOrderRemarks')->middleware('checkiffranchise' );
Route::post('/franchise/systems/order/remove-coupon-code', 'Franchise\FranchiseDashboardControllerV1@removeCouponDiscount');
Route::post('/franchise/customer-care/orders/remove-assigned-agent',
	'Franchise\FranchiseDashboardControllerV1@removeAgentFromNormalOrder')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/customer-care/assist-order/view-details/{orderno}',
		'Franchise\FranchiseDashboardControllerV1@viewAssistOrderDetails')->middleware('checkiffranchise' );
Route::post('/franchise/orders/assist/remove-assigned-agent',
	'Franchise\FranchiseDashboardControllerV1@removeAgentFromAssist')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/pnd-order/update-payment-mode',
	'Franchise\FranchiseDashboardControllerV1@updatePnDOrderPaymentUpdate')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/pnd-order/update-order-status',
	'Franchise\FranchiseDashboardControllerV1@updatePnDOrderStatus')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/pick-and-drop-orders/new-request',
	'Franchise\FranchiseDashboardControllerV1@newPickAndDropRequest')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/order/assist/update-information',  'Franchise\FranchiseDashboardControllerV1@updateAssistOrder')->middleware('auth' , 'checkiffranchise');
Route::post('/franchise/orders/pnd-orders/remove-assigned-agent',
	'Franchise\FranchiseDashboardControllerV1@removeAgentFromPnDOrder')->middleware('auth' , 'checkiffranchise');
Route::get('/franchise/report/monthly-sales-and-service-earning', 'Franchise\FranchiseAccountsAndBillingController@monthlySalesServiceEarningReport')
	->middleware('auth','checkiffranchise');
Route::get('/franchise/report/sales-and-service-per-cycle', 'Franchise\FranchiseAccountsAndBillingController@cyclewiseSalesServiceReport')
->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view-accounts-dashboard',
'Franchise\FranchiseAccountsAndBillingController@viewAccountsDashboard')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/add-deposit', 'Franchise\FranchiseAccountsAndBillingController@addCashDeposit')->middleware('auth', 'checkiffranchise' );
Route::post('/franchise/accounts/save-deposit', 'Franchise\FranchiseAccountsAndBillingController@saveCashDeposit')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/add-expenditure', 'Franchise\FranchiseAccountsAndBillingController@enterDailyExpenses')->middleware('auth', 'checkiffranchise' );
Route::post('/franchise/accounts/save-expenditure', 'Franchise\FranchiseAccountsAndBillingController@saveDailyExpenses')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view-expenditure', 'Franchise\FranchiseAccountsAndBillingController@viewExpenditureReport')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view-deposits-details',
'Franchise\FranchiseAccountsAndBillingController@viewDepositsDetails')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view-expenses-details',
'Franchise\FranchiseAccountsAndBillingController@viewExpensesDetails')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view-advance-details',
'Franchise\FranchiseAccountsAndBillingController@viewAdvanceDetails')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view-reimbursement-details',
'Franchise\FranchiseAccountsAndBillingController@viewReimbursementDetails')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view_cash_upi_details',
'Franchise\FranchiseAccountsAndBillingController@viewCashInUPIDetails')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view_extra_deposit_details',
'Franchise\FranchiseAccountsAndBillingController@viewDepositsExtraDetails')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/accounts/view_due_deposit_details',
'Franchise\FranchiseAccountsAndBillingController@viewDepositsDueDetails')->middleware('auth', 'checkiffranchise' );
Route::get('/franchise/report/daily-sales-and-service', 'Franchise\FranchiseAccountsAndBillingController@dailySalesAndServiceReport')->middleware('auth','checkiffranchise' );
Route::get('/franchise/report/daily-sales-and-service-for-merchant',
	'Franchise\FranchiseAccountsAndBillingController@dailySalesAndServiceMerchantReport')->middleware('auth','checkiffranchise' );

Route::get('/franchise/billing-and-clearance/weekly-sales-and-services-report', 'Franchise\FranchiseAccountsAndBillingController@weeklySalesAndServiceReport')->middleware('auth' ,'checkiffranchise');
Route::get('/franchise/billing-and-clearance/daily-sales',
	'Franchise\FranchiseAccountsAndBillingController@dailySalesReport')
->middleware('auth','checkiffranchise' );
Route::get('/franchise/billing-and-clearance/weekly-sales-and-services-report', 'Franchise\FranchiseAccountsAndBillingController@weeklySalesAndServiceReport')->middleware('auth' ,'checkiffranchise');
Route::get( '/franchise/billing-and-clearance/sales-and-clearance-report', 'Franchise\FranchiseAccountsAndBillingController@salesAndClearanceReport')->middleware('auth','checkiffranchise');

Route::get( '/franchise/billing-and-clearance/migrated-order-sales-and-clearance-report', 'Franchise\FranchiseAccountsAndBillingController@migratedOrderSalesAndClearanceReport')->middleware('auth','checkiffranchise');


Route::post( '/franchise/billing-and-clearance/generate-sales-and-clearance-report', 'Franchise\FranchiseAccountsAndBillingController@salesAndClearanceReport')->middleware('auth' , 'checkiffranchise' );
Route::get('/franchise/billing-and-clearance/generate-clearance-bill', 'Franchise\FranchiseAccountsAndBillingController@generateClearanceBill')->middleware('auth' , 'checkiffranchise' );
Route::get('/franchise/billing-and-clearance/generate-daily-clearance-bill', 'Franchise\FranchiseAccountsAndBillingController@generateDailyClearanceBill')->middleware('auth' , 'checkiffranchise' );

Route::get('/franchise/payment/e-clearance-report/{paymentid}',
	'Franchise\FranchiseAccountsAndBillingController@eClearanceReport')->middleware('auth', 'checkiffranchise' );


//theme update section for booktou franchise
Route::get('theme/update-theme','Franchise\FranchiseDashboardControllerV1@updateTheme')
->middleware('auth','checkiffranchise');


Route::get('/sub-menu-generator', 'TestController@subMenuGenerator')->middleware('preloadcmsconfig' );
Route::get('/signup-from-assist', 'StoreFront\StoreFrontController@signupFromAssistChatWindow') ;
Route::get('/reset-password-assist', 'StoreFront\StoreFrontController@resetPasswordAssistChatWindow') ;
Route::post('/generate-refer-code-assist', 'StoreFront\StoreFrontController@joinAndGenerateReferralLinkAssist')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::get('/admin/customer-care/business/services-offered/{bin}', 'Admin\AdminDashboardController@viewServicesOffered')->middleware('auth' , 'checkifadmin');
Route::post('/admin/customer-care/add-photos-service-products',
'Admin\AdminDashboardController@addPhotosServiceProducts')->middleware('auth' , 'checkifadmin' );
Route::get('/payment-pending','StoreFront\StoreFrontController@paymentPending')->middleware('preloadcmsconfig','logshoppingmainmodule');
Route::get('/filter','StoreFront\StoreFrontController@filter')->middleware('preloadcmsconfig','logshoppingmainmodule');

