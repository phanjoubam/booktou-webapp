<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/web/v3/cms/fetch-businesses', 'Api\V3\WebControllerV3@fetchBusinesses');
Route::post('/web/v3/cms/fetch-products', 'Api\V3\WebControllerV3@fetchProducts');
Route::post('/web/v3/cms/fetch-packages', 'Api\V3\WebControllerV3@fetchPackages');
Route::post('/web/v3/cms/add-section-content', 'Api\V3\WebControllerV3@addSectionContent');
Route::post('/admin/ticket/staff-selecting', 'Api\V3\WebControllerV3@ticketStaffSelecting');
Route::post('/admin/ticket/ticket-by-purchase-id', 'Api\V3\WebControllerV3@ticketByPurchaseId');



