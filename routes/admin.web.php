<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Development Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/app-screen/manage-app-screen', 'Admin\AppCmsController@manageAppScreen')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/add-app-screen', 'Admin\AppCmsController@addAppScreen')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/update-app-screen-visibility', 'Admin\AppCmsController@updateAppScreenVisibility')->middleware('auth', 'checkifadmin' );

Route::get('/admin/app-screen/manage-sections', 'Admin\AppCmsController@manageSections')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/add-section', 'Admin\AppCmsController@addSection')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/update-section', 'Admin\AppCmsController@updateSection')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/remove-section', 'Admin\AppCmsController@removeSection')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/update-leading-icon', 'Admin\AppCmsController@updateSectionLeadingIcon')->middleware('auth', 'checkifadmin' );

Route::get('/admin/app-screen/sections/manage-contents', 'Admin\AppCmsController@manageContents')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/sections/add-content', 'Admin\AppCmsController@addContent')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/sections/update-content', 'Admin\AppCmsController@updateContent')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/sections/remove-content', 'Admin\AppCmsController@removeContent')->middleware('auth', 'checkifadmin' );
Route::post('/admin/app-screen/sections/update-image-or-slider', 'Admin\AppCmsController@updateContentSliderImageIcon')->middleware('auth', 'checkifadmin' );

Route::get('/admin/services/business/search', 'Admin\ServiceBookingAppointmentController@searchBusiness')->middleware('auth' , 'checkifadmin');
Route::get('/admin/services/business/package/search', 'Admin\ServiceBookingAppointmentController@searchPackage')->middleware('auth' , 'checkifadmin');
Route::get('/admin/services/business/package/booking-date-and-time-selection', 'Admin\ServiceBookingAppointmentController@bookingDateAndTimeSelection')->middleware('auth' , 'checkifadmin');
Route::get('/admin/services/business/package/review-order-summary', 'Admin\ServiceBookingAppointmentController@reviewOrderSummary')->middleware('auth' , 'checkifadmin');

Route::post('/admin/services/business/package/place-appointment-reservation', 'Admin\ServiceBookingAppointmentController@placeAppointmentReservation')->middleware('auth' , 'checkifadmin');

Route::post('/admin/services/business/package/place-booking-reservation', 'Admin\ServiceBookingAppointmentController@placeBookingReservation')->middleware('auth' , 'checkifadmin');


Route::post('/admin/services/business/package/generate-reservation-payment-link', 'Admin\ServiceBookingAppointmentController@generateReservationPaymentLink')->middleware('auth' , 'checkifadmin');

Route::get('/admin/services/view-oncall-orders', 'Admin\ServiceBookingAppointmentController@viewOnCallOrders')->middleware('auth' , 'checkifadmin');

Route::post('/admin/services/business/reset-cart', 'Admin\ServiceBookingAppointmentController@resetCart')->middleware('auth' , 'checkifadmin');