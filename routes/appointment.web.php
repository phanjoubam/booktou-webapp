<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Development Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//appointment pages
Route::get('/appointment',"Appointment\AppointmentControllerWeb@appointmentIndex")->middleware('preloadcmsconfig' );
Route::get('/appointment/browse-packages/{package_category}',"Appointment\AppointmentControllerWeb@viewPackagesByCategory")->middleware('preloadcmsconfig' );


Route::get('/appointment/business/prepare-service-appointment', 'Appointment\AppointmentControllerWeb@prepareServiceAppointment')->middleware('preloadcmsconfig' );
Route::get('/appointment/business/appointment-calendar', 'Appointment\AppointmentControllerWeb@appointmentCalendar')->middleware('preloadcmsconfig' );


Route::get('/appointment/services/search',"Appointment\AppointmentControllerWeb@searchAppointmentServices")->middleware('preloadcmsconfig' );

Route::get('/appointment/prepare-service-appointment/{bin}/{packageid}', 'Appointment\AppointmentControllerWeb@bookANewService')->middleware('preloadcmsconfig' );

Route::get('/appointment/remove-package-from-cart', 'Appointment\AppointmentControllerWeb@removePackageFromCart')->middleware('preloadcmsconfig' );


Route::get('/appointment/review-appointment-summary', 'Appointment\AppointmentControllerWeb@reviewAppointmentSummary')->middleware('preloadcmsconfig' );

Route::post('/appointment/book-an-appointment','Appointment\AppointmentControllerWeb@makeNewAppointment')->middleware('preloadcmsconfig' );
Route::get('/appointment/payment-in-progress', 'Appointment\AppointmentControllerWeb@appointmentrazorpayPaymentProgress')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');