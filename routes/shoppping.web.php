<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Development Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/shopping', 'StoreFront\StoreFrontController@index')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::get('/shopping/business/{merchant_code}/{category?}', 'StoreFront\StoreFrontController@shopByCategoryUnderBusiness')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::get('/shop/view-product-details/{pname}/{prsubid}', 'StoreFront\StoreFrontController@viewProductDetails')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');



Route::get('/shop/featured-products', 'StoreFront\StoreFrontController@shopFeaturedProducts')->middleware('preloadcmsconfig','logshoppingmainmodule' );




Route::get('/shopping/browse-by-product-category/{menuitemname}', 'StoreFront\StoreFrontController@browseByProductCategory')->middleware('preloadcmsconfig','logshoppingmainmodule' );




//keep this at the bottom
Route::get('/shopping/browse-by-business-category/{bizcategory}', 'StoreFront\StoreFrontController@browseByBusinessCategory')->middleware('preloadcmsconfig','logshoppingmainmodule' );



Route::match(array('get', 'post'),'/shopping/checkout', 'StoreFront\StoreFrontController@orderCheckout')->middleware('preloadcmsconfig','logshoppingmainmodule' );

Route::post('/shopping/place-order', 'StoreFront\StoreFrontController@placeOrder')->middleware('preloadcmsconfig','logshoppingmainmodule' );
Route::post('/shopping/cancel-shopping-order', 'StoreFront\StoreFrontController@cancelShoppingOrder')->middleware('preloadcmsconfig','logshoppingmainmodule' ); 