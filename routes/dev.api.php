<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/v3/web/appointment/check-date-availability','Api\V3\WebApiControllerV3@checkDateAvailability');
Route::post('/v3/web/appointment/check-staff-availability','Api\V3\WebApiControllerV3@checkStaffAvailability');
Route::post('/v3/cloud-message/business-landing', 'Api\V3\SystemControllerV3@fetchBusinessDetails');
Route::post('/v3/customer/business/get-business-promotions','Api\V3\RetailStoreControllerV3@getPaidPromotions');
Route::post('/v3/web/booking/check-date-availabilitys','Api\V3\WebBookingControllerV3@crsCheckBookDateAvailability');





Route::post('/unit-test-jobs-fcm','TestController@sendBizPushMessage');
Route::post('/{version}/delete-user','CustomerApiController@deleteUserByPhoneNumber');
Route::post('/{version}/customer/business/get-all-reviews','CustomerApiController@getAllReviewsForBusiness');
Route::post('/{version}/business/send-group-sms/monthly','BusinessApiController@sendBusinessMarketingSms');
Route::post('/{version}/business/send-group-sms/all','BusinessApiController@sendBusinessMarketingSmsAll');
Route::post('/{version}/user-account/change-password','Api\V3\UserApiControllerV3@changePassword');

Route::post('/{version}/member/register-check','Api\V4\UserApiControllerV4@registerCheck');
Route::post('/{version}/member/login','Api\V4\UserApiControllerV4@login');
Route::post('/{version}/member/set-password','Api\V4\UserApiControllerV4@setPassword');

Route::post('/{version}/business/order-status/total-count', 'Api\V3\BusinessServiceControllerV3@OrderStatusCount');


Route::post('/{version}/member/update-primary-address','Api\V4\UserApiControllerV4@updatePrimaryAddress');
//chan
Route::post('/v4/merchant-complaints-forms','Api\V4\MerchantComplaintsController@merchantComplaintsForms');

Route::post('/{version}/merchant-complaints-otp-generation','Api\V4\MerchantComplaintsControllerV4@merchantComplaintsOTPGeneration');