<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/v3/web/shopping/add-item-to-cart', 'Api\V3\WebControllerV3@addItemToCart')->middleware('cors' );
Route::post('/v3/shopping/show-chat-result', 'Api\V3\ChatLoginControllerV3@showChatResult');
Route::post('/v3/shopping/show-assist', 'Api\V3\ChatLoginControllerV3@loadShowAssist');
Route::post('/v3/web/shopping/show-assist-steps', 'Api\V3\WebControllerV3@showAssistSteps');
 
//slots route for booking
Route::post('/v3/web/appointment/business/get-day-slots', 'Api\V3\WebBookingControllerV3@getDaySlots');
Route::post('/v3/web/business/get-staffs-availability','Api\V3\WebBookingControllerV3@getStaffAvailability');
Route::post('/v3/web/customer/business/get-staffs-availability-in-a-slot','Api\V3\WebBookingControllerV3@getStaffAvailabilityForSlot');
// paa web api
Route::post('/v3/web/customer/get-available-days','Api\V3\WebBookingControllerV3@getAvailableDays');
Route::post('/v3/web/booking/check-date-availability','Api\V3\WebBookingControllerV3@checkBookingDateAvailability');
Route::post('/v3/web/booking/check-date-availabilitys','Api\V3\WebBookingControllerV3@crsCheckBookDateAvailability');
Route::post('/v3/web/business/view-service-days','Api\V3\WebBookingControllerV3@getServiceDays');
// paa web api ends here
Route::post('/v3/shopping/show-assist', 'Api\V3\ChatLoginControllerV3@loadShowAssist');
Route::post('/v3/web/appointment/check-date-availability','Api\V3\WebBookingControllerV3@checkAppointmentDateAvailability');
Route::post('/v3/web/business/view-service-days','Api\V3\WebBookingControllerV3@getServiceDaysAppointment');
Route::post('/v3/web/appointment/fetch-available-slots-and-staff','Api\V3\WebBookingControllerV3@fetchAvailableSlotsAndStaff');
Route::post('/v3/web/business/toggle-sponsored-service', 'Api\V3\WebControllerV3@toggleSponsoredService');
Route::post('/v3/web/booking/toggle-booking-show', 'Api\V3\WebControllerV3@toggleBookingShow');
Route::post('/v3/web/customer/business/get-day-slots', 'Api\V3\WebBookingControllerV3@getDaySlots');
//chanchan
Route::post('/v3/web/shopping/show-count-item-to-cart', 'Api\V3\WebControllerV3@showCountItemToCart')->middleware('cors' );
