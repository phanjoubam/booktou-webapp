<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/admin/business/event-management/prepare-ticket-generation', 'Admin\EventmManagementController@prepareTicketGeneration')->middleware('auth', 'checkifadmin' );
Route::post('/admin/business/event-management/generate-ticket-qrcodes', 'Admin\EventmManagementController@generateTicketQrcodes')->middleware('auth', 'checkifadmin' );


Route::get('/admin/business/qr-generator', 'Admin\EventmManagementController@urlToQrGenerator')->middleware('auth', 'checkifadmin' );



//ticketing system
Route::get('/events/ticketing/free-pass-registration', 'EventTicketingController@freePassRegistration')->middleware( 'preloadcmsconfig');
Route::get('/events/ticketing/ticket-sale-form', 'EventTicketingController@ticketSaleForm')->middleware( 'preloadcmsconfig');
Route::post('/events/ticketing/make-ticket-purchase', 'EventTicketingController@makeTicketPurchase')->middleware( 'preloadcmsconfig');
Route::get('/events/ticketing/payment-in-progress', 'EventTicketingController@paymentInProgress')->middleware( 'preloadcmsconfig');
Route::post('/events/ticketing/payment-completed', 'EventTicketingController@paymentCompleted')->middleware( 'preloadcmsconfig');
Route::get('/events/ticketing/view-ticket', 'EventTicketingController@viewTicket')->middleware( 'preloadcmsconfig');
Route::get('/events/ticketing/no-ticket-found', 'EventTicketingController@noTicketFound')->middleware( 'preloadcmsconfig');
Route::get('/events/ticketing/send-whatsapp-message', 'EventTicketingController@sendWhatsappMessage')->middleware( 'preloadcmsconfig');
Route::get('/events/ticketing/download-ticket', 'EventTicketingController@downloadTicket');



Route::get('/events/ticketing/send-whatsapp-event-invite', 'EventTicketingController@sendWhatsappInvitationMessage')->middleware( 'preloadcmsconfig');



Route::get('/events/ticketing/phone-verification', 'EventTicketingController@phoneVerification')->middleware( 'preloadcmsconfig');
Route::post('/events/ticketing/otp-verification', 'EventTicketingController@otpVerification')->middleware('preloadcmsconfig');
Route::get('/events/ticketing/ticket-purchases', 'EventTicketingController@ticketPurchaseList')->middleware('preloadcmsconfig');