<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::match(  array('GET', 'POST'),  '/admin/accounts/update-missing-order', 'Admin\AccountsAndBillingController@updateMissingOrder')->middleware('auth', 'checkifadmin' );

Route::match(  array('GET', 'POST'),  '/admin/accounts/search-missing-order', 'Admin\AccountsAndBillingController@searchErrorOrder')->middleware('auth', 'checkifadmin' );


 Route::match( array('GET', 'POST'), '/maps/reverse-geocode','TestController@reverseGeoCode');

 //fcm token update for web users
 Route::post('/fcm/update-user-token','Web\WebApiController@updateWebFcmToken');


Route::match(array('GET', 'POST'),  '/order/payment-summary','OrderPaymentController@processOrderPaymentStatus');
Route::get( '/page-not-found','HomeController@pageNotFound');
Route::get( '/page-invalid','HomeController@pageInvalid');
