<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Development Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//booking pages 
Route::get('/booking',"Booking\BookingController@bookingIndex")->middleware('preloadcmsconfig' ); 
Route::get('/booking/browse-packages/{category}',"Booking\BookingController@viewPackagesUnderCategory")->middleware('preloadcmsconfig' );
Route::get('/booking/services/search',"Booking\BookingController@searchBookingServices")->middleware('preloadcmsconfig' );
Route::get('/booking/business/prepare-booking/{bin}/{srv_code?}', 'Booking\BookingController@bookANewService')->middleware('preloadcmsconfig' );
Route::match(array('GET', 'POST'),'/booking/customer/crs/place-booking','Booking\BookingController@makeCrsBooking')->middleware('auth' ,'preloadcmsconfig' );

Route::get('/booking/business/view-packages/{categoryname}',"Booking\BookingController@viewPackagesByCategory")->middleware('preloadcmsconfig');


Route::get('/booking/business/prepare-service-booking', 'Booking\BookingController@prepareServiceBooking')->middleware('preloadcmsconfig' );
Route::get('/booking/business/booking-calendar', 'Booking\BookingController@bookingCalendar')->middleware('preloadcmsconfig' );
Route::get('/booking/business/review-order-summary', 'Booking\BookingController@reviewOrderSummary')->middleware('preloadcmsconfig' );
Route::post('/booking/business/place-booking-order', 'Booking\BookingController@placeBookingOrder')->middleware('preloadcmsconfig' );
Route::get('/booking/business/remove-package-from-cart', 'Booking\BookingController@removePackageFromCart')->middleware('preloadcmsconfig' );
Route::get('/booking/business/payment-in-progress', 'Booking\BookingController@bookingrazorpayPaymentInProgress')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');

Route::match(array('get', 'post'),'/booking/order/payment-completed', 'Booking\BookingController@bookingrazorpayPaymentCompleted')->middleware('preloadcmsconfig' ,'logshoppingmainmodule');


Route::get('/booking/view-order-details', 'Booking\BookingController@myBookingOrderDetails')->middleware('preloadcmsconfig','logshoppingmainmodule'  );

Route::post('/booking/cancel-booking-order', 'Booking\BookingController@cancelBookingOrder')->middleware('preloadcmsconfig','logshoppingmainmodule'  );

