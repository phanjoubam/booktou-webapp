<!DOCTYPE html>
<html lang="en">
<head>
<title>Water App</title>
<meta charset="utf-8"> 

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="{{ URL::to('/public/assets/css/sb-admin-2.css')}}" rel="stylesheet">
<link href="{{ URL::to('/public/assets/css/style.css')}}" rel="stylesheet">
</head>
<body>
  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5 mt-9">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Set New Password</h1>
                  </div>


                  
                  <form class="user" method='post' action="{{ URL::to('/member/password-reset') }}">
                    {{  csrf_field() }}

 @if(Session("error"))
 
 <p class='alert alert-info'>{{ Session("error") }}</p>
 
@endif

  @if(Session("success"))
 
 <p class='alert alert-info'>{{ Session("success") }}</p>
 
@endif
                    <div class="form-group">
                      <input type="email" name="email" class="form-control form-control-user"   placeholder="Enter Email Address...">
                    </div>
                    <div class="form-group">
                      <input type="password" name="newPassword" class="form-control form-control-user"   placeholder="New Password">
                    </div>
                    <div class="form-group">
                      <input type="password" name="confirmNewPassword" class="form-control form-control-user"   placeholder="Confirm New Password">
                    </div>

                     <button type="submit" class="btn btn-primary btn-user btn-block">Submit</button>
                     <button type="submit" class="btn btn-danger btn-user btn-block">Cancel</button>
                  </form>
                  <hr>
                  <div class="text-center">
                     <a class="small" href="{{URL::to('/user/register')}}">Create an Account!</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="{{URL::to('/login')}}">Already have an account? Login!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</body>
</html>
