@extends('layouts.light_theme')
@section('pagebody')

<section class='container'>
<div class='row'>
  <div class='col-md-12'>
    <h1>Order</h1>

@if(Session("err_msg"))
 
 <p class='alert alert-info'>{{ Session("err_msg") }}</p>
 
@endif
     
        <form method="post" action="{{ action('OrderController@orderSave') }}" enctype="multipart/form-data">
              {{  csrf_field() }}

                <div class="form-row">
                  <div class="form-group col-md-4">
                   <label for="inputEmail4">Item</label>
                    <select  type="text" class="form-control"  name="item_type">
                      <option value="1">Jar</option>
                      <option value="2">Bottle</option>
                    </select>
                  </div>
               

	            
                <div class="form-group col-md-4">
                 <label for="inputEmail4">Quantity</label>
                 <input type="text" class="form-control" name="quantity" placeholder="Quantity">
                </div>
	              <div class="form-group col-md-4">
                 <label for="inputEmail4">Capacity</label>
                 <input type="text" class="form-control" name="capacity" placeholder="Capacity">
                </div> 
	            </div>
	
	            <div class="form-row">     
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Order Date</label>
                 <input type="date" class="form-control" name="order_date">
                </div>
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Service Date</label>
                 <input type="date" class="form-control" name="service_date">
                </div>
	            </div>
	
	            <div class="form-row">
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Locality</label>
                 <input type="text" class="form-control" name="delivery_location" placeholder="Locality">
                </div>
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Landmark</label>
                 <input type="text" class="form-control" name="delivery_landmark" placeholder="Landmark">
                </div> 
	            </div>
	
	            <div class="form-row">
                <div class="form-group col-md-6">
                 <label for="inputEmail4">City</label>
                 <input type="text" class="form-control" name="delivery_city" placeholder="City">
                </div>
                <div class="form-group col-md-6">
                 <label for="inputAddress">State</label>
                 <input type="text" class="form-control" name="delivery_state" placeholder=" State ">
                </div>
                </div> 

                <div class="form-row">
                <div class="form-group col-md-6">
                 <label for="inputAddress2">Pin</label>
                 <input type="number" class="form-control" name="delivery_pin" placeholder="Pin">
                </div>  
                <div class="form-group col-md-6">
                 <label for="inputCity">Phone No.</label>
                 <input type="text" class="form-control" name="delivery_phone" placeholder="Phone No.">
                </div> 
	            </div>
              
                @foreach ($data['order_for'] as $order_for)
                <?php
                              $appl_id= Crypt::encrypt($order_for->id);
                              
                ?>
                
                <input type="hidden" value="{{$appl_id}}" name='encmid'  />

                @endforeach
                 @foreach ($data['order_by'] as $order_by)
                <?php
                              $RetailerId= Crypt::encrypt($order_by->RetailerId);
                              
                ?>
                
                <input type="hidden" value="{{$RetailerId}}" name='order_by'  />

                @endforeach

               

             <button type="submit" value='save' name='btnSave' class="btn btn-primary">Order</button>
             <button type="submit" class="btn btn-danger">Cancel</button>
       </form>
    </div>
   
  </div>
 
</section>
@endsection

