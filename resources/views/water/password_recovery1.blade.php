<!DOCTYPE html>
<html lang="en">
<head>
<title>Water App</title>
<meta charset="utf-8"> 

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="{{ URL::to('/public/assets/css/sb-admin-2.min.css')}}" rel="stylesheet">
</head>
<body>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                    <p class="mb-4">Just enter your email address below and we'll send you a link to reset your password!</p>
                  </div>
                  <form class="user" method='post' action="{{ URL::to('/member/password-confirm') }}">
                    {{  csrf_field() }}

  @if(Session("error"))
 
 <p class='alert alert-info'>{{ Session("error") }}</p>
 
@endif
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" name="email" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">Reset Password</button>
                     <button type="submit" class="btn btn-danger btn-user btn-block">Cancel</button>
                  </form>
                  <hr>
                  <div class="text-center">
                     <a class="small" href="{{URL::to('/user/register')}}">Create an Account!</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="{{URL::to('/login')}}">Already have an account? Login!</a>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</body>
</html>