@extends('layouts.light_theme1')
@section('pagebody')

<section class='container'>
<div class='row'>
  <div class='col-md-12'>
    <h1>Order Status</h1>

    @foreach ($data['orders'] as $item)
        <form  method="post" action="{{  URL::to('/status/updated') }}" >
            {{  csrf_field() }}
                <div class="form-row">    
                <div class="form-group col-md-8">
                 <label>Order Id</label>
                 <input readonly="text" class="form-control" value='{{$item->id}}' name="order_id">
                </div>
              </div>
              <div class="form-row"> 
                <div class="form-group col-md-4">
                 <label>Total</label>
                 <input type="text" class="form-control"  name="total">
                </div>
	              <div class="form-group col-md-4">
                 <label>Order Status</label>
                    <select  type="text" class="form-control"  name="status">
                      <option value="Processed">Processed</option>
                      <option value="Shipped">Shipped</option> 
                      <option value="Dispatched">Dispatched</option>
                      <option value="Delivered">Delivered</option>
                    </select>
                </div> 
	            </div>
             <?php  $mem_id = Crypt::encrypt( $item->id );?> 

                <input type="hidden" value="{{$mem_id}}" name='encmid'  />

             <button type="submit" value='update' name='btnUpdate' class="btn btn-primary">Update</button>
             <button type="submit" class="btn btn-danger">Cancel</button>
       </form>
    </div>
   @endforeach
  </div>
</section>
@endsection
