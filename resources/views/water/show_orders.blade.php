@extends('layouts.light_theme')
@section('pagebody')

 <div class="container-fluid">
 	
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Order List</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
 

        <thead>
		<tr>
	    <th>Sl.No.</th>
		<th>Order By</th>
		<th>Item</th>
		<th>Quantity</th>
		<th>Capacity</th>
		<th>Order Date</th>
		<th>Service Date</th>
		<th>Delivery Location</th>
		<th>Delivery Landmark</th>
		<th>Delivery City</th>
		<th>Delivery State</th>
		<th>Delivery Pin</th>
		<th>Delivery Phone</th>
		<th>Action</th>
		</tr>
		</thead>
		<?php $i = 0 ?>
		@foreach ($data['orders'] as $item)
		<?php $i++ ?>
		 <tbody>
		 <tr>
		 <td>{{$i}}</td>
		 <td>{{ $item->customerName}}</td>
		 <td>
		 	<?php 
		 	if($item->itemType==1){
		 		echo "Jar";
		 	}
		 	else{
		 		echo "Bottle";
		 	}
		 	?>
		 </td>
		 <td>{{ $item->quantity}}</td>
		 <td>{{ $item->capacity}}</td>
		 <td>{{ $item->orderDate}}</td>
		 <td>{{ $item->serviceDate}}</td>
		 <td>{{ $item->deliveryLocation}}</td>
		 <td>{{ $item->deliveryLandmark}}</td>
		 <td><?php echo $item->deliveryCity ; ?></td>
		 <td>{{ $item->deliveryState}}</td>
        <td>{{ $item->deliveryPin}}</td>
        <td>{{ $item->deliveryPhone}}</td>
        <td>
        	<?php  $o_id = Crypt::encrypt( $item->id );?>
        	
        	<?php 
		 	if($item->orderStatus==2){ 
		 		?>
		 		<a href="{{URL::to('/track-order/'.$o_id)}}" class='btn btn-success btn-sm'>On Route</a>

		 	<?php }
		 	else if($item->orderStatus==10){ ?>
		 	
		 		<a href="" class='btn btn-danger btn-sm'>Cancel</a>

		 		<?php }
		 	else if($item->orderStatus==11){ ?>
		 	
		 		<a href="" class='btn btn-info btn-sm'>Serve</a>
		 	<?php }
		 		else{ ?>
        	<a href="" class='btn btn-warning btn-sm'>Pending</a>
        	<?php 	 		
		 	} ?></td>
		 </tr>
		@endforeach
	</tbody>
		</table>

		{{ $data['orders']->links() }}

<!--<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-end">
    <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#">Next</a>
    </li>
  </ul>
</nav> -->

		</div>
		</div>
		</div>
	</div>
 
@endsection


