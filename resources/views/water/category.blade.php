@extends('layouts.light_theme')
@section('pagebody')

<section class='container'>
<div class='row'>
  <div class='col-md-12'>
    <h1>Category Registration</h1>


@if(Session("err_msg"))
 
<p class='alert alert-info'>{{ Session("err_msg") }}</p>
 
@endif
 
     
        <form method="post" action="{{ URL::to('/save-category') }}">
            {{csrf_field()}}
              <div class="form-row">
                <div class="form-group col-md-4">
                 <label>Category Name</label>
                 <input type="text" class="form-control" name="name" placeholder="Category Name">
                </div>
                <div class="form-group col-md-4">
                 <label>Category Id</label>
                 <input type="text" class="form-control" name="cat_id" placeholder="Category Id">
                </div>
               </div>
                 

             <button type="submit" value='save' name='btnSave' class="btn btn-primary">Save</button>
             <button type="submit" class="btn btn-danger">Cancel</button>
       </form>
    </div>
   
  </div>
 
</section>
@endsection

