@extends('layouts.light_theme')
@section('pagebody')


  <div class="container">
    
      <div class="col-sm-12">
        <div class="row">
    
    @foreach ($data['retailers'] as $item)

    
 <div class="col-sm-4">
     <div class="card-body">
    <div class="card" style="width: 20rem;">
      <?php  $ret_id = Crypt::encrypt( $item->id );?>
<a href="{{URL::to('/order/'.$ret_id)}}" >
    <img src="http://waterpp.ezanvel.in/public/uploads/businesslogo/{{ $item->photo}}" class="card-img-top" alt="..." height="200px" width="30px">


    
    <h4 class="box-part text-center">{{ $item->busi_name}}</h4>
    <h6 class="box-part text-center">{{ $item->busi_address}}</h6>
    
  <hr>
      <div class='row'>
      <div class='col-md-12 ml-l'>
      <strong>Retailer Name: </strong>{{ $item->fname}} {{ $item->mname}} {{ $item->lname}}<br/> 
      <strong>Phone: </strong>{{ $item->phone_1}}<br/> 
      <strong>Email: </strong>{{$item->email}}<br/> 
    <strong>Service: </strong>{{ $item->busi_description}}<br/> 
    <strong>Area Cover: </strong>{{ $item->busi_area}}
</div>
</div>

   </a> 

     </div>
    </div>
  </div>




  @endforeach
  </div>
  </div>
  </div>


@endsection