<!DOCTYPE html>
<html lang="en">
<head>
<title>Water App</title>
<meta charset="utf-8"> 

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="{{ URL::to('/public/assets/css/sb-admin-2.css')}}" rel="stylesheet">
<link href="{{ URL::to('/public/assets/css/style.css')}}" rel="stylesheet">
</head>
<body>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5 mt-7" >
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
              
@if(Session("err_msg"))
 
 <p class='alert alert-info'>{{ Session("err_msg") }}</p>
 
@endif

              <form class="user" method='post' action="{{ action('WaterappController@registerMember') }}">
                      {{  csrf_field() }}
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="username" id="exampleFirstName" placeholder="User Name">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user"  name="email" id="exampleInputEmail" placeholder="Email Address">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control form-control-user"  name="password" id="exampleInputPassword" placeholder="Password">
                  </div>

                  
                     <div class="form-group">
                    <select  type="text" class="form-control"  name="catid" id="catid">
                    @foreach ($data as $cat)
                      <option value="{{$cat->Category}}">{{$cat->Name}}</option>
                      @endforeach
                    </select>
                  </div>


                <button type="submit" class="btn btn-primary btn-user btn-block">Register Account</button>
              </form>
              <hr>
              <div class="text-center">
               <a class="small" href="{{URL::to('/member/password-recovery1')}}">Forgot Password?</a>
              </div>
              <div class="text-center">
                <a class="small" href="{{URL::to('/login')}}">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</body>
</html>
