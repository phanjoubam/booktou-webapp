@extends('layouts.light_theme1')
@section('pagebody')
<section class='container'>
<div class='row'>
  <div class='col-md-12'>
    <h1>Business Profile</h1>
 

@if(Session("err_msg"))
 
 <p class='alert alert-info'>{{ Session("err_msg") }}</p>
 
@endif
 




    <form method="post" action="{{  URL::to('/retailer/profile/update') }}" enctype="multipart/form-data">
              {{  csrf_field() }}
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail">Business Name</label>
                 <input type="text" class="form-control" name="busi_name" placeholder="Business Name" required>
              </div>
      	      <div class="form-group col-md-6">
                <label for="inputEmail" >Licence Number</label>
                 <input type="text" class="form-control" name="lic_number" placeholder="Licence Number">
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputPassword3">Address</label>
                 <input type="tex" class="form-control" name="busi_address" placeholder="Address">
               </div>
              <div class="form-group col-md-6">
               <label for="inputPassword3">Landmark</label>
                <input type="text" class="form-control" name="busi_landmark" placeholder="Landmark">
              </div>
             </div>

            <div class="form-row">
             <div class="form-group col-md-4">
               <label for="inputPassword3"> City</label>
                <input type="text" class="form-control" name="busi_city" placeholder="City" >
               </div>
              <div class="form-group col-md-4">
                <label for="inputPassword3">State</label>
                 <input type="text" class="form-control" name="busi_state" placeholder="State">
                </div>
               <div class="form-group col-md-4">
                 <label for="inputPassword3"> Pin</label>
                  <input type="number" class="form-control" name="busi_pin" placeholder="Pin">
               </div>
             </div>

             <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputPassword3">Description</label>
                   <input type="textarea" class="form-control" name="busi_description" placeholder="Description">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputPassword3">Delivered Cover Area</label>
                    <input type="text" class="form-control" name="busi_area" placeholder="Area Cover">
                 </div>
              </div>
                     <div class="form-group col-md-4" >
            <label class='control-label'>Upload Logo:</label>  
            Choose a file to upload: <input name="photo" type="file" /><br />
        
            </div>
    


             <?php $mid= Session("_active_member_id_") ?>

           <?php  $mem_id = Crypt::encrypt( $mid );?>

    <input type="hidden" value="{{ $mem_id }}" name='encmid'  />


      <button type="submit" value='update' name='btnUpdate' class="btn btn-primary">Register</button>
       <button type="submit" class="btn btn-danger">Cancel</button>
    </div>
 </div>
</form>
 </div>
    </div>
  </section>

@endsection
