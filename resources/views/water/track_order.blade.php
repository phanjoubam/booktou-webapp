@extends('layouts.light_theme')
@section('pagebody')

 <div class="container-fluid">

  <div class='row'>
  <div class='col-md-12'>


 	 <div class='row mt-2'>	
 	 <div class='col-md-12'>   
     @foreach ($data['track'] as $item)
	<div class="row text center alert alert-info">
		<div class="col-md-4"><h4>Order No:{{ $item->order_id}}</h4></div>
		<div class="col-md-4"><h4>Total:{{ $item->total}}</h4></div>
		<div class="col-md-4"><h4>Status:<mark>{{ $item->status}}</mark></h4></div>
	</div>

       @endforeach
     
	   @if($item->status=="Ordered")
	   @include('water.steps.ordered')

	   @elseif($item->status=="Processed")
	   @include('water.steps.processed')

	    @elseif($item->status=="Shipped")
	   @include('water.steps.shipped')

	    @elseif($item->status=="Dispatched")
	   @include('water.steps.dispatched')

	    @elseif($item->status=="Delivered")
	   @include('water.steps.delivered')
       
       @endif

</div>
</div>

<div class='row'>
<div class='col-md-6 mt-3'>
 	
<div class="card shadow mb-4">
		
		
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Order Details</h6>
            </div>
       
            <div class="card-body">
		<p>

       @foreach ($data['orders'] as $item)
		
		 <strong>Order By: </strong>{{ $item->customerName}}<br/>
		 <strong>Item: </strong>	<?php 
		 	if($item->itemType==1){
		 		echo "Jar";
		 	}
		 	else{
		 		echo "Bottle";
		 	}
		 	?><br/>
		 <strong>Quantity: </strong>{{ $item->quantity}} <br/>
		 <strong>Capacity: </strong>{{ $item->capacity}}<br/>
          <strong>Order Date: </strong>{{ $item->orderDate}}<br>
          <strong>Service Date: </strong>{{ $item->serviceDate}}<br>
          
		</p>
		</div>
		
     </div>
		
		</div>

		<div class='col-md-6 mt-3'>
 	
<div class="card shadow mb-4">
		
		
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Delivery Address</h6>
            </div>
       
            <div class="card-body">
		<p>

       
		
		 <td>{{ $item->deliveryLocation}}</td><br/>
		 <td>{{ $item->deliveryLandmark}}</td> <br/>
		 <td>{{ $item->deliveryCity}}</td><br/>
          <td>{{ $item->deliveryState}}</td><br>
          <td>{{ $item->deliveryPin}}</td><br>
          <td>{{ $item->deliveryPhone}}</td><br>
          
		</p>
		</div>
		
     </div>
      
		 @endforeach
	
		
		</div>

	</div>
	<div class='col-md-6'>
	</div>
</div> 
 
 </div>
 </div> 
  
 
@endsection