@extends('layouts.light_themes')
@section('pagebody')

<div class="container-fluid">
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Order List</h6>
            </div>
        
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
     <thead>
		<tr>
		<th>Sl.No.</th>
		<th>Order For</th>
		<th>Item</th>
		<th>Quantity</th>
		<th>Capacity</th>
		<th>Order Date</th>
		<th>Service Date</th>
		<th>Delivery Location</th>
		<th>Delivery Landmark</th>
		<th>Delivery City</th>
		<th>Delivery State</th>
		<th>Delivery Pin</th>
		<th>Delivery Phone</th>
		<th>Action</th>
		</tr>
	</thead>
		<?php $i = 0 ?>
		@foreach ($data['orders'] as $item)
		<?php $i++ ?>
		 <tbody>
		 <tr>
		 <td>{{$i}}</td>
		 <td>{{ $item->customerName}}</td>
		 <td>
		 	<?php 
		 	if($item->itemType==1){
		 		echo "Jar";
		 	}
		 	else{
		 		echo "Bottle";
		 	}
		 	?>
		 </td>
		 <td>{{ $item->quantity}}</td>
		 <td>{{ $item->capacity}}</td>
		 <td>{{ $item->orderDate}}</td>
		 <td>{{ $item->serviceDate}}</td>
		 <td>{{ $item->deliveryLocation}}</td>
		 <td>{{ $item->deliveryLandmark}}</td>
		 <td><?php echo $item->deliveryCity ; ?></td>
		 <td>{{ $item->deliveryState}}</td>
        <td>{{ $item->deliveryPin}}</td>
        <td>{{ $item->deliveryPhone}}</td>
        
        <td><?php  $o_id = Crypt::encrypt( $item->id );?>
        <?php 
		 	if($item->orderStatus==1){ ?>
        	<button data-ono ="{{ $o_id }}" class='btn btn-primary btn-sm btnModal003'>Accept</button>
        	<?php 	 		
		 	}
		 	else if($item->orderStatus==2){ ?>
        	<a href="" class='btn btn-success btn-sm'>Taken</a>
		 	<?php
		 	 }
		 	else{ 
		 		?>
		 		<a href="" class='btn btn-warning btn-sm'>Pending</a>
		 		<?php } ?></td>

		 </tr>
		@endforeach
	</tbody>
		</table>

		{{ $data['orders']->links() }}

		</div>
		</div>
		</div>
	</div>
 
<!--modal block starts --> 
 <div class="modal fade" id="dialog003" tabindex="-1" role="dialog" >
 <form method='post' action="{{  URL::to('/order-accepted') }}">
 	{{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm Order Accept</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>
	        This is final step to confirm order. No rollback possible.<br/>
	        Are you sure about this operation? 
    	</p>
      </div>
      <div class="modal-footer">
      	<input type='hidden' name='hon' id='hon'/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</form>
</div> 
<!--modal block ends  -->


@endsection