@extends('layouts.light_theme')
@section('pagebody')

 <div class="container-fluid">
 	
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Retailer List</h6>
            </div>
        
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
         <thead>
		<tr>
		<th>Sl.No.</th>
		<th>Business Name</th>
		<th>Lic. No.</th>
		<th>Addreess</th>
		<th>Landmark</th>
		<th>City</th>
		<th>State</th>
		<th>Pin</th>
		<th>Description</th>
		<th>Area Cover</th>
		<th>Action</th>
		</tr>
	</thead>
		 <?php $i = 0 ?>
		@foreach ($data['retailers'] as $item)
		<?php $i++ ?>
		<tbody>
		 <tr>
		 <td>{{$i}}</td>
		 <td>{{ $item->busi_name}}</td>
		 <td>{{ $item->lic_number}}</td>
		 <td>{{ $item->busi_address}}</td>
		 <td>{{ $item->busi_landmark}}</td>
		 <td>{{ $item->busi_city}}</td>
		 <td>{{ $item->busi_state}}</td>
		 <td>{{ $item->busi_pin}}</td>
		 <td>{{ $item->busi_description}}</td>
		 <td><?php echo $item->busi_area ; ?></td>

		 <td>
		 	<?php  $ret_id = Crypt::encrypt( $item->id );?>
		 	<a href="{{URL::to('order/'.$ret_id)}}" class='btn btn-primary btn-sm'>Order Now</a>
		 </td>
		 </tr>
		@endforeach
	</tbody>
		</table>
		</div>
		</div>
		</div>
	</div>
 
@endsection


