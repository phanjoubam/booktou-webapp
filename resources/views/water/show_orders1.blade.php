@extends('layouts.light_theme1')
@section('pagebody')

 <div class="container-fluid">
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Order List</h6>
            </div>
          <form method="get" action="{{action('OrderController@searchOrder')}}" enctype="multipart/form-data">
         	 {{  csrf_field() }}
             <div class="card-body">
               <select  type="search" class="card-text"  name="keyword">
                      <option value="1">Jar</option>
                      <option value="2">Bottle</option>
                    </select>
            <input type="search" name="keyword1" class="card-text" placeholder="Enter Delivery Location" >
                 <button name="btnSearch" type="submit" value='search' class="btn btn-primary">Search</button>
              </div>
         </form>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
     <thead>
		<tr>
		<th>Sl.No.</th>
		<th>Order For</th>
		<th>Item</th>
		<th>Quantity</th>
		<th>Capacity</th>
		<th>Order Date</th>
		<th>Service Date</th>
		<th>Delivery Location</th>
		<th>Delivery Landmark</th>
		<th>Delivery City</th>
		<th>Delivery State</th>
		<th>Delivery Pin</th>
		<th>Delivery Phone</th>
		<th>Action</th>
		</tr>
	</thead>
		<?php $i = 0 ?>
		@foreach ($data['orders'] as $item)
		<?php $i++ ?>
		 <tbody>
		 <tr>
		 <td>{{$i}}</td>
		 <td>{{ $item->customerName}}</td>
		 <td>
		 	<?php 
		 	if($item->itemType==1){
		 		echo "Jar";
		 	}
		 	else{
		 		echo "Bottle";
		 	}
		 	?>
		 </td>
		 <td>{{ $item->quantity}}</td>
		 <td>{{ $item->capacity}}</td>
		 <td>{{ $item->orderDate}}</td>
		 <td>{{ $item->serviceDate}}</td>
		 <td>{{ $item->deliveryLocation}}</td>
		 <td>{{ $item->deliveryLandmark}}</td>
		 <td><?php echo $item->deliveryCity ; ?></td>
		 <td>{{ $item->deliveryState}}</td>
        <td>{{ $item->deliveryPin}}</td>
        <td>{{ $item->deliveryPhone}}</td>
         <td><?php  $o_id = Crypt::encrypt( $item->id );?>
        	<a href="{{URL::to('/track-order1/'.$o_id)}}" class='btn btn-primary btn-sm'>Track Order</a>
        	<a href="{{URL::to('/order-status/'.$o_id)}}" class='btn btn-success btn-sm'>Update Status</a>
        	<?php 
		 	if($item->orderStatus==0){ ?>
        	<a href="{{URL::to('/order-approved/'.$o_id)}}" class='btn btn-secondary btn-sm'>Approved</a>
        	<?php 	 		
		 	}
		 	else if($item->orderStatus==10){ ?>
        	<a href="{{URL::to('/order-approved/'.$o_id)}}" class='btn btn-secondary btn-sm'>Approved</a>
        	<?php 	 		
		 	}
		 	else{ 
		 		?>
		 		<a href="{{URL::to('/order-cancel/'.$o_id)}}" class='btn btn-danger btn-sm'>Cancel</a>
		 	<?php } ?>
		 </tr>
		 @endforeach
	</tbody>

		</table>

{{ $data['orders']->links() }}
		</div>
		</div>
		</div>
	</div>


@endsection


