<!DOCTYPE html>
<html lang="en">
<head>
<title>Water App</title>
<meta charset="utf-8"> 

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<section class='container'>
 <div class='row'>
  <div class='col-md-12'>
    <h1>Password Recovery Wizard</h1>
     
		 
	 <form>

      <div class="form-group">
       <div class="form-group col-md-6">
        <label for="inputEmail4">An email is sent to your registered mail.
        Please input the OTP key receive in the mail.</label>
         <label for="inputEmail4">OTP:</label>
        <input id="codeBox" type="password" maxlength="6" placeholder="Enter OTP" />
        </div>
       </div>
	
       
       <button type="submit" class="btn btn-primary">Verify</button>
       <button type="submit" class="btn btn-primary">Resend</button>
       
      </div>
  
   </form>
  </div>
 </div>
</section>

</body>
</html>