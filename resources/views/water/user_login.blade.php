<!DOCTYPE html>
<html lang="en">
<head>
<title>Water App</title>
<meta charset="utf-8"> 

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<section class='container'>
 <div class='row'>
  <div class='col-md-12'>
    <h1>Log In</h1>
     
		 
	 <form method='post' action="{{ action('WaterappController@userSave') }}" >
				{{  csrf_field() }}


      <div class="form-group">
       <div class="form-group col-md-6">
        <label for="inputEmail4">User Name</label>
	      <div class="col-sm-10">
        <input type="text" class="form-control" name="username" placeholder="User Name">
        </div>
       </div>
	
       <div class="form-group col-md-6">
        <label for="inputPassword3">Password</label>
        <div class="col-sm-10">
        <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
       </div>
  
       <button type="submit" class="btn btn-primary">Login</button>
       <button type="submit" class="btn btn-danger">Cancel</button>
      </div>
  </div>
   </form>
       <div class="text-center">
        <a class="small" href="">Forgot Password?</a>
       </div>
       <div class="text-center">
        <a class="small" href="">Create an Account!</a>
       </div>
  </div>
 </div>
</section>

</body>
</html>