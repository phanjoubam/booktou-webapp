@extends('layouts.light_theme1')
@section('pagebody')

<section class='container'>
<div class='row'>
  <div class='col-md-12'>
    <h1>Profile Update</h1>


@if(Session("err_msg"))
 
<p class='alert alert-info'>{{ Session("err_msg") }}</p>
 
@endif
 
     
        <form method="post" action="{{ action('WaterappController@updateMember1') }}" enctype="multipart/form-data" >
              {{  csrf_field() }}
              <div class="form-row">
                <div class="form-group col-md-4">
                 <label for="inputEmail4">First Name</label>
                 <input type="text" class="form-control" name="fname" placeholder="First Name">
                </div>
                <div class="form-group col-md-4">
                 <label for="inputEmail4">Middle Name</label>
                 <input type="text" class="form-control" name="mname" placeholder="Middle Name">
                </div>
                <div class="form-group col-md-4">
                 <label for="inputEmail4">Last Name</label>
                 <input type="text" class="form-control" name="lname" placeholder="Last Name">
                </div>
	            </div>

	            <div class="form-row">
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Father</label>
                 <input type="text" class="form-control" name="father" placeholder="Father">
                </div>
	              <div class="form-group col-md-6">
                 <label for="inputEmail4">Mother</label>
                 <input type="text" class="form-control" name="mother" placeholder="Mother">
                </div> 
	            </div>
	
	            <div class="form-row">
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Gender</label><br>
                  <input type="radio" name="gender" value="0" checked> Male 
                  <input type="radio" name="gender" value="1" checked> Female 
                  <input type="radio" name="gender" value="2" checked> Other 
               </div>        
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Date of Birth</label>
                 <input type="date" class="form-control" name="dob">
                </div>
	            </div>
	
	            <div class="form-row">
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Locality</label>
                 <input type="text" class="form-control" name="locality" placeholder="Locality">
                </div>
                <div class="form-group col-md-6">
                 <label for="inputEmail4">Landmark</label>
                 <input type="text" class="form-control" name="landmark" placeholder="Landmark">
                </div> 
	            </div>
	
	            <div class="form-row">
                <div class="form-group col-md-4">
                 <label for="inputEmail4">City</label>
                 <input type="text" class="form-control" name="city" placeholder="City">
                </div>
                <div class="form-group col-md-4">
                 <label for="inputAddress">State</label>
                 <input type="text" class="form-control" name="state" placeholder=" State ">
                </div> 
                <div class="form-group col-md-4">
                 <label for="inputAddress2">Pin</label>
                 <input type="text" class="form-control" name="pin" placeholder="Pin">
                </div>  
              </div>
  
              <div class="form-row">
                <div class="form-group col-md-6">
                 <label for="inputCity">Phone No.</label>
                 <input type="text" class="form-control" name="phone_1" placeholder="Phone No.">
                </div> 
                <div class="form-group col-md-6">
                 <label for="inputCity">Alternate Ph.no.</label>
                 <input type="text" class="form-control" name="phone_2" placeholder="Alternate Ph.no.">
                </div>
	            </div>
	   <div class="form-group col-md-4" >
            <label class='control-label'>Upload Photo:</label>  
            Choose a file to upload: <input name="photo" type="file" /><br />
        
            </div>

             <button type="submit" value='save' name='btnSave' class="btn btn-primary">Save</button>
             <button type="submit" class="btn btn-danger">Cancel</button>
       </form>
    </div>
   
  </div>
 
</section>
@endsection

