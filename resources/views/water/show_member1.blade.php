@extends('layouts.light_theme1')
@section('pagebody')

 <div class="container-fluid">

  <div class='container'> 
  <div class='row'>
  <div class='col-md-8'>
 	
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Member Profile</h6>
            </div>
        
            <div class="card-body">
        
	
		@foreach ($data['members'] as $item)
		<p>

	<div class='row'>
      <div class='col-md-6'>
       
		<strong>Name: </strong>{{ $item->fname}} {{ $item->mname}} {{ $item->lname}}<br/>
		 <strong>Gender: </strong><?php 
		 	if($item->gender==0){
		 		echo "Male";
		 	}
		 	elseif($item->gender==1){
		 		echo "Female";
		 	}
		 	else{
		 		echo "Other";
		 	}
		 	?><br/>
		 <strong>Date of Birth: </strong>{{ $item->dob}}<br/>
		 <strong>Locality: </strong>{{ $item->locality}}<br/>
		 <strong>Landmark: </strong>{{ $item->landmark}}<br/>
		 <strong>Phone: </strong><?php echo $item->phone_1 ; ?><br/>
          <strong>City: </strong>{{ $item->city}}<br>
          <strong>State: </strong>{{ $item->state}}<br>
          <strong>Pin: </strong>{{ $item->pin}}<br>
          
		 
		
		</div>
		<div class='col-md-6'>
           <div class="img-profile">
           <img src='http://waterpp.ezanvel.in/public/uploads/retailer/{{  Session::get("_active_photo_")}}' class="card-img-top" alt="..." height="200px" width="30px">
       </div>
         </div>
     </div>
      </p>
     <hr/>
     <a href="{{URL::to('/profile-update1')}}" class='btn btn-primary btn-sm'>Edit</a>&nbsp;
     <a href="" class='btn btn-danger btn-sm'>Delete</a>
        

		@endforeach
	
		
		</div>
	</div>
</div>
</div>
 
@endsection