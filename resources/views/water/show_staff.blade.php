@extends('layouts.light_theme1')
@section('pagebody')

 <div class="container-fluid">
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Order List</h6>
            </div>
          <form method="get" action="{{action('RetailerController@searchStaff')}}" enctype="multipart/form-data">
         	 {{  csrf_field() }}
             <div class="card-body">
              
            <input type="search" name="keyword" class="card-text" placeholder="Enter Staff Name" >
                 <button name="btnSearch" type="submit" value='search' class="btn btn-primary">Search</button>
              </div>
         </form>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
     <thead>
		<tr>
		<th>Sl.No.</th>
		<th>Name</th>
		<th>Address</th>
		<th>Phone No.</th>
		<th>Alt. Phone No.</th>
		<th>Email</th>
		<th>Action</th>
		</tr>
	</thead>
		<?php $i = 0 ?>
		@foreach ($data['staffs'] as $item)
		<?php $i++ ?>
		 <tbody>
		 <tr>
		 <td>{{$i}}</td>
		 <td>{{ $item->fname}} {{ $item->mname}} {{ $item->lname}}</td>
		 <td>{{$item->locality}} {{$item->landmark}}, {{$item->city}}</td>
		 <td>{{ $item->phone_1}}</td>
		 <td>{{ $item->phone_2}}</td>
		 <td>{{ $item->email}}</td>
         <td>
        	<a href="" class='btn btn-primary btn-sm'>Button</a>
        	<a href="" class='btn btn-success btn-sm'>Button</a>
        </td>
		 </tr>
		 @endforeach
	</tbody>

		</table>

{{ $data['staffs']->links() }}
		</div>
		</div>
		</div>
	</div>


@endsection


