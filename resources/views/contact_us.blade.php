@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
   


?> 
 <div class='outer-top-tm'>
    <div class="container">
 
  
   
<div class="row"> 
 

    <div class="col-xs-12 col-sm-12 col-md-6 offset-md-1">

<div class="card card-body">
    <h3 class="title  ">Contact Us</h3> 
    <hr/>

      @if (session('err_msg'))
        <div class="alert alert-info">
                {{ session('err_msg') }}
      </div>
     @endif 
    <form method='post' id='contact-form' action="{{ action('StoreFront\StoreFrontController@saveContactForm') }}">
      @csrf 
        <div class="form-row mt-3">
          <div class="col-12">
            <input class="form-control" name="name" placeholder="Your Name" type="text">
        </div> 
       
        </div> 

        <div class="form-row mt-3">
          <div class="col-4">
            <input class="form-control" name="email" placeholder="Your email" type="email">
        </div> 
        <div class="col-4"> 
              <input class="form-control" name="phone" placeholder="Phone number" type="text"> 
        </div> 
        </div> 

       <div class="form-row mt-3">
          <div class="col-md-12">
            <textarea class="form-control" name="message" rows='5' placeholder="Your email"  ></textarea>
        </div> 
        
        </div> 
 

       <div class="form-row">
         <div class="col-12">
          <br/> 
            <button class="btn btn-warning " type='submit' name='btnsend' value='send'  >Submit</button>
        </div>
      </div>
    </form><br/>
    <small>We respect your privacy and do not tolerate spam and will never share your information (name, address, email, etc.) to any third party.</small>
</div>

    </div> <!-- col // -->

  
   <div class="col-xs-12 col-sm-12 col-md-4">
<div class="card card-body">
<h3 class="title  ">Office Address</h3>   
Ezanvel Solutions Private Ltd.</br>
Minuthong-Khuyathong Road,</br>
Opposite Kekrupat</br>
Imphal West</br>
795001<br/>

Email: <a href='mailto:booktougi@gmail.com' target='_blank'>booktougi@gmail.com</a><br/>
Sales &amp; Support: +91-8787306375<br/>
Merchant Support: +91-9863086093<br/>

</p>

 </div>
</div>
</div>
  
</div>  
 
 

</div> 
 <div class="mt-5">
    </div>
@endsection 



@section('script')

 
@endsection 