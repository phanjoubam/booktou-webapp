@extends('layouts.franchise_theme_03')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Listed Business</div> 
                </div>


                    <div class="col-md-4 text-right"> 
        <form class="form-inline" method="get" action="{{ action('Franchise\FranchiseDashboardControllerV1@viewListedBusinesses') }}" enctype="multipart/form-data">
              {{  csrf_field() }}
      
     <div class="form-group">
    <input type="search" class="form-control form-control-sm  my-1 mr-sm-4" name="search_key" id="input-search" placeholder="Enter name or tags or city">
    </div>

     <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
      </form>
      </div>

       </div>
                </div>
            </div>
       <div class="card-body">   
<div class="table-responsive table-with-menu">
    <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
		<th scope="col"></th>
		<th scope="col">Name and No.</th>  
		<th scope="col">Phone</th>
   <th scope="col">Visibility</th>
    <th scope="col">Is Blocked</th>
    <th scope="col">Is Open</th>
     <th scope="col">Promotion Status</th>

		<th scope="col">Action</th>
		</tr>
	</thead>
	
		<tbody>

			 <?php $i = 0 ?>
		@foreach ($data['results'] as $item)
		<?php 
    $i++  ;
    $found = false;

    foreach($data['promo_businesses'] as $plist)
    {
      if($plist->bin == $item->id ) 
                              {
                                $found= true;break;
                              }
                            }
                          
     ?>


		 <tr  >
		 <td>
     <?php 
      if($item->banner=="")
      { 
         $banner = $cdn_url . "/assets/image/no-image.jpg" ; 
      }
      else
      {
        $banner = $cdn_url . $item->banner   ;  
      }
      
     ?>

      <img src='{{ $banner }}' alt="{{$item->name}}" height="60px" width="60px"> 
                </td>
		 <td>{{$item->name}} ( Bin: {{  $item->id   }} ) <span class='badge badge-primary'>{{ $item->category}}</span>
      <br/>
      {{$item->locality}}, <br/>{{$item->landmark}}, <br/>{{$item->city}}, {{$item->state}}-{{$item->pin}}
      <br/>
     @if( $item->latitude != 0 and $item->latitude != null && 
    $item->longitude != 0 and $item->longitude != null   )

    <a href='https://www.google.com/maps?q={{$item->latitude}},{{$item->longitude}}' target='_blank' class='btn btn-success btn-xs'>View Location</a>

    @endif 
  </td>
		 <td>{{$item->phone_pri}}</td>

 <td>
    @if($item->limit_by_distance == "yes")
      <span class='badge badge-primary badge-pill'>Nearby Only</span>
    @else 
      <span class='badge badge-warning badge-pill'>Everywhere</span>
    @endif 
    </td>

<td>
        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input btnToggleBlock"   data-id='{{$item->id }}' id="blockSwitchON{{ $item->id }}" 
            <?php if($item->is_block == "yes") echo "checked";  ?>  />
            <label class="custom-control-label" for="blockSwitchON{{$item->id }}">Block</label>
          </div> 
    </td>



    <td>
        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input btnToggleClose"   data-id='{{$item->id }}' id="openSwitchON{{ $item->id }}" 
            <?php if($item->is_open == "open") echo "checked";  ?>  />
            <label class="custom-control-label" for="openSwitchON{{$item->id }}">Open</label>
          </div> 
    </td>
 
      <td>
 @if(  $found )
  <span class='badge badge-primary'>PREMIUM</span>
@else 
<span class='badge badge-warning'>NORMAL</span>
 @endif

      </td>


		 <td class="text-center">

                       
                     <div class="dropdown">
  

                     <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a>



  <ul class="dropdown-menu dropdown-user pull-right"> 
    <li><a class="dropdown-item" href="{{ URL::to('/franchise/business/view-profile') }}/{{$item->id}}">Business Profile</a></li>
    <li><button class="dropdown-item showwgt02"  data-bin="{{$item->id}}">Change Visibility</button></li>
    <li><a class="dropdown-item" href="{{URL::to('/franchise/business/view-products')}}/{{$item->id}}">Products</a></li>
  
    <li><a class="dropdown-item" href="{{ URL::to('/franchise/business/sales-and-clearance-history') }}?bin={{$item->id}}">Monthly Clearance History</a></li>
    <li><a class="dropdown-item" href="{{ URL::to('/franchise/business/sales-and-service-performance') }}?bin={{$item->id}}">Monthly Performance</a></li>
    @if( !$found )
      <li><a href='#' class="dropdown-item btn_add_promo"  data-bin="{{$item->id }}"  >Promote Business</a></li>
    @endif

    @foreach($data['qr_menu'] as $qrlist)
      @if($qrlist->bin == $item->id)
        <li><a target='_blank' href="{{ URL::to('/shopping/browse-menu') }}?mcode={{ $item->merchant_code}}" class="dropdown-item "    >QR Menu</a></li>
         @break
      @endif 
    @endforeach
     
                     
  </ul>
         </div>          
        </td>
		 </tr>
	@endforeach
	</tbody>
		</table>
	

  {{ $data['results']->links() }}
  
		
	</div>
 </div>


  </div> 
	
	</div> 
</div>

 


<div class="modal fade" id="modal_add_promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <form method='post' action="{{ action('Franchise\FranchiseDashboardControllerV1@saveBusinessPromotion') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          This is final step to add promo list.  
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='bin' id='bin'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>

<div class="modal fade" id="wgt02" tabindex="-1" role="dialog" aria-labelledby="wgt02" aria-hidden="true">
       <form method='post' action="{{ action('Franchise\FranchiseDashboardControllerV1@updateBusinessVisibility') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="wgt02">Update Business Visibility</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="form-group">
        <label for="formGroupExampleInput">Visibility:</label>
        <select   class="form-control"  name='dbvisibility'>
            <option value='yes'>Limit by distance</option> 
            <option value='no'>Do not limit by distance</option> 
        </select>
      </div>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='bin' id='key02'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Save</button>
      </div>
    </div>
  </div>
 </form>
</div>



@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 
$(document).on("click", ".showwgt02", function()
{
  $("#key02").val($(this).attr("data-bin"));
  $("#wgt02").modal("show");
});



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 


</script>


@endsection

