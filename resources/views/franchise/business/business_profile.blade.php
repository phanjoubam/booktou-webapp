@extends('layouts.franchise_theme_03')
@section('content')
<?php 
  $cdn_url =    config('app.app_cdn') ;  
  $cdn_path =     config('app.app_cdn_path') ;  
  $business = $data['business']; 
?>

<div class="row">

          <div class="col-lg-6">
            <div class="card  "> 
              <div class="card-header">  
                <h4 class="card-title text-center">{{ $business->name}}</h4> 
              </div>
             <div class="card-body">

              <div class="row"> 
                <div class='col-md-4' >
                  <img src="{{ $cdn_url . $business->banner  }}"  class="align-self-start mr-3" width='200px' alt="{{$business->name}}"> 
              <hr/>

                     <button type="button" class="btn btn-primary btnchangebanner">Change Banner</button>
              
                </div>
                 <div class='col-md-8' >
                  <h5 class="mt-0">Contact Information</h5>
                  
                    {{$business->locality}}, {{$business->landmark}}, {{$business->city}}, {{$business->state}}-{{$business->pin}}</br>
                    <strong>Primary Phone :</strong> {{$business->phone_pri}}</br>
                    <strong>Alternate Phone :</strong> {{$business->phone_alt}}</br>
                    <strong>Is Open :</strong> {{$business->is_open}} 
                </div>
              </div>
</div>

</div>


        
          </div>


   

         <div class="col-md-6">

           <div class="card card-chart">
              <div class="card-header"> 
                <h4 class="card-title text-center">Commission Percentage</h4>
                 
              </div>
             <div class="card-body text-center">
              
                <div class="jumbotron">
  <h1 class="display-4">{{$business->commission}} %</h1> 
</div>   
          </div>


        </div>

              </div> 


       
       
         <div class="col-lg-12 mt-3">
          <div class="card card-chart">  
              <div class="card-header"> 
                <h4 class="card-title ">Item Details</h4>
              </div>
<div class="card-body"> 
  <form class="form-inline" method="get" action="">

            {{ csrf_field() }}
              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Item Category:</label> 
              <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='search_key'>
                 @foreach ($data['category'] as $item)
                    <option value='{{$item->category_name}}'>{{$item->category_name}}</option> 
                 @endforeach
                </select>

                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>
 
 <table class="table"> 
  <thead class=" text-primary">
    
   <tr class="text-center">
      <th scope="col">Sl.No.</th>
    <th scope="col">Item Name</th>
    <th scope="col">Initial Stock</th>
    <th scope="col">Stock in Hand</th>
    <th scope="col">Max Order</th>
    <th scope="col">Unit Name</th>
    <th scope="col">Unit Price</th>
    <th scope="col">Item Image</th>
    </tr>
    </thead>

    <tbody>
     <tr class="text-center">
     <td></td>
     <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
  
    </tr>
   </tbody>

  </table> 

  </div>

   
</div>    
  
   </div> 

   </div>
     

<form action="{{ action('Franchise\FranchiseDashboardControllerV1@uploadBannerImage') }}" method="post"  enctype="multipart/form-data" >
    {{ csrf_field() }}
   <div class="modal fade" id="modfileupload" tabindex="-1" role="dialog" aria-labelledby="del_box" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="del_box"><strong>Upload Banner Image</strong></h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body modal_msg">
       <div class="input-group">
  <div class="custom-file">
    <input type="file" name='photo' class="custom-file-input" id="fileselector" aria-describedby="btnupload">
    <label class="custom-file-label" for="fileselector">Choose file</label>
  </div>
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="submit" name='btnupload' value='upload' id="btnupload">Upload</button>
  </div>
</div> <input type="hidden" name='key' value="{{ $data['bin'] }}" >
         </div>
        
       </div>
     </div>
   </div>

 </form>




 
@endsection
 

@section("script") 

<script> 


$(document).on("click", ".btnchangebanner", function()
{ 
    $('#modfileupload').modal('show');
})
 


 

 
 
</script> 


@endsection




 
  
 