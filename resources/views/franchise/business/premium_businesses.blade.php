@extends('layouts.admin_theme_02')
@section('content')

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">

   <div class="col-md-8">
    <h3>Premium Businesses</h3>
   </div>
    <div class="col-md-4 text-right">
 
        <form class="form-inline" method="get" action="{{ action('Admin\AdminDashboardController@viewListedBusinesses') }}" enctype="multipart/form-data">
              {{  csrf_field() }}
      
     <div class="form-group">
    <input type="search" class="form-control form-control-sm  my-1 mr-sm-2 " name="search_key" id="input-search" placeholder="Enter name or tags or city">
    </div>

     <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
      </form>
      </div>
  </div>    
 </div>
        </div>    
           

  <div class="card-body"> 

 @if (session('err_msg'))
           
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
          
              @endif


 <div class="table-responsive">
 <table class="table"> 
  <thread class="thead-dark">
      <tr class="thead">
     
        <th>Store Name </th>
        <th>Category</th>  
        <th width='200px'>Address</th> 
        <th>Rating</th>
        <th>Is Open</th>
        <th>Action</th>
      </tr>
    </thread>
<tbody>

              <?php $i = 0 ?>
                    @foreach ($data['businesses'] as $item)

             <?php $i++ ?>

                  <tr >
                      
                    <td>
                  {{$item->name}}
                  </td>
                    <td>
                   {{$item->category}}
                  </td>  
                    <td width='200px'>
                {{$item->locality}},{{$item->landmark}},{{$item->city}},{{$item->state}},{{$item->pin}}
                  </td>

             

                    <td>
                {{$item->rating}}
                  </td>

                    <td>
                {{$item->is_open}}
                  </td>

                    <td width='200px'>  

                      <button class="btn btn-danger btn-sm btn-conf-del"  data-bin="{{$item->id }}"  >Stop Promotion</button> 
 
 </td>
         </tr>
                 
                 @endforeach

                </tbody>
</table>
</div>
</div>

  
  </div>


</div>

</div>





 
<!--Delete Modal -->
<div class="modal fade" id="modalConfDel" tabindex="-1" role="dialog"  aria-hidden="true">
        <form method='post' action="{{ action('Admin\AdminDashboardController@deleteFromPromoList') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"  >Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div>
        <p class="text-center">
          This is final step to remove a business from promo list. You can add the business to promotion list later on.
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='bin' id='hidbin'/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="submit" name="btnDelete" value="delete" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>

@endsection
@section("script")
 
<script>
 


  $('.btn-conf-del').on('click', function()
  { 
      var bin  = $(this).attr('data-bin'); 
      $("#hidbin").val( bin );  
     $("#modalConfDel").modal('show');

  
});




</script> 

@endsection 
