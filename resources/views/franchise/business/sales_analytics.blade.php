@extends('layouts.franchise_theme_03')
@section('content')
 
<?php 
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; // cnd - /var/www/html/api/public

?>

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">
         <div class="col-md-7">
           <h5>Monthly Sales Analytics for <span class='badge badge-primary'>{{  $data['business']->name }}</span></h5>
         </div>

         <div class="col-md-5 text-right">
           <form class="form-inline" method="post" action="{{  action('Admin\AdminDashboardController@searchBusinessProducts') }}">
            {{ csrf_field() }}
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Category:</label> 
            <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='search_key'>
            
            </select>
            <input type="hidden" value="{{ $data['business']->id  }}" name="bin">
            <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
          </form>  
        </div> 
      </div> 


                </div>
            </div>
       <div class="card-body"> 
  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif
  <div class="table-responsive">
  <table class="table"> 
    <thead class=" text-primary">
      <tr >
        <th >Image</th>
        <th >Item </th> 
        <th >Stock</th>
        <th >Max Order</th> 
        <th >Price</th>
        <th >Category</th>
        <th >Is Featured?</th>
        <th >Action</th>
      </tr>
    </thead> 
    <tbody> 
    @foreach ($data['products'] as $itemP)  
     <tr >
     <td>

      <?php  
      $first_photo  = $cdn_url . "/assets/image/no-image.jpg" ;

      $all_photos = array(); 
      $files =  explode(",",  $itemP->photos ); 

      if(count($files) > 0 )
      {
          $files=array_filter($files);
          $folder_path = $cdn_path .  "/assets/image/store/bin_" .   $itemP->bin .  "/";
          foreach($files as $file )
          {
            if(file_exists(   $folder_path .   $file ))
            {
              $first_photo = $cdn_url .   "/assets/image/store/bin_" .   $itemP->bin .  "/"  .    $file  ;
              break;
            } 
          } 
      } 
           
      ?> 

      <img src="{{ $first_photo   }}" alt="..." height="50px" width="50px">

    </td>
     <td><span class='badge badge-primary'>{{ $itemP->id }}</span><br/>
      {{ $itemP->pr_name }}<br/>
      {{  $itemP->description }}</td> 
      <td>{{$itemP->stock_inhand}}</td>
      <td>{{$itemP->max_order}}</td> 
      <td>{{$itemP->unit_price}}</td>
   <td>{{$itemP->category}}</td>

<td>
     
       
   </td>
      <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"> 
                          <a class="dropdown-item"  href="{{ URL::to('/admin/customer-care/business/view-product-photo') }}/{{ $itemP->bin }}/{{ $itemP->id  }}">Manage Images</a> 
                          <a class="dropdown-item showdialogm1" data-name="{{ $itemP->pr_name }}" data-key="{{ $itemP->id }}" data-photo="{{ $first_photo   }}"   >Add To Promotion</a>   

                        </div>
                      </div>
                  
        </td>
 
    </tr>
    @endforeach
   </tbody>

  </table>

  {{ $data['products']->links() }}

 </div>

  </div>

   
</div>    
  
   </div> 
 </div>
 

@endsection

@section("script")

<script>

$(document).on("click", ".showdialogm1", function()
{
    $("#span_prname").html( $(this).attr("data-name"));
    $("#hidprid").val( $(this).attr("data-key"));
    $("#primage").attr("src", $(this).attr("data-photo")); 
                          
    $("#widget-m1").modal("show")

});

  </script>

 @endsection