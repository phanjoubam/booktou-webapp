 <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            
            <li class="nav-item">
              <a class="nav-link" href="{{  URL::to('/franchise' )}}">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Orders</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu"> 
                  <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/franchise/orders/normal-orders' )}}">
                    Normal Orders
                  </a>
                </li>
         
          <li class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/orders/view-completed' )}}">
              Completed Normal Orders
            </a>
          </li> 

           <li class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/pick-and-drop-orders/view-all' )}}">
              Pick-And-Drop (PnD)
            </a>
          </li> 

          
 
          <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/normal-orders/stats' )}}"> Order Stats Reports</a>
          </li>


                </ul>
              </div>
            </li>


 


<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-02" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Businesses</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-02">
                <ul class="nav flex-column sub-menu"> 
          
         <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/businesses/view-all' )}}">
             View Businesses
            </a>
          </li>
 
<li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/business/normal-orders/best-performers' )}}">
              Best Performers
            </a>
          </li>
  
<li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/business/pnd-orders/best-performers' )}}">
              Best PnD Performers
            </a>
          </li>
 

                </ul>
              </div>
            </li>


<!-- <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-06" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Products</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-06">
                <ul class="nav flex-column sub-menu"> 
          
          
 
            <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/product/enter-save-product' )}}">
              Add Products
            </a>
            </li>
  
            <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/business/view-all-products' )}}">
              View Products
            </a>
            </li>
 




                </ul>
              </div>
            </li> -->

  
<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-03" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Agents &amp; Staffs</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-03">
                <ul class="nav flex-column sub-menu"> 
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/franchise/staffs/manage-attendance' )}}">
               Staff Attendance
            </a>
          </li> 
        <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/franchise/delivery-agents' )}}">
               View Delivery Agents
            </a>
          </li>  
 
        <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/franchise/delivery-agent/performance-snapshot' )}}">
              Agents Performance
            </a>
          </li> 


<li class="nav-item">
            <a class="nav-link"  target='_blank' href="{{  URL::to('/franchise/delivery-agents-live-locations' )}}">
              Live Location
            </a>
          </li>


  <li class="nav-item">    
   <a class="nav-link" 
   href="{{  URL::to('/franchise/delivery-agent/all-daily-orders-report' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Delivery Report
                </a>
          </li> 



                </ul>

 




              </div>
            </li>

<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-cst" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Customers</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-cst">
                <ul class="nav flex-column sub-menu"> 
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/franchise/manage-customers' )}}">
               All Customers
            </a>
          </li> 
       

                </ul>
              </div>
            </li>

 <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-05" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Marketing &amp; Analytics</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-05">
                <ul class="nav flex-column sub-menu">  

                   <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/franchise/analytics/businesses/products/view-frequently-browsed-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Frequently Browsed Products
                </a>
              </li>

 
    
 
  


                </ul>
              </div>
            </li>
 


<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-04" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
               <span class="menu-title">Accounting</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-04">
                <ul class="nav flex-column sub-menu">




              <li class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/franchise/accounts/view-accounts-dashboard' )}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> Accounts Snapshot
                  </a>
              </li> 

              <li class="nav-item">

                  <a class="nav-link"  href="{{  URL::to('/franchise/accounts/add-deposit')}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> New Deposit
                  </a>
              </li> 
              <li class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/franchise/accounts/add-expenditure' )}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> Expenditure Entry
                  </a>
              </li> 

              <li class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/franchise/accounts/view-expenditure' )}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> View Expenditure
                  </a>
              </li>  

              
                </ul>
              </div>
</li>



<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-06" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
               <span class="menu-title">Sales &amp; Stats</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-06">
                <ul class="nav flex-column sub-menu"> 
          
          
              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/franchise/report/daily-sales-and-service' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales Volume
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/franchise/report/monthly-earning' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Monthly Sales Earning 
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/franchise/billing-and-clearance/daily-sales' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales & Service
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/franchise/report/sales-and-service-per-cycle' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Weekly Sales & Service
                </a>
              </li>





 
 


  <!-- <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/franchise/report/daily-sales' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales Report
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link"  href="{{  URL::to('/franchise/report/sales-and-service-per-cycle' )}}">
                        <i class="now-ui-icons ui-1_bell-53"></i> Weekly Sales &amp; Service 
                      </a>
              </li>
 <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('franchise/report/pending-payments' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Payment Due Report
                </a>
              </li> -->
                


                </ul>
              </div>
            </li> 

              
              <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-07" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
               <span class="menu-title">Billing &amp; Clearance</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-07">
              
              <ul class="nav flex-column sub-menu">  
              <li class="nav-item">
                <a class="nav-link"  
                href="{{  URL::to('/franchise/billing-and-clearance/payment-due-merchants' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Weekly Report
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/franchise/report/pending-payments' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Payment Dues
                </a>
              </li> 
              </ul>

              </div>
            </li>
      
<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-05" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
               <span class="menu-title">System &amp; Config</span>
                <i class="menu-arrow"></i>
              </a>

           <div class="collapse" id="ui-item-05">
                <ul class="nav flex-column sub-menu"> 
          
          
 
 <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/franchise/sytem/config/sales-target' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Configuration
                </a>
              </li>
              </ul>   



</li>
 
            
          </ul>
        </nav>