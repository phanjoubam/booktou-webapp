@extends('layouts.franchise_theme_03')
@section('content')
 

 <div class="row"> 
  <div class="col-md-12">
     
                
                <?php 

                  $i=1; 
                
                foreach ($data['staffs'] as $item)
                { 
                ?>
<div class="card card-default mt-2"> 
       <div class="card-body">  
                    <div class="row">
                      <div class='col-md-12'>
                        <h2>{{ $item->fullname }}</h2> 
                      </div>


                      @foreach ($data['allDeliveries'] as $deliveryLog)   
                          @if($deliveryLog->member_id ==  $item->id )

                          <div class="col-md-6">
                            <div class="d-flex align-items-center pb-2">
                              <div class="dot-indicator bg-danger mr-2"></div>
                              <p class="mb-0">Total Deliveries</p>
                            </div>

                            
                              <h4 class="font-weight-semibold">{{ $deliveryLog->total_count }} till date</h4>
                             

                            <div class="progress progress-md">
                              <div class="progress-bar bg-danger" role="progressbar" style="width: 78%" aria-valuenow="78" aria-valuemin="0" aria-valuemax="78"></div>
                            </div>
                          </div>
                          <div class="col-md-6 mt-4 mt-md-0">
                            <div class="d-flex align-items-center pb-2">
                              <div class="dot-indicator bg-success mr-2"></div>
                              <p class="mb-0">Total revenue to bookTou</p>
                            </div>
                            <h4 class="font-weight-semibold">₹ {{ $deliveryLog->total_earned }} till date</h4>
                            <div class="progress progress-md">
                              <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="45"></div>
                            </div>
                          </div>
                        @break
                      @endif
                  @endforeach


                        </div>

  </div>
            </div>
                         <?php
                          $i++; 
                       }

                          ?>
              
             
          </div>
        </div>  
   
@endsection

@section("script")

<script>

 

$(document).on("click", ".btnshowlogform", function()
{

  var k = $(this).attr("data-key"); 
  var a = $(this).attr("data-act");
  var n = $(this).attr("data-name");  

  $("#key").val(k);
  $("#action").val(a); 
  $("#name").val(n); 
  $(".modallogatt").modal('show');

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 

    
    $('.modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('.modal_processing').modal({
      show:true,
      keyboard: false,
      backdrop: 'static'
    });


    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('.modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
   } 

   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/update-customer-confirmation" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.browser_refresh == "yes")
            location.reload(); 
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });
 

})




$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
</script> 

@endsection 