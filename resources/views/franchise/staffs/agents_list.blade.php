@extends('layouts.franchise_theme_03')
@section('content')
 

 <div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 
         <div class="card-title">
          <div class="title"> All Delivery Agents</div>
        </div> 
       
       </div>
       <div class="card-body"> 
                 <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">  
                  <tr class="text-center" style='font-size: 12px'>  
                    <th></th> 
                    <th scope="col">Full Name/ID</th>
                    <th scope="col">Address</th>
                    <th scope="col">Phone</th> 
                    <th scope="col">Job Count</th> 
                     <th scope="col">Is Free?</th> 
                    <th scope="col"></th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1; 
                
                foreach ($data['agents'] as $item)
                { 
                ?>
  
                  <tr >  
                    <td>
                    <strong>
                      <img width='50' height='50' src="{{  $item->profile_photo}}" alt='{{$item->id}}'/>
                    </strong> 
                   <td>
                    <strong>{{$item->fullname}} ({{$item->id}})</strong></td>
 <td>{{ $item->locality }}</td>
                    <td>{{ $item->phone }}</td>
   <td>{{ $item->taskCount }}</td>
   <td>{{ $item->isFree}}</td>
                  <td>
                  
                   <div class="dropdown">
  

                     <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a>



  <ul class="dropdown-menu dropdown-user pull-right"> 
    <li><a class="dropdown-item" href="{{ URL::to('/admin/customer-care/delivery-agent/get-daily-earning') }}/{{ $item->id }} ">Daily Earnings</a></li>
  <li><a class="dropdown-item" href="{{URL::to('/admin/delivery-agent/daily-orders-report') }}?agent={{ $item->id }}">Daily Orders</a></li>
    
  
                     
  </ul>
         </div> 

 
                  </td>
                     
   
                         </tr>
                         <?php
                          $i++; 
                       }

                          ?>
                </tbody>
                  </table>
              </div>
              </div>
            </div>
          </div>
        </div>  
  
    


@endsection

@section("script")

<script>


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
            


        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 

    
    $('.modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('.modal_processing').modal({
      show:true,
      keyboard: false,
      backdrop: 'static'
    });


    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('.modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
   } 

   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/update-customer-confirmation" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.browser_refresh == "yes")
            location.reload(); 
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });
 

})




</script> 

@endsection 