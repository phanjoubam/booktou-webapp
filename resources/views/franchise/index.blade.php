@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
   


?> 
 <div class='outer-top-tm'>
    <div class="container">

<section class="section-main  ">
<main class="card">
    <div class="card-body"> 
<div class="row">
    <aside class="col-lg col-md-3  ">
        <nav class="nav-home-aside"> 
            <ul class="menu-category"> 
                @foreach( request()->get('MAIN_MENU') as $item)
                <li><a href="{{ URL::to('/shop') }}/{{ strtolower( $item->menu_name ) }}">{{$item->menu_name }}</a></li>
                @endforeach
            </ul>
        </nav>
    </aside> <!-- col.// -->
    <div class="col-md-9 col-xl-9 col-lg-9">

<!-- ================== COMPONENT SLIDER  BOOTSTRAP  ==================  -->
<div id="carousel1_indicator" class="slider-home-banner carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel1_indicator" data-slide-to="0" class="active"></li>
    <li data-target="#carousel1_indicator" data-slide-to="1"></li> 
     <li data-target="#carousel1_indicator" data-slide-to="2"></li>  
      <li data-target="#carousel1_indicator" data-slide-to="3"></li>  
      <li data-target="#carousel1_indicator" data-slide-to="4"></li>  
  </ol>
  <div class="carousel-inner">


    <div class="carousel-item active">
      <a href="{{ URL::to('/') }}/business/btm-361">
        <img src="{{ $cdn_url }}/webapp/upload/promo_ad_1610162208.jpg" alt="VIRO"> 
      </a>
    </div> 

    <div class="carousel-item ">
      <a href="{{ URL::to('/') }}/business/btm-417">
        <img src="{{ $cdn_url }}/webapp/upload/promo_ad_1610617024.png" alt="Khudol"> 
      </a>
    </div>
 
 <div class="carousel-item">
     <a href="{{ URL::to('/') }}/shopping/btm-410/biryani">
        <img src="{{ $cdn_url }}/webapp/upload/promo_ad_1610511657.jpg" alt="Biryani Hub"> 
      </a>
    </div> 

    <div class="carousel-item  ">
      <img src="{{ $cdn_url }}/webapp/upload/promo_ad_1607355260.jpeg" alt="Paint 4 Soul"> 
    </div>
    <div class="carousel-item">
      <img src="{{ $cdn_url }}/webapp/upload/promo_ad_1609320814.jpg" alt="Pearl Berry"> 
    </div>
 
 
    
  </div>
  <a class="carousel-control-prev" href="#carousel1_indicator" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel1_indicator" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> 
<!-- ==================  COMPONENT SLIDER BOOTSTRAP end.// ==================  .// -->  

    </div> <!-- col.// -->
    

</div> <!-- row.// -->

    </div> <!-- card-body.// -->
</main> <!-- card.// -->

</section>
<!-- ========================= SECTION MAIN END// ========================= -->



<!-- =============== SECTION DEAL =============== -->
<section class="padding-bottom">
 <div class="card card-deal">
   
   <div class="row no-gutters items-wrap">

    @php 
             $i=0; 
             @endphp
             @foreach($deals as $product)  
                    @php 
                        $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 
                    @endphp

                           
<div class="col-md col-6">
     <figure class="card-product-grid card-sm">
      
      <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->pr_code ) }}"  class="img-wrap"> 
       <img src="{{ $product->productPhoto }}"> 
      </a>
      <div class="text-wrap p-3">
        <a href="#" class="title">{{ $product->pr_name }}</a>
        <span class="badge badge-danger">{{ $product->unit_price }}</span>
      </div>
   </figure>
 </div>
  

      @php 
        $i++; 
      @endphp
  @endforeach

</div>
</div>

</section>
 


<section class="padding-bottom">
<header class="section-heading heading-line">
    <h4 class="title-section text-uppercase">Biryani</h4>
</header>

<div class="card card-home-category">
<div class="row no-gutters">
    <div class="col-md-3">
    
    <div class="home-category-banner bg-light-orange">
        <h5 class="title">Best biryani in town</h5>
        <p>Biryani is a mixed rice dish with its origins among the Muslims of the Indian subcontinent. It is made with Indian spices, rice, and meat, or vegetables and sometimes, in addition, eggs and/or potatoes in certain regional varieties.</p>
           <a href="https://booktou.in/shop/restaurant" class="btn btn-outline-primary rounded-pill">Browse Restaurants</a>
        <img src="images/items/2.jpg" class="img-bg">
    </div>

    </div> <!-- col.// -->
    <div class="col-md-9">
<ul class="row no-gutters bordered-cols"> 

  @foreach($first_group as $product)
  <?php 
    $all_photos = array(); 
                $files =  explode(",",  $product->photos ); 
                if(count($files) > 0 )
                {
                    $files=array_filter($files);
                    $folder_path =  $cdn_path .  "/assets/image/store/bin_" .   $product->bin .  "/";

                    foreach($files as $file )
                    {

                        $source =  $folder_path .   $file; 
                        if(file_exists( $source  ))
                        {

                            $pathinfo = pathinfo( $source  );
                            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 

                            $destination  = $cdn_path  .    $new_file_name ;
                            if( file_exists( $destination ) )  
                            {
                                $all_photos[]  = $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name;
                            }
                            else 
                            {
                                if(filesize(  $source ) > 307200)  
                                { 
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name ;  
                                }
                                else 
                                {
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" .   $product->bin   . "/" .    $file  ;
                                }
                            } 
                        }
                        else
                        {
                            $all_photos[]   =  $cdn_url .  "/assets/image/no-image.jpg";
                        }
                    }
                    
                    if( count($all_photos ) > 0 )
                    {
                        $image_url =  $all_photos[0];
                    } 
                    else 
                    {
                        $image_url = $cdn_url .  "/assets/image/no-image.jpg";
                    }
                    
                }
                else
                {
                    $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                }
 
  $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

    ?> 

    <li class="col-6 col-lg-3 col-md-4">
          
    <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->pr_code ) }}" class="item"> 
        <div class="card-body">
            <h6 class="title">{{ $product->pr_name }}</h6>
            <img class="img-sm float-right" src="{{ $image_url }}"> 
            <p class="text-muted"><i class="fa fa-map-cutlery "></i>{{ $product->name }}</p>
        </div>
    </a>
        </li> 
  @endforeach

 
</ul>



    </div> <!-- col.// -->
</div> <!-- row.// -->
</div> <!-- card.// -->
</section>
 


<section class="padding-bottom">
<header class="section-heading heading-line">
    <h4 class="title-section text-uppercase">Appliances</h4>
</header>

<div class="card card-home-category">
<div class="row no-gutters">
    <div class="col-md-3">
    
    <div class="home-category-banner bg-light-orange">
        <h5 class="title">Household appliances at affordable priing</h5>
        <p>Grab the opportunity to purchase the best household items at affordable rates</p>
        <a href="https://booktou.in/shop/appliances" class="btn btn-outline-primary rounded-pill">Browse Stores</a>
        <img src="images/items/14.jpg" class="img-bg">
    </div>

    </div> <!-- col.// -->
    <div class="col-md-9">
<ul class="row no-gutters bordered-cols">

  @foreach($second_group as $product)
  <?php 
    $all_photos = array(); 
                $files =  explode(",",  $product->photos ); 
                if(count($files) > 0 )
                {
                    $files=array_filter($files);
                    $folder_path =  $cdn_path .  "/assets/image/store/bin_" .   $product->bin .  "/";

                    foreach($files as $file )
                    {

                        $source =  $folder_path .   $file; 
                        if(file_exists( $source  ))
                        {

                            $pathinfo = pathinfo( $source  );
                            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 

                            $destination  = $cdn_path  .    $new_file_name ;
                            if( file_exists( $destination ) )  
                            {
                                $all_photos[]  = $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name;
                            }
                            else 
                            {
                                if(filesize(  $source ) > 307200)  
                                { 
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name ;  
                                }
                                else 
                                {
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" .   $product->bin   . "/" .    $file  ;
                                }
                            } 
                        }
                        else
                        {
                            $all_photos[]   =  $cdn_url .  "/assets/image/no-image.jpg";
                        }
                    }
                    
                    if( count($all_photos ) > 0 )
                    {
                        $image_url =  $all_photos[0];
                    } 
                    else 
                    {
                        $image_url = $cdn_url .  "/assets/image/no-image.jpg";
                    }
                    
                }
                else
                {
                    $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                }
 
  $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

    ?> 

    <li class="col-6 col-lg-3 col-md-4">
    <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->pr_code ) }}" class="item"> 
        <div class="card-body">
            <h6 class="title">{{ $product->pr_name }}</h6>
            <img class="img-sm float-right" src="{{ $image_url }}"> 
            <p class="text-muted"><i class="fa fa-map-cutlery "></i>{{ $product->name }}</p>
        </div>
    </a>
        </li> 
  @endforeach
</ul>
    </div> <!-- col.// -->
</div> <!-- row.// -->
</div> <!-- card.// -->
</section>
 

<section class="padding-bottom">

<header class="section-heading heading-line">
    <h4 class="title-section text-uppercase">Request for Quotation</h4>
</header>

<div class="row">
    <div class="col-md-8">
<div class="card-banner banner-quote " style="background-image: url('{{ URL::to('/public/store/image/help-desk.jpg') }}')">
  <div class="card-img-overlay white">
    <h3 class="card-title">bookTou HelpDesk</h3>
    <p class="card-text" style="max-width: 400px">Tell us what you need and we will make it happen.</p>
    <a href="tel:+919863086093" class="btn btn-white rounded-pill">Call Now</a>
  </div>
</div>
    </div> <!-- col // -->
    <div class="col-md-4">

<div class="card card-body">
    <h3 class="title  ">One Request, Instant Quote</h3> 

      @if (session('err_msg'))
        <div class="alert alert-info">
                {{ session('err_msg') }}
      </div>
     @endif 
    <form method='post' action="{{ action('StoreFront\StoreFrontController@submitInstantQuote') }}">
      @csrf
        <div class="form-group">
            <input class="form-control" name="body" placeholder="What are you looking for?" type="text">
        </div>
        <div class="form-group">
      
            <div class="input-group">
                <input class="form-control" placeholder="Quantity" name="qty" type="text">
                
                <select class="custom-select form-control" name='unit'>
                    <option>Piece</option>
                    <option>Plate</option>
                    <option>Package</option>
                    <option>Can</option>
                    <option>Kgs</option> 
                </select>
            </div> 
        </div>

        <div class="form-row">
          <div class="col-8">
            <input class="form-control" name="name" placeholder="Your Name" type="text">
        </div> 
        <div class="col-4"> 
              <input class="form-control" name="phone" placeholder="Phone number" type="text"> 
        </div> 
        </div> 
       <div class="form-row">
         <div class="col-12">
          <br/> 
            <button class="btn btn-warning" type='submit' name='btnsend' value='send'>Request for quote</button>
        </div>
      </div>
    </form><br/>
    <small>We respect your privacy and do not tolerate spam and will never share your information (name, address, email, etc.) to any third party.</small>
</div>

    </div> <!-- col // -->
</div> <!-- row // -->
</section>
 


 
<section  class="padding-bottom-sm">

<header class="section-heading heading-line">
    <h4 class="title-section text-uppercase">Recommended Products</h4>
</header>

<div class="row row-sm">

  @foreach($recommended_list as $product)
  <?php 
    $all_photos = array(); 
                $files =  explode(",",  $product->photos ); 
                if(count($files) > 0 )
                {
                    $files=array_filter($files);
                    $folder_path =  $cdn_path .  "/assets/image/store/bin_" .   $product->bin .  "/";

                    foreach($files as $file )
                    {

                        $source =  $folder_path .   $file; 
                        if(file_exists( $source  ))
                        {

                            $pathinfo = pathinfo( $source  );
                            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 

                            $destination  = $cdn_path  .    $new_file_name ;
                            if( file_exists( $destination ) )  
                            {
                                $all_photos[]  = $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name;
                            }
                            else 
                            {
                                if(filesize(  $source ) > 307200)  
                                { 
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name ;  
                                }
                                else 
                                {
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" .   $product->bin   . "/" .    $file  ;
                                }
                            } 
                        }
                        else
                        {
                            $all_photos[]   =  $cdn_url .  "/assets/image/no-image.jpg";
                        }
                    }
                    
                    if( count($all_photos ) > 0 )
                    {
                        $image_url =  $all_photos[0];
                    } 
                    else 
                    {
                        $image_url = $cdn_url .  "/assets/image/no-image.jpg";
                    }
                    
                }
                else
                {
                    $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                }
 
  $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

    ?> 
    <div class="col-xl-2 col-lg-3 col-md-4 col-6">
        <div class="card card-sm card-product-grid">
             <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->pr_code ) }}"  class="img-wrap"> <img src="{{ $image_url }}"> </a>
            <figcaption class="info-wrap">
                <a href="#" class="title">{{ $product->pr_name }}</a>
                <div class="price mt-1">{{ $product->actual_price }}</div>  
            </figcaption>
        </div>
    </div>  
  
  @endforeach

    
    
</div>  
</section>
 
 
 

<article class="my-4">
    <img src="{{ URL::to('/public/store/image/ad-horizontal.jpg') }}" class="w-100">
</article>
</div>  
 
<section class="section-subscribe padding-y-lg">
<div class="container">

<p class="pb-2 text-center text-white">Delivering the latest products and offers straight to your inbox</p>

<div class="row justify-content-md-center">
    <div class="col-lg-5 col-md-6">
    <form class="form-row" action="{{ action('StoreFront\StoreFrontController@subscribeEmailMarketing') }}" method='post'>
      @csrf
        <div class="col-md-8 col-7">
        <input class="form-control border-0" placeholder="Your Email" type="email" name='email'>
        </div> <!-- col.// -->
        <div class="col-md-4 col-5">
        <button type="submit" class="btn btn-block btn-warning" value='subscribe' name='btnsubscribe'> <i class="fa fa-envelope"></i> Subscribe </button>
        </div> <!-- col.// -->
</form>
<small class="form-text text-white-50">We’ll never share your email address with a third-party. </small>
    </div> <!-- col-md-6.// -->
</div>
    

</div>
</section>

</div> <!-- container -->
</div> <!-- spacer -->

@endsection 



@section('script')

 <script>


$(document).ready(function() {


   $('.product_thumbnail_slides').owlCarousel({
            items: 1,
            margin: 0,
            loop: true,
            nav: true,
            navText: ["<img src='img/core-img/long-arrow-left.svg' alt=''>", "<img src='img/core-img/long-arrow-right.svg' alt=''>"],
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 1000
        });
 

});


 
        
      

</script>


@endsection 