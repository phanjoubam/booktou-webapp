@extends('layouts.franchise_theme_03')
@section('content')
 
<?php 

$business = $data['business'];
 
?>
<form action="{{ action('Admin\AdminDashboardController@saveSalesClearance') }}" method='post'>
    @csrf 

  <div class='row'>
    <div class='col-md-8'> 

      <div class="card">
          <div class="card-header"> 
        <div class="row">   
        <div class="col-md-8">  
              <h4 >Clearance Form <strong>{{$business->name}}</strong></h4>
              <p>{{$business->locality}}, {{$business->landmark}}, {{$business->city}}, {{$business->state}}-{{$business->pin}}<br/>
            <i class='fa fa-phone'></i> {{$business->phone_pri}}
            </p>
          </div>
        </div> </div>

        <div class="card-body"> 
            
          <hr/>
<div class='table-responsive'>  
 <table class="table"> 
  <thead class=" text-primary">
    <tr class="text-center"> 
      <th scope="col"></th>
    <th scope="col">Order No.</th>
    <th scope="col">Order Date</th>  
    <th scope="col" class='text-center'>Type</th>  
     <th scope="col" class='text-center'>Source</th>  
    <th scope="col"  class='text-right'>Actual Amount</th> 
    <th scope="col"  class='text-right'>Commission <span class='badge badge-danger'>{{$business->commission }} %</span></th> 
    <th scope="col"  class='text-right'>Payable</th>  
    </tr>
    </thead> 
    <tbody>

       @php 
        $total_sales  = 0; 
        $total_commission =0;
       @endphp  

     <?php $i = 0 ?>
    @foreach ($data['results'] as $item)
      
      @php  
        $actual = 0;
        $commission = 0;
        $payable = 0;
      @endphp 

      @if($item->book_status == "delivered"  || $item->book_status == "completed"  )

    
    <?php $i++ ?> 
     <tr class="text-center"> 
      <td>  
        <input readonly checked class="form-check-input" type="checkbox" value="{{$item->id}}" name="selection[]">  
      </td> 
     <td>
        {{ $item->id }}
     </td> 
     <td>{{ date('d-m-Y', strtotime( $item->book_date) ) }}</td>  
    @php
      $actual = $item->total_amount;     
    @endphp
       
    @if($item->orderType == "pnd")   

      <td class='text-center'><span class='badge badge-success'>{{$item->orderType}}</span></td> 
     

        @if( $item->source == "booktou" || $item->source == "customer" ) 
          @php 
            $commission = ( $item->commission * 0.01 * $item->total_amount ); 
          @endphp
        @else 
          @php 
            $commission = 0 ; 
          @endphp
        @endif   
     @else 
      <td class='text-center'><span class='badge badge-info'>{{$item->orderType}}</span></td> 


        @php
          $commission = ( $item->commission * 0.01 * $item->total_amount ); 
        @endphp 
    @endif

    @php
      $total_sales += $actual ;
      $total_commission += $commission ;      
    @endphp


    <td class='text-center'><span class='badge badge-success'>{{$item->source}}</span></td>
     <td class='text-right'>{{$item->total_amount}}</td>
    <td  class='text-right'>{{ $commission  }}</td> 
    <td  class='text-right'>{{ $actual - $commission }}</td> 
  
    </tr>
    @endif 
@endforeach
@if(isset($data['results']) && count($data['results']) > 0)  
     <tr  > 
      <td   class='text-right' colspan='5'>Total:</td>
      <td class='text-right' >{{ number_format(  $total_sales , 2, '.', '')     }} ₹</td>   
      <td class='text-right' >{{ number_format(  $total_commission , 2, '.', '')     }} ₹</td> 
       <td class='text-right' >{{ number_format( $total_sales-  $total_commission , 2, '.', '')     }} ₹</td>   
  
    </tr> 
 @endif
</tbody>
</table> 
</div> 
</div>
 </div>  

 </div>
  <div class='col-md-4'>  

    <div class="card bg-light mb-3" >
  <div class="card-header">Clerance Information</div>
  <div class="card-body"> 
<form>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="amount">Amount Cleared:</label>
      <input type="text" class="form-control" id="amount" name='amount' value="{{ number_format( $total_sales-  $total_commission , 2, '.', '')     }}">
    </div>
    <div class="form-group col-md-6">
      <label for="paymode">Payment Mode</label>
      <select  class="form-control" id="paymode" name='paymode'>
          <option>UPI</option>
          <option>Google Pay</option>
          <option>Online Transfer</option>
          <option>Cash</option> 
      </select> 
    </div>
  </div>
   <div class="form-row">
   <div class="form-group col-md-6">
    <label for="refno">Transaction Reference No.</label>
    <input type="text" class="form-control" id="refno" name='refno' placeholder="Reference no (if any)">
  </div> 

  <div class="form-group col-md-6">
    <label for="clearedon">Clearance Date</label>
    <input type="text" class="form-control" id="clearedon" name='clearedon' placeholder="Clerance Date">
  </div> </div>
  <div class="form-group">
    <label for="remarks">Remarks (optional)</label>
    <textarea name="remarks" class="form-control" rows='4' id="remarks" placeholder="Clerance remarks"></textarea>
  </div>
  <input type='hidden' name='bin' value="{{ $data['bin']}}" >
  <button type='submit' name='btnsubmit' value='submit' class='btn btn-primary btn-block'>Save Payment Details</button>
</form>
 </div>
</div>


</div>

</div> 
</form>
 
 
     

@endsection


  
 