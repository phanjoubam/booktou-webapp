<?php 
    $total =0;  
?>

@extends('layouts.franchise_theme_01')
@section('content')
 

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
         
                <div class="row">
          <div class="col-md-8">
            <h5>View Daily Orders Delivery Reports for <span class='badge badge-warning'>{{ date('d-m-Y', strtotime($data['date'])) }}</span></h5>
           </div>
    <div class="col-md-4 text-right">
         <form class="form-inline" method="get" 
         action="{{  action('Franchise\FranchiseDashboardControllerV1@allDeliveryAgentDailyOrdersReport') }}">
            {{ csrf_field() }}
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' name='filter_date' /> 
 

                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
            </form>
      </div>
   
    </div>
  </div>
 </div>
 

@foreach($data['agents'] as $agent)
 


 <div class="card mt-3">
       <div class="card-header">
         
                <div class="row">
          <div class="col-md-8">
    <h5>Orders Delivery Reports for <span class='badge badge-primary'>{{ $agent->fullname  }}</span></h5>
   </div>
    <div class="col-md-4 text-right">
         
      </div>
   
    </div>
  </div>

       <div class="card-body">  
          <table class="table">
              <thead class=" text-primary">  
                  <tr class="text-center" style='font-size: 12px'>  
                    <th scope="col">Order No.</th>
                    <th scope="col">Merchant/Customer</th> 
                    <th scope="col">Delivery Time</th> 
                    <th class="text-center">Order Status</th> 
                    <th class='text-right'>Payment Type</th>  
                    <th class='text-right'>Order Type</th>  
                    <th class="text-right">Amount Collected</th> 
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1; 
                $totalcashamount = $totalonlineamount = 0.0;
                foreach ($data['all_orders'] as $item)
                {  

                ?>  
                 @if($item->member_id == $agent->id )

                  <tr id="tr{{$item->orderNo }}">  
                   <td>
                    <strong>{{$item->orderNo}}</strong>
                  </td>


                    <td>{{$item->orderBy}}</td>  
                 
                  <td>
                    <span class='badge badge-danger'>{{ date('H:i a', strtotime( $item->delivery_time )) }}</span>
                  </td>


                     <td class="text-center">

                      @switch($item->orderStatus)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-info'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                        <?php 

                          if(strcasecmp($item->payMode, "cash") == 0 ||strcasecmp($item->payMode, "Cash on delivery") == 0  )
                          {
                              $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                          }
                          else 
                          {
                             $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ;
                          }

                         ?>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>

                        <?php 

                          if(strcasecmp($item->payMode, "cash") == 0 ||strcasecmp($item->payMode, "Cash on delivery") == 0  )
                          {
                              $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                          }
                          else 
                          {
                            $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ;
                          }

                         ?>
 
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>   
                    <td class='text-right'>
                      <span class='badge badge-info'>{{ $item->payMode }}</span>
                    </td>
                    <td class='text-right'>
                      <span class='badge badge-success'>{{ $item->orderType }}</span>
                    </td>
                    <td class='text-right'><span class='badge badge-info'>{{ $item->totalAmount + $item->serviceFee  }}</span></td>
                  </tr>

                  @endif

                  <?php
                    $i++; 
                     }
                    ?>

                    <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='6'>
                      Total Cash: <span class='badge badge-success'>{{ $totalcashamount  }} &#8377;</span> 
                      Total Online: <span class='badge badge-warning'>{{ $totalonlineamount  }} &#8377;</span> 
                      Total Amount: <span class='badge badge-danger'>{{ $totalcashamount +  $totalonlineamount }} &#8377;</span></th>  
                  </tr> 
                   
                </tbody>
                  </table>
            
           
            </div>
  </div>

 
 
 @endforeach

 </div>


 
 
</div> 

     

@endsection


 @section("script")

<script>
  
 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 