@extends('layouts.franchise_theme_03')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 



<div class="cardbodyy margin-top-bg"> 


  <div class="col-md-12">

 <div class="card">
  <div class="card-body">

   <div class="card-header"> 
                <h4 class="card-title text-center">Monthly Earning Report</h4>
              </div>
             


      <form class="form-inline" method="post" action="{{  action('Admin\AdminDashboardController@viewMonthlyEarningReport') }}">

            {{ csrf_field() }}
              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Select Month:</label> 
              <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='month'>
                 
                    <option value='1'>January</option>
                    <option value='2'>Febuary</option> 
                    <option value='3'>March</option> 
                    <option value='4'>April</option> 
                    <option value='5'>May</option> 
                    <option value='6'>June</option> 
                    <option value='7'>July</option> 
                    <option value='8'>August</option> 
                    <option value='9'>September</option> 
                    <option value='10'>October</option> 
                    <option value='11'>November</option> 
                    <option value='12'>December</option> 
                
                </select>
                 <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Select Year:</label> 
                 <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='year'>
                 
                    <option value='2020'>2020</option>
                   
                    
                
                </select>

                <input type="hidden" value="{{ $data['aid'] }}" name="a_id">
              
                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>

            <hr class="my-4" />

 <table class="table">
              


  <thead class=" text-primary">
    <tr class="text-center">
      <th scope="col">Sl.No.</th>
    <th scope="col">Order No.</th>
    <th scope="col">Order By</th>
    <th scope="col">Status</th>
    <th scope="col">Delivery Charge</th>

    </tr>
    </thead>

    <tbody>

     <?php $i = 0 ?>
    @foreach ($data['results'] as $item)
    <?php $i++ ?> 
     <tr class="text-center">
     <td>{{$i}}</td>
     <td>{{$item->orderNo}}</td>
      <td>{{$item->orderBy}}</td>
      <td>{{$item->orderStatus}}</td>
      <td>{{$item->deliveryCharge}}</td>

  
    </tr>
   

    @endforeach
   </tbody>

  </table>

<hr class="my-4" />
  @if(isset($data['results']) && count($data['results']) > 0)  
<div>
   <strong>Total Earning:</strong> {{$data['totalEarning']}}
     </div>

     @endif
  </div>
   
</div>    
  
   </div> 

   </div>

     

@endsection


  
 