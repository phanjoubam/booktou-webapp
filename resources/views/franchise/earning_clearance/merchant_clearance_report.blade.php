@extends('layouts.franchise_theme_03')
@section('content') 
<div class="row">  

<div class="col-md-12">  
 

 <div class="card">
    <div class="card-header"> 
      <div class='row'> 
       <div class='col-md-6'>
        <h4  >Merchant Clearance Report</h4> 
      </div>  
      <div class='col-md-6 text-right'>
        <form class="form-inline" method="get" action="{{  action('Franchise\FranchiseAccountsAndBillingController@salesAndClearanceHistory') }}"> 
            {{ csrf_field() }} 
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Month:</label> 
              <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='month'>
                 
                    <option value='1'>January</option>
                    <option value='2'>Febuary</option> 
                    <option value='3'>March</option> 
                    <option value='4'>April</option> 
                    <option value='5'>May</option> 
                    <option value='6'>June</option> 
                    <option value='7'>July</option> 
                    <option value='8'>August</option> 
                    <option value='9'>September</option> 
                    <option value='10'>October</option> 
                    <option value='11'>November</option> 
                    <option value='12'>December</option> 
                
                </select>
                 <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Year:</label> 
                 <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='year'>
                  
                  <?php 
                  for($year = date('Y'); $year >=2020; $year--)
                  {
                    ?>
                    <option value='{{$year }}'>{{$year }}</option>
                    <?php 
                  }
                  ?> 
                </select>
 
              
                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>
      </div> 
    </div>
  </div> 
 
</div> 


@foreach( $data['payment_history'] as $history )

 <div class="card mt-1">
    <div class="card-header"> 
<div class='row'>
  
  <div class='col-md-6'> 
         <ul class="nav">
          <li class="nav-item">
            <a class="nav-link active" target="_blank" href="{{ URL::to('franchise/payment/view-clearance-report-merchantwise') }}/{{ $history->bin }}/{{ $history->id }}">Clearance # {{ $history->id }}</a>
          </li>

        @foreach( $data['businesss'] as $business )
          @if($business->id == $history->bin )
            <li class="nav-item">
              <a class="nav-link active" target="_blank" href="{{ URL::to('franchise/payment/view-clearance-report-merchantwise') }}/{{ $history->bin }}/{{ $history->id }}">{{ $business->name }}</a>
            </li>
            @break
          @endif 
        @endforeach
          
          <li class="nav-item">
            <a class="nav-link" href="#">Total Paid <span class='badge badge-primary'>{{ $history->amount }}</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Cleared On <span class='badge badge-info'>{{ date('d-m-Y', strtotime($history->cleared_on)) }}</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Payment Type <span class='badge badge-success'>{{ $history->payment_mode }}</span></a>
          </li>
        </ul>
  </div> 
  <div class='col-md-6'>
     {{ $history->remarks }}
  </div>

</div> 
    </div> 

 
</div>     
@endforeach

<div class="mt-2">

{{ $data['payment_history']->links() }}

</div>
  
   </div> 

   </div>

     

@endsection


  
 