@extends('layouts.franchise_theme_03')
@section('content')
<?php  
$total_earning_for_bookTou = 0;
$total_pnd =0;
$total_normal =0;
$total_assist =0;
?>

 <div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 

         <div class="card-title">
         <div class="row">
          <div class="col-md-9">
          <div class="title"> All Delivery Agents Orders Report For: 
            <span class="badge badge-warning">
              {{$data['month']}} , {{$data['year']}}
            </span>

             <a class="btn btn-primary" 
            href="{{ URL::to('/franchise/agents-monthly-orders-export-to-excel')}}/{{ $data['year'] }}/{{$data['mont']}}">Export To Excel</a>
          </div>
      </div>

      <div class="col-md-3 text-right">  
        <form class="form-inline" method="get" 
        action="{{ action('Franchise\FranchiseDashboardControllerV1@franchiseOrderDeliveredByMonth') }}"   >
              {{  csrf_field() }}
      
      <div class="form-row">
    <div class="col-md-12">
         
        
        <select name='month' class="form-control form-control-sm  ">
          <option value='1'>January</option>
          <option value='2'>February</option>
          <option value='3'>March</option>
          <option value='4'>April</option>
          <option value='5'>May</option>
          <option value='6'>June</option>
          <option value='7'>July</option>
          <option value='8'>August</option>
          <option value='9'>September</option>
          <option value='10'>October</option>
          <option value='11'>November</option>
          <option value='12'>December</option>

        </select>
       <select name='year' class="form-control form-control-sm  ">
          <?php 
          for($i=2021; $i >= 2020; $i--)
          {
            ?> 
            <option value='{{ $i }}'>{{ $i }}</option>
          <?php 
          }
          ?>

        </select>
   
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>
         </div>
        </div> 
       
       </div>
       <div class="card-body"> 
                 <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">  
                  <tr class="text-center" style='font-size: 12px'>  
                    
                    <th scope="col">Agent ID</th>
                    <th class="text-left">Agent Name</th>
                    <th scope="col">Normal</th> 
                    <th scope="col">PND</th> 
                    <th scope="col">Assits</th> 
                    <th scope="col">Earned for bookTou</th> 
                    <th scope="col">Advance</th>
                    <th scope="col">Difference</th>
                </tr>
                </thead>

                <tbody>

                @foreach($data['agents'] as $agent) 
                 <tr>
                  <td class="text-center">
                  <a href="{{ URL::to('/franchise/delivery-agents/daily-report')}}/{{$agent->id }}?year={{ $data['year'] }}&month={{$data['mont']}}" target='_blank'>{{$agent->id}}</a>
                  </td>
                  <td class="text-left">{{$agent->fullname}}</td>
                  <?php
                        $pndOrders =0;
                        $assitsOrders=0;
                        $normalOrders=0;
                        $advance = 0;
                        $difference =0;
                        $earningforbookTou=0;

                      foreach($data['orders'] as $item) 
                      {

                        if( $item->member_id == $agent->id )
                        {
                          
                              if($item->type == "normal"){
                   
                                  $normalOrders = $item->orderCount; 

                                  $earningforbookTou += $item->total_earned ;
                                  $difference = $earningforbookTou - $advance;

                              }elseif($item->type == "pnd"){
                                  
                                  $pndOrders = $item->orderCount;

                                  $earningforbookTou += $item->total_earned ;
                                  $difference = $earningforbookTou - $advance;

                              }elseif($item->type == "assist"){
                    
                                  $assitsOrders = $item->orderCount; 

                                  $earningforbookTou += $item->total_earned ;
                                  $difference = $earningforbookTou - $advance;
                              } 
                              
                        }

                            

                         

                      }
                   
                $total_pnd+=$pndOrders;
                $total_assist+=$assitsOrders;
                $total_normal+=$normalOrders;
                $total_earning_for_bookTou += $earningforbookTou;

                 ?>
                  

 

                  <td class="text-center">{{$normalOrders}}</td>
                  <td class="text-center">{{$pndOrders}}</td>
                  <td class="text-center">{{$assitsOrders}}</td>


                  <td class="text-center">{{$earningforbookTou}}</td>
                  <td class="text-center">{{$advance}}</td>
                  <td class="text-center">
                    {{$difference}}
                  </td>

                 </tr>

                 @endforeach


                </tbody>
                <tfoot>
                  <tr>
                    <td></td>
                    <td></td>
                    <td>
                      <span class="badge badge-success">Normal:</span>
                      {{$total_normal}}
                    </td>
                    <td>
                      <span class="badge badge-warning">Pnd:</span>
                      {{$total_pnd}}
                    </td>
                    <td class="text-center">
                    <span class="badge badge-info">Assits:</span>
                    {{$total_assist}}
                    </td>
                    <td class="text-center"><span class="badge badge-primary">Total:</span>  {{$total_earning_for_bookTou}}</td>
                     
                    <td></td>
                    <td></td>
                  </tr>
                </tfoot>
                  </table>
              </div>
              </div>
            </div>
          </div>
        </div>  
  
    



@endsection

@section("script")

<script>


 
 

 


</script> 

@endsection 