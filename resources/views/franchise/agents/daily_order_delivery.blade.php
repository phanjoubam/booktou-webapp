@extends('layouts.franchise_theme_03')
@section('content')
<?php
 	$advance = 0;
 	$difference =0;
 	$total_orders =0;
 	$total_earning=0;
 ?>

<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header" style="background-color: #434343; color: #fff;"> 

         <div class="card-title" >
         <div class="row">
         	<div class="col-md-8">
          <div class="title" > <h3 style="color:#fff !important;">Daily Orders Count Report For:
          <span class="badge badge-warning">
          	{{$data['month']}} , {{$data['year']}} 
          	</span>
          </h3> 	
          	<a class="btn btn-primary" 
             href="{{ URL::to('/franchise/agents-daily-orders-export-to-excel')}}/{{$data['agents']->id}}/{{ $data['year'] }}/{{$data['month_no']}}">Export To Excel</a> 
          </div>
      </div>
      <div class="col-md-4">
      	<h3> <span class="badge badge-primary">{{$data['agents']->fullname}} ({{$data['agents']->phone}})</span></h3>
	</div>       
      	 </div>
        </div> 
       
       </div>
       <div class="card-body"> 
                 <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead  class="thead-dark">  
                  <tr class="text-center" style='font-size: 12px'>  
                    
                    
                    <th scope="col">Date</th>
                    <th scope="col">Order Type</th>
                    <th scope="col">Order Count</th> 
                    <th scope="col">Earned for bookTou</th> 
                    <th scope="col">Advance</th>
                    <th scope="col">Difference</th>
                </tr>
                </thead>





                <tbody>

                @foreach($data['orders'] as $item) 
                 <tr>
                 	<td class="text-center">{{date('d-m-Y', strtotime($item->serviceDate))}}</td>
                 	
                 	<td class="text-center">
                 		@if($item->type=='pnd')
                    <span class="badge badge-warning">{{$item->type}}</span>
                    @elseif($item->type=='assist')
                    <span class="badge badge-info">{{$item->type}}</span>
                    @else
                    <span class="badge badge-success">{{$item->type}}</span>
                    @endif
                 	</td>
                 	<td class="text-center">{{$item->orderCount}}</td>
                 	<td class="text-center">{{$item->total_earned}}</td>
                 	<td class="text-center">{{$advance}}</td>
                 	<td class="text-center">
                 		<?php 

                 		$difference = $item->total_earned - $advance;

                 		?>

                 		{{$difference}}
                 	</td>

                 </tr>
                 <?php
                 	$total_orders+= $item->orderCount;
                 	$total_earning+= $item->total_earned;
                 ?>
                 

                 @endforeach

                 <tfoot>
                 	<tr>
                 		<td></td>
                 		<td></td>
                 		<td>
                 		<span class="badge badge-primary">Total Orders:</span> {{$total_orders}}</td>
                 		<td class=""><span class="badge badge-primary">Total Earning:</span> {{$total_earning}}</td>
                 		<td></td>
                 		<td></td>
                 	</tr>

                 </tfoot>
                </tbody>
                  </table>
              </div>
              </div>
            </div>
          </div>
        </div>





@endsection

@section("script")

<script>


 
 

 


</script> 

@endsection 