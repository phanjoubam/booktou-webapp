@extends('layouts.franchise_theme_03')
@section('content')

 
<div id="map_wrapper">
    <div id="map_canvas" class="mapping"></div>
</div>
 
@endsection

@section("script")

<script>

  $(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyC9KrtxONZFci3FxO0SPZJSejUXHpvgmHI&sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
       
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
      
    // Multiple Markers
    var markers = [
        <?php 

        $infowindowcontent = array();
        foreach($data['agents'] as $item)
        {
          if($item->latitude != 0 &&  $item->longitude  != 0  )
          {echo "['" . $item->fullname . "' ," .  $item->latitude . "," .  $item->longitude . "],";  
          $infowindowcontent[] = '<div class="info_content">' .  $item->fullname .  
          '<p> Current Order: ' .  $item->orderNo .  '</p> </div>';

          } 
        } 
        ?> 
    ];
                        
    // Info Window Content
    var infoWindowContent = [  
      <?php 
        foreach($infowindowcontent as $item)
        {
          echo "['" .  $item .  "'],";   
        }

      ?>
     ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  

    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
 
    }
// Automatically center the map fitting all markers on the screen
         map.setCenter(new google.maps.LatLng( markers[0][1], markers[0][2] ));
    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(12);
        google.maps.event.removeListener(boundsListener);
    });
    
}

</script> 

@endsection 