@extends('layouts.franchise_theme_03')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-6">
                  <div class="title">
                    Daily Sales Report for <span class='badge badge-primary'>{{ date('d-m-Y', strtotime($date)) }}</span> 

                  </div> 
                </div>

<div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-4">  
        <form class="form-inline" method="get" action="{{ action('Franchise\FranchiseDashboardControllerV1@dailySalesReport') }}"   >
              {{  csrf_field() }}
      
      <div class="form-row">
    <div class="col-md-12">
      <input  class="form-control form-control-sm calendar" name='todayDate'  />
   
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>

       </div>
                </div>
            </div>
       <div class="card-body">   
<div class="table-responsive"> 
    <table class="table">
      <thead>
        <tr>
          <th>Order No</th>
          <th>Business</th>
          <th class='text-center'>Payment Mode</th>
          <th class='text-center'>Status</th>
          <th class='text-right'>Merchant Amount</th>
          <th class='text-right'>bookTou Fee</th>  
        </tr>
      </thead>
      
      <tbody>
        @php 
          $totalSale = $totalFee = 0.00
        @endphp
          @foreach ($sales as $normal)
          <tr>
            <td>

            @if($normal->orderType == "normal")
              <a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a> 
            @else 
               <a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a> 
            @endif 
            
            </td>
            <td>{{ $normal->name }} <span class='badge badge-primary'>{{ $normal->category }}</span>
            </td>
            <td class='text-center'>
              @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                <span class='badge badge-info'>ONLINE</span> 
              @else 
                <span class='badge badge-primary'>CASH</span>
              @endif
            </td>
            
              @if($normal->bookingStatus == "delivered")
              <td class='text-center'>
                <span class='badge badge-success'>{{ $normal->bookingStatus }}</span>
              </td>
              <td class='text-right'>{{ $normal->orderCost  }}</td>
              <td class='text-right'>{{ $normal->deliveryCharge }}</td> 

              @php 
                $totalSale += $normal->orderCost; 
                $totalFee += $normal->deliveryCharge ;
              @endphp

              @else 
              <td class='text-center'>
                <span class='badge badge-danger'>{{ $normal->bookingStatus }}</span>
              </td>
              <td class='text-right'>0.00</td>
              <td class='text-right'>0.00</td> 
              @endif

          </tr>
        @endforeach
      
      <tr>
          <th colspan='4' class='text-right'>Total Amount</th> 
          <th class='text-right'>{{ $totalSale }}</th>
          <th class='text-right'>{{ $totalFee }}</th>  
        </tr>

      </tbody>

    </table>
		
	</div>
 </div>


  </div> 
	
	</div> 
</div>

 
 
 

 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

