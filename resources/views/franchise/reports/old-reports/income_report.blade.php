@extends('layouts.franchise_theme_01')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>


  <div class="row">
     

     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-5">
                    <div class="title">
                      bookTou Earning Report for {{ $data['year'] }} 
                    </div> 
                </div>
     <div class="col-md-6"> 
 <form class="form-inline" method="get" action="{{ action('Franchise\FranchiseDashboardControllerV1@monthlyEarningReport') }}"   >
              {{  csrf_field() }}
  <div class="form-row">
    <div class="col-md-4 text-right"> 
                      Year:
    </div> 
    <div class="col-md-6">
      <select  class="form-control form-control-sm" name='year' >
        <?php 
        for($i= date('Y') ; $i >= 2020; $i--)
        { 
        ?> 
        <option value='{{ $i }}'>{{ $i }}</option> 
        <?php 
        }
      ?>
      </select> 
    </div>
    <div class="col-md-2">
      <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
    </div>
  </div>
</form>
   </div>
  

       </div>
                </div>
            </div>
       <div class="card-body">   
<div class="table-responsive">
    <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
      <th scope="col" class="text-left">Period</th>
  		<th scope="col" class="text-center">Total Sales</th>
  		<th scope="col" class="text-center">Delivery Commission</th>  
  		<th scope="col" class="text-center">PnD Order Sales</th>  
      <th scope="col" class="text-center">PnD Delivery Commission</th>   
      <th scope="col" class="text-center">Total bookTou Earning</th>   
		</tr> 
	</thead> 
		<tbody>

			 <?php
        $totalSale =  $totalDelivery  =  $totalPnd  =  $totalPnDCommission  = 0.0;
        $overallTotal = 0.0; 
       ?> 

      @foreach($data['report_data'] as $item) 
      
        <tr> 
          <td class="text-left" ><span class='badge badge-primary'>{{ $item['month'] }}</span></td>
    		  <td class="text-right">{{ $item['normalSale']  }} ₹</td>
    		  <td class="text-right"><span class='badge badge-success'>{{ $item['normalOrderCommission']  }} ₹</span></td>
         <td class="text-right">{{ $item['pndSale']  }}</td>
          <td class="text-right"><span class='badge badge-success'>{{ $item['pndOrderCommission']  }} ₹</span></td>
          <td class="text-right"><span class='badge badge-success'>{{  $item['normalOrderCommission'] + $item['pndOrderCommission']  }} ₹</span></td>
    		 </tr>

      <?php

        $totalSale +=  $item['normalSale'];  
        $totalDelivery += $item['normalOrderCommission'] ;
        $totalPnd +=  $item['pndSale'];  
        $totalPnDCommission += $item['pndOrderCommission'] ; 
        $overallTotal  += $item['normalOrderCommission'] + $item['pndOrderCommission'];

      ?>
       @if($item['month'] == date('F') &&  date('Y') == $data['year'] )
          @break
       @endif  
     @endforeach
 
	 <tr> 
          <td class="text-center" >Total</td>
          <td class="text-right">{{ $totalSale  }} ₹</td>
          <td class="text-right"><span class='badge badge-success'>{{ $totalDelivery  }} ₹</span></td>
         <td class="text-right">{{ $totalPnd  }} ₹</td>
          <td class="text-right"><span class='badge badge-success'>{{ $totalPnDCommission  }} ₹</span></td>
<td class="text-right"><span class='badge badge-success'>{{ $overallTotal  }} ₹</span></td>
           

         </tr>
	</tbody>
		</table>
	  
		
	</div>
 </div>


  </div> 
	
	</div> 

 <div class="col-md-12 grid-margin  ">
                

                <div class="card">
                  <div class="card-header">
                            <h4 class="card-title mb-0">Monthly Performance Graph</h4>
                   
                        </div>
                        <div class="card-body"> 
                    
                    <canvas class="mt-5" height="120" id="sales-statistics-overview"></canvas>
                  </div>
                </div>
 </div>
  



</div>

  

 
@endsection
 

@section("script") 

<script> 

 /* bar graph */

<?php
 
  
   $months  = array();
   $earnings  = array(); 

    foreach($data['report_data'] as $item)
    {
       $months[] =  '"'.  $item['month'] . '"' ;
       $earnings[] =  $item['normalOrderCommission'] + $item['pndOrderCommission'] ;
    }
  ?> 


if ($("#sales-statistics-overview").length) { 

    var barChartCanvas = $("#sales-statistics-overview").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: {
        

        labels: [ <?php echo implode(",",  $months ); ?> ],
        datasets: [{
          label: 'Revenue',
          data: [<?php echo implode(",",  $earnings ); ?> ],
          backgroundColor: ChartColor[0],
          borderColor: ChartColor[0],
          borderWidth: 0
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Sales by date',
              fontSize: 12,
              lineHeight: 2
            },
            ticks: {
              fontColor: '#bfccda',
              stepSize: 50,
              min: 0,
              max: 150,
              autoSkip: true,
              autoSkipPadding: 15,
              maxRotation: 0,
              maxTicksLimit: 10
            },
            gridLines: {
              display: false,
              drawBorder: false,
              color: 'transparent',
              zeroLineColor: '#eeeeee'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Total Earning by delivery',
              fontSize: 12,
              lineHeight: 2
            },
            ticks: {
              display: true,
              autoSkip: false,
              maxRotation: 0,
              fontColor: '#bfccda',
              stepSize: 5000,
              min: 1000 
            },
            gridLines: {
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul>');
          for (var i = 0; i < chart.data.datasets.length; i++) {
            console.log(chart.data.datasets[i]); // see what's inside the obj.
            text.push('<li>');
            text.push('<span style="background-color:' + chart.data.datasets[i].backgroundColor + '">' + '</span>');
            text.push(chart.data.datasets[i].label);
            text.push('</li>');
          }
          text.push('</ul></div>');
          return text.join("");
        },
        elements: {
          point: {
            radius: 0
          }
        }
      }
    });
    document.getElementById('bar-traffic-legend').innerHTML = barChart.generateLegend();
  }



 
 
</script> 


@endsection

