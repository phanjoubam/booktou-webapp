<footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                       <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright &copy; {{ date('Y') }}</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">All right reserved. Owned by <a href="https://booktou.in">bookTou.in</a></span>
                    </div>
                </div>
            </footer>