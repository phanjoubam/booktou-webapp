<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>bookTou Franchise - Dashboard</title>

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="manifest" href="{{ URL::to('/public/store/image') }}/site.webmanifest">
    <link rel="mask-icon" href="{{ URL::to('/public/store/image') }}/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::to('/public/store/image') }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('/public/store/image') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::to('/public/store/image') }}/favicon-16x16.png"> 
    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Ubuntu:300,400,500,700' rel='stylesheet' type='text/css' />



    <!-- Custom fonts for this template-->
   <link href="{{ URL::to('/public/assets/bs2') }}/vendor/fontawesome-free/css/all.min.css" 
                                                            rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ URL::to('/public/assets/bs2') }}/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="{{ URL::to('/public/assets/bs2') }}/css/jquery-ui.css" rel="stylesheet">
  <!--   <link href="{{ URL::to('/public/assets/bs2/vendor') }}/jexcel/jexcel.css" rel="stylesheet"> 
    <link href="{{ URL::to('/public/assets/bs2/vendor') }}/jsuites/dist/jsuites.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ URL::to('/public/assets/bs2') }}/css/style.css"> 
    <link href="{{ URL::to('/public/assets/bs2/vendor') }}/pn/css/pignose.calendar.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ URL::to('/public/assets/bs2/vendor/select2/css/select2.css') }}" /> 
</head>