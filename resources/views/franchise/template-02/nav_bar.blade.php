<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" 
            href="{{  URL::to('/franchise') }}">
                <div class="sidebar-brand-icon ">
                    <img class="img-fluid"  src="{{ URL::to('/public/assets/image/logo_white.png') }}" />
                </div>
               
            </a>
 
            <!-- Divider -->
            <hr class="sidebar-divider my-0"> 
            <!-- Nav Item - Dashboard --> 
            <li class="nav-item active">
                <a class="nav-link" href="{{  URL::to('/franchise') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li> 

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
              bookTou Franchise Module
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapsefirst"
                    aria-expanded="true" aria-controls="navCollapsefirst">
                   <i class="fa fa-shopping-basket"></i>
                    <span>Orders</span>
                </a>
                <div id="navCollapsefirst" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                        
            <a class="collapse-item" href="{{URL::to('/franchise/orders/normal-orders' )}}">Normal Orders</a>
            <a class="collapse-item"  href="{{  URL::to('/franchise/orders/view-completed' )}}">
              Completed Normal Orders
            </a>
            <a class="collapse-item"  href="{{  URL::to('/franchise/pick-and-drop-orders/view-all' )}}">
              Pick-And-Drop (PnD)
            </a>
             <a class="collapse-item"  href="{{  URL::to('/franchise/normal-orders/stats' )}}"> Order Stats Reports</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapsesecond"
                    aria-expanded="true" aria-controls="navCollapsesecond">
                    <i class="fas fa-box-open"></i>
                    <span>Businesses</span>
                </a>
                <div id="navCollapsesecond" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                         
            <a class="collapse-item"  href="{{  URL::to('/franchise/businesses/view-all' )}}">
             View Businesses
            </a>
          
 
 
            <a class="collapse-item"  href="{{  URL::to('/franchise/business/normal-orders/best-performers' )}}">
              Best Performers
            </a>
          
 
            <a class="collapse-item"  href="{{  URL::to('/franchise/business/pnd-orders/best-performers' )}}">
              Best PnD Performers
            </a>
          
                         
                    </div>
                </div>
            </li>


            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseThird"
                    aria-expanded="true" aria-controls="navCollapseThird">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Agents &amp; Staffs</span>
                </a>
                <div id="navCollapseThird" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                         
                      
            <a class="collapse-item" href="{{  URL::to('/franchise/staffs/manage-attendance' )}}">
               Staff Attendance
            </a>
         
        
            <a class="collapse-item" href="{{  URL::to('/franchise/delivery-agents' )}}">
               View Delivery Agents
            </a>
           
 
        
            <a class="collapse-item" href="{{  URL::to('/franchise/delivery-agent/performance-snapshot' )}}">
              Agents Performance
            </a>
        


 
            <a class="collapse-item"  target='_blank' href="{{  URL::to('/franchise/delivery-agents-live-locations' )}}">
              Live Location
            </a>
         


    
   <a class="collapse-item" 
   href="{{  URL::to('/franchise/delivery-agent/all-daily-orders-report' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Delivery Report
                </a>
           
                        
                    </div>
                </div>
            </li>


    

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseFourth"
                    aria-expanded="true" aria-controls="navCollapseFourth">
                    <i class="fas fa-user"></i>
                    <span>Customer</span>
                </a>
                <div id="navCollapseFourth" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                         
                          <a class="collapse-item" href="{{  URL::to('/franchise/manage-customers' )}}">
               All Customers
            </a>
                    </div>
                </div>
            </li>



            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseFifth"
                    aria-expanded="true" aria-controls="navCollapseFifth">
                    <i class="fas fa-chart-line"></i>
                    <span>Marketing &amp; Analytics</span>
                </a>
                <div id="navCollapseFifth" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                         
                         <a class="collapse-item"  href="{{  URL::to('/franchise/analytics/businesses/products/view-frequently-browsed-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Frequently Browsed Products
                </a>
 
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseSixth"
                    aria-expanded="true" aria-controls="navCollapseSixth">
                    <i class="fas fa-money-check-alt"></i> 
                    <span>Accounting</span>
                </a>
                <div id="navCollapseSixth" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
             

                  <a class="collapse-item"  href="{{  URL::to('/franchise/accounts/view-accounts-dashboard' )}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> Accounts Snapshot
                  </a>
              

             

                  <a class="collapse-item"  href="{{  URL::to('/franchise/accounts/add-deposit')}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> New Deposit
                  </a>
              
                  <a class="collapse-item"  href="{{  URL::to('/franchise/accounts/add-expenditure' )}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> Expenditure Entry
                  </a>
              
                  <a class="collapse-item"  href="{{  URL::to('/franchise/accounts/view-expenditure' )}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> View Expenditure
                  </a>
              

        </div>
                </div>

                

            </li>


            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseSeventh"
                    aria-expanded="true" aria-controls="navCollapseSeventh">
                    <i class="fas fa-clipboard-list"></i>
                    <span>Sales &amp; Stats</span>
                </a>
                <div id="navCollapseSeventh" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
             

                <a class="collapse-item"  href="{{  URL::to('/franchise/report/daily-sales-and-service' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales Volume
                </a>
               
              
                <a class="collapse-item"  href="{{  URL::to('/franchise/report/monthly-earning' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Monthly Sales Earning 
                </a>
              

              
                <a class="collapse-item"  href="{{  URL::to('/franchise/billing-and-clearance/daily-sales' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales & Service
                </a>
              
               
                <a class="collapse-item"  href="{{  URL::to('/franchise/report/sales-and-service-per-cycle' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Weekly Sales & Service
                </a>
              

                   
              

        </div>
                </div>

                

            </li>


<li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseEight"
                    aria-expanded="true" aria-controls="navCollapseEight">
                    <i class="fas fa-clipboard-list"></i>
                    <span>Billing &amp; Clearance</span>
                </a>
                <div id="navCollapseEight" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item"  
                href="{{  URL::to('/franchise/billing-and-clearance/weekly-sales-and-services-report' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Weekly Report
            </a>

            <a class="collapse-item"  
                href="{{  URL::to('/franchise/billing-and-clearance/migrated-order-sales-and-clearance-report' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Migrated Orders
            </a>

            <a class="collapse-item"  href="{{  URL::to('/franchise/report/pending-payments' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Payment Dues
                </a>

                <a class="collapse-item"  href="{{  URL::to('/franchise/sales-and-clearance-history' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Clearance History
                </a>

                

                   
              

        </div>
                </div>

                

            </li>

     <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseNinth"
                    aria-expanded="true" aria-controls="navCollapseNinth">
                    <i class="fas fa-cog"></i>
                    <span>System &amp; Config</span>
                </a>
                <div id="navCollapseNinth" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
             

                
                <a class="collapse-item"  href="{{  URL::to('/franchise/sytem/config/sales-target' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Configuration
                </a>
               
             
                   
              

        </div>
                </div>

                

            </li>        

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

             

        </ul>