@extends('layouts.franchise_theme_03')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?> 

  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header"> 
                 <div class="row">

                   <div class="col-md-7">
                  <h2 >
                    Unpaid Dues for merchants <span class='badge badge-primary'>{{ $endDay }}-{{ $month }}-{{ $year }}</span>  
                  </h2> 
                </div>

<div class="col-md-5 text-right"> 
  <form class="form-inline" method="get" action="{{ action('Franchise\FranchiseAccountsAndBillingController@weeklySalesAndServiceReport') }}"   >
     
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInput">Clearance Week</label>
      <select name='cycle' class="form-control form-control-sm  ">
          <option value='1'>1st Week</option>
          <option value='2'>2nd Week</option>
          <option value='3'>3rd Week</option>
          <option value='4'>4th Week</option>
          <option value='5'>last Week</option>
        </select>
    </div>
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInputGroup">Month</label>
       <select name='month' class="form-control form-control-sm  ">
          <option @if($month == 1) selected @endif   value='1'>January</option>
          <option @if($month == 2) selected @endif value='2'>February</option>
          <option @if($month == 3) selected @endif  value='3'>March</option>
          <option @if($month == 4) selected @endif  value='4'>April</option>
          <option @if($month == 5) selected @endif  value='5'>May</option>
          <option @if($month == 6) selected @endif  value='6'>June</option>
          <option @if($month == 7) selected @endif  value='7'>July</option>
          <option @if($month == 8) selected @endif  value='8'>August</option>
          <option @if($month == 9) selected @endif  value='9'>September</option>
          <option @if($month == 10) selected @endif  value='10'>October</option>
          <option @if($month == 11) selected @endif  value='11'>November</option>
          <option @if($month == 12) selected @endif  value='12'>December</option> 
        </select>
    </div>
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInputGroup">Year</label>
       <select name='year' class="form-control form-control-sm">
            <?php 
            for($i=date('Y'); $i >= 2020; $i--)
            {
                echo "<option value='$i'>" . $i . "</option>";
            }
            ?> 
        </select> 

      
    </div>
    <div class="col-auto">
       <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>
  </div>
  </form>

 
 </div> 
             
      </div>

       </div>
            
    
       <div class="card-body">    
    <table class="table">
      <thead>
        <tr>
          <th>BIN</th>
          <th>Business</th> 
          <th>Phone</th> 
          <th class='text-right'>PnD Amount</th> 
          <th class='text-right'>Normal Sales Amount</th> 
          <th class='text-right'>Sub-Total</th> 
          <th class='text-right'>Action</th>  
        </tr>
      </thead>
      
      <tbody>
      @php
        $grossTotal =0.00;
      @endphp
          @foreach ($businesses as $item)
          <tr>
            <td>{{ $item->id  }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->phone_pri }}</td>

              @php
                  $totalPnd =0.00;
              @endphp

              @foreach ($pnd_sales as $pnd)
                @if($pnd->request_by == $item->id )
                  @php
                    $totalPnd += $pnd->totalAmount;
                  @endphp
                  @break
                @endif 
              @endforeach

             <td class='text-right'>{{  $totalPnd }}</td> 

              @php
                  $totalNormal =0.00;
              @endphp

              @foreach ($normal_sales as $normal)
                @if($normal->bin == $item->id )
                  @php
                    $totalNormal += $normal->totalAmount;
                  @endphp
                  @break
                @endif 
              @endforeach


             <td class='text-right'>{{ $totalNormal }}</td>  
    

            @php
              $totalAmount = $totalPnd + $totalNormal; 
              $grossTotal +=$totalAmount;
            @endphp

          <td class='text-right'>{{ $totalAmount }}</td>  
 
            <td class='text-right'>
                <a class="btn btn-primary btn-sm" target='_blank' 
                href="{{ URL::to('/franchise/billing-and-clearance/sales-and-clearance-report')}}?bin={{$item->id}}&month={{ $month }}&year={{ $year }}">View Details</a>
            </td>
          </tr>
        @endforeach  
        <tr>
          <th colspan='5' class='text-right'>Total:</th>
          <th class='text-right'>{{ $grossTotal }}</th>  
          <th></th>
        </tr> 
      </tbody>

    </table>
 
 </div>


  </div> 
	
	</div> 
</div> 
 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

