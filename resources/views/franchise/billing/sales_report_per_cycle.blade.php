@extends('layouts.franchise_theme_03_hollow')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>


  <div class="row">
     <div class="col-md-12"> 
     <div class="card ">

       <div class="card-body" style='overflow-x: scroll;'>    
    <table class="table table-bordered" max-width='960px;'>
      <thead>
        <tr>
          <th colspan="6">
            <h4>Sales report for  @switch($cycle)
                @case(1)
                  <span class='badge badge-info'>First Cycle</span>
                @break
                @case(2)
                  <span class='badge badge-info'>Second Cycle</span>
                @break
                @case(3)
                  <span class='badge badge-info'>Third Cycle</span>
                @break
                @case(4)
                  <span class='badge badge-info'>Fourth Cycle</span>
                @break
                @case(5)
                  <span class='badge badge-info'>Last Cycle</span>
                @break

              @endswitch

              <button class="btn btn-primary" id="btn_graph_comparison">Show Graph</button>
              
            </h4>


              
          </th>
          <th colspan="6">
  <form class="form-inline" method="get" action="{{ action('Franchise\FranchiseAccountsAndBillingController@cyclewiseSalesServiceReport')}}">
     
  <div class="form-row">
    <div class="col">
      <label class="sr-only" for="inlineFormInput">Service Week</label>
      <select name='cycle' class="form-control form-control-sm  ">
          <option value='1' <?php if( $cycle == 1 ) echo "selected"; ?> >1st Week</option>
          <option value='2' <?php if( $cycle == 2 ) echo "selected"; ?>>2nd Week</option>
          <option value='3' <?php if( $cycle == 3 ) echo "selected"; ?>>3rd Week</option>
          <option value='4' <?php if( $cycle == 4 ) echo "selected"; ?> >4th Week</option>
          <option value='5' <?php if( $cycle == 5 ) echo "selected"; ?>>last Week</option>
        </select>
    </div>
   <div class="col">
      <label class="sr-only" for="inlineFormInputGroup">Month</label>
       <select name='month' class="form-control form-control-sm  ">
          <option <?php if( $month  == 1 ) echo "selected"; ?> value='1'>January</option>
          <option <?php if( $month== 2 ) echo "selected"; ?>  value='2'>February</option>
          <option <?php if( $month == 3 ) echo "selected"; ?> value='3'>March</option>
          <option <?php if( $month== 4 ) echo "selected"; ?> value='4'>April</option>
          <option <?php if( $month == 5 ) echo "selected"; ?> value='5'>May</option>
          <option <?php if( $month == 6 ) echo "selected"; ?> value='6'>June</option>
          <option <?php if( $month == 7 ) echo "selected"; ?> value='7'>July</option>
          <option <?php if( $month == 8 ) echo "selected"; ?> value='8'>August</option>
          <option <?php if( $month == 9 ) echo "selected"; ?> value='9'>September</option>
          <option <?php if($month == 10 ) echo "selected"; ?> value='10'>October</option>
          <option <?php if( $month== 11 ) echo "selected"; ?> value='11'>November</option>
          <option <?php if( $month == 12 ) echo "selected"; ?> value='12'>December</option> 
        </select>
    </div>
   <div class="col">
      <label class="sr-only" for="inlineFormInputGroup">Year</label>
        <input readonly name='year' class="form-control form-control-sm " value="{{ date('Y') }}" /> 
    </div>
 <div class="col">
     
       <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>
  </div>
  </form>

          </th>
         
        </tr>
        <tr>
          <th>Order No</th> 
          <th>Date</th> 
          <th>Order Type | Source</th>
          <th class='text-center'>Status | Mode | Target</th> 
          <th class='text-right'>Sale Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Commission</th>
          <th class='text-right'>Delivery</th> 
          <th class='text-right'>Due on Merchant</th> 
          <th class='text-right'>Earning</th> 
        </tr>
      </thead>
      
      <tbody>
        @php 
          $packagingCost = $commission = $dueOnMerchant = $deliveryFee = $orderCost = $earning = $commissionPc = 0.00;
          $totalCash = $totalOnline =  $totalCollectedAtMerchant = $totalDueOnMerchant =$totalSale = $totalFee = $totalCommission = $totalEarning = $totalDeliveryFee = $totalOnlineMerchant = $totalPackagingCost =0.00; 
        @endphp
          @foreach ($results as $normal)

            @php 
              $business = "";
            @endphp
            @foreach($businesses as $bitem)

                @if( $normal->bin == $bitem->id )
                  @php 
                    $business = $bitem;
                  @endphp
                  @break
                @endif
 
            @endforeach
           

           @if($normal->bookingStatus == "delivered") 

            <tr>
                
                @if($normal->orderType == "normal")
                  @php 
                    $tempPackagingCost = 0.00;
                  @endphp
                  @foreach($cart_items as $cart)
                    @if($normal->id == $cart->order_no)
                      
                      @php 
                        $tempPackagingCost += $cart->qty * $cart->package_charge;
                      @endphp

                    @endif
                  @endforeach

                  @php 
                      $normal->packingCost  = $tempPackagingCost;
                  @endphp

                  <td><a target='_blank' href="{{ URL::to('/franchise/order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td> 
                  <td class='text-center'><span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
                  </td> 
                  <td class='text-center'>  
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0)
                     <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                     <span class='badge badge-primary'>CASH</span> - 
                    @endif 
                    <span class='badge badge-success'>bookTou</span>
                  </td>
   

                  @php
                      $dueOnMerchant =0.00;
                      $commissionPc = ($business == null) ? 0 : $business->commission ; 
                      $orderCost = $normal->orderCost;
                      $packagingCost = $normal->packingCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalPackagingCost += $packagingCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning;  

                  @endphp
 

                  @if( strcasecmp($normal->paymentType , "online") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                    @php 
                      $totalOnline += $normal->orderCost  ;  //packing cost already included
                    @endphp
                  @else 
                    @php
                      $totalCash += $normal->orderCost  ;  //packing cost already included  
                    @endphp
                  @endif 

 
                  <td class='text-right'>{{ number_format( $orderCost, 2 )   }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format( $deliveryFee, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $dueOnMerchant, 2 ) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td> 


                @elseif($normal->orderType == "pnd")

                  <td><a target='_blank'  href="{{ URL::to('/franchise/pnd-order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                   <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-primary'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>
                      {{  ($normal->source=="business" || $normal->source=="merchant" ) ? "Merchant" : "Customer" }}
                    </span>
                  </td>
                  <td class='text-center'>  
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                     <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                     <span class='badge badge-primary'>CASH</span> - 
                    @endif 
                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                      <span class='badge badge-warning'>Merchant</span>
                    @else 
                       @if( strcasecmp($normal->paymentType , "ONLINE") == 0 &&  ( $normal->source == "business" || $normal->source == "merchant" )  )
                          <span class='badge badge-danger'>bookTou</span>
                       @else 
                          <span class='badge badge-success'>bookTou</span>
                       @endif
                      
                    @endif
                  </td>
 

                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)
 
                    @php
                      $dueOnMerchant =0.00;       
                      $commissionPc = ($business == null) ? 0 : $business->commission ; 
                      $orderCost = $normal->orderCost;
                      $packagingCost = $normal->packingCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning; 
                    @endphp 

                    @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 ) 
                          @php 
                            $dueOnMerchant  = $normal->deliveryCharge; 
                            $totalCollectedAtMerchant += $normal->orderCost;  
                            $totalPackagingCost += $packagingCost;
                            $totalDueOnMerchant += $dueOnMerchant; 
                            $totalOnlineMerchant += $normal->orderCost + $normal->packingCost;
                          @endphp
                        @else
                            @php
                              $totalOnline += $normal->orderCost + $normal->packingCost;
                            @endphp  
                        @endif  
                   @else
                        @php 
                          $dueOnMerchant  = 0.00; 
                          $totalCash += $normal->orderCost + $normal->packingCost;   
                        @endphp     
                    @endif  

                  @else 
  
                      @php
                            $commissionPc = 0.00 ; 
                            $orderCost = $normal->orderCost;
                            $packagingCost = $normal->packingCost;
                            $deliveryFee = $normal->deliveryCharge;
                            $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                            $earning =  $deliveryFee +  $commission; 
                            $totalSale += $orderCost;
                            $totalPackagingCost += $packagingCost;
                            $totalFee += $deliveryFee;
                            $totalCommission += $commission;
                            $totalEarning += $earning;  
                      @endphp 

                    @if( strcasecmp($normal->paymentType , "online") == 0  )   
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 )  
                          @php
                             $totalOnlineMerchant += $normal->orderCost;  
                             $totalCollectedAtMerchant += $normal->orderCost; 
                             $totalPackagingCost += $normal->packingCost; 
                             $totalDueOnMerchant += $normal->deliveryCharge; 
                             $dueOnMerchant  = $normal->deliveryCharge;  
                          @endphp  
                        @else
                          @php
                            $dueOnMerchant  = 0.00;
                            $totalOnline += $normal->orderCost + $normal->packingCost;
                          @endphp 
                        @endif 
                    @else
                        @php 
                          $dueOnMerchant  = 0.00;
                          $totalCash += $normal->orderCost + $normal->packingCost;   
                        @endphp     
                    @endif   

                  @endif 
                   
                
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td>  

                @elseif($normal->orderType == "assist")

                  <td><a target='_blank' href="{{ URL::to('/franchise/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
                  </td>
                  <td class='text-center'>
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> -  
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                      <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                      <span class='badge badge-primary'>CASH</span> - 
                    @endif
                  
                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                      <span class='badge badge-danger'>Merchant</span> - 
                    @else 
                      <span class='badge badge-success'>bookTou</span> - 
                    @endif  
                  </td> 



                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                    @php
                      $dueOnMerchant =0.00;
                      $totalDueOnMerchant = 0.00;
                      $totalCollectedAtMerchant = 0.00;
                      $packagingCost = 0.00;                  
                    @endphp
                      
                    @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0 || strcasecmp($normal->paymentTarget , "business") == 0) 
                          @php
                            $dueOnMerchant = $normal->deliveryCharge;
                            $totalCollectedAtMerchant += $normal->orderCost;  
                            $totalDueOnMerchant += $dueOnMerchant; 
                            $totalOnlineMerchant += $totalCollectedAtMerchant;  
                          @endphp 
                        @else
                          @php
                            $totalOnline += $normal->orderCost;
                          @endphp  
                        @endif
                    @else
                          @php 
                            $totalCash += $normal->orderCost;  
                          @endphp 
                        
                    @endif

                    @php 
                      $commissionPc =  ($business == null) ? 0 : $business->commission ; 
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * $orderCost ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning; 
                    @endphp 
 

                  @else 


                    @php
                      $dueOnMerchant =0.00;
                      $commissionPc =  0.00 ; 
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * $orderCost ) / 100 ;
                      $earning =  $deliveryFee +  $commission; 
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning;
                      $totalDueOnMerchant = 0.00;
                      $totalCollectedAtMerchant = 0.00; 

                    @endphp 


                  @endif

             
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td>  

                @endif   

              </tr>

           @else

            <tr> 
              
                @if($normal->orderType == "normal")
                  <td><a target='_blank' href="{{ URL::to('/franchise/order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td> 
                  <td class='text-center'><span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                @elseif($normal->orderType == "pnd")
                  <td><a target='_blank'  href="{{ URL::to('/franchise/pnd-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-info'>{{ $normal->orderType  }}</span> by 
                @else
                  <td><a target='_blank'  href="{{ URL::to('/franchise/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                @endif 
                <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
              </td>
              <td  class='text-center'>
                <span class='badge badge-danger'>{{ $normal->bookingStatus }}</span> - 
                @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                  <span class='badge badge-info'>ONLINE</span> - 
                @else 
                  <span class='badge badge-primary'>CASH</span> - 
                @endif
              
                @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                  <span class='badge badge-danger'>Merchant</span>
                @else 
                  <span class='badge badge-success'>bookTou</span>
                @endif
              </td>
              <td class='text-right'>0 %</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td>  
          </tr> 

           @endif
  
        @endforeach
     
         <tr>
          <th>Order No</th> 
          <th>Date</th> 
          <th>Order Type | Source</th>
          <th class='text-center'>Status | Mode | Target</th> 
          <th class='text-right'>Sale Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Commission</th>
          <th class='text-right'>Delivery</th> 
          <th class='text-right'>Due on Merchant</th> 
          <th class='text-right'>Earning</th> 
        </tr> 
        <tr>
            <th colspan='4' class='text-right'>Sum Totals</th> 
            <th class='text-right'>A = {{ number_format($totalSale,2, '.', '') }}</th>
            <th class='text-right'>B = {{ number_format($totalPackagingCost,2, '.', '') }}</th>
            <th class='text-right'>C = {{ number_format($totalCommission,2, '.', '') }}</th> 
            <th class='text-right'>D = {{ number_format($totalFee,2, '.', '') }}</th> 
            <th class='text-right'>E = {{ number_format($totalDueOnMerchant,2, '.', '') }}</th>
            <th class='text-right'>F = {{ number_format($totalEarning,2, '.', '') }}</th>  
        </tr> 
<tfoot>
        
</tfoot> 
      </tbody> 
    </table> 
<hr/>
<form>
    <div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Collected at Merchant:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalCollectedAtMerchant ,2, '.', '') }}"  >
    </div> 
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Due towards Merchant:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalDueOnMerchant ,2, '.', '') }}"  >
    </div>

     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Commission to deduct:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalCommission ,2, '.', '') }}"  >
    </div>


  </div>

<div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Cash Collected at bookTou:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalCash ,2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Online Collected at bookTou:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalOnline ,2, '.', '') }}"  >
    </div>

     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Total To Clear:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm is-valid" readonly value="{{ number_format( ( $totalCash + $totalOnline - $totalCommission - $totalDueOnMerchant)  ,2, '.', '') }}"  >
    </div>
 
  </div>


</form>

 </div> 
  </div> 
  
  </div> 







 


</div>





 </div>
 
 
<div class="modal fade" id="widget-graph" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="widget-graph" aria-hidden="true" style="width: 100%;">
  <div class="modal-dialog modal-dialog-centered" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="">
         <div class="col-md-12 grid-margin  mt-2">
                

                <div class="card">
                  <div class="card-header">
                            <h4 class="card-title mb-0">
                Sales graph for  @switch($cycle)
                @case(1)
                  <span class='badge badge-info'>First Cycle</span>
                @break
                @case(2)
                  <span class='badge badge-info'>Second Cycle</span>
                @break
                @case(3)
                  <span class='badge badge-info'>Third Cycle</span>
                @break
                @case(4)
                  <span class='badge badge-info'>Fourth Cycle</span>
                @break
                @case(5)
                  <span class='badge badge-info'>Last Cycle</span>
                @break

              @endswitch</h4>
                   
                        </div>
                        <div class="card-body"> 
                    

                    <canvas id="myChart"></canvas>


                  </div>
                </div>
 </div>
      </div>
      <div class="modal-footer">
         
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
 
@endsection



@section("script")

<script>
//bar chart section for normal, pnd and assist
  <?php
 
  
   $days  = array();
   $normalamount  = array(); 
   $pndamount  = array(); 
   $assistamount  = array(); 

    foreach($day as $item)
    {
       
       $days[] =  $item;
      
    }

    foreach($normal_sales_chart as $item)
    {
       
       $normalamount[] =  $item;
      
    }

    foreach($pnd_sales_chart as $item)
    {
       
       $pndamount[] =  $item;
      
    }

    foreach($assist_sales_chart as $item)
    {
       
       $assistamount[] =  $item;
      
    }


  ?>
 
var ctx = document.getElementById("myChart").getContext("2d");

var data = {
  labels: <?php echo json_encode($days);?>,
  datasets: [{
    label: "normal",
    backgroundColor: "blue",
    data: <?php echo json_encode($normalamount);?>
  }, {
    label: "pnd",
    backgroundColor: "green",
    data: <?php echo json_encode($pndamount);?>
  }, {
    label: "assist",
    backgroundColor: "yellow",
    data: <?php echo json_encode($assistamount);?>
  }]
};

var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    }
  }
});
//bar chart ends here

 $(document).on("click", "#btn_graph_comparison", function()
{ 
    $('#widget-graph').modal('show');
})
 
 
 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    }); 

});

    
  

</script>  


@endsection

