@extends('layouts.franchise_theme_03')
@section('content')

<div class="row">
<div class="col-md-12">  

	
     <div class="card  ">  
      <div class="card-header">
      <div class="form-row"> 
      	<div class="col-md-6">
      	<h3>System Configuration:</h3>
		</div>
		<div class="col-md-6"> 
      	@if (session('err_msg'))
    <div class="col alert alert-warning">
        {{ session('err_msg') }}
    </div>
  
	@endif
	  </div>
	</div>
      </div>
  </div>
</div>
</div>

<div class="row">
<div class="col-md-12">	

<div class="card">

<div class="card-body"> 

<form  method="post" 
action="{{  action('Franchise\FranchiseDashboardControllerV1@salesTarget')}}">
                      {{ csrf_field() }}
  <table class="table">
         <thead class=" text-primary">
          <tr > 
            <th scope="col" class="text-left">Maximum Target</th>
            <th scope="col" class="text-center">Minimum Target</th>    
          </tr>
        </thead>
  
    <tbody>
    
     
     <tr id='tr-'>  
     <td class="text-left"> 
      <input class='col-md-4 form-control' type="text" name='maximum' placeholder="maximum target"   value="@if(isset($target)){{$target->max}}@else @endif" required="" /> 
    </td>  
     <td class="text-right">
      <div class="form-row">
      <div class="col-md-8 text-right"> 
      
      <input class='col-md-4 form-control' type="text" name='minimum' placeholder="minimum target" 
      value="@if(isset($target)){{$target->min}}@else @endif"required="" />
        </div>
        <div class="col-md-4 text-left">
        <button type="submit" class="btn btn-primary" value='Save' name='btn_save'>
          <i class='fa fa-save'></i>
        </button> 
        </div>
      </div>

      </td>  
     </tr>
  
   
    </tbody>

</table>       

</form>

</div>

</div>
</div>
</div>
@endsection
@section("script")

<script>




</script> 

@endsection 
