@extends('layouts.franchise_theme_03')
@section('content')

 


   <div class="row">
     <div class="col-md-12"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="row">
                <div class='col-md-8'>
                  <h2>Completed Orders</h2>
                </div>
                <div class='col-md-4'>
                   <form class="form-inline" method="post" action="{{  action('Franchise\FranchiseDashboardControllerV1@viewCompletedOrders') }}">

            {{ csrf_field() }}
              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Order Date:</label> 
                <input   class='form-control form-control-sm  my-1 mr-sm-2  calendar' type="text"  name='orderdate'  /> 
                <button type="submit" class="btn btn-primary btn-xs my-1  " value='search' name='btn_search'>Search</button>
            </form>
                </div>
                </div> 
            </div>
       <div class="card-body"> 
  
   <div class="table-responsive">
    <table class="table">
                    <thead class=" text-primary">  
                  <tr class="text-center" style='font-size: 12px'>  
                     <th scope="col">Order No.</th>
                    <th scope="col">Shop</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Assigned To</th> 
                    <th scope="col">Payment</th> 
                    <th scope="col">Status</th>   
                    <th scope="col">Cost</th>
                    <th scope="col">Action</th>  
                  </tr>
                </thead> 
           <tbody>
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                {

                
                ?>
  
                  <tr >  
                    <td>
                       <a class='badge badge-info' href="{{ URL::to('/franchise/order/view-details') }}/{{ $item->id }}" target='_blank'>
                    {{$item->id}}
                     </a>

                     
                  <td>
                    <?php 
                    foreach ($data['businesses'] as $business)
                      {    
                          if($business->id == $item->bin)
                          {
                            echo "<strong>". $business->name . "</strong>";
                            echo "<br/><small><i class='fa fa-phone'></i> " .  $business->phone_pri  . "</small>";
                            break;
                          }

                        }
 
                    ?>
 
                    

                  </td>


                   <td>
                    <strong>{{$item->fullname}}</strong><br/><small><i class='fa fa-phone'></i> {{ $item->phone }}</small>
                  </td>

                  <td>
                    <strong>{{$item->agentName}}</strong><br/><small><i class='fa fa-phone'></i> {{ $item->phone }}</small>
                  </td> 
                
                 <td>             
    @if( strcasecmp($item->payment_type,"Cash on delivery") == 0  || strcasecmp($item->payment_type,"COD") == 0     )
      COD
    @else @if( strcasecmp($item->payment_type,"POD") == 0   )
            POD
          @else @if(  strcasecmp($item->payment_type,"OTC") == 0   )
                    OTC 
                @else
                  ONLN
                @endif
          @endif  
    @endif
    </td> 
    <td>
                  @if($item->book_status == "delivered" )
                  <strong class='badge badge-info'>{{$item->book_status}}</strong>
                  @else 
                  <strong class='badge badge-danger'>{{$item->book_status}}</strong>
                  @endif 
                    
                </td> 
                <td>{{ $item->total_cost }}</td>

                <td>
                  <a  class="btn btn-primary btn-xs" target='_blank' href="{{ URL::to('/franchise/order/view-details') }}/{{$item->id}}">View</a> 
                </td>

                <td>  
                <?php 


                ?> 
                  </td>
                    
                       <td>
                          
                       </td>  
                                      
    
   
                         </tr>
                         <?php
                          $i++; 
                       }

                          ?>
                </tbody>
                  </table>
            
              </div>
            </div>
          </div>
        </div>  
   </div>
 
   

<div class="modal hide fade modal_processing"   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Processing Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"> 
        <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn_action" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modalTaskList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
  <img src='{{ URL::to("/public/assets/image/no-image.jpg") }}' id='apimg' width="40px" height="40px" class="img-rounded" style='margin-right:10px; display:inline-block'/>
<h5 class="modal-title" id="exampleModalLongTitle">Delivery Tasks for <strong id='span_anm'></strong></h5>
  

        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content_area">
         <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



@endsection

@section("script")

<script>


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
            


        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 

    
    $('.modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('.modal_processing').modal({
      show:true,
      keyboard: false,
      backdrop: 'static'
    });


    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('.modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
   } 

   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/update-customer-confirmation" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.browser_refresh == "yes")
            location.reload(); 
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });
 

})

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script> 

@endsection 