@extends('layouts.franchise_theme_03')
@section('content')

 
  <?php
    $order_info = $data['order_info'];  
    $agent = $data['agent_info'];   
    $order_no =$order_info->id ; 


  ?> 
 


 <div class="row"> 

    @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif
 
 @if( $order_info->milestone != "" && $order_info->milestone != null  )
 
  <div class="col-md-12">
  
       <p class='alert alert-success h2'><i class='fa fa-trophy  '></i>  {{ $order_info->milestone }}</p>
 
  </div>  
    @endif

  
 <div class="col-md-5">    
     <div class="card panel-default">
           <div class="card-header"> 
            <div class="row">
              <div class="col-md-6"> 

                 <h5 class="card-category">PnD # {{ $order_info->id  }}
                  @switch($order_info->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 
                      @case("canceled")
                        <span class='badge badge-danger'>Cancelled</span>
                      @break 
                      @endswitch  
                    </h5> 

                  </div>

                  

                    </div>

            </div>
       <div class="card-body">
        <!--  @if($order_info->route_to_frno==0)
           
          <h5>Routed to: <span class="badge badge-success badge-pill">bookTou HQ</span></h5>
          @endif  -->

        
         
              Ordere Date <span class='badge badge-primary'>{{ date('d-m-Y H:i:s', strtotime( $order_info->book_date)) }}</span> 
              Service Date <span class='badge badge-success'>{{ date('d-m-Y', strtotime( $order_info->service_date)) }}</span>
           <hr/>  
              <span>Order is from </span> 
              @if(  strcasecmp($order_info->source,"business") == 0 || 
                strcasecmp($order_info->source,"merchant") == 0    )
                <span class='badge badge-info float-right'>Merchant</span>
              @else 
                <span class='badge badge-primary float-right'>bookTou</span>
              @endif
            <hr/>  
              <span>Paid</span> 
               <span class='float-right'>
              @if( strcasecmp($order_info->pay_mode,"cash") == 0    )
                <span class='badge badge-warning '>CASH</span>
              @else 
                <span class='badge badge-success'>ONLINE</span>
              @endif
              to 
              @if( strcasecmp($order_info->payment_target,"business") == 0 || 
              strcasecmp($order_info->payment_target,"merchant") == 0   )
                <span class='badge badge-danger'>Merchant</span>
              @else 
                <span class='badge badge-success'>bookTou</span>
              @endif
            </span>
              <hr/> 
              Merchat Cost: <span class='badge badge-info float-right'>{{ number_format( $order_info->total_amount,2) }}</span>
              <hr/> 
              Packaging Cost: <span class='badge badge-primary float-right'>{{ number_format( $order_info->packaging_cost,2) }}</span> 
              <hr/>
              bookTou Service Fee: <span class='badge badge-primary float-right'>{{  number_format( $order_info->service_fee,2) }}</span>
              <hr/> 
              Total amount to collect: <span class='badge badge-success float-right'>{{ number_format(  
                $order_info->service_fee + $order_info->total_amount + $order_info->packaging_cost,2) }}</span>
              <hr/>   
              Download Invoice: <a class='btn btn-success btn-sm float-right' type='button' target='_blank' href="{{ URL::to('/pos/orders/view-bill') }}?o={{ $order_info->id }}" >Invoice</a>
               <hr/> 

              <strong>Order Details</strong><br/>
              @if($order_info->pickup_details !="")
                {{ $order_info->pickup_details}}
              @else 
                No Order details Provided.
              @endif  
        
              </div>

 <div class="card-footer">

    
    <button class='btn btn-success btn-sm btnpoppaybox' type='button' 
     data-paymode='{{  $order_info->pay_mode  }}'
    data-paytarget='{{  $order_info->payment_target  }}'
    data-source='{{  $order_info->source  }}'
    data-key='{{ $order_info->id   }}' >Edit Order</button>

    <button class='btn btn-primary btn-sm showremmodal' type='button' 
    data-key='{{ $order_info->id  }}'  >Remarks</button> 

    <button class='btn btn-warning btn-sm btnchangestatus' type='button' 
    data-key='{{ $order_info->id   }}'  >Edit Status</button>
  
   <button class='btn btn-danger btn-sm btnchangetofranchise' type='button' 
   data-key='{{ $order_info->id   }}'>Migrate</button> 

   <a class="btn btn-primary btn-sm  btnchangezone" data-key='{{ $order_info->id  }}' href="#">Edit Source Zone</a>  


 </div> 

            </div> 

 </div>



 
    <div class="col-md-4">   
          <div class="card panel-default">
       <div class="card-header">
                <h5 class="card-category">Cash Memo/Receipt</h5> 
                </div>
 <div class="card-body text-center">  
                 @if($order_info->order_receipt != null)      
   
   <a data-src="{{ $order_info->order_receipt }}"  class="btn btn-link btnViewRcpt" href='#'  >
    <img src="{{  $order_info->order_receipt }}" width='100' class="img-fluid" alt="Responsive image">
    <br/>
        <br/>
    
   </a>
<small>Click on the image enlarge</small>
     @else 
     <p class='alert alert-info'>No order receipt uploaded.</p>
      @endif
       </div>
    </div>


     

    <div class="card panel-default mt-3">
       <div class="card-header">
          <h5 class="card-category">Remarks History</h5> 
        </div>
    <div class="card-body ">  
      
      @if($order_info->agent_remarks != null) 
       <table>
        <tr> 
          <td>
            <span class='badge badge-danger'>Agent Remarks</span><br/>
            {{  $order_info->agent_remarks }}</td> 
        </tr>
        </table> 
      @endif


      @if(isset( $data['remarks'] ) && count($data['remarks']) > 0)
                  <ul class="list-group">
                    @foreach($data['remarks'] as $rem)
                      <li class="list-group-item">
                          @if($rem->remark_by == 0)
                            @php 
                              $staff_name = "Admin";
                            @endphp
                          @else
                            @php 
                              $staff_name = "Untracked";
                            @endphp
                            @foreach($data['staffs'] as $staff)
                              @if($staff->id == $rem->remark_by)
                                @php 
                                  $staff_name = $staff->fullname;
                                @endphp
                                @break
                              @endif
                            @endforeach
                          @endif 
                          <span class='badge badge-danger badge-pill'>{{ $rem->rem_type}}</span> entered by 
                          <span class='badge badge-info badge-pill'>{{ $staff_name}}</span>
                          <br/> 
                          <i class="fa fa-tick"></i>{{ $rem->remarks}}</td> 
                      </li>
                    @endforeach
                  </ul> 
                  @else 
                  <p class='alert alert-info'>No remarks recorded.</p>
      @endif 


      
       </div>
    </div>

 

      @if(isset($agent))
              <div class="card card-default mt-2">
              <div class="card-header">
                <h5 class="card-category">Delivery Agent</h5>
                
                </div>
              <div class="card-body"> 
                <h4 class="card-title">{{$agent->deliveryAgentName}}</h4>
                  <p><i class='fa fa-phone'></i> {{$agent->deliveryAgentPhone}}</p>  


                  </div>
              <div class="card-footer">
                 <button class='btn btn-danger btn-sm btnRemoveAgent' data-key='{{ $order_info->id   }}'>Remove Agent</button>
              </div>
            </div>
 
                @else  

              <div class="card card-default mt-2">
              <div class="card-header">
                <h5 class="card-category">Delivery Agent</h5> 
                </div>
              <div class="card-body"> 
                 <div class="input-group mb-3">
                    
                    <select   class='form-control  ' id="aid_{{$order_info->id }}" name='agents' >
                      @foreach($data['all_agents'] as $aitem) 
                        <option value='{{ $aitem->id }}'   >{{ $aitem->fullname }}</option>
                      @endforeach 
                    </select>
                   <div class="input-group-append"> 
                     
                   <button class="btn btn-primary btn-assign" data-oid="{{$order_info->id}}">
    
                     Assign
                   </button>
                    </div>
                  </div>

                  </div>
              
            </div>


   @endif



  </div>

 <div class="col-md-3">   
 

<div class="card panel-default">
              <div class="card-header">
                <h5 class="card-category">Pick & Drop Locations</h5> 
                </div>
              <div class="card-body"> 


                 <div class="timeline timeline-xs">
                  <div class="timeline timeline-xs">
                    <div class="timeline-item">
                      <div class="timeline-item-marker">
                        <div class="timeline-item-marker-text"><span class='badge badge-danger badge-pill'>Pickup from</span></div>
                        <div class="timeline-item-marker-indicator bg-red"></div>
                      </div>
                      <div class="timeline-item-content">
                        {{$order_info->businessName}}<br/>
                        {{$order_info->businessLocality}}<br/>
                        {{$order_info->businessLandmark}}, {{$order_info->businessCity}}<br/>
                        {{$order_info->businessState}} - {{$order_info->businessPin}}<br/>
                        <i class='fa fa-phone'></i> {{$order_info->businessPhone}}
                      </div>
                    </div>

                     <div class="timeline-item">
                      <div class="timeline-item-marker">
                        <div class="timeline-item-marker-text"><span class='badge badge-success badge-pill'>Drop At</span></div>
                        <div class="timeline-item-marker-indicator bg-green"></div>
                      </div>
                      <div class="timeline-item-content">
                        {{$order_info->drop_name}}<br/>
                        {{$order_info->drop_address}}<br/>
                        {{$order_info->drop_landmark}}<br/>
                        <i class='fa fa-phone'></i> {{$order_info->drop_phone}}
                      </div>
                    </div>  
                  </div>
                </div>
 

 
              </div> 
            </div> 
             
                

            @if(isset($agent))
              <div class="card card-default mt-2">
              <div class="card-header">
                <h5 class="card-category">Delivery Agent</h5>
                <h4 class="card-title">{{$agent->deliveryAgentName}}</h4>
                </div>
              <div class="card-body"> 
                 <p>
                  <i class='fa fa-phone'></i> {{$agent->deliveryAgentPhone}}</p> 
              </div>
              <div class="card-footer">
                 <button class='btn btn-danger btn-sm btnRemoveAgent' data-key='{{ $order_info->id   }}'>Remove Agent</button>
              </div>
            </div>

            @endif


          </div>  

       </div> 
 

 



<form action="{{ action('Admin\AdminDashboardController@removeCouponDiscount') }}" method="post">
  {{ csrf_field()  }} 
 <div class="modal" id='widget-rem-coupon' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title">Discount Coupon Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body-content" style='padding: 10px;'>  
         
   <p>You are about to remove discount coupon from this order. Are you sure?</p> 
     
        <input type="hidden" id="key" name="key" >
<div class="clear"></div>
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span> 
        <button type="submit" class="btn btn-success" name="btnsave"  value='save'>Remove</button>
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Cancel</button> 
      </div>
    </div> 
  </div>
</div>
</form>


<div class="modal fade" id="modalConfDelAgent" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Admin\AdminDashboardController@removeAgentFromNormalOrder') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to remove agent from this delivery task?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key2'/> 
        <button type="submit" name="btnRemoveAgent" value="save" class="btn btn-primary btn-sm">Yes</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
 </form>
</div>

 
<div class="modal" id='modalViewReceipt' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Enlarged Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="img-fluid" width='600'  src="" id='imgreceipt' alt='Order Receipt' /> 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'> 
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
 


 

<form action="{{ action('Franchise\FranchiseDashboardControllerV1@updatePnDOrderPaymentUpdate') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalpayment" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Order Information Update</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <div class="form-row">
           <div class="form-group col-md-6">
            <label for="ordersource">Order Source</label>
            <select name="ordersource" class="form-control" id="ordersource">
                <option value='customer'>Customer</option>
                <option value='booktou'>bookTou</option>
                <option value='business'>Merchant</option> 
            </select>
          </div>
</div>
        

    <div class="form-row">

     <div class="form-group col-md-3">
      <label for="servicedate">Service Date:</label>
      <input type="text" class="form-control calendar" id="servicedate" name='servicedate' 
      value="{{ date('d-m-Y', strtotime($order_info->service_date)) }}">
      <small class='red '>(date of delivery)</small>
    </div>

    <div class="form-group col-md-3">
      <label for="delivery">Service Fee:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="delivery" name='delivery' value='{{ $order_info->service_fee }}'>
      <small class='red'>(bookTou fee)</small>
    </div>

    <div class="form-group col-md-3">
      <label for="total">Order Cost:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="total" name='total' value="{{ $order_info->total_amount }}">
      <small class='red'>(total item cost)</small>
    </div>
 <div class="form-group col-md-3">
      <label for="total">Packaging Cost:</label>
      <input type="number" min='0' step='0.1'  class="form-control" id="packcost" name='packcost' value="{{ $order_info->packaging_cost }}">
      <small class='red'>(if any)</small>
    </div>
    
  </div>
  
        
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="paymode">Payment Type:</label>
        <select name="paymode" class="form-control" id="paymode">
            <option value='cash'>Cash</option>
            <option value='online'>ONLINE</option> 
        </select>
      </div>
      <div class="form-group col-md-6">
        <label for="paytarget">Payment Target</label>
        <select name="paytarget" class="form-control" id="paytarget">
            <option value='booktou'>bookTou</option>
            <option value='merchant'>Merchant</option> 
        </select>
      </div>
    </div>
    <div class="form-group">
            <label for="remarks">Order Details</label> 
            <textarea class="form-control" id="orderdetails" name='orderdetails' rows="2">{{ $order_info->pickup_details != "" ? $order_info->pickup_details : "No Order details Provided."}}</textarea>

          </div> 
    <div class="form-group">
            <label for="remarks">Order Editing Remarks <span style='color:red'>(important field)</span></label> 
            <textarea class="form-control" id="remarks" name='remarks' rows="2"></textarea>

          </div> 

    </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key4'/> 
           
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 

        </div>
      </div>
    </div>
  </div>
</form>


<form action="{{ action('Franchise\FranchiseDashboardControllerV1@updatePnDOrderStatus') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalstatus" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Order Status Update</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             
        
    <div class="form-row"> 
      <div class="form-group col-md-6">
        <label for="status">Order Status</label>
        <select name="status" class="form-control" id="status">
            <option value='new'>Make it New</option>
            <option value='delivered'>Delivered</option>
            <option value='returned'>Returned</option> 
            <option value='canceled'>Canceled</option>  
        </select>
      </div>
    </div>

      <div class="form-group">
            <label for="remarks">Remarks (if any)</label> 
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>

          </div> 
    
    </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key5'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save Status</button>
          
        </div>
      </div>
    </div>
  </div>
</form> 


 


<form action="{{ action('Franchise\FranchiseDashboardControllerV1@updatePnDOrderRemarks') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalrem" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Order Remarks</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"> 

          <div class="form-group">
          <label for="type">Remark Type</label>
          <select  class="form-control" id="type" name='type' aria-describedby="type">
            <option>Customer Feedback</option>
            <option>CC Remarks</option>
            <option>Agent Feedback</option>
            <option>Completeion Remark</option>
          </select>
           
        </div>


          <div class="form-group">
            <label for="remarks">Remarks</label>
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div> 

        </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key3'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save Remarks</button>
          
        </div>
      </div>
    </div>
  </div>
</form>

 
<form action="{{ action('Franchise\FranchiseDashboardControllerV1@routeOrderToHeadquarter') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalchangeToFranchise" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Migrate PnD Order</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             
        
    <div class="form-row"> 
      <div class="form-group col-md-6">
        <label for="frno">Select Route</label>
        <select name="frno" class="form-control" id="frno">
            <option value='0'>bookTou HQ</option>
        </select> 
        
      </div>

      <div class="form-group col-md-12">
        <label for="remarks">Migration Remarks</label>
        <textarea name="remarks" class="form-control" id="remarks" rows='3' ></textarea> 
      </div>

    </div>
 
    </div>
        <div class="modal-footer">
          <input type="hidden" value="{{$order_info->id}}" name="orderno"> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Route Order</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>  
        </div>
      </div>
    </div>
  </div>
</form> 



<form  method='post' action="{{ action('Franchise\FranchiseDashboardControllerV1@updateOrderSourceZone')}}">
  {{ csrf_field() }}
  <div class="modal modalUpdateOrderZone" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Update Original Business Source Zone</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             
        
    <div class="form-row"> 
      <div class="form-group col-md-8">
        <label for="frno">Select Business Registration Zone</label>
        <select name="frno" class="form-control" id="frno"> 
            <option value='0'>bookTou HQ</option>
            <option value="{{ session()->get('_frno')  }}">{{  session()->get('_full_name') }}</option>
        </select> 
        
      </div>

      <div class="form-group col-md-12">
        <label for="remarks">Order Zone Change Remarks (if any)</label>
        <textarea name="remarks" class="form-control" id="remarks" rows='3' ></textarea> 
      </div>

    </div>
 
    </div>
        <div class="modal-footer">
          <input type="hidden" value="{{$order_info->id}}" name="orderno"> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>  
        </div>
      </div>
    </div>
  </div>
</form> 


@endsection


@section('script')
 

<script>
 

$(document).on("click", ".showremmodal", function()
{
  $("#key3").val($(this).attr("data-key"));
  $(".modalrem").modal("show") 
}); 

 



  $(document).on("click", ".btnRemoveAgent", function()
{
  $("#key2").val($(this).attr("data-key"));
  $("#modalConfDelAgent").modal("show")

});


 
$(document).on("click", ".showmodal", function()
{
  var widget = $(this).attr("data-widget");
  var key = $(this).attr("data-key");
   $("#key").val(key);
  $("#widget-" + widget ).modal("show") ;

});


$(document).on("click", ".btnViewRcpt", function()
{
    var img = $(this).attr("data-src");
    $("#imgreceipt").attr("src", img );
    $("#modalViewReceipt").modal("show");

});




$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
 




$(document).on("click", ".btnchangestatus", function()
{
  $("#key5").val($(this).attr("data-key"));
  $(".modalstatus").modal("show")

}); 


 $(document).on("click", ".btnpoppaybox", function()
 {
  $("#key4").val($(this).attr("data-key"));

  

  if( $(this).attr("data-paytarget") == "merchant" || $(this).attr("data-paytarget") == "business")
       $("#paytarget").val('merchant').trigger('change'); 
  else
      $("#paytarget").val('booktou').trigger('change'); 

  if( $(this).attr("data-paymode") == "cash" )
       $("#paymode").val('cash').trigger('change'); 
  else
      $("#paymode").val('online').trigger('change'); 

  if( $(this).attr("data-source") == "customer" )
       $("#ordersource").val('customer').trigger('change'); 
  else if( $(this).attr("data-source") == "booktou" )
      $("#ordersource").val('booktou').trigger('change');
  else 
      $("#ordersource").val('business').trigger('change');
 

  $(".modalpayment").modal("show");

}); 
 


 $(document).on("click", ".btnchangetofranchise", function()
{
  $(".modalchangeToFranchise").modal("show");

});


 $(document).on("click", ".btnchangezone", function()
{
    $(".modalUpdateOrderZone").modal("show"); 
});


</script>


@endsection

   