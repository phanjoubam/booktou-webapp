@extends('layouts.franchise_theme_01')
@section('content')
 
  <div class="row">
     <div class="col-md-12">

    <div class='card'> 
      <div class='card-body'>
        <div class="row">
                <div class="col-md-3">
                  Order Stats
                </div>
                <div class="col-md-6">
                </div>
                <div class="col-md-3 text-right">
                     <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@normalOrderStatsLog') }}"> 
                        {{ csrf_field() }}
                         <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

                         <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar'  
                         value="{{ date('d-m-Y', strtotime($data['last_keyword_date']))  }}" name='filter_date' /> 

                            <button type="submit" class="btn btn-primary btn-sm my-1" value='search' name='btn_search'>Search</button>
                      </form>
                </div> 
 </div> 
                </div> 
           </div>
     </div>
 </div>

   <div class="row">
     <div class="col-md-12">    
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');  

                ?>

                 <div class='card mt-2'> 
                  <div class='card-body'> 
                  <div class='row'>
                    <div class='col-md-2' 
                   <?php 
                    if( $interval->format('%H') >=  1  ) 
                      echo 'style="background-color:#ff6e6e;color:#fff; width: 200px"' ;
                    else 
                       if( $interval->format('%H') < 1 )
                        echo 'style="background-color: #67cafa;color:#fff; width: 200px"' ;
                   
                    ?> >
                    <strong>Order #</strong>
                    {{$item->id}}<br/>{{$item->fullname}}<br/><small><i class='fa fa-phone'></i> {{ $item->phone }}</small> 
               
                  </div>  
                  <div class='col-md-3'>
                      @php  
                       $repeatOrder =0  
                      @endphp

                      @foreach($data['book_counts'] as $bookItem)
                        @if($bookItem->book_by == $item->book_by)
                          @php  
                            $repeatOrder  = $bookItem->totalOrders 
                          @endphp 
                          @break
                        @endif
                      @endforeach
                      @foreach($data['all_businesses'] as $business)
                      @if($business->id == $item->bin)
                        {{  $business->name  }}<br/>
                        @break
                      @endif
                    @endforeach  
                      <strong>Order Status:</strong> 
                      @switch($item->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 

                        @case("canceled")
                        <span class='badge badge-danger'>Cancelled</span>
                      @break 

                      @endswitch

                      <br/>
                      <strong>Payment Type:</strong> <span class='badge badge-success'>
                      @if( strcasecmp($item->payment_type,"Cash on delivery") == 0    )
                        CASH
                      @else 
                        ONLN
                      @endif
                      </span><br/>
                       <strong>Delivery for:</strong> <span class='badge badge-info'>{{ date('d-m-Y', strtotime( $item->service_date)) }}</span>
                @if($item->preferred_time  != "")
                <span class='badge badge-primary'>{{ date('h:i a', strtotime($item->preferred_time)) }}</span>
                @endif 
                   </div>
                   <div class='col-md-2'>
                       <strong>Associated Agent</strong> 
                        <div class='form-flex'>
                        
                          @foreach($data['delivery_orders_list'] as $ditem) 
                                @if( $item->id  == $ditem->order_no )
                                  <p>{{ $ditem->fullname  }} </p>
                                 @break;
                              @endif  
                          @endforeach
                      
                      
                </div>
            </div> 
            <div class='col-md-5'>
               <?php 

                $stages = array(); 
                $stages[0] = array();
                $stages[1] = array();
                $stages[2] = array();
               
                foreach($data['order_logs'] as $log) 
                {
                  if($log->order_no != $item->id )
                  {
                    continue;
                  }
                    if($log->order_status == "new")
                    {
                      $stages[0][0] =  $log->log_date; 
                      continue;
                    }

                    if($log->order_status == "confirmed")
                    { 
                      $stages[0][1] =  $log->log_date; 
                      continue;
                    }

                  
                    if($log->order_status == "waiting_package")
                    {
                      $stages[1][0] =  $log->log_date;
                      continue;
                    }

                    if($log->order_status == "package_picked_up")
                    {
                      $stages[1][1] =  $log->log_date;  
                      $stages[2][0] =  $log->log_date; 
                      continue;
                    }
   
                    if($log->order_status == "delivered")
                    { 
                      $stages[2][1] =  $log->log_date; 
                      continue;
                    }
                }

              ?>

                <div class="timeline timeline-xs">
                  <div class="timeline timeline-xs">
                                        
                    <?php   

                    $stage_length ="na";
                    if(count($stages[0]) == 2 )
                    { 
                      $date1 = new \DateTime( $stages[0][0] ); 
                      $date2  = new \DateTime( date('Y-m-d H:i:s', strtotime($stages[0][1])) ); 
                      $interval = $date1->diff(  $date2 ); 
                      $stage_length =  $interval->format('%a days %H hrs  %i mins');  
                      ?>

                       <div class="timeline-item">
                          <div class="timeline-item-marker">
                            <div class="timeline-item-marker-text">{{ $stage_length }}</div>
                            <div class="timeline-item-marker-indicator bg-blue"></div>
                          </div>
                          <div class="timeline-item-content"> Order placement to confirmation</div>
                        </div> 
                      <?php  
                    } 
 $stage_length ="na";
                    if(count($stages[1]) == 2 )
                    { 

                      $date1 = new \DateTime( $stages[1][0] ); 
                      $date2  = new \DateTime( date('Y-m-d H:i:s', strtotime($stages[1][1])) );

                      $interval = $date1->diff(  $date2 ); 
                      $stage_length =  $interval->format('%a days %H hrs  %i mins');  
                      ?>

                       <div class="timeline-item">
                          <div class="timeline-item-marker">
                            <div class="timeline-item-marker-text">{{ $stage_length }}</div>
                            <div class="timeline-item-marker-indicator bg-purple"></div>
                          </div>
                          <div class="timeline-item-content"> Waiting time in restaurant</div>
                        </div> 
                      <?php  
                    } 

$stage_length ="na";
                    if(count($stages[2]) == 2 )
                    { 
                      $date1 = new \DateTime( $stages[2][0] ); 
                      $date2  = new \DateTime( date('Y-m-d H:i:s', strtotime($stages[2][1])) );

                      $interval = $date1->diff(  $date2 ); 
                      $stage_length =  $interval->format('%a days %H hrs  %i mins');  
                      ?>

                       <div class="timeline-item">
                          <div class="timeline-item-marker">
                            <div class="timeline-item-marker-text">{{ $stage_length }}</div>
                            <div class="timeline-item-marker-indicator bg-green"></div>
                          </div>
                          <div class="timeline-item-content"> Time taken in final delivery</div>
                        </div> 
                      <?php  
                    } 

                    ?>

                         </div>
                  </div>  
         </div> 
          </div>  
        </div>  </div> 
                         <?php
                          $i++; 
                       }

              ?> 
              
   </div>
 </div>
 
   

  
 
@endsection

@section("script")

<script>

$(document).on("click", ".btncancel", function()
{

    $("#orderno").val($(this).attr("data-ono"));

      $(".modal_cancel").modal("show")

});


 

$(document).on("click", ".btnchangedelivery", function()
{
  $("#orderno2").val($(this).attr("data-ono"));
  $(".changedelivery").modal("show"); 
});




$(document).on("click", ".btnconfim", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['reason'] =  $("#tareason").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/order/cancel" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_cancel").modal("hide") ;

         $("#tr" + $("#orderno").val( )).remove();
      },
      error: function( ) 
      {
       // alert(  'Something went wrong, please try again'); 
      } 

    });

       

});

  


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
             
        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 

    
    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');

  

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
        //alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id");

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsaveconfirmation").attr("data-id",  oid);
      $("#btnsaveconfirmation").attr("data-conf", confirmation ); 
      $("#confirmOrder").modal("show");   
   }  

})

$(document).on("change", "#closeconfmodal", function()
{

  
})
 

$('body').delegate('#btnsaveconfirmation','click',function(){
  
   var confirmation = $(this).attr("data-conf"); 
   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");


   $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/update-customer-confirmation" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.browser_refresh == "yes")
        {
          $(".loading_span").html(" ");
          location.reload(); 
        }    
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        //alert(  'Something went wrong, please try again'); 
      } 

    });
  
})

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 