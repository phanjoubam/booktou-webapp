@extends('layouts.admin_theme_02')
@section('content')
 
 

   <div class="row">
     <div class="col-md-12"> 

       @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif
  
    <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');   
                ?>
 
 <div class="card">
  <div class="card-body">

  <div class='row'> 
  <div class='col-sm-12 col-md-3'>
      <div class='p-4'  <?php 
          if( $interval->format('%H') >=  1  ) 
            echo 'style="background-color:#ff6e6e;color:#fff;"' ;
          else 
              if( $interval->format('%H') < 1 )
                echo 'style="background-color: #67cafa;color:#fff"' ;
                   
              ?> >
      <strong>Order Source</strong><br/>  
      <span scope="col">Business/Seller</span><br/>
      {{$item->name}}<small><br/>
      <i class='fa fa-phone'></i> {{ $item->businessPhone }}</small>  
     </div>  <hr/>
    <div class='p-3'>
    <strong>Delivery Location</strong><br/>
    {{$item->fullname }}<br/>
     {{$item->address}}<br/> 
     <small><i class='fa fa-phone'></i> {{ $item->phone }}</small> 
    </div>
   
   <hr/>

@if( $item->book_status == "new"   ) 

   @foreach($data['requests_to_process'] as $reqitem)


      @if($item->id == $reqitem->order_no )
      <div class='card'>
        <div class="card-body">
          <h5 class="card-title">Agent Requesting to handle order</h5>
            <span class='badge badge-primary'>{{ $reqitem->fullname }}</span>
            <hr/>
            <button class="btn btn-success btn-sm btnconfselfassign" 
            data-aid="{{$reqitem->id}}"  data-oid="{{$item->id}}" data-action="allow"><i class='fa fa-tick'></i> Allow</button>  
            <button class="btn btn-danger btn-sm btnconfselfassign" 
            data-aid="{{$reqitem->id}}" 
            data-oid="{{$item->id}}"  data-action="deny"><i class='fa fa-cross'></i> Deny</button>

          </div>
      </div>
        @break
      @endif

   @endforeach

 @endif
  </div> 
  <div class='col-sm-12 col-md-3'>

    <span>Order #</span> <span class="badge badge-primary badge-pill">{{$item->id}}</span>
    <a class='badge badge-info' target='_blank' href="{{ URL::to('/shopping/where-is-my-order') }}/{{ $item->tracking_session }}">Where is it?</a><br/> 
      <span>Time Elapse</span>  <span class="badge badge-primary badge-pill">{{ $order_age }}</span><br/> 
      <strong>Order Remarks</strong><br/>
      @if($item->cust_remarks !=  "") 
        {{ $item->cust_remarks }}
      @else 
        Not provided
      @endif

  @if($item->order_receipt != null) 
  <hr/>

  <img src="{{  $item->order_receipt }}" class="img-thumbnail" alt="Receipt">
  <br/>
 <a data-src="{{  $item->order_receipt }}" class="btn btn-link btnViewRcpt" href="#">View Receipt</a>

   

 


@endif

 



</div>
  <div class='col-sm-12 col-md-6 text-right'> 

    <div class='p-3'>  
      <ul class="list-group"> 
        <li class="list-group-item ">
          <div class="dropdown ml-lg-auto ml-3 toolbar-item">
            <button class="btn btn-primary " type="button" id="dropdownexport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog "></i></button>
               <div class="dropdown-menu" aria-labelledby="dropdownexport">
                  <a   data-src="{{  $item->order_receipt }}"  class="dropdown-item btn btn-link btnViewRcpt" href='#'  >View Receipt</a>
                  <button type='button' class="dropdown-item btn btn-link btnedit"  data-receipt="{{$item->order_receipt}}" 
                    data-ono="{{$item->id}}"
                    data-fee="{{$item->service_fee}}"
                    data-total="{{$item->total_amount}}"
                    data-date="{{ date('d-m-Y', strtotime( $item->service_date )) }}"
                    data-fullname="{{$item->fullname}}"
                    data-phone="{{$item->phone}}"
                    data-address="{{$item->address}}"
                    data-landmark="{{$item->landmark}}"
                    data-pay_mode="{{ $item->pay_mode}}"
                    data-remarks="{{ $item->cust_remarks }}" >Edit Order</button>
<a   class="dropdown-item btn btn-link btnChangeTime"  data-ono="{{$item->id}}"  data-receipt="{{$item->order_receipt}}"  href='#'  >Change Delivery Time</a>  
    </div>
           </div> 
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Delivery Date
          <span class="badge badge-primary badge-pill">{{ date('d-m-Y', strtotime( $item->service_date )) }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Service/Delivery Fee
          <span class="badge badge-primary badge-pill">{{ $item->service_fee }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Amount to pay to Merchant
          <span class="badge badge-primary badge-pill">{{ $item->total_amount  }}</span>
      </li>
  

      <li class="list-group-item d-flex justify-content-between align-items-center">
           Status
          <span class="badge badge-primary badge-pill">
            @switch($item->book_status)
              @case("new")
                New
              @break
              @case("confirmed")
                Confirmed
              @break
                @case("order_packed")
                Order Packed
                      @break

                      @case("package_picked_up")
                      Package Picked Up
                      @break

                       @case("pickup_did_not_come")
                       Pickup Didn't Come
                      @break

                       @case("in_route")
                       In Route
                      @break

                       @case("completed")
                       Completed
                      @break

                      @case("delivered")
                      Delivered
                      @break

                       @case("delivery_scheduled")
                       Delivery Scheduled
                      @break 
                      @endswitch
                    </span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
           Payment Type
          <span class="badge badge-primary badge-pill">{{ $item->pay_mode  }}</span>
      </li>
  

      <li class="list-group-item d-flex justify-content-between align-items-center">
        <strong>Delivery Agent Information</strong>
      </li>

      
        @if( $item->book_status == "new" ||  $item->book_status == "confirmed" ||   $item->book_status == 'order_packed' ) 
        <li class="list-group-item d-flex justify-content-between align-items-center">
         
          <div class="form-row">
            <div class="form-group col-md-10"> 
                  <select   class='form-control form-control-sm' id="aid_{{$item->id }}" name='agents[]' >
                              @foreach($data['all_agents'] as $aitem) 
                                 @if($aitem->isFree == "yes" )
                                  <option value='{{ $aitem->id }}' data-img="{{ $aitem->profile_photo }}"  >{{ $aitem->nameWithTask }}</option>
                                @endif 
                             @endforeach 
                  </select>
          </div>
          <div class="form-group col-md-2">
            <button class='btn btn-secondary btn-xs showTaskList' data-cid="aid_{{$item->id }}"  ><i class='fa fa-eye'></i></button>
          </div>
          </div>  
      </li>
      
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <button class="btn btn-primary btn-assign" data-oid="{{$item->id}}" >Assign Agent</button>
      </li>

  
        
        @else
         <li class="list-group-item d-flex justify-content-between align-items-center">
          @foreach($data['delivery_orders_list'] as $ditem) 
              @if( $item->id  == $ditem->order_no )
                <p class='text-left'>{{ $ditem->fullname  }} <br/> 
                                 @if(  $ditem->agent_ready == "yes")
                                 <span class='badge badge-success white'>TASK ACCEPTED</span>  
                                  @else 
                                  <span class='badge badge-danger white'>NO ACTION</span>
                                 @endif
                                  </p>
                @break;
                @endif  
            @endforeach 
          <button class='btn btn-danger btn-sm btnRemoveAgent' data-key='{{ $item->id }}'>Remove Agent</button>
         </li>   
        @endif 

    </ul>

                               
     
    </div> 
     

  </div>



 </div>



</div>
</div>

<?php
  $i++; 
}
?>


             </div>
              </div>
 </div>




              
            </div>
          </div>
        </div>  
  

 
<div class="modal hide fade modal_cancel"   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Confirm Order Cancellation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body "> 
            <div class='confloading'>
        </div> 
        <div class="form-group"> 
    <textarea  placeholder='Reason for cancelling' class="form-control form-control-sm  " id="tareason" name="tareason" rows='5'></textarea>

  </div>
  

      </div>
      <div class="modal-footer">
        <input type='hidden' id='orderno' name="orderno" />
        <button type="button" class="btn btn-primary btnconfim" >Cancel</button> 
        <button type="button" class="btn btn-secondary" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
 
 

<div class="modal hide fade modal_processing" id='modal_processing'   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Processing Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"> 
        <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn_action" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modalTaskList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
  <img src='{{ URL::to("/public/assets/image/no-image.jpg") }}' id='apimg' width="40px" height="40px" class="img-rounded" style='margin-right:10px; display:inline-block'/>
<h5 class="modal-title" id="exampleModalLongTitle">Delivery Tasks for <strong id='span_anm'></strong></h5>
  

        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content_area">
         <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


<div class="modal" id='confirmOrder' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Customer Order By Call</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class='alert alert-danger'>THIS ACTION CANNOT BE REVERSED!</p>
        <p>

          Have you called back the customer to confirm this order?
          <br/>

          If you haven't call back, pleaes do call and confirm first.
          <br/> 
        </p>
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <button type="button" class="btn btn-success" id="btnsaveconfirmation" data-conf="" data-id="" >Save Changes</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


  <form action="{{ action('Admin\AdminDashboardController@newPickAndDropRequest') }}"  method="post">
    {{ csrf_field() }}
<div class="modal" id='modalneworder' tabindex="-1">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add New Pick-And-Drop Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      

<div class="form-row">
    <div class="form-group col-md-8">
      <label for="bin">Pick-and-drop From:</label>
      <select name ="bin" class="form-control" id="bin"  >
        @foreach($data['all_business'] as $bitem)
          <option value="{{ $bitem->id  }}">{{ $bitem->name }}</option>
        @endforeach
      </select>
    </div> 

  </div>

  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="cname">Customer Name:</label>
      <input type="text" class="form-control" id="cname" name='cname'>
    </div> 
 <div class="form-group col-md-4">
      <label for="cphone">Customer Phone:</label>
      <input type="text" class="form-control" id="cphone" name='cphone'>
    </div>

    <div class="form-group col-md-12">
        <label for="address">Address:</label>
    <input type="text" class="form-control" id="address" name='address' placeholder="Delivery Address">
    </div>

<div class="form-group col-md-12">
        <label for="landmark">Landmark (optional):</label>
    <input type="text" class="form-control" id="landmark" name='landmark' placeholder="Nearest landmark to easily locate address">
    </div>

  </div>
    
 
  <div class="form-row">

     <div class="form-group col-md-3">
      <label for="servicedate">Service Date:</label>
      <input type="text" class="form-control calendar" id="servicedate" name='servicedate'>
    </div>

    <div class="form-group col-md-3">
      <label for="sfee">Service Fee:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="sfee" name='sfee'>
    </div>

    <div class="form-group col-md-6">
      <label for="total">Amount (excluding service fee):</label>
      <input type="number" min='0' step='0.1' class="form-control" id="total" name='total'>
    </div>
 
    
  </div>

 <div class="form-row">
 <div class="form-group col-md-12">
      <label for="remarks">Remarks (if any):</label>
      <textarea  class="form-control" id="remarks" rows='4' name='remarks'></textarea>
    </div>  </div>
 
      </div>
      <div class="modal-footer">
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>



<!-- edit order -->
<form action="{{ action('Admin\AdminDashboardController@convertReceiptToPickAndDropRequest') }}"  method="post">
    {{ csrf_field() }}
<div class="modal" id='modalcopyreceipt' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Convert Uploaded Receipt/Image to Pick-And-Drop Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 

        <div class='row'>
          <div class='col-md-6'> 

  <div class="form-row">
    <div class="form-group col-md-12">
      <label for="ecname">Customer Name:</label>
      <input type="text" class="form-control" id="ecname" name='cname'>
    </div> 
 <div class="form-group col-md-12">
      <label for="ecphone">Customer Phone:</label>
      <input type="text" class="form-control" id="ecphone" name='cphone'>
    </div>

    <div class="form-group col-md-12">
        <label for="eaddress">Address:</label>
    <input type="text" class="form-control" id="eaddress" name='address' placeholder="Delivery Address">
    </div>

<div class="form-group col-md-12">
        <label for="elandmark">Landmark (optional):</label>
    <input type="text" class="form-control" id="elandmark" name='landmark' placeholder="Nearest landmark to easily locate address">
    </div>

  </div> 
 
  <div class="form-row">

     <div class="form-group col-md-6">
      <label for="eservicedate">Service Date:</label>
      <input type="text" class="form-control calendar" id="eservicedate" name='servicedate'>
    </div>

    <div class="form-group col-md-6">
      <label for="esfee">Service Fee:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="esfee" name='sfee'>
    </div>

    <div class="form-group col-md-6">
      <label for="etotal">Total Amount (excluding service fee):</label>
      <input type="number" min='0' step='0.1' class="form-control" id="etotal" name='total'>
    </div>
 
    <div class="form-group col-md-6">
      <label for="etotal">Payment Mode:</label>
      <br/>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="paymode1" name="paymode" class="custom-control-input" value='CASH'>
        <label class="custom-control-label" for="paymode1">COD</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="paymode2" name="paymode" class="custom-control-input" value='ONLINE'>
        <label class="custom-control-label" for="paymode2">ONLINE</label>
      </div>


    </div>


  </div>

 <div class="form-row">
 <div class="form-group col-md-12">
      <label for="eremarks">Remarks (if any):</label>
      <textarea  class="form-control" id="eremarks" rows='4' name='remarks'></textarea>
    </div>  </div>
  
  </div>
          <div class='col-md-6'>
            <img class="img-fluid"   width='700' src="{{ config('app.app_cdn') .  '/assets/image/no-image.jpg'  }}" id='receipt' alt='Order Receipt' />
          </div> 
        </div> 

      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'>
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>



<div class="modal fade" id="modalConfDelAgent" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Admin\AdminDashboardController@removeAgentFromPnDOrder') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to remove agent from this delivery task?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key2'/> 
        <button type="submit" name="btnRemoveAgent" value="save" class="btn btn-primary btn-sm">Yes</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
 </form>
</div>

<div class="modal" id='modalViewReceipt' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Order Receipt</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="img-fluid" width='700'  src="" id='imgreceipt' alt='Order Receipt' /> 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'>
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



<form action="{{ action('Admin\AdminDashboardController@processAgentSelfAssignRequest') }}" method='post'>
 {{ csrf_field() }}
<div class="modal" id='modalconfselfassign' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Agent Self Assign Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p class='alert alert-danger'>This action is non recoverable.</p>
       <div class='modalinfo'>
       </div>
      </div>
      <div class="modal-footer"> 
        <input type="hidden" id="confsakey" name='confsakey'>
        <input type="hidden" id="confsaaid" name='confsaaid'>
        <button type="submit" name='btnupdatesa' value='save' class="btn btn-primary">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
</div>
</form>




@endsection

@section("script")

<script>



  

$(document).on("click", ".addOrder", function()
{
    $("#modalneworder").modal("show");

});


$(document).on("click", ".btncancel", function()
{

    $("#orderno").val($(this).attr("data-ono"));
    $(".modal_cancel").modal("show")

});




$(document).on("click", ".btnedit", function()
{
  $("#erono").val($(this).attr("data-ono"));  
  $("#receipt").attr("src", $(this).attr("data-receipt"));   
  $("#ecname").val( $(this).attr("data-fullname") );
  $("#ecphone").val( $(this).attr("data-phone") );  
  $("#eaddress").val( $(this).attr("data-address") );  
  $("#elandmark").val( $(this).attr("data-landmark") );   
  $("#esfee").val( $(this).attr("data-fee") ); 
  $("#etotal").val( $(this).attr("data-total") ); 
  $("#eservicedate").val( $(this).attr("data-date") );  
  $("#eremarks").val( $(this).attr("data-remarks") ); 

  if($(this).attr("data-pay_mode") == "ONLINE")
    $("#paymode2").prop("checked", true);
  else
    $("#paymode1").prop("checked", true);

  $("#modalcopyreceipt").modal("show") 
});




$(document).on("click", ".btnconfim", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['reason'] =  $("#tareason").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
 

    $.ajax({
      type: 'post',
      url: api + "/v2/web/customer-care/pnd-order/cancel" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_cancel").modal("hide") ;

         $("#tr" + $("#orderno").val( )).remove();
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });
    

});

  


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
             
        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 
    json['otype'] =  'pickup' ; 

    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');

  

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
       // alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id");

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsaveconfirmation").attr("data-id",  oid);
      $("#btnsaveconfirmation").attr("data-conf", confirmation ); 
      $("#confirmOrder").modal("show");   
   }  

})

 
  

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


 

$(document).on("click", ".btnRemoveAgent", function()
{
  $("#key2").val($(this).attr("data-key"));
  $("#modalConfDelAgent").modal("show")

});


 
$(document).on("click", ".btnViewRcpt", function()
{
    var img = $(this).attr("data-src");
    $("#imgreceipt").attr("src", img );
    $("#modalViewReceipt").modal("show");

});



$(document).on("click", ".btnconfselfassign", function()
{
    var oid = $(this).attr("data-oid");
    var aid = $(this).attr("data-aid");  
    var action = $(this).attr("data-action");

    $("#confsakey").val( oid);
    $("#confsaaid").val(   aid ); 

    if(action== "allow")
    {
      $(".modalinfo").html("<p>Request to self assign will be deleted. Are you sure, you want to take this action?</p>");
    }
    else 
    {
      $(".modalinfo").html("<p>Request to self assign will be granted. Are you sure, you want to take this action?</p>");
    }

    $("#modalconfselfassign").modal("show");

});




</script> 

@endsection 