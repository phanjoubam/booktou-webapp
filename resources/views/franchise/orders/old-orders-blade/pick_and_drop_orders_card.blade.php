@extends('layouts.franchise_theme_01')
@section('content')
 
 

   <div class="row">
     <div class="col-md-12"> 

       @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif
   

<div class='row'>  
    <?php
      $i=1;
      $date2  = new \DateTime( date('Y-m-d H:i:s') );
      foreach ($data['results'] as $item)
      {
        $date1 = new \DateTime( $item->book_date ); 
        $interval = $date1->diff($date2); 
        $order_age =  $interval->format('%a days %H hrs  %i mins');   
      ?> 


  <div class='col-sm-12 col-md-3'> 
 <div class="card">
  <div class="card-body">   

   @if($item->order_receipt != null)  
      <div class="dropdown ddright">
            <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" 
            aria-expanded="false">
            <i class="fa fa-cog "></i>
            </a> 
              <ul class="dropdown-menu dropdown-user dropdown-menu-right">  
                    
                    <li><a   class="dropdown-item btn btn-link btnedit"  data-receipt="{{$item->order_receipt}}" data-ono="{{$item->id}}"   href='#'  >Edit From Receipt</a></li>
                    <li><a   class="dropdown-item btn btn-link btnChangeTime"  data-ono="{{$item->id}}"  data-receipt="{{$item->order_receipt}}"  href='#'  >Change Delivery Time</a></li>
                    <li><a   class="dropdown-item btn btn-link btncancel" data-ono="{{$item->id}}" href='#' >Cancel Order</a></li>    

              </ul>
       </div>
  @endif

     <div class='p-2' <?php 
          if(  $item->book_status = "delivered"  ) 
            echo 'style="background-color:#37b516;color:#fff;"' ;
          else  
                echo 'style="background-color: #f26522;color:#fff"' ;
            ?> >
      <strong>Order #</strong> <span class="badge badge-primary badge-pill">{{$item->id}}</span> 
      <br/>
      <strong>Order Source</strong><br/>  
      <span scope="col">Business/Seller</span><br/>
      {{$item->name}}<small><br/>
      <i class='fa fa-phone'></i> {{ $item->businessPhone }}</small>  
     </div> 
 
    <div class='p-2'>
      <strong>Delivery Location Information</strong><br/>
      {{$item->fullname }}<br/>
      {{$item->address}}<br/> 
     <small><i class='fa fa-phone'></i> {{ $item->phone }}</small> 
    </div> 
 


 <hr/>
      <ul class="list-group">
         
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Time Elapse
          <span class="badge badge-primary badge-pill">{{ $order_age }}</span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
           Service/Delivery Fee
          <span class="badge badge-primary badge-pill">{{ $item->service_fee }}</span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
           Total to collect
          <span class="badge badge-primary badge-pill">{{ $item->total_amount + $item->service_fee  }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Status
          <span class="badge badge-primary badge-pill">
            @switch($item->book_status)
              @case("new")
                New
              @break
              @case("confirmed")
                Confirmed
              @break
                @case("order_packed")
                Order Packed
                      @break

                      @case("package_picked_up")
                      Package Picked Up
                      @break

                       @case("pickup_did_not_come")
                       Pickup Didn't Come
                      @break

                       @case("in_route")
                       In Route
                      @break

                       @case("completed")
                       Completed
                      @break

                      @case("delivered")
                      Delivered
                      @break

                       @case("delivery_scheduled")
                       Delivery Scheduled
                      @break 
                      @endswitch
                    </span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
        OTP
        <span class="badge badge-primary badge-pill">{{ $item->otp }}</span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
        <strong>Order Remarks</strong> 
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
        {{ $item->cust_remarks }}
      </li>


      <li class="list-group-item d-flex justify-content-between align-items-center">
        <strong>Delivery Agent Information</strong>
      </li>

      
        @if( $item->book_status == "new" ||  $item->book_status == "confirmed" ||   $item->book_status == 'order_packed' ) 
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <form>
          <div class="form-row">
            <div class="form-group col-md-10"> 
          <select   class='form-control form-control-sm' id="aid_{{$item->id }}" name='agents[]' >
                              @foreach($data['all_agents'] as $aitem) 
                                 @if($aitem->isFree == "yes" )
                                  <option value='{{ $aitem->id }}' data-img="{{ $aitem->profile_photo }}"  >{{ $aitem->nameWithTask }}</option>
                                @endif 
                             @endforeach 
                          </select>
          </div>
<div class="form-group col-md-2">
  <button class='btn btn-secondary btn-xs showTaskList' data-cid="aid_{{$item->id }}"  ><i class='fa fa-eye'></i></button>
</div>
</div>
</form>

      </li>
      
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <button class="btn btn-primary btn-assign" data-oid="{{$item->id}}" >Assign Agent</button>
      </li>

 
     
        
        @else
         <li class="list-group-item d-flex justify-content-between align-items-center">
          @foreach($data['delivery_orders_list'] as $ditem) 
              @if( $item->id  == $ditem->order_no )
                {{ $ditem->fullname  }}  
                @break;
                @endif  
            @endforeach

         </li>   
        @endif 

    </ul>  
</div>
</div>
</div>
<?php
  $i++; 
}
?>

</div>
             </div>
              </div>
 </div>
</div>
</div>
</div>  
  

 
 
 


<!-- edit order -->
 
<div class="modal" id='modalViewReceipt' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Order Receipt</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="img-fluid"  src="" id='imgreceipt' alt='Order Receipt' /> 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'>
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>







<!-- edit order -->
<form action="{{ action('Admin\AdminDashboardController@convertReceiptToPickAndDropRequest') }}"  method="post">
    {{ csrf_field() }}
<div class="modal" id='modalcopyreceipt' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Convert Uploaded Receipt/Image to Pick-And-Drop Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 

        <div class='row'>
          <div class='col-md-6'> 

  <div class="form-row">
    <div class="form-group col-md-12">
      <label for="ecname">Customer Name:</label>
      <input type="text" class="form-control" id="ecname" name='cname'>
    </div> 
 <div class="form-group col-md-12">
      <label for="ecphone">Customer Phone:</label>
      <input type="text" class="form-control" id="ecphone" name='cphone'>
    </div>

    <div class="form-group col-md-12">
        <label for="eaddress">Address:</label>
    <input type="text" class="form-control" id="eaddress" name='address' placeholder="Delivery Address">
    </div>

<div class="form-group col-md-12">
        <label for="elandmark">Landmark (optional):</label>
    <input type="text" class="form-control" id="elandmark" name='landmark' placeholder="Nearest landmark to easily locate address">
    </div>

  </div> 
 
  <div class="form-row">

     <div class="form-group col-md-6">
      <label for="eservicedate">Service Date:</label>
      <input type="text" class="form-control" id="eservicedate" name='servicedate'>
    </div>

    <div class="form-group col-md-6">
      <label for="esfee">Service Fee:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="esfee" name='sfee'>
    </div>

    <div class="form-group col-md-6">
      <label for="etotal">Total Amount (excluding service fee):</label>
      <input type="number" min='0' step='0.1' class="form-control" id="etotal" name='total'>
    </div>
 
    
  </div>

 <div class="form-row">
 <div class="form-group col-md-12">
      <label for="eremarks">Remarks (if any):</label>
      <textarea  class="form-control" id="eremarks" rows='4' name='remarks'></textarea>
    </div>  </div>
 

  </div>
          <div class='col-md-6'>
            <img class="img-fluid"  src="{{ config('app.app_cdn') .  '/assets/image/no-image.jpg'  }}" id='receipt' alt='Order Receipt' />
          </div>

        </div>

 

      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'>
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>



 


<!-- edit order -->
 
<div class="modal" id='modalupdatetime' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change Delivery Time</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 

        <div class='row'>
          <div class='col-md-6'> 

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="ecname">Delivery Time:</label>
                <input type="text" class="form-control" id="ecname" name='cname'>
              </div>  
            </div>
          </div>
          <div class='col-md-6'>
            <img class="img-fluid"  src="{{ config('app.app_cdn') .  '/assets/image/no-image.jpg'  }}" id='receipt' alt='Order Receipt' />
          </div> 
        </div> 
       </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'>
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
 


@endsection

@section("script")
 


<script>
 

$(document).on("click", ".addOrder", function()
{
    $("#modalneworder").modal("show");

});


$(document).on("click", ".btncancel", function()
{

    $("#orderno").val($(this).attr("data-ono"));
    $(".modal_cancel").modal("show")

});




$(document).on("click", ".btnedit", function()
{

    $("#erono").val($(this).attr("data-ono"));  
    $("#receipt").attr("src", $(this).attr("data-receipt"));  
    $("#modalcopyreceipt").modal("show")

});




$(document).on("click", ".btnChangeTime", function()
{

    $("#erono").val($(this).attr("data-ono"));  
    $("#receipt").attr("src", $(this).attr("data-receipt"));  
    $("#modalupdatetime").modal("show")

});




$(document).on("click", ".btnconfim", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['reason'] =  $("#tareason").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
 

    $.ajax({
      type: 'post',
      url: api + "/v2/web/customer-care/pnd-order/cancel" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_cancel").modal("hide") ;

         $("#tr" + $("#orderno").val( )).remove();
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });
    

});

  


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
             
        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 
    json['otype'] =  'pickup' ; 

    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');

  

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
        alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id");

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsaveconfirmation").attr("data-id",  oid);
      $("#btnsaveconfirmation").attr("data-conf", confirmation ); 
      $("#confirmOrder").modal("show");   
   }  

})

 
  

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 
