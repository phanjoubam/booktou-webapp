@extends('layouts.franchise_theme_01')
@section('content')
  

   <div class="row">
     <div class="col-md-12">  
       @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif
   

<div class='row'>  
    <?php
      $i=1;
      $date2  = new \DateTime( date('Y-m-d H:i:s') );
      foreach ($data['all_requests'] as $item)
      {
          $date1 = new \DateTime( $item->request_date ); 
          $interval = $date1->diff($date2); 
          $order_age =  $interval->format('%a days %H hrs  %i mins');   
      ?> 


  <div class='col-sm-12 col-md-3 mt-4'> 
  <div class="card"><div class='p-2' <?php 
          if(  $item->is_processed == "no"  ) 
            echo 'style="background-color:#1c45ef;color:#fff; padding: 10px 20px;"' ;
          else  
                echo 'style="background-color: #ff6600;color:#fff; padding:10px 20px;"' ;
            ?> >
      <strong>Request #</strong> <span class="badge badge-primary badge-pill">{{$item->id}}</span> 
      <br/>
      <strong>Customer Name</strong>{{$item->name}}<br/>  
      <span scope="col">Contact No.:</span> {{$item->phone}} 
     </div> 
  <div class="card-body">   
    
    @if($item->request_body == null)
    <p class='alert alert-info'>No enquiry request provided.</p>
    @else 

    <div class='p-2'>
      <strong>Quotation Body</strong><br/>
      {{ $item->request_body }} 
    </div>  


  @endif
 </div>
 <div class="card-footer text-muted">
    
 <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input switch"   data-id='{{$item->id }}' id="switchON{{ $item->id }}"  
                          <?php if($item->is_spam == "yes") echo "checked";  ?>  />
                          <label class="custom-control-label" for="switchON{{$item->id }}">SPAM</label>
                        </div> 

</div>
</div>
</div>
<?php
  $i++; 
}
?>

</div>
             </div>
              </div>
 </div>
</div>
</div>
</div>  
  
  

<!-- edit order -->
 
<div class="modal" id='modalViewReceipt' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Order Receipt</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="img-fluid"  src="" id='imgreceipt' alt='Order Receipt' /> 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'>
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>


@endsection

@section("script")

<script>

 
$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var crid = $(this).attr("data-id");
 

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";  
   } 
   
   var json = {}; 
   json['status'] = confirmation ; 
   json['key'] = crid ; 
  
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/update-callback-enquiry-spam-statusddd" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {

      }  
    });


})



$(document).on("click", ".btnViewRcpt", function()
{
    var img = $(this).attr("data-src");
    $("#imgreceipt").attr("src", img );
    $("#modalViewReceipt").modal("show");

});

 
  

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 