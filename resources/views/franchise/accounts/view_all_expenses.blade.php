@extends('layouts.franchise_theme_03')
@section('content')

<div class="row"> 

<div class="col-md-12">



 <div class="card">
 	<div class="card-header">  
		<h3>Expenditure list for month of {{$month}}, {{$year}}</h3>
	</div>
 	<div class="card-body">
@if(isset($expense)) 
	<table class="table  table-bordered">
	<thead>
		<tr> 
			<th>Date</th>
			<th>Expend By</th>
			<th>Category</th>
			<th>Details</th>
			<th>Amount</th> 
		</tr>
	</thead>
	<tbody>
		@php 
			$i = 0;
			$totalAdvance=0.00;
			$totalFuel=0.00;
			$totalExpenditure=0.00; 
			$cdate = date('Y') . "-" . date('m') . "-01" ;

		@endphp 
		@foreach($expense as $item)
		@php 
			$i++; 
			$cdate = $item->use_date;
		@endphp

		@switch($item->category )
			@case("advance")
				@php 
					$totalAdvance += $item->amount;
				@endphp  
			@break
			@case("fuel")
				@php 
					$totalFuel += $item->amount;
				@endphp  
			@break
			@default
				@php 
					$totalExpenditure += $item->amount;
				@endphp  
			@break  
		@endswitch  

		<tr> 
			<td>{{ date('d-m-Y', strtotime($item->use_date))}}</td>
			<td>
				@if($item->use_by == request()->session()->get('_frno'))
				<span class='badge badge-info badge-pill'>Office</span>
				@else
					@foreach($all_staffs as $staff)
						@if($staff->id == $item->use_by)
							<span class='badge badge-warning badge-pill'>{{ $staff->fullname }}</span>

							@break
						@endif
					@endforeach
				@endif</td>
			<td>{{$item->category}}</td>
			<td>{{$item->details}}</td> 
			<td>{{$item->amount}}</td> 
		</tr> 

		@endforeach

		 

		<tr>
			<th colspan="4" class='text-right'>Total Expenditure</th>
			<th>{{ $totalExpenditure + $totalAdvance + $totalFuel }}</th> 
		</tr>


	</tbody>
</table>

@else 
<p class='alert alert-info'>No expenditure report on the selected date.</p>
@endif
</div>
</div>
</div>
</div>

@endsection

@section("script")
<script>



</script>
@endsection