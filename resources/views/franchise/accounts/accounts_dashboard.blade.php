@extends('layouts.franchise_theme_03')
@section('content')
 
    @php 
          $totalCash = $totalOnline = $totalPndCash = $totalPndOnline = 0.00; 
        @endphp
          @foreach ($all_orders as $normal)
           

           @if($normal->bookingStatus == "delivered" || $normal->bookingStatus == "completed") 

                  <!-- checking normal order -->

                            @if($normal->orderType == "normal")

                               
             

                              @if( strcasecmp($normal->paymentType , "online") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                                @php 
                                  $totalOnline += $normal->orderCost  ;  //packing cost already included
                                @endphp
                              @else 
                                @php
                                  $totalCash += $normal->orderCost  ;  //packing cost already included  
                                @endphp
                              @endif 
                    <!-- normal order check ends here -->
                    @elseif($normal->orderType=="pnd")

                    <!-- pnd section checks starts from here -->
                   

                    @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                                             


                                              @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                                                  @if(strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 ) 
                                                    @php 
                                                      $totalPndOnline += $normal->orderCost;
                                                    @endphp
                                                  @else
                                                      @php
                                                        $totalPndOnline += $normal->orderCost  ;
                                                      @endphp  
                                                  @endif  
                                                   @else
                                                        @php 
                                                          $totalPndCash += $normal->orderCost  ;   
                                                        @endphp     
                                              @endif  

                    @else 
  
                                              

                    @if( strcasecmp($normal->paymentType , "online") == 0  )   
                        @if( strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 )  
                          @php
                            
                          @endphp  
                        @else
                          @php
                            $dueOnMerchant  = 0.00;
                            $totalPndOnline += $normal->orderCost;
                          @endphp 
                        @endif 
                    @else
                        @php 
                          $dueOnMerchant  = 0.00;
                          $totalPndCash += $normal->orderCost  ;   
                        @endphp     
                    @endif 

                    @endif

                    <!-- pnd section ends here -->
                    @endif
 


           @endif
  
        @endforeach

  

<div class="row"> 
   
<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper">
                          	<h3>
                          	@if($deposits->depositAmount) 
                          	{{$deposits->depositAmount}}
                          	@else 
                          	0.00
                          	@endif &#x20b9; </h3>
                            <a target="_blank" href="{{ URL::to('franchise/accounts/view-deposits-details')}}">
                            	<h5 class="mb-0 font-weight-medium text-primary">Total Deposits</h5>
                            </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>

<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          	<h3>
                          	@if($expense->expensesAmount) 
                          	{{$expense->expensesAmount}}
                          	@else 
                          	0.00
                          	@endif &#x20b9; 
                            </h3>
                <a target="_blank" href="{{URL::to('franchise/accounts/view-expenses-details')}}"> <h5 class="mb-0 font-weight-medium text-primary">Total Expenses</h5></a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          	<h3>
                          	@if($advance->totalAdvance) 
                          	{{$advance->totalAdvance}}
                          	@else 
                          	0.00
                          	@endif &#x20b9;  
                          </h3>
                          <a target="_blank" href="{{URL::to('franchise/accounts/view-advance-details')}}">
                            	<h5 class="mb-0 font-weight-medium text-primary">Total Advance</h5>
                            </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          <h3> 	@if($reimbursement->Totalreimbursement) 
                          	{{$reimbursement->Totalreimbursement}}
                          	@else 
                          	0.00
                          	@endif &#x20b9; </h3>
                   <a target="_blank" href="{{URL::to('franchise/accounts/view-reimbursement-details')}}"> <h5 class="mb-0 font-weight-medium text-primary">Total Fuel Reembursement</h5>
                           </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>




 

</div>

<div class="row mt-4">

	<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"><h3>
                            <?php
                              $totalAmount = $totalCash + $totalPndCash; 
                              $UPI = 0.00;
                            ?>
                            @if($upi) 
                            <?php  $UPI = $upi->upiAmount; ?>
                            @endif
                            {{$totalAmount-$UPI}}  &#x20b9; </h3>
                           <a href="#"> <h5 class="mb-0 font-weight-medium text-primary">Cash in bookTou</h5>
                           </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>




<!-- <div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> <h3>@if($upi) 
                            <?php  $UPI = $upi->upiAmount; ?>
                            {{$upi->upiAmount}}
                            @else 
                            0.00
                            @endif &#x20b9; </h3>
                          <a target="_blank" href="{{URL::to('franchise/accounts/view_cash_upi_details')}}">  <h5 class="mb-0 font-weight-medium text-primary">Cash in UPI</h5>
                          </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div> -->

<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> <h3>
                            @if($deposits->extraAmount) 
                            {{$deposits->extraAmount}}
                            @else 
                            0.00
                            @endif &#x20b9; </h3>
                          <a target="_blank" href="{{URL::to('franchise/accounts/view_extra_deposit_details')}}">  <h5 class="mb-0 font-weight-medium text-primary">Total Extra</h5>
                          </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>

<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> <h3>
                            @if($deposits->dueAmount) 
                            {{$deposits->dueAmount}}
                            @else 
                            0.00
                            @endif &#x20b9; </h3>
                          <a target="_blank" href="{{URL::to('franchise/accounts/view_due_deposit_details')}}">  <h5 class="mb-0 font-weight-medium text-primary">Total Due</h5>
                          </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>
</div>


@endsection

@section("script")
<script>



</script>
@endsection
