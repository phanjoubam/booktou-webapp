@extends('layouts.franchise_theme_03')
@section('content')

<div class="row"> 

<div class="col-md-12">
@if(isset($deposits)) 
<div class="row mt-3"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header">   
       	<div class='row'>
       		<div class='col-md-8'>
       			<h3>Deposit list for month of {{$month}}, {{$year}}</h3>
       		</div>
       		<div class='col-md-4'>
       			  
       		</div>

       	</div>
 

    </div>
<div class="card-body">  
	
	<table class="table  table-bordered">
	<thead>
		<tr> 
			<th>Date</th>
			<th>Deposit By</th>
			<th>Category</th>
			<th>Details</th>
			<th>Amount</th> 
		</tr>
	</thead>
	<tbody>
		@php 
			$i = 0;
			$totalAgentDeposit=0.00;
			$totalMerchantDuePayback=0.00;
			$totalDeposit=0.00;  

		@endphp 
		@foreach($deposits as $item)
		@php 
			$i++;  
		@endphp

		@switch($item->category )
			@case("daily collection deposit")
				@php 
					$totalAgentDeposit += $item->amount;
				@endphp  
			@break
			@case("merchant due")
				@php 
					$totalMerchantDuePayback += $item->amount;
				@endphp  
			@break
			@default
				@php 
					$totalDeposit += $item->amount;
				@endphp  
			@break  
		@endswitch  

		<tr> 
			<td>{{ date('d-m-Y', strtotime($item->deposit_date))}}</td>
			<td>
				@if($item->bin == 0)
					@foreach($all_staffs as $staff)
						@if($staff->id == $item->deposit_by)
							<span class='badge badge-warning badge-pill'>{{ $staff->fullname }}</span>
							@break
						@endif
					@endforeach

				
				@else
					@foreach($all_businesses as $business)
						@if($business->id == $item->bin)
							<span class='badge badge-danger badge-pill'>{{ $business->name }}</span> 
							@break
						@endif
					@endforeach
				@endif</td>
			<td>{{$item->category}}</td>
			<td>{{$item->details}}</td> 
			<td>{{$item->amount}}</td> 
		</tr> 

		@endforeach

		<tr>
			<th colspan="4" class='text-right'>Total Agent Deposit</th>
			<th>{{ $totalAgentDeposit }}</th> 
		</tr>	
		<tr>
			<th colspan="4" class='text-right'>Total Merchant Due Deposit</th>
			<th>{{ $totalMerchantDuePayback }}</th> 
		</tr>

		<tr>
			<th colspan="4" class='text-right'>Combined Deposits</th>
			<th>{{ $totalDeposit + $totalAgentDeposit + $totalMerchantDuePayback  }}</th> 
		</tr>


	</tbody>
</table> 

</div>
</div>
</div>
</div> 
@endif

</div>
</div>

@endsection

@section("script")
<script>



</script>
@endsection