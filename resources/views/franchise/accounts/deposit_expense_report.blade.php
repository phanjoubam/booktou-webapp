@extends('layouts.franchise_theme_03')
@section('content')

<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header">   
       	<div class='row'>
       		<div class='col-md-8'>
       			<h3>Expenditure list for month of {{$month}}, {{$year}}</h3>
       		</div>
       		<div class='col-md-4'>
       			 <form class=" "   action="{{action('Admin\AdminDashboardController@viewExpenditureReport') }}" method="get">
					<div class="form-row">
						<div class="col-md-4"> 
							<select name='month' class="form-control form-control-sm  ">
					          <option <?php if( $month  == 1 ) echo "selected"; ?> value='1'>January</option>
					          <option <?php if( $month== 2 ) echo "selected"; ?>  value='2'>February</option>
					          <option <?php if( $month == 3 ) echo "selected"; ?> value='3'>March</option>
					          <option <?php if( $month== 4 ) echo "selected"; ?> value='4'>April</option>
					          <option <?php if( $month == 5 ) echo "selected"; ?> value='5'>May</option>
					          <option <?php if( $month == 6 ) echo "selected"; ?> value='6'>June</option>
					          <option <?php if( $month == 7 ) echo "selected"; ?> value='7'>July</option>
					          <option <?php if( $month == 8 ) echo "selected"; ?> value='8'>August</option>
					          <option <?php if( $month == 9 ) echo "selected"; ?> value='9'>September</option>
					          <option <?php if($month == 10 ) echo "selected"; ?> value='10'>October</option>
					          <option <?php if( $month== 11 ) echo "selected"; ?> value='11'>November</option>
					          <option <?php if( $month == 12 ) echo "selected"; ?> value='12'>December</option> 
					        </select>
						</div>
						<div class='col-md-4'>
							<input readonly name='year' class="form-control form-control-sm " value="{{ date('Y') }}" /> 
						</div>
						<div class="col-md-2"> 
							<button type='submit' name='btnsearch' value="search" class="btn btn-primary">Search</button>
						</div> 
					</div>
					</form>

       		</div>

       	</div>
 

    </div>
<div class="card-body"> 

	@if(isset($expense)) 
	<table class="table  table-bordered">
	<thead>
		<tr> 
			<th>Date</th>
			<th>Expend By</th>
			<th>Category</th>
			<th>Details</th>
			<th>Amount</th> 
		</tr>
	</thead>
	<tbody>
		@php 
			$i = 0;
			$totalAdvance=0.00;
			$totalFuel=0.00;
			$totalExpenditure=0.00; 
			$cdate = date('Y') . "-" . date('m') . "-01" ;

		@endphp 
		@foreach($expense as $item)
		@php 
			$i++; 
			$cdate = $item->use_date;
		@endphp

		@switch($item->category )
			@case("advance")
				@php 
					$totalAdvance += $item->amount;
				@endphp  
			@break
			@case("fuel")
				@php 
					$totalFuel += $item->amount;
				@endphp  
			@break
			@default
				@php 
					$totalExpenditure += $item->amount;
				@endphp  
			@break  
		@endswitch  

		<tr> 
			<td>{{ date('d-m-Y', strtotime($item->use_date))}}</td>
			<td>
				@if($item->use_by == 0)
				<span class='badge badge-info badge-pill'>Office</span>
				@else
					@foreach($all_staffs as $staff)
						@if($staff->id == $item->use_by)
							<span class='badge badge-warning badge-pill'>{{ $staff->fullname }}</span>

							@break
						@endif
					@endforeach
				@endif</td>
			<td>{{$item->category}}</td>
			<td>{{$item->details}}</td> 
			<td>{{$item->amount}}</td> 
		</tr> 

		@endforeach

		<tr>
			<th colspan="4" class='text-right'>Total Salary Advance</th>
			<th>{{ $totalAdvance }}</th> 
		</tr>	
		<tr>
			<th colspan="4" class='text-right'>Total Fuel Advance</th>
			<th>{{ $totalFuel }}</th> 
		</tr>

		<tr>
			<th colspan="4" class='text-right'>Combined Expenditure</th>
			<th>{{ $totalExpenditure + $totalAdvance + $totalFuel }}</th> 
		</tr>


	</tbody>
</table>

@else 
<p class='alert alert-info'>No expenditure report on the selected date.</p>
@endif
</div>
</div>
</div>
</div>



@if(isset($deposits)) 
<div class="row mt-3"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header">   
       	<div class='row'>
       		<div class='col-md-8'>
       			<h3>Deposit list for month of {{$month}}, {{$year}}</h3>
       		</div>
       		<div class='col-md-4'>
       			  
       		</div>

       	</div>
 

    </div>
<div class="card-body">  
	
	<table class="table  table-bordered">
	<thead>
		<tr> 
			<th>Date</th>
			<th>Deposit By</th>
			<th>Category</th>
			<th>Details</th>
			<th>Amount</th> 
		</tr>
	</thead>
	<tbody>
		@php 
			$i = 0;
			$totalAgentDeposit=0.00;
			$totalMerchantDuePayback=0.00;
			$totalDeposit=0.00;  

		@endphp 
		@foreach($deposits as $item)
		@php 
			$i++;  
		@endphp

		@switch($item->category )
			@case("daily collection deposit")
				@php 
					$totalAgentDeposit += $item->amount;
				@endphp  
			@break
			@case("merchant due")
				@php 
					$totalMerchantDuePayback += $item->amount;
				@endphp  
			@break
			@default
				@php 
					$totalDeposit += $item->amount;
				@endphp  
			@break  
		@endswitch  

		<tr> 
			<td>{{ date('d-m-Y', strtotime($item->deposit_date))}}</td>
			<td>
				@if($item->bin == 0)
					@foreach($all_staffs as $staff)
						@if($staff->id == $item->deposit_by)
							<span class='badge badge-warning badge-pill'>{{ $staff->fullname }}</span>
							@break
						@endif
					@endforeach

				
				@else
					@foreach($all_businesses as $business)
						@if($business->id == $item->bin)
							<span class='badge badge-danger badge-pill'>{{ $business->name }}</span> 
							@break
						@endif
					@endforeach
				@endif</td>
			<td>{{$item->category}}</td>
			<td>{{$item->details}}</td> 
			<td>{{$item->amount}}</td> 
		</tr> 

		@endforeach

		<tr>
			<th colspan="4" class='text-right'>Total Agent Deposit</th>
			<th>{{ $totalAgentDeposit }}</th> 
		</tr>	
		<tr>
			<th colspan="4" class='text-right'>Total Merchant Due Deposit</th>
			<th>{{ $totalMerchantDuePayback }}</th> 
		</tr>

		<tr>
			<th colspan="4" class='text-right'>Combined Deposits</th>
			<th>{{ $totalDeposit + $totalAgentDeposit + $totalMerchantDuePayback  }}</th> 
		</tr>


	</tbody>
</table> 

</div>
</div>
</div>
</div> 
@endif


@endsection

@section("script")
<script>

 $(".redirect").click(function(){

    var code = $(this).attr("data-date");
    var key = $(this).attr("data-dkey");
    var ser = "//localhost/alpha/";

    switch(code)
    { 
        case "1":
            
            var win = window.open( ser+"admin/staff/e-daily-expense?o=" + key , '_blank');
            if (win) { 
                win.focus();
            } else { 
                alert('Please allow popups for this website');
            } 
            break;
 


    }


});
 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
</script>

@endsection

