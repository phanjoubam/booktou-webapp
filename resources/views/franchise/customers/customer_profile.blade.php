@extends('layouts.franchise_theme_03')
@section('content')
 
 
<?php 

$item = $data['profiles'];
 
?> 

<div class="row">
   @if(isset($item))

 <div class="  col-md-6 col-sm-12 col-xs-12  "> 
 

  <div class="card  ">
    <div class="card-body  ">  

      
        

        <?php 

            if($item->cusImage=="")
             { 
                $image_url =  URL::to('/') . "/public/assets/image/no-image.jpg"; 
             } 
          else
             {
              
                $image_url =  URL::to('/') . $item->cusImage;
             }     
         ?>
      <img class="rounded" src='{{ $image_url }}' alt="..." height="200px" width="240px">
              
       
       <br/>

      
        <h3 class="text-danger">{{$item->cusFullName}}</h3>
        <strong>Warning Status:</strong> <span class='badge badge-danger'>{{$item->flagged}}</span>
 
<br/> 
      <hr class="my-4" /> 


     <div class="row">
       <div class="col-md-6">
        <div><strong>Customer ID # :</strong> {{$item->cusProfileId}}</div><br/>

         <div><strong>Date of Birth :</strong> {{date('d-m-Y', strtotime($item->cusDateOfBirth))}}</div><br/>

       </div>

       <div class="col-md-6">
         <div><strong>Contact No. :</strong> {{$item->cusPhone}}</div><br/>
         <div><strong>Email : </strong><span class="text-primary"> <?php echo $item->cusEmail ; ?></span></div><br/>

       </div>

     </div>


</div>

   </div>
     



      <div class="card mt-3">
        <div class="card-body"> 
      <div class="card-title"> 
       <div class="title">Customer Reviews</div>
     </div>

      <hr class="my-4" /> 

<div class="row">
<div class="col-md-12">
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<a class="btn card-link">  {{$data['totalRating']}} rating </a>
</div>
</div>

<div class="row">
<div class="col-md-12">

<div class="row">
<div class="col-md-2">
  5 star
</div>

<div class="col-md-8">
  <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingFiveCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

</div>
</div>
<div class="col-md-2">
  {{$data['ratingFiveCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-2">
  4 star
</div>

<div class="col-md-8">
 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingFourCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingFourCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-2">
  3 star
</div>

<div class="col-md-8">

 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingThreeCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingThreeCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-2">
  2 star
</div>

<div class="col-md-8">

 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingTwoCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingTwoCount']}}%
</div>
</div>


<div class="row">
<div class="col-md-2">
  1 star
</div>

<div class="col-md-8">
 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingOneCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div> 
</div>
</div>
<div class="col-md-2">
  {{$data['ratingOneCount']}}%
</div>

</div>
 

     </div>
   </div>
 </div>


</div>



  </div>


       
  

 <div class="col-md-6 col-sm-12 col-xs-12"> 
<div class="card ">
  <div class="card-body ">
   <div class="row">
    <div class="col-md-12">  
      <div class="card-heading"> 
       <div class="title">Address Details</div>
     </div>

     <hr class="my-4" /> 

     <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12">

     <div><strong>Locality :</strong> {{$item->cusLocality}}</div><br/>


                 <div><strong>City :</strong> {{$item->cusCity}}</div><br/>
                             
    
    
                            
                  <div><strong>Pin Code :</strong> {{$item->cusPin}}</div>

     </div>

     <div class="col-md-6 col-sm-12 col-xs-12">

    

                 <div><strong>Landmark :</strong> {{$item->cusLandmark}}</div><br/>

    
                <div><strong>State :</strong> {{$item->cusState}}</div><br/>
    
       

     </div>

   </div>
   
     </div>
   </div>
 </div>
</div>


<div class="card mt-4">
  <div class="card-body ">
    <div class="card-title"> 
       <div class="title">Recent Orders</div>
     </div>
<hr class="my-4" />  

 @if(isset($data['orders']) && count($data['orders']) > 0)

@foreach($data['orders'] as $itemO) 

    <div class="row">
      <div class="col-md-4">
        <strong>Order No. :</strong> <a href="{{ URL::to('/admin/order/view-information') }}?orderno={{$itemO->id}}" target='_blank'>{{$itemO->id}}</a>
      </div>
      <div class="col-md-4">
        <strong>Order Date :</strong> {{date('d-m-Y', strtotime($itemO->book_date))}}
      </div>
      <div class="col-md-4">
        <strong>Order Status :</strong> {{$itemO->book_status}}
      </div>
    </div>
   
    @endforeach
    
    @else
    <div class="text-center">No any orders!</div>
    @endif
</div>
</div>


</div>

@else 
 <div class="  col-md-12 col-sm-12 col-xs-12  "> 
        <div class='alert alert-danger'>No matching customer found.</div>
      </div>
     @endif 


</div>
 

   
 


@endsection