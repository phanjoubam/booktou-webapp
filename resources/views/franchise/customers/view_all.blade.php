@extends('layouts.franchise_theme_03')
@section('content')

  
   <div class="row">
     <div class="col-md-12">  
     <div class="card  ">  
      <div class="card-header"> 
           <div class="row"> 
               <div class="col-md-4"> 

                 <div class="panel-heading">
              <div class="card-title">
                  <div class="title">{{ $data['title'] }}</div>
                </div>
            </div>
                
                </div>
               <div class="col-md-8 text-right"> 
                  <form class="form-inline" method="get" action="{{  URL::to('/admin/systems/search-customers')    }}">
                      {{ csrf_field() }}
                      <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Name:</label> 
                        <input   class='form-control form-control-sm  my-1 mr-sm-2 ' type="text"  name='search_key'  /> 

              <label for="inlineFormCustomSelectPref">Is cloud message receipent:</label> 
            <select   class='form-control form-control-sm custom-select my-1 mr-sm-2'   name='filter_key'>
              <option value='both'>Any Value</option> 
              <option value='present'>YES</option> 
              <option value='absent'>NO</option> 
            </select>
                        <button type="submit" class="btn btn-primary btn-xs my-1" value='search' name='btn_search'>Search</button>
                  </form>
   
             </div>
          </div> 
          </div> 
  <div class="card-body">    
<div class="table-responsive">
    <table class="table">
                    <thead class="text-primary">  
                  <tr>  
                     <th scope="col">Customer ID</th>
                    <th scope="col">Full Name</th>
                    <th scope="col" width='250px;' >Last OTP</th> 
                    <th scope="col">Total Orders So Far</th> 
                    <th scope="col">Cloud Msg. Receipent</th>  
                    <th scope="col" class='text-center'>Action</th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                { 
                ?>
  
                  <tr >  
                    <td>
                    <a   href="{{  URL::to('/franchise/view-complete-profile/'.$item->id )}}"><strong>{{$item->id}} </strong></a>
                  </td>
                   <td width='250px;'>
                    <strong>{{$item->fullname}} </strong>@if($item->category ==  "0")
                    <span class='badge badge-success'>CUSTOMER</span>
                  @elseif($item->category ==  "100")
                    <span class='badge badge-danger'>AGENT</span>
                  @endif<br/>
                  {{ $item->locality}},<br/>
                  {{ $item->landmark}}        
                  </td>
                  <td  >
                   {{ $item->otp}}
                  </td> 

                  <td> 
                     @php  
                            $totalOrders = 0;
                            $totalDelivered = 0;
                            $totalCancelled = 0; 
                        @endphp 

                      @foreach ($data['orders'] as $order)
                       
                           @if($item->id  == $order->book_by) 
                              
                           @switch($order->book_status)

                              @case("new")
                                @php  
                                    $totalOrders++;
                                @endphp
                              @continue

                              @case("confirmed")
                                @php  
                                    $totalOrders++; 
                                @endphp
                              @continue 
                              @case("order_packed")
                                @php  
                                    $totalOrders++; 
                                @endphp
                              @continue

                              @case("package_picked_up")
                                @php  
                                    $totalOrders++; 
                                @endphp
                              @continue

                              @case("pickup_did_not_come")
                                @php  
                                    $totalOrders++; 
                                    $totalCancelled++; 
                                @endphp
                              @continue

                               @case("in_route")
                                @php  
                                    $totalOrders++; 
                                @endphp
                              @continue

                               @case("completed")
                                @php  
                                    $totalOrders++;
                                    $totalDelivered++;
                                @endphp
                              @continue

                              @case("delivered")
                               @php  
                                    $totalOrders++;
                                    $totalDelivered++; 
                                @endphp
                              @continue

                               @case("delivery_scheduled")
                                @php  
                                    $totalOrders++; 
                                @endphp
                              @continue 

                                @case("canceled")
                                @php  
                                    $totalOrders++; 
                                    $totalCancelled++; 
                                @endphp
                              @continue 

                              @endswitch

                           @endif
                      @endforeach

 <div class="btn-group" role="group" aria-label="Basic example">
   <button type="button" class="btn btn-info redirect" style='width: 120px'  data-pgc='2' >Total: {{ $totalOrders }}</button>
   <button type="button" class="btn btn-danger redirect"  style='width: 120px' data-pgc='1' >Cancelled: {{ $totalCancelled }}</button>
   <button type="button" class="btn btn-success delete"  style='width: 120px'  >Delivered: {{ $totalDelivered }}</i></button>
 </div>


                  </td>


                  <td class='text-center'>
                    @if($item->firebase_token !=  "")
                    <span class='badge badge-success'>YES</span>
                  @else 
                    <span class='badge badge-danger'>NO</span>
                  @endif
                </td>
               
                   

                    <td class="text-center">
                      <div class="dropdown"> 
                        <a class="btn btn-primary btn-sm btn-icon-only text-light " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-cog "></i>
                        </a> 
                        <ul class="dropdown-menu dropdown-user pull-right"> 
                          <li> <a class="dropdown-item btn btn-notify" data-id="{{ $item->id }}">Post Message</a></li>
                          <li><a class="dropdown-item btn" href="{{  URL::to('/admin/customer/view-complete-profile/'.$item->id )}}">View Profile</a></li> 
                        </ul>
                    </div>          
                </td>

              </tr>
              <?php
              $i++; 
            }
      ?>
    </tbody>
  </table> 
  {{ $data['results']->appends(request()->input())->links() }}  
</div>
       </div>     </div>

          </div>
        </div>  
   </div>
 
       </div>
 

 <div class="modal" id='notifyModal' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Send Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="height: 400px;overflow-y: scroll;">
         
    <div class="table-responsive">
    <table class="table">
                    <thead class=" text-primary">  
                  <tr class="text-center" style='font-size: 12px'>  
                     <th scope="col"></th>
                    <th scope="col">Title</th>
                    <th scope="col">Body</th> 
                    <th scope="col" class='text-center'></th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 
 
                
                foreach ($data['messages'] as $item)
                { 
                ?>
  
                  <tr >  
                     <td class='text-center'>
                    <input type="radio" name='cmids' class='cmids' value="{{ $item->id }}"  /> 
                  </td>  

                    <td>
                    <strong>{{$item->id}} </strong>
                  
                   <td>
                    <strong>{{$item->title}}</strong>
                  </td>
                  <td>
                   {{ $item->body}}    
                  </td> 
                  
   
                         </tr>
                         <?php 
                       }

                          ?>
                </tbody>
                  </table>
            
              </div>

  
   <input type="hidden" id='hidcid' name='hidcid' /> 
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span> 
        <button type="button" class="btn btn-success" id="btn-notify" data-conf="" data-id="" >Send Message</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
 


  </div>
</div>
 



@endsection

@section("script")

<script>

 
 
$(document).on("click", ".btn-notify", function()
{
  $cid = $(this).attr('data-id');

  $("#hidcid").val($cid); 
  $("#notifyModal").modal("show") 


});





$('body').delegate('#btn-notify','click',function()
{

  var cmid ; 
  $('.cmids').each(function() 
  {
    if($(this).is(':checked'))
    {
      cmid =  $(this).val();
    }
  });

  var mid = $("#hidcid").val(); 
 

  if(cmid > 0 && mid > 0)
  {
    var json = {}; 
    json['cmid'] = cmid ;
    json['mid'] = mid ; 
    $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $.ajax({
        type: 'post',
        url: api + "v2/web/customer-care/notification/send-cloud-message" ,
        data: json,
        success: function(data)
        {
          data = $.parseJSON(data);   
          $(".loading_span").html(" ");
          $("#notifyModal").modal("hide") 
        },
        error: function( ) 
        {
          $(".loading_span").html(" ");
          alert(  'Something went wrong, please try again'); 
        } 

      });
  }
  else 
  {
    alert("No receipt or message selected!");
  } 

})



</script> 

@endsection 