@extends('layouts.franchise_theme_03')
@section('content')
 
 
<?php 

$item = $data['profiles'];
 
?> 

<div class="row">
   @if(isset($item))

 <div class="  col-md-6 col-sm-12 col-xs-12  "> 
 

  <div class="card  ">
    <div class="card-body  ">  

      
        

        <?php 

            if($item->cusImage=="")
             { 
                $image_url =  URL::to('/') . "/public/assets/image/no-image.jpg"; 
             } 
          else
             {
              
                $image_url =  URL::to('/') . $item->cusImage;
             }     
         ?>
      <img class="rounded" src='{{ $image_url }}' alt="..." height="200px" width="240px">
              
       
       <br/>

      
        <h3 class="text-danger">{{$item->cusFullName}}</h3>
        <strong>Warning Status:</strong> <span class='badge badge-danger'>{{$item->flagged}}</span>
 
<br/> 
      <hr class="my-4" /> 


     <div class="row">
       <div class="col-md-6">
        <div><strong>Customer ID # :</strong> {{$item->cusProfileId}}</div><br/>

         <div><strong>Date of Birth :</strong> {{date('d-m-Y', strtotime($item->cusDateOfBirth))}}</div><br/>

       </div>

       <div class="col-md-6">
         <div><strong>Contact No. :</strong> {{$item->cusPhone}}</div><br/>
         <div><strong>Email : </strong><span class="text-primary"> <?php echo $item->cusEmail ; ?></span></div><br/>

       </div>

     </div>


</div>

   </div>
      


  </div>


       
  

 <div class="col-md-6 col-sm-12 col-xs-12"> 
<div class="card ">
  <div class="card-body ">
   <div class="row">
    <div class="col-md-12">  
      <div class="card-heading"> 
       <div class="title">Address Details</div>
     </div>

     <hr class="my-4" /> 

     <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12">

     <div><strong>Locality :</strong> {{$item->cusLocality}}</div><br/>


                 <div><strong>City :</strong> {{$item->cusCity}}</div><br/>
                             
    
    
                            
                  <div><strong>Pin Code :</strong> {{$item->cusPin}}</div>

     </div>

     <div class="col-md-6 col-sm-12 col-xs-12"> 

                 <div><strong>Landmark :</strong> {{$item->cusLandmark}}</div><br/>  
                <div><strong>State :</strong> {{$item->cusState}}</div><br/> 

     </div> 
   </div> 
     </div>
   </div>
 </div>
</div>
 

</div>

@else 
 <div class="  col-md-12 col-sm-12 col-xs-12  "> 
        <div class='alert alert-danger'>No matching customer found.</div>
      </div>
     @endif 


</div>
 

   
 


@endsection