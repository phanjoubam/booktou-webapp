@extends('layouts.franchise_theme_03')
@section('content')

@php 
    $target = 20; 
    $totalOrders = 0; 
    $totalPndOrders = 0; 
   

    $totalnormalOrderSales = 0; 
    $totalPndOrderSales = 0;
   

    $totalOrdersCompleted =0 ;
    $totalPndOrdersCompleted = 0; 
    
@endphp
 
<?php 

  $totalPndOrdersCompleted =0;
  $totalOrdersCompleted = 0; 
  $totalassitsOrdersCompleted = 0; 

  $totalAssitsOrders = 0;
  $totalNormalOrders = 0;
  $totalPndOrders =0; 

  $totalassitsOrderSales = 0;
  $total_amount_achieved =0;

  foreach($normal_orders as $item)
  {
     if( $item->book_status == "delivered" || $item->book_status == "completed"   )
     {
      $totalOrdersCompleted++;
      $totalNormalOrders++;
      $totalnormalOrderSales+= $item->total_cost;
     }
  }

  foreach($pnd_orders as $item)
  {
     if( $item->book_status == "delivered" || $item->book_status == "completed"   )
     {
      $totalPndOrdersCompleted++;
       $totalPndOrders++;
      $totalPndOrderSales+=$item->total_amount;
     }
  }

   foreach($assits_orders as $item)
  {
     if( $item->book_status == "delivered" || $item->book_status == "completed"   )
     {
      $totalassitsOrdersCompleted++;
      $totalAssitsOrders++;
      $totalassitsOrderSales+=$item->total_amount;
     }
  }


?>
 
 
<div class="row">
<?php 

$dates = array(); 
        $pnddates = $normaldates = array(); 
        $pnd_orders  = array();
        $normal_orders  = array();

       // $pnd_orders[]= $normal_orders[]=0;

        $begin = new Datetime($report_date);
        $end = new Datetime($todayDate);

        for($i = $begin; $i <= $end; $i->modify('+1 day')){
           $dates[] ='"'.date('Y-m-d',strtotime($i->format('Y-m-d'))).'"';
           $normaldates[]='"'.date('Y-m-d',strtotime($i->format('Y-m-d'))).'"'; 
           $pnddates[]='"'.date('Y-m-d',strtotime($i->format('Y-m-d'))).'"';
        }

          
 
            $arrayLength = count($dates);
        
            $i = 0;
            while ($i < $arrayLength)
            {
                
                foreach($normal_sales_chart_data as $item)
                {
                  if ('"'.$item->serviceDate.'"'==$dates[$i])
                  {
                    $normaldates[$i] = $item->normalOrder;
                  }
                }

                foreach($pnd_sales_chart_data as $item)
                {
                  if ('"'.$item->serviceDate.'"'==$dates[$i]) 
                  {
                    $pnddates[$i] = $item->pndOrders;
                          
                       }  
                         
                    }  
              
                   
                   

                $i++;


            }

            $j = 0;
            while ($j < $arrayLength)
            {

                   
                        
                      if ($pnddates[$j]==$dates[$j]) {
                           
                           $pnddates[$j]=0;
                          
                       } 


                       if ($normaldates[$j]==$dates[$j]) {
                           
                           $normaldates[$j]=0;
                          
                       }  
                         
                     
            $j++;
          }

?>

<div class="col-md-3">
       <div class="card border-left-info shadow h-100 py-2">
          <div class="card-body">
             <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Normal Order Count ({{$totalNormalOrders}})</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">₹ {{$totalNormalOrders}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
            </div>
      </div>
  </div>

<div class="col-md-3">
<div class="card border-left-warning shadow h-100 py-2">
    <div class="card-body">
  <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                PND Order Count({{$totalPndOrderSales}})</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">₹ {{$totalPndOrderSales}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-rupee-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
</div></div>
</div>


<div class="col-md-3">
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
  <div class="row no-gutters align-items-center">
      <div class="col mr-2">
        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                               Assists Count({{$totalassitsOrderSales}})</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">₹ {{$totalassitsOrderSales}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-rupee-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
</div></div>
</div>


<div class="col-md-3">
<div class="card border-left-success shadow h-100 py-2">
    <div class="card-body">
  <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
            Active Cutomers Count 
          </div>
          <div class="h5 mb-0 font-weight-bold text-gray-800">
            <a href="{{ URL::to('/franchise/active-users-activity-history') }}" target='_blank'>
                @if(count($active_users)>0)
                    {{ count($active_users) }}  
                @else
                    0
                @endif
            </a>
          </div>
        </div>
        <div class="col-auto">
          <i class="fas fa-calendar fa-2x text-gray-300"></i>
        </div>
      </div>
      </div>
  </div>
</div> 
</div>

 

<div class="row mt-4">
<div class="col-md-6 grid-margin main-panel-dashboard ">
                

                <div class="card">
                  <div class="card-header">
                            <h4 class="card-title mb-0">{{ date('Y') }} Sales Performance Graph</h4>
                   
                        </div>
                        <div class="card-body"> 

                          <div class="chart-area">
                                        <canvas id="myAreaChart"></canvas>
                                    </div>
                    
                     <!-- <canvas class="mt-5" height="120" id="sales-statistics-overview"></canvas>  -->
                  </div>
                </div>


  <div class="card mt-4">
                  
                </div> 

</div> 
<div class="col-md-6"> 
<div class="row">   
      


      

      <div class="col-xs-6 col-md-6 grid-margin stretch-card">
        <div class="card card-default">
          <div class="card-body easypiechart-panel">
            <h6>Normal Orders</h6>
            <div class="easypiechart" id="easypiechart-orange" data-percent="{{ (   $totalNormalOrders  * 100 ) /  30  }}" >
              <a href="{{ URL::to('/franchise/orders/normal-orders') }}"> 
              <span class="percent h1">{{ $totalNormalOrders }}/30</span>
            </a>
            </div>
          </div>
        </div>
      </div>

<div class="col-xs-6 col-md-6 grid-margin stretch-card">
        <div class="card card-default">
          <div class="card-body easypiechart-panel">
            <h6>Pick-and-Drop Orders</h6>
            <div class="easypiechart" id="easypiechart-teal" data-percent="{{ (   $totalPndOrders  * 100 ) / 80  }}" >
              <a href="{{ URL::to('/franchise/pick-and-drop-orders/view-all') }}"> 
                <span class="percent h1">{{ $totalPndOrders  }}/80</span></a>
            </div>
          </div>
        </div>
      </div>

 <div class="col-xs-6 col-md-6 grid-margin stretch-card">
        <div class="card card-default">
          <div class="card-body easypiechart-panel">
            <h6>Assists Orders</h6>
            <div class="easypiechart" id="easypiechart-red"  > 
             <a href="#">  
              <span class="percent h3">{{ $totalAssitsOrders  }}/10</span> 
             </a>
            </div>
          </div>
        </div>
      </div> 

<div class="col-xs-6 col-md-6">
        <div class="card bg-danger card-default">
          <div class="card-body easypiechart-panel">
            @if(date('d') <=15) 
              <h4 class='white'>Min Target till 15th, {{ date('F') }}</h4>
              <div class="easypiechart"     >
                  <span class="percent h3 white">
                    <?php 
                    $total_amount_achieved = $normal_income_achieved + $pnd_income_achieved + 
                    $assits_income_achieved;?>
                     {{  $total_amount_achieved }} 
                     of 
                     {{ ( $sales_target != null ) ? $sales_target->min / 2 : 15000 }} ₹
                  </span> 
              </div>
            @else
              <h6 class='white'>Min Target till {{ cal_days_in_month(CAL_GREGORIAN , date('m'),  date('Y') ) }}, {{ date('F') }}</h6>
              <div class="easypiechart"     >
                  <span class="percent h3 white">
                  <?php  $total_amount_achieved = $normal_income_achieved + $pnd_income_achieved + 
                    $assits_income_achieved;?>
                  
                     {{  $total_amount_achieved }} 
                    of 
                    {{ ( $sales_target != null ) ? $sales_target->min   : 30000 }} ₹
                  </span> 
              </div> 
            @endif
          </div>
        </div>
      </div>




  </div>


 


                    <div class="card card-default mt-4">
                        <div class="card-header">
                            Recently Added Customer
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                               @if(count($new_customers ) > 0 ) 

                                <table class="table table-striped">
                                    <thead>
                                        <tr> 
                                            <th>Customer Name</th>
                                            <th>Phone</th>
                                             <th>OTP</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($new_customers as $cust)
                                        <tr> 
                                          <td class="text-left">
                                            <a href="{{ URL::to('/admin/customer/view-complete-profile') }}/{{  $cust->id  }}" target='_blank'>
                                              {{$cust->fullname }}
                                            </a> 
                                          </td>
                                           <td class="text-left">{{$cust->phone }}</td>
                                            <td class="text-left"><span class='badge badge-primary'>{{ $cust->otp == -1 ?  "Signup Complete" : $cust->otp  }}</span></td>
                                          <td class="td-actions text-right">
                                            {{$cust->locality}}, {{$cust->landmark }}
                                          </td>
                                        </tr>
                                         
                                        @endforeach
                                    </tbody>
                                </table>

                                @else 
                                <p class='alert alert-info'>No New signup today.</p>
                                @endif 

                            </div>
                        </div>
                    </div> 
                 
                      <!--    Striped Rows Table  -->

 




                 


                     
                </div>



</div>


@endsection


@section("script")

<script> 

//Area chart

<?php 
        
        // $dates = array();  
        // $pnd_orders  = array();
        // $normal_orders  = array();

        // foreach($pnd_sales_chart_data as $item)
        // {
          
        //   $dates[] =  '"'.  $item->serviceDate . '"' ;
        //   $pnd_orders[] =  $item->pndOrders ;
        // }

        // foreach($normal_sales_chart_data as $item)
        // { 
        //   $normal_orders[] =  $item->normalOrder ;
        // }

      ?>  

      var data_1_1 = [ <?php echo implode(',', $pnddates) ; ?> ]; 
      var data_1_2 = [ <?php echo implode(',', $normaldates) ; ?> ];
  // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

// Area Chart Example
var ctx = document.getElementById("myAreaChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [<?php echo implode(",",  $dates ); ?>],
    datasets: [{
      label: "Normal Orders",
      lineTension: 0.3,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "rgba(78, 115, 223, 1)",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: data_1_2,
    },

    {
      label: "PND Orders",
      lineTension: 0.3,
      backgroundColor: "rgba(128, 115, 223, 0.15)",
      borderColor: "rgb(28, 200, 138)",
      pointRadius: 3,
      pointBackgroundColor: "rgb(28, 200, 138)",
      pointBorderColor: "rgb(28, 200, 138)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgb(28, 200, 138)",
      pointHoverBorderColor: "rgb(28, 200, 138)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: data_1_1,
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ':' + number_format(tooltipItem.yLabel);
        }
      }
    }
  }
});







 
// $(function() {
//   'use strict'; 

//     var lineStatsOptions = {
//       scales: {
//         yAxes: [{
//           display: false
//         }],
//         xAxes: [{
//           display: false
//         }]
//       },
//       legend: {
//         display: false
//       },
//       elements: {
//         point: {
//           radius: 0
//         },
//         line: {
//           tension: 0
//         }
//       },
//       stepsize: 100
//     }



//     if ($('#sales-statistics-overview').length) {
//       var salesChartCanvas = $("#sales-statistics-overview").get(0).getContext("2d");
//       var gradientStrokeFill_1 = salesChartCanvas.createLinearGradient(0, 0, 0, 450);
//       gradientStrokeFill_1.addColorStop(1, 'rgba(255,255,255, 0.0)');
//       gradientStrokeFill_1.addColorStop(0, 'rgba(102,78,235, 0.2)');
//       var gradientStrokeFill_2 = salesChartCanvas.createLinearGradient(0, 0, 0, 400);
//       gradientStrokeFill_2.addColorStop(1, 'rgba(255, 255, 255, 0.01)');
//       gradientStrokeFill_2.addColorStop(0, '#14c671');
  
//       <?php 
        
//         $dates = array();  
//         $pnd_orders  = array();
//         $normal_orders  = array();

//         foreach($pnd_sales_chart_data as $item)
//         {
          
//           $dates[] =  '"'.  $item->serviceDate . '"' ;
//           $pnd_orders[] =  $item->pndOrders ;
//         }

//         foreach($normal_sales_chart_data as $item)
//         { 
//           $normal_orders[] =  $item->normalOrder ;
//         }

//       ?>  

//       var data_1_1 = [ <?php //echo implode(',', $pnd_orders) ; ?> ]; 
//       var data_1_2 = [ <?php //echo implode(',', $normal_orders) ; ?> ];
//       var areaData = {
//         labels: [ <?php //echo implode(",",  $dates ); ?>],
//         datasets: [{
//           label: 'PnD Orders',
//           data: data_1_1,
//           borderColor: infoColor,
//           backgroundColor: gradientStrokeFill_1,
//           borderWidth: 2
//         }, {
//           label: 'Normal Orders',
//           data: data_1_2,
//           borderColor: successColor,
//           backgroundColor: gradientStrokeFill_2,
//           borderWidth: 2
//         }]
//       };
//       var areaOptions = {
//         responsive: true,
//         animation: {
//           animateScale: true,
//           animateRotate: true
//         },
//         elements: {
//           point: {
//             radius: 3,
//             backgroundColor: "#fff"
//           },
//           line: {
//             tension: 0
//           }
//         },
//         layout: {
//           padding: {
//             left: 0,
//             right: 0,
//             top: 0,
//             bottom: 0
//           }
//         },
//         legend: false,
//         legendCallback: function (chart) {
//           var text = [];
//           text.push('<div class="chartjs-legend"><ul>');
//           for (var i = 0; i < chart.data.datasets.length; i++) { 
//             text.push('<li>');
//             text.push('<span style="background-color:' + chart.data.datasets[i].borderColor + '">' + '</span>');
//             text.push(chart.data.datasets[i].label);
//             text.push('</li>');
//           }
//           text.push('</ul></div>');
//           return text.join("");
//         },
//         scales: {
//           xAxes: [{
//             display: false,
//             ticks: {
//               display: false,
//               beginAtZero: false
//             },
//             gridLines: {
//               drawBorder: false
//             }
//           }],
//           yAxes: [{
//             ticks: {
//               max:20,
//               min: 0,
//               stepSize: 1,
//               fontColor: "#858585",
//               beginAtZero: false
//             },
//             gridLines: {
//               color: '#e2e6ec',
//               display: true,
//               drawBorder: false
//             }
//           }]
//         }
//       }
//       var salesChart = new Chart(salesChartCanvas, {
//         type: 'line',
//         data: areaData,
//         options: areaOptions
//       });
//       document.getElementById('sales-statistics-legend').innerHTML = salesChart.generateLegend(); 
//     }
  


 
// }) ;



// /* bar graph */

// <?php
  
//   $day = $pnd_revenue = array();
//   foreach($pnd_sales_chart_data  as $item)
//   {
//     $day[] =  '"'.  date('d, M', strtotime($item->serviceDate)) . '"' ;
//     $pnd_revenue[] =  $item->totalRevenue ;
//   }
//   ?> 


// if ($("#barChart").length) { 

//     var barChartCanvas = $("#barChart").get(0).getContext("2d");
//     var barChart = new Chart(barChartCanvas, {
//       type: 'bar',
//       data: {
        

//         labels: [ <?php //echo implode(",",  $day ); ?> ],
//         datasets: [{
//           label: 'Revenue',
//           data: [<?php //echo implode(",",  $pnd_revenue ); ?> ],
//           backgroundColor: ChartColor[0],
//           borderColor: ChartColor[0],
//           borderWidth: 0
//         }]
//       },
//       options: {
//         responsive: true,
//         maintainAspectRatio: true,
//         layout: {
//           padding: {
//             left: 0,
//             right: 0,
//             top: 0,
//             bottom: 0
//           }
//         },
//         scales: {
//           xAxes: [{
//             display: true,
//             scaleLabel: {
//               display: true,
//               labelString: 'Sales by date',
//               fontSize: 12,
//               lineHeight: 2
//             },
//             ticks: {
//               fontColor: '#bfccda',
//               stepSize: 50,
//               min: 0,
//               max: 150,
//               autoSkip: true,
//               autoSkipPadding: 15,
//               maxRotation: 0,
//               maxTicksLimit: 10
//             },
//             gridLines: {
//               display: false,
//               drawBorder: false,
//               color: 'transparent',
//               zeroLineColor: '#eeeeee'
//             }
//           }],
//           yAxes: [{
//             display: true,
//             scaleLabel: {
//               display: true,
//               labelString: 'Earning by delivery',
//               fontSize: 12,
//               lineHeight: 2
//             },
//             ticks: {
//               display: true,
//               autoSkip: false,
//               maxRotation: 0,
//               fontColor: '#bfccda',
//               stepSize: 400,
//               min: 0 
//             },
//             gridLines: {
//               drawBorder: false
//             }
//           }]
//         },
//         legend: {
//           display: false
//         },
//         legendCallback: function (chart) {
//           var text = [];
//           text.push('<div class="chartjs-legend"><ul>');
//           for (var i = 0; i < chart.data.datasets.length; i++) {
//             console.log(chart.data.datasets[i]); // see what's inside the obj.
//             text.push('<li>');
//             text.push('<span style="background-color:' + chart.data.datasets[i].backgroundColor + '">' + '</span>');
//             text.push(chart.data.datasets[i].label);
//             text.push('</li>');
//           }
//           text.push('</ul></div>');
//           return text.join("");
//         },
//         elements: {
//           point: {
//             radius: 0
//           }
//         }
//       }
//     });
//     document.getElementById('bar-traffic-legend').innerHTML = barChart.generateLegend();
//   } 

</script> 
@endsection