@extends('layouts.franchise_theme_01')
@section('content')

@if (session('err_msg'))
    <div class="alert alert-danger">
        {{ session('err_msg') }}
    </div>
@endif


 
<?php 


if(isset($all['prod']))
{

foreach($all['prod'] as $val)
{
  $category = $val->category;
  $prodcode = $val->pr_code;
  $prodname = $val->pr_name;
  $proddescription = $val->description;
  $proddescription = $val->description;
  $price = $val->unit_price;
  $package = $val->packaging;
  
  $foodtype = $val->food_type;
  $discountPC = $val->discountPc;
  $discount = $val->discount;

  $unitname = $val->unit_name;
  $unitvolume = $val->volume;
  $unitvalue = $val->unit_value;

  $stockinitial = $val->initial_stock;
  $stockinhand = $val->stock_inhand;
  $minorder = $val->min_required;
  $maxorder = $val->max_order;

  $id = $val->id;


} 
} 

?>

<div class="container-fluid page-content content-paddding">


<div class="row mt-12">
  
<div class="col-md-10 offset-md-1">

  <div class="card panel-default">
    <div class="card-body">
      <div class="row">
<form action="{{action('Franchise\FranchiseDashboardControllerV1@saveProduct')}}" 
method="post">
  {{csrf_field()}}
      <input type="hidden" @if(isset($id)) value="{{$id}}" @else value="" @endif name="productid">
        <div class="col-lg-12">
          @if(isset($id))<h5>Update Product {{$prodname}}</h5> @else<h5>Add New Product</h5> @endif
          
          <hr>

<div class="form-row">

<div class="form-group col-md-4">
<label>Select Merchant:</label>
 <select class="form-control" name="merchantList">
      <option value="0">--Select Merchant List--</option>

      @foreach($all['business'] as $biz)
      <option value="{{$biz->id}}" 

        @if(request()->route()->bin)

        {{ $biz->id == request()->route()->bin ? 'selected' : '' }}
        
        @endif
        >

        {{$biz->name}}
        


      </option>
      @endforeach
    </select>
</div>

<div class="form-group col-md-4">
    <label>Product Category:</label>
    <select class="form-control" name="productCategory">
      <option value="0">--Select Product Category--</option>

      @foreach($all['cat_'] as $cat)
      <option value="{{$cat->category_name}}" 

        @if(isset($category))
        {{ $cat->category_name == $category ? 'selected' : '' }}
        @endif
        >

        {{$cat->category_name}}


      </option>
      @endforeach
    </select>
</div>

<div class="form-group col-md-4">
 
</div>

</div>


          <div class="form-row">
          <div class="form-group col-md-4">
            <label>Product Code:</label>
    <input type="text" class="form-control" name="productCode" placeholder="Product Code"
    @if(isset($prodcode)) value="{{$prodcode}}" @else value="" @endif>

          </div>

          <div class="form-group col-md-4">

 <label>Product Name:</label>
    <input type="text" class="form-control" name="productName" placeholder="Product Name" 
    @if(isset($prodname)) value="{{$prodname}}" @else value="" @endif>

          </div>

          <div class="form-group col-md-4">
           

          </div>


          



          </div>

<div class="form-row">
  <div class="form-group col-md-4">
    <label>Amount:</label>
    <input type="number" class="form-control" name="productprice" placeholder="Price" 
   id="productPrice" @if(isset($price)) value="{{$price}}" @else value="" @endif >
  </div>
  <div class="form-group col-md-4">
    <label>Package Charge:</label>
    <input type="number" class="form-control" name="productPackCharge" placeholder="Packaging charges" @if(isset($package)) value="{{$package}}" @else value="" @endif >
  </div>


</div>

<div class="form-row">
  <div class="form-group col-md-4">
    <label>Food Type:</label>
<select class="form-control" name="productFoodType">
  <option value="">--Select Food Type--</option>

  <option value="Not-applicable" 
  @if(isset($foodtype))
  {{ $foodtype == "Not-applicable" ? 'selected' : '' }}
  @endif
  >Not-applicable</option>

  <option value="Veg" 
  @if(isset($foodtype))
  {{ $foodtype == "Veg" ? 'selected' : '' }}
  @endif
  >Veg</option>
  <option value="Non-Veg" 
  @if(isset($foodtype))
  {{ $foodtype == "Non-Veg" ? 'selected' : '' }}
  @endif
  >Non-Veg</option>
</select>
  </div>
  <div class="form-group col-md-4">
    <label>Percentage:</label>
    <input type="number" class="form-control" name="productDiscountPercentage" 
    id="productDiscountPC" 
    placeholder="Discount Percentage" @if(isset($discountPC)) value="{{$discountPC}}" @else value="" @endif >
  </div>
  <div class="form-group col-md-4">
    <label>Discount:</label>
    <input type="number" class="form-control" name="productDiscount" id="productDiscount" placeholder="Discount"
    @if(isset($discount)) value="{{$discount}}" @else value="" @endif >
  </div>
  

</div>

<div class="form-row">
  <div class="form-group col-md-4">
    <label>Unit Type:</label>
<select class="form-control" name="productUnit">
  <option value="">--Select Unit Type--</option>
  @foreach($all['unit'] as $val)
  <option value="{{$val->unit}}" 
    
    @if(isset($unitname)) 
    {{ $val->unit == $unitname ? 'selected' : '' }}
    @endif >
    
    {{$val->unit}}

  </option>
  @endforeach
</select>
  </div>
  <div class="form-group col-md-4">
    <label>Volume:</label>
    <input type="number" class="form-control" name="productVolume" 
    placeholder="Volume" @if(isset($unitvolume)) value="{{$unitvolume}}" @else value="" @endif >
  </div>

  <div class="form-group col-md-4">
    <label>Unit Value</label>
    <input type="number" class="form-control" name="productUnitValue" 
    placeholder="Unit value" @if(isset($unitvalue)) value="{{$unitvalue}}" @else value="" @endif >
  </div>
</div>

<div class="form-row">
  <div class="form-group col-md-3">
    <label>Initial Stock:</label>
    <input type="number" class="form-control" name="productInitialStock" 
    placeholder="Initial stock" @if(isset($stockinitial)) value="{{$stockinitial}}" @else value="0" @endif>
  </div>

  <div class="form-group col-md-3">
    <label>Stock in Hand:</label>
    <input type="number" class="form-control" name="productStock" 
    placeholder="Stock in hand" 
    @if(isset($stockinhand)) value="{{$stockinhand}}" @else value="0" @endif>
  </div>

  <div class="form-group col-md-3">
    <label>Minimum:</label>
    <input type="number" class="form-control" name="productMiniumStock" 
    placeholder="Minimum Stock" 
    @if(isset($minorder)) value="{{$minorder}}" @else value="0" @endif>
  </div>

  <div class="form-group col-md-3">
    <label>Maximum:</label>
    <input type="number" class="form-control" name="productMaximumOrder" 
    placeholder="Maximum order"
    @if(isset($maxorder)) value="{{$maxorder}}" @else value="0" @endif>
  </div>
</div>
<div class="form-row">
  <div class="form-group col-md-12">
            
    <label>Description:</label>
    <textarea class="form-control" placeholder="Product Description" name="productDescription" height="100px">@if(isset($proddescription)){{$proddescription}}@endif</textarea>         
          </div>
</div>
<div class="form-row">
  <div class="form-group col-md-4">
    @if(isset($id))
    <button class="btn btn-primary">Update</button>    
   @else
    <button class="btn btn-primary" name="btn_save" value="Save">Save</button>
   @endif
   </div>
</div>




      </div>
    </form>
      </div>
    </div>
  </div>

</div>

 </div>
</div>


@endsection
@section("script")

<script type="text/javascript">
  

  $('#productDiscountPC').keyup(function() {
    var price = $('#productPrice').val();
    if (price=='') {

      $('#productPrice').focus();
    }
    var pc = $('#productDiscountPC').val();

    var discount = price * pc/100;
    $('#productDiscount').val(parseFloat(discount).toFixed(2));

    });

  
$('#productDiscount').keyup(function() {
    var price = $('#productPrice').val();
    if (price=='') {
      
      $('#productPrice').focus();
    }
    var discount = $('#productDiscount').val();

    var pc = discount/price * 100;
    $('#productDiscountPC').val(parseFloat(pc).toFixed(2));

    });

</script>

@endsection 