{{csrf_field()}}

@php

$total=0;
$discount=0;
$delivery=0;

@endphp
<table class="table">
	<thead>
		<th>Sl.no</th>
		<th>Item</th>
		<th>Qty</th>
		<th>Unit</th>
		<th>Unit Price</th>
		<th>Amount</th>
	</thead>
	
@foreach ($product as $list)
	<tr>
		<td>{{$loop->count}}</td>
		<td class="col-md-4">{{$list->pr_name}}</td>
		<td>
		
           
          {{csrf_field()}}  
          

<span class="incr-btn input-group-btn btn btn-danger btn-sm" data-action="decrease" href="#">
          
                <span class="fa fa-minus left"></span>
          
      	  </span>
		  <input type="hidden" value="{{$list->pr_code}}" name="txtproduct_code" class="txtproduct_code" />
		  <input type="hidden" value="{{$list->id}}" name="txtproduct_id"/>
           	  <input class="quantity col-md-3 border-0" type="text" name="quantity"  value="1"/>
          	  <input type="hidden" name="stock-quantity" id="stock_quantity" class="input-number col-md-3" value="{{$list->stock_inhand}}" min="1" max="100">
          
          
          <span class="incr-btn input-group-btn right btn  btn-success btn-sm" data-action="increase" href="#">
          
                <span class="fa fa-plus right"></span>
          
      	  </span>
  
      </td>
		<td>{{$list->unit_name}}</td>
		<td>{{$list->unit_price}}</td>
		<td>{{$list->unit_price}}</td>
	
	</tr>

	@php	

	$total = $list->unit_price;
	$delivery = 0;
	$discount =  $list->discount;
	@endphp


@endforeach

<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td>Total Amount:</td>
	<td><input type="text" class="form-control" value="{{$total}}" name="total_amount" id="total_amount"></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td>Discount:</td>
	<td><input type="text" class="form-control" value="{{$discount}}" name="discount" id="discount"></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td>Delivery charge:</td>
	<td><input type="text" class="form-control" value="{{$delivery}}" name="delivery_charge" id="delivery_charge"></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td>
<input type="submit" class="btn btn-success" value="Save Order" name="btn_save" id="btn_save" />
</td>
</tr>


</table>

<script type="text/javascript">
	
	$(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        var stockValue = parseFloat( $button.parent().find('#stock_quantity').val());
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
        	//alert(stockValue);
        	if(oldValue==stockValue)
        	{
        		alert("You cannot add more quantity for the selected product");
        		$('.quantity').val(stockValue);
        		return;
        	}else{
        	var newVal = parseFloat(oldValue) + 1;
        	}

            
        } else {
            // Don't allow decrementing below 1
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        
//ajax call
        	var data = {
        		'_token': $('input[name=_token]').val(),
                'quantity': $button.parent().find('.quantity').val(),
                'prodCode':$button.parent().find('.txtproduct_code').val(),
            };
            $.ajax({
                url: 'update-quantity',
                type: 'POST',
                data: data,
                success: function (response) {
                    if(response.success)
		            {
		            	

		            }
		            else
			            {
                      
                      alert("failed");

			            }
                }
            });


       // e.preventDefault();

    });




</script>


