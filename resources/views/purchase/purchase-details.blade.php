<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<title>Purchase</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.13.0/themes/prism.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/icon.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</head>


<body>

<div class="container-fluid">
   @if (session('msg'))
    <div class="container">
    <div class="col-md-4">
        
    </div>
    <div class="col-md-4 alert alert-success">
        {{ session('msg') }}
    </div>
    <div class="col-md-4">

    </div>
    
    </div>

@endif


        

    <!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" 
data-toggle="modal" data-target="#ProductWarningmessage">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="ProductWarningmessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Warningmessage"><i class="fa fa-alert"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


        


	<div class="container">
		<span id="spanErronEnter" class="necessary-field"></span>
<form id="frmProductSearch" method="post" 
action="{{action('PurchaseController@saveOrderDetails')}}">
	<br>
	<div class="row">
	 {{csrf_field()}}
	<div class="col-md-3">
      <label class="label">Search By Product Code:</label>
    </div>
    <div class="col-md-3">
      
    <input type='text' id='prod_search' name='prod_search' class="form-control" placeholder='Enter Product code'>
		<span id="spanErronEnter"></span>
    </div>
    <div class="col-md-3">
<!--	<input type='button' class="hide btn btn-primary" value='Search' id='btn_search'> -->
	</div>
	<div class="col-md-3">



  <a class="btn btn-success btn-sm ml-3" href="">
  <i class="fa fa-shopping-cart"></i> Temporary Cart


  <span class="badge badge-light" id="cart-box">
 
  {{$total_cart}}

  </span>


  </a>
  </div>
    
</div>

 
  


 




	<div class="clearfix"></div>
	<br>

	<div class="col">
 

	<div id="showProducttableResult" class="card">	



	</div>


  



</form>

</body>
</html>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
  

       
     

     

     	
$('#prod_search').keypress(function(event){
if (event.keyCode == 13)
{
  event.preventDefault();
  fetchRecords();
}
//section for button click on search button
//$('#btn_search').click(function(){
                  
  //                fetchRecords();
     
    //              });
});
     
function fetchRecords(){
     
       $.ajax({
         url: 'show-product/',
         type: 'get',
         data: {'product_code':$('#prod_search').val()},
       	 dataType: 'json',
         contentType: 'application/json',
         success: function(response){
				if(response.success)
		            {
		            	$('#showProducttableResult').empty().append(response.html);
                  $('#cart-box').text(response.total_cart);
                  if(response.warning!=null)
                  {
                    alert(response.warning);
                  }
                  

                  

		            }
		            else
			            {
                      //$('#showProducttableResult').empty().append();
                      alert(response.msg);

			            }
 

         }
       });
     }







       

</script>