@extends('layouts.store_front_no_heading')
@section('content') 

 <div class="breadcumb_area bg-img" style="background-image: url('{{ URL::to("/public/store/image/breadcrumb.jpg" ) }}') ;"  >
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Privacy Policy</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> 
   
<section class="single_product_details_area d-flex align-items-center box-lg" >
<div class="container">
<div class="row">
  
<div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1"> 
    
          <ul>
              <p class="text-justify">
       Please ensure that this Privacy Policy is perused by You before availing any services from Us. This Privacy Policy shall be updated from time to time and to say abreast with our methods of using Your Information and protecting You privacy, please keep reviewing this policy.
       bookTou is developed by Ezanvel Solutions OPC Pvt. Ltd.
       </p>
         
         <p class="text-justify">
       Ezanvel Solutions OPC Pvt. Ltd., is a private limited company with its registered office at Wangkhei Koijam Leikai, Imphal East 795005, Manipur, India.Unit No. 201, (“Company”, "We", "Us", "Our" and terms of similar meaning) is committed to protecting your privacy. For the purposes of this Privacy Policy, the Users, Delivery Partners, Merchants/Business Partner/ (as defined in the Terms of Use) shall together hereinafter be referred to as “You”, “Your” or “Yourself”.
       </p>
       
       <p class="text-justify">
       The Company has adopted this Privacy Policy to set out the manner in which personal data and other information is collected, received, stored, processed, disclosed, transferred, dealt with and handled by the Company in relation to Your use of our services through the Application bookTou, (collectively referred to as the “Platform”) or initiating or undertaking any transaction on the Platform. This Privacy Policy does not apply to information that You provide to, or that is collected by, any third-party through the Platform, and any third-party site that You access or use in connection with the Services offered on the Platform.
     </p>
       
       <p class="text-justify">
       Please read the Privacy Policy carefully before using or registering on the Platform or accessing any material, information or availing any services (“Services”) through the Platform. By clicking on the “I accept” button on the landing page, You accept this Privacy Policy and agree to be legally bound by the same.
     </p>


       <p class="text-justify">
       This Privacy Policy is incorporated into and subject to Our Terms of Use (“Terms”) and shall be read harmoniously and in conjunction with the Terms. In the event of any conflict between the Terms and this Privacy Policy, the provisions of the Terms shall supersede the Privacy Policy. All capitalised terms not defined under this Privacy Policy shall have the meaning ascribed to it under the applicable Terms.
     </p>

       <p class="text-justify">
       Please feel free to direct any questions or concerns regarding this Privacy Policy by contacting Us through this Platform or by writing to Us at booktou@gmail.com
</p>

      
          

        </ul>
    
<ul>
         <h4>1. Our Statement of Privacy Practice</h4>
   
         <li class="text-justify">
           Your information is collected and processed in accordance with the terms and conditions of this Privacy Policy.
         </li>

</ul>
  
<ul>
         <h4>2. Collection of Information</h4>

<p>        
      <li class="text-justify">
        2.1 In order to enhance the user experience and provide Our Services, Company collects information about You from two sources: (i) information You affirmatively and voluntarily give to us; and (ii) information automatically collected when You visit Our Platform or use any of Our Services.
      </li>
    </p>

<p>
<li class="text-justify">
2.2 Information You give Us: In order to provide Our full range of Services, We may collect the following types of information from You (as applicable) including but not limited to: Your name, gender, email address, mobile and/or telephone numbers, service address and other information about the service address that You give us, information provided by You while initiating and running a task, including, without limitation: search words, location; products or services being sought; and all chat history, etc. We may also collect certain medical information in the form of medical prescriptions or other similar such documents from You, depending on the nature of product or service sought through the Platform, to initiate a transaction on the Platform, and share such information on a need to know basis and upon request with the Merchants selling/providing such product or services to You. We may ask You to provide additional information about Yourself on an optional basis. We may ask You for certain financial information, including credit card account data or other payment method data, to process payments for some of Our Services. We currently do not store any financial information provided to Us. We may also ask You to provide any other personal information which You give us in connection while booking a service or is relevant to customer surveys and/or offers available on the Platform. All such information shall be collectively referred to as “Personal Information”.
</li>
</p>

<p>
<li class="text-justify">
2.3 Non-Personal Information: When You visit the Platform, We may collect certain non-personal information such as Your internet protocol address, operating system, browser type, internet service provider, aggregate user data, browser type, software and hardware attributes, pages You request, and cookie information, etc. This type of information does not identify You personally.
</li>
</p>

<p>
<li class="text-justify">
2.4 Automatic Information: We receive and store certain types of information whenever You access Our Platform. We use "cookies" (small file containing a string of characters that uniquely identifies Your browser) and We obtain certain types of information when Your web browser accesses the Platform. These server logs may include information such as Your web request, Internet Protocol address, browser type, browser language, the date and time of Your request, information about Your internet connection, and web beacon information. We use cookies primarily for user authentication but may also use them to improve the quality of Our Services by storing user preferences and tracking user trends. Further, the Company may also collect information about Your preference and settlings such as time zone and language, Your searches and the results You selected from the searches.
</li>
</p>

<p>
<li class="text-justify">
2.5 Mobile: When and if You download and/or use Our Platform through Your mobile, We may receive information about Your location and Your mobile device, including a unique identifier number for Your device. We may use this information to provide You with location-based Services including but not limited to search results and other personalized content. When You use the Platform through the telecommunication device, we collect Your location data. If You permit the Platform to access Your location through the permission system used by Your mobile operating system, We may also collect the precise location of Your device when the App is running in the foreground or background. We may also derive Your approximate location from Your IP address.
</li>
</p>

<p>
<li class="text-justify">
2.6 User communications: When You send emails or other communications to Us, We may retain those communications in order to process Your inquiries, respond to Your requests and improve Our Services.
</li>
</p>

<p>
<li class="text-justify">
2.7 Other Parties: We may receive information about You from third parties, such as other users, partners or our affiliated companies or if You use any of the other websites/apps We operate or the other services we provide. If You are a partner restaurant, delivery partner, We may, additionally receive feedback and rating from other users.
</li>
</p>

<p>
<li class="text-justify">
2.8 On receiving personal information about our Users, You no longer remain anonymous to Us. We may use this information to do internal research on our Your demographics, interests, and behavior to better understand, protect and serve You better. This information is compiled and analyzed on an aggregated basis and in a manner that does not personally identify You. We indicate fields that are mandatorily required to be filled and fields that are optional. You may decide whether or not to provide such information to Us.
2.9 You may choose not to provide us with any Personal Information or information as required to complete your tasks/transactions initiated on Your Platform. If We do not receive information required for Your tasks, a Delivery Partner or Merchant may choose not to complete Your task/transaction initiated on the Platform
</li>
</p> 

</ul>
 
<ul>
         <h4>3. Additional Information</h4>
      <p class="text-justify">
       When using our services, We may collect information about You, with Your permission, in the following general categories:
     </p>
  
<p>   
<li class="text-justify">   
3.1 Address Book: For certain features, We will need to access Your address book on Your device. If You permit Us to access Your address book, We may access and store names and contact information from Your address book to facilitate certain service features on the App.
</li>
</p>

<p>
<li class="text-justify">
3.2 Transaction Information: We collect transaction details related to Your use of Our Platform, including the type of service requested, date and time the service was provided, amount charged, and other related transaction details. Further, We may also collect personal information of third parties who are not registered on the Platform, including names, telephone numbers and addresses, for the limited purpose of completing the transaction initiated by You on the Platform.
</li>
</p>

<p>
<li class="text-justify">
3.3 Device Information: We may collect information about Your mobile device, including, for example, the hardware model, operating system and version, software and file names, preferred language, unique device identifier, advertising identifiers, serial number, device motion information, and mobile network information.
</li>
</p>
</ul>
   
<ul>
         <h4>4. Use of Personal Information</h4>
<p>
<li class="text-justify">      
       4.1 The information collected by Us through our Platform maybe used inter alia for the following purposes but not limited to:


<ul>
  </br>
<li>
(a) To allow You to use Services on the Platform;
</li>
<li>
(b) To allow You to undertake or initiate any transactions on the Platform
</li>
<li>
(c) For internal record keeping of the Company;
</li>
<li>
(d) To improve our Services;
</li>
<li>
(e) To improve Your usage of the Platform, to manage Your Account, etc
</li>
<li>
(f) To process payments with respect to transactions initiated on the Platform;
</li>
<li>
(g) to respond to Your comments, reviews and questions and provide better Service;
</li>
<li>
(h) to communicate important notices or changes to the Services provided by Company on the Platform, use of the Platform and the terms/policies which govern the relationship between You and the Company;
</li>
<li>
(i) or internal purposes of the Company such as auditing, data analysis and research conducted either indirectly/directly by Company
</li>
<li>
(j) or promotion and marketing purposes by Company;
</li>
<li>
(k) for any other purposes with Your consent
</li>
</ul>
</li>
</p>

<p>
<li class="text-justify">
4.2 From time to time, we may use Your information to customize, develop, improve the Platform according advancement of technology.
</li>
</p>

<p>
<li class="text-justify">
4.3 We may use personal information to resolve disputes that may arise with the use of Our services on the Platform, help promote a safe service to You on the Platform, measure consumer interest in Our services, customize Your experience, detect and protect Us against error, fraud and other illegal activity, enforce Our terms and conditions.
</li>
</p>

<p>
<li class="text-justify">
4.4 We identify and use your IP address to help diagnose problems with our server, and to administer our Platform. Your IP address is also used to help identify You and to gather broad demographic information
</li>
</p>

<p>
<li class="text-justify">
4.5 We shall be entitled to retain Your Personal Information for such duration as may be required for the purposes specified hereunder and shall be used Us only in consonance with this Privacy Policy
</li>
</p>

</ul>

<ul>
         <h4>5. Sharing of Personal Information</h4>
<p>
 <li class="text-justify">     
      5.1 We share Your information with third parties and third party service providers including Merchants or insurance service providers upon receipt of request by You to initiate a transaction to carry out specific service requests and fulfill such service requests.
</li>
</p>

<p>
<li class="text-justify">
5.2 We may also share your information with payment service providers for the purpose of fulfilling Services to you.
</li>
</p>

<p>
<li class="text-justify">
5.3 We may disclose Your personal information if required to do so by law or in the good faith belief that such disclosure is reasonably necessary to respond to subpoenas, court orders, or other legal process. We may disclose personal information to law enforcement offices, third party rights owners, or others in the good faith belief that such disclosure is reasonably necessary to enforce our Terms or Privacy Policy; respond to claims that an advertisement, posting or other content violates the rights of a third party; or protect the rights, property or personal safety of our Users or the general public.
</li>
</p>

<p>
<li class="text-justify">
5.4 Company may need to disclose Your information, to (i) protect and defend the rights or property of the Company, including to enforce Our agreements, policies, and Terms and; (ii) protect the personal safety of You, the Company, its members and employees, or any person, in an emergency; and (iv) protect Company from incurring any legal liability. In such an event Company shall be under no obligation to specifically inform You or seek Your approval or consent.
</li>
</p>

<p>
<li class="text-justify">
5.5 Any content or personal information that You share or upload on any current or future publicly viewable portion of the Platform (on discussion boards, in messages and chat areas, etc.) will be publicly available, and can be viewed by others.
</li>
</p>

<p>
<li class="text-justify">
5.6 If You provide a mobile phone number and/or e-mail address, the Company, or other parties registered on the Platform may call You or send You communications in relation to Your use of the Platform or Your any transaction initiated by You on the Platform. We and our affiliates will share/transfer some or all of the collected information (personal or otherwise) with another business entity should We (or our assets) plan to merge with, or be acquired by that business entity, or re-organization, amalgamation, restructuring of business, in connection with or during negotiation of any merger, financing, acquisition, bankruptcy, dissolution, transaction or proceeding. Should such a transaction occur, that other business entity (or the new combined entity) will be contractually bound to comply with the terms of this Privacy Policy.
</li>
</p>

<p>
<li class="text-justify">
5.7 We do not disclose personal information about identifiable individuals to advertisers, but We may provide them with aggregate and/or anonymised information about You and other registered Users, Delivery Partner, Merchants to help advertisers reach the kind of audience they want to target. We may make use of the information We have collected from You to enable Us to comply with our advertisers' wishes by displaying their advertisement to that target audience.
</li>
</p>

</ul>
   
<ul>
         <h4>6. Transparency and Choice</h4>
<p>
<li class="text-justify">
      6.1 Your acknowledgement. All information disclosed by You shall be deemed to be disclosed willingly and without any coercion. No liability pertaining to the authenticity/genuineness/ misrepresentation/ fraud/ negligence, etc. of the information disclosed shall lie on the Company nor will the Company in any way be responsible to verify any information obtained from You.
</li>
</p>

<p>
<li class="text-justify">
6.2 Withdraw consent. You may choose to withdraw Your consent provided hereunder at any point in time. Such withdrawal of the consent must be sent in writing to booktou@gmail.com In case You do not provide or later withdraw Your consent, We reserve the option to not allow You to undertake any transaction on the Platform or access the Service available on the Platform for which the said information was sought on the Platform. However, if You are a part of any on-going transaction on the Platform, We reserve the right to retain all your information until completion of the transaction. Further, You acknowledge and agree that in case of such withdrawal of Your consent, the Company reserves the right to store Your information in an anonymise form such that the information stored will not be attributable to you or identify You in any manner whatsoever. The Company shall complete the anonymizing the data from the date of withdrawal of consent.
</li>
</p>

<p>
<li class="text-justify">
6.3 Rectification: You represent and warrant that any and all information, including but not limited to Your Personal Information is absolutely correct and complete in all aspects. You further undertake to immediately update any change or variation of Your Personal Information on the Platform by sending Us a request in writing to booktou@gmail.com to rectify or update Your Account. Once You send us an email to Us, You agree to comply with the instructions as may be provided/communicated by Us.
</li>
</p>
</ul>
    
<ul>
         <h4>7. Information Safety</h4>
<p class="text-justify">
        All information is saved and stored on servers, which are secured with passwords and pins to ensure no unauthorized person has access to it. Once your information is in Our possession We adhere to strict security guidelines, protecting it against unauthorized access. Company shall take reasonable steps to help protect Your rights of privacy and Your information (personal or otherwise) in an effort to prevent loss, misuse, unauthorized access, disclosure, alteration, or destruction of such information, in compliance with the applicable laws. You expressly consent to the sharing of Your information with third party service providers, including Merchants, insurance agents and insurers, financial service providers, payment gateways, to process payments and manage payment card information. Company does not itself store Your payment card account information, and does not have direct control over or responsibility for Your payment card account information. Hence, Company cannot guarantee that transmissions of Your payment card account information or personal information will always be secure or that unauthorized third parties will never be able to defeat the security measures taken by Company or Company’s third-party service providers. Company assumes no liability or responsibility for disclosure of Your information due to any reason, including but not limited to errors in transmission, unauthorized third-party access, or other causes beyond its control. Although we shall try our best to protect Your data we cannot take any guarantee for the security of Your data transmitted through the Platform. You play an important role in keeping Your personal information secure. You shall not share Your user name, password, or other security information for Your account with anyone
</p>
</ul>
    
<ul>
         <h4>8. Public Posts</h4>
<p class="text-justify">
      If the Platform permits You, You may provide Your feedback, reviews, comments, etc. on the Platform on Your use of the Platform and/or in relation to any transactions on the Platform on any portion of the Platform that is publicly viewable (“Posts”). In such an event, (i) Your Posts shall have to comply with the conditions relating to Posts as mentioned in the applicable Terms of Use and applicable law; and (ii) the Company shall have an unconditional right to remove and delete any Post or such part of the Post that, in the sole opinion of the Company, does not comply with the conditions in the applicable Terms of Use or applicable law. All Posts shall be publicly accessible and visible to all Users of the Platform. Company reserves the right to use, reproduce and share such Posts for any purpose at its discretion. If You delete Your Posts from the Platform, copies of such Posts may remain viewable in archived pages, or such Posts may have been copied or stored by other Users
    </p>
</ul>
    
<ul>
         <h4>9. Choice/Opt-Out</h4>
<p class="text-justify">
      We provide all Users with the opportunity to opt-out of receiving non-essential (promotional, marketing-related) communications from Us on behalf of our partners, and from Us in general, after providing Us with personal information. If You want to remove your contact information from all lists and newsletters, please contact booktou@gmail.com. If You opt out, the Company may still send You non-promotional emails/communications, such as emails/communications with respect to the Platform and Your account and related transactions.
    </p>
</ul>
    
<ul>
         <h4>10. Third Party Link</h4>
<p>
<li class="text-justify">
       10.1 You hereby acknowledge and agree that when You use the Platform, there may be certain links which may direct You to other websites or applications not operated/maintained by the Company or third-party advertising companies to serve advertisements when You visit our Platform (“Other Sites”). These companies or third-party platform may use Your information (not including Your name, address, email address, or telephone number) about Your visits to this and other Platform in order to provide advertisements about services of interest to You. This Privacy Policy does not apply to information that You provide to, or that is collected by, any Other Site through the Platform, and any Other Site that You access or use in connection with the use of the Platform. The manner in which Your information is collected, received, stored, processed, disclosed, transferred, dealt with and handled by such Other Site(s) is governed by the terms and conditions and privacy policy of the respective Other Site(s). The Company urges You to acquaint yourself with the terms and conditions and privacy policy of every such Other Site(s)
</li>
</p>

<p>
<li class="text-justify">
10.2 The Company hereby expressly disclaims all liabilities with respect to the manner in which the Other Site(s) collects and/or uses Your information. On the Platform, there may be third parties who advertise their products and/or services. Such third parties may place or recognize a unique "cookie" on Your browser and may use information about Your visits to Our Platform and other websites in order to provide advertisements about goods and services of interest to You. We shall, in no event, be liable for any unauthorized or unlawful disclosures of Your personal information or any other information made by advertisers, who are not subject to Our control
</li>
</p>

</ul>
    
<ul>
         <h4>11. Complaints</h4>
<p class="text-justify">
        In case of any dissatisfaction in relation to the Platform, You shall first file a formal complaint with the customer service of the Company, prior to pursuing any other recourse. The complaints can be lodged at bootkout@gmail.com and upon lodging a complaint You agree to provide complete support to the customer service team with such reasonable information as may be sought by them from You. The decision of the Company on the complaints shall be final and You agree to be bound by the same.
</p>
</ul>
   
<ul>
         <h4>12. Changes to our Privacy Policy</h4>
<p class="text-justify">
       We reserve the unconditional right to change, modify, add, or remove portions of this Privacy Policy at any time, without specifically notifying You of such changes. Any changes or updates will be effective immediately. You should review this Privacy Policy regularly for changes. You can determine if changes have been made by checking the “Last Updated” legend above. If we make any significant changes we will endeavor to provide you with reasonable notice of such changes, such as via prominent notice on the Platform or to your email address on record and where required by applicable law, We will obtain your consent. Your acceptance of the amended Privacy Policy / Your continued use of our services after we publish or send a notice about our changes to this Policy shall signify Your consent to such changes and agreement to be legally bound by the same
</p>
</ul>
    
<ul>
         <h4>13. Grievances</h4>
<p class="text-justify">
       In the event You have any grievances relating to the Privacy Policy, please inform within 24 hours of occurrence of the instance from which the grievance has arisen, by writing an email to the Grievance Officer at bootktou@gmail.com.
</p>
</ul>
  </div>

 
</div>
</div>
</section> 

 
 
@endsection 
 