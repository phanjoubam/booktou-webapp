@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<div class='outer-top-tbm'> 
<div class="container">
<div class="row">

<div class="col-sm-10 col-md-10 col-lg-10 offset-md-10 offset-lg-1 offset-md-1 offset-lg-1">
<div class="card"> 
    <div class="card-body">
        <h1 class='text-center'>Merchant Agreement</h1>
        <hr/>
<p><strong>1. ROLES & RESPONSIBILITIES OF bookTou</strong></p>
<p>
1.1 bookTou will promote the Store’s online and offline as per the listing done by the store from time to time.<br/>
1.2. Once the order from the customer has been confirmed, bookTou will initiate the delivery process by informing the Store through phone calls and our Delivery Executives will pick up the order from Store within the proposed time boundary, deliver to customer and will collect the item price, plus tax and delivery charge wherever the charges apply.<br/>
1.3. bookTou shall be allowed to inspect the packaging and make reasonable suggestions on improvements accordingly and refuse to carry any package which it doesn’t find transit worthy.<br/>
1.4. bookTou may add a separate receipt of delivery charge to every order. <br/>
1.5. bookTou will remit money to the Store on a set number of days (to be agreed between bookTou and the Store), the remit amount to the Store will be the Total Order Value of the agreed days after deducting the agreed commissions.. 
1.6. bookTou represents and warrants to abide by all applicable laws, rules and regulations as may be prescribed by the government and authorities in relation to the Delivery Services.<br/>
1.7. For all orders coming through bookTou (for restaurants):<br/>
i. In case of return, bookTou shall bear the cost of item/s.
ii. In case of spill or damage of item/s, bookTou shall bear the cost of item/s.
iii. Once orders have been picked up by our agents after inspection - it would be solely company responsibilities to reach out to the customers, the company would bear the cost in case of any eventuality.     
</p> 
<p><strong>2. PnD(Pick and Deliver)</strong></p> 
<p>
2.1 Once order has been placed by the store, it can be cancelled if agents have not been assigned.<br/>
2.2 For all PnD orders - if agents have been assigned, 50% of the delivery cost has to be borne by the store owner.<br/>
2.3. If the item/s while on the way to delivery gets spilled or damaged - bookTou shall bear the cost of the item/s.<br/>
2.4 If a customer refuses to take the order, the store owner shall take the responsibilities of that order item/s and shall bear the cost of the delivery.<br/>
2.5 In case the agent reaches the customer end with unacceptable delay (due to unavoidable circumstances from bookTou) and customer refuses to receive the order - bookTou shall bear the cost of item/s and the delivery cost.<br/>
2.6 In case customer does not respond to our delivery agent upon reaching the customer location - we allow 10 minutes wait of delivery agent so as the store owner reach out to customer and confirm, beyond that period we shall recall our agents to base location and the order would be returned to the store, delivery cost shall be beared by the store owner in case such situation arises.
 2.7 In case of eventualities like bandh, area lockdown of the customer location, item/s would be returned back to the store.
</p> 
<p><strong>3. ROLES & RESPONSIBILITIES</strong></p> 
<p>
3.1. Store has to take orders from bookTou Service Operator through phone calls.<br/>
3.2. Store, to the best of their ability, will provide undamaged, well packaged and labelled items/products in a timely fashion to the bookTou delivery Executives.<br/>
 
3.3. Items prices which are subject to change with prior notice to bookTou only. If not, any variance in the orders amount shall be bear by the Store.<br/>
3.4. The Store shall not handover or allow to be handed over any Product which is expired, poisonous, banned, restricted, illegal, prohibited, stolen, infringing any third-party rights, hazardous or dangerous or in breach of any tax laws. bookTou shall not be liable for the Delivery of any such Products<br/>
3.5. The Store shall defend, hold harmless, indemnify and keep indemnified and harmless bookTou Delivery Personnel, Service Provider’s directors, employees, contractors and agents against all suits, investigations, enforcements, actions, fines, penalties, fees, interests, losses, damages and costs (including reasonable attorney fees) incurred by bookTou, due to Merchant’s  breach / alleged breach of Clause herein above.<br/>
</p> 
<p><strong>4. DELIVERY TIME</strong></p>
<p> 
4.1. Any delay due to unavoidable circumstances will be communicated to the customers.<br/>
4.2. The Store shall give the ordered items to the bookTou delivery executives time after receiving the ordered. In case the Store takes more time to hand over the items to bookTou delivery executive, any inconvenience caused to the customer shall be bear by the Store, including if the customer chooses to return the item/s. 
</p> 
<p><strong>5. RATE OF SERVICE</strong></p> 
<p>
An agreed % commission of the order value will be charged from the Store for the time-frame agreed upon which would be received from time to time with all stakeholder common understanding.
</p> 
<p><strong>6. OPENING HOURS</strong></p> 
<p>
For every working days bookTou office is open from<br/>
- 11.00am to 7.00pm for summer season<br/>
- 11.00am to 5.00pm for winter season<br/>
Ordering times for the Store will be settled between bookTou and The Store owner and this may vary over time.
</p> 
<p><strong>7. CONFIDENTIALITY OBLIGATION</strong></p> 
<p>
Both Parties shall keep confidential (and to ensure that its employees, agents and affiliates keep confidential) any Confidential Information, including Commission Rate and Delivery Charge. Both Parties shall not, and shall procure that they shall use Confidential Information for any purpose other than for the provision of Delivery Services and for performance under this Agreement.
</p> 
<p><strong>8. DISPUTE RESOLUTION</strong></p> 
<p>
All disputes arising out of or in relation to this Agreement, including any question regarding its existence, validity or termination, which cannot be amicably resolved by the Parties within 15 days of being brought to their attention, shall be settled by arbitration governed by the provisions of Arbitration and Conciliation Act, 1996. A dispute shall be deemed to have arisen when either Party notifies the other Party in writing to that effect. 
</p> 

<hr/>
<div id='taxdetails'>
 <form action="{{ action('HomeController@saveMerchantAgreementForm') }}" method="post">
    {{ csrf_field() }} 

    @if (session('err_msg'))
        <div class="row">
            <div class="col-md-12"> 
                <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
            </div> 
        </div> 
    @endif


    <div class="form-row">
    <div class="form-group col-md-3">
      <label for="priphone">Registered Phone No.</label>
      <input type="text" class="form-control" id="priphone" name='priphone'>
    </div>
<div class="form-group col-md-3">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name='password'>
    </div>
</div>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="business">Business Name</label>
      <input type="text" class="form-control" required  id="business" name='business'>
    </div>
    <div class="form-group col-md-6">
      <label for="name">Owner Name</label>
      <input type="text" class="form-control"  required id="name" name='name'>
    </div>
  </div>
 
  <div class="form-group">
    <label for="address">Address</label>
    <textarea type="text" class="form-control" required id="address" name='address' placeholder="Business address"></textarea>
  </div>
  
 <div class="form-row">

    <div class="form-group col-md-3">
    <label for="regdno">License/Registration No. (if any)</label>
    <input type="text" class="form-control" id="regdno" placeholder="Registration or license no if any" name='regdno'>
  </div>

<div class="form-group col-md-3">
      <label for="pan">PAN (Compulsory)</label>
      <input required type="text"  required class="form-control" id="pan" name='pan'>
    </div>
     <div class="form-group col-md-3">
      <label for="gstin">GSTIN</label>
      <input type="text" class="form-control" id="gstin" name='gstin'>
    </div>

    <div class="form-group col-md-3">
      <label for="fssai">FSSAI (for business in food sector)</label>
      <input type="text" class="form-control" id="fssai" name='fssai'>
    </div>
 
  </div>
   <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="agree" name="agree">
      <label class="form-check-label" for="agree">
        I agree the above terms and conditions.
      </label>
    </div>
  </div>

  <button type="submit" name='submit' value='submit' class="btn btn-primary">Submit</button>
</form>
</div>

</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>

@endsection 