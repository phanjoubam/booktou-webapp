@extends('layouts.store_front_01')
@section('content')
<?php 
    $cdn_url =  config('app.app_cdn') ;  
    $cdn_path =   config('app.app_cdn_path');  
?>
<section class="single_product_details_area d-flex align-items-center box-lg"  >
<div class="container">
<div class="row">
  <div class='col-md-12 text-center' style="padding: 120px;">
      <img src="{{  URL::to('/public/assets/image/404.jpg') }}" ='Resource you are looking for is not found!' />
  </div> 
</div>
</div>
</section>

    <section class='app-download-invert' style="background-color: #dce9fd;">
<div class='container'>
<div class="row h-100 ">

    <div class='col-md-6'>
    <img src="{{ URL::to('public/store/image/app.png') }}" alt="bookTou App"> 
    </div>
    <div class='col-md-6'>
    <div class='download-section'> 
    <div class="hero-content mt-4"> 
                        <h2>Smarter Personal Assistant</h2>
                        <p>
    Order from stores or restaurants,<br/>
    track with ease and chil!
    <br/>
    <a href='https://bit.ly/2Ln8C1Y' target='_blank'>
    <img src="{{ URL::to('public/store/image/google-play.png') }}" width='200px' alt="Download from Google Play"></a>
    </p> 
    </div>
                    </div>
                    </div>
 
    
    </div>

    </div> 

</section>
@endsection 