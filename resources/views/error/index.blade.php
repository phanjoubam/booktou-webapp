@extends('layout.dashboard')
@section('content') 
<article class="content forms-page">

 <div class="card card-block "> 

                    <div class="title-block">
                        <h3 class="title">Ooops Error occurred!</h3>
                        <p class="title-description">Error sumary is listed below</p>
                    </div> 
                      
<section class="section">
     <div class="row ">
	 @if (session('err_msg'))
	<div class="col-12"> <div class="alert alert-info">
		{{ session('err_msg') }}
	</div>
	</div>
	@endif 
 </div>
   </section>
   </article> 	
@endsection
