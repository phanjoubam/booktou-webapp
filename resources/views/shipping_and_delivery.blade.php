@extends('layouts.store_front_no_heading')
@section('content')
<style type="text/css">
p.x-biggest{
    font-family: sans-serif;
    font-size: 1.2em;
    text-align: justify;
    line-height: 1.8;
}
li.x-biggest{
    font-family: sans-serif;
    font-size: 1.1em;
    text-align: justify;
    line-height: 1.8;
}
h2{
    text-decoration-line: underline;
    font-size: 2.5em;
}
h3{
    font-size: 2em;
  }
}

</style> 

 <div class="breadcumb_area bg-img" style="background-image: url('{{ URL::to("/public/store/image/breadcrumb.jpg" ) }}') ;"  >
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Shipping And Delivery Policies</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> 
   
    <section class="single_product_details_area d-flex align-items-center box-lg" >
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1">
                <h3 class="mt-5">1.FNB</h3>

           <p class="x-biggest mt-5">
            As bookTou is only a service provider, we will try our best to provide smooth and efficient delivery service. Following steps will be our service conditions.
            <p>

            <ul>
                <li class="x-biggest">
                    1. RESTAURANT has to take orders from bookTou Service Operator through App and phone calls.
                </li>

                <li class="x-biggest">
                    2. RESTAURANT, to the best of their ability, will provide warm and undamaged, well packaged and labelled foods in a timely fashion to the bookTou delivery Executives. 
                </li>

                <li class="x-biggest">
                    3. In case the customer avoids or cancels their orders of food value less than Rs.500 (for COD case), Restaurant has to collect back the ordered food packet without any defect. 
                </li>
                <li class="x-biggest">
                    4. Menu items and prices are subject to change with prior notice to bookTou only. If not, any variance in the order amount shall be borne by the Restaurant.
                </li>
                <li class="x-biggest">
                    5. Non-compete during the term of this agreement, RESTAURANT agrees not to engage another delivery service of this kind or to start their own deliveries.
                </li>
                <li class="x-biggest">
                    6. The Restaurant shall not handover or allow to be handed over any Product which is stale, poisonous, expired raw materials and packaged items, banned, restricted, illegal, prohibited, stolen, infringing of any third-party rights, hazardous or dangerous or in breach of any tax laws. bookTou shall not be liable for the Delivery of any such Products.
                </li>

                <li class="x-biggest">
                    7. The Restaurant shall defend, hold harmless, indemnify and keep indemnified and harmless bookTou Delivery Personnel, Service Provider’s directors, employees, contractors and agents against all suits, investigations, enforcements, actions, fines, penalties, fees, interests, losses, damages and costs (including reasonable attorney fees) incurred by bookTou, due to Restaurant’s breach / alleged breach of Clause 1 to 6 herein above.
                </li>
                <li class="x-biggest">
                    8. The delivery time to the customer will be taken as 45 mins from the time an item is picked up from the vendor. Any delay due to unavoidable circumstances will be communicated to the customers.
                </li>
                <li class="x-biggest">9. The restaurant shall give the ordered food items to the bookTou delivery executives on or before 20 minutes starting from the time when the customer ordered. In case the RESTAURANT takes more than 20 minutes to hand over the items to bookTou delivery executive, any inconvenience caused to the customer shall be bear by the restaurant, including if the customer chooses to return the item. 

                </li>
                <li class="x-biggest">
                    10. bookTou shall deliver the food items to the customer on or before 25 minutes after handing the food items by the restaurant. In case the bookTou takes more than 25 minutes to hand over the items to Customer, any inconvenience caused to the customer shall be borne by the bookTou, including if the customer chooses to return the item.
                </li>
            </ul>  


            <br/>
        <h3>Shipping And Delivery</h3>

                  <br/>
                  <h3>2. Contact us</h3>
                  <p class="x-biggest">If you have any inquiries or requests in respect of your Personal Data or our Privacy and Data Protection Policy, you may contact us at:</p>
                  <p class="x-biggest">Ezanvel Office<br/>
                  Ezanvel Solutions private Ltd.</br>
              Minuthong-Khuyathong Road,</br>
          Opposite Kekrupat</br>
      Imphal West</br>
      795001<br/>
      Email: <a href='mailto:booktougi@gmail.com' target='_blank'>booktougi@gmail.com</a><br/>
      Phone: +91-986-308-6093<br/>
  </p>

</ul>

</div>


</div>
</div>
</section> 

 
 
@endsection 
 