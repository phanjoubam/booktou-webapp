<!DOCTYPE html>
<html> 
	 <head>
		@include('store_front.template-03.head')

	  	
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-7TE5XV8H82"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-7TE5XV8H82');
		</script>
	 </head>
	 <body>
	 	
	 	@include('store_front.template-03.nav_bar')
	 	
		@yield('content') 
		
	 	@include('store_front.template-03.footer')  

	 	@include('store_front.template-03.footer-scripts') 
	 	@yield('script')  
 
 
 </body> 
</html>