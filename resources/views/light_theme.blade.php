<!doctype html>
<html> 
	 <head>
	 	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

		@include('templates.head') 
	 </head>


	 <body id="page-top">
	 
	 <div id="wrapper">
	 @include('templates.sidebar') 
	 
	 
	 
	 <div id="content-wrapper" class="d-flex flex-column">
		
		
			@include('templates.headerc') 

			
			<div id="content" class="body-height">
			<div class="container-fluid">
				 
				@yield('pagebody')
			</div>

		
		 
		
		</div>
		
		
		
		
	  </div>
	 </div>
	@include('templates.footer') 
	  
   @include('templates.script') 

 </body>  
</html> 
