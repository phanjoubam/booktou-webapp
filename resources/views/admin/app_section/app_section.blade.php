@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
    <div class="col-md-8 offset-2"> 
	      <div class="card">
            <div class="card-header" style="text-align: center;">ADD SECTION</div>
               
             <div class="card-body">
                   
              <form  method="post" action="{{action('Admin\AdminDashboardController@saveAppSection')}}"
              class="row g-3" enctype="multipart/form-data">


                {{csrf_field()}}
                <input  type="hidden"  name="alsid" value="{{ request()->alsid }}"/>

                  <div class="col-md-6">
                        <label class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" id="title">
                  </div> 

                    <div class="col-md-6">
                      <label for="inputState" class="form-label">Show Title</label>
                      <select id="inputState" class="form-control" name="showtitle"> 
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
    
                    <div class="col-md-3">
                    <label for="Rows" class="form-label">Rows</label>
                    <input type="Rows" class="form-control" name="rows" id="Rows">
                  </div>
                  <div class="col-md-3">
                    <label for="Columns" class="form-label">Columns</label>
                    <input type="Columns" class="form-control" name="cols" id="Columns">
                  </div>

                   <div class="col-md-6">
                        <label  class="form-label">Background-Image</label>
                       <input class="form-control" type="file" id="photo" name="photo">

                    </div>
                    <div class="col-md-6">
                      <label  class="form-label">Show Background Image?</label>
                      <select id="showbackground" class="form-control" name="showbackground"> 
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
                    <div class="col-md-6" >
                      <label  class="form-label">Background-color</label>
                      <input type="text" class="form-control" id="bgColor" name="bgColor">
                    </div> 
                    <div class="col-md-6">
                      <label class="form-label">UI Layout</label>
                       <select id="caption" class="form-control" name="uitype"> 
                        <option value="list">List</option>
                        <option value="grid">Grid</option>
                        <option value="slider">Slider</option>
                        <option value="fullblock">Block</option>
                        <option value="icons">Icons</option> 
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label class="form-label">Item Shape</label>
                       <input type="text" name="itemshape" class="form-control">
                    </div> 
                    <div class="col-md-2">
                      <label class="form-label">Item Width</label>
                       <input type="text" name="itemwidth" class="form-control">
                    </div>
                    <div class="col-md-2">
                      <label class="form-label">Item Height</label>
                       <input type="text" name="itemheight" class="form-control">
                    </div> 
                                        
                      <div class="col-md-6">
                      <label class="form-label">Published</label>
                       <select id="caption" class="form-control" name="publish"> 
                        <option value="yes">Yes</option>
                        <option value="no">No</option> 
                      </select>
                    </div>

                    <div class="col-md-6">
                      <label class="form-label">Sub Module</label>
                        <input type="text" name="submodule" class="form-control">
                    </div>
                     <div class="col-md-6">
                      
                    </div>

                      
                     <div class="col-md-12 mt-3 text-center">
                       <div class="col">
                       	<button class="btn btn-primary">Save</button>
                       </div>
                    </div>
</form>
            </div>
        </div>
    </div>
</div>
@endsection 
@section("script")



@endsection