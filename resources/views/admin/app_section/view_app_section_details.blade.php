@extends('layouts.admin_theme_02')
@section('content')
 <div class="row">
<div class="col-md-12"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-4">
                  <div class="title">Manage Section Listing</div> 
                  @if (session('err_msg'))
				      <div class="col-md-12"> 
				      <div class="alert alert-info">
				      {{ session('err_msg') }}
				      </div>
				      </div>
				  @endif  
                </div>
 <div class="col-md-8 text-right">
 	<form method="get" action="{{action('Admin\AdminDashboardController@viewAppSectionDetails')}}">
      	{{csrf_field()}}
      <div class="row">
<div class="col-md-6"> 

</div>
<div class="col-md-5">
	<select class="form-control" name="sectionid" id="sectionid">
    @foreach($results  as $items)  
    <option value="{{$items->id}}" 
    <?php 
        if (request()->get('sectionid')==$items->id) {
             echo 'selected';
        }else{

        }
    ?>
    >{{$items->title}}</option> 
    @endforeach
    </select> 
</div>
<div class="col-md-1">
	<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
</div>
     </div> 
 </form>
 </div>
</div>
</div>
</div>










<!-- delete modal form -->
<div class="modal confirmdel" tabindex="-1" role="dialog">
  <div class="modal-dialog" >

     <form action="{{action('Admin\AdminDashboardController@deleteSectionDetails')}}" method="get">
         
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><p>Warning !!! Do You Want To Delete ?</p></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
          
               <div class="modal-body">
                <input type="hidden" name="sid" id="key">
                <button type="submit" class="btn btn-danger" >Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
               </div>
              
              
        </div>

     </form>
   
  </div>
</div>
<!-- delete modal ends here -->

<!-- edit section -->
<div class="modal" id="showedit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
     <div class="modal-header">
      <h4 class="modal-title"><b>Update Section</b></h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
     </div>
     <div class="modal-body">
              
        <form  method="post" action="{{action('Admin\AdminDashboardController@saveAppSectionDetails')}}"
              class="row g-3" enctype="multipart/form-data">
            {{csrf_field()}}
                <input  type="hidden"  name="key" id="sid" />
 
                           
                            <div class="col-md-6">
                                   <label>Select Section Title:</label>
                                  <div class="dropdown show ">
                                   
                                    <select  class="form-control" name="sectionid">
                                      @foreach($results as $items)
                                      <option value="{{$items->id}}">{{$items->title}}</option>
                                      @endforeach 
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6"> 
                                <label>image URL:</label> 
                                <input id="imageurl" type="file" class="form-control" name="photo">
                               </div> 
                       
                        
                                                  
                        <div class="col-md-6">
                        <label>Caption:</label> 
                        <textarea rows="3" name="caption" id="caption" class="form-control" placeholder="caption"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Sub Caption:</label> 
                            <textarea rows="3" name="subcaption" id="subcaption" class="form-control" placeholder="sub caption"></textarea>
                        </div>
                        
                        
                         
                         
                           
                            <div class="col-md-4">
                                <label>Caption Layout:</label>

                               <select class="form-control" name="layout">
                                   <option value="horizontal">Horizontal</option>
                                   <option value="vertical">Vertical</option>
                               </select>
                               
                            </div>

                            <div class="col-md-4">
                                <label>Rating:</label>

                                <select class="form-control" name="rating" id="rating">
                                    <option value="1">1</option>
                                     <option value="2">2</option>
                                      <option value="3">3</option>
                                       <option value="4">4</option>
                                        <option value="5">5</option>
                                </select>
                               
                            </div>
                            
                             <div class="col-md-4">
                                 <label>Type:</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="product">Products</option>
                                    <option value="service">Service</option>
                                    <option value="business">Business</option>
                                </select>
                            </div> 
                        

                        
                            <div class="col-md-12">
                                <label>Target App URL:</label> 
                                <input id="appurl" type="type" class="form-control"  name="appurl" required>
                            </div>
                         
                        
                            <div class="col-md-12"> 
                                <label>Target Web URL:</label> 
                                <input id="weburl" type="text" class="form-control" name="weburl" required>
                            </div> 

                             <div class="col-md-6">
                                  <label>Is sponsored</label>
                                  <select id="sponsored" class="form-control" name="sponsored">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                  </select>
                            </div>
                            <div class="col-md-6"></div>  
                            
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  </div>

                  
        </form>

     </div>
     </div>
   </div>
</div>

<!-- edit section ends here -->



@if(isset($sectiondetails))
<div class="card-body">  
	<div class="table-responsive" >
    <table class="table">
      <thead class=" text-primary">
      <tr>
      <th scope="col">Caption</th> 
      <th scope="col">Action</th> 
      </tr>
      </thead> 
      <tbody>
		 @foreach($sectiondetails as $items)
              <tr>
              	<td>{{$items->caption}}</td>
              	<td>
                    <button class="btn btn-primary btneditconfirm" 
                    data-key="{{$items->id}}"
                    data-sectionid="{{$items->section_id}}"
                    data-caption="{{$items->caption}}"
                    data-subcaption="{{$items->sub_caption}}"
                    data-layout="{{$items->caption_layout}}"
                    data-rating="{{$items->rating}}"
                    data-type="{{$items->type}}"
                    data-appUrl="{{$items->target_app_url}}"
                    data-webUrl="{{$items->target_web_url}}"
                    data-sponsored="{{$items->is_sponsored}}"
                    >Edit
                    </button>
                    <button class="btn btn-danger delconfirm" data-key="{{$items->id}}">Delete</button>
                </td>  
              </tr>
         @endforeach 
      </tbody> 
    </table> 
	</div>
	</div>
	@endif 
</div> 
</div> 
</div>
@endsection 
@section("script")
<script>
$(document).on("click",".btneditconfirm", function() 
{
        $("#sid").val($(this).attr("data-key"));
        $("#sectionid").val($(this).attr("data-sectionid"));
        $("#caption").val($(this).attr("data-caption"));
        $("#subcaption").val($(this).attr("data-subcaption"));
        $("#layout").val($(this).attr("data-layout"));
        $("#rating").val($(this).attr("data-rating")); 
        $("#type").val($(this).attr("data-type"));
        $("#appurl").val($(this).attr("data-appUrl")); 
        $("#weburl").val($(this).attr("data-webUrl")); 
        $("#sponsored").val($(this).attr("data-sponsored")); 
         
        $("#showedit").modal("show");
});

$(document).on('click', '.delconfirm', function() {
  $("#key").val($(this).attr("data-key"));
  $(".confirmdel").modal("show");
});
</script>
@endsection