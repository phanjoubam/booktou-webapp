@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
    <div class="col-md-12"> 
	
        <div class="card">
        <div class="card-header">View Section List</div>
        <div class="card-body">
        <table class="table">
        	<thead>
        		<th>Title</th>
        		<th></th>
        	</thead>
        	<tbody>
        		@foreach($results as $items)
        		<tr>
        			<td>{{$items->title}}</td>
        			<td>
        				<button class="btn btn-primary btneditconfirm" 
        						data-key="{{ $items->id }}"
        						data-title="{{$items->title}}"
        						data-showtitle="{{$items->show_title}}"
        						data-row="{{$items->rows}}"
								data-column="{{$items->cols}}"
								data-bgcolor="{{$items->bgcolor}}"
                                data-bgimage="{{$items->bgimage}}"
                                data-showimg="{{$items->show_bgimage}}"
								data-shape="{{$items->item_shape}}"
								data-width="{{$items->item_width}}"
								data-height="{{$items->item_height}}"
								data-uitype="{{$items->ui_type}}"
								data-publish="{{$items->published}}"
								data-submodule="{{$items->sub_module}}">
							Edit
						</button>
        				<button class="btn btn-danger delconfirm"
        				data-key="{{ $items->id }}"
        				>Delete</button>
        			</td>
        		</tr>
        		@endforeach		
        	</tbody>
		</table>           
    
		</div>
		</div>
		</div>
		</div>






<!-- delete modal form -->
<div class="modal confirmdel" tabindex="-1" role="dialog">
  <div class="modal-dialog" >

     <form action="{{action('Admin\AdminDashboardController@deleteSection')}}" method="get">
         
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><p>Warning !!! Do You Want To Delete ?</p></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
          
               <div class="modal-body">
                <input type="hidden" name="sid" id="key">
                <button type="submit" class="btn btn-danger" >Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
               </div>
              
        </div>

     </form>
   
  </div>
</div>
<!-- delete modal ends here -->

<!-- edit section -->
<div class="modal" id="showedit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
     <div class="modal-header">
      <h4 class="modal-title"><b>Update Section</b></h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
     </div>
     <div class="modal-body" style="text-align: center;">
              
     	<form  method="post" action="{{action('Admin\AdminDashboardController@saveAppSection')}}"
              class="row g-3" enctype="multipart/form-data">


                {{csrf_field()}}
                <input  type="hidden"  name="key" id="sid" />

                  <div class="col-md-6">
                        <label class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" id="title">
                  </div> 

                    <div class="col-md-6">
                      <label for="inputState" class="form-label">Show Title</label>
                      <select id="showtitle" class="form-control" name="showtitle"> 
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
    
                    <div class="col-md-3">
                    <label for="Rows" class="form-label">Rows</label>
                    <input type="Rows" class="form-control" name="rows" id="Rows">
                  </div>
                  <div class="col-md-3">
                    <label for="Columns" class="form-label">Columns</label>
                    <input type="Columns" class="form-control" name="cols" id="Columns">
                  </div>

                   <div class="col-md-6">
                        <label  class="form-label">Background-Image</label>
                       <input class="form-control" type="file" id="photo" name="photo">

                    </div>
                    <div class="col-md-6">
                      <label  class="form-label">Show Background Image?</label>
                      <select id="showbackground" class="form-control" name="showbackground"> 
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
                    <div class="col-md-6" >
                      <label  class="form-label">Background-color</label>
                      <input type="text" class="form-control" id="bgColor" name="bgColor">
                    </div> 
                    <div class="col-md-6">
                      <label class="form-label">UI Layout</label>
                       <select id="uitype" class="form-control" name="uitype"> 
                        <option value="list">List</option>
                        <option value="grid">Grid</option>
                        <option value="slider">Slider</option>
                        <option value="fullblock">Block</option>
                        <option value="icons">Icons</option> 
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label class="form-label">Item Shape</label>
                       <input type="text" name="itemshape" class="form-control" id="itemshape">
                    </div> 
                    <div class="col-md-2">
                      <label class="form-label">Item Width</label>
                       <input type="text" name="itemwidth" class="form-control" id="itemwidth">
                    </div>
                    <div class="col-md-2">
                      <label class="form-label">Item Height</label>
                       <input type="text" name="itemheight" class="form-control" id="itemheight">
                    </div> 
                                        
                      <div class="col-md-6">
                      <label class="form-label">Published</label>
                       <select id="publish" class="form-control" name="publish"> 
                        <option value="yes">Yes</option>
                        <option value="no">No</option> 
                      </select>
                    </div>

                    <div class="col-md-6">
                      <label class="form-label">Sub Module</label>
                        <input type="text" name="submodule" id="submodule" class="form-control">
                    </div>
                     <div class="col-md-6">
                      
                    </div>

                      
                     <div class="col-md-12 mt-3 text-center">
                       <div class="col">
                       	<button class="btn btn-primary">Save</button>
                       </div>
                    </div>
</form>

     </div>
     </div>
   </div>
</div>

<!-- edit section ends here -->
@endsection 
@section("script")
<script>
$(document).on("click",".btneditconfirm", function()
{
        $("#sid").val($(this).attr("data-key"));
        $("#title").val($(this).attr("data-title"));
        $("#showtitle").val($(this).attr("data-showtitle"));
        $("#Rows").val($(this).attr("data-row"));
        $("#Columns").val($(this).attr("data-column"));
        $("#bgColor").val($(this).attr("data-bgcolor"));
        //$("#").val($(this).attr("data-bgimage")); 
        $("#showbackground").val($(this).attr("data-showimg"));
        $("#itemshape").val($(this).attr("data-shape")); 
        $("#itemwidth").val($(this).attr("data-width")); 
        $("#itemheight").val($(this).attr("data-height")); 
        $("#uitype").val($(this).attr("data-uitype"));
        $("#publish").val($(this).attr("data-publish")); 
        $("#submodule").val($(this).attr("data-submodule")); 
        
        $("#showedit").modal("show");
});


$(document).on('click', '.delconfirm', function() {
  $("#key").val($(this).attr("data-key"));
  $(".confirmdel").modal("show");
});

</script>


@endsection