@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
    
	<div class="col-md-8 offset-md-2">

 <div class="card">
                <div class="card-header">Section Detils</div>

                <div class="card-body ">
            <form method="post" 
            action="{{action('Admin\AdminDashboardController@saveAppSectionDetails')}}" 
            enctype="multipart/form-data" > 

                   {{ csrf_field()}} 
                        <div class="row">
                           
                            <div class="col-md-6">
                                   <label>Select Section Title:</label>
                                  <div class="dropdown show ">
                                   
                                    <select  class="form-control" name="sectionid">
                                      @foreach($results as $items)
                                      <option value="{{$items->id}}">{{$items->title}}</option>
                                      @endforeach 
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6"> 
                                <label>image URL:</label> 
                                <input id="imageurl" type="file" class="form-control" name="photo">
                               </div> 
                        </div> 
                        
						<div class="row">                          
                        <div class="col-md-6">
                        <label>Caption:</label> 
                        <textarea rows="3" name="caption" id="caption" class="form-control" placeholder="caption"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Sub Caption:</label> 
                            <textarea rows="3" name="subcaption" id="subcaption" class="form-control" placeholder="sub caption"></textarea>
                        </div>
                       	</div>
 						
                       	 
                        <div class="form-group row">
                           
                            <div class="col-md-4">
                                <label>Caption Layout:</label>

                               <select class="form-control" name="layout">
                                   <option value="horizontal">Horizontal</option>
                                   <option value="vertical">Vertical</option>
                               </select>
                               
                            </div>

                            <div class="col-md-4">
                                <label>Rating:</label>

                                <select class="form-control" name="rating">
                                    <option value="1">1</option>
                                     <option value="2">2</option>
                                      <option value="3">3</option>
                                       <option value="4">4</option>
                                        <option value="5">5</option>
                                </select>
                               
                            </div>
                            
                             <div class="col-md-4">
                                 <label>Type:</label>
                                 
                                <select name="type" id="type" class="form-control">
                                	<option value="product">Products</option>
                                	<option value="service">Service</option>
                                	<option value="business">Business</option>

                                </select>
                               
                            </div>

                        </div>

                        <div class="form-group row">
                           
                            <div class="col-md-12">
                                <label>Target App URL:</label>

                                <input id="appurl" type="type" class="form-control"  name="appurl" required >
                               
                            </div>
                        </div>

                        <div class="form-group row">
                        	<div class="col-md-12">
 								 
                                <label>Target Web URL:</label>

                                <input id="weburl" type="text" class="form-control" name="weburl" required >
                                
                        	</div>
                        </div>
                        <div class="form-group row">

                             <div class="col-md-4">
                                 
                                  <label>Is sponsored</label>
                                  <select id="sponsored" class="form-control" name="sponsored">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                  </select>
                                </div>
                            </div>
                         
                        <div class="form-group row mb-0 " >
                        <div class="col-md-4">
                        <button type="submit" class="btn btn-primary w-50 " name="btsave" value="save">Save</button>
                        </div>
                            
                        </div>
                    </form>
                </div>
                </div>
	</div>
</div>

@endsection 

@section("script")
<script>
$(document).on("click",".btneditconfirm", function() 
{
        $("#sid").val($(this).attr("data-key"));
        $("#title").val($(this).attr("data-title"));
        $("#showtitle").val($(this).attr("data-showtitle"));
        $("#Rows").val($(this).attr("data-row"));
        $("#Columns").val($(this).attr("data-column"));
        $("#bgColor").val($(this).attr("data-bgcolor"));
        //$("#").val($(this).attr("data-bgimage")); 
        $("#showbackground").val($(this).attr("data-showimg"));
        $("#itemshape").val($(this).attr("data-shape")); 
        $("#itemwidth").val($(this).attr("data-width")); 
        $("#itemheight").val($(this).attr("data-height")); 
        $("#uitype").val($(this).attr("data-uitype"));
        $("#publish").val($(this).attr("data-publish")); 
        $("#submodule").val($(this).attr("data-submodule")); 
        
        $("#showedit").modal("show");
});

$(document).on('click', '.delconfirm', function() {
  $("#key").val($(this).attr("data-key"));
  $(".confirmdel").modal("show");
});
</script>
@endsection