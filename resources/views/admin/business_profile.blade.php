@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 


<div class="cardbodyy margin-top-bg"> 

        <?php 

$business = $data['business'];
 
?>


          <div class="card-body">
            <div class="row">

          <div class="col-lg-8">
            <div class="card card-chart">
              <div class="card-header"> 
                <h4 class="card-title text-center">Shop/Business Profile</h4>
                 
              </div>
             <div class="card-body">
          <div class="row">
            <div class="col-md-6">
         <div><strong>Store Name :</strong> {{$business->name}}</div>
         <div><strong>Shop No.:</strong> {{$business->shop_number}}</div>
         <div><strong>Store Address :</strong> {{$business->locality}}, {{$business->landmark}}, {{$business->city}}, {{$business->state}}-{{$business->pin}}</div>
         <div><strong>Primary Phone :</strong> {{$business->phone_pri}}</div>
         <div><strong>Alternate Phone :</strong> {{$business->phone_alt}}</div>
         <div><strong>Is Open :</strong> {{$business->is_open}}</div>
 
              </div> 
               <div class="col-md-6">
                  <img src='http://booktou.in/app/<?php 
      if($business->profile_image=="")
      { 
        echo "public/assets/image/no-image.jpg"; 
       }
  else{
        echo $business->profile_image ;
       }?>'
       alt="..." height="130px" width="190px">
              </div> 
  
            </div>
          </div>


        </div>
      </div>
       
       
         <div class="col-lg-12">
         <div class="card card-chart">  
              
<div class="card-body">
     <div class="card-header"> 
                <h4 class="card-title text-center">Item Details</h4>
              </div>
              
  <form class="form-inline" method="get" action="">

            {{ csrf_field() }}
              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Item Category:</label> 
              <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='search_key'>
                 @foreach ($data['category'] as $item)
                    <option value='{{$item->category_name}}'>{{$item->category_name}}</option> 
                 @endforeach
                </select>

                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>

              <hr class="my-4" />

 
 <table class="table">
  
   
  <thead class=" text-primary">
    
   <tr class="text-center">
      <th scope="col">Sl.No.</th>
    <th scope="col">Item Name</th>
    <th scope="col">Initial Stock</th>
    <th scope="col">Stock in Hand</th>
    <th scope="col">Max Order</th>
    <th scope="col">Unit Name</th>
    <th scope="col">Unit Price</th>
    <th scope="col">Item Image</th>
    </tr>
    </thead>

    <tbody>
     <tr class="text-center">
     <td></td>
     <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
  
    </tr>
   </tbody>

  </table>
</br>

  </div>

   
</div>    
  
   </div> 

   </div>
     
  </div>
</div>


@endsection


 
  
 