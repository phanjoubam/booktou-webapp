@extends('layouts.admin_theme_02')
@section('content')
<?php 
  $cdn_url =    config('app.app_cdn') ;  
  $cdn_path =     config('app.app_cdn_path') ;  
?>
<style>
  .cards {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
 
  margin-left: 16px;
}

.cards:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
  padding: 2px 16px;
}
.card .card-header {
  /*background: #3bbbd9;*/
}
</style>

@if (session('err_msg'))
    <div class="col-md-12 alert alert-success">
        {{ session('err_msg') }}
    </div>
@endif
<div class="row">
    
 <div class='col-md-4' > 
   <div class="card  ">
    <div class="card-header ">
      <strong><b class="text-dark">Add CRS Content</b></strong>
    </div>
    <div class="card-body  ">
      <div class='row'>
       
       <form  method="post" action="{{action('Admin\CarRentalServiceController@addLandingScreenSectionContent')}}"
       class="row g-3" enctype="multipart/form-data">


       {{csrf_field()}}
       
       <div class="col-md-12">
        <label for="inputState" class="form-label">Meta Key </label>                     
        <select  class="form-control selectType" name="meta_key">                     
          
         @foreach($package_metas as $item)           
         <option value="{{$item->meta_key}}">
          {{ $item->meta_key_text}}
        </option>
        @endforeach
      </select>                      
    </div>
    
    <div class="col-md-12">
      <label  class="form-label">Select-Image</label>
      <input class="form-control" type="file" id="photo" name="photo" required>
    </div>
    <div class="col-md-6">
      <label  class="form-label">Meta Value</label>
      <input class="form-control" type="text"  name="meta_value" required>
      
  </div>
  
  <div class="col-md-6">
    <label  class="form-label">Image Caption</label>
    <input class="form-control" type="text" id="image_caption" name="image_caption" required>
  </div>
  <div class="col-md-6">
    <label  class="form-label">Target Screen</label>
    <select  class="form-control selectType" name="target_screen" >                                  
     <option id="bus_view" value="business_view" >Business</option>
     <option id="bus_view" value="package_view" >Package</option>
     <option id="bus_view" value="details" >Details</option>  
  </select>   
  </div>

  <!-- <div class="col-md-6 view_business" id="dauid">
    <select id="dbbin" >

    </select>

  </div> -->
  <div class="col-md-6">
    <label  class="form-label">Layout Type</label>
    <select  class="form-control " name="section_id">                     
     @foreach($sections as $item)           
     <option value="{{$item->id}}">
      {{$item->layout_type}}
    </option>
    @endforeach
  </select>
</div>

<div class="col-md-12 mt-4 text-center">
 <div class="col-md-12">
  <button class="btn btn-primary col-md-12" name="btn_save" value="save">Save</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
   
<div class='col-md-8'>
  
  <div class="card  ">
     
    <div class="card-header "> 
      <div class='row'>
        <div class='col-md-10'>
          <h4><b class="text-dark">Content Listing</b></h4>
        </div>
        <div class='col-md-2 text-right'>
         <button class='btn btn-primary btn-sm showdialogm1' data-toggle="modal" data-target="#modalepg" data-key=""
        ><i class='fa fa-plus'></i>
   
         </button>
        </div>
      </div>
    </div>
    <div class="card-body  ">
       

      @foreach($sections as $section) 
 
              <table class='table table-bordered mt-3'>
                <tr>
                  <th colspan="4" class='text-left'>{{ $section->heading_text }}</th>
                </tr>
                <tr>
                  <th >Icon</th>
                  <th >Meta Key</th>
                  <th >Meta Value</th>
                  <th >Action</th>

                </tr>


              @foreach($section_contents as $section_section)

              @if( $section->id == $section_section->section_id)
                <tr>
                   <td><img src="{{ $section_section->image_url  }}" width="50px" style="white-space:normal;"><button class="badge badge-primary badge-pill  btnUpdateIcon" data-icon="{{ $section_section->id }}" 
                        data-leading="{{ $section_section->image_url}}" style="margin-left:20px;" >
                  <i class="fa fa-pencil"></i>
                </button></td>

                  <td>
                    {{ $section_section->meta_key  }}
                  </td>
                  <td>
                    {{ $section_section->meta_value  }}
                  </td>
                  <td>

                   <div class="dropdown  ">
                  <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog "></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item btnedit"  data-widget="del"
                       data-keyid="{{$section_section->id}}"
                       data-secid="{{$section_section->section_id}}"
                        data-photo="{{$section_section->image_url}}"
                        data-meta-key="{{$section_section->meta_key}}"
                        data-meta-value="{{$section_section->meta_value}}"
                        data-image-caption="{{$section_section->image_caption}}"
                        data-target-screen="{{$section_section->target_screen}}"
                        data-uid="{{$section_section->uid}}"
                        data-widget="edit"

                    >Edit</a>


      <a class="dropdown-item " 
      href="{{ URL::to('/admin/link-landing-section-with-package-business')}}/{{ $section_section->id }}" target="_blank" >Link Package/Business</a>



                  <a class="dropdown-item btndel" data-id="{{$section_section->id}}" data-widget="del">Delete</a>
              
                </div>
              </div>
                  </td>

                </tr> 
              @endif
              @endforeach
            </table>
  

           
       @endforeach 
      </div>
    </div>    
  </div>
</div>     
<form action='{{ action("Admin\CarRentalServiceController@editLandingScreenSectionsImage") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='widget-m2' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit car rental services</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
           <div class="row ">            
             <div class="col-md-6">
                      <label for="inputState" class="form-label">Select-Image</label>
                      <input class="form-control" type="file" id="photo" name="photo">
                    </div>
          </div> 
        </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='image' id='image' />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>
<form action='{{ action("Admin\CarRentalServiceController@editLandingScreensection") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='widget-m1' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit car rental services</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
       <div class="row "> 
                 <div class="col-md-6">
                      <label for="inputState" class="form-label">Meta Key </label>
                      <input class="form-control" type="text" id="metakey" name="metakey">                     
                      
                  </div>

                    <div class="col-md-6">
                      <label for="inputState" class="form-label">Meta Value </label>
                      <input class="form-control" type="text" id="metavalue" name="metavalue">                     
                      
                  </div>
                    <div class="col-md-6">
                      <label  class="form-label">Image Caption</label>
                      <input class="form-control" type="text" id="imagecaption" name="imagecaption">
                    </div>
                   <div class="col-md-6">
                      <label  class="form-label">Target Screen</label>
                      <input class="form-control" type="text" id="targetscreen" name="targetscreen">
                    </div>
                    <div class="col-md-6">
                      <label  class="form-label">Uid</label>
                      <input class="form-control" type="text" id="uids" name="uids">
                    </div>
                     <div class="col-md-6">
                      <label for="inputState" class="form-label">Layout Type</label>
                      <input  class="form-control selectType" name="sectionid" id="sectionid">                     
                    </div>
                   </div> 
        </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='key' id='key' />
       
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>
<form action="{{action('Admin\CarRentalServiceController@deleteLandingScreensection')}}" method="post" enctype="multipart/form-data"> 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-del" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Confirm Record Deletion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">You are about to delete this record. Please confirm your action?</div>
        <div class="modal-footer">
          
         <input type='hidden' name='key' id='keydel'  />
        
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' name='btndel'>Delete</button>
        </div>
      </div>
    </div>
  </div>
 </form>

@endsection
 @section("script")

<script>
$(".btndel").on('click', function(){ 
      var key  = $(this).attr("data-id"); 

      $("#keydel").val(key); 
      $widget  = $(this).attr("data-widget"); 
      $("#widget-"+$widget).modal('show'); 
});
 $(document).on("click", ".btnUpdateIcon", function()
    {
    $("#image").val($(this).attr("data-icon"));
    $("#photo").attr("src", $(this).attr("data-photo")); 
    $("#widget-m2").modal("show");
    });
$(".btnedit").on("click", function()
  {

    $("#key").val( $(this).attr("data-keyid"));
    $("#sectionid").val($(this).attr("data-secid"));
   
    $("#metakey").val( $(this).attr("data-meta-key"));
    $("#metavalue").val( $(this).attr("data-meta-value"));    
    $("#imagecaption").val( $(this).attr("data-image-caption"));
    $("#targetscreen").val( $(this).attr("data-target-screen"));
    $("#uids").val( $(this).attr("data-uid")); 

    $("#widget-m1").modal("show")
  });
// /////////////////////
  var siteurl = " <?php echo config('app.url') ?>";  
  // $('.selectType').on('change', function()
  // {
  //   var selection = $(this).val();

  //   $.ajax({
    
  //   url : siteurl +'/admin/manage-landing-section-contents-business', 
  //   data: {
  //   'bus_view': selection
  //   },
  //   type: 'get', 
  //   success: function (response) {
  
  //       response = $.parseJSON (response);
  //       $.each(response.results, function(index, element){
  //         $("#dbbin").append("<option>" +   element.name  +"</option>");
  //       }); 
  //   }
  // });
// });



  </script>
 
 @endsection





 
  
 