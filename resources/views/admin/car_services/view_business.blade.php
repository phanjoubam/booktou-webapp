@extends('layouts.admin_theme_02')
@section('content')

<?php 
$cdn_url =   env('APP_CDN') ;  
    $cdn_path =   env('APP_CDN_PATH') ; // cnd - /var/www/html/api/public
    $business = $data['business'];
    $section_contents  =$data['section_contents'];
    $services = $data['services'];
    ?>
    <div class="row">
     <div class="col-md-12"> 

       <div class="card card-default">
         <div class="card-header">
          <div class="card-title"> 
            <div class="row">
             <div class="col-md-7">
               <h5>Service Listed Under <span class='badge badge-primary'></span></h5>
             </div>
           </div> 
         </div>
       </div>
       <div class="card-body">
        @if (session('err_msg'))
        <div class="col-md-12">
          <div class="alert alert-info">
           {{session('err_msg')}}
         </div>
       </div>
       @endif 
       <div class="table-responsive">
        <table class="table"> 
          <thead class=" text-primary">
            <tr >
              <th scope="col">Image</th>
              <th scope="col">Item </th> 
              <th scope="col">Price</th>
              <th scope="col">Discount</th>
              <th scope="col">Service Category</th>
              <th scope="col">Service type</th>
              <th scope="col">Gender</th>
              <th scope="col">Add Package/Business</th>
            </tr>
          </thead> 
          <tbody> 
            @foreach ( $data['services'] as $item)  
            <tr >
             <td> 
             </td>
             <td style="white-space: normal;"><span class='badge badge-primary'>{{ $item->id }}</span><br/>
              {{ $item->srv_name }}<br/>     
              <td>{{$item->pricing}}</td>
              <td>{{$item->discount}}</td> 
              <td>{{$item->service_category}}</td>
              <td>{{$item->service_type}}</td>
              <td>{{$item->gender}}</td>
              <td><button class="btn btn-primary showdialogm1"
                        data-keyid="{{$section_contents->id}}"
                        data-name="{{$item->id}}"                        
                        data-widget="edit">Edit</button></td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $data['services']->links() }}
      </div>
    </div>
  </div>    
</div> 
</div>
<form action='{{ action("Admin\CarRentalServiceController@landingSectionPackageBusinessSave") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='widget-m2' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add package/business</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
           <div class="row ">            
             <div class="col-md-12">
                          <label for="uid" class="form-label">Uid</label>
                          <select class="form-control" name="uid" id="uid">
                         @foreach ( $data['services'] as $item)
                            <option value="{{$item->id}}">
                              {{$item->srv_name}}
                            </option>
                          @endforeach
                          </select>

                        </div>


          </div> 
        </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='key' id='key' value="{{$section_contents->id}}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>
@endsection
@section("script")
<script>
  $(document).on("click", ".showdialogm1", function()
  {
    $("#key").val( $(this).attr("data-keyid"));
    $("#uid").val( $(this).attr("data-name"));
    $("#widget-m2").modal("show")
  });
</script>
 
@endsection


