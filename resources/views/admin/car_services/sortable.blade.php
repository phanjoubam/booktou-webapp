@extends('layouts.admin_theme_02')
@section('content')
<?php 
  $cdn_url =    config('app.app_cdn') ;  
  $cdn_path =     config('app.app_cdn_path') ;  
 
?>

<!-- 
 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
  /*#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }*/
  </style> -->
 @if (session('err_msg'))
    <div class="col-md-12 alert alert-success">
        {{ session('err_msg') }}
    </div>
@endif
<div class="row"> 
   
<div class='col-md-12'>
  
  <div class="card  ">
     
    <div class="card-header "> 
      <div class='row'>
        <div class='col-md-10'>
          <h4><b class="text-light">Landing Sections</b></h4>
        </div>
        <div class='col-md-2 text-right'>
         <button class='btn btn-primary btn-sm showdialogm1' data-toggle="modal" data-target="#modalepg" data-key=""
        ><i class='fa fa-plus'></i>
   
         </button>
        </div>
      </div>
    </div>
    <div class="card-body "  > 
       <div class="card-group grid "  id="sortable">
             <?php $i = 0 ?>   
      @foreach($sections as $section) 
     <?php 
    $i++  ;
    $found = false;

    foreach($section_contents as $plist)
    {
      if($plist->section_id == $section->id ) 
                              {
                                $found= true;break;
                              }
                            }
                          
     ?>
         <div class="col-md-12 mt-2" >           
          <div class="card " >            
            <div class="card-body well span2 tile" >
             <table class='table table-bordered'>
             <tr>
                  <th colspan="2" class='text-left'>{{ $section->heading_text }}</th>
                </tr>
                <tr>
                  <th >Display Position</th>
                  <th >Is Visible</th>                  
                </tr> 
           <!--   <hr> -->
               <tr>
                  <td> 
                    <a class=" {{$section->id}} nextimage 
             alt="{{$section->display_sequence}}" 
                   data-key="{{$section->id}}" 
                   data-order="{{$section->display_sequence}}"  
                   data-index="{{$loop->iteration}}" 
                   data-newindex="" ">{{ $section->display_sequence }}</a>
                  </td>
                  <td>
                  <form method='post' action="">
                      <div class="form-row align-items-center">
                        <div class="col-auto">
                          <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input btnToggleShowApp"   data-id='{{$section->id }}' id="blockSwitchON{{ $section->id }}" 
                             <?php if($section->show_in_app == "yes") echo "checked";  ?>  />
                            <label class="custom-control-label" for="blockSwitchON{{$section->id }}">Yes</label>
                          </div>
                         </div>                          
                      </div>
                    </form> 
                  </td>
                </tr> 
              </table>
           </div>
         </div>      
       </div>
       @endforeach
     </div>
   </div>
    </div>    
  </div>
</div>     

 


@endsection
 @section("script")
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
 <script>
$('body').delegate('.btnToggleShowApp','click',function(){
  
   var section_id  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
   var json = {};
    json['section_id'] =  section_id ;
    json['status'] =  status ;
    console.log(json);    
   $.ajax({
      type: 'post',
      url: api + "v3/web/booking/toggle-booking-show" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);                
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 
    });
});

  $( function() {
    $( "#sortable" ).sortable();
  } );
  </script>
 
 
 

 @endsection

<!-- ===================================================================================== -->
 @extends('layouts.admin_theme_02')
@section('content')
<?php 
  $cdn_url =    config('app.app_cdn') ;  
  $cdn_path =     config('app.app_cdn_path') ;  
 
?>
<style>
  .cards {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
 
  margin-left: 16px;
}

.cards:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
  padding: 2px 16px;
}
.card .card-header {
  background: #3bbbd9;
}
</style>

@if (session('err_msg'))
    <div class="col-md-12 alert alert-success">
        {{ session('err_msg') }}
    </div>
@endif
<div class="row"> 
   
<div class='col-md-12'>
  
  <div class="card  ">
     
    <div class="card-header "> 
      <div class='row'>
        <div class='col-md-10'>
          <h4><b class="text-light">Landing Sections</b></h4>
        </div>
        <div class='col-md-2 text-right'>
         <button class='btn btn-primary btn-sm showdialogm1' data-toggle="modal" data-target="#modalepg" data-key=""
        ><i class='fa fa-plus'></i>
   
         </button>
        </div>
      </div>
    </div>
    <div class="card-body"> 
       <div class="card-group grid" id="listofimage">
             <?php $i = 0 ?>   
      @foreach($sections as $section) 
     <?php 
    $i++  ;
    $found = false;

    foreach($section_contents as $plist)
    {
      if($plist->section_id == $section->id ) 
                              {
                                $found= true;break;
                              }
                            }
                          
     ?>
         <div class="col-md-4 ">           
          <div class="card ">            
            <div class="card-body well span2 tile">
             <table class='table table-bordered'>
             <tr>
                  <th colspan="2" class='text-left'>{{ $section->heading_text }}</th>
                </tr>
                <tr>
                  <th >Display Position</th>
                  <th >Is Visible</th>                  
                </tr> 
           <!--   <hr> -->
               <tr>
                  <td> 
                    <a class=" a{{$section->id}} nextimage 
             alt="{{$section->display_sequence}}" 
                   data-key="{{$section->id}}" 
                   data-order="{{$section->display_sequence}}"  
                   data-index="{{$loop->iteration}}" 
                   data-newindex="" ">{{ $section->display_sequence }}</a>
                  </td>
                  <td>
                  <form method='post' action="">
                      <div class="form-row align-items-center">
                        <div class="col-auto">
                          <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input btnToggleShowApp"   data-id='{{$section->id }}' id="blockSwitchON{{ $section->id }}" 
                             <?php if($section->show_in_app == "yes") echo "checked";  ?>  />
                            <label class="custom-control-label" for="blockSwitchON{{$section->id }}">Yes</label>
                          </div>
                         </div>                          
                      </div>
                    </form> 
                  </td>
                </tr> 
              </table>
           </div>
         </div>      
       </div>
       @endforeach
     </div>
   </div>
    </div>    
  </div>
</div>     

 

@endsection
 @section("script")

<script>
$('body').delegate('.btnToggleShowApp','click',function(){
  
   var section_id  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
   var json = {};
    json['section_id'] =  section_id ;
    json['status'] =  status ;
    console.log(json);    
   $.ajax({
      type: 'post',
      url: api + "v3/web/booking/toggle-booking-show" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);                
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 
    });
});

 $(".grid").sortable({
    tolerance: 'pointer',
    revert: 'invalid',
    placeholder: 'span2 well placeholder tile',
    forceHelperSize: true,
        //function to call ajax parts
        stop: function(event, ui) {
          var key = ui.section.find('a').attr("data-key");

          var counter= 1;
          $("#listofimage a").each(function () {

            newId = $(this).attr("data-index") + counter;
            $(this).attr("data-newindex", counter);
            counter++;

          });  

          var order_list = $(this).find('.a'+key).attr('data-order');
          var generated_index = $(this).find('.a'+key).attr('data-newindex');

          $.ajax({
           url: '/admin/update-landing-screen-sections-by-drag',
           type: 'get',
           data: {'code': key,
           'orderlist':order_list,
           'neworderlist':generated_index
         },
         dataType: 'json',
         contentType: 'application/json',
         success: function(response){ 
           if (response.success) {
            $("#listofimage").empty().append(response.html);
          }else{
           alert("failed");
         }
       }
     });
        }

        //ends here

      });
  </script>
 
 @endsection





 
  
 