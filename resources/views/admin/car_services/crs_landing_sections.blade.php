@extends('layouts.admin_theme_02')
@section('content')
<?php 
  $cdn_url =    config('app.app_cdn') ;  
  $cdn_path =     config('app.app_cdn_path') ;  
?>
 @if (session('err_msg'))
    <div class="col-md-12 alert alert-success">
        {{ session('err_msg') }}
    </div>
@endif
<div class="row"> 
<div class='col-md-12'>
    <form action="{{ action('Admin\CarRentalServiceController@reorderLandingScreenSections') }}" method='post' >
    {{ csrf_field()  }}
  <div class="card  ">     
    <div class="card-header "> 
      <div class='row'>
        <div class='col-md-10'>
          <h4><b class="text-dark">Landing Sections</b></h4>
        </div>
        <div class='col-md-2 text-right'>
         <button class='btn btn-primary btn-sm showdialogm1' data-toggle="modal" data-target="#modalepg" data-key=""
        ><i class='fa fa-plus'></i>   
         </button>
        </div>
      </div>
    </div>
    <div class="card-body "  > 
       <div class="card-group grid"  id="sortable">
        <?php $i = 0 ?>   
          @foreach($sections as $section) 
         <?php 
        $i++  ;
        $found = false;
        foreach($section_contents as $plist)
        {
          if($plist->section_id == $section->id ) 
          {
            $found= true;break;
          }
        }                              
        ?>
         <div class="col-md-12 mt-2" >           
          <div class="card cardrow" >             
             <table class='table table-bordered'>
                <tr>
                  <th colspan="2" class='text-left'>{{ $section->heading_text }}</th>
                </tr>
                <tr>
                  <th >Display Position</th>
                  <th >Is Visible</th>                  
                </tr>         
               <tr>
                  <td> 
                    <a alt="{{ $section->display_sequence }}" 
              
                   data-newindex="">{{ $section->display_sequence }}</a>
                   <input type='hidden'   name='keys[]'  value="{{  $section->display_sequence }}" />
                  </td>
                  <td>
                  <form method='post' action="">
                      <div class="form-row align-items-center">
                        <div class="col-auto">
                          <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input btnToggleShowApp"   data-id='{{$section->id }}' id="blockSwitchON{{ $section->id }}" 
                             <?php if($section->show_in_app == "yes") echo "checked";  ?>  />
                            <label class="custom-control-label" for="blockSwitchON{{$section->id }}">Yes</label>
                          </div>
                         </div>                          
                      </div>
                    </form> 
                  </td>
                </tr> 
              </table> 
         </div>      
       </div>
       @endforeach
     </div>
   </div>
   <div class='card-footer'>
      <div class='row'>
        <div class='col-md-12'>
            <button type='submit' value='reorder' name='btnupdate' class='btn btn-primary btn-sm'>Save New Order</button>
        </div> 
      </div>
   </div>
    </div>    
  </form>
  </div>
</div>     
@endsection
 @section("script")
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
 <script>
$('body').delegate('.btnToggleShowApp','click',function(){
  
   var section_id  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
   var json = {};
    json['section_id'] =  section_id ;
    json['status'] =  status ;
    console.log(json);    
   $.ajax({
      type: 'post',
      url: api + "v3/web/booking/toggle-booking-show" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);                
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 
    });
});

  $( function() {
    $( "#sortable" ).sortable();   
  });


  </script>
 
 
 

 @endsection