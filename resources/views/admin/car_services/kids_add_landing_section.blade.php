@extends('layouts.admin_theme_02')
@section('content')
<?php 
$cdn_url =    config('app.app_cdn') ;  
$cdn_path =     config('app.app_cdn_path') ;  

?>
<style>
  .cards {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;

    margin-left: 16px;
  }

  .cards:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }

  .container {
    padding: 2px 16px;
  }
  .card .card-header {
    /*background: #3bbbd9;*/
  }
</style>

@if (session('err_msg'))
<div class="col-md-12 alert alert-success">
  {{ session('err_msg') }}
</div>
@endif
<div class="row">
  <div class="col-md-12">
      <nav class="nav navbar-top-links">
    <h4><span style="padding-left:4px"><a href="{{URL::to('admin/app-landing-screen-sections')}}" class="badge badge-primary"><i class='fa fa-plus'></i> CRS Section</a> </span> 
      <span style="padding-left:15px"><a href="{{URL::to('admin/app-landing-screen-sections-for-kids')}}" class="badge badge-primary"><i class='fa fa-plus'></i> kids Section</a></span>
    </h4>
    </nav>
  </div>    
</div>
<div class="row mt-1">

    <div class='col-md-4' >
      <div class="card  ">
        <div class="card-header ">
          <strong><b class="text-dark">Add Kids Section</b></strong>
        </div>
           <div class="card-body  ">
              <div class='row'>
               <form  method="post" action="{{action('KidsController@addKidsLandingScreenSections')}}"
               class="row g-3" enctype="multipart/form-data">
               {{csrf_field()}}
               <div class="col-md-6">
                  <label for="main_module" class="form-label">Main Module </label>       @foreach($bookables as $item) 
                  <input type="text" class="form-control" name="main_module" value="{{$item->main_module}}" readonly>         
                  @endforeach                          
               </div>
             <div class="col-md-6">
              <label  class="form-label">Sub Module</label>
              @foreach($bookables as $item)
              <input class="form-control" type="text"  name="sub_module" value="{{$item->sub_module}}" readonly>
            </div>
          @endforeach 
     <div class="col-md-12">
      <label  class="form-label">Heading text</label>
      <input class="form-control" type="text"  name="heading_text" required>
    </div>
    <div class="col-md-6">
      <label  class="form-label">Leading Icon</label>
      <input type="file"  name="photo" class="form-control" required>
    </div>
     <div class="col-md-6">
      <label  class="form-label">Layout Type</label>
      <select  class="form-control selectType" name="layout_type" >                                
        <option value="image-slider">image-slider</option>
        <option value="icon-grid">icon-grid</option>
        <option value="block-image">block-image</option>
      </select>
    </div>
    <div class="col-md-6">
      <label  class="form-label">Horizontal scroll</label>
      <select  class="form-control" type="text" name="horizontal_scroll" >                                
        <option value="yes">yes</option>
        <option value="no">no</option>
      </select>
    </div>
       <div class="col-md-6">
      <label  class="form-label">Show In App</label>
      <select  class="form-control" type="text" name="show_in_app">                                
        <option value="yes">yes</option>
        <option value="no">no</option>
      </select>
    </div>  
    <div class="col-md-6">
      <label for="number" class="form-label">Row</label>
      <input class="form-control input-number" type="number"  name="row" required>
    </div>
    <div class="col-md-6">
      <label  class="form-label">Column</label>
      <input class="form-control input-number" type="number" name="col" required>
    </div>
    
    <div class="col-md-6">
      <label  class="form-label">Display Sequence</label>
      <input class="form-control" type="number"  name="display_sequence" required>
    </div>
    <div class="col-md-12 mt-4 text-center">
     <div class="col-md-12">
      <button class="btn btn-primary col-md-12" name="btnsave" value="save">Save</button>
    </div>
  </div>
</form>
</div>
</div>
</div>
</div>

<div class='col-md-8'>

  <div class="card  ">

    <div class="card-header "> 
      <div class='row'>
        <div class='col-md-10'>
          <h4><b class="text-dark">Landing screen section Listing</b></h4>
        </div>
        <div class='col-md-2 text-right'>
         <button class='btn btn-primary btn-sm ' data-toggle="modal" data-target="#modalepg" data-key=""
         ><i class='fa fa-plus'></i>

       </button>
     </div>
   </div>
 </div>
 <div class="card-body  ">



  <table class='table table-bordered table-responsive-md tab mt-3'>
    <thead class=" text-primary">
      <tr>

        <th >Leading Icon</th>               
        <th >Heading Text</th>
        <th >Layout Type </th>
        <th >Show In App</th>
        <th >Display Sequence</th>
        <th >Action</th>

      </tr>
    </thead>
    <tbody> 
     @foreach($sections as $section) 
     <tr>
      <td><img src="{{$section->leading_icon  }}" width="50px" style="white-space:normal;"><button class="badge badge-primary badge-pill  btnUpdateIcon" data-icon="{{ $section->id }}" 
                data-leading="{{$section->leading_icon}}" style="margin-left:20px;" >
          <i class="fa fa-pencil"></i>
        </button>
      </td>
      <td style="white-space:normal;">{{$section->heading_text}} 
      </td>                   
      <td>{{$section->layout_type}}</td>
      <td>{{$section->show_in_app}}</td>
      <td>{{$section->display_sequence}}</td>

      <td class="text-center">
        <div class="dropdown">
         <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-icon="{{ $section->id }}" 
                data-leading="{{ $section->leading_icon}}">
          <i class="fa fa-cog "></i>
        </a>
        <ul class="dropdown-menu dropdown-user pull-right"> 
          <li><a class="dropdown-item showdialogm1"                
                data-key="{{ $section->id }}" 
                data-layout="{{ $section->layout_type}}"
                data-head="{{ $section->heading_text}}"
                data-row="{{ $section->row}}"
                data-col="{{ $section->col}}"
                data-scroll="{{ $section->horizontal_scroll}}"
                data-display="{{ $section->display_sequence}}"
                data-show="{{ $section->show_in_app}}"              
            >Edit </a></li>
            <li><a class="dropdown-item btndel" data-id="{{$section->id}}" data-widget="del" >Delete</a>
            </li>
          </ul>
        </div>
      </td>
    </tr>
    @endforeach             
</table>
</div>
</div>    
</div>
</div>

<form action='{{ action("KidsController@editKidsLandingScreenSectionsIcon") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='widget-m2' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Change image</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
           <div class="row ">            
             <div class="col-md-6">
              <label for="inputState" class="form-label">Select-Image</label>
              <input class="form-control" type="file" id="photo" name="photo">
            </div>
          </div> 
        </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='icon' id='icon' />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>



<form action='{{ action("KidsController@editKidsLandingScreenSections") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='widget-m1' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit Landing screen section</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
           <div class="row "> 
    <div class="col-md-6">
      <label  class="form-label">Heading_text</label>
      <input class="form-control" type="text"  name="heading_text" id="heading_text">
    </div>

    <div class="col-md-6">
      <label  class="form-label">Layout Type</label>
       <input class="form-control" type="text"  name="layout_type" id="layout_type">
    </div>
    <div class="col-md-6">
      <label  class="form-label">Row</label>
      <input class="form-control" type="text"  name="row" id="row">
    </div>
    <div class="col-md-6">
      <label  class="form-label">Column</label>
      <input class="form-control" type="text"  name="col" id="col">
    </div>
    <div class="col-md-6">
      <label  class="form-label">Horizontal scroll</label>
      <input class="form-control" type="text"  name="horizontal_scroll" id="horizontal_scroll">
    </div>
    <div class="col-md-6">
      <label  class="form-label">Display Sequence</label>
      <input class="form-control" type="text"  name="display_sequence" id="display_sequence">
    </div>
    <div class="col-md-6">
      <label  class="form-label">Show In App</label>
      <input class="form-control" type="text"  name="show_in_app" id="show_in_app">
    </div>
            </div> 
          </div>         
        </div>
        <div class="modal-footer"> 
          <span class='loading_span'></span>
          <input type='hidden' name='key' id='key' />

          <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
          <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
        </div>
      </div>
    </div>
  </div>
</form>

<form action="{{action('KidsController@deleteKidsLandingScreenSections')}}" method="post" enctype="multipart/form-data"> 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-del" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Confirm Record Deletion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">You are about to delete this record. Please confirm your action?</div>
        <div class="modal-footer">
         <input type='hidden' name='keys' id='keydel'/>
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' name='btndel'>Delete</button>
        </div>
      </div>
    </div>
  </div>
 </form>
@endsection
@section("script")
<script>
  $(".btndel").on('click', function(){ 
    var key  = $(this).attr("data-id"); 

    $("#keydel").val(key); 
    $widget  = $(this).attr("data-widget"); 
    $("#widget-"+$widget).modal('show'); 
  });

   $(document).on("click", ".btnUpdateIcon", function()
    {
    $("#icon").val($(this).attr("data-icon"));
    $("#photo").attr("src", $(this).attr("data-leading")); 
    $("#widget-m2").modal("show");
    });
  $(".showdialogm1").on("click", function()
  {
    $("#key").val( $(this).attr("data-key"));
    $("#main_module").val( $(this).attr("data-main"));
    $("#sub_module").val($(this).attr("data-sub"));
    $("#heading_text").val( $(this).attr("data-head"));
    $("#layout_type").val( $(this).attr("data-layout")); 
    $("#row").val( $(this).attr("data-row"));   
    $("#col").val( $(this).attr("data-col"));
    $("#horizontal_scroll").val( $(this).attr("data-scroll"));
    $("#display_sequence").val( $(this).attr("data-display"));
    $("#show_in_app").val( $(this).attr("data-show")); 
    $("#widget-m1").modal("show");
  });
</script> 
@endsection







