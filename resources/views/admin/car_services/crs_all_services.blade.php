@extends('layouts.admin_theme_02')
@section('content')

<?php 
$cdn_url =    config('app.app_cdn') ;  
$cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>
    <div class="row">
     <div class="col-md-12"> 
       <div class="card card-default">
         <div class="card-header">
          <div class="card-title"> 
            <div class="row">
              <div class="col-md-8">
                <div class="title-block"> 
                 <h5>All Car Rental Services</h5> 
               </div> 
             </div>
             <div class="col-md-4">
              <form class="form-inline"   method="get" action="{{ action('Admin\CarRentalServiceController@allCarRentalServices') }}" enctype="multipart/form-data">
                {{  csrf_field() }}
                <div class="form-row">
                  <div class="col-md-12"> 
                    <input type="text" name ="search_key" class="form-control form-control-sm"  /> 
                    <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="card-body">
        @if (session('err_msg'))
        <div class="col-md-12">
          <div class="alert alert-info">
           {{session('err_msg')}}
         </div>
       </div>
       @endif 
       <div class="table-responsive">
        <table class="table"> 
          <thead class=" text-primary">
            <tr >
              <th scope="col">Image</th>
              <th scope="col">Business Name</th>
              <th scope="col">Item </th>
               <th scope="col">Phone</th> 
              <th scope="col">Price</th>
              <th scope="col">Discount</th>
              <th scope="col">Sponsored</th>
              <th scope="col">Delete</th>
              <th scope="col">Action</th>

            </tr>
          </thead> 
          <tbody> 
            <?php $i = 0 ?>
            @foreach ( $data['packages'] as $package) 
              <?php 
              $i++  ;
              $found = false;

              if(isset($data['sponsored_packages'])):
                  foreach($data['sponsored_packages'] as $sponsored_package)
                  {
                    if($sponsored_package->package_id == $package->id )
                      {
                        $found= true;break;
                      }
                   }
               endif;
                                    
               ?> 
            <tr >
             <td> 
              <img src="{{ $package->photos  }}" alt="..." height="50px" width="50px"> 
            </td>
            <td style="white-space: normal;"><span class='badge badge-primary'>{{ $package->bin }}</span><br/>
              {{ $package->name }}<br/></td>
             <td style="white-space: normal;"><span class='badge badge-primary'>{{ $package->id }}</span><br/>
              {{ $package->srv_name }}<br/></td> 
              
              <td>{{$package->phone_pri}}</td>     
              <td>{{$package->pricing}}</td>
              <td>{{$package->discount}}</td> 
              <td>
                <div class="custom-control custom-switch">
                  <input type="checkbox" class="custom-control-input btnToggleYes"   data-packageid='{{$package->id}}' 
                  id="yesSwitchON{{ $package->id }}" 
                  <?php if( $found  ) echo "checked";  ?>  />
                  <label class="custom-control-label" for="yesSwitchON{{$package->id }}">Yes</label>
                </div> 
              </td>
              <td><button class="btn btn-danger btndel" 
                        data-id="{{$package->id}}" data-widget="del"
                        >Delete</button></td>
           
              <td class="text-center">
                <div class="dropdown">
                  <a class="btn btn-primary btn-sm btn-icon-only text-light showdialogm2"  role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                  
                  >
                    <i class="fa fa-cog "></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">  
                    <a class="dropdown-item showdialogm1"
                    data-name="{{ $package->service_category }}" 
                    data-key="{{ $package->id }}" 
                    data-srvname="{{ $package->srv_name}}"
                    data-gender="{{ $package->gender}}"
                    data-price="{{ $package->pricing}}"
                    data-srvtype="{{ $package->service_type}}"
                  

                    >Update services</a>
                   <a class="dropdown-item" href="{{URL::to('/admin/customer-care/business/view-more-details-crs')}}/{{$package->id}}">View More Details</a>
                     @if( !$found )
                    <a href='#' class="dropdown-item btn_add_promo"  data-sprid="{{$package->id }}" data-bin="{{$package->bin }}"   >Sponsored Package</a>
                    @endif
                  </div>
                 
                </div>                  
              </td>
             
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $data['packages']->links() }}
      </div>
    </div>
  </div>    
</div> 
</div>
<div class="modal fade" id="modal_add_promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form method='post' action="{{ action('Admin\CarRentalServiceController@addSponsoredPackage') }}" enctype="multipart/form-data">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <select class="form-control form-control-sm boxed " id='is_sponsored' name='is_sponsored' >
                        <option>Yes</option>
                        <option>No</option>                       
                        </select>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='bin' id='bin'/>
        <input type='hidden' name='srv_prid' id='srv_prid'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>
 
<form action='{{ action("Admin\CarRentalServiceController@editAllCarRentalService") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='widget-m1' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit car rental services</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
           <div class="col-md-4">
            <label  class="form-label">Category-Image</label>
            <input class="form-control" type="file" id="photo" name="photo">
            
          </div>
          <div class="col-md-4 ">
            <label  class="form-label">Service category:</label>
            <input type="text" name="service_category" id="serviceCategory" class=" form-control">
          </div>
          <div class="col-md-4 ">
            <label  class="form-label"> Service name:</label>
            <input type="text" name="srv_name" class=" form-control" id="srvname">
          </div>
            <div class="col-md-3 ">
    <label  class="form-label">gender :</label>
    <select class="form-control "  name='gender' id="gender">
      <option>male</option>
      <option>female</option>
      <option>none</option>
      <option>unisex</option>
      <option>not-relevant</option>                      
    </select>
  </div>
  <div class="col-md-4 ">
          <label  class="form-label"> Pricing:</label>
          <input type="text" name="pricing" class=" form-control" id="price">
        </div>
        <div class="col-md-3 ">
    <label  class="form-label">service type :</label>
    <input type="text" name="service_type" class=" form-control" id="srvtype">
  </div>
        </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='key' id='key' />
        <input type='hidden' name='bin' id='bin' value="{{ $package->bin }}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>

<form action="{{action('Admin\CarRentalServiceController@deleteAllCarRentalServices')}}" method="post" enctype="multipart/form-data"> 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-del" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Confirm Record Deletion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">You are about to delete this record. Please confirm your action?</div>
        <div class="modal-footer">
         <input type='hidden' name='key' id='keydel'/>
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' name='btndel'>Delete</button>
        </div>
      </div>
    </div>
  </div>
 </form>
@endsection
@section("script")
<script>


 


  $(document).on("click", ".btn_add_promo", function()
{
  $("#srv_prid").val($(this).attr("data-sprid"));
  $("#bin").val($(this).attr("data-bin"));

  $("#modal_add_promo").modal("show"); 
});
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
  });
$(".btndel").on('click', function(){ 
      var key  = $(this).attr("data-id"); 

      $("#keydel").val(key); 
      $widget  = $(this).attr("data-widget"); 
      $("#widget-"+$widget).modal('show'); 
});

  $(document).on("click", ".showdialogm1", function()
  {
    $("#serviceCategory").val( $(this).attr("data-name"));
    $("#key").val( $(this).attr("data-key"));
    $("#photo").attr("src", $(this).attr("data-photo"));
    $("#srvname").val( $(this).attr("data-srvname"));
    $("#price").val( $(this).attr("data-price"));
    $("#srvtype").val( $(this).attr("data-srvtype"));
    $("#gender").val( $(this).attr("data-gender")); 

    $("#widget-m1").modal("show")
  });

  $('body').delegate('.btnToggleYes','click',function(){ 

  var srv_prid  = $(this).attr("data-packageid"); 
  var status = $(this).is(":checked"); 
  var json = {};
  json['srv_prid'] =  srv_prid ;
  json['status'] =  status ;  
  console.log(json);  
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-sponsored-service" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  
  });
  
</script>

@endsection