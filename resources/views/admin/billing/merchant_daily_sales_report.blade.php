@extends('layouts.admin_theme_02')
@section('content')

<div class="row"> 

<div class="col-md-12">
 
     <div class="card card-default">
       <div class="card-header">   
       	<div class='row'>
       		<div class='col-md-8'>
       			<h3>Merchant List  </h3>
       		</div>
       		<div class='col-md-3'> 
                       <!--  <input type="date" class="calendar form-control"> -->
                        
       		</div>
                  <div class="col-md-1">
                    <!--   <button type="submit" class="btn btn-primary">Search</button>   -->
                  </div> 
       	</div> 
       </div>
<div class="card-body">  
	<table class="table">
		<thead>
                  <th>Bin</th>
			<th>Merchant Name</th>
			<th>Phone No</th>
			<th style="text-align: right;">Action</th>
		</thead>

            <tbody>
            @foreach($business as $biz)
                  <tr>
                        <td>{{$biz->id}}</td>
                        <td>{{$biz->name}}</td>
                        <td >{{$biz->phone_pri}}</td>
                  <td style="text-align: right;">  
                  <a class="btn btn-primary" href="{{URL::to('admin/daily-sales-and-service-export-to-excel')}}?bin={{$biz->id}}&todayDate={{$todayDate}}">
                  View Daily Sales
                  </a>
                  </td>
                  </tr>
            @endforeach
            </tbody>

	</table>
	 

</div>
 
</div> 
 

</div>
</div>

@endsection

@section("script")
<script>

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script>
@endsection