@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">
                    Unpaid Dues for merchants <span class='badge badge-primary'>{{ $month }}-{{ $year }}</span> 

                  </div> 
                </div>

<div class="col-md-4 text-right"> 
  <form class="form-inline" method="get" action="{{ action('Admin\AccountsAndBillingController@monthlyServiceReport') }}"   >
     
  <div class="form-row align-items-center"> 
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInputGroup">Month</label>
       <select name='month' class="form-control form-control-sm  ">
          <option value='1'>January</option>
          <option value='2'>February</option>
          <option value='3'>March</option>
          <option value='4'>April</option>
          <option value='5'>May</option>
          <option value='6'>June</option>
          <option value='7'>July</option>
          <option value='8'>August</option>
          <option value='9'>September</option>
          <option value='10'>October</option>
          <option value='11'>November</option>
          <option value='12'>December</option> 
        </select>
    </div>
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInputGroup">Year</label>
        <input readonly name='year' class="form-control form-control-sm  " value="{{ date('Y') }}">
      
    </div>
    <div class="col-auto">
       <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>
  </div>
  </form>

 
 </div> 
             
      </div>

       </div>
            
    
       <div class="card-body">    
    <table class="table">
      <thead>
        <tr>
          <th>BIN</th>
          <th>Business</th> 
          <th class='text-right'>PnD Amount</th> 
          <th class='text-right'>Normal Sales Amount</th> 
          <th class='text-right'>Sub-Total</th> 
          <th class='text-right'>Action</th>  
        </tr>
      </thead>
      
      <tbody>
      @php
        $grossTotal =0.00;
      @endphp
          @foreach ($businesses as $item)
          <tr>
            <td>{{ $item->id  }}</td>
            <td>{{ $item->name }} 

              @php
                  $totalPnd =0.00;
              @endphp

              @foreach ($pnd_sales as $pnd)
                @if($pnd->request_by == $item->id )
                  @php
                    $totalPnd += $pnd->totalAmount;
                  @endphp
                  @break
                @endif 
              @endforeach

             <td class='text-right'>{{  $totalPnd }}</td> 

              @php
                  $totalNormal =0.00;
              @endphp

              @foreach ($normal_sales as $normal)
                @if($normal->bin == $item->id )
                  @php
                    $totalNormal += $normal->totalAmount;
                  @endphp
                  @break
                @endif 
              @endforeach


             <td class='text-right'>{{ $totalNormal }}</td>  
    

            @php
              $totalAmount = $totalPnd + $totalNormal; 
              $grossTotal +=$totalAmount;
            @endphp

          <td class='text-right'>{{ $totalAmount }}</td>  
 
            <td class='text-right'>
                <a class="btn btn-primary btn-sm" target='_blank' 
                href="{{ URL::to('/admin/billing-and-clearance/sales-and-clearance-report')}}?bin={{$item->id}}">View Details</a>
            </td>
          </tr>
        @endforeach  
        <tr>
          <th colspan='4' class='text-right'>Total:</th>
          <th class='text-right'>{{ $grossTotal }}</th>  
          <th></th>
        </tr> 
      </tbody>

    </table>
 
 </div>


  </div> 
	
	</div> 
</div> 
 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

