@extends('layouts.admin_theme_02')
@section('content')
 
<?php 

$business = $data['business'];
 
?>
<div class="row">   
<div class="col-md-12">  
 <div class="card">
  <div class="card-header"> 
    <div class="row">   
<div class="col-md-8">  
      <h4 >Sale Report For <strong>{{$business->name}}</strong></h4>
      <p>{{$business->locality}}, {{$business->landmark}}, {{$business->city}}, {{$business->state}}-{{$business->pin}}<br/>
  <i class='fa fa-phone'></i> {{$business->phone_pri}}
</p>
 </div> 
<div class="col-md-4">  
 
  </div> 
   </div> 
  </div> 
  <div class="card-body"> 

@if( isset($data['results'])) 

<div class='table-responsive'> 
  <form action="{{ action('Admin\AdminDashboardController@salesClearanceForm') }}" method='post'>
    @csrf 
 <table class="table"> 
  <thead class=" text-primary">
    <tr class="text-center"> 
      <th scope="col">Select</th>
    <th scope="col">Order No.</th>
    <th scope="col">Order Date</th> 
    <th scope="col">Customer</th> 
    <th scope="col">Order Type</th> 
    <th scope="col" class='text-right'>Order Status</th>    
     <th scope="col" class='text-right'>Order Origin</th>    
    <th scope="col"  class='text-right'>Amount to clear</th>
    <th scope="col"  class='text-right'>Delivery Charge</th>
     <th scope="col" class='text-right'>Pay Mode</th>   
    <th scope="col" class='text-right'>Clerance Status</th>  
    </tr>
    </thead> 
    <tbody>

       @php 
        $total_sales  = 0;
        $total_fee = 0;

       @endphp 
 

     <?php $i = 0 ?>
    @foreach ($data['results'] as $item)
    <?php $i++ ?> 
     <tr class="text-center"> 
      <td>  
         <input <?php if( $item->clerance_status != "un-paid"  || $item->book_status == "canceled" ) echo "disabled" ?> class="form-check-input" type="checkbox" value="{{$item->id}}" name="selection[]">  
      </td>
        

      @if($item->orderType == "pnd")  
        <td>
          <a target='_blank' href="{{ URL::to('admin/customer-care/pnd-order/view-details') }}/{{ $item->id }}"  >{{$item->id}}</a>   
        </td>
        <td>{{ date('d-m-Y', strtotime( $item->book_date) ) }}</td>  
       <td class='text-left'>{{$item->customerName}}</td> 
       <td class='text-center'>
          <span class='badge badge-success'>{{ $item->orderType }}</span>  
       </td>
 
        
        @else
          <td>
            <a target='_blank' href="{{ URL::to('admin/customer-care/order/view-details/') }}/{{ $item->id }}"  >{{$item->id}}</a>   
          </td>
           <td>{{ date('d-m-Y', strtotime( $item->book_date) ) }}</td>  
           <td class='text-left'>{{$item->customerName}}</td> 
           <td class='text-center'>
               <span class='badge badge-info'>{{ $item->orderType }}</span>
           </td> 

        @endif  

     <td>@switch($item->book_status)
          @case("new")
            <span class='badge badge-primary'>New</span>
            @break 
          @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>

                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                        @break 

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                        @break
                      
                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                        @break

                      @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                        @break 

                      @case("cancel_by_client")
                        <span class='badge badge-danger'>Canceled by customer</span>
                        @break
                      @case("cancel_by_owner")
                        <span class='badge badge-danger'>Canceled by merchant</span>
                        @break
                      @case("canceled")
                        <span class='badge badge-danger'>Canceled</span>
                        @break
                      @endswitch
 
           </td> 
            <td>
    @if($item->source == "booktou")
      <span class='badge badge-primary'>bookTou</span>
    @else
      <span class='badge badge-danger'>Merchant</span>
    @endif
  </td>

    @if($item->book_status == "delivered")     

     @php 
        $total_sales +=    $item->total_amount;
        $total_fee += $item->service_fee;
       @endphp
  
      <td  class='text-right'>
      {{ $item->total_amount  }}
    </td> 
  <td  class='text-right'>
      {{ $item->service_fee  }}
    </td> 
       @else
         <td class='text-right'> 0.00</td> <td class='text-right'> 0.00</td> 
      @endif
    <td  class='text-right'>
      {{$item->pay_mode}}
    </td> 

     <td  class='text-right'>
      @switch($item->clerance_status)

                      @case("paid")
                        <span class='badge badge-success'>PAID</span>
                      @break

                      @case("un-paid")
                        <span class='badge badge-danger'>UN-PAID</span>
                      @break 

                      @case("not required")
                        <span class='badge badge-info'>NOT REQUIRED</span>
                      @break


   @endswitch
    </td> 

    </tr>
    
@endforeach
@if(isset($data['results']) && count($data['results']) > 0)  
     <tr  > 
      <td   class='text-right' colspan='7'>Total:</td>
     <td class='text-right' >{{ number_format(  $total_sales , 2, '.', '')     }} ₹</td>   
     <td colspan='4'>
       
       <input type='hidden' name='bin' value="{{ $data['bin']}}" >
       <button type='submit' name='btnsubmit' value='submit' class='btn btn-primary btn-block'>Prepare Payment Form</button>

     </td>  
    </tr> 
 @endif
</tbody>
</table>

</form>
</div> 

@endif 
</div>
   
</div>    
  
   </div> 

   </div>

     

@endsection


  
 