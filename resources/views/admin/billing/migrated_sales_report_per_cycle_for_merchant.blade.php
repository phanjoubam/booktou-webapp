@extends('layouts.admin_theme_hollow')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;   

?>


  <div class="row">
     <div class="col-md-12"> 
     <div class="card ">

       <div class="card-body" style='overflow-x: scroll;'>    

        <div class='row'>
          <div class='col-md-7'>
         <h4>Migrated Orders Report for <span class='badge badge-info badge-pill'>{{  date('F', mktime(0, 0, 0, $month, 10)) }}, {{ $year  }}</span>

              @if(count($results) > 0)
<a  href="#" data-pdfurl="{{  URL::to('/admin/billing-and-clearance/migrated-order-sales-and-clearance-report') }}?start={{ $start }}&end={{ $end }}&month={{ $month }}&year={{ $year }}&format=a4&download=no"
 class='btn btn-success btn-print btn-sm btn-oval'  title='Click to print'><i class='fa fa-print'></i> Print A4</a> 
 <a  href="#" data-pdfurl="{{ URL::to('/admin/billing-and-clearance/migrated-order-sales-and-clearance-report') }}?start={{ $start }}&end={{ $end }}&month={{ $month }}&year={{ $year }}&format=roll"
 class='btn btn-info btn-print btn-sm btn-oval'  title='Click to print'><i class='fa fa-print'></i> Print Roll</a>

<a target='_blank' href ="{{  URL::to('/admin/billing-and-clearance/migrated-order-sales-and-clearance-report') }}?start={{ $start }}&end={{ $end }}&month={{ $month }}&year={{ $year }}&format=a4&download=yes"
 class='btn btn-warning  btn-print btn-sm btn-oval'  title='Click to download'><i class='fa fa-download'></i> Download</a> 
 
@endif
            </h4>
          </div>
          <div class='col-md-5'>


<form class="form-inline" method="get" action="{{ action('Franchise\FranchiseAccountsAndBillingController@migratedOrderSalesAndClearanceReport') }}"   >
   
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                 <div class="input-group-prepend">
                  <span class="input-group-text">Report Filter</span>
                </div> 
              </div>  
              <select name='month' class="form-control  ">
          <option <?php if( $month  == 1 ) echo "selected"; ?> value='1'>January</option>
          <option <?php if( $month== 2 ) echo "selected"; ?>  value='2'>February</option>
          <option <?php if( $month == 3 ) echo "selected"; ?> value='3'>March</option>
          <option <?php if( $month== 4 ) echo "selected"; ?> value='4'>April</option>
          <option <?php if( $month == 5 ) echo "selected"; ?> value='5'>May</option>
          <option <?php if( $month == 6 ) echo "selected"; ?> value='6'>June</option>
          <option <?php if( $month == 7 ) echo "selected"; ?> value='7'>July</option>
          <option <?php if( $month == 8 ) echo "selected"; ?> value='8'>August</option>
          <option <?php if( $month == 9 ) echo "selected"; ?> value='9'>September</option>
          <option <?php if($month == 10 ) echo "selected"; ?> value='10'>October</option>
          <option <?php if( $month== 11 ) echo "selected"; ?> value='11'>November</option>
          <option <?php if( $month == 12 ) echo "selected"; ?> value='12'>December</option> 
        </select>

        <input readonly name='year' class="form-control   " value="{{ date('Y') }}" /> 

              <div class="input-group-append">
<button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>

              </div>
            </div>

  </form>
  

          </div>
        </div>
            <hr/>
    <table class="table table-bordered" width="100%; font-size:10px">
      <thead>
        
        <tr>
          <th>Order No</th>  
          <th>Date</th> 
          <th>Business</th>
          <th width='340px'>Order Type | Source | Status | Pay Mode | Payment Target</th> 
          <th class='text-right'>Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Comm. (@ %)</th>
          <th class='text-right'>Delivery</th> 
          <th class='text-right'>Due on Merchant</th> 
          <th class='text-right'>Earning</th> 
        </tr>
      </thead>
      
      <tbody>
        @php 
          $packagingCost = $commission = $dueOnMerchant = $deliveryFee = $orderCost = $earning = $commissionPc = 0.00;
          $totalCash = $totalOnline =  $totalCollectedAtMerchant = $totalDueOnMerchant =$totalSale = $totalFee = $totalCommission = $totalEarning = $totalDeliveryFee = $totalOnlineMerchant = $totalPackagingCost =0.00; 
        @endphp
          @foreach ($results as $normal)

              @foreach($businesses as $business)
                @if($normal->bin == $business->id) 
                     

           @if($normal->bookingStatus == "delivered") 

            <tr>
                
                @if($normal->orderType == "normal")
                  @php 
                    $tempPackagingCost = 0.00;
                  @endphp
                  @foreach($cart_items as $cart)
                    @if($normal->id == $cart->order_no)
                      
                      @php 
                        $tempPackagingCost += $cart->qty * $cart->package_charge;
                      @endphp

                    @endif
                  @endforeach

                  @php 
                      $normal->packingCost  = $tempPackagingCost;
                  @endphp
                  <td><a target='_blank' href="{{ URL::to('/admin/order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td> 
                 <td>
                  {{ $business->name }} ({{ $business->zone }})
                 </td> 
                  <td class='text-center' width='340px'><span class='badge badge-success'>{{ $normal->orderType  }}</span> <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0)
                     <span class='badge badge-info'>ONLINE</span>
                    @else 
                     <span class='badge badge-primary'>CASH</span>
                    @endif 
                    <span class='badge badge-success'>bookTou</span>
                  </td>
   

                  @php
                      $dueOnMerchant =0.00;
                      $commissionPc = $business->commission ; 
                      $orderCost = $normal->orderCost;
                      $packagingCost = $normal->packingCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalPackagingCost += $packagingCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning;  

                  @endphp
 

                  @if( strcasecmp($normal->paymentType , "online") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                    @php 
                      $totalOnline += $normal->orderCost  ;  //packing cost already included
                    @endphp
                  @else 
                    @php
                      $totalCash += $normal->orderCost  ;  //packing cost already included  
                    @endphp
                  @endif 

 
                  <td class='text-right'>{{ number_format( $orderCost, 2 )   }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} (@ {{ $commissionPc }}%)</td> 
                  <td class='text-right'>{{ number_format( $deliveryFee, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $dueOnMerchant, 2 ) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td> 


                @elseif($normal->orderType == "pnd")

                  <td><a target='_blank'  href="{{ URL::to('/admin/pnd-order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                   <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                   <td>
                  {{ $business->name }} ({{ $business->zone }})
                 </td>
                  <td class='text-center' width='340px' ><span class='badge badge-primary'>{{ $normal->orderType  }}</span> <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>
                      {{  ($normal->source=="business" || $normal->source=="merchant" ) ? "Merchant" : "Customer" }}
                    </span> <span class='badge badge-success'>{{ $normal->bookingStatus }}</span>
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                     <span class='badge badge-info'>ONLINE</span>
                    @else 
                     <span class='badge badge-primary'>CASH</span>
                    @endif 
                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                      <span class='badge badge-danger'>Merchant</span>
                    @else 
                      <span class='badge badge-success'>bookTou</span>
                    @endif
                  </td>
 

                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)
 
                    @php
                      $dueOnMerchant =0.00;       
                      $commissionPc = $business->commission  ; 
                      $orderCost = $normal->orderCost;
                      $packagingCost = $normal->packingCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning; 
                    @endphp 

                    @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 ) 
                          @php 
                            $dueOnMerchant  = $normal->deliveryCharge; 
                            $totalCollectedAtMerchant += $normal->orderCost;  
                            $totalPackagingCost += $packagingCost;
                            $totalDueOnMerchant += $dueOnMerchant; 
                            $totalOnlineMerchant += $normal->orderCost + $normal->packingCost;
                          @endphp
                        @else
                            @php
                              $totalOnline += $normal->orderCost + $normal->packingCost;
                            @endphp  
                        @endif  
                   @else
                        @php 
                          $dueOnMerchant  = 0.00; 
                          $totalCash += $normal->orderCost + $normal->packingCost;   
                        @endphp     
                    @endif  

                  @else 
  
                      @php
                            $commissionPc = 0.00 ; 
                            $orderCost = $normal->orderCost;
                            $packagingCost = $normal->packingCost;
                            $deliveryFee = $normal->deliveryCharge;
                            $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                            $earning =  $deliveryFee +  $commission; 
                            $totalSale += $orderCost;
                            $totalPackagingCost += $packagingCost;
                            $totalFee += $deliveryFee;
                            $totalCommission += $commission;
                            $totalEarning += $earning;  
                      @endphp 

                    @if( strcasecmp($normal->paymentType , "online") == 0  )   
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 )  
                          @php
                             $totalOnlineMerchant += $normal->orderCost;  
                             $totalCollectedAtMerchant += $normal->orderCost; 
                             $totalPackagingCost += $normal->packingCost; 
                             $totalDueOnMerchant += $normal->deliveryCharge; 
                             $dueOnMerchant  = $normal->deliveryCharge;  
                          @endphp  
                        @else
                          @php
                            $dueOnMerchant  = 0.00;
                            $totalOnline += $normal->orderCost + $normal->packingCost;
                          @endphp 
                        @endif 
                    @else
                        @php 
                          $dueOnMerchant  = 0.00;
                          $totalCash += $normal->orderCost + $normal->packingCost;   
                        @endphp     
                    @endif   

                  @endif 
                    
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format($commission,2) }} (@ {{ $commissionPc }}%)</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td>  

                @elseif($normal->orderType == "assist")
                  <td><a target='_blank' href="{{ URL::to('/admin/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td >---</td>
                  <td class='text-center' width='340px'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>

                  <span class='badge badge-success'>{{ $normal->bookingStatus }}</span>
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                      <span class='badge badge-info'>ONLINE</span>
                    @else 
                      <span class='badge badge-primary'>CASH</span>
                    @endif
                  
                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                      <span class='badge badge-danger'>Merchant</span>
                    @else 
                      <span class='badge badge-success'>bookTou</span>
                    @endif  
                  </td>  
                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                    @php
                      $dueOnMerchant =0.00;
                      $totalDueOnMerchant = 0.00;
                      $totalCollectedAtMerchant = 0.00;
                      $packagingCost = 0.00;                  
                    @endphp
                      
                    @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0 || strcasecmp($normal->paymentTarget , "business") == 0) 
                          @php
                            $dueOnMerchant = $normal->deliveryCharge;
                            $totalCollectedAtMerchant += $normal->orderCost;  
                            $totalDueOnMerchant += $dueOnMerchant; 
                            $totalOnlineMerchant += $totalCollectedAtMerchant;  
                          @endphp 
                        @else
                          @php
                            $totalOnline += $normal->orderCost;
                          @endphp  
                        @endif
                    @else
                          @php 
                            $totalCash += $normal->orderCost;  
                          @endphp 
                        
                    @endif

                    @php 
                      $commissionPc =  $business->commission  ; 
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * $orderCost ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning; 
                    @endphp   

                  @else   

                    @php
                      $dueOnMerchant =0.00;
                      $commissionPc =  0.00 ; 
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * $orderCost ) / 100 ;
                      $earning =  $deliveryFee +  $commission; 
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning;
                      $totalDueOnMerchant = 0.00;
                      $totalCollectedAtMerchant = 0.00; 

                    @endphp

                  @endif
 
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format($commission,2) }} (@ {{ $commissionPc }}%)</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td>  

                @endif   

              </tr>

           @else

            <tr> 
              
                @if($normal->orderType == "normal")
                  <td><a target='_blank' href="{{ URL::to('/admin/order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td> 
                  <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td> 
                  <td>
                  {{ $business->name }} ({{ $business->zone }})
                </td> 
                <td class='text-center' width='340px'><span class='badge badge-success'>{{ $normal->orderType  }}</span>
                @elseif($normal->orderType == "pnd")
                  <td><a target='_blank'  href="{{ URL::to('/admin/pnd-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td>
                  {{ $business->name }} 
                 </td>
                  <td class='text-center'><span class='badge badge-info'>{{ $normal->orderType  }}</span>  
                @else
                  <td><a target='_blank'  href="{{ URL::to('/admin/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td>---</td>
                  <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> 
                @endif 
                <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span> <span class='badge badge-danger'>{{ $normal->bookingStatus }}</span> - 
                @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                  <span class='badge badge-info'>ONLINE</span>
                @else 
                  <span class='badge badge-primary'>CASH</span>
                @endif
              
                @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                  <span class='badge badge-danger'>Merchant</span>
                @else 
                  <span class='badge badge-success'>bookTou</span>
                @endif
              </td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00  (@ 0%)</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td>  
          </tr> 

           @endif 
          @endif
        @endforeach  
   @endforeach
     
       <tr>
          <th>Order No</th>  
          <th>Date</th> 
          <th>Business</th>
          <th width='340px'>Order Type | Source | Status | Pay Mode | Payment Target</th> 
          <th class='text-right'>Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Comm. (@ %)</th>
          <th class='text-right'>Delivery</th> 
          <th class='text-right'>Due on Merchant</th> 
          <th class='text-right'>Earning</th> 
        </tr> 
        <tr>
          <th colspan='3' class='text-right' style='color:red'><i>i) F=C+D; ii) D includes E</i> </th> 
            <th   class='text-right'>Total</th> 
            <th class='text-right'>A = {{ number_format($totalSale,2, '.', '') }}</th>
            <th class='text-right'>B = {{ number_format($totalPackagingCost,2, '.', '') }}</th>
            <th class='text-right'>C = {{ number_format($totalCommission,2, '.', '') }}</th> 
            <th class='text-right'>D = {{ number_format($totalFee,2, '.', '') }}</th> 
            <th class='text-right'>E = {{ number_format($totalDueOnMerchant,2, '.', '') }}</th>
            <th class='text-right'>F = {{ number_format($totalEarning,2, '.', '') }}</th>  
        </tr> 
<tfoot>
        
</tfoot> 
      </tbody> 
    </table> 
<hr/>
<form>
    <div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Collected at Merchant (G = A-H-I ):</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalCollectedAtMerchant ,2, '.', '') }}"  >
    </div> 
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Due towards Merchant (E):</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalDueOnMerchant ,2, '.', '') }}"  >
    </div>

     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Commission to deduct (C):</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalCommission ,2, '.', '') }}"  >
    </div>


  </div>

<div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Cash to bookTou (H):</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalCash ,2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Online to bookTou (I):</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalOnline ,2, '.', '') }}"  >
    </div>

     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Total To Clear (J=H+I-E-C):</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm is-valid" readonly value="{{ number_format( ( $totalCash + $totalOnline - $totalCommission - $totalDueOnMerchant)  ,2, '.', '') }}"  >
    </div>
 
  </div> 
</form>

 </div> 
  </div> 
  
  </div> 
</div>
 </div>
 
 

 
@endsection



@section("script")

<style>
.multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}

</style>

<script>

var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}


$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
   var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    }); 

});


$('.selectize').select2({
  selectOnClose: true
});
 

$(document).on("click", ".btn-print", function()
{
  window.open($(this).attr("data-pdfurl"), "popupWindow", "width=600,height=600,scrollbars=yes"); 
});

</script>  


@endsection

