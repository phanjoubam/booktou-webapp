@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header"> 
                 <div class="row">

                   <div class="col-md-6">
                  <div class="title">
                    Daily Sales &amp; Service for <span class='badge badge-primary'>{{ $reportDate }}</span> 

                  </div> 
                </div>

<div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-4">  
 <form class="form-inline" method="get" action="{{ action('Admin\AccountsAndBillingController@dailySalesAndServiceReport') }}"   > 
      <div class="form-row">
    <div class="col-md-12">
        <input name='reportDate' class='form-control form-control-sm calendar' /> 
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>

       </div>
            
            </div>
       <div class="card-body">    
    <table class="table">
      <thead>
        <tr>
          <th>BIN</th>
          <th>Business</th> 
          <th>PnD and Normal Orders Count</th> 
          <th class='text-right'>PnD Amount</th> 
          <th class='text-right'>Normal Sales Amount</th>  
          <th class='text-right'>Sub-Total</th> 
          <th class='text-right'>Action</th>  
        </tr>
      </thead>
      
      <tbody>
      @php
        $subTotal =0.00;
        $grossTotal =0.00;

         $totalOnline =0.00;
         $totalCash =0.00;
      @endphp
          @foreach ($businesses as $item)
          <tr>
            <td>{{ $item->id  }}</td>
            <td><a href="{{ URL::to('/admin/customer-care/business/view-profile') }}/{{$item->id}}" target='_blank'>
              {{ $item->name }}</a>
            </td>

              @php
                  $totalPnd =0.00;
                  $orderCount =0;
              @endphp

              @foreach ($pnd_sales as $pnd)
                @if($pnd->request_by == $item->id )
                  
                  @php
                    $totalPnd += $pnd->total_amount;
                    $orderCount++;
                  @endphp 


                  @if( ($pnd->source == "business" || $pnd->source == "merchant") &&  
                    strcasecmp($pnd->pay_mode, "online" ) == 0   )
                      @php
                        $totalOnline += 0;
                      @endphp
                  @else
                      @php
                        $totalCash += $pnd->total_amount;
                      @endphp
                  @endif 

                  @if( ($pnd->source == "booktou" || $pnd->source == "customer") && 
                      strcasecmp($pnd->pay_mode, "online" ) ==0   )
                      @php
                        $totalOnline += $pnd->total_amount;
                      @endphp
                  @else
                    @php
                        $totalCash += $pnd->total_amount;
                    @endphp
                  @endif

 

                  @break
                @endif 
              @endforeach 

              @php
                  $totalNormal =0.00;
              @endphp

              @foreach ($normal_sales as $normal)
                @if($normal->bin == $item->id )
                  @php
                    $totalNormal += $normal->total_cost;
                    $orderCount++;
                  @endphp


                  @if(  strcasecmp($normal->payment_type, "Pay online (UPI)" ) == 0  ||  
                        strcasecmp($normal->payment_type, "online" ) == 0   )
                      @php
                        $totalCash += $pnd->total_amount;
                      @endphp 
                  @else
                      @php
                        $totalOnline += 0;
                      @endphp
                  @endif  
 

                  @break
                @endif 
              @endforeach

              <td>{{ $orderCount }}</td>  
             <td class='text-right'>{{  $totalPnd }}</td> 
             <td class='text-right'>{{ $totalNormal }}</td>  
    

            @php
              $totalAmount = $totalPnd + $totalNormal; 
              $grossTotal +=$totalAmount;
            @endphp

          <td class='text-right'>{{ $totalAmount }}</td>  

           
            <td class='text-right'>
              
              <div class="dropdown">

                       <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a> 
 
                      <ul class="dropdown-menu dropdown-user pull-right"> 
                          <li><a class="dropdown-item btn btn-link" target='_blank'
                            href="{{ URL::to('/admin/report/daily-sales-and-service-for-merchant')}}?bin={{$item->id}}">View Details</a>
                          </li> 
                            
                        
                           </ul>
                      </div>
            </td>
          </tr>
        @endforeach  
      
      </tbody>

    </table>
 <hr/>
<form> 

<div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Total Cash Collection:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalCash ,2) }}"  >
    </div> 
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Total Online Collection:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalOnline ,2) }}"  >
    </div> 
     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Total To Clear:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm is-valid" readonly value="{{ number_format( $grossTotal  ,2) }}"  >
    </div> 
  </div>  
</form>
 </div> 
  </div> 
	
	</div> 
</div> 
 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{
   $("#bin").val($(this).attr("data-bin"));
   $("#modal_add_promo").modal("show")

});



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked");
   var json = {};
   json['bin'] =  bin ;
   json['status'] =  status ;


 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });

})



$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });

})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>


@endsection

