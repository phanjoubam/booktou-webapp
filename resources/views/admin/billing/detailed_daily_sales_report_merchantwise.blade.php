@extends('layouts.admin_theme_hollow')
@section('content')
    <?php
    $cdn_url = config('app.app_cdn');
    $cdn_path = config('app.app_cdn_path');
    
    $business = null;
    if (isset($businesses) && count($businesses) == 1) {
        $business = $businesses[0];
    }
    ?>


    <div class="row">
        <div class="col-md-12">

            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">

                            <div class="col-md-6">
                                <h2>

                                    @if (isset($business))
                                        Service Report for <span class='button button-info'><a
                                                href="{{ URL::to('/admin/customer-care/business/view-profile') }}/{{ $business->id }}"
                                                target='_blank'>{{ $business->name }}</a></span>
                                    @else
                                        Service Report for bookTou
                                    @endif

                                </h2>

                                @if (count($results) > 0)
                                    <a href="#"
                                        data-pdfurl="{{ URL::to('/admin/billing-and-clearance/generate-daily-clearance-bill') }}?bin={{ $business->id }}&reportDate={{ request()->reportDate }}&format=a4&download=no&frno={{ request()->frno }}"
                                        class='btn btn-success btn-print btn-xs' title='Click to print'><i
                                            class='fa fa-print'></i> Print A4</a>
                                    <a href="#"
                                        data-pdfurl="{{ URL::to('/admin/billing-and-clearance/generate-daily-clearance-bill') }}?bin=
 {{ $business->id }}&reportDate={{ request()->reportDate }}&format=roll&frno={{ request()->frno }}"
                                        class='btn btn-info btn-print btn-xs' title='Click to print'><i
                                            class='fa fa-print'></i> Print Roll</a>

                                    <a target='_blank'
                                        href="{{ URL::to('/admin/billing-and-clearance/generate-daily-clearance-bill') }}?bin={{ $business->id }}&reportDate={{ request()->reportDate }}&format=a4&download=yes&frno={{ request()->frno }} "
                                        class='btn btn-warning  btn-print btn-xs' title='Click to download'><i
                                            class='fa fa-download'></i> Download Report</a>
                                @endif





                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                        Change Period:
                                    </div>
                                    <div class="col-md-6">
                                        <form class="form-inline" method="get"
                                            action="{{ action('Admin\AccountsAndBillingController@dailySalesAndServiceMerchantReport') }}">

                                            <div class="form-row">
                                                <div class="col-md-12">
                                                    <input type="hidden" name='bin' value="{{ $bin }}" />
                                                    <input class="form-control form-control-sm calendar"
                                                        name='reportDate' />
                                                    <button type="submit" class="btn btn-primary btn-sm" value='search'
                                                        name='search'><i class='fa fa-search'></i></button>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card-body" style='overflow-x: scroll;'>
                    <table class="table" max-width='960px;'>
                        <thead>
                            <tr>
                                <th>Order No</th>
                                <th>Date</th>
                                <th>Order Type | Order Source</th>
                                <th class='text-center'>Status | Pay Mode | Payment Target</th>
                                <th class='text-right'>Comission %</th>
                                <th class='text-right'>Sale Amount</th>
                                <th class='text-right'>Comission</th>
                                <th class='text-right'>Delivery Fee</th>
                                <th class='text-right'>Due On Merchant</th>
                                <th class='text-right'>Due On bookTou</th>
                                <th class='text-right'>Total Earning</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php
                                $packagingCost = $commission = $dueOnMerchant = $deliveryFee = $orderCost = $earning = $commissionPc = 0.0;
                                $totalCash = $totalOnline = $totalCollectedAtMerchant = $totalDueOnMerchant = $totalDueOnBooktou = $totalSale = $totalFee = $totalCommission = $totalEarning = $totalDeliveryFee = $totalOnlineMerchant = $dueOnBooktou = $totalPackagingCost = 0.0;
                                
                            @endphp
  {{--Foreach start for orders --}}                      
                            @foreach ($results as $normal)
                                    @php
                                        $commissionPc = 0;
                                    @endphp
                                @if ($bin == 0)
                                    @foreach ($businesses as $business)
                                        @if ($business->id == $normal->requestBy)
                                            @php
                                                $commissionPc = $business->commission;
                                            @endphp
                                            @break
                                        @endif
                                    @endforeach
                                @else
                                    @php
                                        $commissionPc = $business->commission;
                                    @endphp
                                @endif


                                @if ($normal->bookingStatus == 'delivered')
                                <tr>
    {{-- if order type is normal --}}
                                    @if ($normal->orderType == 'normal')
                                        @php
                                            $tempPackagingCost = 0.0;
                                        @endphp
                                        @foreach ($cart_items as $cart)
                                            @if ($normal->id == $cart->order_no)
                                                @php
                                                    $tempPackagingCost += $cart->qty * $cart->package_charge;
                                                @endphp
                                            @endif
                                        @endforeach

                                        @php
                                            $normal->packingCost = $tempPackagingCost;
                                        @endphp


                                        <td><a target='_blank'
                                                href="{{ URL::to('/admin/customer-care/order/view-details/') }}/{{ $normal->id }}">{{ $normal->id }}</a>
                                        </td>

                                        <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date)) }}
                                        </td>


                                        <td class='text-center'><span
                                                class='badge badge-success'>{{ $normal->orderType }}</span> by <span
                                                class='badge badge-{{ $normal->source == 'business' || $normal->source == 'merchant' ? 'warning' : 'success' }}'>{{ $normal->source }}</span>
                                        </td>

                                        <td>
                                            <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> -
                                            @if (strcasecmp($normal->paymentType, 'online') == 0 || strcasecmp($normal->paymentType, 'Pay online (UPI)') == 0)
                                                <span class='badge badge-info'>online</span> -
                                            @else
                                                <span class='badge badge-primary'>cash</span> -
                                            @endif
                                            <span class='badge badge-success'>bookTou</span>
                                        </td>



                                        @php
                                            $dueOnMerchant = 0.0;
                                            $orderCost = $normal->orderCost;
                                            $deliveryFee = $normal->deliveryCharge;
                                            $commission = ($commissionPc * $orderCost) / 100;
                                            $earning = $deliveryFee + $commission;
                                            $totalSale += $orderCost;
                                            $totalFee += $deliveryFee;
                                            $totalCommission += $commission;
                                            $totalEarning += $earning;
                                            $dueOnBooktou += $normal->seller_payable;
                                            $totalDueOnBooktou += $dueOnBooktou; 
                                            
                                        @endphp



                                        @if (strcasecmp($normal->paymentType, 'online') == 0 || strcasecmp($normal->paymentType, 'Pay online (UPI)') == 0)
                                            @php
                                                $totalOnline += $normal->orderCost;
                                            @endphp
                                        @else
                                            @php
                                                $totalCash += $normal->orderCost;
                                            @endphp
                                        @endif


                                        <td class='text-right'>{{ $commissionPc }} %</td>
                                        <td class='text-right'>{{ number_format($orderCost, 2) }}</td>
                                        <td class='text-right'>{{ number_format($commission, 2) }}</td>
                                        <td class='text-right'>{{ number_format($deliveryFee, 2) }}</td>
                                        <td class='text-right'>{{ number_format($dueOnMerchant, 2) }}</td>
                                        <td class='text-right'>{{ number_format($dueOnBooktou, 2) }}</td>
                                        <td class='text-right'>{{ number_format($earning, 2) }}</td>
                                }

{{-- pnd section starts here --}}

                                    @elseif($normal->orderType == 'pnd')
                                        <td><a target='_blank'
                                                href="{{ URL::to('/admin/customer-care/pnd-order/view-details/') }}/{{ $normal->id }}">{{ $normal->id }}</a>
                                        </td>

                                        <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date)) }}
                                        </td>

                                        <td class='text-center'><span
                                                class='badge badge-primary'>{{ $normal->orderType }}</span> by
                                            <span
                                                class='badge badge-{{ $normal->source == 'business' || $normal->source == 'merchant' ? 'warning' : 'success' }}'>
                                                {{ $normal->source == 'business' || $normal->source == 'merchant' ? 'Merchant' : 'Customer' }}
                                            </span>
                                        </td>

                                        <td>
                                            <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> -
                                            @if (strcasecmp($normal->paymentType, 'online') == 0)
                                                <span class='badge badge-info'>online</span> -
                                            @else
                                                <span class='badge badge-primary'>cash</span> -
                                            @endif
                                            @if (strcasecmp($normal->paymentTarget, 'merchant') == 0)
                                                <span class='badge badge-danger'>Merchant</span>
                                            @else
                                                <span class='badge badge-success'>bookTou</span>
                                            @endif
                                        </td>


                                        @if (strcasecmp($normal->source, 'merchant') != 0 && strcasecmp($normal->source, 'business') != 0)
                                            @php
                                                $dueOnMerchant = 0.0;
                                                $dueOnBooktou = 0.0;
                                                $orderCost = $normal->orderCost;
                                                $deliveryFee = $normal->deliveryCharge + $normal->service_fee;
                                                $commission = ($commissionPc * $orderCost) / 100;
                                                $earning = $deliveryFee + $commission;
                                                $totalSale += $orderCost;
                                                $totalFee += $deliveryFee;
                                                $totalCommission += $commission;
                                                $totalEarning += $earning;
                                                $totalDueOnBooktou += $dueOnBooktou; 

                                            @endphp

                                            @if (strcasecmp($normal->paymentType, 'online') == 0)
                                                @if (strcasecmp($normal->paymentTarget, 'merchant') == 0 || strcasecmp($normal->paymentTarget, 'business') == 0)
                                                    @php
                                                        $dueOnMerchant = $normal->deliveryCharge + $normal->service_fee;
                                                        $dueOnBooktou = 0.0;
                                                        $totalCollectedAtMerchant += $normal->orderCost;
                                                        $totalDueOnMerchant += $dueOnMerchant;
                                                        $totalOnlineMerchant += $normal->orderCost;
                                                        $totalDueOnBooktou += $dueOnBooktou; 
                                                    @endphp
                                                @else
                                                    @php
                                                        $totalOnline += $normal->orderCost;
                                                        $dueOnBooktou = $normal->orderCost;
                                                        $totalDueOnBooktou += $dueOnBooktou; 
                                                    @endphp
                                                @endif
                                            @else
                                                @php
                                                    $dueOnMerchant = 0.0;
                                                    $dueOnBooktou = $normal->orderCost;
                                                    $totalDueOnBooktou += $dueOnBooktou; 
                                                    $totalCash += $normal->orderCost;
                                                @endphp
                                            @endif

    {{-- source is merchent or business --}}                                        
                                        @else
                                            @php
                                                
                                                $commissionPc = 0.0;
                                                $orderCost = $normal->orderCost;
                                                $deliveryFee = $normal->deliveryCharge + $normal->service_fee;
                                                $commission = ($commissionPc * $orderCost) / 100;
                                                $earning = $deliveryFee + $commission;
                                                $totalSale += $orderCost;
                                                $totalFee += $deliveryFee;
                                                $totalCommission += $commission;
                                                $totalEarning += $earning;
                                            @endphp

                                            @if (strcasecmp($normal->paymentType, 'online') == 0)
                                                @if (strcasecmp($normal->paymentTarget, 'merchant') == 0 || strcasecmp($normal->paymentTarget, 'business') == 0)
                                                    @php
                                                        $totalOnlineMerchant += $normal->orderCost;
                                                        $totalCollectedAtMerchant += $normal->orderCost;
                                                        $dueOnMerchant = $normal->deliveryCharge + $normal->service_fee;
                                                        $totalDueOnMerchant += $dueOnMerchant;
                                                        $dueOnBooktou = 0.00;
                                                        
                                                    @endphp
                                                @else
                                                    @php
                                                        $dueOnMerchant = 0.0;
                                                        $dueOnBooktou = $normal->orderCost;
                                                        $totalOnline += $normal->orderCost;
                                                    @endphp
                                                @endif
                                            @else
                                                @php
                                                    $dueOnMerchant = 0.0;
                                                    $dueOnBooktou = $normal->orderCost;
                                                    $totalDueOnBooktou += $dueOnBooktou;
                                                    $totalCash += $normal->orderCost;
                                                @endphp
                                            @endif
                                        @endif


                                        <td class='text-right'>{{ $commissionPc }} %</td>
                                        <td class='text-right'>{{ number_format($orderCost, 2) }}</td>
                                        <td class='text-right'>{{ number_format($commission, 2) }}</td>
                                        <td class='text-right'>{{ number_format($deliveryFee, 2) }}</td>
                                        <td class='text-right'>{{ number_format($dueOnMerchant, 2) }}</td>                                  
                                        <td class='text-right'>{{ number_format($dueOnBooktou, 2) }}</td>
                                        <td class='text-right'>{{ number_format($earning, 2) }}</td>
{{-- Assist starts here--}}                                        
                                    @elseif($normal->orderType == 'assist')
                                        <td><a target='_blank'
                                                href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{ $normal->id }}">{{ $normal->id }}</a>
                                        </td>
                                        <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date)) }}
                                        </td>
                                        <td class='text-center'><span
                                                class='badge badge-warning'>{{ $normal->orderType }}</span> -
                                            <span class='badge badge-success'>{{ $normal->source }}</span>
                                        </td>
                                        <td>
                                            <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> -
                                            @if (strcasecmp($normal->paymentType, 'online') == 0)
                                                <span class='badge badge-info'>online</span> -
                                            @else
                                                <span class='badge badge-primary'>cash</span> -
                                            @endif
                                            <span class='badge badge-success'>{{ $normal->paymentTarget }}</span>
                                        </td>


                                        @if (strcasecmp($normal->source, 'merchant') != 0 && strcasecmp($normal->source, 'business') != 0)
                                            @php
                                                $dueOnMerchant = 0.0;
                                                $totalDueOnMerchant = 0.0;
                                                $totalCollectedAtMerchant = 0.0;
                                            @endphp

                                            @if (strcasecmp($normal->paymentType, 'online') == 0)
                                                @if (strcasecmp($normal->paymentTarget, 'merchant') == 0 || strcasecmp($normal->paymentTarget, 'business') == 0)
                                                    @php
                                                        $totalCollectedAtMerchant += $normal->orderCost;
                                                        $totalDueOnMerchant += ($normal->deliveryCharge + $normal->service_fee);
                                                        $dueOnMerchant = $normal->deliveryCharge;
                                                        $totalOnlineMerchant += $normal->orderCost;
                                                    @endphp
                                                @else
                                                    @php
                                                        $totalOnline += $normal->orderCost;
                                                    @endphp
                                                @endif
                                            @else
                                                @php
                                                    $totalCash += $normal->orderCost;
                                                @endphp
                                            @endif

                                            @php
                                                $orderCost = $normal->orderCost;
                                                $deliveryFee = $normal->deliveryCharge;
                                                $commission = ($commissionPc * $orderCost) / 100;
                                                $earning = $deliveryFee + $commission;
                                                $totalSale += $orderCost;
                                                $totalFee += $deliveryFee;
                                                $totalCommission += $commission;
                                                $totalEarning += $earning;
                                            @endphp
                                        @else
                                            @php
                                                $dueOnMerchant = 0.0;
                                                $commissionPc = 0.0;
                                                $orderCost = $normal->orderCost;
                                                $deliveryFee = $normal->deliveryCharge;
                                                $commission = ($commissionPc * $orderCost) / 100;
                                                $earning = $deliveryFee + $commission;
                                                $totalSale += $orderCost;
                                                $totalFee += $deliveryFee;
                                                $totalCommission += $commission;
                                                $totalEarning += $earning;
                                                $totalDueOnMerchant = 0.0;
                                                $totalCollectedAtMerchant = 0.0;
                                                
                                            @endphp
                                        @endif

                                        <td class='text-right'>{{ $commissionPc }} %</td>
                                        <td class='text-right'>{{ number_format($orderCost, 2) }}</td>
                                        <td class='text-right'>{{ number_format($commission, 2) }}</td>
                                        <td class='text-right'>{{ number_format($deliveryFee, 2) }}</td>
                                        <td class='text-right'>{{ number_format($dueOnMerchant, 2) }}</td>
                                        <td class='text-right'>{{ number_format($earning, 2) }}</td>
                                    @endif

                                </tr>
                            @else
                                <tr>

                                    @if ($normal->orderType == 'normal')
                                        <td><a target='_blank'
                                                href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{ $normal->id }}">{{ $normal->id }}</a>
                                        </td>
                                        <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date)) }}
                                        </td>
                                        <td class='text-center'><span
                                                class='badge badge-success'>{{ $normal->orderType }}</span> by
                                        @elseif($normal->orderType == 'pnd')
                                        <td><a target='_blank'
                                                href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{ $normal->id }}">{{ $normal->id }}</a>
                                        </td>
                                        <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date)) }}
                                        </td>
                                        <td class='text-center'><span
                                                class='badge badge-info'>{{ $normal->orderType }}</span> by
                                        @else
                                        <td><a target='_blank'
                                                href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{ $normal->id }}">{{ $normal->id }}</a>
                                        </td>
                                        <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date)) }}
                                        </td>
                                        <td class='text-center'><span
                                                class='badge badge-warning'>{{ $normal->orderType }}</span> by
                                    @endif
                                    <span
                                        class='badge badge-{{ $normal->source == 'business' || $normal->source == 'merchant' ? 'warning' : 'success' }}'>{{ $normal->source }}</span>
                                    </td>
                                    <td>
                                        <span class='badge badge-danger'>{{ $normal->bookingStatus }}</span> -
                                        @if (strcasecmp($normal->paymentType, 'online') == 0)
                                            <span class='badge badge-info'>online</span> -
                                        @else
                                            <span class='badge badge-primary'>cash</span> -
                                        @endif

                                        @if (strcasecmp($normal->paymentTarget, 'merchant') == 0)
                                            <span class='badge badge-danger'>Merchant</span>
                                        @else
                                            <span class='badge badge-success'>bookTou</span>
                                        @endif
                                    </td>
                                    <td class='text-right'>0 %</td>
                                    <td class='text-right'>0.00</td>
                                    <td class='text-right'>0.00</td>
                                    <td class='text-right'>0.00</td>
                                    <td class='text-right'>0.00</td>
                                    <td class='text-right'>0.00</td>
                                </tr>
                            @endif
                        @endforeach

                        <tr>
                            <th>Order No</th>
                            <th>Date</th>
                            <th>Order Type | Order Source</th>
                            <th class='text-center'>Status | Pay Mode | Payment Target</th>
                            <th class='text-right'>Comission %</th>
                            <th class='text-right'>Sale Amount</th>
                            <th class='text-right'>Comission</th>
                            <th class='text-right'>Delivery Fee</th>
                            <th class='text-right'>Due On Merchant</th>
                            <th class='text-right'>Due On bookTou</th>
                            <th class='text-right'>Total Earning</th>
                        </tr>
                        <tr>
                            <th colspan='6' class='text-right'>Sum Totals</th>
                            <th class='text-right'>{{ number_format($totalSale, 2) }}</th>
                            <th class='text-right'>{{ number_format($totalCommission, 2) }}</th>
                            <th class='text-right'>{{ number_format($totalFee, 2) }}</th>
                            <th class='text-right'>{{ number_format($totalDueOnMerchant, 2) }}</th>
{{-- Check calculation needed here  --}}
                            <th class='text-right'>{{ number_format($totalDueOnBooktou, 2) }}</th>
                            <th class='text-right'>{{ number_format($totalEarning, 2) }}</th>

                        </tr>
                    <tfoot>

                    </tfoot>
                    </tbody>
                </table>
                <hr />
                <form>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label col-form-label-sm text-right">Collected at
                            Merchant:</label>
                        <div class="col-sm-2">
                            <input type="email" class="form-control form-control-sm" readonly
                                value="{{ number_format($totalCollectedAtMerchant, 2) }}">
                        </div>
                        <label class="col-sm-2 col-form-label col-form-label-sm text-right">Due towards
                            Merchant:</label>
                        <div class="col-sm-2">
                            <input type="email" class="form-control form-control-sm" readonly
                                value="{{ number_format($totalDueOnMerchant, 2) }}">
                        </div>

                        <label class="col-sm-2 col-form-label col-form-label-sm text-right">Commission to
                            deduct:</label>
                        <div class="col-sm-2">
                            <input type="email" class="form-control form-control-sm" readonly
                                value="{{ number_format($totalCommission, 2) }}">
                        </div>


                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label col-form-label-sm text-right">Cash Collected at
                            bookTou:</label>
                        <div class="col-sm-2">
                            <input type="email" class="form-control form-control-sm" readonly
                                value="{{ number_format($totalCash, 2) }}">
                        </div>

                        <label class="col-sm-2 col-form-label col-form-label-sm text-right">Online Collected at
                            bookTou:</label>
                        <div class="col-sm-2">
                            <input type="email" class="form-control form-control-sm" readonly
                                value="{{ number_format($totalOnline, 2) }}">
                        </div>

                        <label class="col-sm-2 col-form-label col-form-label-sm text-right">Total To Clear:</label>
                        <div class="col-sm-2">
                            <input type="email" class="form-control form-control-sm is-valid" readonly
                                value="{{ number_format($totalCash + $totalOnline - $totalCommission - $totalDueOnMerchant, 2) }}">
                        </div>

                    </div>


                </form>

            </div>
        </div>

    </div>
</div>
</div>




@endsection



@section('script')
<script>
    $(document).on("click", ".btn_add_promo", function() {

        $("#bin").val($(this).attr("data-bin"));

        $("#modal_add_promo").modal("show")

    });





    $('body').delegate('.btnToggleBlock', 'click', function() {

        var bin = $(this).attr("data-id");
        var status = $(this).is(":checked");
        var json = {};
        json['bin'] = bin;
        json['status'] = status;




        $.ajax({
            type: 'post',
            url: api + "v3/web/business/toggle-business-block",
            data: json,
            success: function(data) {
                data = $.parseJSON(data);

            },
            error: function() {
                $(".loading_span").html(" ");
                alert('Something went wrong, please try again');
            }

        });



    })






    $('body').delegate('.btnToggleClose', 'click', function() {

        var bin = $(this).attr("data-id");
        var status = $(this).is(":checked");

        var json = {};
        json['bin'] = bin;
        json['status'] = status;


        $.ajax({
            type: 'post',
            url: api + "v3/web/business/toggle-business-open",
            data: json,
            success: function(data) {
                data = $.parseJSON(data);
            },
            error: function() {
                $(".loading_span").html(" ");
                alert('Something went wrong, please try again');
            }

        });



    })


    $(function() {
        $('.calendar').pignoseCalendar({
            format: 'DD-MM-YYYY'
        });
    });



    $(document).on("click", ".btn-print", function() {
        window.open($(this).attr("data-pdfurl"), "popupWindow", "width=600,height=600,scrollbars=yes");
    });
</script>
@endsection
