@extends('layouts.admin_theme_hollow')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
    $cyclesize = count($cycles) - 1;

?>


  <div class="row">
     <div class="col-md-12"> 
     <div class="card ">

       <div class="card-body" style='overflow-x: scroll;'>    

         <h4>Sales report for <span class='button button-info'><a href="{{ URL::to('/admin/customer-care/business/view-profile') }}/{{$business->id}}" target='_blank'>{{ $business->name }}</a></span> for 
              @foreach($cycles as $item)
                @switch($item)
                  @case(1)
                    <span class='badge badge-info badge-pill'>First Cycle</span> 
                  @break
                  @case(2)
                    <span class='badge badge-primary badge-pill'>Second Cycle</span> 
                  @break
                  @case(3)
                    <span class='badge badge-warning badge-pill'>Third Cycle</span> 
                  @break
                  @case(4)
                    <span class='badge badge-danger badge-pill'>Fourth Cycle</span> 
                  @break
                  @case(5)
                    <span class='badge badge-success badge-pill'>Last Cycle</span> 
                  @break 
                @endswitch
              @endforeach 

              @if(count($results) > 0)
<a  href="#" data-pdfurl="{{  URL::to('/admin/billing-and-clearance/generate-clearance-bill') }}?bin={{$business->id}}&start={{ $start }}&end={{ $end }}&month={{ $month }}&year={{ $year }}&format=a4&download=no"
 class='btn btn-success btn-print btn-xs'  title='Click to print'><i class='fa fa-print'></i> Print A4</a> 
 <a  href="#" data-pdfurl="{{ URL::to('/admin/billing-and-clearance/generate-clearance-bill') }}?bin={{$business->id}}&start={{ $start }}&end={{ $end }}&month={{ $month }}&year={{ $year }}&format=roll"
 class='btn btn-info btn-print btn-xs'  title='Click to print'><i class='fa fa-print'></i> Print Roll</a>

<a target='_blank' href ="{{  URL::to('/admin/billing-and-clearance/generate-clearance-bill') }}?bin={{$business->id}}&start={{ $start }}&end={{ $end }}&month={{ $month }}&year={{ $year }}&format=a4&download=yes"
 class='btn btn-warning  btn-print btn-xs'  title='Click to download'><i class='fa fa-download'></i> Download Report</a> 
 
 <button type='button' class='btn btn-info btn-xs showclrform'  title='Click to add clearance details'><i class='fa fa-file-text'></i> Add Clearance Info</button> 


@endif
            </h4>
            <hr/>
    <table class="table table-bordered" max-width='960px;'>
      <thead>
        <tr>
          
          <th colspan="12  ">
            <form class="form-inline" method="post" action="{{ action('Admin\AccountsAndBillingController@salesAndClearanceReport') }}"   >
     {{ csrf_field() }}
  <div class="form-row  "> 
 <div class="col">
  <select name ="bin" class="selectize" id="bin"  >
                      @foreach($businesses as $bitem)
                        @if($business->id == $bitem->id )
                          <option value="{{ $bitem->id  }}" selected>{{ $bitem->name }} ({{ $bitem->frno }})</option>
                        @else
                          <option value="{{ $bitem->id  }}" selected>{{ $bitem->name }} ({{ $bitem->frno }})</option>
                        @endif
                      @endforeach
                    </select>
      </div>              
    <div class="col">

   <div class="multiselect">

    <div class="dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Select Cycle
    </button>
    <div class="dropdown-menu  " aria-labelledby="dropdownMenuButton">
      <ul  class="list-group list-group-flush">

        <li class="list-group-item"><label  for="cycleone "><input type="checkbox" id="cycleone" name='cycle[]' value='1' 
          <?php  if( in_array( 1,  $cycles )  ) echo "checked"; ?>/>1st Week</label></li>
       <li class="list-group-item"><label  for="cycletwo"><input type="checkbox" id="cycletwo" name='cycle[]'   value='2' 
          <?php  if( in_array( 2,  $cycles )  )   echo "checked"; ?>/>2nd Week</label></li>
       <li class="list-group-item"><label  for="cyclethree"><input type="checkbox" id="cyclethree" name='cycle[]'  value='3'  
          <?php if( in_array( 3,  $cycles )  )  echo "checked"; ?>/>3rd Week</label></li>
       <li class="list-group-item"><label for="cyclefour"><input type="checkbox" id="cyclefour" name='cycle[]'   value='4' 
          <?php if( in_array( 4,  $cycles )  )  echo "checked"; ?>/>4th Week</label></li>
      <li class="list-group-item"><label for="cyclelast"><input type="checkbox" id="cyclelast"  name='cycle[]'  value='5' 
          <?php if( in_array( 5,  $cycles )  )  echo "checked"; ?>/>Last Week</label></li>
    </div>
  </div>


  
  </div>
  
    </div>
   <div class="col">

      <label class="sr-only" for="inlineFormInputGroup">Month</label>
       <select name='month' class="form-control form-control-sm  ">
          <option <?php if( $month  == 1 ) echo "selected"; ?> value='1'>January</option>
          <option <?php if( $month== 2 ) echo "selected"; ?>  value='2'>February</option>
          <option <?php if( $month == 3 ) echo "selected"; ?> value='3'>March</option>
          <option <?php if( $month== 4 ) echo "selected"; ?> value='4'>April</option>
          <option <?php if( $month == 5 ) echo "selected"; ?> value='5'>May</option>
          <option <?php if( $month == 6 ) echo "selected"; ?> value='6'>June</option>
          <option <?php if( $month == 7 ) echo "selected"; ?> value='7'>July</option>
          <option <?php if( $month == 8 ) echo "selected"; ?> value='8'>August</option>
          <option <?php if( $month == 9 ) echo "selected"; ?> value='9'>September</option>
          <option <?php if($month == 10 ) echo "selected"; ?> value='10'>October</option>
          <option <?php if( $month== 11 ) echo "selected"; ?> value='11'>November</option>
          <option <?php if( $month == 12 ) echo "selected"; ?> value='12'>December</option> 
        </select>
   
      <label class="sr-only" for="inlineFormInputGroup">Year</label>
      <select name='year' class="form-control form-control-sm">
            <?php 
            for($i=date('Y'); $i >= 2020; $i--)
            {
                echo "<option value='$i'>" . $i . "</option>";
            }
            ?> 
        </select>  
     
       <button type="submit" class="btn btn-primary btn-xs" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>
  </div>
  </form>

          </th>
         
        </tr>
        <tr>
          <th>Order No</th> 
          <th>Date</th> 
          <th>Order Type | Source</th>
          <th class='text-center'>Status | Pay Mode | Payment Target</th>
          <th class='text-right'>Comission %</th>
          <th class='text-right'>Sale Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Comission</th>
          <th class='text-right'>Delivery + sev fee</th> 
          <th class='text-right'>Due on Merchant</th>
          <th class='text-right'>Due on Booktou</th>
          <th class='text-right'>Total Earning</th> 
        </tr>
      </thead>
      
      <tbody>
        @php 
          $packagingCost = $commission = $dueOnMerchant =$dueOnBooktou = $deliveryFee = $orderCost = $earning = $commissionPc = 0.00;
          $totalCash = $totalOnline =  $totalCollectedAtMerchant = $totalDueOnMerchant =$totalDueOnBooktou =$totalSale = $totalFee = $totalCommission = $totalEarning = $totalDeliveryFee = $totalOnlineMerchant = $totalPackagingCost =0.00; 
        @endphp

          @foreach ($results as $normal)
           @if($normal->bookingStatus == "delivered") 

            <tr>
{{--Normal Order Starts Here --}}              
                
                @if($normal->orderType == "normal")
                  @php 
                    $tempPackagingCost = 0.00;
                  @endphp
                  @foreach($cart_items as $cart)
                    @if($normal->id == $cart->order_no)
                      
                      @php 
                        $tempPackagingCost += $cart->qty * $cart->package_charge;
                      @endphp

                    @endif
                  @endforeach

                  @php 
                      $normal->packingCost  = $tempPackagingCost;
                  @endphp

                  <td><a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td> 
                  <td class='text-center'><span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
                  </td> 
                  <td class='text-center'>  
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0)
                     <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                     <span class='badge badge-primary'>CASH</span> - 
                    @endif 
                    <span class='badge badge-success'>bookTou</span>
                  </td>
   

                  @php
                      $dueOnMerchant = $dueOnbooktou = 0.00;
                      $commissionPc = $business->commission ; 
                      $orderCost = $normal->orderCost;
                      $packagingCost = $normal->packingCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalPackagingCost += $packagingCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning;  

                  @endphp
 

                  @if( strcasecmp($normal->paymentType , "online") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                    @php 
                      $totalOnline += $normal->orderCost  ;  //packing cost already included
                    @endphp
                  @else 
                    @php
                      $totalCash += $normal->orderCost  ;  //packing cost already included  
                    @endphp
                  @endif 


                  <td class='text-right'>{{ $commissionPc }} %</td> 
                  <td class='text-right'>{{ number_format( $orderCost, 2 )   }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $deliveryFee, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $dueOnMerchant, 2 ) }}</td> 
                  <td class='text-right'>{{ number_format( $dueOnbooktou, 2 ) }}</td>
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td> 

{{--Pnd Starts here --}}
                @elseif($normal->orderType == "pnd")

                  <td><a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                   <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-primary'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>
                      {{  ($normal->source=="business" || $normal->source=="merchant" ) ? "Merchant" : "Customer" }}
                    </span>
                  </td>
                  <td class='text-center'>  
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                     <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                     <span class='badge badge-primary'>CASH</span> - 
                    @endif 
                    

                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                     <span class='badge badge-warning'>Merchant</span>
                    @else 
                        @if( strcasecmp($normal->paymentType , "ONLINE") == 0 &&  ( $normal->source == "business" || $normal->source == "merchant" )  )
                            <span class='badge badge-danger'>bookTou</span>
                         @else 
                            <span class='badge badge-success'>bookTou</span>
                         @endif 
                    @endif


                  </td>
 

                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)
 
                    @php
                      $dueOnMerchant = $dueOnbooktou = 0.00;       
                      $commissionPc = $business->commission  ; 
                      $orderCost = $normal->orderCost;
                      $packagingCost = $normal->packingCost;
                      $deliveryFee = $normal->deliveryCharge + $normal->service_fee;
                      $serviceFee = $normal->service_fee;
                      $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                      $earning =  $deliveryFee +  $serviceFee ;  
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning; 
                    @endphp 

                    @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 ) 
                          @php 
                            $deliveryFee = $normal->deliveryCharge + $normal->service_fee;
                            $dueOnMerchant  = $normal->deliveryCharge + $normal->service_fee; 
                            $totalCollectedAtMerchant += $normal->orderCost;  
                            $totalPackagingCost += $packagingCost;
                            $totalDueOnMerchant += $dueOnMerchant; 
                            $totalOnlineMerchant += $normal->orderCost + $normal->packingCost;
                            $dueOnbooktou = 0.00;
                          @endphp
                        @else
                            @php
                              $totalOnline += $normal->orderCost + $normal->packingCost;
                            @endphp  
                        @endif  
                   @else
                        @php 
                          $dueOnMerchant  = 0.00;
                          $totalCash += $normal->orderCost + $normal->packingCost;
                          $dueOnbooktou = $normal->orderCost;
                          $totalDueOnBooktou += $dueOnbooktou; 
                        @endphp     
                    @endif  
{{--source is merchant --}}
                  @else 
  
                      @php
                            $commissionPc = 0.00 ; 
                            $orderCost = $normal->orderCost;
                            $packagingCost = $normal->packingCost;
                            $deliveryFee = $normal->deliveryCharge + $normal->service_fee;
                            $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                            $earning =  $deliveryFee +  $commission;
                            $totalSale += $orderCost;
                            $totalPackagingCost += $packagingCost;
                            $totalFee += $deliveryFee;
                            $totalCommission += $commission;
                            $totalEarning += $earning;  
                      @endphp 

                    @if( strcasecmp($normal->paymentType , "online") == 0  )   
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 )  
                          @php
                             $dueOnbooktou = 0.00;
                             $totalOnlineMerchant += $normal->orderCost;  
                             $totalCollectedAtMerchant += $normal->orderCost; 
                             $totalPackagingCost += $normal->packingCost; 
                             $dueOnMerchant  = $normal->deliveryCharge + $normal->service_fee;
                             $totalDueOnMerchant += $dueOnMerchant ; 
                               
                          @endphp  
                        @else
                          @php
                            $dueOnMerchant = $dueOnbooktou = 0.00; 
                            $totalOnline += $normal->orderCost + $normal->packingCost;
                          @endphp 
                        @endif 
                    @else
                        @php 
                          $dueOnMerchant  = 0.00;
                          $totalCash += $normal->orderCost + $normal->packingCost;
                          $dueOnbooktou = $orderCost;
                          $totalDueOnBooktou += $dueOnbooktou;
                        @endphp     
                    @endif   

                  @endif 
                   
                  <td class='text-right'>{{ $commissionPc }} %</td> 
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format($commission,2) }}</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td>
                  <td class='text-right'>{{ number_format($dueOnbooktou, 2 ) }}</td> 
                  <td class='text-right'>{{ number_format($earning, 2 )  }}</td>  
{{-- Assist starts here --}}   


                @elseif($normal->orderType == "assist")

                  <td><a target='_blank' href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
                  </td>
                  <td class='text-center'>
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> -  
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                      <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                      <span class='badge badge-primary'>CASH</span> - 
                    @endif
                  
                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                      <span class='badge badge-danger'>Merchant</span>
                    @else 
                      <span class='badge badge-success'>bookTou</span>
                    @endif  
                  </td> 



                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                    @php
                      $dueOnMerchant =0.00;
                      $totalDueOnMerchant = 0.00;
                      $totalCollectedAtMerchant = 0.00;
                      $packagingCost = 0.00;                  
                    @endphp
                      
                    @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0 || strcasecmp($normal->paymentTarget , "business") == 0) 
                          @php
                            $dueOnMerchant = $normal->deliveryCharge;
                            $totalCollectedAtMerchant += $normal->orderCost;  
                            $totalDueOnMerchant += $dueOnMerchant; 
                            $totalOnlineMerchant += $totalCollectedAtMerchant;  
                          @endphp 
                        @else
                          @php
                            $totalOnline += $normal->orderCost;
                          @endphp  
                        @endif
                    @else
                          @php 
                            $totalCash += $normal->orderCost;  
                          @endphp 
                        
                    @endif

                    @php 
                      $commissionPc =  $business->commission  ; 
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge + $normal->service_fee;
                      $commission = ( $commissionPc * $orderCost ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning; 
                    @endphp 
 

                  @else 


                    @php
                      $dueOnMerchant =0.00;
                      $commissionPc =  0.00 ; 
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge + $normal->service_fee;;
                      $commission = ( $commissionPc * $orderCost ) / 100 ;
                      $earning =  $deliveryFee +  $commission; 
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning;
                      $totalDueOnMerchant = 0.00;
                      $totalCollectedAtMerchant = 0.00; 

                    @endphp 


                  @endif

                  <td class='text-right'>{{ $commissionPc }} %</td> 
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format($commission,2) }}</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td>
                  <td class='text-right'>{{ number_format( $dueOnbooktou, 2 ) }}</td>
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td>  

                @endif   

              </tr>

           @else

            <tr> 
              
                @if($normal->orderType == "normal")
                  <td><a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td> 
                  <td class='text-center'><span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                @elseif($normal->orderType == "pnd")
                  <td><a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-info'>{{ $normal->orderType  }}</span> by 
                @else
                  <td><a target='_blank'  href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                @endif 
                <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
              </td>
              <td  class='text-center'>
                <span class='badge badge-danger'>{{ $normal->bookingStatus }}</span> - 
                @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                  <span class='badge badge-info'>ONLINE</span> - 
                @else 
                  <span class='badge badge-primary'>CASH</span> - 
                @endif
              
                @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                  <span class='badge badge-danger'>Merchant</span>
                @else 
                  <span class='badge badge-success'>bookTou</span>
                @endif
              </td>
              <td class='text-right'>0 %</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td>  
          </tr> 

           @endif
  
        @endforeach
     
        <tr>
          <th>Order No</th> 
          <th>Date</th>
          <th>Order Type | Source</th>
          <th class='text-center'>Status | Pay Mode | Payment Target</th>
          <th class='text-right'>Commission %</th>
          <th class='text-right'>Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Commission</th>
          <th class='text-right'>Delivery</th> 
          <th class='text-right'>Due on Merchant</th>
          <th class='text-right'>Due on booktou</th>
          <th class='text-right'>Earning</th> 
        </tr>  
        <tr>
          <th colspan='4' class='text-right' style='color:red'><i>i) F=C+D; ii) D includes E</i> </th> 
            <th   class='text-right'>Total</th> 
            <th class='text-right'>A = {{ number_format($totalSale,2, '.', '') }}</th>
            <th class='text-right'>B = {{ number_format($totalPackagingCost,2, '.', '') }}</th>
            <th class='text-right'>C = {{ number_format($totalCommission,2, '.', '') }}</th> 
            <th class='text-right'>D = {{ number_format($totalFee,2, '.', '') }}</th> 
            <th class='text-right'>E = {{ number_format($totalDueOnMerchant,2, '.', '') }}</th>
            <th class='text-right'>F = {{ number_format($totalDueOnBooktou,2, '.', '') }}</th> 
            <th class='text-right'>G = {{ number_format($totalEarning,2, '.', '') }}</th>  
        </tr> 
<tfoot>
        
</tfoot> 
      </tbody> 
    </table> 
<hr/>
<form >
  {{ csrf_field() }}
    <div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Collected at Merchant (G = A-H-I ):</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm" readonly value="{{ number_format( $totalCollectedAtMerchant ,2, '.', '') }}"  >
    </div> 
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Due towards Merchant (E):</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm" readonly value="{{ number_format( $totalDueOnMerchant ,2, '.', '') }}"  >
    </div>

     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Commission to deduct (C):</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm" readonly value="{{ number_format( $totalCommission ,2, '.', '') }}"  >
    </div>


  </div>

<div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Cash to bookTou (H):</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm" readonly value="{{ number_format( $totalCash ,2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Online to bookTou (I):</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm" readonly value="{{ number_format( $totalOnline ,2, '.', '') }}"  >
    </div>

     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Total To Clear (J=H+I-E-C):</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm is-valid" readonly value="{{ number_format( ( $totalCash + $totalOnline - $totalCommission - $totalDueOnMerchant)  ,2, '.', '') }}"  >
    </div> 
  </div> 
 

</form>

 </div> 
  </div> 
  
  </div> 
</div>
 </div>
 
 
<form   method="post" action="{{ action('Admin\AccountsAndBillingController@saveClearanceInformation') }}"   >
     {{ csrf_field() }}
 <div class="modal" id='wdtclearance' tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Save Clearance Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       

  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="weekno">Week No.</label>
      <select class="form-control" id="weekno" name="weekno">
          <option value='1'>First Week</option>
          <option  value='2'>Second Week</option>
          <option  value='3'>Third Week</option>
          <option  value='4'>Fourth Week</option> 
          <option  value='5'>Last Week</option> 
      </select> 
    </div>

    <div class="form-group col-md-4">
      <label for="month">Select Month</label> 
       <select name='month' id='month' class="form-control form-control-sm  "  >
            <option <?php if( $month  == 1 ) echo "selected"; ?> value='1'>January</option>
            <option <?php if( $month== 2 ) echo "selected"; ?>  value='2'>February</option>
            <option <?php if( $month == 3 ) echo "selected"; ?> value='3'>March</option>
            <option <?php if( $month== 4 ) echo "selected"; ?> value='4'>April</option>
            <option <?php if( $month == 5 ) echo "selected"; ?> value='5'>May</option>
            <option <?php if( $month == 6 ) echo "selected"; ?> value='6'>June</option>
            <option <?php if( $month == 7 ) echo "selected"; ?> value='7'>July</option>
            <option <?php if( $month == 8 ) echo "selected"; ?> value='8'>August</option>
            <option <?php if( $month == 9 ) echo "selected"; ?> value='9'>September</option>
            <option <?php if($month == 10 ) echo "selected"; ?> value='10'>October</option>
            <option <?php if( $month== 11 ) echo "selected"; ?> value='11'>November</option>
            <option <?php if( $month == 12 ) echo "selected"; ?> value='12'>December</option> 
          </select>
    </div> 

<div class="form-group col-md-4">
      <label for="year">Year</label> 
        <input   name='year'  id='year'  class="form-control form-control-sm " value="{{ date('Y') }}" /> 

    </div> 
  

</div>
  <div class="form-row">

    <div class="form-group col-md-6">
      <label for="amount">Amount Cleared:</label>
      <input type="text" class="form-control" id="amount" name="amount" value="{{ number_format( ( $totalCash + $totalOnline - $totalCommission - $totalDueOnMerchant)  ,2, '.', '') }}">
    </div>
    <div class="form-group col-md-6">
      <label for="paymode">Payment Mode</label>
      <select class="form-control" id="paymode" name="paymode">
          <option>UPI</option>
          <option>Google Pay</option>
          <option>Online Transfer</option>
          <option>Cash</option> 
      </select> 
    </div>
  </div>


   <div class="form-row">
   <div class="form-group col-md-6">
    <label for="refno">Transaction Reference No.</label>
    <input type="text" class="form-control" id="refno" name="refno" placeholder="Reference no (if any)">
  </div> 

  <div class="form-group col-md-6">
    <label for="clearedon">Clearance Date</label>
    <input type="text" class="form-control calendar" id="clearedon" name="clearedon" placeholder="Clerance Date">
  </div> </div>
  <div class="form-group">
    <label for="remarks">Remarks (optional)</label>
    <textarea name="remarks" class="form-control" rows="4" id="remarks" placeholder="Clerance remarks"></textarea>
  </div>
  <div class="form-group">
    <label for="sms">Clearance SMS</label>
    <input type="checkbox" name="clearance_sms" class="form-check"> 
  </div>
</div>
      
      <div class="modal-footer">
        <input type="hidden" name="bin" value="{{ $business->id }}">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button  name="btnsubmit" value="submit"   class="btn btn-primary">Save changes</button>
      </div>


    </div>
  </div>
</div>
</form>


 
@endsection



@section("script")

<style>
.multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}

</style>

<script>

var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}


$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    }); 

});

    

      $('.selectize').select2({
        selectOnClose: true
      });


      $(".selectize").val({{ $bin }}).trigger('change');


$(document).on("click", ".btn-print", function()
{
  window.open($(this).attr("data-pdfurl"), "popupWindow", "width=600,height=600,scrollbars=yes"); 
});


$(".showclrform").on('click', function(){
    $("#wdtclearance").modal('show'); 
})

</script>  


@endsection

