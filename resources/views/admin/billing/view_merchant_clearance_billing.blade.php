@extends('layouts.admin_theme_02')
@section('content')
<?php 
  $cdn_url =    config('app.app_cdn') ;  
  $cdn_path =     config('app.app_cdn_path') ;
  ?> 

<div class="row">
   <div class="col-md-12">

   <div class="card card-default">
         <div class="card-header"> 
               <div class="row">

                 <div class="col-md-6">
                <h4 style="color: firebrick;" >
                 Dues for merchants
     @if(isset($from_date) && isset($upto_date))
      <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
        <button type="button" class="btn btn-primary">Date Range</a>
        <button type="button" class="btn btn-info">{{$from_date}}</a>
          <button type="button" class="btn btn-info">{{$upto_date}}</a>

      </div> 
      @else
      <div class="btn-group btn-group-sm"; role="group" aria-label="Basic example">
        <button type="button" class="btn btn-primary">Please Select</button> 
        <button type="button" class="btn btn-info">Date Range</button>
         
      </div> 
      @endif

                </h4>

              </div>

<div class="col-md-6 text-center"> 
<form class="form-inline" method="get" action="{{ action('Admin\AccountsAndBillingController@merchantClearanceBillingReports') }}"   >
   

  <label class="my-1 mr-2 btn btn-primary" for="inlineFormCustomSelectPref">From:</label>
      <input style="width:5rem;" type="text" class='form-control form-control-sm border-primary custom-select my-1 mr-sm-2 calendar' value="{{ date('d-m-Y', strtotime($from_date))  }}" name='from_date' />

      <label class="my-1 mr-2 btn btn-primary" for="inlineFormCustomSelectPref"> To :</label> 
      <input style="width:5rem;" type="text" class='form-control form-control-sm border-primary custom-select my-1 mr-sm-2 calendar'  
      value="{{ date('d-m-Y', strtotime($upto_date))  }}" name='to_date' />
       <div class="col-auto">
     <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
 
</div>
</form>
</div>
</div>
</div>
          
  
  <div class="card-body">    
  <table class="table">
    <thead>
      <tr>
        <th>BIN</th>
        <th>Business</th> 
        <th>Phone</th> 
        <th>Commission</th> 
        <th class='text-right'>PnD Amount</th> 
        <th class='text-right'>Normal Sales Amount</th> 
        <th class='text-right'>Sub-Total</th> 
        <th class='text-right'>Action</th>  
      </tr>
    </thead>
    
    <tbody>
    @php
      $grossTotal =0.00;
    @endphp
        @foreach ($businesses as $item)
        <tr>
          <td>{{ $item->id  }}</td>
          <td>{{ $item->name }}</td>
          <td>{{ $item->phone_pri }}</td>
          <td>{{ $item->commission }} %</td>
            @php
                $totalPnd =0.00;
            @endphp

            @foreach ($pnd_sales as $pnd)
              @if($pnd->request_by == $item->id )
                @php
                  $totalPnd += $pnd->totalAmount;
                @endphp
                @break
              @endif 
            @endforeach

           <td class='text-right'>{{  $totalPnd }}</td> 

            @php
                $totalNormal =0.00;
            @endphp

            @foreach ($normal_sales as $normal)
              @if($normal->bin == $item->id )
                @php
                  $totalNormal += $normal->totalAmount;
                @endphp
                @break
              @endif 
            @endforeach


           <td class='text-right'>{{ $totalNormal }}</td>  
  

          @php
            $totalAmount = $totalPnd + $totalNormal; 
            $grossTotal +=$totalAmount;
          @endphp

        <td class='text-right'>{{ $totalAmount }}</td>  

          <td class='text-right'>
              <a class="btn btn-primary btn-sm" target='_blank' 
              href="{{ URL::to('/admin/billing-and-clearance/generate-sales-and-merchant-clearance-report')}}?bin={{$item->id}}&from={{$from_date}}&to={{$upto_date}}">View Details</a>
          </td>
        </tr>
      @endforeach  
      <tr>
        <th colspan='6' class='text-right'>Total:</th>
        <th class='text-right'>{{ $grossTotal }}</th>  
        <th></th>
      </tr> 
    </tbody>

  </table>
</div>


</div> 

</div> 
</div> 

@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

  $("#bin").val($(this).attr("data-bin"));

    $("#modal_add_promo").modal("show")

});





$('body').delegate('.btnToggleBlock','click',function(){

 var bin  = $(this).attr("data-id"); 
 var status = $(this).is(":checked"); 
var json = {};
  json['bin'] =  bin ;
  json['status'] =  status ;




 $.ajax({
    type: 'post',
    url: api + "v3/web/business/toggle-business-block" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);    
          
    },
    error: function( ) 
    {
      $(".loading_span").html(" "); 
      alert(  'Something went wrong, please try again'); 
    } 

  });



})






$('body').delegate('.btnToggleClose','click',function(){

 var bin  = $(this).attr("data-id"); 
 var status = $(this).is(":checked"); 

  var json = {};
  json['bin'] =  bin ;
  json['status'] =  status ;


 $.ajax({
    type: 'post',
    url: api + "v3/web/business/toggle-business-open" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);        
    },
    error: function( ) 
    {
      $(".loading_span").html(" "); 
      alert(  'Something went wrong, please try again'); 
    } 

  });



})


$(function() {
  $('.calendar').pignoseCalendar( 
  {
    format: 'DD-MM-YYYY' 
  });
});


</script>  


@endsection

