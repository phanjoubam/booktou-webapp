<meta charset="utf-8" /> 
<link rel="icon" type="image/x-icon" href="{{ URL::to('/')  }}/favicon.ico"> 
<title>Booktou Admin Dashboard</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' /> 
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /> 
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/bootstrap.css" rel="stylesheet" />
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/font-awesome.css" rel="stylesheet" /> 
<link href="{{ URL::to('/public/assets/admin') }}/atheme/js/morris/morris-0.4.3.min.css" rel="stylesheet" /> 
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/custom-styles.css" rel="stylesheet" />  
<link rel="stylesheet" href="{{ URL::to('/public/assets/admin') }}/atheme/js/Lightweight-Chart/cssCharts.css">   
<link href="{{ URL::to('/public/assets') }}/vendor/pn/css/pignose.calendar.min.css" rel="stylesheet" /> 


<link href="{{ URL::to('/public/assets/vendor') }}/jexcel/jexcel.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::to('/public/assets/vendor') }}/jsuites/dist/jsuites.css" type="text/css" />

<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/style.css" rel="stylesheet" /> 

<style type="text/css">
      /* Set the size of the div element that contains the map */
      #agentmap {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
      }

      #map_wrapper {
    height: 600px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}



</style>