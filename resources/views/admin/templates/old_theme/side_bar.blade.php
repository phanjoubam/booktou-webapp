<div class="logo">
    
        <a href="http://booktou.in/app/admin" class="simple-text logo-normal">
           BookTou
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          
          <li class="active ">
            <a href="{{  URL::to('/' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>Dashboard</p>
            </a>
          </li>


          <li  >
            <a href="{{  URL::to('/admin/customer-care/orders/view-all' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>View All Orders</p>
            </a>
          </li>
         
          <li>
            <a href="{{  URL::to('/admin/customer-care/orders/view-completed' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>Completed Orders</p>
            </a>
          </li>


          <li>
            <a href="{{  URL::to('/admin/products/category/manage-categories' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>Add Product Category</p>
            </a>
          </li>
  


  <li>
            <a href="{{  URL::to('/admin/customer-care/business/view-all' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>View Businesses</p>
            </a>
          </li>

        <li>
            <a href="{{  URL::to('/admin/customer-care/delivery-agents' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>View Delivery Agents</p>
            </a>
          </li>
          <li>
            <a href="{{  URL::to('/admin/customer-care/business/view-promotion-businesses' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>Promotion Businesses</p>
            </a>
          </li>
          
        </ul>
      </div>