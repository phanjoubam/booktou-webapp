 <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            
            <li class="nav-item">
              <a class="nav-link" href="{{  URL::to('/admin' )}}">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Orders</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">


                  <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/orders/view-all' )}}">
             Normal Orders
            </a>
          </li>
         
          <li class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/orders/view-completed' )}}">
              Completed Normal Orders
            </a>
          </li> 

           <li class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/pick-and-drop-orders/view-all' )}}">
             Pick-And-Drop (PnD)
            </a>
          </li> 
          <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/customer-care/callback-requests' )}}">
                    Callback Requests
                  </a>
                </li>

                 <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/customer-care/quotation-requests' )}}">
                    Quotation Enquiry
                  </a>
                </li> 
                 <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/normal-orders/stats' )}}"> Order Stats Reports</a>
          </li>
          <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/orders/remarks-and-feedback' )}}"> Remarks and Feedbacks</a>
          </li>
  <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/pick-and-drop-orders/prepare-allocation-table' )}}"> Route Discovery Checker</a>
          </li>

            <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/orders/route-with-tasks-table' )}}"> Route and Tasks Table</a>
          </li>

          
                </ul>
              </div>
            </li>

            
<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-booking" aria-expanded="false" aria-controls="ui-booking">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Bookings</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-booking">
                <ul class="nav flex-column sub-menu"> 
                  <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/services/view-bookings' )}}">
                    All Bookings
                  </a>
                </li>
          

                </ul>
              </div>
   </li>


<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-02" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Businesses</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-02">
                <ul class="nav flex-column sub-menu"> 
          
         <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/view-all' )}}">
               View Businesses
            </a>
          </li>

         <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/view-promotion-businesses' )}}">
               Premium Businesses
            </a>
          </li>
<li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/normal-orders/best-performers' )}}">
              Best Performers
            </a>
          </li>
  
<li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/pnd-orders/best-performers' )}}">
              Best PnD Performers
            </a>
          </li>
 
                </ul>
              </div>
            </li>

 



<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-03" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Agents &amp; Staffs</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-03">
                <ul class="nav flex-column sub-menu"> 
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/staffs/manage-attendance' )}}">
               Staff Attendance
            </a>
          </li> 
        <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/customer-care/delivery-agents' )}}">
               View Delivery Agents
            </a>
          </li>  
 
<li class="nav-item">
            <a class="nav-link"  target='_blank' href="{{  URL::to('/admin/customer-care/delivery-agents-live-locations' )}}">
              Live Location
            </a>
          </li>

            <li class="nav-item">    
   <a class="nav-link" href="{{  URL::to('/admin/delivery-agent/all-daily-orders-report' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Delivery Report
                </a>
          </li> 



                </ul>
              </div>
            </li>

<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-cst" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Customers</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-cst">
                <ul class="nav flex-column sub-menu"> 
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/systems/search-customers' )}}">
               All Customers
            </a>
          </li> 
       

                </ul>
              </div>
            </li>


 <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-05" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Marketing &amp; Analytics</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-05">
                <ul class="nav flex-column sub-menu">  

                   <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/analytics/products/view-frequently-browsed-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Frequently Browsed Products
                </a>
              </li>
 <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/marketing/campaign-area' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View Campaign Areas
                </a>
              </li>

 <li class="nav-item">
                <a  class="nav-link"  href="{{  URL::to('/admin/systems/manage-cloud-messages' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Cloud Message
                </a>
              </li>

       <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/systems/manage-voucher-codes' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Voucher Codes
                </a>
              </li>
 <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/products/view-all-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Manage Featured Products
                </a>
              </li>

<li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/products/view-featured-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View Featured Products
                </a>
              </li>


                </ul>
              </div>
            </li>


<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-04" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Reports</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-04">
                <ul class="nav flex-column sub-menu"> 
          
         
          
 
 <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/report/monthly-earning' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Monthly Earning Report
                </a>
              </li>

  <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/report/daily-sales' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales Report
                </a>
              </li>


               <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/report/pending-payments' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Payment Due Report
                </a>
              </li>



                </ul>
              </div>
            </li>



<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-04" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Sales Stats</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-04">
                <ul class="nav flex-column sub-menu"> 
          
          
 
 <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/sales/targeted-orders-summary' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales Target
                </a>
              </li>
  

                </ul>
              </div>
            </li>



<li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/systems/manage-business-categories' )}}" >
              Systems
            </a>
          </li>


    <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/cms' )}}">
              CMS
            </a>
          </li>
          
          
          </ul>
        </nav>