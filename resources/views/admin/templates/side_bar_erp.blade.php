
 <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                     <li class="active ">
            <a href="{{  URL::to('/erp' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Dashboard
            </a>
          </li> 

            <li  >
            <a href="{{  URL::to('/erp/orders/view-all' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> View Orders
            </a>
          </li>
          <li  >
            <a href="{{  URL::to('/erp/orders/add-new-order' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Add New Order
            </a>
          </li>

          
          <li  >
            <a href="{{  URL::to('/erp/product-categories' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Product Categories
            </a>
          </li>
         
          <li>
            <a href="{{  URL::to('/erp/business/get-daily-earning' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Daily Earning
            </a>
          </li>


          <li>
            <a href="{{  URL::to('/erp/business/get-monthly-earning' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Monthly Earning
            </a>
          </li>

         
    <li><a href="#"><i class="fa fa-sitemap"></i> Reports<span class="fa arrow"></span></a>
      <ul class="nav nav-second-level">
        <li>
            <a href="{{  URL::to('/erp/orders/view-active-orders' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> View All Orders
            </a>
          </li>
        </ul>
     </li>
       
 </ul>
                   

<p class='text-center white'>All right reserved. Owned by <a href="https://booktou.in">bookTou.in</a></p> 
            </div>

        </nav>


          
        
     