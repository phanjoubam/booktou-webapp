<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
<link rel="icon" type="image/png" href="../assets/img/favicon.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Booktou Admin Dashboard</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
 

  <link href="{{ URL::to('/public/assets/admin') }}/atheme/css/bootstrap.css" rel="stylesheet" />
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/font-awesome.css" rel="stylesheet" /> 
<link href="{{ URL::to('/public/assets/admin') }}/atheme/js/morris/morris-0.4.3.min.css" rel="stylesheet" /> 
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/custom-styles.css" rel="stylesheet" />  
<link rel="stylesheet" href="{{ URL::to('/public/assets/admin') }}/atheme/js/Lightweight-Chart/cssCharts.css">  
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/style.css" rel="stylesheet" /> 

<link href="{{ URL::to('/public/assets') }}/vendor/pn/css/pignose.calendar.min.css" rel="stylesheet" /> 