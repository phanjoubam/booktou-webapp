<?php 
    $total =0;  
?>

@extends('layouts.admin_theme_02')
@section('content')
 

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
         
                <div class="row">
          <div class="col-md-8">
            <h5>View Daily Orders Delivery Reports for <span class='badge badge-warning'>{{ date('d-m-Y', strtotime($data['date'])) }}</span></h5>
           </div>
    <div class="col-md-4 text-right">
         <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@allDeliveryAgentDailyOrdersReport') }}"> 
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' name='filter_date' />  <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
            </form>
      </div>
   
    </div>
  </div>
 </div>
  
   <div class="card "> 
       <div class="card-body"> 
    @foreach($data['agents'] as $agent) 
      <table class="table table-bordered mb-3"> 
          <tr class="text-left" style='font-size: 14px'>  
            <th scope="col" colspan="7" style='background-color: #343434; color: #fff'><h4>Orders Delivery Reports for {{ $agent->fullname  }}</h4></th> 
          </tr>  
          <tr class="text-center" style='font-size: 12px'>  
            <th scope="col" class="text-center" >Order No.</th> 
            <th scope="col" class="text-left">Merchant/Customer</th>
            <th scope="col" class="text-center" >Delivery Date</th>
            <th class='text-right'>Order Type</th>    
            <th class="text-center">Order Status</th> 
            <th class='text-center'>Payment Type</th>                    
            <th class="text-right">Amount Collected</th> 
          </tr>
            
            @php
              $i=1; 
              $totalcashamount = $totalonlineamount = 0.0; 
            @endphp 

            @foreach ($data['all_orders'] as $item)
              @php 
                $amountCollected = 0.00;
              @endphp 
              @if($item->member_id == $agent->id )
                <tr >
                  <td class="text-center">
                    @if($item->orderType == "normal")
                    <a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{ $item->orderNo }}">{{ $item->orderNo }}</a> 
                    @elseif($item->orderType == "pnd")
                    <a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                    @else
                    <a target='_blank'  href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                  @endif
                </td> 
                <td>{{$item->orderBy}}</td>
                <td>{{ date('d-m-Y', strtotime($item->service_date))}}</td>  
                <td class='text-center'>
                    <span class='badge badge-success'>{{ $item->orderType }}</span>
                </td>
                <td class="text-center"> 
                      @switch($item->orderStatus)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-info'>In Route</span>
                      @break

                      @case("completed")
                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
 
                        <?php 

                          if(  $item->source == "booktou" || $item->source == "customer"  )
                          {
                              if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00;
                                $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                              else //cash collection
                              {
                                $amountCollected = $item->totalAmount + $item->serviceFee ;
                                $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                          }
                          else if(  $item->source == "business" || $item->source == "merchant"  )
                          {
                            if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00; 
                              }
                              else //cash collection
                              {
                                $amountCollected = $item->totalAmount + $item->serviceFee ;
                                $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                          } 

                         ?>
 
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>   
                    <td class='text-center'>
                      
                        @switch( strtolower($item->payMode) ) 
                          @case("cash on delivery")
                            <span class='badge badge-success'>Cash</span>
                            @break
                          @case("cash")
                            <span class='badge badge-success'>Cash</span>
                            @break

                          @case("online")
                            <span class='badge badge-warning'>Online</span>
                            @break

                          @case("pay online (upi)")
                            <span class='badge badge-warning'>Online</span>
                            @break

                          @other
                            <span class='badge badge-success'>Cash</span>
                            @break

                        @endswitch 
                    </td>
                   
                    <td class='text-right'>{{ number_format( $amountCollected, 2, '.', '')  }}</td>
                  </tr>

                  @endif

                @php
                    $i++;
                @endphp
            @endforeach
            
            <tr class="text-center" style='font-size: 12px'>  
              <th scope="col" class="text-center" >Order No.</th>
              <th scope="col" class="text-left">Merchant/Customer</th>
              <th scope="col" class="text-center" >Delivery Date</th>
              <th class='text-right'>Order Type</th>    
              <th class="text-center">Order Status</th> 
              <th class='text-right'>Payment Type</th>                    
              <th class="text-right">Amount Collected</th> 
            </tr>
            <tr  style='font-size: 12px'>  
              <th scope="col" colspan="5" class="text-right">
                <h4>Agent Collection {{ $agent->fullname  }}</h4></th>
              <th scope="col" colspan="3"  >

                   <div class="form-group row">
                    <label  class="col-sm-6 col-form-label col-form-label-sm text-right">Cash to collect:</label>
                    <div class="col-sm-6">
                      <input type="email" class="form-control form-control-sm is-valid" readonly value="{{ number_format( $totalcashamount ,2, '.', '') }}"  >
                    </div> 
                    
                  </div>
                </th>  
              </tr>     
       </table>
    @endforeach
     </div>
    </div>
 </div> 
</div>  

@endsection


 @section("script")

<script>
  
 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 