<?php 
    $total =0;   
 	$agent =  $data['agent'] ; 

?>
@extends('layouts.admin_theme_02')
@section('content')
 

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
         
                <div class="row">
          <div class="col-md-8">
            <h5>View Daily Orders Delivery Reports for <span class='badge badge-warning'>{{ date('d-m-Y', strtotime($data['date'])) }}</span></h5>
           </div>
    <div class="col-md-4 text-right">
         <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@prepareDailyCollection') }}">
            {{ csrf_field() }}
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' name='filter_date' /> 
 
 
                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
                <input type="hidden"   name='ai' value="{{$agent->id}}" /> 
            </form>
      </div>
   
    </div>
  </div>
 </div>
  
 </div>
   <div class="col-md-8">
   	<div class="card mt-3">
       <div class="card-header">
       	<div class="row">
            <div class="col-md-8">
          		<h5>Orders Delivery Reports for <span class='badge badge-primary'>{{ $agent->fullname  }}</span></h5>
   			</div>
    		<div class="col-md-4 text-right">  </div> 
	    </div>
	  </div>

       <div class="card-body">  
          <table class="table">
              <thead class=" text-primary">  
                  <tr class="text-center" style='font-size: 12px'>  
                    <th scope="col">Order No.</th>
                    <th scope="col">Merchant/Customer</th>  
                    <th class="text-center">Order Status</th> 
                    <th class='text-right'>Payment Type</th>  
                    <th class='text-right'>Order Type</th>  
                    <th class="text-right">Amount</th> 
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1; 
                $totalcashamount = $totalonlineamount = 0.0;
                foreach ($data['all_orders'] as $item)
                {  

                ?>  
               

                  <tr id="tr{{$item->orderNo }}">  
                   <td>
                    <strong>{{$item->orderNo}}</strong>
                  </td>
 
                    <td>{{$item->orderBy}}</td>  
                 
                     <td class="text-center">

                      @switch($item->orderStatus)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-info'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                        <?php 

                          if(strcasecmp($item->payMode, "cash") == 0 ||strcasecmp($item->payMode, "Cash on delivery") == 0  )
                          {
                              $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                          }
                          else 
                          {
                             $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ;
                          }

                         ?>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>

                        <?php 

                          if(strcasecmp($item->payMode, "cash") == 0 ||strcasecmp($item->payMode, "Cash on delivery") == 0  )
                          {
                              $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                          }
                          else 
                          {
                            $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ;
                          }

                         ?>
 
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>   
                    <td class='text-right'>
                      <span class='badge badge-info'>{{ $item->payMode }}</span>
                    </td>
                    <td class='text-right'>
                      <span class='badge badge-success'>{{ $item->orderType }}</span>
                    </td>
                    <td class='text-right'><span class='badge badge-info'>{{ $item->totalAmount + $item->serviceFee  }}</span></td>
                  </tr>

       

                  <?php
                    $i++; 
                     }
                    ?>

                    <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='7'>
                      Total Cash: <span class='badge badge-success'>{{ $totalcashamount  }} &#8377;</span> 
                      Total Online: <span class='badge badge-warning'>{{ $totalonlineamount  }} &#8377;</span> 
                      Total Amount: <span class='badge badge-danger'>{{ $totalcashamount +  $totalonlineamount }} &#8377;</span></th>  
                  </tr> 
                   <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='7'>
                      <a target='_blank' class='btn btn-sm btn-primary' href="{{ URL::to('/admin/accounting/prepare-daily-collection') }}?ai={{ $agent->id }}">Collect Amount</a>
                    </th>  
                  </tr> 
                </tbody>
                  </table>
           </div>
 
           
            </div>
  </div>

  
<div class='col-md-4'> 
	<div class="card bg-danger mt-3">
       <div class="card-header bg-danger">
       	<div class="row">
            <div class="col-md-8">
          		<h5>Daily Collection Logging</h5>
   			</div>
    		<div class="col-md-4 text-right">  </div> 
	    </div>
	  </div>

       <div class="card-body"> 
       	<p class='alert alert-info'>PLEASE BE AWARE, THIS IS NON-EDITABLE STEP.</p>

	           	<form action="{{ action('Admin\AdminDashboardController@saveDailyCollection') }}" method='post'>
	           		{{ csrf_field() }}
					  <div class="form-row">
					    <div class="form-group col-md-6">
					      <label for="cash">Total Cash</label>
					      <input type="number" class="form-control" id="cash" name="cash" placeholder="0.00" value="{{ $totalcashamount }}">
					    </div>
					    <div class="form-group col-md-6">
					      <label for="online">Total Online</label>
					      <input type="number" class="form-control" id="online" name="online" placeholder="0.00" value="{{ $totalonlineamount  }}">
					    </div>
					    
					  </div>
					   <div class="form-row">
					  	<div class="form-group col-md-6">
					     <label for="due">Total Due</label>
					    <input type="number" class="form-control" id="due" name="due" placeholder="0.00" value="0.00">
					    </div>
					    <div class="form-group col-md-6">
					      <label for="total">Total </label>
					      <input type="number" readonly class="form-control" id="total" name='total' placeholder="0.00" 
					      value="{{ $totalcashamount  + $totalonlineamount  }}">
					    </div> 
					    
					  </div>

					  <div class="form-row">
					  	
					  	<div class="form-group col-md-6">
					     <label for="accountdate">Accounting Date</label>
					    <input type="text" readonly class="form-control" id="accountdate" name='accountdate'  value="{{ date('d-m-Y', strtotime($data['date'])) }}" >
					    </div>
					    <div class="form-group col-md-6">
					      <label for="actualdate">Actual Collection Date </label>
					      <input type="text" readonly class="form-control calendar" id="actualdate" name='actualdate' >
					    </div> 
					    
					  </div>


					  <div class="form-group">
					    <label for="remarks">Collection Remarks</label>
					    <textarea type="number" class="form-control" id="remarks" name='remarks' rows='5'  ></textarea>
					  </div>


					 <input type="hidden"  name='ai' value="{{$agent->id}}" /> 
					  <button type="submit" name='submit' value='save' class="btn btn-primary">Save Money Collection Report</button>
					</form>

           </div> 
</div> </div> 

 </div>


 
 
 

     

@endsection


 @section("script")

<script>
  
 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 