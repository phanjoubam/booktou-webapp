<?php 
    $total =0;   
    $agent = $data['agent_info'];
    $deposited =false; 
?>

@extends('layouts.admin_theme_02')
@section('content') 

 <div class="row">
   <div class="col-md-12"> 
  
   

   <div class="card "> 
    <div class="card-header">  
<div class='row'>
          <div class='col-md-8'>
            <h4>Commission Report for {{ $agent->fullname  }} ( {{ $agent->phone }} ) on 
                <span class='badge badge-warning badge-pill'>{{ date('d-m-Y', strtotime($data['date'])) }}</span></h4>
          </div>
<div class='col-md-4 text-right'>
  <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@updateDeliveryAgentDailyCommission') }}"> 
    <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label>  
    <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' name='filter_date'  value="{{ date('d-m-Y', strtotime($data['date'])) }}" /><button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
             <input type="hidden" name='aid' value="{{ $agent->id  }}"  /> 
  </form>
</div>
</div>

    </div>
       <div class="card-body">  
   <form   method="post" action="{{  action('Admin\AdminDashboardController@updateDeliveryAgentDailyCommission') }}"> 
    {{  csrf_field() }}
      <table class="table table-bordered mb-3"> 
           
          <tr class="text-center" style='font-size: 12px'>  
            <th scope="col" class="text-center" >Order No.</th> 
            <th scope="col" class="text-left">Merchant/Customer</th>  
            <th class="text-center">Order Type</th> 
            <th class='text-center'>Status</th>                    
            <th class="text-right"  width='120px'>Deposit Status</th>     
            <th class="text-right"  width='140px'>Service Fee</th>       
            <th class="text-center" width='120px'>Commission</th> 
          </tr> 
            
            @php
              $i=1; 
              $totalcashamount = $totalonlineamount = 0.0; 
            @endphp 

            @foreach ($data['all_orders'] as $item)
              @php 
                $amountCollected = 0.00;
              @endphp 
              @if($item->member_id == $agent->id )
                <tr >
                  <td class="text-center">
                    @if($item->orderType == "normal")
                    <a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{ $item->orderNo }}">{{ $item->orderNo }}</a> 
                      @elseif($item->orderType == "pnd")
                      <a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                      @else
                      <a target='_blank'  href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                    @endif
                </td> 
                <td>{{$item->orderBy}}</td> 
                <td class='text-center'>
                    @switch( strtolower($item->orderType) ) 
                          @case("pnd")
                            <span class='badge badge-warning badge-pill'>PnD</span>
                            @break
                          @case("normal")
                            <span class='badge badge-success badge-pill'>Normal</span>
                            @break

                          @case("assist")
                            <span class='badge badge-info badge-pill'>Assist</span>
                            @break
  
                        @endswitch  
                </td>
                <td class="text-center"> 
                      @switch($item->orderStatus)

                      @case("new")
                        <span class='badge badge-primary badge-pill'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info badge-pill'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info badge-pill'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info badge-pill'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning badge-pill'>Pickup Didn't Come</span>
                      @break
                      @case("returned")
                        <span class='badge badge-danger badge-pill'>Returned</span>
                      @break

                      @case("cancelled")
                      @case("canceled")
                        <span class='badge badge-danger badge-pill'>Cancelled</span>
                      @break
                       @case("in_route")
                        <span class='badge badge-info badge-pill'>In Route</span>
                      @break

                      @case("completed")
                      @case("delivered")

                        <?php 

                          if(  $item->source == "booktou" || $item->source == "customer"  )
                          {
                              if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00;
                                $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                              else //cash collection
                              {
                                $amountCollected = $item->totalAmount + $item->serviceFee ;
                                $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                          }
                          else if(  $item->source == "business" || $item->source == "merchant"  )
                          {
                            if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00; 
                              }
                              else //cash collection
                              {
                                $amountCollected = $item->totalAmount + $item->serviceFee ;
                                $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                          }

                         ?>

                        <span class='badge badge-success badge-pill'>Delivered</span> 
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info badge-pill'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>   
                   
                     
                    <td class='text-center'  width='120px'> 
                    @if($item->isDeposited =="yes")   
                          @if($amountCollected  <= 0 )
                            <span class='badge badge-warning badge-pill'>Ignored</span>
                          @else
                            @php
                              $deposited =true;
                            @endphp  
                            <span class='badge badge-success badge-pill'>Deposited</span>
                          @endif  
                    @else 
                      <span class='badge badge-danger badge-pill'>Not-deposited</span>  
                    @endif 
                    </td> 
                    <td class='text-right'  width='120px'>{{ number_format( $item->serviceFee , 2, '.', '')  }}</td>
                    <td class='text-right'  width='120px'>
                        <input type='hidden'    name='ordernos[]'  value="{{  $item->orderNo }}" />
                        @php 
                          $order_comm = 0.00;
                        @endphp
                        @foreach($data['order_commissions'] as $commission) 
                          @if($item->orderNo == $commission->order_no) 
                            @php 
                              $order_comm = $commission->commission;
                            @endphp
                            @break
                          @endif
                        @endforeach 
                        <input type="text" value="{{ $order_comm }}" name="comms[{{  $item->orderNo }}]" class='form-control form-control-sm text-right' />
                    </td> 
                  @endif 
                </tr>
                @php
                    $i++;
                @endphp
            @endforeach
            
            <tr class="text-center" style='font-size: 12px'>  
            <th scope="col" class="text-center" >Order No.</th> 
            <th scope="col" class="text-left">Merchant/Customer</th>  
            <th class="text-center">Order Type</th> 
            <th class='text-center'>Status</th>                    
            <th class="text-right"  width='120px'>Deposit Status</th>     
            <th class="text-right"  width='140px'>Service Fee</th>       
            <th class="text-center" width='120px'>Commission</th> 
          </tr>  
              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="6" class="text-right"></th>
              <th scope="col" colspan="1"  >  
                <input class='btn btn-primary btn-sm' type="submit"  name='btnsave' value='Save Commission'  > 
                <input type="hidden" name='aid' value="{{ $agent->id  }}"  /> 
                <input type="hidden" name='filter_date' value="{{ Request::get('filter_date') }}"  /> 
 
              </th>   
              </tr>   
       </table>  


</form>

 
     </div>
    </div>
 </div> 
</div>  



 <form action="{{ action('Admin\AdminDashboardController@updatePaymentTarget') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalchangeToFranchise" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Update Final Payment Location</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             
        
    <div class="form-row"> 
      <div class="form-group col-md-6">
        <label for="frno">Select Franchise </label>
     
      </div>

      <div class="form-group col-md-12">
        <label for="remarks">Migration Remarks</label>
        <textarea name="remarks" class="form-control" id="remarks" rows='3' ></textarea> 
      </div>

    </div>
 
    </div>
        <div class="modal-footer"> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Route Order</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>  
        </div>
      </div>
    </div>
  </div>
</form> 


@endsection


 @section("script")

<script>
  
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

$('input[type=checkbox].select').change(function() {
    
    var totalAmt= 0.00;
    $('input[type=checkbox].select').each(function () {
       totalAmt +=  (this.checked) ? parseFloat(  $(this).attr("data-value") ) :  parseFloat(0.00); 
    });

    $(".totalcollected").val(totalAmt); 
 });





</script> 

<style>
input[type="checkbox"][readonly] {
  pointer-events: none;
}
</style>
@endsection 