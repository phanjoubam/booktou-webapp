@extends('layouts.admin_theme_02')
@section('content')
 
<?php 

$business = $data['business'];
 
?>
<div class="row">  

<div class="col-md-12">  
 <div class="card">
  <div class="card-header"> 
      <h4 class="card-title text-center">Merchant Earning Report</h4>
  </div> 
  <div class="card-body">
    <div class='row'>
      <div class='col-md-7'>
        <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@viewBusinessMonthlySales') }}"> 
            {{ csrf_field() }} 
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Month:</label> 
              <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='month'>
                 
                    <option value='1'>January</option>
                    <option value='2'>Febuary</option> 
                    <option value='3'>March</option> 
                    <option value='4'>April</option> 
                    <option value='5'>May</option> 
                    <option value='6'>June</option> 
                    <option value='7'>July</option> 
                    <option value='8'>August</option> 
                    <option value='9'>September</option> 
                    <option value='10'>October</option> 
                    <option value='11'>November</option> 
                    <option value='12'>December</option> 
                
                </select>
                 <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Year:</label> 
                 <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='year'> 

                  <?php 
                  for($i=date('Y'); $i >=2020; $i--)
                   {
                    ?>
                  <option value='{{ $i }}'>{{ $i }}</option>
                   <?php 
                 }
                 ?>
                    
                
                </select>

                <input type="hidden" value="{{ $data['bin'] }}" name="bin">
              
                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>
      </div>
       <div class='col-md-5'>

        <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@viewBusinessMonthlySales') }}"> 
            {{ csrf_field() }} 
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Select Month:</label> 
              <input class='form-control form-control-sm' name='start_date' />
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Select Year:</label> 
               <input class='form-control form-control-sm' name='end_date' /> 
               <input type="hidden" value="{{ $data['bin'] }}" name="bin"> 
               <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>

      </div>
    </div>
      

 <hr class="my-4" />
<div>
  <strong>Store Name :</strong> {{$business->name}}<br/>
  <strong>Store Address :</strong> {{$business->locality}}, {{$business->landmark}}, {{$business->city}}, {{$business->state}}-{{$business->pin}}<br/>
  <strong>Primary Phone :</strong> {{$business->phone_pri}} </div>  
 
 <table class="table"> 
  <thead class=" text-primary">
    <tr class="text-center"> 
    <th scope="col">Order No.</th>
    <th scope="col">Date</th>
    <th scope="col">Customer</th>   
    <th scope="col" class='text-right'>Item Cost</th>
    <th scope="col"  class='text-right'>Packaging</th>
     <th scope="col"  class='text-right'>Total Cost</th>
    <th scope="col">Order Status</th>  
    </tr>
    </thead>

    <tbody>

     <?php $i = 0 ?>
    @foreach ($data['results'] as $item)
    <?php $i++ ?> 
     <tr class="text-center"> 

     <td>
      <a target='_blank' href="{{ URL::to('admin/customer-care/order/view-details') }}/{{ $item->id }}"  >{{$item->id}}</a> 
     </td>
     <td>{{ date('d-m-Y', strtotime( $item->book_date) ) }}</td> 
     
      <td class='text-left'>{{$item->orderBy}}</td>
      
      @php
        $item_cost = 0;
        $package_cost = 0;
      @endphp
    @foreach ($data['orderItems'] as $order_item)
        @if( $order_item->order_no  ==  $item->id )
          @php 
            $item_cost =   $order_item->qty * $order_item->price;
            $package_cost =   $order_item->qty * $order_item->package_charge;
          @endphp  
          @break
        @endif
     @endforeach 

    <td  class='text-right'>
      {{  $item_cost }}
    </td> 

    <td  class='text-right'>
      {{  $package_cost }}
    </td> 


      <td class='text-right'>
         @if($item->book_status == "delivered")
             {{$item->seller_payable}}
        @else
          0.00
         @endif
      </td>
      <td>@switch($item->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 

                      @case("cancel_by_client")
                        <span class='badge badge-danger'>Canceled by customer</span>
                      @break

                       @case("cancel_by_owner")
                        <span class='badge badge-danger'>Canceled by merchant</span>
                      @break
                       @case("canceled")
                        <span class='badge badge-danger'>Canceled</span>
                      @break
                      @endswitch

                    </td>
      
    </tr>
    
@endforeach
@if(isset($data['results']) && count($data['results']) > 0)  
     <tr  > 
     <td class='text-right' colspan='4'>Total Sales:</td>
     <td class='text-right' >{{ number_format( $data['totalEarning'] , 2, '.', '')     }} ₹</td> 
     <td colspan='2'></td>  
    </tr> 
 @endif


 
     <tr  > 
     <td class='text-left' colspan='2'>Commission Percentage:</td>
     <td class='text-left' >{{  $business->commission  }} %</td> 
     <td class='text-right'  >Commission:</td>
     <td class='text-right' >{{  ($business->commission/100) * $data['totalEarning'] }} ₹</td> 
     <td colspan='2'></td>  
    </tr>
  <tr  > 
     <td class='text-right' colspan='4'>Amount to clear:</td>
     <td class='text-right' >{{ $data['totalEarning']  - (    ($business->commission/100) * $data['totalEarning']  )  }} ₹</td> 
     <td colspan='2'></td>  
    </tr>



   </tbody>

  </table>
 
    
  </div>
   
</div>    
  
   </div> 

   </div>

     

@endsection


  
 