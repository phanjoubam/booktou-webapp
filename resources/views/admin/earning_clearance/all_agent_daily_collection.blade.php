<?php 
    $total =0;  
?>

@extends('layouts.admin_theme_02')
@section('content')
 

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
         
                <div class="row">
          <div class="col-md-8">
            <h3>View Daily Orders Delivery Reports for <span class='badge badge-warning'>{{ date('d-m-Y', strtotime($data['date'])) }}</span></h3>
           </div>
    <div class="col-md-4 text-right">
         <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@allDeliveryAgentsDailyCollectionReport') }}"> 
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' name='filter_date' />  <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
            </form>
      </div>
   
    </div>
  </div>
 </div>
  
   <div class="card "> 
       <div class="card-body"> 
        @php
          $slno = 1; 
          $sumTotal =  0;
          $collectionTotal=0;

          $grossTotalNormal =  0;
          $grossTotalPnD =  0;
          $grossTotalAssist  =  0;
          $grossDeposit =0;

        @endphp 
  
    @if(isset($data['all_orders']) && count($data['all_orders']) > 0)

      <table class="table  ">  
        <tr class="text-center" style='font-size: 12px'>  
          <th scope="col" class="text-center" >Sl. No.</th> 
          <th scope="col" class="text-left">Agent Name</th>
          <th scope="col" class="text-center" >Collection Date</th> 
          <th class='text-right'>Normal</th>    
          <th class="text-center">PnD</th> 
          <th class='text-center'>Assist</th>
          <th class="text-center">Amount</th>                     
          <th class='text-center'>Deposits</th> 
          <th class="text-center">More</th> 
        </tr>

        @foreach($data['agents'] as $agent)


          @php
            $totalcashamount = $totalonlineamount = 0.0;  
            $totalNormals = $totalPnds = $totalAssists =0;
            $totalDeposits = 0;
          @endphp 
          <tr>
            <td>{{ $slno }}</td>
            <td>{{ $agent->fullname }} ( {{ $agent->phone }} )</td>
              

            @foreach ($data['all_orders'] as $item)  


              @if( $item->member_id == $agent->id ) 
                @if( strcasecmp($item->orderStatus, "completed") == 0 || strcasecmp($item->orderStatus, "delivered") == 0) 

                  @if($item->orderType == "normal")
                    @php 
                      $totalNormals++; 
                      $grossTotalNormal++;
                    @endphp  
                  @elseif($item->orderType == "pnd")
                    @php 
                      $totalPnds++;
                      $grossTotalPnD++;
                    @endphp
                  @elseif($item->orderType == "assist")
                    @php 
                      $totalAssists++; 
                       $grossTotalAssist++;
                    @endphp
                  @endif 
 
                  <?php 
                      if(  $item->source == "booktou" || $item->source == "customer"  )
                      {
                        if( strcasecmp($item->payMode, "online" ) == 0  ||  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                        {
                          $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ; 
                        }
                        else //cash collection
                        {
                          $totalcashamount  +=  $item->totalAmount + $item->serviceFee ; 
                          
                          if($item->is_deposited == "yes")
                          {
                            $totalDeposits +=  $item->totalAmount + $item->serviceFee ;
                            $collectionTotal +=  $item->totalAmount + $item->serviceFee ;
                          }

                        } 
                      }
                      else if(  $item->source == "business" || $item->source == "merchant"  )
                      {
                        if( strcasecmp($item->payMode, "online" ) == 0  || 
                          strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                        {
                          $totalonlineamount  += 0.00;  
                        }
                        else //cash collection
                        { 
                          $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;

                          if($item->is_deposited == "yes")
                          {
                            $totalDeposits +=  $item->totalAmount + $item->serviceFee ;
                            $collectionTotal +=  $item->totalAmount + $item->serviceFee ;
                          }

                        } 
                      }
                    ?>
                @endif
              @endif

            @endforeach 
            @php
              $sumTotal += $totalcashamount;
              $slno++;
            @endphp
          <td class="text-center">{{ date('d-m-Y', strtotime($item->service_date))}}</td> 
          <td class="text-right">{{ $totalNormals }}</td>
          <td class="text-right">{{ $totalPnds }}</td>
          <td class="text-right">{{ $totalAssists }}</td>
          
          
            @if($totalcashamount > $totalDeposits) 
               <td class='text-right'><span class='badge  badge-pill badge-warning'>{{ number_format( $totalcashamount ,2, '.', '') }}</span></td>  
               <td class="text-right"><span class='badge  badge-pill  badge-danger'>{{ number_format( $totalDeposits ,2, '.', '') }}</span></td>
            @elseif($totalcashamount == $totalDeposits)
               <td class='text-right'><span class='badge badge-success badge-pill'>{{ number_format( $totalcashamount ,2, '.', '') }}</span></td> 
               <td class="text-right"><span class='badge  badge-success  badge-pill'>{{ number_format( $totalDeposits ,2, '.', '') }}</span></td>
            @else
                <td class='text-right'><span class='badge badge-success badge-pill'>{{ number_format( $totalcashamount ,2, '.', '') }}</span></td> 
                <td class="text-right"><span class='badge  badge-warning badge-info'>{{ number_format( $totalDeposits ,2, '.', '') }}</span></td>
            @endif 

          
          <td style="text-align: center;">
              <div class="dropdown pull-right show">
                <button class="btn btn-primary btn-sm " type="button"   data-toggle="dropdown"  >
                 <i class='fa fa-cog'></i>
                </button>

                <ul class="dropdown-menu"  >
                  <li><a href="{{ URL::to('/admin/delivery-agent/daily-collection-report')}}?aid={{ $agent->id }}&filter_date={{ $data['date'] }}" 
              class='dropdown-item' target='_blank'>Details</a></li>


              <li><a href="{{ URL::to('/admin/delivery-agent/update-daily-delivery-commission')}}?aid={{ $agent->id }}&filter_date={{ $data['date'] }}" 
              class='dropdown-item' target='_blank'>Update Commission</a></li>
               <div class="dropdown-divider"></div>
              <li><a href="{{ URL::to('/admin/delivery-agent/printable-daily-collection-report')}}?aid={{ $agent->id }}&filter_date={{ $data['date'] }}" 
              class='dropdown-item' target='_blank'>Print Collection Sheet</a></li>

              
                </ul>
              </div>


              


          </td> 
        </tr> 
    @endforeach
    <tr>
      <th colspan='3' class='text-right'>Total:</th> 
      <th class='text-right'>{{$grossTotalNormal }}</th>
      <th class='text-right'>{{ $grossTotalPnD}}</th>
      <th class='text-right'>{{ $grossTotalAssist}}</th>
      <th class='text-right'>{{ number_format( $sumTotal ,2, '.', '') }}</th>
      <th class='text-right' style='width: 130px'>
        <input type="text" class="form-control form-control-sm is-valid" readonly value="{{  number_format( $collectionTotal ,2, '.', '') }}"/>
      </th>
      <th colspan=''></th>
    </tr>
  

    </table>
 @endif
     </div>
    </div>
 </div> 
</div>  

@endsection


 @section("script")

<script>
  
 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 