@extends('layouts.admin_theme_02')
@section('content')

<?php 

$total_service_fee=0;
$total_sum_amount=0;
$total_due_amount=0;
?>

<div class="row">
     <div class="col-md-12"> 
      <div class="card panel-default"> 
           <div class="card-header"> 
               <div class="row">

                   <div class="col-md-6">
                  <div class="title">
                  Report for payment clearance no <span class='badge badge-primary'># {{$clearanceid}}</span> 

                  </div> 
                </div>

                <div class="col-md-2 text-right"> 
                  <span class="badge badge-warning">Paid amount:</span> 
                  <span class="badge badge-success">
                    <?php 

                    foreach ($paid as $clear) {
                    $paidAmount = $clear->amount;
                    }

                    ?>
                    
                 <small>{{$paidAmount}}</small> 
                  </span> 
                </div> 
                <div class="col-md-4">  
                <span class="badge badge-primary"><small>Generate receipt:</small></span> 
                   <span class="">
              <a href="{{url('/admin/payment/e-clearance-report')}}/{{$clearanceid}}"><i class="fa fa-download"></i>
              </a>
              </span>  
                </div>

       </div>

       <div class="card-body">
        @if (session('err_msg'))
        <div class="col-md-12"> 
        <div class="alert alert-danger">
      {{ session('err_msg') }}
      </div>
      </div>
      @endif
<hr>

@if(isset($clearanceReport))
        <table class="table table-responsive">
          <thead>
            <th>Order No</th>
            <th>Source</th>
            <th>Order type</th>
            <th>Clearance</th>
            <th>Payment Type</th>
            <th>Service Fee</th>
            <th>Book Date</th>
            <th>Commission</th>
            <!-- <th>Payment Target</th> -->
            <th>Due by merchant</th>
            <th>Amount</th>


            
          </thead>

          <tbody>

       @php 
        $total_sales  = 0; 
        $total_commission =0;
       @endphp 


            @foreach($clearanceReport as $clearance)

             @php  
              $actual = 0;
              $commission = 0;
              $payable = 0;
              $total_due=0;
              @endphp 

              @php
              $actual = $clearance->total_amount;     
              @endphp




            <tr>
              <td>{{$clearance->id}}</td>
              <td>{{$clearance->source}}</td>

              @if($clearance->orderType == "pnd")
                    @if($clearance->payment_target=="merchant" && $clearance->source == "booktou" || $clearance->source == "customer" ) 
                      @php 
                      $commission = 0 ;
                      @endphp
                    @elseif($clearance->source == "booktou" || $clearance->source == "customer" ) 
                      @php 
                         
                    $commission = ( $clearance->commission * 0.01 * $clearance->total_amount ); 

                      @endphp
                      @else
                       @php 
                        $commission = 0 ; 
                      @endphp
                    @endif 
                  @else 

                    @php
                    $commission = ( $clearance->commission * 0.01 * $clearance->total_amount ); 
                    @endphp 

              @endif

              @php
              $total_sales += $actual ;
              $total_commission += $commission ;      
              @endphp

              <td>{{$clearance->orderType}}</td>


              <td>{{$clearance->clerance_status}}</td>
              <td>{{$clearance->pay_mode}}</td>
              <td>{{$clearance->service_fee}}</td>
              <td>{{date('d-m-Y', strtotime($clearance->book_date))}}</td>
              <td>{{$commission}}</td>
              <!-- <td>{{$clearance->payment_target}}</td>-->
              <td>


                <?php


                if ($clearance->payment_target=="merchant" && 
                    $clearance->pay_mode == "ONLINE" || 
                    $clearance->pay_mode == "Pay online (UPI)") {
                   $total_due = $clearance->service_fee;
                }else{
                  $total_due=0;
                }

                ?>

                {{$total_due}}
              </td>
              <td>

                 @if($clearance->orderType == "pnd")
                    @if( $clearance->payment_target == "merchant" 
                      && $clearance->pay_mode == "ONLINE" || 
                      $clearance->pay_mode == "Pay online (UPI)") 
                      @php 
                        $_amount = 0; 
                      @endphp
                    @else 
                      @php 
                        $_amount = $clearance->total_amount;
                      @endphp
                    @endif 
                  @else 

                   @php 
                        $_amount = $clearance->total_amount;
                   @endphp
              @endif


                {{$_amount}}



              </td>


            </tr>
            <?php 

              $total_sum_amount+=$_amount;
              $total_service_fee+=$clearance->service_fee;
              $total_due_amount+= $total_due;
            ?>

            @endforeach

            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td class="text-right"><span class="badge badge-info ">Service fee</span></td>
              <td>{{$total_service_fee}}</td>
              <td><span class="badge badge-info">
                 Total Commission </span></td>
              <td>
                {{$total_commission}}</td>
               <!--  <td></td> -->
              <td><span class="badge badge-warning"><small>Total Amount</small></span></td> 
              <td>{{$total_sum_amount}} </td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><span></span></td>
              <td></td>
              <!-- <td></td> -->
              <td><span class="badge badge-danger">
                Total due by merchant: </span></td>
              <td>{{$total_due_amount}}</td>
              <td>
                <span class="badge badge-success">
                <small>Paid Amount</small></span></td> 
              <td>{{$total_sum_amount - $total_commission-$total_due_amount}} </td>
            </tr>



            <tr>
               
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <!-- <td></td> -->
              <td></td>
              <td><span class="badge badge-primary">Generate receipt:</span></td> 
              <td><span class="">
              <a href="{{url('/admin/payment/e-clearance-report')}}/{{$clearanceid}}"><i class="fa fa-download"></i>
              </a>
              </span></td>
            </tr>
          </tbody>

          
        </table>
@endif

       </div>
          </div>
      </div>
     </div>
</div>





@endsection

@section("script")


@endsection 