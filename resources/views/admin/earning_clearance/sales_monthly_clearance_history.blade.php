@extends('layouts.admin_theme_02')
@section('content')
 
<?php 

$business = $data['business'];
 
?>
<div class="row">  

<div class="col-md-12">  
 

 <div class="card">
    <div class="card-header"> 
      <div class='row'> 
       <div class='col-md-8'>
        <h4  >Clearance History for {{$business->name}}</h4> 
      </div>  
      <div class='col-md-4 text-right'>
        <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@salesAndClearanceHistory') }}"> 
            {{ csrf_field() }} 
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Month:</label> 
              <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='month'>
                 
                    <option value='1'>January</option>
                    <option value='2'>Febuary</option> 
                    <option value='3'>March</option> 
                    <option value='4'>April</option> 
                    <option value='5'>May</option> 
                    <option value='6'>June</option> 
                    <option value='7'>July</option> 
                    <option value='8'>August</option> 
                    <option value='9'>September</option> 
                    <option value='10'>October</option> 
                    <option value='11'>November</option> 
                    <option value='12'>December</option> 
                
                </select>
                 <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Year:</label> 
                 <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='year'>
                  
                  <?php 
                  for($year = date('Y'); $year >=2020; $year--)
                  {
                    ?>
                    <option value='{{$year }}'>{{$year }}</option>
                    <?php 
                  }
                  ?> 
                </select>

                <input type="hidden" value="{{ $data['bin'] }}" name="bin">
              
                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>
      </div> 
    </div>
  </div> 
 
</div> 


@foreach( $data['payment_history'] as $history )

 <div class="card mt-3">
    <div class="card-header"> 
<div class='row'>
  
  <div class='col-md-6'> 
         <ul class="nav">
          <li class="nav-item">
            <a class="nav-link active" target="_blank" href="{{ URL::to('admin/payment/view-clearance-report-merchantwise') }}/{{ $history->id }}">Clearance # {{ $history->id }}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Total Paid <span class='badge badge-primary'>{{ $history->amount }}</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Cleared On <span class='badge badge-info'>{{ date('d-m-Y', strtotime($history->cleared_on)) }}</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Payment Type <span class='badge badge-success'>{{ $history->payment_mode }}</span></a>
          </li>
        </ul>
  </div> 
  <div class='col-md-6'>
     {{ $history->remarks }}
  </div>

</div> 
    </div> 

  <div class="card-body">  
<div class='table-responsive'> 
 
 <table class="table"> 
  <thead class=" text-primary">
    <tr class="text-center">  
    <th scope="col">Order No.</th>
    <th scope="col">Order Date</th>
    <th scope="col">Customer</th> 
    <th scope="col" class='text-center'>Order Status</th>    
     <th scope="col" class='text-center'>Order Origin</th>    
    <th scope="col"  class='text-right'>Amount Cleared</th> 
     <th scope="col" class='text-center'>Customer Pay Mode</th>   
    <th scope="col" class='text-right'>Clerance Status</th>  
    </tr>
    </thead> 
    <tbody>

       @php 
        
        $total_sales  = 0;
        $total_fee = 0; 
        $orderlist_csv =  $history->order_nos ;  
        $ordernolist =  array_unique( array_filter( explode(",", $orderlist_csv) ) ); 

       @endphp 


     <?php $i = 0 ?>
    @foreach ($data['results'] as $item)

      @foreach($ordernolist as $orderno ) 
 
          @if( $item->id ==  $orderno  )
            <tr class="text-center"> 
            
           <td> 
              <a target='_blank' href="{{ URL::to('admin/customer-care/pnd-order/view-details') }}/{{ $item->id }}"  >{{$item->id}}</a>  
           </td> 
           <td>{{ date('d-m-Y', strtotime( $item->book_date) ) }}</td> 
           <td class='text-left'>{{$item->fullname}}</td>
           <td>@switch($item->book_status)
                @case("new")
                  <span class='badge badge-primary'>New</span>
                  @break 
                @case("confirmed")
                              <span class='badge badge-info'>Confirmed</span>
                            @break 

                            @case("order_packed")
                              <span class='badge badge-info'>Order Packed</span>
                            @break

                            @case("package_picked_up")
                              <span class='badge badge-info'>Package Picked Up</span>

                            @break

                             @case("pickup_did_not_come")
                              <span class='badge badge-warning'>Pickup Didn't Come</span>
                            @break

                             @case("in_route")
                              <span class='badge badge-success'>In Route</span>
                              @break 

                             @case("completed")
                              <span class='badge badge-success'>Completed</span>
                              @break
                            
                            @case("delivered")
                              <span class='badge badge-success'>Delivered</span>
                              @break

                            @case("delivery_scheduled")
                              <span class='badge badge-success'>Delivery Scheduled</span>
                              @break 

                            @case("cancel_by_client")
                              <span class='badge badge-danger'>Canceled by customer</span>
                              @break
                            @case("cancel_by_owner")
                              <span class='badge badge-danger'>Canceled by merchant</span>
                              @break
                            @case("canceled")
                              <span class='badge badge-danger'>Canceled</span>
                              @break
                            @endswitch
       
                 </td> 
                  <td class='text-center'>
          @if($item->source == "booktou")
            <span class='badge badge-primary'>bookTou</span>
          @else
            <span class='badge badge-danger'>Merchant</span>
          @endif
        </td>

          @if($item->book_status == "delivered")     

           @php 
              $total_sales +=    $item->total_amount;
              $total_fee += $item->service_fee;
             @endphp
        
            <td  class='text-right'>
            {{ $item->total_amount  }}
          </td>  
          @else
               <td class='text-right'> 0.00</td>  
            @endif
          <td  class='text-center'>
            <span class='badge badge-info'>{{$item->pay_mode}}</span>
          </td> 

           <td  class='text-right'>
            @switch($item->clerance_status)

                            @case("paid")
                              <span class='badge badge-success'>PAID</span>
                            @break

                            @case("un-paid")
                              <span class='badge badge-danger'>UN-PAID</span>
                            @break 

                            @case("not required")
                              <span class='badge badge-info'>NOT REQUIRED</span>
                            @break


         @endswitch
            </td>  
          </tr>
          @endif  


      @endforeach
      

    <?php $i++ ?> 
     
@endforeach
 
</tbody>
</table>  
</div> 
</div> 
</div>     
@endforeach

  
   </div> 

   </div>

     

@endsection


  
 