<?php 
    $total =0;  
?>

@extends('layouts.admin_theme_02')
@section('content')
 

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
         
                <div class="row">
          <div class="col-md-8">
    <h5>Daily Orders Reports for <span class='badge badge-primary'>{{ $data['agentName'] }}</span> for 
      <span class='badge badge-warning'>{{ date('d-m-Y', strtotime($data['date'])) }}</span></h5>
   </div>
    <div class="col-md-4 text-right">
         <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@getDeliveryAgentDailyOrdersReport') }}">
            {{ csrf_field() }}
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' name='filter_date' /> 

             <input  type="hidden"  value="{{ $data['agent']}}" name='agent'> 

                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
            </form>
      </div>
   
    </div>
  </div>
       <div class="card-body"> 

      <div class="table-responsive">
          <table class="table">
                    <thead class=" text-primary"> 
 
                  <tr class="text-center" style='font-size: 12px'>  
                    <th scope="col">Order No.</th>
                    <th scope="col">Merchant/Customer</th> 
                    <th class="text-center">Order Status</th> 
                    <th class='text-right'>Payment Type</th>  
                    <th class='text-right'>Order Type</th>  
                    <th class="text-right">Amount Collected</th> 
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1; 
                $totalAmount =0.0;
                foreach ($data['all_orders'] as $item)
                { 
                  
                ?>  

                  <tr id="tr{{$item->orderNo }}">  
                   <td>
                    <strong>{{$item->orderNo}}</strong>
                  </td>
                    <td>{{$item->orderBy}}</td>  
                 
                     <td class="text-center">

                      @switch($item->orderStatus)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-info'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                        <?php $totalAmount  +=  $item->totalAmount + $item->serviceFee ?>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                        <?php $totalAmount  +=  $item->totalAmount + $item->serviceFee  ?>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>   
                    <td class='text-right'>
                      <span class='badge badge-info'>{{ $item->payMode }}</span>
                    </td>
                    <td class='text-right'>
                      <span class='badge badge-success'>{{ $item->orderType }}</span>
                    </td>

                        <td class='text-right'><span class='badge badge-info'>{{ $item->totalAmount + $item->serviceFee  }}</span></td>
                         </tr>
                         <?php
                          $i++; 
                       }

                          ?>
                    <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='5'>Total Amount to collect:</th>  
                    <th class="text-right" >{{ $data['total_amount']  }} &#8377;</th>  
                  </tr>
                </tbody>
                  </table>
            
              </div>
            </div>
 </div>
 

 </div>



@if($total > 0) 
 
<div class="col-md-12">
<div class="panel panel-default">
       <div class="panel-heading">
         <div class="card-title"> 
         Clearance History 
         </div>
       </div>

              <div class="panel-body"> 
                 <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary"> 
 
                  <tr class="text-center" style='font-size: 12px'>  
                     <th class="text-left"   >Reference No.</th>
                    <th class="text-left"  >Date</th> 
                    <th class="text-left"  >Remarks</th> 
                    <th class="text-left">Type</th> 
                    <th class="text-right" >Amount</th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1; 
                  $total_paid  =0;  
                $clearance_status = "HAS DUE PAYMENT" ; 
                foreach ($data['payments'] as $item)
                {

                ?>
  
                  <tr id="tr{{$item->id }}">  
                   <td>
                   <strong>{{$item->transact_no}}</strong>
                  </td>
                    <td>{{ date('d-m-Y', strtotime($item->dealing_date )) }}</td> 
                    <td>{{$item->remarks}}</td>
                     <td>

                      @switch($item->clearance_type)

                      @case("partial")
                        <span class='badge badge-primary'>PARTIAL</span>
                      @break

                      @case("full")
                        <span class='badge badge-success'>FULL</span>
                        <?php $clearance_status = "PAID" ;?>
                      @break 
  
                      @endswitch

                    </td>   
                    <td class='text-right'>
                      {{ $item->amount }}
                    </td>
                       
                         </tr>
                         <?php
                         $total_paid += $item->amount;
                          $i++; 
                       }

                          ?>
                 <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='4'>Total Paid</th>  
                    <th class="text-right" >{{ $total_paid  }} &#8377;</th>  
                  </tr>

                  <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='4'>Total Due</th>  
                    <th class="text-right" >{{  number_format( $total - $total_paid, 2, ".", "")  }} &#8377;</th>  
                  </tr>
 
                 <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='4'>Clearance Status</th>  
                    <th class="text-right" ><span class='badge badge-info'>{{  $clearance_status  }}</span></th>  
                  </tr>


                </tbody>
                  </table>
            </div>
              </div>
            </div>

  </div>


 <div class="col-md-6">
 
 <form method="post" action="{{  action('Admin\AdminDashboardController@saveAgentDailyEarning') }}">
            {{ csrf_field() }}  
 
         <div class="panel panel-default ">
          <div class="panel-heading">
         Add Payments Information 
       </div>   
<div class="panel-body">
 
  <div class="form-row">
    <div class="col-md-6">
       <label for="billing_date">Bill For:</label>
      <input type="text" class="form-control calendar" placeholder="Payment Date" id='billing_date' name='billing_date'>
    </div>

    <div class="col-md-6">
       <label for="pay_date">Payment Date:</label>
      <input type="text" class="form-control calendar" id='pay_date' placeholder="Payment Date" name='pay_date'>
    </div>

    <div class="col-md-6">
      <label for="tsact_no">Transaction No.:</label>
      <input type="text" class="form-control" placeholder="Transaction reference no." id='tsact_no' name="tsact_no">
    </div>
</div>

<div class="form-row"> 
    <div class="col-md-6">
      <label for="paid_amount">Paid Amount:</label>
      <input type="text" class="form-control" placeholder="Type number only" id='paid_amount' name="paid_amount">
    </div>

     <div class="col-md-6">
      <label for="clearance_type">Clearance Type:</label>
      <select  class="form-control " id="clearance_type" name="clearance_type">
        <option value='partial'>Partial</option>
        <option value='full'>Full</option>
      </select>
    </div>


    <div class="col-md-12">
      <label for="remarks">Remarks:</label>
      <textarea style='border: 1px solid #E3E3E3;' class="form-control" rows='5' id="remarks" name="remarks"></textarea>
    </div>


<input  type="hidden"  value="{{ $data['aid']}}" name='aid'>    
     <button type="submit"  name='btnSave' class="btn btn-primary btn-sm"  value='save'>Save</button> 



  </div> 

          
       

  </div> 
</div>   

  </form>
</div>


 
 @endif
 
</div> 

     

@endsection


 @section("script")

<script>
  
 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 