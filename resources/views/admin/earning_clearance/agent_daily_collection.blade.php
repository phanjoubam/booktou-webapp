<?php 
    $total =0;   
    $agent = $data['agent_info'];
    $deposited =false; 
?>

@extends('layouts.admin_theme_02')
@section('content') 

 <div class="row">
   <div class="col-md-12"> 
  
   

   <div class="card "> 
    <div class="card-header">  
<div class='row'>
          <div class='col-md-8'>
            <h4>Collection Report for {{ $agent->fullname  }} ( {{ $agent->phone }} ) on 
                <span class='badge badge-warning badge-pill'>{{ date('d-m-Y', strtotime($data['date'])) }}</span></h4>
          </div>
<div class='col-md-4 text-right'>
  <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@deliveryAgentDailyCollectionReport') }}"> 
    <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label>  
    <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' name='filter_date'  value="{{ date('d-m-Y', strtotime($data['date'])) }}" /><button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
             <input type="hidden" name='aid' value="{{ $agent->id  }}"  /> 
  </form>
</div>
</div>

    </div>
       <div class="card-body"> 

   <form   method="get" action="{{  action('Admin\AdminDashboardController@saveAgentDailyCollectionReport') }}"> 
      <table class="table table-bordered mb-3"> 
           
          <tr class="text-center" style='font-size: 12px'>  
            <th scope="col" class="text-center" >Order No.</th> 
            <th scope="col" class="text-left">Merchant/Customer</th>
            <th scope="col" class="text-center" >Delivery Date</th>
            <th class='text-right'>Order Type</th>    
            <th class="text-center">Order Status</th> 
            <th class='text-center'>Payment Type</th>                    
            <th class="text-right"  width='120px'>Amount</th> 
            <th class="text-right" width='120px'>Ac Name</th>            
            <th class="text-center" width='120px'>Select</th> 
          </tr> 
            
            @php
              $i=1; 
              $totalcashamount = $totalonlineamount = 0.0; 
            @endphp 

            @foreach ($data['all_orders'] as $item)
              @php 
                $amountCollected = 0.00;
              @endphp 
              @if($item->member_id == $agent->id )
                <tr >
                  <td class="text-center">
                    @if($item->orderType == "normal")
                    <a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{ $item->orderNo }}">{{ $item->orderNo }}</a> 
                    @elseif($item->orderType == "pnd")
                    <a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                    @else
                    <a target='_blank'  href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                  @endif
                </td> 
                <td>{{$item->orderBy}}</td>
                <td>{{ date('d-m-Y', strtotime($item->service_date))}}</td>  
                <td class='text-center'>
                    @switch( strtolower($item->orderType) ) 
                          @case("pnd")
                            <span class='badge badge-warning badge-pill'>PnD</span>
                            @break
                          @case("normal")
                            <span class='badge badge-success badge-pill'>Normal</span>
                            @break

                          @case("assist")
                            <span class='badge badge-info badge-pill'>Assist</span>
                            @break
  
                        @endswitch  
                </td>
                <td class="text-center"> 
                      @switch($item->orderStatus)

                      @case("new")
                        <span class='badge badge-primary badge-pill'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info badge-pill'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info badge-pill'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info badge-pill'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning badge-pill'>Pickup Didn't Come</span>
                      @break
                      @case("returned")
                        <span class='badge badge-danger badge-pill'>Returned</span>
                      @break

                      @case("cancelled")
                      @case("canceled")
                        <span class='badge badge-danger badge-pill'>Cancelled</span>
                      @break
                       @case("in_route")
                        <span class='badge badge-info badge-pill'>In Route</span>
                      @break

                      @case("completed")
                      @case("delivered")
                        <span class='badge badge-success badge-pill'>Delivered</span>
 
                        <?php 
                        $order_amount = $item->itemTotal + $item->packagingCost + 
                        $item->deliveryCharge + $item->serviceFee  + $item->gst ;
                          if(  $item->source == "booktou" || $item->source == "customer"  )
                          {
                              if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00;
                                $totalonlineamount  +=  $order_amount ;
                              }
                              else //cash collection
                              {
                                $amountCollected = $order_amount;
                                $totalcashamount  +=  $order_amount;
                              }
                          }
                          else if(  $item->source == "business" || $item->source == "merchant"  )
                          {
                            if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00; 
                              }
                              else //cash collection
                              {
                                $amountCollected = $order_amount ;
                                $totalcashamount  +=  $order_amount ;
                              }
                          }

                         ?>
 
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info badge-pill'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>   
                    <td class='text-center'>
                
                      
                        @switch( strtolower($item->payMode) ) 
                          @case("cod")
                            <span class='badge badge-success badge-pill'>Cash</span>
                            @break 
                          @case("cash on delivery")
                            <span class='badge badge-success badge-pill'>Cash</span>
                            @break
                          @case("cash")
                            <span class='badge badge-success badge-pill'>Cash</span>
                            @break 
                          @case("online")
                            <span class='badge badge-warning badge-pill'>Online</span>
                            @break 
                          @case("pay online (upi)")
                            <span class='badge badge-warning badge-pill'>Online</span>
                            @break 
                          @other
                            <span class='badge badge-success badge-pill'>Cash</span>
                            @break 
                        @endswitch 
                     
                    </td> 
                    <td class='text-right'  width='120px'>{{ number_format( $amountCollected, 2, '.', '')  }}</td>  
                    <td class='text-right'  width='120px'>
                      <span class='badge badge-success badge-pill'>Cash-in Hand</span>
                    </td> 


                     <td class='text-center'  width='120px'>
                      @if($item->isDeposited =="yes")
                        @if($amountCollected  <=0 )
                          <span class='badge badge-danger badge-pill'>Ignored</span>
                        @else
                          @php
                            $deposited =true;
                          @endphp  
                          <span class='badge badge-success badge-pill'>Deposited</span>
                        @endif
                      @else 
                      <input type='checkbox' class='select' data-value="{{ $amountCollected}}" name='ordernos[]'  @if($amountCollected==0)  checked readonly  @endif value="{{  $item->orderNo }}" />
                      @endif
                     </td> 
                  </tr> 
                  @endif 
                @php
                    $i++;
                @endphp
            @endforeach
            
            <tr class="text-center" style='font-size: 12px'>  
              <th scope="col" class="text-center" >Order No.</th>
              <th scope="col" class="text-left">Merchant/Customer</th>
              <th scope="col" class="text-center" >Delivery Date</th>
              <th class='text-right'>Order Type</th>    
              <th class="text-center">Order Status</th> 
              <th class='text-right'>Payment Type</th>                    
              <th class="text-right" width='120px'>Amount</th> 
              <th class="text-right" width='120px'>Ac Name</th> 
              <th class="text-center" width='120px'>Select</th> 
            </tr>
            @if( $deposited )
              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="7" class="text-right">
                <h4>Total to collect from {{ $agent->fullname  }}</h4></th>
              <th scope="col" colspan="1"  > 
                <input type="text" readonly class="form-control form-control-sm form-control-block is-valid"  value="{{ number_format( $totalcashamount ,2, '.', '') }}"  > 
              </th>  
              <th> </th>
              </tr>  
              
            @else 

              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="7" class="text-right">
                <h4>Total to collect from {{ $agent->fullname  }}</h4></th>
              <th scope="col" colspan="1"  > 
                <input type="text" readonly class="form-control form-control-sm form-control-block is-valid"  value="{{ number_format( $totalcashamount ,2, '.', '') }}"  > 
              </th>  
              <th>
                <input type="text" readonly  class="form-control form-control-sm form-control-block totalcollected is-valid "    > 
                </th>
              </tr>     
 
            

            @endif
         

<tr  style='font-size: 12px'>  
              <th scope="col" colspan="8" class="text-right">
         

</th>
              </tr> 
       </table>
 
  <div class='card'>
  <div class='card-body'>
     <div class="form-row"> 
      <div class="form-group col-md-3"> 
      <label for="amount">Deposited Amount:</label>
      <input type="number" name='totalcollected' id="amount" class="form-control form-control-sm   totalcollected"  value="0.00" step=".10" min="0"    >
    </div>
    <div class="form-group col-md-3"> 
      <label for="dueamount">Due Amount:</label>
      <input type="number" name='dueamount' id="dueamount" class="form-control form-control-sm" value="0.00" step=".10" min="0"  >
    </div> 
    <div class="form-group col-md-3"> 
      <label for="extraamount">Extra Amount:</label>
       <input type="number" name='extraamount' id="extraamount" class="form-control form-control-sm" value="0.00" step=".10" min="0"  >
    </div> 
    <div class="form-group col-md-3"> 
      <label for="depdate">Deposit Date:</label>
      <input type='text' value="{{ date('d-m-Y', strtotime($data['date'])) }}" name="date" id='depdate' class="form-control form-control-sm calendar" />
    </div> 
  </div>
  <div class="form-row">
      <div class="form-group col-md-3"> 
      <label for="acname">Account Heading:</label>
      <select name="acname" id='acname' class="form-control form-control-sm " >
        <option value='cash'>Cash-in-Hand</option> 
        <option value='primary'>Primary Bank Account</option> 
        <option value='secondary'>Secondary Account</option> 
      </select>
    </div>


      <div class="form-group col-md-12"> 
      <label for="remark">Remarks <span style='color:red'>*</span></label>
      <textarea class="form-control form-control-sm  " id="details" rows="3" name='details' ></textarea>
    </div>
 
  </div>

  <input type="hidden" name='aid' value="{{ $agent->id  }}"  /> 
  <input class='btn btn-primary btn-sm' type="submit"  name='btnsave' value='Save Collection'  >

 
     </div> 
  </div> 
</form>

 
     </div>
    </div>
 </div> 
</div>  



 <form action="{{ action('Admin\AdminDashboardController@updatePaymentTarget') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalchangeToFranchise" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Update Final Payment Location</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             
        
    <div class="form-row"> 
      <div class="form-group col-md-6">
        <label for="frno">Select Franchise </label>
     
      </div>

      <div class="form-group col-md-12">
        <label for="remarks">Migration Remarks</label>
        <textarea name="remarks" class="form-control" id="remarks" rows='3' ></textarea> 
      </div>

    </div>
 
    </div>
        <div class="modal-footer"> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Route Order</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>  
        </div>
      </div>
    </div>
  </div>
</form> 


@endsection


 @section("script")

<script>
  
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

$('input[type=checkbox].select').change(function() {
    
    var totalAmt= 0.00;
    $('input[type=checkbox].select').each(function () {
       totalAmt +=  (this.checked) ? parseFloat(  $(this).attr("data-value") ) :  parseFloat(0.00); 
    });

    $(".totalcollected").val(totalAmt); 
 });





</script> 

<style>
input[type="checkbox"][readonly] {
  pointer-events: none;
}
</style>
@endsection 