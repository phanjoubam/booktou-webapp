<?php 

$business_owner = $data['business'];

?>


@extends('layouts.admin_theme_02')
@section('content')
 

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">
          <div class="col-md-8">
    <h5>Earning Reports for <span class='badge badge-primary'>{{ $business_owner->name }}</span></h5>
   </div>
    <div class="col-md-4 text-right">
<form class="form-inline" method="post" action="{{  URL::to('/admin/customer-care/business/search-daily-earning')    }}">

            {{ csrf_field() }}
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' name='filter_date' /> 

             <input type="hidden" value="{{ $data['bin'] }}" name="bin">

                <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
            </form>
      </div>
      </div> 
                </div>
            </div>
       <div class="card-body"> 

  <div class="table-responsive">
 
 <table class="table"> 
  <thead class=" text-primary">
    <tr class="text-center"> 
    <th scope="col">Order No.</th>
    <th scope="col">Book Date</th>
    <th scope="col">Order By</th> 
    <th class="text-center" >Status</th>
    <th scope="col">Total Cost</th> 
    
    </tr>
    </thead>

    <tbody>

     <?php $i = 0;

     $total =0;

      ?>
    @foreach ($data['results'] as $item)
    <?php $i++ ?> 
     <tr class="text-center"> 
     <td>{{$item->orderNo}}</td>
     <td>{{$item->book_date->format('d/m/Y')}}</td> 
      <td>{{$item->orderBy}}</td> 
       <td class="text-center" > 

       @switch($item->orderStatus)
                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-info'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span> 
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span> 
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>
      <td>{{$item->totalCost}}</td> 
     
    </tr>
   
    <?php 
    $total += $item->totalCost;
    ?>

    @endforeach
   </tbody>

<tr > 
    <th class="text-right" colspan='3'>Total Sales</th> 
    <th scope="col">{{$data['totalEarning']}}</th> 
    
    </tr> 
  </table>
  
</div>    
  
   </div> 

   </div>

</div>



@if($total > 0)
   
<div class="col-md-12">

 

<div class="card card-default">
       <div class="card-header">
              <div class="card-title"> 
                <div class="row">
          <div class="col-md-12">
            Clearance History 
        </div>
      </div> 


                </div>
            </div>


              <div class="card-body"> 
                  <table class="table">
                    <thead class=" text-primary"> 
 
                  <tr class="text-center" style='font-size: 12px'>  
                     <th class="text-left"   >Reference No.</th>
                    <th class="text-left"  >Date</th> 
                    <th class="text-left"  >Remarks</th> 
                    <th class="text-left">Type</th> 
                    <th class="text-right" >Amount</th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1; 
                  $total_paid  =0;  
                $clearance_status = "HAS DUE PAYMENT" ; 
                foreach ($data['payments'] as $item)
                {

                ?>
  
                  <tr id="tr{{$item->id }}">  
                  
                    <td>{{ date('d-m-Y', strtotime($item->dealing_date )) }}</td>
                     <td>{{$item->remarks}}</td>
                    <td>
                    <strong>{{   $item->transact_no   }}</strong>
                  </td>



                     <td>

                      @switch($item->clearance_type)

                      @case("partial")
                        <span class='badge badge-primary'>PARTIAL</span>
                      @break

                      @case("full")
                        <span class='badge badge-success'>FULL</span>
                        <?php $clearance_status = "PAID" ;?>
                      @break 
  
                      @endswitch

                    </td>   
                    <td class='text-right'>
                      {{ $item->amount }}
                    </td>
                       
                         </tr>
                         <?php
                         $total_paid += $item->amount;
                          $i++; 
                       }

                          ?>
                 <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='4'>Total Paid</th>  
                    <th class="text-right" >{{ $total_paid  }} &#8377;</th>  
                  </tr>

                   <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='4'>Total Due</th>  
                    <th class="text-right" >{{  number_format( $total - $total_paid, 2, ".", "")  }} &#8377;</th>  
                  </tr> 

 
                 <tr style='font-size: 12px'>  
                    <th class="text-right" colspan='4'>Clearance Status</th>  
                    <th class="text-right" ><span class='badge badge-info'>{{  $clearance_status  }}</span></th>  
                  </tr>


                </tbody>
                  </table>
            
              </div>
            </div>

  </div>

 <div class="col-md-6">
 
 <form method="post" action="{{  action('Admin\AdminDashboardController@saveBusinessDailyEarning') }}">
            {{ csrf_field() }}  
 
         <div class="card panel-default ">  
          <div class="card-header">
         Add Payments Information 
       </div>  
<div class="card-body">
 
  <div class="form-row">
    <div class="col-md-6">
       <label for="billing_date">Bill For:</label>
      <input type="text" class="form-control calendar" placeholder="Payment Date" id='billing_date' name='billing_date'>
    </div>

    <div class="col-md-6">
       <label for="pay_date">Payment Date:</label>
      <input type="text" class="form-control calendar" id='pay_date' placeholder="Payment Date" name='pay_date'>
    </div>
    <div class="col-md-6">
      <label for="tsact_no">Transaction No.:</label>
     <input type="text" class="form-control" placeholder="Transaction reference no." id='tsact_no' name="tsact_no">
    </div>
</div>
<div class="form-row">
    <div class="col-md-6">
      <label for="paid_amount">Paid Amount:</label>
      <input type="text" class="form-control" placeholder="Type number only" id='paid_amount' name="paid_amount">
    </div>

     <div class="col-md-6">
      <label for="clearance_type">Clearance Type:</label>
      <select  class="form-control" id="clearance_type" name="clearance_type">
        <option value='partial'>Partial</option>
        <option value='full'>Full</option>
      </select>
    </div>

    <div class="col-md-12">
      <label for="remarks">Remarks:</label>
      <textarea style='border: 1px solid #E3E3E3;' class="form-control" rows='5' id="remarks" name="remarks"></textarea>
    </div>

 <div class="col-md-12">
<input  type="hidden"  value="{{ $data['bin']}}" name='bin'>  
<br/>
     <button type="submit"  name='btnSave' class="btn btn-primary btn-sm"  value='save'>Save</button> 
 </div>
 

  </div> 
          
       

  </div> 
</div>   

  </form>
</div>

 
 
 @endif

</div>
 
     

@endsection

 @section("script")
 
  
 <script>
  
 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 