<?php 
    $total =0;   
    $agent = $data['agent_info'];
    $deposited =false; 
?>

@extends('layouts.admin_theme_02')
@section('content') 

 <div class="row">
   <div class="col-md-12"> 
  
   <div class="card "> 
       <div class="card-body"> 
   
      <table class="table table-bordered mb-3"> 
          <tr class="text-left" style='font-size: 14px'>  
            <th scope="col" colspan="5" style='background-color: #434343; color: #fff'>
              <h4>Collection Report for {{ $agent->fullname  }} ( {{ $agent->phone }} )  
                <span class='badge badge-warning badge-pill'>
                  {{$data['month']}},  {{$data['year']}}
                    
                </span></h4>
                  
            </th>
            <th scope="col" colspan="3" style='background-color: #434343; color: #fff; text-align:right'>
              <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@deliveryAgentMonthlyCollectionReport') }}"> 


             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Select Month:</label> 
              <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='month'>
                 
                    <option value='1'>January</option>
                    <option value='2'>Febuary</option> 
                    <option value='3'>March</option> 
                    <option value='4'>April</option> 
                    <option value='5'>May</option> 
                    <option value='6'>June</option> 
                    <option value='7'>July</option> 
                    <option value='8'>August</option> 
                    <option value='9'>September</option> 
                    <option value='10'>October</option> 
                    <option value='11'>November</option> 
                    <option value='12'>December</option> 
                
                </select>
                 <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Select Year:</label> 
                 <select class='form-control form-control-sm custom-select my-1 mr-sm-2' name='year'>
                 
                   <?php 
                  for($i=date('Y'); $i >=2020; $i--)
                   {
                    ?>
                  <option value='{{ $i }}'>{{ $i }}</option>
                   <?php 
                 }
                 ?>
                   
                    
                
                </select>


             <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
             <input type="hidden" name='aid' value="{{ $agent->id  }}"  /> 
            </form>
            </th>
          </tr>  
          <tr class="text-center" style='font-size: 12px'>  
            <th scope="col" class="text-center" >Order No.</th> 
            <th scope="col" class="text-left">Merchant/Customer</th>
            <th scope="col" class="text-center" >Delivery Date</th>
            <th class='text-right'>Order Type</th>    
            <th class="text-center">Order Status</th> 
            <th class='text-center'>Payment Type</th>                    
            <th class="text-right"  width='120px'>Amount</th> 
            <th class="text-center" width='120px'>Select</th> 
          </tr>
        <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@saveAgentMonthlyCollectionReport') }}"> 
            
            @php
              $i=1; 
              $totalcashamount = $totalonlineamount = 0.0; 
            @endphp 

            @foreach ($data['all_orders'] as $item)
              @php 
                $amountCollected = 0.00;
              @endphp 
              @if($item->member_id == $agent->id )
                <tr >
                  <td class="text-center">
                    @if($item->orderType == "normal")
                    <a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{ $item->orderNo }}">{{ $item->orderNo }}</a> 
                    @elseif($item->orderType == "pnd")
                    <a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                    @else
                    <a target='_blank'  href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                  @endif
                </td> 
                <td>{{$item->orderBy}}</td>
                <td>{{ date('d-m-Y', strtotime($item->service_date))}}</td>  
                <td class='text-center'>
                    @switch( strtolower($item->orderType) ) 
                          @case("pnd")
                            <span class='badge badge-warning'>PnD</span>
                            @break
                          @case("normal")
                            <span class='badge badge-success'>Normal</span>
                            @break

                          @case("assist")
                            <span class='badge badge-info'>Assist</span>
                            @break
  
                        @endswitch  
                </td>
                <td class="text-center"> 
                      @switch($item->orderStatus)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break
                      @case("returned")
                        <span class='badge badge-danger'>Returned</span>
                      @break

                      @case("cancelled")
                      @case("canceled")
                        <span class='badge badge-danger'>Cancelled</span>
                      @break
                       @case("in_route")
                        <span class='badge badge-info'>In Route</span>
                      @break

                      @case("completed")
                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
 
                        <?php 

                          if(  $item->source == "booktou" || $item->source == "customer"  )
                          {
                              if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00;
                                $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                              else //cash collection
                              {
                                $amountCollected = $item->totalAmount + $item->serviceFee ;
                                $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                          }
                          else if(  $item->source == "business" || $item->source == "merchant"  )
                          {
                            if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00; 
                              }
                              else //cash collection
                              {
                                $amountCollected = $item->totalAmount + $item->serviceFee ;
                                $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                          }

                         ?>
 
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>   
                    <td class='text-center'>
                      
                        @switch( strtolower($item->payMode) ) 
                          @case("cash on delivery")
                            <span class='badge badge-success'>Cash</span>
                            @break
                          @case("cash")
                            <span class='badge badge-success'>Cash</span>
                            @break

                          @case("online")
                            <span class='badge badge-warning'>Online</span>
                            @break

                          @case("pay online (upi)")
                            <span class='badge badge-warning'>Online</span>
                            @break

                          @other
                            <span class='badge badge-success'>Cash</span>
                            @break

                        @endswitch 
                    </td>
                   
                    <td class='text-right'  width='120px'>{{ number_format( $amountCollected, 2, '.', '')  }}</td>

                     <td class='text-center'  width='120px'>
                      @if($item->isDeposited =="yes")
                        @if($amountCollected  <=0 )
                          <span class='badge badge-danger badge-pill'>Ignored</span>
                        @else
                          @php
                            $deposited =true;
                          @endphp  
                          <span class='badge badge-success badge-pill'>Deposited</span>
                        @endif
                      @else 
                      <input type='checkbox' class='select' data-value="{{ $amountCollected}}" name='ordernos[]'  @if($amountCollected==0)  checked readonly  @endif value="{{  $item->orderNo }}" />
                      @endif
                     </td>

                  </tr>

                  @endif

                @php
                    $i++;
                @endphp
            @endforeach
            
            <tr class="text-center" style='font-size: 12px'>  
              <th scope="col" class="text-center" >Order No.</th>
              <th scope="col" class="text-left">Merchant/Customer</th>
              <th scope="col" class="text-center" >Delivery Date</th>
              <th class='text-right'>Order Type</th>    
              <th class="text-center">Order Status</th> 
              <th class='text-right'>Payment Type</th>                    
              <th class="text-right" width='120px'>Amount</th> 
              <th class="text-center" width='120px'>Select</th> 
            </tr>
            @if( $deposited )
              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="6" class="text-right">
                <h4>Total to collect from {{ $agent->fullname  }}</h4></th>
              <th scope="col" colspan="1"  > 
                <input type="text" readonly class="form-control form-control-sm form-control-block is-valid"  value="{{ number_format( $totalcashamount ,2, '.', '') }}"  > 
              </th>  
              <th> </th>
              </tr>  
              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="6" class="text-right">
                <h4>Total deposited by {{ $agent->fullname  }}</h4></th>
              <th scope="col" colspan="1"  > 
                 <input type="text" readonly class="form-control form-control-sm form-control-block is-valid"  value="{{ number_format( $totalcashamount ,2, '.', '') }}"  > 
              </th>  
              <th></th>
              </tr> 
            @else 

              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="6" class="text-right">
                <h4>Total to collect from {{ $agent->fullname  }}</h4></th>
              <th scope="col" colspan="1"  > 
                <input type="text" readonly class="form-control form-control-sm form-control-block is-valid"  value="{{ number_format( $totalcashamount ,2, '.', '') }}"  > 
              </th>  
              <th>
                <input type="text" id='totalcollected' name='totalcollected' class="form-control form-control-sm form-control-block  is-valid "    > 
                </th>
              </tr>     

              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="6" class="text-right">
                <h4>Actual Collected</h4></th>
              <th scope="col" colspan='2'  >  
                 <input type="hidden" name='aid' value="{{ $agent->id  }}"  /> 
                  
                <input class='btn btn-primary btn-sm' type="submit"  name='btnsave' value='Save Collection'  >
                </th>
              </tr> 

            @endif
          </form>
       </table>
   
 
     </div>
    </div>
 </div> 
</div>  

@endsection


 @section("script")

<script>
  
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

$('input[type=checkbox].select').change(function() {
    
    var totalAmt= 0.00;
    $('input[type=checkbox].select').each(function () {
       totalAmt +=  (this.checked) ? parseFloat(  $(this).attr("data-value") ) :  parseFloat(0.00); 
    });

    $("#totalcollected").val(totalAmt); 
 });



</script> 

<style>
input[type="checkbox"][readonly] {
  pointer-events: none;
}
</style>
@endsection 