@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 
  
<div class="cardbodyy margin-top-bg"> 

  <div class="col-md-12">
    
  <?php 

    $order_info = $data['order_info'];
    $order_items  = $data['order_items'];
    $customer = $data['customer'];
    $agent = $data['agent_info'];  

  ?> 
    <div class="row"> 
      <div class="col-lg-8"> 
        <div class="card">
              <div class="card-header">
                <h5 class="card-category">Order # {{ $order_info->id  }} 
                @switch($order_info->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                      <span> Order Date: {{ date('d-m-Y H:i:s', strtotime( $order_info->book_date)) }}</span> 
                      OTP: <span class='badge badge-info'>{{ $order_info->otp }}</span>


                    </h5> 
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Sl.No</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Unit Price</th> 
                        <th>Total</th> 
                    </thead>
                    <tbody>
                      <?php $i = 0 ;

                      $sub_total = 0;
                      ?>
               

               @foreach ( $order_items   as $item)

               <?php $i++;

               $sub_total += $item->price * $item->qty;
                ?>

                    <tr >
                      <td>{{$i}}</td>
                      <td>
                      <span> {{$item->pr_name}}  </span>
                    </td>
                      <td>
                      {{$item->qty}} {{$item->unit}}  
                    </td>  
 

                    <td>
                       {{ $item->price }}  
                    </td>  
                     <td>
                       {{  number_format( $item->price * $item->qty, 2, ".", "" ) }}  
                    </td> 


                    </tr>
                   @endforeach
                      
                   <tr>

                    <td colspan="4">
                      <strong>Total:</strong>
                    </td>
                    <td>
                      {{ number_format( $sub_total , 2, ".", "" )   }}
                    </td>

                   </tr>
                   <tr>

                    <td colspan="4">
                      <strong>Delivery Charge:</strong>
                    </td>
                    <td>
                      {{ number_format( $order_info->delivery_charge , 2, ".", "" )   }}
                    </td>

                   </tr>

                   <tr>

                    <td colspan="4">
                      <strong>Total Payable:</strong>
                    </td>
                    <td>
                      {{ number_format(  ( $sub_total+ $order_info->delivery_charge ) , 2, ".", "" )   }}
                    </td>

                   </tr>



                    </tbody>


                  </table>
                </div>
              </div>
            </div>
 </div>

       <div class="col-lg-4">  
       
          <div class="card  ">
              <div class="card-header">
                <h5 class="card-category">Shop/Business</h5>
                <h4 class="card-title">{{$order_info->businessName}}</h4>
                </div>
              <div class="card-body"> 
                 <p>{{$order_info->businessLocality}}</p> 
                  {{$order_info->businessLandmark}}<br/>
                  {{$order_info->businessCity}}<br/>
                  {{$order_info->businessState}} - {{ $order_info->businessPin}}<br/>
                  <i class='fa fa-phone'></i> {{$order_info->businessPhone}}</p> 
              </div>
              <div class="card-footer">
                
              </div>
            </div>
 


            <div class="card  ">
              <div class="card-header">
                <h5 class="card-category">Customer</h5>
                <h4 class="card-title">{{$customer->fullname}} </h4>
               </div>
              <div class="card-body"> 
                 <p>Delivery Address:<br/>
                  {{$order_info->address}}<br/>
                  {{$order_info->landmark}}<br/>
                  {{$order_info->city}}<br/>
                  {{$order_info->state}} - {{ $order_info->pin_code}} <br/>
                  <i class='fa fa-phone'></i> {{$customer->phone}}</p> 
              </div>
              <div class="card-footer">
                
              </div>
            </div>

            @if(isset($agent))
             <div class="card  ">
              <div class="card-header">
                <h5 class="card-category">Delivery Agent</h5>
                <h4 class="card-title">{{$agent->deliveryAgentName}}</h4>
                </div>
              <div class="card-body"> 
                 <p>
                  <i class='fa fa-phone'></i> {{$agent->deliveryAgentPhone}}</p> 
              </div>
              <div class="card-footer">
                
              </div>
            </div>

            @endif


          </div>  

       </div> 
      </div>  

 
@endsection


@section('script')

<script>

$(function() {
    $('.calendar').pignoseCalendar();
});

</script>


@endsection

   