@extends('layouts.admin_theme_02')
@section('content')

<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 


         <div class="card-title">
          <div class="title">Search Daily Expenses</div>
		 </div>
  
         

    </div>
<div class="card-body">
	<form action="{{action('Admin\AdminDashboardController@getExpenseDetails') }}" method="get">
	<div class="form-row">
		<div class="col-md-2">
			 
			<input type="date" name="expensedate" id="expensedate" class="form-control">
		</div>
		<div class="col-md-2">
			<label><small> </small></label>
			<button class="btn btn-primary">Search</button>
		</div>
	</div>
	</form>
<br>


	@if(isset($expense))
	<h7>Expenses for the month of {{$month}} , {{$day}}, {{$year}}

	<button class="btn btn-success redirect" type="button" 
                                data-dkey="{{request()->get('expensedate') }}" data-date="1"><i class="fa fa-info"></i></button>

	</h7><hr>
	<table class="table    table-responsive">
	<thead>
		<tr>
			<th>Use by</th>
			<th>Details</th>
			<th>Amount</th>
			<th>User Date</th>
			<th></th>
		</tr>

		@foreach($expense as $daily)
		<tr>
			<td>{{$daily->fullname}}</td>
			<td>{{$daily->details}}</td>
			<td>{{$daily->amount}}</td>
			<td>{{$daily->use_date}}</td>
			<td> </td>
		</tr>
		@endforeach
	</thead>
</table>
@endif
</div>
</div>
</div>
</div>




@endsection

@section("script")
<script>

 $(".redirect").click(function(){

    var code = $(this).attr("data-date");
    var key = $(this).attr("data-dkey");
    var ser = "//localhost/alpha/";

    switch(code)
    { 
        case "1":
            
            var win = window.open( ser+"admin/staff/e-daily-expense?o=" + key , '_blank');
            if (win) { 
                win.focus();
            } else { 
                alert('Please allow popups for this website');
            } 
            break;
 


    }


});
 
</script>

@endsection

