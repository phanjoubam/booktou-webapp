@extends('layouts.admin_theme_02')
@section('content')
 

 <div class="row"> 
  <div class="col-md-12">
     <div class="card  ">
       <div class="card-header"> 
         <div class="card-title">
          <div class="title"> All Delivery Agents</div>
        </div> 
       
       </div>
       <div class="card-body"> 
                 <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">  
                  <tr   style='font-size: 12px'>  
                    <th></th> 
                    <th  >Full Name/ID</th> 
                    <th class='text-center'>Phone</th> 
                    <th class='text-center'>Category</th>
                    <th class='text-center'>Shift</th> 
                     <th class='text-center'>Shift Starts</th> 
                     <th class='text-center'>Shift Ends</th> 
                    <th class='text-center'></th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1; 
                
                foreach ($data['staffs'] as $item)
                { 
                ?>
  
                  <tr >  
                    <td>
                     {{ $i }}
                   <td  >
                    <strong>{{ $item->fullname }}</strong></td> 
                    <td class='text-center'>{{ $item->phone }}</td>
                    <td class='text-center'>
                      @switch($item->category)

                      @case(100)
                        <span class='badge badge-primary'>Delivery Agent</span>
                        @break
                      @case(1000)
                        <span class='badge badge-info'>Customer Care</span>
                        @break
                      @case(10000)
                        <span class='badge badge-warning'>Business Development</span>
                        @break 

                      @endswitch
                    </td>
                    @php 
                      $found = false ;
                    @endphp 
                @foreach($data['active_staffs'] as $active) 
                   @if($active->staff_id  == $item->id ) 
                      <td class='text-center'><span class='badge badge-success'>{{ $active->shift }}</span></td> 
                      <td class='text-center'>{{ date('H:i:s a', strtotime($active->start_log)) }}</td>
                      <td class='text-center'>
                        @if($active->end_log != null)
                          date('H:i:s a', strtotime($active->end_log))
                        @else
                          ---
                        @endif
                      </td> 
                      <td>
                <div class="dropdown ml-lg-auto ml-3 toolbar-item">
                      <button class="btn btn-primary" type="button" id="dmenuaction" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class='fa fa-cog'></i></button>
                      <div class="dropdown-menu" aria-labelledby="dmenuaction"> 
                        <a class="dropdown-item btnshowlogform" href="#" data-name='{{ $item->fullname }}' data-key='{{ $item->id }}' data-act='u'>Update Attendance</a> 

                        <a class="dropdown-item btnremovestafflog" href="#" data-name='{{ $item->fullname }}' data-id='{{ $item->id }}' data-act='a'>Remove Attendance</a> 
                        <a class="dropdown-item"  
href="{{ URL::to('/admin/delivery-agent/monthly-collection-report')}}?aid={{$item->id}}">Monthly Deposit
</a>

<button class="dropdown-item blockact"  data-action="block" data-key="{{ $item->id}}">Disable Login</button>
                      </div>
                </div> 
              </td> 

                      @php 
                        $found = true ;
                      @endphp 
                      @break
                   @endif 
                @endforeach

                @if( !$found )
                  <td class='text-center'>---</td>
                  <td class='text-center'>---</td>
                  <td class='text-center'>---</td>
                  <td>
                <div class="dropdown ml-lg-auto ml-3 toolbar-item">
                      <button class="btn btn-primary" type="button" id="dmenuaction" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class='fa fa-cog'></i></button>
                      <div class="dropdown-menu" aria-labelledby="dmenuaction">
                        <a class="dropdown-item btnshowlogform" href="#" data-name='{{ $item->fullname }}' data-key='{{ $item->id }}' data-act='a'>Log Attendance</a> 

<a class="dropdown-item"  
href="{{ URL::to('/admin/delivery-agent/monthly-collection-report')}}?aid={{$item->id}}">Monthly Deposit
</a> 
<button class="dropdown-item blockact"  data-action="block" data-key="{{ $item->id}}">Disable Login</button>
                         

                      </div>
                </div> 
              </td> 
                @endif 
                 
     </tr>
                         <?php
                          $i++; 
                       }

                          ?>
                </tbody>
                  </table>
              </div>
              </div>
            </div>
          </div>
   </div>  
  
<form   method="post" action="{{  action('Admin\AdminDashboardController@saveAttendance') }}"> 
  {{ csrf_field() }}
<div class="modal hide fade modallogatt "   role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Save Staff Attendance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body "> 
           
  <div class="form-row">
    <div class="form-group col-md-12">
      <label for="name">Full Name</label>
      <input type="text" readonly class="form-control" id="name">
    </div> 
  </div>
   <div class="form-row">
    <div class="form-group col-md-4">
      <label for="status">Attendance:</label>
      <select class="form-control" id="status" name='status'>
          <option value='present'>Present</option>
          <option value='absent'>Absent</option>
          <option value='leave'>Leave</option>
          <option value='off'>Off</option> 
      </select>
    </div>

    <div class="form-group col-md-4">
      <label for="shift">Shift</label>
      <select class="form-control" id="shift" name='shift'>
          <option value='morning'>Morning</option>
          <option value='afternoon'>Afternoon</option> 
          <option value='full'>Full</option> 
      </select>
    </div>
 <div class="form-group col-md-4">
      <label for="start">On-duty from</label>
      <input type="time" class="form-control" id="start" name='start'  >
    </div>
 

  <div class="form-group col-md-6">
      <label for="start">Reporting Location</label>
      <input type="text" class="form-control" id="location" name='location'  >
    </div>
  </div> 



      </div>
      <div class="modal-footer">
        <input type='hidden' id='key' name="key" />
        <input type='hidden' id='action' name="action" />
        <button type="submit" value='save' name='btnsave' class="btn btn-primary" >Save</button> 
        <button type="button" class="btn btn-secondary" data-action='non' data-dismiss="modal">Cancel</button> 
      </div>
    </div>
  </div>
</div>
</form>


<!-- remove staff attendance log -->

 <form method="post" action="{{action('Admin\AdminDashboardController@removeAttendance')}}">
  {{csrf_field()}}
<div class="modal fade" id="widget-removelog" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="widget-removelog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="widget">Remove Staff Attendance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure?</p>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="logId" id="staffid_log" value="">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">OK</button>
      </div>
    </div>
  </div>
</div>
</form>



 <form method="post" action="{{action('Admin\AdminDashboardController@blockUserAccount')}}">
  {{csrf_field()}}
<div class="modal fade" id="widget-blockact" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="widget-removelog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="widget">Block Staff/Agent Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selected user account will be block. Are you sure about this operation?</p>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="agid" id="agid" value="">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
</form>

@endsection

@section("script")

<script>

 

$(document).on("click", ".btnshowlogform", function()
{

  var k = $(this).attr("data-key"); 
  var a = $(this).attr("data-act");
  var n = $(this).attr("data-name");  

  $("#key").val(k);
  $("#action").val(a); 
  $("#name").val(n); 
  $(".modallogatt").modal('show');

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 

    
    $('.modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('.modal_processing').modal({
      show:true,
      keyboard: false,
      backdrop: 'static'
    });


    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('.modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
   } 

   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/update-customer-confirmation" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.browser_refresh == "yes")
            location.reload(); 
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });
 

})


$(document).on("click", ".btnremovestafflog", function()
{

  var k = $(this).attr("data-id"); 
   

  $("#staffid_log").val(k); 
  $("#widget-removelog").modal('show');

});



 
$(document).on("click", ".blockact", function()
{

  var agid = $(this).attr("data-key");  
  $("#agid").val(agid); 
  $("#widget-blockact").modal('show');

});

 



$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
</script> 

@endsection 