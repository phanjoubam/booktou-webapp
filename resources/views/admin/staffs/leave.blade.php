<p class="mt-2"><b><span class="badge badge-danger">Leave Details</span></b></p> 
<table class="table table-striped table-bordered mt-2">
					 
						<thead>
							<tr>
								<th><small>Leave Date</small></th>  
							</tr>
						</thead>
						<tbody>
							@foreach($staffleave as $leave)
							<tr>
								<td>{{$leave->log_date}}</td>
							</tr>
							@endforeach

						</tbody>
					</table>