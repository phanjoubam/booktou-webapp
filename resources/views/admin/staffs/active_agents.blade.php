@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
 <div class="col-md-12">

  <div class='card'> 
    <div class='card-body'>
      <div class="row">
        <div class="col-md-8">
         <h3> Merchant Wise Total Booking List for:
          <span class="badge badge-success">
            {{ date('Y-m-d',strtotime($data['today'])) }}
          </span>
        </h3>
      </div>
 
    </div> 
  </div> 
</div>
</div>
</div>
<div class="row mt-2">
  <div class="col-md-12">
    <div class='card'> 
      <div class='card-body'>
       <table class="table">
        <thead class=" text-primary">  
          <tr   style='font-size: 12px'>  
            <th></th> 
            <th  >Full Name/ID</th> 
            <th class='text-center'>Phone</th>  
            <th class='text-center'>Current Status</th>
            <th class='text-center'>Action</th>  
          </tr>
        </thead>
        <tbody>
          <?php 
          $i=1;                 
          foreach ($data['agents'] as $item)
          { 
            ?>
            <tr >  
              <td>
               {{ $i }}
               <td  >
                <strong>{{ $item->fullname }}</strong></td> 
                <td class='text-center'>{{ $item->phone }}</td>

                <td class='text-center'>
                  @foreach( $data['agents_status'] as $agent_status)
                    @if($item->id == $agent_status->agent_id )

                      @switch( $agent_status->agent_status )
                        @case('ready-for-task')
                        @case( 'ready-for-multiple-tasks' )
                        <span class='badge badge-primary'>{{ $agent_status->agent_status }}</span>
                        @break

                        @case( 'engaged' )
                        <span class='badge badge-success'>{{ $agent_status->agent_status }}</span>
                        @break
                        @case( 'break' )
                        <span class='badge badge-warning'>{{ $agent_status->agent_status }}</span>
                        @break


                        @case( 'request-end-of-day' )
                        <span class='badge badge-danger'>{{ $agent_status->agent_status }}</span>
                        @break


                      @endswitch
                      
                    @break
                  @endif 
                  @endforeach
                </td>
                @php 
                $found = false ;
                @endphp 
                <td class="text-center">
                  <div class="dropdown ml-lg-auto ml-3 toolbar-item">
                    <button class="btn btn-primary" type="button" id="dmenuaction" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class='fa fa-cog'></i></button>
                    <div class="dropdown-menu" aria-labelledby="dmenuaction"> 

                      <a class="dropdown-item" href="{{ URL::to('/admin/delivery-agent/daily-collection-report')}}?aid={{ $agent_status->agent_id }}">View Report</a>

                      

                    </div>
                  </div> 
                </td> 
              </tr>
              <?php
              $i++; 
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
      
@endsection

@section("script")

<script>  
  $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
  });

</script>

@endsection 