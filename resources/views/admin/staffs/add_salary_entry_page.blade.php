@extends('layouts.admin_theme_02')
@section('content')

 
<?php 
 if(isset($salary)){

 $id =  $salary->id;
 $staffid = $salary->staff_id;
 $name =  $salary->fullname; 
 $basic = $salary->basic_pay;
 $ta = $salary->ta;
 $da =  $salary->da;
 $hra =  $salary->hra;
 $bonus =  $salary->bonus;
 $gross =  $salary->gross;
 $leave =  $salary->no_of_leave;
 $expense =  $salary->deduction;
 $entrydate = date('m-d-Y',strtotime($salary->entry_date));

 }
 
?>
 
 <div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 


         <div class="card-title">
          <div class="title">Salary Entry Page </div>
        </div>
  
        @if (session('err_msg'))
	      <div class="col-md-12"> 
	      <div class="alert alert-danger">
      {{ session('err_msg') }}
		  </div>
		  </div>
      @endif
<form  action="{{action('Admin\AdminDashboardController@saveStaffSalaryEntry') }}" method="post">
      <div class="row">

      	<div class="form-row col-md-4">

        <div class="card-body">
         
         	{{csrf_field()}}
         	<div class="form-row">
         		@if(isset($id))<input type="hidden" value="{{$id}}" name="staffSalaryid">@endif
			 @if(isset($id))
			 
			 @else
			 <div class="col">
			    <label><small>Select Agents:</small></label>
			   
			    <select class="form-control showstafffLeave" name="agentsName" id="agents_name">
			    	<option value="">--Select--</option>
			    	@foreach($staffprofile as $staff)
			    	<option value="{{$staff->id}}">

				        {{$staff->fullname}}

				    </option>
			    	@endforeach
			    </select>

			<small id="alert" class="text-danger"></small> 
			  </div>
			  
			  <div class="col">
			     <label><small>Entry Date</small></label>
			     <input type="date" name="entryDate" id="entry_date" class="form-control" value="">
			     <small id="alert" class="text-danger"></small>
			  </div> 

			  @endif

        	</div>
        	<div class="form-row">

			  <div class="col">
			    <label><small>Basic Pay:</small></label>
			    <input type="number" name="basicPay" id="basic_pay" class="form-control"  
			    @if(isset($basic)) value="{{$basic}}" @else value="0" @endif  
			    placeholder="0.00">
			    <small id="alert" class="text-danger"></small>
			  </div>
			  
			  <div class="col">
			     <label><small>DA</small></label>
			     <input type="number" name="da" id="_da" class="form-control" 
			     @if(isset($da)) value="{{$da}}" @else value="0" @endif 
			    placeholder="0.00">
			    <small id="alert" class="text-danger"></small>
			  </div>
			  <div class="col">
			     <label><small>TA</small></label>
			     <input type="number" name="ta" id="_ta" class="form-control" 
			     @if(isset($ta)) value="{{$ta}}" @else value="0" @endif 
			    placeholder="0.00">
			    <small id="alert" class="text-danger"></small>
			  </div>
			</div>

			<div class="form-row">

			  <div class="col">
			     <label><small>HRA</small></label>
			     <input type="number" name="hra" id="_hra" class="form-control" 
			     @if(isset($hra)) value="{{$hra}}" @else value="0" @endif 
			    placeholder="0.00">
			    <small id="alert" class="text-danger"></small>
			  </div>
			  <div class="col">
			     <label><small>Bonus</small></label>
			     <input type="number" name="bonus" id="_bonus" class="form-control" 
			     @if(isset($bonus)) value="{{$bonus}}" @else value="0" @endif 
			    placeholder="0.00">
			    <small id="alert" class="text-danger"></small>
			  </div> 

        		<div class="col">
			     <label><small>GROSS</small></label>
			     <input type="number" name="gross" id="_gross" class="form-control" 
			     @if(isset($gross)) value="{{$gross}}" @else value="0" @endif 
			    placeholder="0.00">
			    <small id="alert" class="text-danger"></small>
			  </div>

			  	
			</div>
        	 
        	 <div class="form-row">
        	 	<div class="col">
    		 	<label><small>No of Leave:</small></label>
			    <input type="number" name="leave" id="leave" class="form-control" 
			    @if(isset($leave)) value="{{$leave}}" @else value="0" @endif>
				</div>

				<div class="col">
    		 	<label><small>Total Expense:</small></label>
			    <input type="number" name="dailyexpense" id="daily_expense" class="form-control" 
			    @if(isset($expense)) value="{{$expense}}" @else value="0" @endif>
				</div>

				<div class="col">
    		 	<label><small>Total Deduction:</small></label>
			    <input type="number" name="deduction" id="deduction" class="form-control" 
			    @if(isset($expense)) value="{{$expense}}" @else value="0" @endif>
				</div>

        	 </div>
        	


       
        </div>

    </div>

    <div class="col-md-8">
    	<div class="card-body">
    		<div class="form-row">
    		 	
				<div class="col-md-10" id="expense">
					
					@if(isset($id))

					<p><b><span class="badge badge-primary">Expense Details</span></b></p> 
					<table class="table table-striped table-bordered mt-2">
					 	<thead>
							<tr>
								<th><small>Expense Date</small></th>
								<th><small>Details</small></th>
								<th><small>Amount</small></th>
							</tr>
						</thead>
						<tbody>
							@foreach($staffexpense as $staffexpense)
							<tr>
								<td>{{$staffexpense->use_date}}</td>
								<td>{{$staffexpense->details}}</td>
								<td>{{$staffexpense->amount}}</td>
							</tr>
							@endforeach

						</tbody>
					</table>

					@endif
					 
				</div>


				<div class="col-md-4" id="staffLeave">
					
					@if(isset($id))
					<p class="mt-2"><b><span class="badge badge-danger">Leave Details</span></b></p> 
<table class="table table-striped table-bordered mt-2">
					 
						<thead>
							<tr>
								<th><small>Leave Date</small></th>  
							</tr>
						</thead>
						<tbody>
							@foreach($staffleave as $leave)
							<tr>
								<td>{{$leave->log_date}}</td>
							</tr>
							@endforeach

						</tbody>
					</table>

					@endif
				
					
				</div>

			 </div>
    		  
			  
    	</div>
    </div>


</div>
<div class="card-body">
        		<div class="col">
        			@if(isset($id))
        			<button class="btn btn-primary btnSaveSalary" id="btn_staff_salary" value="update" 
        			name="updateStaffSalary">Update</button>
        			@else
        			<button class="btn btn-primary btnSaveSalary" id="btn_staff_salary" value="save"
        			name="saveStaffSalary">Save</button>
        			@endif
        		</div>
        	</div>
</form>  
    </div>
   </div>
</div>
</div>




@endsection

@section("script")
<script>

// var siteurl =  "//booktou.in";

var siteurl =  "<?php echo config('app.url') ?>";
// $('#btn_staff_salary').click(function()
// {

// if ($('#agents_name').val()=="") {
// 	$('#alert').text("please select staff name!");
// 	$('#agents_name').focus();
// 	return false;	
// }

// if ($('#entry_date').val()=="") {
// 	$('#alert').text("please select entry date!");
// 	$('#entry_date').focus();
// 	return false;	
// }
// if ($('#basic_pay').val()=="" || $('#basic_pay').val()=="0") {
// 	$('#alert').text("basic pay must be greater than 0!");
// 	$('#agents_name').focus();
// 	return false;	
// }



// });

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
	
$( "#basic_pay" ).keyup(function() {
 var basic = this.value;
 var da = $('#_da').val();
 var ta = $('#_ta').val();
 var hra = $('#_hra').val();
 var bonus = $('#_bonus').val();
 var expense = $('#daily_expense').val();

 var total = parseInt(basic) + parseInt(da) +parseInt(ta)+parseInt(hra)+parseInt(bonus)-parseInt(expense);
  
 $("#_gross").val(parseInt(total));
 $("#deduction").val(parseInt(expense));

});

$("#_da").keyup(function() {
 var basic = $('#basic_pay').val();
 var da = this.value;
 var ta = $('#_ta').val();
 var hra = $('#_hra').val();
 var bonus = $('#_bonus').val();

 var expense = $('#daily_expense').val();

 var total = parseInt(basic) + parseInt(da) +parseInt(ta)+parseInt(hra)+parseInt(bonus)-parseInt(expense);
  
 $("#_gross").val(parseInt(total));
 $("#deduction").val(parseInt(expense));

});

$("#_ta").keyup(function() {
 var basic = $('#basic_pay').val();
 var da = $('#_da').val();
 var ta = this.value;
 var hra = $('#_hra').val();
 var bonus = $('#_bonus').val();
 var expense = $('#daily_expense').val();

 var total = parseInt(basic) + parseInt(da) +parseInt(ta)+parseInt(hra)+parseInt(bonus)-parseInt(expense);
  
 $("#_gross").val(parseInt(total));
 $("#deduction").val(parseInt(expense));

});

$("#_hra").keyup(function() {
 var basic = $('#basic_pay').val();
 var da = $('#_da').val();
 var ta = $('#_ta').val();
 var hra = this.value;
 var bonus = $('#_bonus').val();
 var expense = $('#daily_expense').val();

 var total = parseInt(basic) + parseInt(da) +parseInt(ta)+parseInt(hra)+parseInt(bonus)-parseInt(expense);
  
 $("#_gross").val(parseInt(total));
 $("#deduction").val(parseInt(expense));
});

$("#_bonus").keyup(function() {
 var basic = $('#basic_pay').val();
 var da = $('#_da').val();
 var ta = $('#_ta').val();
 var hra = $('#_hra').val();
 var bonus = this.value;
 var expense = $('#daily_expense').val();

 var total = parseInt(basic) + parseInt(da) +parseInt(ta)+parseInt(hra)+parseInt(bonus)-parseInt(expense);
    
 $("#_gross").val(parseInt(total));
 $("#deduction").val(parseInt(expense));
});












$('.showstafffLeave').change(function(){

  
      
   $.ajax({
          
              
              url : siteurl +'/admin/staff/get-staff-leave-daily-details',
              data: { 
                    'agentsName': this.value 
              },
              type: 'get',
               
              success: function (response) {
                
                  if (response.success) {

                     
                   $('#leave').val(response.leave);
                   $('#daily_expense').val(response.daily); 
                   $('#expense').empty().html(response.html1); 
                   $('#staffLeave').empty().html(response.html2); 


                    }
                  else {
                     
                $('#expensesuser').empty();
                  }
              }
            
      });




 });

</script>

@endsection