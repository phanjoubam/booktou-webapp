@extends('layouts.admin_theme_02')
@section('content')

<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 


         <div class="card-title">
          <div class="title"> View staff salary list </div>
        </div>

        <div class="card-body">
        	<form action="{{action('Admin\AdminDashboardController@viewStaffSalary') }}" method="get">
        	<div class="row">
        		<div class="col-md-2">
        			 
        			<!-- <input type="date" name="filter_date" id="filter_date" class="form-control"> -->
        			<select name='month' class="form-control form-control-sm  ">
                      <option value='1'>January</option>
                      <option value='2'>February</option>
                      <option value='3'>March</option>
                      <option value='4'>April</option>
                      <option value='5'>May</option>
                      <option value='6'>June</option>
                      <option value='7'>July</option>
                      <option value='8'>August</option>
                      <option value='9'>September</option>
                      <option value='10'>October</option>
                      <option value='11'>November</option>
                      <option value='12'>December</option>

                    </select>
                </div>
                <div class="col-md-2">
                    <select name='year' class="form-control form-control-sm  ">
                      <?php 
                      for($i=2021; $i >= 2020; $i--)
                      {
                        ?> 
                        <option value='{{ $i }}'>{{ $i }}</option>
                      <?php 
                      }
                      ?>

                    </select>
                </div>
        		 <div class="col-md-2"><button class="btn btn-primary">Search</button></div>
        	</div>
        	<br>
        	@if(isset($staffs))
        	<p class=""><b> Staff salary list for the month of  </b> </p>
        	<table class="table responsive table-striped table-bordered">
        		
        			<thead>
        				<tr>
        					<th>Staff Name</th>	
        					<th>Basic Pay</th>
        					<th>DA</th>
        					<th>TA</th>
        					<th>HRA</th>
        					<th>Bonus</th>
        					<th>Gross</th>
        					<th><i class="fa fa-cog "></i></th>
        				</tr>
        			</thead>
        	
        			<tbody>
        				@foreach($staffs as $staff)
        				<tr>
        					<td>{{$staff->fullname}}</td>
        					<td>{{$staff->basic_pay}}</td>
        					<td>{{$staff->da}}</td>
        					<td>{{$staff->ta}}</td>
        					<td>{{$staff->hra}}</td>
        					<td>{{$staff->bonus}}</td>
        					<td>{{$staff->gross}}</td>
        					<td>
        						<a class="btn btn-primary" href="{{url('admin/staff/enter-salary-page')}}?id={{$staff->id}}"><i class="fa fa-pencil"></i></a>
        						<button class="btn btn-success redirect" type="button" 
                                data-key="{{$staff->id}}" data-pgcs="1"><i class="fa fa-info"></i></button>

        					</td>
        				</tr>
        				@endforeach
        			</tbody>
        	</table>
        	@endif


        </form>
        </div>

    </div>
</div>
</div>
</div>




@endsection
@section("script")
<script>

 $(".redirect").click(function(){

    var code = $(this).attr("data-pgcs");
    var key = $(this).attr("data-key");
    var ser = "//localhost/alpha/";

    switch(code)
    { 
        case "1":
            
            var win = window.open( ser+"admin/staff/e-staff-salary-slip?o=" + key , '_blank');
            if (win) { 
                win.focus();
            } else { 
                alert('Please allow popups for this website');
            } 
            break;
 


    }


});
 
</script>

@endsection
