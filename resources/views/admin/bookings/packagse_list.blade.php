@extends('layouts.admin_theme_spark')
<?php 
    $cdn_url =   env('APP_CDN');  
    $cdn_path =   env('APP_CDN_PATH');

?>
@section('cartbutton')
  <li class="nav-item">

    
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-info btn-rounded">Back</button>

  <a role="button" href="{{ URL::to('/admin/services/business/package/booking-date-and-time-selection') }}?bin={{ $data['bin'] }}" class='btn btn-primary btn-rounded'>Next</a> 
</div>

 </li>

@endsection         

@section('content')
 

<div class='hr-md mt-3'></div> 
<div class="container" >
    <div class='row'>
        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 p-1'> 
                <div class="owl-carousel owl-theme">
                  @foreach( $data['categories'] as $package_category)
                        <a class="btn btn-info filter-box " 
                        href="{{ URL::to('admin/services/business/package/search')}}?key={{ ucwords( strtolower($package_category->category ) ) }}&bin={{ $data['bin'] }}&search=true">
               <img width="20px" src="{{ $package_category->icon_url}}"   > {{ ucwords( strtolower($package_category->category ) ) }}</a>
                    @endforeach
                </div>
        </div>
    </div>
</div>
<div class='hr-md '></div>


<div class="container" >
 

<div class="row mt-3">
  @if (session('err_msg'))
  <div class="col-md-12 mb-2">
    <div class="alert alert-info">
      {{ session('err_msg') }}
    </div>
  </div>
  @endif
  
  
  @if( strcasecmp($data['business']->main_module, "appointment") == 0  )
  <table class="table"> 
    <thead class=" text-primary">
      <tr >
        <th scope="col">Image</th>
        <th scope="col">Package</th> 
        <th scope="col">Pricing</th>
        <th scope="col">Discount</th>
        <th scope="col">Actual Pricing</th> 
        <th scope="col">Duration</th>
        <th></th>
      </tr>
    </thead> 
    <tbody> 
    @foreach ($data['packages'] as $package)  
     <tr >
     <td>

      @foreach($data['package_specifications'] as $package_specification)
          @if(  $package_specification->service_product_id  == $package->id  )
            <img src="{{ $package_specification->photos }}" class="align-self-start mr-3 img-rounded" height="50px" width="50px" alt="{{ $package->srv_name }}">
            @break
          @endif 
      @endforeach 
    </td>
      <td>{{ $package->srv_name }}</td>
      <td>{{$package->pricing}}</td>
      <td>{{$package->discount}}</td>
      <td>{{$package->actual_price}}</td> 
      <td>{{$package->duration_hr}} hr {{$package->duration_min}} min</td>
  <td>
 
      <button type="button"  class='btn btn-sm btn-rounded btn-mw btn-outline-dark btnselectitems' 
      data-status="add" 
      data-key="{{ $package->id  }}" 
      data-srvname="{{ $package->srv_name }}" data-price="{{ $package->actual_price }}">Add Package</button>

              </td>
    </tr>
    @endforeach
   </tbody>
  </table>

  @else


  @foreach ($data['packages'] as $item)
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> 
      <div class="card mb-3  rca-2">  
        <div class='card-body'>
          <div class="row align-items-start"> 
            <div class="col-md-12"> 
              <p><span class='h5'>{{$item->srv_name}}</span></p>
              <hr/>
            </div>
            <div class="col-md-5 float-sm-start float-md-end float-lg-end">
               
                @php 
                $active_slider ="active";
                @endphp
                <div id="service_slider{{$item->id}}" class="carousel slide" data-bs-ride="true" >
                 
                    @foreach($data['package_specifications'] as $package_specification)
                    @if(  $package_specification->service_product_id  == $item->id  )
                      <div class="carousel-item {{ $active_slider }}">
                        <img src="{{ $package_specification->photos }}" class="d-block image-rounded w-100" alt="{{ $item->srv_name }}">
                      </div> 
                    @break
                    @endif 
                    @endforeach
              

                  
                </div>   
              </div>

              <div class="col-md-7 float-sm-end float-md-start float-lg-start">
                            <div class='box-content-sm'> 
                                <p>
                                @php 
                                    $len = strlen ( $item->srv_details ); 
                                    if($len > 250) 
                                    {
                                         
                                        echo substr($item->srv_details, 0, 200 ) . ' [+]';
                                    }
                                    else
                                    {
                                          echo $item->srv_details;
                                    } 
                                @endphp  
                             </p>
                         </div> 
                        </div>

            </div>
            </div>
            <div class="card-footer">
             <div class="row align-items-start"> 
                <div class="col-md-7">  
                            <span class="h4">Pricing</span> <span class='orange'>{{$item->actual_price}}₹</span>
                       
                </div>
                <div class="col-md-5 text-right"> 
                    <button class="btn btn-primary btn-rounded btnselectitems" data-status="add" 
                    data-key="{{ $item->id  }}"  data-srvname="{{ $item->srv_name }}" 
                    data-price="{{ $item->actual_price }}">Add</button>
                </div>
              </div>
              </div>
            
          </div>
          </div>
          @endforeach 
    

  @endif


  {{ $data['packages']->links() }}
 
  
   
</div>
</div>
 



@endsection

@section("script") 

 
<script>

$(document).ready(function ()
{
  $(".owl-carousel").owlCarousel({
        margin:10,
        center: false,
        items:2,
        loop:false,
        navigation : false,
        autoWidth:true,
    });



});









  </script>

 @endsection