<?php
if( strcasecmp( request()->query('theme'), 'new') == 0)
{

 $theme = "layouts.admin_theme_03";  
}
else 
{
  $theme = "layouts.admin_theme_02";   
}

 
?> 
@extends( $theme )
@section('content')

   

<?php 

$totalOrders=0;
$target = 20; 
$paa_orders = 0;
$paa_completed_orders =0 ;

if( ($data['picnic']) != 0)
{
  $paa_orders = $data['picnic'];

}

?>

 <div class="row my-flex-card mt-4">
      <div class="col-lg-3 col-sm-6 mb-2">
        <div class="card h-100">
          <div class="card-block">
           <div class="card card-default">
            <div class="card-body easypiechart-panel">
              <h4>Picnic And Activities</h4>
              <div class="easypiechart" id="easypiechart-orange" data-percent="{{ (   $totalOrders  * 100 ) /  80  }}" >

                <span class="percent h3">
                  <a href="{{URL::to('services/view-picnic-bookings?status=new')}}">

                    <span class='badge badge-primary'>{{$data['picnic']}} active</span> </a>

                    <span class='badge badge-warning'>60 target</span> 
                    <a href="{{ URL::to('/services/view-picnic-bookings?status=confirmed')}}">
                      <span class='badge badge-success'>{{$data['completed_picnic']}} done</span></a>
                    </span>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 mb-2">
          <div class="card h-100">
            <div class="card-block">
              <div class="card-body easypiechart-panel">
            <h4>Appointment</h4>
            <div class="easypiechart" id="easypiechart-orange" data-percent="{{ (   $totalOrders  * 100 ) /  80  }}" >
               
              <span class="percent h3">
                <a href="{{ URL::to('/services/view-all-active-appointment?status=new')}}">
                
                <span class='badge badge-primary'>{{$data["appointment_orders"]}} active</span> </a>
                
                <span class='badge badge-warning'>60 target</span> 
                 <a href="{{URL::to('/services/view-all-active-appointment?status=confirmed')}}">
                <span class='badge badge-success'>{{$data["completed_appointment_orders"]}} done</span></a>
              </span>
            
            </div>
          </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 mb-2">
          <div class="card h-100">
            <div class="card-block">
              <div class="card-body easypiechart-panel">
            <h4>Offers</h4>
            <div class="easypiechart" id="easypiechart-orange" data-percent="{{ (   $totalOrders  * 100 ) /  80  }}" >
               
              <span class="percent h3">
                <a href="{{ URL::to('/services/view-all-offers?status=new')}}">
                
                <span class='badge badge-primary'>{{$data['offer_appointment_orders']}} active</span> </a>
                
                <span class='badge badge-warning'>60 target</span> 
                 <a href="{{URL::to('services/view-all-offers')}}?status={{'confirmed'}}">
                <span class='badge badge-success'>{{$data['completed_offer_appointment_orders']}} done</span></a>
              </span>
            
            </div>
          </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 mb-2">
          <div class="card h-100">
            <div class="card-block">
              <div class="card-body easypiechart-panel">
            <h4>Water</h4>
            <div class="easypiechart" id="easypiechart-orange" data-percent="{{ (   $totalOrders  * 100 ) /  80  }}" >
               
              <span class="percent h3">
                <a href="{{ URL::to('services/view-all-water-orders?status=new')}}">
                
                <span class='badge badge-primary'>{{$data["water_orders"]}} active</span> </a>
                
                <span class='badge badge-warning'>60 target</span> 
                 <a href="{{ URL::to('services/view-all-water-orders?status=confirmed')}}">
                <span class='badge badge-success'>{{$data["completed_water_orders"]}} done</span></a>
              </span>
            
            </div>
          </div>
            </div>
          </div>
        </div>
      </div>







@endsection
@section("script")
<script>  
</script> 
@endsection