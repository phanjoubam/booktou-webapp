
@extends('layouts.admin_theme_02')
@section('content') 
<div class="row">

@if (session('err_msg'))
<div class="col-md-12">
  <div class="alert alert-info">
    {{ session('err_msg') }}
  </div>
</div>
@endif

<div class="col-md-8">
  <div class="card">
   
    <div class="card-body">
     <div class="card-title"> 
         <div class="title" style="font-size: 1rem;">Book # <span class='badge badge-warning badge-pill'>{{ $data['order_info']->id}}</span> Order Status:
          @switch( $data['order_info']->book_status)

          @case("new")
          <span class='badge badge-primary badge-pill'>New</span>
          @break

          @case("confirmed")
          <span class='badge badge-info badge-pill'>Confirmed</span>
          @break

          @case("in_route")
          <span class='badge badge-success badge-pill'>In Route</span>
          @break

          @case("completed")
          <span class='badge badge-success badge-pill'>Completed</span>
          @break 
          @case("cancelled")
          <span class='badge badge-danger badge-pill'>Order Cancelled</span>
          @break

          @endswitch

          Order Type:
          <span class='badge badge-success badge-pill' >{{$data['order_info']->book_category}}</span>


          <div class="pull-right">
            <span class="" style="font-weight: bold; font-size:1rem">Payment:</span> 
            @if(  strcasecmp($data['order_info']->payment_type,"ONLINE") == 0   ) 
            <span class='badge badge-success badge-pill'>ONLINE</span>
            @else
            <span class='badge badge-info badge-pill'>CASH</span>
            @endif

            @if(   strcasecmp($data['order_info']->payment_status,"pending") == 0   )  
            <span class='badge badge-warning badge-pill'>PENDING</span>
            @else
            <span class='badge badge-success badge-pill'>PAID</span>
            @endif
          </div> 
        <hr/> 
          Booking Date:
          <span class='badge badge-success badge-pill' >{{ date('d-m-Y', strtotime( $data['order_info']->book_date )) }}</span>


          <div class="pull-right">
          Service Date:
          <span class='badge badge-info badge-pill' >{{ date('d-m-Y', strtotime( $data['order_info']->service_date )) }}</span>

          @if( $data['order_info']->preferred_time != null )
          Time:
          <span class='badge badge-info badge-pill' >{{ date('h:i A', strtotime( $data['order_info']->preferred_time )) }}</span>
          @endif
          </div>
        </div>


        <hr/>
        

      </div> 
      <table class="table table-colored"   >
        <thead class=" ">
          <th style='width: 140px'>Service Type</th> 
           <th style='width: 50px' class='text-right'>GST %</th>
          <th style='width: 100px'  class='text-right'>Pricing</th>
          <th style='width: 100px'  class='text-right'>discount</th>
          <th class="text-right" style='width: 100px'>Sub Total</th> 

          
        </thead>
        <tbody>



          <?php

          $i = 1 ;
          $sub_total = 0;
          $total = 0;
          ?>

          @if(strcasecmp($data['order_type'] ,"booking")==0 && strcasecmp($data['scode'],"WTR")==0)
            @foreach($data['booking'] as $book)
            <?php 
            $total = $book->price*$book->qty;
            $sub_total += $book->price*$book->qty;
            ?>
            <tr>
              <td>{{  $i }}</td>
              <td style='width: 140px'>{{$book->service_name}}</td> 
               @if($book->preferred_time == null)
              <td>NA</td>
             @else
              <td>{{$book->preferred_time}}</td>
              @endif
              <td>qty-{{(int)$book->qty}} x {{$book->price}}</td>
              <td class='text-right'> {{$sub_total}}</td>
            </tr>
            @endforeach  

          @elseif( strcasecmp(  $data['order_type'],"offers")==0)
              @foreach($data['booking'] as $book)

             <?php 
              $total = $book->price*$book->qty;
              $sub_total += $book->price*$book->qty;
              ?>
              <tr>
              <td>{{  $i }}</td>
            <td>{{$book->service_name}}</td>    
              <td class='text-right'>{{$book->price}}</td>

              <td class='text-right'>₹ {{$sub_total}} </td>
              </tr>
          @endforeach

          @else

              @php
                $sub_total =0;
                $service_charge = 0;
              @endphp
              @foreach($data['booking'] as $book)
                @php
                $total = $book->service_price;
                $gst_pc = ($book->cgst_pc + $book->sgst_pc);
                $sub_total += $book->service_price;
                $service_charge += $book->service_charge;
                @endphp
              <tr>
                <td style='width: 140px'>{{$book->service_name}}</td>
                <td class="text-right">{{$gst_pc}} %</td>
                <td class="text-right">₹ {{$book->service_price}}</td>
                <td class="text-right">₹ {{$book->discount}}</td>
                <td class="text-right">₹ {{$book->service_charge}} </td>
              </tr>
              @endforeach


          @endif
 

 @if($data['scode']=="WTR")
        
       <tr>
         <td style="border: none"></td>
         <td style="border: none"></td>
         <td style="border: none"></td> 
         <td><b>Total Payable After Discount :</b></td>
         <td class="text-right">₹ {{number_format($sub_total + $data['order_info']->service_fee  - $data['order_info']->discount , 2, ".", "")  }}</td>
       </tr>

@else

         @if($data['order_info']->coupon)
         <tr>
           <td style="border: none"></td>
           <td style="border: none"></td>
           <td style="border: none"></td> 

           <td>
            <strong>Discount Coupon:</strong> <button data-widget="rem-coupon" data-key="{{ $data['order_info']->id }}" class='btn btn-sm btn-danger showmodal'>Remove</button>
          </td>
          <td class='text-right'>
            <span class='badge badge-info'>{{ $data['order_info']->coupon }}</span> 
          </td> 
        </tr>

        @endif
        <tr> 
          <tr> 
         <td colspan="4"  class='text-right'><b>Total Amount :</b></td>
         <td class="text-right">₹ {{number_format( $service_charge, 2, ".", "")  }}</td>
       </tr>

         <td colspan="4" class='text-right'><b>Total Offer/Discount :</b></td>
         @if($data['order_info']->discount =="")
         <td>0.00</td>
         @else
         <td class="text-right">₹ {{ number_format( $data['order_info']->discount , 2, ".", "" )   }}</td>
       </tr>
       @endif

       

       <tr> 
         <td colspan="4"  class='text-right'><b>Applicable GST:</b></td>
         <td class="text-right">₹ {{number_format(  $data['order_info']->gst , 2, ".", "")  }}</td>
       </tr>

       <tr> 
         <td colspan="4"  class='text-right'><b>Total Payable Amount:</b></td>
         <td class="text-right">₹ {{number_format( $data['order_info']->total_cost + $data['order_info']->gst , 2, ".", "")  }}</td>
       </tr>
@endif
</tbody>       
<tfoot>
       <tr>

         <td colspan="4"  class='text-right'  ><b>Printable e-bill</b></td> 

         @if($data['scode']=="WTR") 
         <td class="text-right">
           <a href="{{  URL::to('/services/download-water-receipt')}}?o={{$data['order_info']->id}}">
             <i class="fa fa-download"></i>
           </a>
         </td>

         @else 
         <td class="text-right">
           <a href="{{  URL::to('/pos/orders/view-bill')}}?o={{$data['order_info']->id}}">
             <i class="fa fa-download"></i>
           </a>
         </td>


         @endif

       </tr>
     </tfoot>  

   </table>
     

    </div>
    
    
  </div>

 
  
</div>
 <!-- customer and shop details sections -->
<div class="col-md-4">  


  <!-- customer remarks ends here -->
  <!-- cc remarks -->
  <div class="card">
    <div class="card-header">
      <div class='row'>
        <div class='col-md-10'>
          <h5 class="card-category">Remarks &amp; Status Update</h5> 
        </div>
        <div class='col-md-2 text-right'>
          <div class="dropdown ">
            <button class="btn btn-primary  " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class='fa fa-cog'></i></button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item showremmodal"   data-key="{{$data['order_info']->id}}" href="#">Add Remarks</a> 
              <a class="dropdown-item showchangestatus"   data-key="{{$data['order_info']->id}}" href="#">Change Status</a> 
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card-body">   
              @if(  $data['order_info']->cust_remarks !=  "")
                <div class='alert alert-warning'>
                  <strong >Customer Remarks</strong><br/>
                  {{ $data['order_info']->cust_remarks }}
                </div>
                <hr/>
              @endif

              @if(isset( $data['remarks'] ) && count($data['remarks']) > 0)
                <div class="timeline timeline-xs">
                  <div class="timeline timeline-xs"> 
                    @foreach($data['remarks'] as $rem)

                        @if($rem->remark_by == 0)
                            @php 
                              $staff_name = "Admin";
                            @endphp
                        @else
                            @php 
                              $staff_name = "Untracked";
                            @endphp
                            @foreach($data['staffs'] as $staff)
                              @if($staff->id == $rem->remark_by)
                                @php 
                                  $staff_name = $staff->fullname;
                                @endphp
                                @break
                              @endif
                            @endforeach
                        @endif
                        <div class="timeline-item">
                          <div class="timeline-item-marker">
                            <div class="timeline-item-marker-text">
                              <span class='badge badge-danger badge-pill white'>{{ date('d-m-Y h:i a', strtotime( $rem->created_at)) }}</span>
                            </div>
                            <div class="timeline-item-marker-indicator bg-red"></div>
                          </div>
                          <div class="timeline-item-content">
                            {{ $rem->remarks}}<br/> 
                            <span class='badge badge-danger badge-pill'>{{ $rem->rem_type}}</span><br/>
                            <strong>Entered by</strong> <span class='badge badge-info badge-pill'>{{ $staff_name}}</span>

                          </div>
                        </div> 

                    @endforeach 
                  </div>
                </div>
                @else 
                  <p class='alert alert-info'>No remarks recorded.</p>
                @endif
 


              </div> 
  </div>  

  <!-- cc remarks ends here -->


  @if($data['scode']=="WTR")
  <!-- water booking section -->
  <div class="card  mt-2">                  
    <div class="card-header" >
     <h5> Merchant Name:</span>
     </h5>
   </div>
   <div class="card-body">

    <h5 class="card-title"> {{$data['business']->name}} </h5>
    <p class="card-text">{{$data['business']->shop_number}}
      <br> {{$data['business']->locality}}

      <br>
      {{$data['business']->landmark}}
      <br>
      {{$data['business']->phone_pri}}
      <span> <i class="fa fa-phone"></i></span> </p>



    </div>
  </div>

  <div class="card mt-3">                  
    <div class="card-header">
     <h5>Customer Details:</span>
     </h5>
   </div>
   <div class="card-body">

     <h5 class="card-title"> 
       {{$data['customer']->fullname}}
     </h5>
     <p>Address:<br/>
      {{$data['customer']->locality}}<br/>
      {{$data['customer']->landmark}}<br/>
      {{$data['customer']->city}}<br/>
      {{$data['customer']->state}} - {{ $data['customer']->pin_code}} <br/>
      <i class='fa fa-phone'></i> {{$data['customer']->phone}}</p>  

    </div>
  </div>
  <!-- water booking section ends here -->
  @else                  


  <div class="card  mt-2">
    <div class="card-header">  
      <h3 class="title">Customer Details</h3>
    </div> 
    <div class="card-body">
      <h4>{{$data['order_info']->customer_name}}</h4>
     <p>Address:<br/>
       {{$data['order_info']->address}}<br/> 
       <i class='fa fa-phone'></i> {{$data['order_info']->customer_phone}}<br/> 
       @if(strcasecmp( $data['order_info']->payment_status , "paid") != 0 )
         <button type="button" class="btn btn-primary btn-xs btn-rounded showsendpaylink" data-key="{{ $data['order_info']->id }}"  >
          Send Payment Link
         </button>
       @endif
    </p>   

    @endif
  </div> 
</div>




  
  @foreach($data['staff'] as $staff)
  <div class="card mt-2">
        <div class="card-header">  
          <h3 class="title">Assigned Staff</h3>
        </div>
    <div class="card-body">
      <ul class="list-group list-group-flush">

        <div class="info-block block-info clearfix">
           <p>{{$staff->fullname}}<br/>
          Email: {{$staff->email}}<br>
          </p>

        </div>
      </ul>
    </div>
  </div>

  @endforeach




  @foreach($data['venue'] as $shop)
  <div class="card mt-2">
     <div class="card-header">  
          <h3 class="title">Business Address</h3>
        </div> 
     @if($shop->preferred_time==null)

     <div class="card-body">
     <h5 class="card-title">{{$shop->name}}:</h5>
     <h6 class="card-subtitle mb-2 text-muted">
     Shop No: <span class="badge badge-secondary">{{$shop->shop_number}}</span></h6>
     <p class="card-text">{{$shop->locality}},
      <br>
      {{$shop->city}} <br>
      <span>{{$shop->phone_pri}}<i class="fa fa-phone"></i></span> </p>

    </div>

   </div> 
    @else
   <div class="card-body ">
      <h5 class="card-title">Address:</h5>
      <p>
        {{$data['customer']->locality}}<br/>
        {{$data['customer']->landmark}}<br/>
        {{$data['customer']->city}}<br/>
        {{$data['customer']->state}} - {{ $data['customer']->pin_code}} <br/>
        <i class='fa fa-phone'></i> {{$data['customer']->phone}}</p>
   </div>

      @endif 
    @endforeach    
             
  <!-- ends here -->

  <!-- booking section ends here -->
               



         </div>

</div>

<!-- modal for remarks update -->
  <div class="modal fade" id="modalRemarks" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
    <form method='post' action="{{ action('Admin\ServiceBookingController@updateCustomerRemarks') }}">
      {{  csrf_field() }}

      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalDisable">Update Remarks</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="card-body">
              <div class="col-md-12">
                <label>Remarks:</label> 
                <textarea rows="3" name="ccremarks" required="" class="form-control" id="ccremarks"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type='hidden' name='orderkey' id='orderkey'/> 
            <input type='hidden' name='bin' value="{{$data['order_info']->bin}}" /> 
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
            <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Update</button>
          </div>
        </div>
      </div>
    </form>
  </div>

   <form action="{{ action('Admin\BookingAppointmentController@removeCouponCodeBooking') }}" method="post">
    {{ csrf_field()  }} 
    <div class="modal" id='widget-rem-coupon' tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h5 class="modal-title">Discount Coupon Removal</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body-content" style='padding: 10px;'>  

           <p>You are about to remove discount coupon from this order. Are you sure?</p> 

           <input type="hidden" id="key" name="key" >
           <div class="clear"></div>
         </div>
         <div class="modal-footer"> 
          <span class='loading_span'></span> 
          <button type="submit" class="btn btn-success" name="btnsave"  value='save'>Remove</button>
          <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Cancel</button> 
        </div>
      </div> 
    </div>
  </div>
</form>

<form action="{{ action('Admin\BookingAppointmentController@updateRemarksBooking') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalrem" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Remarks Update</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"> 

          <div class="form-group">
          <label for="type">Remark Type</label>
          <select  class="form-control" id="type" name='type' aria-describedby="type">
            <option>Customer Feedback</option>
            <option>CC Remarks</option>
            <option>Completeion Remark</option>
          </select>
           
        </div>


          <div class="form-group">
            <label for="remarks">Remarks</label>
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div> 

        </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key3'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save Remarks</button>
          
        </div>
      </div>
    </div>
  </div>
</form>


<form action="{{ action('Admin\BookingAppointmentController@updateBookingOrderStatus') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalstatus" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Order Status Update</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             

        
    <div class="form-row"> 
      <div class="form-group col-md-6">
        <label for="status">Order Status</label>
        <select name="status" class="form-control" id="status">
            <option value='new'>Renew</option>
            <option value='confirmed'>confirmed</option> 
            <option value='cancelled'>Cancelled</option> 
        </select>
      </div>
    </div>
        <div class="form-row"> 
    <div class="form-group col-md-12">
            <label for="remarks">Remarks</label>
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div> 
  </div> 

    
    </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key5'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save Status</button>
          
        </div>
      </div>
    </div>
  </div>
</form>



<div class="modal fade" id="showsendpaybox" tabindex="-1" aria-labelledby="showsendpaybox" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">System Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class='alert alert-info'>A payment link will be sent to the customer's phone number. Please confirm your action.</p>
        <div id="sysbusy"></div>
      </div>
      <div class="modal-footer">
        <input type='hidden' id="wgt-link-key" value="" />
        <button type="button" id='sendlink' class="btn btn-primary btn-rounded">Send Payment Link</button>
        <button type="button" class="btn btn-secondary btn-rounded" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@endsection






@section("script")
<script> 
 $(document).on("click", ".btnremarks", function()
 {
  $("#orderkey").val($(this).attr("data-orderid"));
  $("#modalRemarks").modal("show");
});

 $(document).on("click", ".showmodal", function()
 {
  var widget = $(this).attr("data-widget");
  var key = $(this).attr("data-key");
  $("#key").val(key);
  $("#widget-" + widget ).modal("show") ;

});

 $(document).on("click", ".btnRemoveAgent", function()
 {
  $("#key2").val($(this).attr("data-key"));
  $("#modalConfDelAgent").modal("show")

});

 $(document).on("click", ".showremmodal", function()
{
  $("#key3").val($(this).attr("data-key"));
  $(".modalrem").modal("show")

}); 

$(document).on("click", ".showchangestatus", function()
{
  $("#key5").val($(this).attr("data-key"));
  $(".modalstatus").modal("show")

});
 

$(".showsendpaylink").on("click", function(){

  $("#sysbusy").html("");
  $("#wgt-link-key").val($(this).attr("data-key"));
  $("#showsendpaybox").modal('show');
})



$("#sendlink").on("click", function(){


    var key = $("#wgt-link-key").val();
    var json = {};
    json['key'] = key;
 

    $("#sysbusy").html("<img src='" + siteurl + "/public/assets/image/processing.gif" + "' alt='Loading ...' />");
    $.ajax({
      type: 'post',
      url: api + "v3/web/admin/services/generate-payment-link" ,
      data: json,
      success: function(data)
      {

        data = $.parseJSON(data);
        $("#sysbusy").html("");

        if(data.status_code == 3089)
        {
          $("#sysbusy").html("<p class='alert alert-success'>Payment link sent successfully. You can now close this window.</p>");
        }
        else
        {
          $("#sysbusy").html("<p class='alert alert-success'>Payment link could not be successfully. Please retry again.</p>");
        }
      },
      error: function()
      {
        $("#sysbusy").html("");
        alert(  'Something went wrong, please try again')
      }
    });


})
 

</script>
 



@endsection
