@extends('layouts.admin_theme_02')
@section('content')

<?php 
    $cdn_url =  config('app.app_cdn');  
    $cdn_path = config('app.app_cdn_path');
?>
<div class="row">
<div class="col-md-12"> 
	@if (session('err_msg'))
				       
				      <div class="p-2 alert-info">
				      {{ session('err_msg') }}
				      </div>
				       
	@endif 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-4">
                  <div class="title">Business Servie Category</div> 
                   
                </div>
 <div class="col-md-8 text-right">  
    
      <form method="get" action="{{action('Admin\ServiceBookingController@snsBusinessList')}}">
       
      <div class="row">
		<div class="col-md-4 text-center">
		<select class="form-control selectmodule" name="module" id="module" required> 
				  <option value="">Select Module Name</option>
				  @foreach($module as $items)  
		          <option <?php if(request()->get('module')==$items->moduleName){ echo 'selected';}?> 
		          value="{{$items->moduleName}}">{{$items->moduleName}}</option>
		          @endforeach
		</select>
		</div>
		<div class="col-md-4 paa" style="display: none;">

			<select class="selectize paabin col-md-12"   name="bin" id="bin">
		            	@foreach($paa as $items)  
		            	<option <?php if(request()->get('bin')==$items->id){ echo 'selected';}?> value="{{$items->id}}">{{$items->name}}</option>
		            	@endforeach
		 	</select> 
		</div>
		<div class="col-md-4 sns"  style="display: none;">
			<select class="selectize snsbin col-md-12"   name="bin" id="bin">
		            	@foreach($sns as $items)  
		            	<option <?php if(request()->get('bin')==$items->id){ echo 'selected';}?> value="{{$items->id}}">{{$items->name}}</option>
		            	@endforeach
		 	</select>
		</div>
		<div class="col-md-1">
			<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
		</div>
	  </div> 
 	  </form>

      </div>

      </div>
      </div>
      </div>
  
  </div>

    
  </div> 
</div>

		@if(isset($products))
<div class="row">
	 

 	@foreach($products as $items)

 		<?php 
 		if ($items->photos=="") {
 			
 			$image_url = $cdn_url .  "/assets/image/no-image.jpg";

 		}else{

 			$image_url = $items->photos; 
 		}

 		?> 
		  <div class="col-sm-3 mt-1 mb-1">
		    <div class="card">
		      <div class="card-body">
		        
		        <h5 class="card-title">{{$items->srv_name}} - ₹ {{$items->actual_price}}</h5>
		        
		        <img src="{{$image_url}}" class="img-fluid mb-2">  
		      </div>
		      <div class="card-footer">
		      	 <button class="btn btn-primary editservice"
		      	 data-key="{{$items->id}}"
		      	 data-service="{{$items->srv_name}}" 
		      	 >Edit</button> 
		      </div>

		    </div>
		  </div> 
	 
	@endforeach
	
 </div>
@endif 



<div class="modal fade" id="modalConfirmService" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Admin\ServiceBookingController@saveServiceCategoryPhoto') }}"
  enctype="multipart/form-data">
  {{  csrf_field() }}
   
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Update Service Photo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="card-body">
         	<div class="col-md-12">
         	<label>Section Title:</label>	<input type="text" name="service_name" required="" class="form-control" id="serviceName">
         	</div>
         	<div class="col-md-12">
         	<label>Photo:</label>	<input type="file" name="photo" required="" class="form-control">
         	</div>
         </div>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key'/> 
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Update</button>
      </div>
    </div>
  </div>
 </form>
</div>



@endsection

@section("script")
 
<script> 
$(document).on("click", ".editservice", function()
{

    $("#key").val($(this).attr("data-key"));
    $("#serviceName").val($(this).attr("data-service"));
    $("#modalConfirmService").modal("show")

});


$( document ).ready(function() {
     
	<?php 
	 
	if (request()->get('module')!= "") {

		if(request()->get('module')=="BOOKING")
		{
		?> 
			$('.paa').css('display','block');
			$('.sns').css('display','none');
			$('.snsbin').removeAttr('name');
			$('.paabin').attr("name","bin");
		<?php

		}elseif(request()->get('module')=="APPOINTMENT"){
		?> 
			$('.paa').css('display','none');
			$('.sns').css('display','block');
			$('.paabin').removeAttr('name');
			$('.snsbin').attr("name","bin");
		<?php
		}

	} 
	 
	?>

});

$(".selectmodule").on('change',function(){ 
 


	if ($('.selectmodule').val()=="") {
		$('.selectmodule').focus();
		return;
	}else if ($('.selectmodule').val()=="BOOKING") {
			$('.paa').css('display','block');
			$('.sns').css('display','none');
			$('.snsbin').removeAttr('name');
			$('.paabin').attr("name","bin");
	}
	else if ($('.selectmodule').val()=="APPOINTMENT") {
			$('.paa').css('display','none');
			$('.sns').css('display','block');
			$('.paabin').removeAttr('name');
			$('.snsbin').attr("name","bin");
	}
    

});


 $('.selectize').select2({
        selectOnClose: true
 }); 


 

</script>
@endsection