@extends('layouts.admin_theme_02')
@section('content')


<div class="row">
    
<div class="col-md-3"> 
  <label >Select Date:</label>
  <input type="date" class="form-control input-calendar" id="service_from" name="filter_date">
</div>
<div class="col-md-3">
  <label >Search by Merchant Name:</label>
  <input type="text" class="typeahead form-control" id="merchantID"> 
</div>


<div class="col-md-3"> 
</div>
<div class="col-md-12"> 
<div id="divResult">  
</div>
</div>
</div>  

 


<!-- Modal HTML -->
 <div id="showModal" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header flex-column text-center">
        <h4 class="modal-title w-100 text-center"><i class="fa fa-trash text-center"></i> Are you sure? </h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <p>Do you really want to delete these records? This process cannot be undone.</p>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="deleteCancel">Cancel</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirm">Delete</button>
      </div>
    </div>
  </div>
</div> 
 

@endsection

@section("script")

<script>

//calender ends here
//auto suggestion part

var path = 'auto-businees-name';

$( "#merchantID" ).autocomplete({

        source: function( request, response ) {

          // Fetch data

          $.ajax({

            url:path,
            type: 'get',
            dataType: "json",
            data: {
              _token: $('input[name=_token]').val(),
               search: request.term
            },

            success: function( data ) {
               response( data );
            }
          });
        },

        select: function (event, ui) {
          $('#merchantID').val(ui.item.label);
           return false;
        }

      });







$('#merchantID').keypress(function(event){
 if (event.keyCode == 13)
 {
   event.preventDefault();
   fetchRecords();
 }
});


function fetchRecords(){
        StartLoading();
       $.ajax({
         url: 'get-bookings-by-bin',
         type: 'get',
         data: {'merchantcode':$('#merchantID').val(),
                'filter_date':$('#service_from').val()
              },
         dataType: 'json',
         contentType: 'application/json',
         success: function(response){
        if(response.success)
                {
                  $('#divResult').empty().append(response.html);
                  StopLoading();
                }
                else
                  {
                      StopLoading();
                      alert(response.msg);

                  }
 

         }
       });
}


//delete booking section
  function deleteBooking(action,key) {
   
  var key =key; 
  if (action == 'delete') {
        
  var modalConfirm = function(callback){
  
  $("#deleteConfirm").on("click", function(){
    callback(true);
  });
  
  };


  modalConfirm(function(confirm){

  if(confirm){
   
    $.ajax({

        type: 'get',
        url : 'remove-bookings-by-bin',
        type: 'get',
        data: {'merchantcode':$('#merchantID').val(),
                'filter_date':$('#service_from').val(),
                'key': key
              },
        dataType: 'json',
        contentType: 'application/json',
        success: function (response) {
           
            if (response.success) {
                $("#divResult").empty().append(response.html);
              }
            else {
                 alert("Unable to perform action");
            }
        }
    });

  }else{
   
    
     //$("#showModal").modal('hide');
     //return false;
  }
  });
}

    
}

function StartLoading(){
    $('#loadSpinner').show();
}
function StopLoading(){
    $('#loadSpinner').hide();
}

 
</script>

@endsection