@extends('layouts.admin_theme_spark')
<?php 
    $cdn_url =   env('APP_CDN');  
    $cdn_path =   env('APP_CDN_PATH');
?>

 
 @section('cartbutton')
  <li class="nav-item"> 
<div class="btn-group" role="group" aria-label="Basic example">
  
    <a type="button" class="btn btn-info btn-rounded" href="{{ URL::to('/admin/services/business/package/search') }}?bin={{ $data['bin'] }}" >Back</a>


  <a role="button" href="{{ URL::to('/admin/services/business/package/review-order-summary') }}?bin={{ $data['bin'] }}" class='btn btn-primary btn-rounded'>Next</a> 
</div>

 </li>

@endsection   
 

@section('content')
  


<div class="container-fluid" >

  @if (session('err_msg'))
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-danger">
      {{ session('err_msg') }}
    </div>
  </div> 
</div>
  @endif

<div class="row">
   <div class="col-md-4">
      <div class="card card-default">
      <div class="card-header"> 
        <p style='font-size:18px'><strong>Service Calendar</strong></p>
      </div>
       <div class="card-body"> 
    <div id="slotcalendar"></div>
  </div>
</div> 
    
  </div>

 <div class="col-md-5">
   <div class="card card-default">
      <div class="card-header">
        <p style='font-size:18px'><strong>Available Time Slot</strong></p>
      </div>
       
       <div class="card-body"> 
        <div id="slotarea" class="text-center">

          @if($data['timeslots']->count() > 0 )
            @foreach( $data['timeslots'] as $slot)
              @if($data['slot_status'] == "close" )
              <div class="slotbox-disabled" data-msg="This time slot is already booked!" data-slot="{{ $slot->slotNo }}" role="button">Time Slot # {{ $slot->slotNo }}<br/>
                {{ date('h:i', strtotime( $slot->startTime ) )   }} - {{ date('h:i A', strtotime( $slot->endTime) )  }}
              </div>
              @else
              <div class="slotbox" data-slot="{{ $slot->slotNo }}" role="button">Time Slot # {{ $slot->slotNo }}<br/>
                {{ date('h:i', strtotime( $slot->startTime ) )   }} - {{ date('h:i A', strtotime( $slot->endTime) )  }}
              </div>
              @endif
            @endforeach
          @else

            <div class="slotbox-disabled" data-msg="This time slot is already booked!"   role="button">
               No available time slot.
            </div>
          @endif
        </div>
      </div>
</div>
</div>


 <div class="col-md-3">
      <div class="card card-default">
      <div class="card-header">
        <p style='font-size:18px'><strong>Select Professional</strong></p>
      </div>
      <div class="card-body text-left"> 
        <div id="availablestaff">

          @foreach($data['staffs']  as $staff )
            
            @if( $staff->profilePicture == null )
              @php 
                $imgUrl = "https://cdn.booktou.in/assets/image/no-image.jpg";
              @endphp
            @else
              @php 
                $imgUrl = $staff->profilePicture;
              @endphp
            @endif

            <div style='min-height:100px;'>

              <img src='{{ $imgUrl }}' width='90px' class='rounded mx-auto d-block' style='display:inline-block; float:left'/>
              <p class='h5'>{{ $staff->staffName }}</p>
              <button type='button' data-key='{{ $staff->staffId }}' class='btn btn-primary btn-xss btn-rounded btnselectstaff'>Select Staff</button>
              
            </div>
            <hr/>
            
          @endforeach

        </div>
      </div>
</div> 
    
  </div>

 </div>

</div> 

@endsection

@section("script") 


<script>
 
$(document).ready(function ()
{
  var bin = <?php echo $data['business']->id;?>;
  var gmid =$("#gmid").html();
  var today = new Date();
  var d = String(today.getDate()).padStart(2, '0');
  var m = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var y = today.getFullYear();
  const dates = [];
  
  for(var i = 0; i < d-1; i++)
  {
    day = i+1;
    if(day<10)
      day = "0" + day;

    dates[i] = {"date": y + "-" + m + "-" + day , "badge":false, "classname": "bg-red disablebtn", "title":"Not available"}; 
  }
  dates[i] = {"date": y + "-" + m + "-" + d  , "badge":false, "classname": "bg-sgreen", "title":"Today"}; 


  $("#slotcalendar").zabuto_calendar({
        data: dates,
        cell_border: true,
        show_days: true,
        weekstartson: 0,
        legend: [
            {type: "block", label: "Today", classname: "bg-sgreen"},
            {type: "block", label: "All Booked",  classname: "bg-red"}
          ],
        nav_icon: {
          prev: '<i class="fa fa-chevron-left"></i>',
          next: '<i class="fa fa-chevron-right"></i>'
        },
        action: function () {
          return calendarDateChange(this.id, bin, gmid, false);
      },

    });
 

    $(".owl-carousel").owlCarousel({
        margin:10,
        center: false,
        items:2,
        loop:false,
        navigation : false,
        autoWidth:true,
    }); 

});


</script>

 @endsection