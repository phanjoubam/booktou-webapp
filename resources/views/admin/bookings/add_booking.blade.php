@extends('layouts.admin_theme_02')
@section('content')


   <div class="row">
     <div class="col-md-12"> 
     <div class="card  "> 
             <div class="card-header"> 
            <h1 class="text-center">Add New Service Booking Form</h1>
             </div>
             
	 @if (session('msg'))
    <div class="col-md-3 alert alert-success">
        {{ session('msg') }}
    </div>
@endif
<form method="post" action="{{action('Admin\ServiceBookingController@saveServiceBooking')}}" 
id="ShowServiceProduct" >
  {{csrf_field()}}


<div class="row">
	<div class="col-md-3"></div>
		<div class="col-md-3">
			<label >Service Category</label>
    <select class="form-control" name="sbservicecat" 
    onchange="GetProduct();">
  <option>--Select Service Category--</option>
 	  @foreach($categories as $cat)
      <option value="{{$cat->category}}">{{ $cat->category}}</option>
      @endforeach
  </select>
</div>
<div class="col-md-3">
<div id="showproductlist">
			 	

</div>

  </div>
<div class="col-md-3"></div>

</div>

<div class="row">
  <div class="col-md-3"> 
       
    </div>


    <div class="col-md-3">
    <label>Service (Hr)</label>
    <select name="shour" class="form-control">
    	<option value="0">0</option>
    	<option value="1">1</option>
    	<option value="2">2</option>
    	<option value="3">3</option>
    	<option value="4">4</option>
    	<option value="5">5</option>
    	<option value="6">6</option>
    	<option value="7">7</option>
    	<option value="8">8</option>
    	<option value="9">9</option>
    	<option value="10">10</option>
    	<option value="11">11</option>
    	<option value="12">12</option>
    	<option value="13">13</option>
    	<option value="14">14</option>
    	<option value="15">15</option>
    	<option value="16">16</option>
    	<option value="17">17</option>
    	<option value="18">18</option>
    	<option value="19">19</option>
    	<option value="20">20</option>
    	<option value="21">21</option>
    	<option value="22">22</option>
    	<option value="23">23</option>
    	
    </select>
    </div>
    
    <div class="col-md-3">
    <label>Service (min)</label>
    <select name="smin" class="form-control">
    	<option value="0">0</option>
    	<option value="5">5</option>
    	<option value="10">10</option>
    	<option value="15">15</option>
    	<option value="20">20</option>
    	<option value="25">25</option>
    	<option value="30">30</option>
    	<option value="35">35</option>
    	<option value="40">40</option>
    	<option value="45">45</option>
    	<option value="50">50</option>
    	<option value="55">55</option>
    </select>
    	
    </div>
	<div class="col-md-3">
    </div>

  </div>



<div class="row">
  <div class="col-md-3"> 
       
    </div>


    <div class="col-md-3"><label>Service Date</label>
    <input type="date" class="form-control input-calendar" name ="sbdate" id="sbdate">
    </div>
    
    <div class="col-md-3">
      <label >Payment Type</label>
    <select class="form-control" name="sbpayment">
  <option value="0">--Select Payment Type--</option>
  <option value="COD">COD</option>
  <option value="NetBanking">Net Banking</option>
  <option value="Card">Card</option>
  </select>
    </div>
<div class="col-md-3">
      <label></label>
    
    </div>

  </div>




<div class="row">
	<div class="col-md-3">
      
    </div>
	<div class="col-md-3">
      <label >Seller Payable</label>
    <input type="number" class="form-control" id="sbsellerpayable" name="sbsellerpayable">
    </div>
    <div class="col-md-3">
      <label >Service Fee</label>
    <input type="number" class="form-control" id="sbfee" name="sbfee">
    </div>
    <div class="col-md-3">
      
    </div>
</div>


<div class="row">
	<div class="col-md-3">
      
    </div>
    <div class="col-md-3">
      <label >Discount</label>
    <input type="number" class="form-control" id="sbdiscount" name="sbdiscount">
    </div>
    <div class="col-md-3">
	<label >Coupon Code</label>
    <input type="text" class="form-control" id="sbcoupon" name="sbcoupon">
	</div>
	<div class="col-md-3">
      
    </div>
</div>

<br>
<div class="row">
	<div class="col-md-3">
      
    </div>
    <div class="col-md-3">
     <input type="submit" class="btn btn-primary" value="Save" name="btn_save" id="btn_save"> 
     <input type="button" class="btn btn-danger" value="Cancel" name="btn_cancel" id="btn_cancel"> 
    <div class="col-md-3">
      
    </div>
    <div class="col-md-3">
      
    </div>
</div>

</form>

</div>



</div>
</div>




@endsection

@section("script")


<script type="text/javascript">
    //<![CDATA[
    $(function () {
        $('#wrapper .version strong').text('v' + $.fn.pignoseCalendar.version);

        function onSelectHandler(date, context) {
            /**
             * @date is an array which be included dates(clicked date at first index)
             * @context is an object which stored calendar interal data.
             * @context.calendar is a root element reference.
             * @context.calendar is a calendar element reference.
             * @context.storage.activeDates is all toggled data, If you use toggle type calendar.
             * @context.storage.events is all events associated to this date
             */

            var $element = context.element;
            var $calendar = context.calendar;
            var $box = $element.siblings('.box').show();
            var text = 'You selected date ';

            if (date[0] !== null) {
                text += date[0].format('YYYY-MM-DD');
            }

            if (date[0] !== null && date[1] !== null) {
                text += ' ~ ';
            }
            else if (date[0] === null && date[1] == null) {
                text += 'nothing';
            }

            if (date[1] !== null) {
                text += date[1].format('YYYY-MM-DD');
            }

            $box.text(text);
        }

        function onApplyHandler(date, context) {
            /**
             * @date is an array which be included dates(clicked date at first index)
             * @context is an object which stored calendar interal data.
             * @context.calendar is a root element reference.
             * @context.calendar is a calendar element reference.
             * @context.storage.activeDates is all toggled data, If you use toggle type calendar.
             * @context.storage.events is all events associated to this date
             */

            var $element = context.element;
            var $calendar = context.calendar;
            var $box = $element.siblings('.box').show();
            var text = 'You applied date ';

            if (date[0] !== null) {
                text += date[0].format('YYYY-MM-DD');
            }

            if (date[0] !== null && date[1] !== null) {
                text += ' ~ ';
            }
            else if (date[0] === null && date[1] == null) {
                text += 'nothing';
            }

            if (date[1] !== null) {
                text += date[1].format('YYYY-MM-DD');
            }

            $box.text(text);
        }

        // Default Calendar
        $('.calendar').pignoseCalendar({
            select: onSelectHandler
        });

        // Input Calendar
        $('.input-calendar').pignoseCalendar({
            theme:'blue'
            
        });

       
    });
    //]]>


function showProduct()
{

    $.ajax({
        type: 'Get',
        url: "{{action('Admin\ServiceBookingController@showServiceProduct')}}",
        contentType: 'application/json',
        dataType: 'json',
        success: function (response) {
        	 
            if (response.success) {
            	
                $('#showproductlist').empty().append(response.html);
            }
            else {
                 alert("Unable to load product list");
            }
        },
        error: function () {
             alert("Technical error!");
        }
    });
}



function GetProduct() {
    var formData = new FormData($('#ShowServiceProduct')[0]);
    $.ajax({
        type: 'POST',
        url: "{{action('Admin\ServiceBookingController@showServiceProduct')}}",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
        	 
            if (response.success) {
                $('#showproductlist').empty().append(response.html);
            }
            else {
                 alert("Unable to load product list");
            }
        },
        error: function () {
             alert("Technical error!");
        }
    });
}



function Setproductname(item)
{
  var selected = $(item).find("option:selected").text();
  $("#sproduct").val(selected);
} 

  


</script>
@endsection 