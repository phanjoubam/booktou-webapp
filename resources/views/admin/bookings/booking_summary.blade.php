@extends('layouts.admin_theme_spark')
<?php 
    $cdn_url =   env('APP_CDN');  
    $cdn_path =   env('APP_CDN_PATH');

    $total_amount =0.00;
    $total_gst =0.00;
    $total_discount =0.00;
    $actual_amount =0.00;
?>


 @section('cartbutton')
  <li class="nav-item">
    <a type="button" class="btn btn-info btn-rounded" href="{{ URL::to('/admin/services/business/package/booking-date-and-time-selection') }}?bin={{ $data['bin'] }}" >Back</a>
 </li>

@endsection 



@section('content')
 
<div class='container-fluid'>


  <form method="post" action="{{action('Admin\ServiceBookingAppointmentController@placeBookingReservation')}}" enctype="multipart/form-data">
    {{csrf_field()}} 
  
<div class="row">
  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
      {{ session('err_msg') }}
    </div>
  </div>
  @endif

  <div class="col-md-8">
    <div class="card">
  <div class="card-header">
    Booking Summary
  </div>
  <div class="card-body">

  <div class="table-responsive">

  


  <table class="table"> 
      <thead class=" text-primary">
        <tr >
          <th scope="col">Image</th>
          <th scope="col">Package</th> 
          <th scope="col">Pricing</th>
          <th scope="col">Discount</th>
          <th scope="col">Actual Pricing</th> 
          <th scope="col">Duration</th>
          <th></th>
        </tr>
      </thead>
    <tbody>

    @foreach ($data['packages'] as $package)

    @php

      $sub_total = ($package->pricing -  $package->discount);
      $total_amount += $package->pricing;
      $total_discount += $package->discount;
      $sub_gst = ( $sub_total * 0.01 * ( $package->cgst_pc + $package->sgst_pc )  );
      $total_gst +=$sub_gst;
      $actual_amount += $sub_total + $sub_gst;
    @endphp
     <tr >
     <td>

      @foreach($data['package_specifications'] as $package_specification)
          @if(  $package_specification->service_product_id  == $package->id  )
            <img src="{{ $package_specification->photos }}" class="align-self-start mr-3 img-rounded" height="50px" width="50px" alt="{{ $package->srv_name }}">
            @break
          @endif 
      @endforeach 
    </td>
      <td>{{ $package->srv_name }}</td>
      <td>{{$package->pricing}}</td>
      <td>{{$package->discount}}</td>
      <td>{{$package->actual_price}}</td> 
      <td>Whole Day</td>
  <td>
      <input type="hidden" name="packageNos[]" value='{{ $package->id  }}'>
      <button type="button"  class='btn btn-sm btn-rounded btn-mw btn-outline-dark btnselectitems' 
      data-status="rem" 
      data-key="{{ $package->id  }}" 
      data-srvname="{{ $package->srv_name }}" data-price="{{ $package->actual_price }}">Remove</button>
    </td>
    </tr>
    @endforeach
   </tbody>
  </table>
 

<hr/>

 <table class="table">
    <tbody>
      <tr >
        <th scope="col">Service Date</th>
        <th scope="col">
          {{ date('Y-m-d', strtotime($data['service_date'])) }}
          <input type="hidden" name="serviceDate" id="bookingServiceDate" value="{{ date('Y-m-d', strtotime($data['service_date'])) }}">
        </th>
      </tr>

      <tr >
        <th scope="col">Service Serial No.</th>
        <th scope="col">{{ $data['timeslot']->slotNo }}
        <input type="hidden" name="serviceTimeSlot" id="serviceTimeSlot" value="{{ $data['timeslot']->slotNo }}">
      </th>
      </tr>
      <tr >
        <th scope="col">Service Time</th>
        <th scope="col">{{ date('h:i A', strtotime($data['timeslot']->startTime )) }} - {{ date('h:i A', strtotime($data['timeslot']->endTime)) }}</th>
      </tr>
      <tr >
        <th scope="col">Selected Staff</th>
        <th scope="col">{{ $data['staff']->staffName }}
        <input type="hidden"  id="staffId" name="staffId" value="{{ $data['staff']->staffId }}" ></th>
      </tr>

      <tr >
        <th scope="col">Total Amount</th>
        <th scope="col">{{ $total_amount  }}</th>
      </tr>
      <tr >
        <th scope="col">Discount</th>
        <th scope="col">{{ $total_discount  }}</th>
      </tr>


      <tr >
        <th scope="col">GST (5% of the total amount)</th>
        <th scope="col">{{ $total_gst  }}</th>
      </tr>


      <tr >
        <th scope="col">Actual Amount</th>
        <th scope="col">{{ $actual_amount   }}
          <input type="hidden" id="subtotal" value="{{ $actual_amount   }}"> </th>
      </tr>

    </tbody>
  </table> 

 </div>

  </div>
</div>

  </div> 
    <div class="col-md-4">
    <div class="card">
  <div class="card-header">
    Customer Details
  </div>
  <div class="card-body">

    <div class="form-group">
    <label for="cname">Customer Name</label>
    <input type="text" class="form-control" required id="cname" name="cname" >
  </div>
 

<div class="form-row">
  <div class="col-md-6">
    <label for="cphone">Customer Phone</label>
    <input type="number" maxlength="10" required class="form-control" id="cphone" name="cphone">
  </div>

 <div class="col-md-6">
    <label for="preftime">Preferred time of visit</label>
    <input type="text" maxlength="10" required class="form-control showtime" id="preftime" name="preftime">
  </div>
 </div>

  <div class="form-group mt-2">
    <label for="caddress">Customer Address</label>
    <textarea  class="form-control" id="caddress" name="caddress"></textarea>
  </div>

  <div class="form-group mt-2">
    <label for="caddress">Additional Requests/Remarks:</label>
    <textarea  class="form-control" id="bookingRemark" name="bookingRemark"></textarea>
  </div>

  <div class="form-row">
    <p class='alert alert-info'>If the phone number provided is already registered with us, 
    we can show the order to him/her account. Click on the checkbox below to link this order with existing customer.</p>
  <div class="col-md-12 m-3"> 
      <input class="form-check-input" type="checkbox" id="linkcustomer" name="linkcustomer" value="yes">
      <label class="form-check-label" for="gridCheck">
        Link order with registered customer
      </label> 
  </div>
  </div>

  </div>


    <div class="card-footer text-right">
      <input type="hidden" name="bin" id="bin" value="{{ $data['business']->id}}" > 
      <button type="submit" class="btn btn-primary btn-rounded btn_bookcheckout" value="save" name="save">Save</button>
    </div>
</div>
</div>
</div>
</form>
</div>
<div class='mt-4'></div>

@endsection

@section("script")
 
 
<script>


$('.showtime').timepicker({
    timeFormat: 'h:mm p',
    interval: 30,
    minTime: '10',
    maxTime: '7:00pm',
    defaultTime: '11',
    startTime: '9:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});



</script>

 @endsection