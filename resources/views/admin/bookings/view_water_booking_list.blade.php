@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
     <div class="col-md-12">

    <div class='card'> 
      <div class='card-body'>
        <div class="row">
                <div class="col-md-8">
                 <h3>Water Booking List for:
                 	<span class="badge badge-success">
                 	{{date('Y-m-d',strtotime($sdate))}}	 
                 </span>
               	 </h3>
                </div>
                <div class="col-md-4">
                 
                 
                </div> 
 </div> 
                </div> 
           </div>
     </div>
 </div>
<div id="divResult" class="mt-2">
<div class="row" >  
  
   {{csrf_field()}}  
     <div class="col-md-12"> 
                   
                               @if( count($result) > 0 ) 
                               <div class='card'>
                               <div class="card-body">
                               <table class="table" >
                                    <thead>
                                             <th >Order #</th>
                                            <th  >Booking Date</th> 
                                            <th  >Service Time</th>
                                            <th  >Customer</th>
                                            <th  >Total Cost</th>  
                                            <th  >Payment Type</th>
                                            <th  class='text-center'>Action</th>
                                          
                                    </thead>
                                    <tbody>
                                    	@foreach($result as $items)
                                    		<tr>
                                    			<td>
                                    				<a href="{{ URL::to('/services/booking/view-details') }}/{{$items->id}}?bin={{$items->bin}}" target='_blank'  class="badge badge-primary"> {{$items->id}}</a>
                                    			</td>
                                    			<td>{{$items->book_date}}</td>
                                    			<td>{{date('h:i a', strtotime( $items->preferred_time ))}} </td>
                                    	 		<td>{{$items->customer_name}}</td>
                                    		    <td>{{$items->total_cost}}</td>
                                    			<td>{{$items->pay_mode}}</td>
                                    			<td class="text-center"> 

                                    			<div class="btn-group" role="group" aria-label="Basic example">
												                                             
												 <a class="btn btn-info " href="{{ URL::to('/services/booking/view-details') }}/{{$items->id}}?bin={{$items->bin}}" target='_blank'>
												                                              <i class="fa fa-info-circle"></i>
												                                            </a> 
												<button type="button" class="btn btn-warning btn-process" data-widget="1"  data-key="{{ $items->id   }}" data-pgc='1' data-bin="{{$items->bin}}"><i class="fa fa-cog"></i></button>



												<button type="button" class="btn btn-success redirect" 
												data-key="{{ $items->id   }}" data-pgc='4' data-bin="{{$items->bin}}"><i class="fa fa-download"></i></button>
												 
												                                          </div></td>
                                    		</tr>
                                    	@endforeach
                                    </tbody>
                                 </table>
								</div>
								</div>
								@else
									<p class='alert alert-info'>No New booking to show.</p>
								@endif
</div>
</div>
</div>


<form method="post" action="{{action('Admin\ServiceBookingController@updateWaterBookingStatus')}}" 
id="ShowServiceProduct" >
{{csrf_field()}}
 <div class="modal" id="wg1" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Booking Progress Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
  
      <div class="form-group row">
      <label for="status" class="col-sm-12 col-md-4 col-form-label">Booking Status:</label>
      <div class="col-sm-12 col-md-8">
      <select id="status" name='status' class="form-control">
        <option value='engaged'>Engage</option>
        <option value='canceled'>Cancelled</option>
        <option value='no_show'>No Show</option>
        <option value='completed'>Completed</option> 
      </select>
      </div>  
      </div>

       <div class="form-group row">
      <label for="status" class="col-sm-12 col-md-4 col-form-label">Remark:</label>
      <div class="col-sm-12 col-md-8">
       <textarea class="form-control" name="remarks" id="remark" cols="20" rows="7"></textarea>
      </div>  
      </div>

      </div>
      <div class="modal-footer">
      <input type='hidden' value="" id="key1" name="bookingno" />
      <input type='hidden' value="" id="bin" name="bin" />
      <input type='hidden' value="{{$sdate}}" id="sdate" name="sdate" />
      <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      
      </div>
    </div>
  </div>
</div>


</form>

@endsection

@section("script")

<script> 
 $(document).on("click", ".btn-process", function(){ 
    
    var widgetno = $(this).attr("data-widget");
    $("#key" + widgetno).val(    $(this).attr("data-key") );
    $("#bin").val($(this).attr("data-bin") );
    $("#wg" + widgetno).modal('show');

});
</script> 

@endsection 