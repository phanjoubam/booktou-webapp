@extends('layouts.admin_theme_02')
@section('content')

<div class="row">
     <div class="col-md-12">

    <div class='card'> 
      <div class='card-body'>
        <div class="row">
                <div class="col-md-8">
                 <h3> Merchant Wise Total Booking List for:
                 	<span class="badge badge-success">
                 		{{ date('Y-m-d',strtotime($data['today'])) }}
                 </span>
               	 </h3>
                </div>
                <div class="col-md-4">
                <form method="post" action="{{action('Admin\ServiceBookingController@viewMerchantWiseBooking')}}">
                	{{csrf_field()}}
               	<div class="row">
               		<div class="col">	
               			<input type="text" class="calendar form-control" name="filter_date">
                  </div>
                  <div class="col"> 
                  	<button class="btn btn-primary" type="submit">Search</button>
                  </div>
                 </div>
                 </form>

                </div> 
 </div> 
                </div> 
           </div>
     </div>
 </div>

<div class="row mt-2">
    <div class="col-md-12">
	<div class='card'> 
    <div class='card-body'>
	<table class="table">
		<thead>
		<tr>
			<th>Name</th>
			<th>Service Category</th>
			<th>Service Code</th>
			<th>Phone</th>
			<th style="text-align: center;">Booking</th>
			<th style="text-align: center;">Offer</th>
			<th style="text-align: center;">Action</th>
		</tr>
		</thead>
		<tbody>
			@foreach($data['biz'] as $biz_list)
			<tr>
				<td>{{$biz_list->name}}</td>
				<td><span class='badge badge-primary'>{{ $biz_list->main_module}}</span></td>
				<td><span class="badge badge-primary">{{$biz_list->sub_module}}</span></td>
  <td>{{$biz_list->phone_pri}}</td>
				 	<?php 
				 	

				 	 $total_booking = $total_offers =  0;
						foreach ($data['booking'] as $book) {
							 switch ($book->type) {
							 	case 'booking':
							 		if ($biz_list->id == $book->bin) {
							  
									 	$total_booking++;
									 } 
							 		break;
							 	case 'offers':
							 		 if ($biz_list->id == $book->bin) {
							   			$total_offers++;
									 }
							 		break;
							 } 
						}
					?>
					 
				<td class="text-center">
					<!-- {{$book->type}} -->
					@if($biz_list->sub_module=="WTR")
					<a href="{{URL::to('services/view-water-bookings')}}?bin={{$biz_list->id}}&sdate={{$data['today']}}" class="btn btn-info" style="width:70px;" target="_blank">{{$total_booking}}</a>
					@elseif($book->type=="booking")
							<a href="{{URL::to('services/view-bookings')}}?bin={{$biz_list->id}}&sdate={{$data['today']}}" class="btn btn-info" style="width:70px;" target="_blank">{{$total_booking}}</a>
				  @endif
				</td>
				<td class="text-center">
					 @if($total_offers>0)
					<a href="{{URL::to('services/view-bookings-offer')}}?bin={{$biz_list->id}}&sdate={{$data['today']}}" class="btn btn-info" style="width:70px;" target="_blank">{{$total_offers}}</a>
					@else
					<h5><span class="badge badge-info">No offer found!</span></h5>
				  @endif
				</td>
				<td class="text-center">
					<div class="dropdown">
                     <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a>
 									<ul class="dropdown-menu dropdown-user pull-right"> 
  								<li>
  								
  									@if($biz_list->sub_module=="WTR")
										
										<a class="dropdown-item" href="{{URL::to('services/view-water-bookings')}}?bin={{$biz_list->id}}&sdate={{$data['today']}}" target="_blank">View bookings</a>
										
										@else
										
											<a class="dropdown-item" href="{{URL::to('services/view-bookings')}}?bin={{$biz_list->id}}&sdate={{$data['today']}}" target="_blank">View bookings</a>
										
										@endif

  									
  								</li>
  								@if($biz_list->sub_module=="WTR")
									
									@elseif($total_offers>0)
									<li><a class="dropdown-item" href="{{URL::to('services/view-bookings-offer')}}?bin={{$biz_list->id}}&sdate={{$data['today']}}">View Offers</a></li>	
									@endif
  								<li><a class="dropdown-item" href="{{ URL::to('/admin/booking/order/view-more-details/') }}/{{$book->id}}">View more details</a></li>

                  </ul>
          </div>

				</td>

			</tr>
			@endforeach

			{{$data['biz']->links()}}
		</tbody>
	</table>
</div>
	</div>
	</div>
</div>



@endsection

@section("script")

<script>
	
	$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
	});

</script>

@endsection