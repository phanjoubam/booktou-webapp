@extends('layouts.admin_theme_02')
@section('content')

<div class="card">
 <div class="p-2"> 
  <div class="row">
    <div class="col-md-4">
      <div class="btn-group btn-group-sm" role="group"  >
        
        <a type="button" class="btn btn-success" href="{{URL::to('/admin/services/business/search')}}?status={{'confirmed'}}">Create New Booking</a> 


      </div>

    </div>
 

    <div class="col-md-8">
      <form class="form-inline" method="post" action="{{  action('Admin\ServiceBookingController@viewAllActiveAppointment') }}"> 
        {{ csrf_field() }}

        <label class="my-1 mr-2" for="inlineFormCustomSelectPref"> Date:</label> 
        <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar'  
        value="{{ date('d-m-Y')}}" name='filter_date' />

        <label class="my-1 mr-2"  for="inlineFormCustomSelectPref">&nbsp Filter By:</label> 
        <select   class='form-control form-control-sm'   name='filter_by' >
        <option class="form-control input-lg" value='booking_date'>Book date</option> 
        <option class="form-control input-lg" value='service_date'>Service Date</option> 
        </select>

        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">&nbsp Status:</label> 
        <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='status' >
        <option value='-1'>All</option> 
        <option value='new'>New</option> 
        <option value='confirmed'>Confirmed</option>
        <option value='cancelled'>cancelled</option>  
        </select>

        <button type="submit" class="btn btn-primary btn-sm my-1" value='search' name='btn_search'>Search</button>
      </form>
    </div>

  </div> 
 </div>         
</div>
 

<?php
 $all_orders= $appointment_booking;
?>

<div class="row mt-3"> 
          @if( count($all_orders ) > 0 )
          @foreach($all_orders as $item)                
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-2">
            <ul class="list-group"> 
              <li class="list-group-item d-flex justify-content-between align-items-center" style='list-style: none;'>  
                <span class="display-4">Booking Order #</span> 
        <a href="{{ URL::to('/services/booking/view-details')}}/{{ $item->id }}?bin={{ $item->bin }}" target="_blank">
        <span class="badge badge-primary badge-pill display-5">{{ $item->id }}</span></a> 
              </li>
              <li style='list-style: none; padding:10px; background-color: #fff'>
 <table class='table table-striped'> 
                  <tr>
                    <td>Customer</td>
                    <td class='text-right'>{{ $item->customer_name }}</td> 
                  </tr>

                  <tr>
                    <td>Contact No</td>
                    <td class='text-right'>{{ $item->customer_phone }}</td> 
                  </tr>

                  <tr>
                    <td>Book Date</td>
                    <td class='text-right'>{{date('d-m-Y', strtotime( $item->book_date ))}}</td> 
                    
                  </tr>

                  <tr>
                    <td>Service Date</td>
                    <td class='text-right'>{{date('d-m-Y', strtotime( $item->service_date ))}}</td> 
                  </tr>

                  <tr>
                    <td>Amount</td>
                    <td class='text-right'>{{$item->total_cost}}</td> 
                  </tr>

                   <tr>
                    <td>Discount</td>
                    <td class='text-right'>{{$item->discount}}</td> 
                  </tr>
                  <tr>
                    <td>GST</td>
                    <td class='text-right'>{{$item->gst}}</td> 
                  </tr>
                  <tr>
                    <td>Booking Status</td>
                    <td class='text-right'><span class='badge badge-{{ strcasecmp( $item->book_status, "cancelled" ) == 0 ? "danger" : "success" }} badge-pill'>{{ $item->book_status }}</span>
                    </td> 
                  </tr>

                  <tr>
                    <td>Payment Status</td>
                    <td class='text-right'><span class='badge badge-primary badge-pill'>{{$item->payment_status}}</span> 
                    </td> 
                  </tr>
 
                </table> 
              </li>

              <li class="list-group-item d-flex justify-content-between align-items-center">
              <a  class="btn btn-primary btn-rounded"   href="{{ URL::to('/services/booking/view-details') }}/{{$item->id}}?bin={{$item->bin}}" target='_blank' >View details</a>
            </li>


            </ul>
         
          </div>
          @endforeach
          @else 
          <p class='alert alert-info'>No New Active Orders.</p>
          @endif 
        
</div>




@endsection
@section("script")



<script> 


  $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});



 $(document).on("click", ".btn-process", function(){ 
  var widgetno = $(this).attr("data-widget");
  $("#key" + widgetno).val(    $(this).attr("data-key") );
  $("#bin").val($(this).attr("data-bin") );
  $("#wg" + widgetno).modal('show');

});
</script> 

@endsection 