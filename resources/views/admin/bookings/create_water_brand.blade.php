@extends('layouts.admin_theme_02')
@section('content')

 
  <div class="row">
     <div class="col-md-8"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Brand Name List</div> 
                </div>
 <div class="col-md-4 text-right"> 
        
      </div>

       </div>
                </div>
            </div>
       <div class="card-body"> 

       @if (session('err_msg'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg') }}
      </div>
      </div>
    @endif


<div class="table-responsive" >
    <table class="table">
         <thead class=" text-primary">
      		<tr > 
      		<th scope="col" class="text-left">Brand Name</th>  
      		<th scope="col" class="text-left">Brand Photo</th>
      		<th scope="col" class="text-left">Jar Photo</th>
      		<th>Action</th>
          <th></th>
      		</tr>
      	</thead>
	
		<tbody>
			@if(isset($data['brand']))
				@foreach($data['brand'] as $items)
						<tr>
							<td>{{$items->brand_name}}</td>
			 				<td><img src="{{$items->brand_image}}" width="250px" height="250px" class="rounded-circle"></td>
							<td><img src="{{$items->brand_jar}}" width="250px" height="250px" class="rounded-circle"></td>
							<td>
								<button class="btn btn-primary btnEditBrandName" 
								data-key="{{$items->id}}"
								data-name="{{$items->brand_name}}"
								data-url = "{{$items->brand_url}}"
							    >Edit</button>
								<!-- <button class="btn btn-danger">Delete</button> -->
							</td>
						</tr>
				@endforeach
			@endif
 		</tbody>
	
	</table>
	
		
	</div>
 </div> 
  </div>  
	</div> 
 
   <div class="col-md-4"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Water Brand Name</div> 
                </div>
 <div class="col-md-4 text-right"> 
        
      </div>

       </div>
                </div>
            </div>
  <div class="card-body">   
    
    <form action="{{ action('Admin\AdminDashboardController@saveBrandName') }}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }} 
      <div class="card-body ">
    <div class="row"> 
    @if (session('err_msg_right'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg_right') }}
      </div>
      </div>
    @endif
    </div>
    <div class="form-group row">
    <div class="col-md-12">
    <input type="hidden" name="brandid" id="key"> 
    <label class="control-label">Brand Name:</label>
    <input type="text" required name='brandname' id='brandname' class="form-control boxed">
    </div>
   
      <div class="col-md-12 mt-2"> 
      <label class="control-label">Business web url:</label>
      <input type="text" class="form-control" name="weburl" id="weburl">
       
      </div>

      <div class="col-md-12 mt-2"> 
      <label class="control-label">Brand Photo:</label>
      <input type="file" class="form-control" name="photo">
       
      </div>

      <div class="col-md-12 mt-2"> 
      <label class="control-label">Jar Photo:</label>
      <input type="file" class="form-control" name="jarphoto">
       
      </div>
    </div>  
    
  
                      
 
                      <div class="form-row">
                      <div class="col"> 
                      <div class="form-group">  
                      <br/>
                        <input type="hidden"   name='tb_oldbizcategory' id='tb_oldbizcategory'  > 
                        <button type="submit" id='btnSave' name='btnSave' value='save' class="btn btn-primary">Save</button>
                        <button type="submit" name='btnCancel' value='save' class="btn btn-danger">Cancel</button>
                      </div>
                    </div> 
                      </div>
                      
               
         </div>
     
     </form>


 </div> 
  </div>  
  </div> 



</div>

 


 

@endsection



@section("script")

<script>
	$(document).on("click", ".btnEditBrandName", function()
	{
	 	$("#key").val($(this).attr("data-key"));
	    $("#brandname").val($(this).attr("data-name"));
	    $("#weburl").val($(this).attr("data-url"));
	    //$("#modalConfirmService").modal("show")

	}); 
</script>

@endsection

