@extends('layouts.admin_theme_02')
@section('content')



 {{  csrf_field() }}


 
  <div class="row">

    <!-- staff details -->

    <div class="col-sm">
      @foreach( $result as $staff)

<div class="card">
  <div class="card-body">
    <h5 class="card-title">
   {{$staff->service_name}} </h5>
    <span class="badge badge-secondary">
    @if($staff->service_time==null)
    0.00
    @else
   {{$staff->service_time}} 
    @endif

     
    </span>
  </div>
<div class="card-body">
    <div class="row">
        <div class="col-sm-4">
           @if($staff->profile_photo==null)
           <img src="https://via.placeholder.com/80" class="rounded-circle">
           @else
           <img src="" class="rounded-circle">
           @endif 
        </div>
        <div class="col-sm-7">
            <div class="card-body">
                <h5 class="card-title">{{$staff->fullname}}</h5>
                <p class="card-text">
                  Email:<span class="badge badge-secondary">{{$staff->email}}</span> 
                </p>
                
            </div>
        </div>
    </div>
</div>
<div class="card-body">


<button type="button" class="btn btn-success" data-toggle="modal" 
data-target="#customerRemarks">
Add Remarks
</button>
 
  </div>
</div>
<br>
@endforeach

<div class="clearfix"></div>
 </div>


<!--staff details ends here -->
<form id="frmcustomerRemarks">
          {{csrf_field()}} 
<div class="modal fade" id="customerRemarks" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
   
          <div class="modal-content">
      <div class="modal-header">
        
        <input type="hidden" id="serviceid" name="serviceid" value="{{$service_id}}">
        <h5 class="modal-title" id="customerRemarks"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">Remarks</span>
  </div>
  <textarea class="form-control" aria-label="remarks" id="cus_remark" name="cus_remark"></textarea>
</div>


      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary"  onclick="UpdateRemark();">Update</button>
      </div>
    </form>
    </div>
  </div>
</div>




    <div class="col-sm">
     @foreach($venue as $shop)

@if($shop->preferred_time==null)
<div class="card"><div class="card-header">
    Venue:
  </div>
  <div class="card-body">
    <h5 class="card-title">{{$shop->name}}:</h5>
    <h6 class="card-subtitle mb-2 text-muted">
     Shop No: <span class="badge badge-secondary">{{$shop->shop_number}}</span></h6>
    <p class="card-text">{{$shop->locality}},
      <br>
      {{$shop->city}} <br>
      <span>{{$shop->phone_pri}}<i class="fa fa-phone"></i></span> </p>
    
  </div>
</div>

@endif


@endforeach
<br>

@foreach ($customer as $cust)
<div class="card">
  <div class="card-header">
    Customer: {{$cust->fullname}}
  </div>
  <div class="card-body">
    <h5 class="card-title"></h5>
    <h6 class="card-subtitle mb-2 text-muted">{{$cust->locality}}</h6>
    <p class="card-text">{{$cust->landmark}}, <br>
    {{$cust->city}},
    <br>
    <span>{{$cust->phone}}<i class="fa fa-phone"></i></span>
    </p>
    
  </div>
</div>
<br>
@endforeach

    </div>
    <div class="col-sm">
     
<div class="card">
  <div class="card-header">
    Billing Information:
  </div>
<div class="card-body">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Service Type: 
      

     @foreach($result as $staff_service)
       <span class="badge badge-secondary">{{$staff_service->service_name  }} </span>
      @endforeach

  
     </li>
     
    </div>
@foreach ($bill as $billing)
@php 

    $total = $billing->fees + $billing->charge;

    @endphp
  <div class="card-body">
    
    
<div class="card">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Service Date: 
      <span class="badge badge-secondary">{{\Carbon\Carbon::parse($billing->service_date)->format('j F, Y')}}</span></li>
    
    <li class="list-group-item">Service Fee: 
      <span class="badge badge-secondary">{{$billing->fees}}</span></li>
    <li class="list-group-item">bookTou Service Fee: 
      <span class="badge badge-secondary">{{$billing->charge}}</span></li>
    <li class="list-group-item">Total Fee: 
      <span class="badge badge-secondary">{{$total}}</span></li>



  </ul>
</div>
<div class="card text-center ">
    <button type="button" class="card-link text-center btn btn-success redirect" 
    data-key="{{$billing->id}}" data-pgc="1">
      Print Receipt
    </button>
</div>
</div>

  @endforeach
</div>

    </div>
  </div>
 


@endsection

@section("script")
<script type="text/javascript">
  
  function UpdateRemark() {
     
     var formData = new FormData($('#frmcustomerRemarks')[0]);
    $.ajax({
        type: 'POST',
        url: "{{action('Admin\AdminDashboardController@updateRemark')}}",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
           
            if (response.success) {
                 alert(response.detail_msg);
                 $("#cus_remark").val('');
            }
            else {
                 alert("Unable to perform action");
            }
        },
        error: function (ts) {
             alert(ts.responseText);
        }
    });
}
</script>

@endsection 