@extends('layouts.admin_theme_02')
@section('content')
 
 
 <div class="card">
   <div class="p-2"> 
        <div class="row">
                <div class="col-md-6">
                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                      <a type="button" class="btn btn-info" href="{{ URL::to('/admin/booking/orders-view-all')}}">New Orders</a>
                      <a type="button" class="btn btn-success" href="{{ URL::to('/admin/booking/orders/view-completed') }}">Completed Orders</a> 
                   </div> 

                </div>
                <div class="col-md-6">
               
                      <form class="form-inline" method="post" action="{{  action('Admin\CarRentalServiceController@viewAllOrders') }}"> 
            {{ csrf_field() }}
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar'  
             value="{{ date('d-m-Y', strtotime($data['last_keyword_date']))  }}" name='filter_date' />

              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Status:</label> 
                <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='filter_status' >
                  <option value='-1'>All</option> 
                    <option value='new'>New</option> 
                    <option value='confirmed' <?php if($data['last_keyword']  == "confirmed") echo "selected";  ?>>Confirmed</option> 
                      <option value='delivery_scheduled' <?php if($data['last_keyword']  == "delivery_scheduled") echo "selected";  ?>>Wating Agent Pickup</option>  
                    <option value='order_packed' <?php if($data['last_keyword']  == "order_packed") echo "selected";  ?>>Packed and Ready for delivery</option>    
                    <option value='in_route' <?php if($data['last_keyword'] == "in_route") echo "selected";  ?>>In Route</option>    

                </select>

                <button type="submit" class="btn btn-primary btn-sm my-1" value='search' name='btn_search'>Search</button>
            </form>
                </div> 
 </div> 
      </div>         
 </div>


 <div class="row">
 <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');  

                ?>
               
               <div class="col-md-12 mt-2">

                <div class="card "><div class="card-body ">
                  
                  @if($item->delivery_type=="takeaway")

                  @else
                  <div class="dropdown pull-right">

                       <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a> 

                      <ul class="dropdown-menu dropdown-user pull-right"> 
                          <li><a class="dropdown-item btn btn-link btn-assign" data-oid="{{$item->id}}" href='#' >Assign Agent</a></li> 
                          <li><a  class="dropdown-item btn btn-link " href="{{ URL::to('/admin/customer-care/order/view-more-details/') }}/{{$item->id}}">View Order Details</a></li> 

                          <li><a   class="dropdown-item btn btn-link btnchangedelivery" data-ono="{{$item->id}}" href='#' >Change Service Date</a></li> 
                          <li><a   class="dropdown-item btn btn-link btnaddremarks" data-ono="{{$item->id}}" href='#' >Add Remarks</a></li>    
                          <li><a   class="dropdown-item btn btn-link btncancel" data-ono="{{$item->id}}" href='#' >Cancel Order</a></li>   
                           </ul>
                  </div>
                  @endif
                  <div class="row">   
                 <div class='col-md-3' id="tr{{$item->id }}" > 


                  <div class="timeline timeline-xs">
                  <div class="timeline timeline-xs">
                    <div class="timeline-item">
                      <div class="timeline-item-marker">
                        <div class="timeline-item-marker-text"><span class='badge badge-danger badge-pill'>Customer</span></div>
                        <div class="timeline-item-marker-indicator bg-red"></div>
                      </div>
                      <div class="timeline-item-content"> 
                        <strong>Order #</strong>
                        <a class='badge badge-info badge-pill' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{ $item->id }}" target='_blank'>
                        {{$item->id}}
                         </a> @if( $interval->format('%H') >=  1 || $interval->format('%a') >=  1  )
                        <span class='radar'></span>
                        @else 
                          @if( $interval->format('%H') < 1 )
                            <span class='radar-green'></span> 
                          @endif
                        @endif<br/>

                        {{ $item->customer_name }} <a style='color: #01a230;' href="{{  URL::to('/admin/customer/view-complete-profile') }}/{{ $item->book_by }}" target='_blank'><i class='fa fa-bar-chart-o'></i></a><br/>
                        {{$item->address}}<br/>
                        {{$item->city}} - {{$item->pin_code}}<br/>
                        <i class='fa fa-phone'></i> {{$item->customer_phone}}   
                        @foreach($data['flag_customers'] as $flagitem) 
                          @if( $flagitem->phone == $item->customer_phone )
                             <div  class='alert alert-info mt-2'>
                               Cutomer is flagged <span class='badge badge-danger'>{{ $flagitem->flag }}</span>
                             </div> 
                            @break 
                           @endif 
                        @endforeach

                      </div> 
                      </div>
                    </div>

                     <div class="timeline-item">
                      <div class="timeline-item-marker">
                        <div class="timeline-item-marker-text"><span class='badge badge-success badge-pill'>Merchant</span></div>
                        <div class="timeline-item-marker-indicator bg-green"></div>
                      </div>
                      <div class="timeline-item-content">
                        @foreach($data['all_businesses'] as $business)
                          @if($business->id == $item->bin)
                            {{  $business->name  }}<br/>
                            {{  $business->locality  }}<br/>
                            <i class='fa fa-phone'></i> {{$business->phone_pri}} 
                            @break
                          @endif
                        @endforeach
                      </div>
                    </div> 
                  </div> 
                </div> 
                <div class='col-md-3'  >  
                  <strong>Order Snapshot</strong><br/>
                      @php  
                       $repeatOrder =0  
                      @endphp

                      @foreach($data['book_counts'] as $bookItem)
                        @if($bookItem->book_by == $item->book_by)
                          @php  
                            $repeatOrder  = $bookItem->totalOrders 
                          @endphp 
                          @break
                        @endif
                      @endforeach

                      <strong>Order Age:</strong> <span class='badge badge-primary badge-pill'>{{ $order_age }}</span><br/>
                      <strong>Repeat Order Count:</strong> <span class='badge badge-info badge-pill'>{{  $repeatOrder  }}</span><br/>
                      <strong>Delivery OTP:</strong> <span class='badge badge-danger badge-pill'>{{ $item->otp }}</span><br/> 

                      <strong>Order Status:</strong> 
                      @switch($item->book_status)

                      @case("new")
                        <span class='badge badge-primary badge-pill'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info badge-pill'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info badge-pill'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info badge-pill'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning badge-pill'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success badge-pill'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success badge-pill'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success badge-pill'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success badge-pill'>Delivery Scheduled</span>
                      @break 

                        @case("canceled")
                        <span class='badge badge-danger badge-pill'>Cancelled</span>
                      @break 

                      @endswitch

                      <br/>
                      <strong>Payment Type:</strong> <span class='badge badge-success badge-pill'>
                      
                      @if( strcasecmp($item->payment_type,"Cash on delivery") == 0  ||  strcasecmp($item->payment_type,"COD") == 0    )
                          COD
                      @else 
                        @if( strcasecmp($item->payment_type,"POD") == 0    )
                           POD
                        @else 
                          ONLN
                        @endif
                      @endif

                      </span><br/>

                   @if( isset($item->cust_remarks) )
                    <div class='alert alert-warning mt-2'>
                      <strong >Customer Remarks</strong><br/>
                      {{ $item->cust_remarks }}
                      </div> 
                  @endif   
                </div>
                 <div class='col-md-3'  >
               
                  Delivery Date: <span class='badge badge-info  badge-pill'>{{ date('F dS, Y', strtotime( $item->service_date)) }}</span>
                  <br/>  
                  @if($item->preferred_time  != "")
                    Preferred Delivery Time: <span class='badge badge-primary badge-pill'>{{ date('h:i a', strtotime($item->preferred_time)) }}</span>
                  @endif 


        <?php
        $delay_sms_sent = false;
          if(isset($data['global_settings']) &&  date('H:i', strtotime($data['global_settings']->config_value)) < date('H:i')   )
          {
            foreach($data['all_notifications'] as $notification)
            {
              if( $notification->order_no == $item->id && $notification->notif_name =="order_delay_sms"  )
              {
                $delay_sms_sent = true;
                break;
              }
            }

            if($delay_sms_sent)
            {
                echo "<strong>Has customer been informed about delayed delivery?</strong> ";
                echo "<span class='badge badge-success badge-pill notifstate'>Yes</span>"; 
            }
            else 
            {
                echo "<div class='card mt-3'><div class='card-body'><strong>Send Delivery delay notification SMS</strong><br/>"; 
              ?> 
             
              <div class="custom-control custom-switch">
                  <input type="checkbox" class="custom-control-input switchNotification"  
                  data-id='{{$item->id }}' id="switchON{{ $item->id }}" />
                  <label class="custom-control-label" for="switchON{{$item->id }}">Notify</label>
              </div> 
              <?php
              echo "</div></div>";
            }
 
          }
          ?> 


            </div>
 


                <div class='col-md-3'  > 

                  <strong>Confirmation Status</strong><br/>
                      <?php  
                      if($item->book_status != "new")
                      {
                        echo "Confirmed";
                      }
                      else 
                      {
                        ?>
                        <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input switch"   data-id='{{$item->id }}' id="switchON{{ $item->id }}"  
                          <?php if($item->is_confirmed == "yes") echo "checked";  ?>  />
                          <label class="custom-control-label" for="switchON{{$item->id }}">Confirmed</label>
                        </div> 
                        <?php 
                      }
                ?> 

                <hr/>

                @if($item->delivery_type=="takeaway")
                  <h3 class="alert alert-danger">Take Away Order</h3>
                @else
                                  <strong>Delivery Agent</strong><br/>
                  @if( $item->book_status == "new" ||  $item->book_status == "confirmed" ||   $item->book_status == 'order_packed' )
                    <div class="input-group mb-3">
                     
                      <select   class='form-control   '   name='agents[]'  id="aid_{{$item->id }}" name='agents[]' >
                        @foreach($data['all_agents'] as $aitem) 
                                                   @if($aitem->isFree == "yes" )
                                                    <option value='{{ $aitem->id }}' data-img="{{ $aitem->profile_photo }}"  >{{ $aitem->nameWithTask }}</option>
                                                  @endif 
                                                @endforeach 

                                              </select>
                     <div class="input-group-append">
                      <button class='btn btn-secondary btn-xs showTaskList' data-cid="aid_{{$item->id }}"  ><i class='fa fa-eye'></i></button> 
                      
                      </div>
                    </div>
                @else
                  @foreach($data['delivery_orders_list'] as $ditem) 
                                @if( $item->id  == $ditem->order_no )
                                  <p>{{ $ditem->fullname  }} <br/> 
                                 @if(  $ditem->agent_ready == "yes")
                                 <span class='badge badge-success white'>TASK ACCEPTED</span>  
                                  @else 
                                  <span class='badge badge-danger white'>NO ACTION</span>
                                 @endif
                                  </p>
                                 @break;
                              @endif  
                          @endforeach 
                        @endif
                @endif
  </div>
              
             
                
              </div>
           </div>
                   </div>
 </div> 
                 
          <?php
                          $i++; 
                       }

                 ?>
              
             </div>
        </div>
    </div>  
  </div>  
              

 
  
<div class="modal hide fade modal_cancel"   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Confirm Order Cancellation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body "> 
            <div class='confloading'>
        </div> 
        <div class="form-group"> 
        <textarea  placeholder='Reason for cancelling' class="form-control form-control-sm  " id="tareason" name="tareason" rows='5'></textarea>

  </div>
  

      </div>
      <div class="modal-footer">
        <input type='hidden' id='orderno' name="orderno" />
        <button type="button" class="btn btn-primary btnconfim" >Cancel</button> 
        <button type="button" class="btn btn-secondary" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>

<div class="modal hide fade modal_processing" id='modal_processing'   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Processing Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"> 
        <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn_action" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modalTaskList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
  <img src='{{ URL::to("/public/assets/image/no-image.jpg") }}' id='apimg' width="40px" height="40px" class="img-rounded" style='margin-right:10px; display:inline-block'/>
<h5 class="modal-title" id="exampleModalLongTitle">Delivery Tasks for <strong id='span_anm'></strong></h5>
  

        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content_area">
         <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


<div class="modal" id='confirmOrder' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Customer Order By Call</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class='alert alert-danger'>THIS ACTION CANNOT BE REVERSED!</p>
        <p>

          Have you called back the customer to confirm this order?
          <br/>

          If you haven't call back, pleaes do call and confirm first.
          <br/> 
        </p>
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <button type="button" class="btn btn-success" id="btnsaveconfirmation" data-conf="" data-id="" >Sent Alert</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



<form   method="post" action="{{  action('Admin\AdminDashboardController@updateDeliveryDate') }}"> 
  {{ csrf_field() }}
<div class="modal hide fade changedelivery"   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Confirm Order Information Changing</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body "> 
            <div class='confloading'>
        </div> 

         

        <div class="form-row">
           <div class="form-group col-md-6">
            <label for="servicedate">New Service Date:</label>
            <input type="text" class="form-control form-control-sm calendar" id="servicedate" name='servicedate' > 
          </div> 
         
 
         <div class="form-group col-md-12">
             <label for="tareason2">Reason for changing:</label>
              <textarea  placeholder='Reason for changing order status' class="form-control form-control-sm" id="tareason2" name="tareason" rows='5'></textarea> 
      </div>
 
 

      </div>
      <div class="modal-footer">
        <input type='hidden' id='orderno2' name="orderno" />
        <button type="submit" class="btn btn-primary" name='save'>Save</button> 
        <button type="button" class="btn btn-secondary" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</div>
</form>



<div class="modal" id='widget-delay-alert' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Sent SMS to Customer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
        <p> 
          Below sms will be sent to the customer regarding order processing delay possibility.
        </p>
        <p class='alert alert-info'>
          Hi dear! We received your order. As our customer care team is offline, order processing may get delayed. 
          We regret this inconvenience - Team bookTou
        </p>
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <button type="button" class="btn btn-success" id="btnsentdelayalert" data-conf="" data-id="" >Sent Alert</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



 
@endsection

@section("script")

<style type='text/css'>
  .pa-2
  {
    padding: 10px;
  }
</style>

<script>


$(document).on("click", ".btnaddremarks", function()
{

    $("#orderno").val($(this).attr("data-ono"));
    $(".modal_addremarks").modal("show")

});



$(document).on("click", ".btncancel", function()
{

    $("#orderno").val($(this).attr("data-ono"));
    $(".modal_cancel").modal("show")

});




 

$(document).on("click", ".btnchangedelivery", function()
{
  $("#orderno2").val($(this).attr("data-ono"));
  $(".changedelivery").modal("show"); 
});




$(document).on("click", ".btnconfim", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['reason'] =  $("#tareason").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

     

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/order/cancel" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_cancel").modal("hide") ;

         $("#tr" + $("#orderno").val( )).remove();
      },
      error: function( ) 
      {
       // alert(  'Something went wrong, please try again'); 
      } 

    }); 
});

  


  $(document).on("click", ".btnsaveccremarks", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['ccremark'] =  $("#taccremark").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

     

    $.ajax({
      type: 'post',
      url: api + "v3/web/customer-care/order/add-remarks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_addremarks").modal("hide") ;
 
      },
      error: function( ) 
      {
       // alert(  'Something went wrong, please try again'); 
      } 

    }); 
});




 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
             
        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 

    
    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');

  

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
        //alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id");

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsaveconfirmation").attr("data-id",  oid);
      $("#btnsaveconfirmation").attr("data-conf", confirmation ); 
      $("#confirmOrder").modal("show");   
   }  

})

$(document).on("change", "#closeconfmodal", function()
{

  
})
 

$('body').delegate('#btnsaveconfirmation','click',function(){
  
   var confirmation = $(this).attr("data-conf"); 
   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");


   $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/update-customer-confirmation" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.browser_refresh == "yes")
        {
          $(".loading_span").html(" ");
          location.reload(); 
        }    
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        //alert(  'Something went wrong, please try again'); 
      } 

    });
  
})

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});




$(document).on("change", ".switchNotification", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id"); 

   var source = $(this).attr("data-source"); 
   $("#spansource").html(source); 

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsentdelayalert").attr("data-id",  oid);
      $("#btnsentdelayalert").attr("data-conf", confirmation ); 
      $("#widget-delay-alert").modal("show");   
   }  

})




$('body').delegate('#btnsentdelayalert','click',function(){
  
   var confirmation = $(this).attr("data-conf"); 
   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 
   json['title'] =  "orderdelay" ;
   $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");


   $.ajax({
      type: 'post',
      url: api + "v3/web/orders/send-notification-sms" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.status_code == 7005 ){
          $("#widget-delay-alert").modal("hide");    
        } 

      },
      error: function( ) 
      {
        $(".loading_span").html(" ");  
      } 

    });

    $("#confirmOrder").modal("hide");     
    

})




</script> 

@endsection 