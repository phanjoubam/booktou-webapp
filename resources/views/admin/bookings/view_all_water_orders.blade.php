@extends('layouts.admin_theme_02')
@section('content')

<!-- ---------------------------------------------------------------------------------------------- -->
<div class="card">
 <div class="p-2"> 
  <div class="row">
    <div class="col-md-4">
      <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
        <a type="button" class="btn btn-info" href="{{URL::to('services/view-all-water-orders')}}?status={{'new'}}">New Orders</a>
        <a type="button" class="btn btn-success" href="{{URL::to('services/view-all-water-orders')}}?status={{'confirmed'}}">Completed Orders</a> 
      </div> 

    </div>

    <div class="col-md-8">
      <form class="form-inline" method="post" action="{{  action('Admin\ServiceBookingController@viewAllWaterOrders') }}"> 
        {{ csrf_field() }}

        


        <label class="my-1 mr-2" for="inlineFormCustomSelectPref"> Date:</label> 
        <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar'  
        value="{{ date('d-m-Y')}}" name='filter_date' />

        <label class="my-1 mr-2"  for="inlineFormCustomSelectPref">&nbsp Filter By:</label> 
        <select   class='form-control form-control-sm'   name='filter_by' >
        <option class="form-control input-lg" value='booking_date'>Book date</option> 
        <option class="form-control input-lg" value='service_date'>Service Date</option> 
        </select>

        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">&nbsp Status:</label> 
        <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='status' >
        <option value='-1'>All</option> 
        <option value='new'>New</option> 
        <option value='confirmed'>Confirmed</option>
        <option value='cancelled'>cancelled</option>  
        </select>

        <button type="submit" class="btn btn-primary btn-sm my-1" value='search' name='btn_search'>Search</button>
      </form>
    </div>

  </div> 
 </div>         
</div>

<!-- ---------------------------------------------------------------------------------------------------- -->

<?php
 $all_orders= $water_orders;
?>

<div class="row">
  <div class="col-md-12"> 
    <div class="card card-default mt-2">       
      <div class="card-body">

        <div class="row ">
          @if( count($all_orders ) > 0 )
          @foreach($all_orders as $item)                
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-2">
            <div class="card">
              <div class="card-header">
                <div class="row ">                          
                  <div class="col-md-10">
                    <h2>Order #{{$item->id}}</h2>
                  </div>
                  <div class="col-md-2">

                  </div>
                </div>
              </div>

              

              <div class="card-body">
                <div class="row">                          
                  <div class="col-md-6">Customer Name:</div>
                  <div class="col-md-6">{{ $item->customer_name }}</div>
                  <div class="col-md-6 mt-1">Contact No:</div>
                  <div class="col-md-6 mt-1">{{$item->customer_phone}}</div>
                  <div class="col-md-6 mt-1"> Book Date:</div>
                  <div class="col-md-6 mt-1">{{date('d:m:Y', strtotime( $item->book_date ))}}</div>
                  <div class="col-md-6 mt-1">Service Date:</div>
                  <div class="col-md-6 mt-1">{{date('d:m:Y', strtotime( $item->service_date ))}}</div>
                  <div class="col-md-6 mt-1">Preferred Time:</div>
                  <div class="col-md-6 mt-1">{{date('h:i a', strtotime( $item->preferred_time ))}}</div>
                  <div class="col-md-6 mt-1">Order Amount:</div>
                  <div class="col-md-6 mt-1">{{$item->total_cost}}</div>
                  <div class="col-md-6 mt-1">Book Status:</div>
                  <div class="col-md-6 mt-1">
                    <span class="badge  badge-primary ">{{ $item->book_status  }}</span>       
                  </div>
                  <div class="col-md-6 mt-1">Payment Mode:</div>
                  <div class="col-md-6 mt-1">
                    <span class="badge  badge-success ">{{ $item->pay_mode  }}</span>
                  </div>
                  <div class="col-md-6 mt-1">Payment Status:</div>
                  <div class="col-md-6 mt-1">

                    <span class="badge  badge-warning ">{{ $item->payment_status  }}</span>

                  </div>
                  <div class="col-md-12 mt-3 text-center">
                    <button type="button" class="btn btn-outline-danger btn-sm"  ata-toggle="modal" data-target="#modalepg" data-key="{{$item->id}}" style="border-radius:8px;color: #e83e8c;border-color: #e83e8c;background: white;">
                      <a  href="{{ URL::to('/services/booking/view-details') }}/{{$item->id}}?bin={{$item->bin}}" target='_blank' style="color: #e83e8c;">View details</a>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          @else 
          <p class='alert alert-info'>No New Active Orders.</p>
          @endif 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ----------------------------------------------------------------- -->

@endsection
@section("script")



<script> 


  $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});



 $(document).on("click", ".btn-process", function(){ 
  var widgetno = $(this).attr("data-widget");
  $("#key" + widgetno).val(    $(this).attr("data-key") );
  $("#bin").val($(this).attr("data-bin") );
  $("#wg" + widgetno).modal('show');

});
</script> 

@endsection 