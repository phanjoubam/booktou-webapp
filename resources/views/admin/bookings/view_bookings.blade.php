@extends('layouts.admin_theme_02')
@section('content')


<!-- <div class="row">
     <div class="col-md-12">

    <div class='card'>
      <div class='card-body'>
        <div class="row">
                <div class="col-md-6">
                  Manage Bookings
                </div>
                <div class="col-md-6">

                      <form class="form-inline" method="post" action="">
            {{ csrf_field() }}
             <label class="my-1 mr-2">Date:</label>

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar'
             name='filter_date'  id="filter_date" />


              <label class="my-1 mr-2">Merchant:</label>

            <input type="text" class="typeahead form-control mr-2" id="merchantID"
            placeholder="Search by Merchant Name:">
            <input type="hidden" name="bin" id="bin">
                <button type="button" class="btn btn-primary btn-sm my-1 btnBookingSearch" value='search' name='btn_search'>Search</button>
            </form>
                </div>
 </div>
                </div>
           </div>
     </div>
 </div> -->



<div id="divResult" class="mt-2">
<div class="row" >
  {{csrf_field()}}
  <div class="col-md-12">

                               @if( count($orders ) > 0 )
                               <div class='card'>
                                <div class='card-header'>
                                  <h3>{{ $business->name  }}
                                    <span class='badge badge-primary badge-pill'>{{ $business->category }}</span></h3>
                                </div>
                               <div class="card-body">
                               <table class="table table-responsive">

                                            <th width=''>Order #</th>
                                            <th width=''>Booking Date</th>
                                            <th width=''>Service Date</th>
                                            <th width=''>Service Time</th>
                                            <th width=''>Services</th>
                                            <th width=''>Customer</th>
                                            <th width=''>Address</th>
                                            <th width=''>Total Cost</th>
                                            <th width=''>Paymode</th>
                                            <th width=''>Source</th>
                                            <th width='' class='text-center'>Action</th>

                               @foreach($orders as $item)
                                     <tbody>
                                        <tr  >
                                          <td class="text-left">
                                            <a class="badge badge-secondary" href="{{ URL::to('/services/booking/view-details') }}/{{  $item->id  }}?bin={{$item->bin}}" target='_blank'>
                                              {{$item->id }}
                                            </a>
                                          </td>
                                          <td>{{date('d-m-Y', strtotime( $item->book_date)) }}</td>
                                          <td>{{date('d-m-Y', strtotime( $item->service_date)) }}</td>
                                             <td>
                                              <span class="badge badge-success">
                                             {{$item->service_time}}

                                              </span>
                                            </td>

                                            <td>{{$item->service_name}} </td>

                                          <td>
                                               {{  $item->customer_name  }}
                                          </td>
                                          <td style="width: 200px;">{{ $item->address == "business" ? "Business Location" :  $item->address  }}
                                          </td>
                                          <td>{{ $item->total_cost }}</td>
                                          <td>
                                            <span class='badge badge-primary'>{{ $item->pay_mode  }}</span>
                                          </td>
                                          <td class="">
                                           @if($item->bizOrderNo=="0")
                                           <span class="badge badge-success"> Customer</span>
                                           @else
                                           <span class="badge badge-success">BIZ </span>
                                           @endif
                                          </td>
                                          <td>


                                          <div class="btn-group" role="group" aria-label="Basic example">

 <a class="btn btn-info "
 href="{{ URL::to('/services/booking/view-details') }}/{{$item->id}}?bin={{$item->bin}}" target='_blank'>
                                              <i class="fa fa-info-circle"></i>
                                            </a>
<button type="button" class="btn btn-warning btn-process" data-widget="1"  data-key="{{ $item->id   }}" data-pgc='1' ><i class="fa fa-cog"></i></button>



<button type="button" class="btn btn-success redirect"
data-key="{{ $item->id   }}" data-pgc='1' ><i class="fa fa-download"></i></button>
                                           </div>


                                          </td>


                                        </tr>
                                        </tbody>

                            @endforeach
                            </table>

                            </div>
                                  </div>
                                 @else
                                <p class='alert alert-info'>No New booking to show.</p>
                                @endif



     </div>

 </div>


</div>



<!-- Modal HTML -->
 <div id="widgetModalDelete" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <form method="get" action="{{action('Admin\ServiceBookingController@removeBooking')}}">
      {{csrf_field()}}
    <div class="modal-content">
      <div class="modal-header flex-column text-center">
        <h4 class="modal-title w-100 text-center"><i class="fa fa-trash text-center"></i> Are you sure? </h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <p>Do you really want to delete these records? This process cannot be undone.</p>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="deleteCancel">Cancel</button>
        <input type="hidden" id="deletebookingid" name="code">
        <input type="hidden" id="bin" name="bin" value="{{request()->get('bin')}}">
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>
    </div>
    </form>
  </div>
</div>
<!-- end modal html -->




<form method="post" action="{{action('Admin\ServiceBookingController@updateBookingStatus')}}"
id="ShowServiceProduct" >
{{csrf_field()}}
 <div class="modal" id="wg1" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Booking Progress Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="form-group row">
      <label for="status" class="col-sm-12 col-md-4 col-form-label">Booking Status:</label>
      <div class="col-sm-12 col-md-8">
      <select id="status" name='status' class="form-control">
        <option value='engaged'>Engage</option>
        <option value='canceled'>Cancelled</option>
        <option value='no_show'>No Show</option>
        <option value='completed'>Completed</option>
      </select>
      </div>
      </div>

       <div class="form-group row">
      <label for="status" class="col-sm-12 col-md-4 col-form-label">Remark:</label>
      <div class="col-sm-12 col-md-8">
       <textarea class="form-control" name="remarks" id="remark" cols="20" rows="7"></textarea>
      </div>
      </div>

      </div>
      <div class="modal-footer">
      <input type='hidden' value="" id="key1" name="bookingno" />
      <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>


</form>








@endsection

@section("script")

<script>


var path = 'auto-businees-name';


      $( "#merchantID" ).autocomplete({

        source: function( request, response ) {

          // Fetch data

          $.ajax({

            url:path,
            type: 'get',
            dataType: "json",
            data: {
              _token: $('input[name=_token]').val(),
               search: request.term
            },

            success: function( data ) {
               response( data );
            }
          });
        },

        select: function (event, ui) {
          $('#merchantID').val(ui.item.label);
          $('#bin').val(ui.item.value);
           return false;
        }

});



$('#merchantID').keypress(function(event){
 if (event.keyCode == 13)
 {
   event.preventDefault();
   fetchRecords();
 }
});



$(".btnBookingSearch").click(function(){

$.ajax({
         url: 'view-bookings',
         type: 'get',
         data: {'merchantcode':$('#bin').val(),
                'filter_date':$('#filter_date').val()
              },
         dataType: 'json',
         contentType: 'application/json',
         success: function(response){
        if(response.success)
                {
                  $('#divResult').empty().append(response.html);
                  StopLoading();
                }
                else
                  {
                      StopLoading();
                      alert(response.msg);

                  }


         }
});

});


$(document).on("click", ".btn-process", function(){

    var widgetno = $(this).attr("data-widget");
    $("#key" + widgetno).val(    $(this).attr("data-key") );
    $("#wg" + widgetno).modal('show');

});

$(document).on("click", ".delete", function(){

    var key = $(this).attr("data-key");

    $('#deletebookingid').val(key);

    $('#widgetModalDelete').modal('show');

 });


// $("#deleteConfirm").on("click", function(){

//     var key = $('#deletebookingid').val();
//     var filter_date = $('#filter_date').val();
//     $.ajax({

//         type: 'get',
//         url : 'remove-bookings',
//         data: {
//           'code': key,
//           'merchantcode':$('#bin').val(),
//           'filter_date':$('#filter_date').val()
//       },
//         dataType: 'json',
//         contentType: 'application/json',
//         success: function (response) {

//             if (response.success) {
//                 $("#divResult").empty().append(response.html);
//               }
//             else {
//                  alert("Unable to perform action");
//             }
//         }
//     });


//   });

function updateStatus(action,key) {
  var key =key;

  if (action == 'status') {

  $("#BookingID").val(key);
  //$("#merchant-code").val($('#merchantID').val());
  //$("#FilterDate").val($('#service_from').val());
  }
}



$(function() {
    $('.calendar').pignoseCalendar(
    {
      format: 'DD-MM-YYYY'
    });
});
</script>

@endsection
