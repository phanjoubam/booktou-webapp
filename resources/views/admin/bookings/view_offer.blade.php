@extends('layouts.admin_theme_02')
@section('content')
<div class="row">

	<div class="col-md-12">
	<div class="card">
		<div class="card-header">
			<h3>List of offer for {{$biz->name}} <span class="badge badge-primary badge-pill">{{$biz->category}}</span></h3>
		</div>
	</div>
	</div>
</div>

	
	@foreach($offer as $items)
	<div class="col-md-12">
	<div class="row mt-2 pt-2 pb-2" style="background-color: #fff;" >
		<!-- <div class="p-2" style="background-color:#fff;"> -->
	<div class="col-md-8">
	<div class="card">
		<div class="card-body">
		<div class="card-title">
		 Offer order no <span class="badge badge-primary mr-5"> {{$items->order_no}}</span>
			 Service name:<span class="badge badge-info mr-2"><small> {{$items->srv_name}}</small></span>
			Price: <small> {{$items->price}}</small><hr> 
			Details: <span class="mr-3"><small> {{$items->srv_details}}</small></span>	 
			 
		</div> 
     	</div>
	</div>
	</div>


	 <div class="col-md-4 ">
		<div class="card">
			<div class="card-body">
				 
				 @if($items->has_used=="yes")
				  <img src="{{ URL::to('/public/assets/image/coupon.png') }}">	 Applied in Order No: 				 	 
				 	<a class="badge badge-primary" href="{{URL::to('services/booking/view-details')}}/{{$items->order_paid_for}}?bin={{$biz->id}}"
				 	  >{{$items->order_paid_for}}</a>
				 @else 
				  <h5 class="blink text-center"> 
				  <span class="text-uppercase">Coupon valid<br> Apply Now.</span>
				  </h5>
				 @endif
			</div>
		</div>
	</div>
</div>
<!-- </div> -->
</div>	@endforeach


@endsection 
 
@section("script")

<script>
	
	$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
	});

</script>

@endsection