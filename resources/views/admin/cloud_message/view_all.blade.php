@extends('layouts.admin_theme_02')
@section('content')
<div class="row"> 
   
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                { 
                ?>
 <div class="col-sm-12 col-md-3 col-lg-3 mt-2">
                <div class="card" >
                  <img src="{{$item->image_url }}" height="240px" class="card-img-top"  >
                  <div class="card-body">
                    <h5 class="card-title">{{$item->title}}</h5>
                    <p class="card-text">{{ $item->body}}</p>
                  </div>
                  
                  <div class="card-body">
                    <button data-id='{{ $item->id }}' class='btn btn-sm btn-success btnConfBroadCast'>Broadcast</button>
                    <button data-id='{{ $item->id }}' class='btn btn-sm btn-danger btnConfBroadCastDelete'>Delete</button>
                  </div>
                </div>
 </div>
                
                         <?php
                          $i++; 
                       }

    ?> 

     <div class="col-sm-12 col-md-12 col-lg-12 mt-5">
      {{   $data['results']->appends(request()->input())->links()  }}
     </div>
 </div>

          
 
    
 <form  action="{{  action('Admin\AdminDashboardController@saveCloudMessage') }}" enctype="multipart/form-data"   method="post">

  {{ csrf_field() }}

 <div class="modal" id='composeMessage' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Compose Cloud Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-area">
        
      
  <div class="form-row">
    <div class="form-group col-md-12">
      <label for="business">Business</label>
      <select  name="business"  class="form-control btnbusiness selectize">
        @foreach($data['business'] as $items)
        <option value="{{$items->id}} , {{$items->name}}"> {{$items->name}}</option>
        @endforeach
      </select>
    </div>
    </div>
    <div class="productlist"></div>
    
        
      
  <div class="form-row">
    <div class="form-group col-md-12">
      <label for="title">Title</label>
      <input type="text" class="form-control" id="title" name='title'>
    </div>
   
  </div>
    <div class="form-row">

  <div class="form-group col-md-12">
    <label for="body">Message Body</label>
    <textarea type="text" class="form-control" id="body" name='body' placeholder="Message body" required></textarea>
  </div>

   </div>

   <div class="form-row">
    <div class="form-group col-md-12">
      <label for="url">Select Image:</label>
      <input type="file"  id="photo" name='photo' required>
    </div> 
  </div>
<div class='clear'></div>
  

      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" data-conf="" data-id="" >Save Changes</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>


 <form  action="{{  action('Admin\AdminDashboardController@broadcastCloudMessage') }}" enctype="multipart/form-data"  method="post"> 
  {{ csrf_field() }} 
 <div class="modal" id='confirmBroadcast' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Compose  &amp; Send Cloud Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-area"> 
      <p class='alert alert-info'>Repetitive marketing campaigns with lesser time frame may hurt your audience retention.</p>
      <div class="form-row">
       
       <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="changenotice">
        <label class="form-check-label" name='changenotice' for="changenotice">Modify notification message details</label>
      </div>

      <div class="form-row"> 
        <div class="form-group col-md-12"> 
          <label for="newtitle">New Title:</label>
          <input type="text" class="form-control" id="newtitle"  name="newtitle" placeholder="New title">
        </div>

        <div class="form-group col-md-12">
          <label for="newbody">New Message Body:</label>
          <textarea type="text" class="form-control" id="newbody" placeholder="New message body" name='newbody'></textarea>
        </div> 
      </div> 
    </div>  
    <div class='clear'></div>
  </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type="hidden"   name="cmid" id='cmid' />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" data-conf="" data-id="" >Send Now</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Cancel</button> 
      </div>
    </div>
  </div>
</div>
</form>

 <form  action="{{  action('Admin\AdminDashboardController@broadcastCloudMessageDelete') }}" enctype="multipart/form-data"  method="post"> 
  {{ csrf_field() }} 
 <div class="modal" id='confirmBroadcastdelete' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"> Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

   <div class="modal-body">You are about to delete this record. Please confirm your action?</div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type="hidden"   name="key" id='key' />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" data-conf="" data-id="" >delete</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">close</button> 
      </div>
    </div>
  </div>
</div>

</form>
@endsection

@section("script")

<script> 
 $(document).on("click", ".btn-compose", function()
{
  $("#composeMessage").modal("show") 
});


 $(document).on("click", ".btnConfBroadCast", function()
{
  var id = $(this).attr("data-id");
  $("#cmid").val(id);
  $("#confirmBroadcast").modal("show");

});

 $(document).on("click", ".btnConfBroadCastDelete", function()
{
  var id = $(this).attr("data-id");
  $("#key").val(id);
  $("#confirmBroadcastdelete").modal("show");

});

$('.selectize').select2({
        selectOnClose: true
      }); 


</script> 

@endsection 