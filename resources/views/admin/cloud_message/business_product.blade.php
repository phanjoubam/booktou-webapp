<div class="form-row ">
    <div class="form-group col-md-12">
      <label for="product">Product or Service List</label>
      <select class="select2 form-control" name="product">
        @foreach($products as $product)
        <option value="{{$product->package_id}} , {{$product->product_id}}, {{$product->prsubid}}" > {{$product->srv_name}}</option>
        @endforeach
      </select>
      
    </div>
    </div>

  <!--   <script>

    $('.select2').select2({
        selectOnClose: true
      }); 
    </script> 

 -->