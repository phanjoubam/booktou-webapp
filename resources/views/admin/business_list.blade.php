@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 



<div class="cardbodyy margin-top-bg"> 


  <div class="col-md-12">

 <div class="card">

 	  <div class="card-header">

 <div class="row">
   <div class="col-md-6">
    <h3>Listed Business</h3>
   </div>
    <div class="col-md-6 text-right">
 
        <form class="form-inline" method="get" action="{{ action('Admin\AdminDashboardController@viewListedBusinesses') }}" enctype="multipart/form-data">
              {{  csrf_field() }}
      
     <div class="form-group">
    <input type="search" class="form-control form-control-sm  my-1 mr-sm-2 " name="search_key" id="input-search" placeholder="Enter name or tags or city">
    </div>

     <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
      </form>
      </div>
  </div>    

           
            <hr/>
       </div>



 	<div class="card-body"> 

       <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
		<th scope="col">Sl.No.</th>
		<th scope="col">Name and No.</th> 
		<th scope="col">Address</th>
		<th scope="col">Phone</th>
    <th scope="col">Promotion Status</th>
		<th scope="col">Action</th>
		</tr>
	</thead>
	
		<tbody>

			 <?php $i = 0 ?>
		@foreach ($data['results'] as $item)
		<?php 
    $i++  ;
    $found = false;

    foreach($data['promo_businesses'] as $plist)
    {
      if($plist->bin == $item->id ) 
                              {
                                $found= true;break;
                              }
                            }
                          
     ?>


		 <tr  >
		 <td>{{$i}}</td>
		 <td>{{$item->name}} ({{$item->shop_number}})</td> 
		 <td>{{$item->locality}}, {{$item->landmark}}, {{$item->city}}, {{$item->state}}-{{$item->pin}}</td>
		 <td>{{$item->phone_pri}}</td>
      <td>
 @if(  $found )
  <span class='badge badge-primary'>PREMIUM</span>
@else 
<span class='badge badge-warning'>NORMAL</span>
 @endif

      </td>


		 <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                           <a class="dropdown-item" href="{{ URL::to('/admin/customer-care/business/view-profile') }}/{{$item->id}}">Business Profile</a> 
                           <a class="dropdown-item" href="{{URL::to('/admin/customer-care/business/view-products')}}/{{$item->id}}">Products</a> 
                           <a class="dropdown-item" href="{{ URL::to('/admin/customer-care/business/get-daily-earning/') }}/{{$item->id}}">Daily Earning  Report</a>    

                            <a class="dropdown-item" href="{{ URL::to('/admin/customer-care/business/view-importable-product/') }}/{{$item->id}}">View Importable Products</a>    
                           @if( !$found )
                            
                            <button class="dropdown-item btn_add_promo"  data-bin="{{$item->id }}"  >Promote Business</button> 

                            @endif

                        </div>
                      </div>
                  
        </td>
		 </tr>
	@endforeach
	</tbody>
		</table>
	
		
	</div>

	
	</div>


</div>

</div>


<div class="modal fade" id="modal_add_promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <form method='post' action="{{ action('Admin\AdminDashboardController@saveBusinessPromotion') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          This is final step to add promo list.  
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='bin' id='bin'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>

 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});
</script>


@endsection

