                      <div class="row">
                        <div class="col-md-4">
                          <label for="submodule" class="form-label">Sub Module</label>
                          <select class="form-control" name="submodule">
                           @foreach($submodule as $sub)
                            <option value="{{$sub->sub_module}}">
                              {{$sub->sub_module}}
                            </option>
                           @endforeach
                          </select>
                        </div>

                        <div class="col-md-4">
                          <label for="category" class="form-label">Category</label>
                          <select class="form-control" name="category">
                           @foreach($category as $cat)
                            <option value="{{$cat->name}}">
                              {{$cat->name}}
                            </option>
                           @endforeach
                          </select>
                        </div>
                      </div>