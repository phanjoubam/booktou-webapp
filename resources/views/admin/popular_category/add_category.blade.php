@extends('layouts.admin_theme_02')
@section('content')

<div class="row">
    <div class="col-md-8 offset-2"> 
	      <div class="card">
           
          <div class="card-header">
          <div class="row">
           <div class="col-md-4"> <h5>Add popular category:</h5></div>
           <div class="col-md-8"> 
            @if (session('detailed_msg'))
            <div class="card">
            <div class="alert alert-info p-0">
            {{session('detailed_msg')}}
            </div>
            </div>
            @endif 
          </div>
          </div>
          </div>
               
             <div class="card-body">
                   
              <form  method="post" action="{{action('Admin\PopularcategoryController@savePopularCategory')}}"
              class="row g-3" enctype="multipart/form-data">


                {{csrf_field()}}
                <!-- <input  type="hidden"  name="alsid" value="{{ request()->alsid }}"/> -->
                 
                
                   <div class="col-md-4">
                      <label for="inputState" class="form-label">Main Module</label>
                     
                      <select  class="form-control selectType" name="mainmodule"> 
                        <option>SELECT</option>
                       @foreach($category as $items)

                        <option value="{{$items->main}}">{{$items->main}}</option>
                         @endforeach 
                      </select>
                      
                  </div>
                  <div class="col-md-8 show-category-submodule">

                   </div>                  
                   <div class="col-md-6">
                        <label  class="form-label">Category-Image</label>
                       <input class="form-control" type="file" id="photo" name="photo">

                    </div>
                   <div class="col-md-6">
                        <label  class="form-label">Display-name</label>
                       <input class="form-control" type="text" id="display_name" name="display_name">

                    </div>
                                        
                     <div class="col-md-12 mt-3 text-center">
                       <div class="col">
                       	<button class="btn btn-primary" name="btn_save" value="save">Save</button>
                       </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection 
@section("script")
<script> 
  // var siteurl = "//stagingapi.booktou.in";
  var siteurl = " <?php echo config('app.url') ?>"; 
 
  $('.selectType').on('change', function() { 
    
    $.ajax({ 
    url : siteurl +'/admin/systems/view-submodule-category',

    data: {
    'items': this.value
    },
    type: 'get', 
    success: function (response) {
    if (response.success) { 
                        
                   $('.show-category-submodule').empty().append(response.html);
    }
    else  {
                    
                   $('.show-category-submodule').empty();
          }
    }

    });
});

</script>
@endsection