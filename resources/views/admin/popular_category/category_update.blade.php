@extends('layouts.admin_theme_02')
@section('content')

<div class="row">
<div class="col-md-12 ">
<div class="card">
    
          <div class="card-header">
          <div class="row">
           <div class="col-md-4"> <h5>View Category list</h5></div>
           <div class="col-md-8"> 
            @if (session('err_msg'))
            <div class="alert alert-info p-0">
            {{session('err_msg')}}
            </div>
            @endif 
          </div>
          </div>
          </div>
</div>
<div class="card">
<div class="card-body">
    <table class="table  table-bordered">
        <th scope="col">Category</th>
        <th scope="col">Image</th> 
        <th scope="col">Main Module</th>
        <th scope="col">Sub Module</th>
        <th scope="col">Action</th>
        <tbody>
            @foreach($category as $items)
                <tr>
                    <td>{{$items->category}}</td>
                    <td style="white-space:normal;padding:5px 5px;">
                     <img src='{{$items->image}}'  height="60px" width="60px"> 
                    </td>
                     <td>{{$items->main_module}}</td>
                      <td>{{$items->sub_module}}</td> 
                    <td>
                        <button class="btn btn-primary btnedit"
                        data-keyid="{{$items->id}}"
                        data-category="{{$items->category}}"
                        data-main="{{$items->main_module}}"
                        data-sub="{{$items->sub_module}}"
                        data-widget="edit">Edit</button>
                        <button class="btn btn-danger btndel" 
                        data-id="{{$items->id}}" data-widget="del"
                        >Delete</button>
                    </td>
                </tr>   
            @endforeach
        </tbody>
    </table>
   
  
</div>
</div>
</div>
</div>

<!-- edit option -->
<form action="{{action('Admin\PopularcategoryController@updateCategory')}}" method="post" > 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-edit" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Update popular movies information</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="card mt-2 mb-2">
            <div class="row "> 
                 <div class="col-md-4">
                      <label for="mainmodule" class="form-label">Main Module</label>
                     
                    <select class="form-control mainmodule"  id="mainmodule" name="mainmodule">

                    <option se>Shopping</option>
                    <option value="appointment">Appointment</option>
                    <option value="booking">Booking</option>
                    </select>
                     
                  </div>
                  <div class="col-md-8 show-category-submodule">
                      <div class="row">
                        <div class="col-md-4">
                          <label for="submodule" class="form-label">Sub Module</label>
                          <select class="form-control" name="submodule" id="submodule">
                         @foreach($submodule as $sub)
                            <option value="{{$sub->sub_module}}">{{$sub->sub_module}}
                            
                            </option>
                          @endforeach
                          </select>

                        </div>

                        <div class="col-md-4">
                          <label for="category" class="form-label">Category</label>
                          <select class="form-control" name="category" id="category">
                           @foreach($categorys as $cat)
                            <option value="{{$cat->name}}">{{$cat->name}}
                            
                            </option>
                           @endforeach
                          </select>
                        </div>
                      </div>
                   </div>                  
                   <div class="col-md-6">
                        <label  class="form-label">Category-Image</label>
                       <input class="form-control" type="file" id="photo" name="photo">

                    </div>
            </div>
             
        </div>
        <div class="modal-footer">
         <input type='hidden' name='key' id='key'/>
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' name='btndel'>Update</button>
        </div>
      </div>
    </div>
  </div>
 </form>
<!-- edit ends here -->

<!-- delete modal -->
<form action="{{action('Admin\PopularcategoryController@deleteCategory')}}" method="post" > 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-del" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Confirm Record Deletion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">You are about to delete this record. Please confirm your action?</div>
        <div class="modal-footer">
         <input type='hidden' name='key' id='keydel'/>
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' name='btndel'>Delete</button>
        </div>
      </div>
    </div>
  </div>
 </form>
 <!-- delete ends here -->

 

@endsection

@section('script')
<script type="text/javascript">


$(".btnedit").on('click', function(){ 
      var key  = $(this).attr("data-keyid"); 
      $("#key").val(key); 
      $widget  = $(this).attr("data-widget");  
      $("#category").val($(this).attr("data-category"));
      $("#mainmodule").val($(this).attr("data-main"));
       $("#submodule").val($(this).attr("data-sub")); 
      $("#widget-"+$widget).modal('show'); 
}); 

$(".btndel").on('click', function(){ 
      var key  = $(this).attr("data-id"); 

      $("#keydel").val(key); 
      $widget  = $(this).attr("data-widget"); 
      $("#widget-"+$widget).modal('show'); 
}); 

 
</script>
@endsection