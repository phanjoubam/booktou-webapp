@extends('layouts.admin_theme_02') 
@section('content')
<div class="row">
<div class="col-md-8"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Product Group Listing</div> 
                </div>
 <div class="col-md-4 text-right"> 
        
      @if (session('err_msg'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg') }}
      </div>
      </div>
      @endif
      </div>

       </div>
                </div>
            </div>
       <div class="card-body">  
<div class="table-responsive" >
    <table class="table">
         <thead class=" text-primary">
          <tr >
      <th scope="col">Title</th>
      <th scope="col">Rows</th>
      <th scope="col">Columns</th>
      <th scope="col">Allow View</th>
      <th scope="col">Action</th>
          </tr>
        </thead>
  
         <tbody>
		@foreach($data['section'] as $items)
             <tr>
             	<td>{{$items->title}}</td>
             	<td>{{$items->no_of_cols}}</td>
             	<td>{{$items->no_of_rows}}</td>
              <td>{{$items->allow_view}}</td>
             	<td>
             <button class="btn btn-primary showWidgetEdit" 
             data-keys="{{$items->id}}"
             data-title="{{$items->title}}"
             data-tag="{{$items->section_tag}}"
             data-description="{{$items->description}}"
             data-cols="{{$items->no_of_cols}}"
             data-rows="{{$items->no_of_rows}}"
             data-category="{{$items->category}}"
             data-view="{{$items->allow_view}}"
             >Edit</button>
             <button class="btn btn-danger showWidget" data-key="{{$items->id}}">Delete</button>
             	</td>
             </tr>    
         @endforeach
         </tbody>
            </table>
  
    
</div>
</div> 
  </div>  
  </div>  



<div class="col-md-4"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Product Group Setting</div> 
                </div>
 <div class="col-md-4 text-right"> 
 </div>

       </div>
                </div>
            </div>
  <div class="card-body"> 
  @if (session('err_msg_right'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg_right') }}
      </div>
      </div>
    @endif  
    
     <form class="row g-3" method="post" 
action="{{action('Admin\AdminDashboardController@saveSectionItems')}}">
      {{ csrf_field() }} 
 
      <div class="card-body">
          <div class="form-row">
			<div class="form-group col-md-12">
            <select class="form-control" name="category" id="category">
            	@foreach($data['category'] as $items) 
            	<?php 
            	$menu_name = strtolower($items->menu_name); 
            	?>
            	<option value="{{$menu_name}}">{{$items->menu_name}}</option>
            	@endforeach
            </select>
            </div>

            <div class="form-group col-md-12">
            <input type="text" class="form-control" id="title" name="title"  placeholder="Title" required="">
             </div>

           <div class="form-group col-md-12">
           <input type="text" class="form-control" id="tag" name="tag" placeholder="Tag">
           </div>
            
            <div class="form-group col-md-12 row">
            
           <div class="col-md-6 "> 
           	<input type="text" class="form-control" id="colsection" name="colsection"  placeholder="No of cols" required="">
           </div>
             <div class="col-md-6"> 
           	<input type="text" class="form-control" id="rowsection" name="rowsection"  placeholder="No of rows" required="">
           </div>
            </div> 

            <div class="form-group col-md-12">
            <textarea class="form-control"  placeholder="description" id="description"  
            name="description" style="height: 200px;"></textarea>
            </div>

            <div class="form-group col-md-12 row">
            
             <div class="col-md-6 "> 
             <label class="badge badge-info">Allow Section View</label>
             </div>
             <div class="col-md-3"> 
             <label class="mr-4">Yes</label>
             <input type="radio" class="form-check-input" id="allowviewyes" name="allowview" value="yes" selected>
             </div>
             <div class="col-md-3">
             <label class="mr-4">No</label>
             <input type="radio" class="form-check-input" id="allowviewno" name="allowview" value="no">
             </div>
            
            </div>

			<div class="form-group col-md-12"> 
				<input type="hidden" name="keyid" id="keyupdate">
			 <button type="submit" class="btn btn-primary" name="btn_save" value="Save">Save</button>  
			</div>

</div>
</div>
</form>

</div> 
</div>  
</div>  
</div>
 

<form action="{{action('Admin\AdminDashboardController@deleteSectionItems')}}" method="post" >
{{csrf_field()}}
<!-- Modal HTML -->
 <div id="deleteSection" class="modal fade deleteSection">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header flex-column text-center">
        <h4 class="modal-title w-100 text-center"><i class="fa fa-trash text-center"></i> Are you sure? </h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <p>Do you really want to delete these records? This process cannot be undone.</p>
        <input type="hidden" id="key" name="keyid">
      </div>
      <div class="modal-footer justify-content-center">
        <button type="submit" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="deleteCancel">Cancel</button>
      </div>
    </div>
  </div>
</div> 
<!-- end modal html -->
</form>


@endsection

@section("script")
<script> 

$(document).on("click", ".showWidgetEdit", function()
{
    var key = $(this).attr("data-keys");

    $('#title').val($(this).attr("data-title"));  
    $('#tag').val($(this).attr("data-tag"));  
    $('#category').val($(this).attr("data-category"));  
    $('#colsection').val($(this).attr("data-cols"));  
    $('#rowsection').val($(this).attr("data-rows"));  
    $('#description').val($(this).attr("data-description")); 

    if ($(this).attr("data-view")=="yes") {
        $('#allowviewyes').attr('checked',true);
        $('#allowviewno').attr('checked',false);
    }else{
        $('#allowviewno').attr('checked',true);
        $('#allowviewyes').attr('checked',false);
    }

   
   	$("#keyupdate").val(key);
    //$('.editSection').modal('show');
    
})

$(document).on("click", ".showWidget", function()
{
    var key = $(this).attr("data-key");  
   	$("#key").val(key);
    $('.deleteSection').modal('show');
    
})



</script>
@endsection