@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
<div class="col-md-12"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-4">
                  <div class="title">Manage Section Listing</div> 
                  @if (session('err_msg'))
				      <div class="col-md-12"> 
				      <div class="alert alert-info">
				      {{ session('err_msg') }}
				      </div>
				      </div>
				  @endif  
                </div>
 <div class="col-md-8 text-right">
 	<form method="get" action="{{action('Admin\AdminDashboardController@editProductSectionDetails')}}">
      	{{csrf_field()}}
      <div class="row">
<div class="col-md-6"> 

</div>
<div class="col-md-5">
	
	<select class="form-control" name="sectionid" id="sectionid">
    @foreach($data['section'] as $items)  
    <option @if(request()->get('sectionid')==$items->id)  selected @endif  value="{{$items->id}}">{{$items->title}}</option> 
    @endforeach
    </select> 

</div>
<div class="col-md-1">
	<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
</div>
     </div> 
 </form>
 </div>
</div>
</div>
</div>

@if(isset($data['sectionlist']))
<div class="card-body">  
	<div class="table-responsive" >
    <table class="table">
      <thead class=" text-primary">
      <tr>
      <th scope="col">Title</th> 
      <th scope="col">Product name</th>
      <th scope="col">Action</th> 
      </tr>
      </thead>
  
      <tbody>
		 @foreach($data['sectionlist'] as $items)
              <tr>
              	<td>{{$items->title}}</td>
              	<td>{{$items->prname}}</td>
              	 
              	<td><button class="btn btn-danger deleteSection" data-key="{{$items->id}}">Delete</button></td>  
              </tr>
         @endforeach 
      </tbody> 
    </table> 
	</div>
	</div>
	@endif 
</div> 
</div> 
</div>



<form action="{{action('Admin\AdminDashboardController@deleteProductSectionDetails')}}" method="post" >
{{csrf_field()}}
<!-- Modal HTML -->
 <div id="widgetSectionDelete" class="modal fade widgetSectionDelete">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header flex-column text-center">
        <h4 class="modal-title w-100 text-center"><i class="fa fa-trash text-center"></i> Are you sure? </h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <p>Do you really want to delete these records? This process cannot be undone.</p>
        <input type="hidden" id="keyid" name="keyid">
      </div>
      <div class="modal-footer justify-content-center">
        <button type="submit" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="deleteCancel">Cancel</button>
      </div>
    </div>
  </div>
</div> 
<!-- end modal html -->
</form>

@endsection

@section("script") 
<script> 
$(document).on("click", ".deleteSection", function()
{
    var key = $(this).attr("data-key");  
   	$("#keyid").val(key);
    $('.widgetSectionDelete').modal('show');
    
}) 
</script>
@endsection