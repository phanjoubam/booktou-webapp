@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
<div class="col-md-12"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-4">
                  <div class="title">Product Section Listing</div> 
                  @if (session('err_msg'))
				      <div class="col-md-12"> 
				      <div class="alert alert-info">
				      {{ session('err_msg') }}
				      </div>
				      </div>
				  @endif  
                </div>
 <div class="col-md-8 text-right"> 
        
    <!--   @if (session('err_msg'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg') }}
      </div>
      </div>
      @endif -->
      <form method="get" action="{{action('Admin\AdminDashboardController@productSectionDetails')}}">
      	{{csrf_field()}}
      <div class="row">
<div class="col-md-6">
 <select class="selectize" name="bin" id="bin">
            	@foreach($data['business'] as $items)  
            	<option value="{{$items->id}}">{{$items->name}}({{$items->frno}})</option>
            	@endforeach
      </select>

</div>
<div class="col-md-5">
	
	<select class="form-control" name="sectiontitle" id="sectiontitle">
            	@foreach($data['section'] as $items)  
            	<option value="{{$items->title}}">{{$items->title}}</option> 
            	@endforeach
      </select> 
</div>
<div class="col-md-1">
	<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
</div>
     </div> 
 </form>

      </div>

       </div>
                </div>
            </div>
@if(isset($data['product']))
<form method="post" action="{{action('Admin\AdminDashboardController@saveSectionDetails')}}">

	{{csrf_field()}}
	<input type="hidden" name="sectionname" value="{{$data['title']}}">
    <div class="card-body">  
	<div class="table-responsive" >
    <table class="table">
      <thead class=" text-primary">
      <tr >
      <th scope="col">Product name</th>
      <th scope="col">Description</th>
      <th scope="col">Category</th> 
      </tr>
      </thead>
  
      <tbody>
		 @foreach($data['product'] as $items)
              <tr>
              	<td><input type="checkbox" name="prsubid[]" value="{{$items->id}}" class="m-2">{{$items->pr_name}}</td>
              	<td>{{$items->description}}</td>
              	<td>{{$items->category}}</td>
              </tr>
         @endforeach
         <tr>
         <td></td>
         <td>
         <button type="submit" class="btn btn-primary text-center" value="Save" name="btn_save">
         Save
     	 </button>
     	 </td>
         <td></td>

         </tr>
      </tbody> 
    </table> 
	</div>
	</div>
</form>
@endif 
  </div>  
  </div>  


 
</div>


@endsection

@section("script")
 
<script> 

 $('.selectize').select2({
        selectOnClose: true
      }); 

</script>
@endsection