@extends('layouts.admin_theme_02')
@section('content')

 
  <div class="row">
     <div class="col-md-12"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Platform Configuration</div> 
                </div>
 <div class="col-md-4 text-right"> 
        
      </div>

       </div>
                </div>
            </div>
       <div class="card-body"> 

       @if (session('err_msg'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg') }}
      </div>
      </div>
    @endif


<div class="table-responsive" >
   <form class="form-inline" method="post" action="{{ action('Admin\AdminDashboardController@saveConfiguration') }}"  >
              {{  csrf_field() }}
    <table class="table">
         <thead class=" text-primary">
          <tr > 
        		<th scope="col" class="text-left">Config Key</th>
        		<th scope="col" class="text-center">Config Value</th>    
      		</tr>
      	</thead>
	
		<tbody>

			 <?php $i = 0 ?>
		@foreach ($data['configs'] as $item) 
		 <tr id='tr-{{ $item->id }}'>  
		 <td class="text-left" style='width: 160px'>{{$item->config_key }}</td>  
		 <td class="text-right" style='width: 350px'>
      <div class="form-row">
         <div class="col-md-8 text-right"> 
          <input type='text' value='{{$item->config_value}}' class='form-control form-control-sm' name='config_value[]'/>  
          <input type='hidden' value='{{$item->config_key}}' class='form-control form-control-sm' name='config_name[]'/>
        </div>
        <div class="col-md-4 text-left">
          <button class='btn btn-primary btn-sm' name='save' value='{{$item->config_key}}' type='submit'><i class='fa fa-save'></i></button> 
        </div>
      </div>

      </td>  
		 </tr>
	@endforeach

   <tr id='tr-{{ $item->id }}'> 
      <td  ></td>  
     <td class="text-left" style='width: 350px'> 

      <div class="form-row">
         <div class="col-md-8 text-right">
          
        </div>
        <div class="col-md-4 text-left">
          <button class='btn btn-primary btn-sm' type='submit' value='save_all' name='save_all'>Save All</button>  
        </div>
      </div>


          
      </td>  
     </tr> 

	</tbody>
		</table>
	
  </form>
		
	</div>
 </div> 
  </div>  
	</div> 
 
    
</div>

   

@endsection



@section("script")

<script>

  
  
</script>

@endsection

