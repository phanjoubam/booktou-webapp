@extends('layouts.admin_theme_02')
@section('content')

  
   <div class="row">
     <div class="col-md-12">  
     <div class="card  ">  
      <div class="card-header">
      <form method="get" 
          action="{{action('Admin\AdminDashboardController@viewMigrationReadylist')}}"> 
           <div class="row"> 
              <div class="col-md-8"> 

              <div class="panel-heading">
              <div class="card-title">
              <div class="title">PND NON Migrated List
                
                <span class="badge badge-warning">
                      <?php 
                      if (request()->get('month')!="") {
                          
                         $month =request()->get('month');

                      }else{

                           $month = date('m');
                      }
                      ?>
                    {{date('F', mktime(0, 0, 0, $month, 10))}}  
                </span>

              </div>
              </div>
              </div>
              </div>
              <div class="col-md-3 text-right"> 
                  <select name='month' class="form-control form-control-sm">
                  <option <?php if( date('m') == 1 ) echo "selected"; ?> value='1'>January</option>
                  <option <?php if( date('m') == 2 ) echo "selected"; ?>  value='2'>February</option>
                  <option <?php if( date('m') == 3 ) echo "selected"; ?> value='3'>March</option>
                  <option <?php if( date('m') == 4 ) echo "selected"; ?> value='4'>April</option>
                  <option <?php if( date('m') == 5 ) echo "selected"; ?> value='5'>May</option>
                  <option <?php if( date('m') == 6 ) echo "selected"; ?> value='6'>June</option>
                  <option <?php if( date('m') == 7 ) echo "selected"; ?> value='7'>July</option>
                  <option <?php if( date('m') == 8 ) echo "selected"; ?> value='8'>August</option>
                  <option <?php if( date('m') == 9 ) echo "selected"; ?> value='9'>September</option>
                  <option <?php if( date('m') == 10 ) echo "selected"; ?> value='10'>October</option>
                  <option <?php if( date('m') == 11 ) echo "selected"; ?> value='11'>November</option>
                  <option <?php if( date('m') == 12 ) echo "selected"; ?> value='12'>December</option>
                </select> 
             </div>

             <div class="col-md-1 text-left">
              <button class="btn btn-primary">Search</button> 
             </div>
          </div> 
        </form>
      </div>
           
  <div class="card-body">    
<div class="table-responsive">
    <table class="table">
                    <thead class="text-primary">  
                  <tr>  
                     
                    <th scope="col">Phone</th> 
                    <th scope="col">Name</th> 
                    <th class="text-right">Total PND Orders Delivered So Far</th> 

                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['pndorders'] as $item)
                { 
                ?>
  
                  <tr >  
                     
                   <td scope="col">
                     {{$item->phoneNo}} 
                           
                  </td>
                  <td>
                  	{{$item->drop_name}}  
                  </td>
                  <td class="text-right">
                     	<button type="button" class="btn btn-info" style='width: 120px'>Total: {{ $item->orderCount }}</button>
                  </td>

                 

              </tr>
              <?php
              $i++; 
            }
      ?>
    </tbody>
  </table> 
  {{ $data['pndorders']->appends(request()->input())->links() }}
</div>
       </div>     </div>

          </div>
        </div>  
   </div>
 
       </div>
 

 
 



@endsection

@section("script")

<script>
  

</script> 

@endsection 