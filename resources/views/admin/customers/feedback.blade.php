@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>



  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-6">
                  <div class="title">
                    Complaint/Feedback from customer <span class='badge badge-primary'>{{$month}} ,  {{$year}}</span> <br>
                   
                  Total Complaint: <span class="badge badge-danger">
                 {{$total_complaint}}</span>
               
                   Not Reply:<span class="badge badge-primary">{{$reply}}</span>
                  </div> 
                </div>

                <div class="col-md-6">
                   
                       <form class="form-inline offset-6" method="get"  
                        action="{{ action('Admin\AdminDashboardController@viewFeedback') }}">
                                  {{  csrf_field() }}
                          
                        <div class="form-row">
                        <div class="col-md-12">
                             
                             
                            <select name='month' class="form-control form-control-sm  ">
                              <option value='1'>January</option>
                              <option value='2'>February</option>
                              <option value='3'>March</option>
                              <option value='4'>April</option>
                              <option value='5'>May</option>
                              <option value='6'>June</option>
                              <option value='7'>July</option>
                              <option value='8'>August</option>
                              <option value='9'>September</option>
                              <option value='10'>October</option>
                              <option value='11'>November</option>
                              <option value='12'>December</option>

                            </select>
                           <select name='year' class="form-control form-control-sm  ">
                              <?php 
                              for($i=2021; $i >= 2020; $i--)
                              {
                                ?> 
                                <option value='{{ $i }}'>{{ $i }}</option>
                              <?php 
                              }
                              ?>

                            </select>
                       
                            <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
                        </div>

                        </div>
                      
                        </form>
            </div>


            </div>
            </div>

            </div>
        </div>
    </div>
</div>



@foreach($complaint as $feedback)
<div class="row mt-2">
<div class="col-md-12">
<div class="card">           
<div class="card-body">
<div class="row mb-3"> 
<div class="col-sm-12 col-md-4">
<div class="card">
<div class="p-4" style="background-color:#2196f3;color:#fff;">
       <h3>Order No: <span class="badge badge-danger">

        @foreach($order_type as $type)
        
        @if($type->id==$feedback->order_no)
        
         

        @if($type->type=="pnd") 
          #<a target="_blank" style="color: #fff;" href="{{URL::to('/admin/customer-care/pnd-order/view-details')}}/{{$feedback->order_no}}">{{$feedback->order_no}}</a>

          <br>

         
 
        @else
          #<a target="_blank" style="color: #fff;" href="{{URL::to('/admin/customer-care/order/view-details')}}/{{$feedback->order_no}}">{{$feedback->order_no}}</a>
 


        @endif

        @endif
        @endforeach
      </span></h3>  


      <h4>Posted On: <span class="badge badge-info"> {{$feedback->posted_on}}</span></h4>
       
</div>
<div class="p-3">   

          <strong>Customer:</strong><br>  
          <span scope="col">{{$feedback->fullname}}</span>
           
          <br>
          
          <span scope="col">{{$feedback->locality}}</span>
           <br>
           <span scope="col">{{$feedback->landmark}}</span>
          <small><br>
          <i class="fa fa-phone"></i>{{$feedback->phone}}</small>  
          <hr>
       </div>

</div>
</div> 
<div class="col-sm-12 col-md-4">
<div class="card p-2">
<h4 class="p-1"><span class="badge badge-primary">Type:</span> {{$feedback->type}}
</h4> <br>
 <p class="p-1"><span class="badge badge-primary">Complaint:</span></p><br>
  <span scope="col">{{$feedback->body}}</span>
</div>
</div>
 
<div class="col-sm-12 col-md-4 text-right"> 
<div class="card p-2">
  
  @if($feedback->reply=="")

  <div class="col text-right">
    <button class="btn btn-primary complaintmodal" data-bin="{{$feedback->id}}">Reply</button>
  </div>

  @else

  <h4 class="p-1"><span class="badge badge-primary">Reply from bookTou:</span></h4><br>
  <p class="p-1"><span class="badge badge-primary">Replied on:</span> {{date('Y-m-d',strtotime($feedback->replied_on))}}</p><br>
    
    <span class="p-1" scope="col">{{$feedback->reply}}</span> 
  @endif
   
   




</div>
</div>  
</div>
</div>
</div>
</div>
</div>
@endforeach






{{$complaint->links()}}





 
 


<div class="modal" tabindex="-1" id="modal_complaint">
  <div class="modal-dialog">
    <form  action="{{action('Admin\AdminDashboardController@replyFromBookTou')}}" method="post">
      {{csrf_field()}}

      @if(isset($feedback))
      <input type="hidden" value="{{$feedback->id}}" name="feedback_id" id="feedback_id">
      @endif
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reply from bookTou</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
      <div class="col-12"><textarea class="form-control" rows="7" cols="20" name="complaintReply" id="complaint_reply" required="" placeholder="your text here"></textarea></div>
      </div>
  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Reply</button>
      </div>
    </div>
    </form>
  </div>
</div>



@endsection



@section("script")

<script type="text/javascript">
  
 $(document).on("click", ".complaintmodal", function()
{

    $("#feedback_id").val($(this).attr("data-bin"));

    $("#modal_complaint").modal("show")

});

</script>


@endsection