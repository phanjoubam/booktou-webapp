@extends('layouts.admin_theme_02')
@section('content')
 
 
<?php 

$item = $data['profiles'];
 
?> 

<div class="row">
 <div class="  col-md-6 col-sm-12 col-xs-12  "> 
  <div class="card  ">
    <div class="card-body">  
        <?php 

            if($item->cusImage=="")
             { 
                $image_url =  URL::to('/') . "/public/assets/image/no-image.jpg"; 
             } 
          else
             {
              
                $image_url =  URL::to('/') . $item->cusImage;
             }     
         ?>
      <div class='text-center'>
      <img  class="rounded-circle" src='{{ $image_url }}' alt="..." height="200px" width="200px">
              </div>
       
       <br/>

      
        <h3 class="text-danger">{{$item->cusFullName}}</h3>
 <hr/>
        @if(isset( $data['banDetails'] ))
          <strong>Warning Status:</strong> <span class='badge badge-danger'>{{  $data['banDetails']->flag }}</span><br/>
          <strong>Warning Reason</strong> {{  $data['banDetails']->reason }}
       
        @else 
          <strong>Warning Status:</strong> <span class='badge badge-info'>NONE</span>
        @endif
          
      <hr class="my-4" /> 


     <div class="row">
       <div class="col-md-6">
        <div><strong>Customer ID # :</strong> {{$item->cusProfileId}}</div><br/>

         <div><strong>Date of Birth :</strong> {{date('d-m-Y', strtotime($item->cusDateOfBirth))}}</div><br/>

       </div>

       <div class="col-md-6">
         <div><strong>Contact No. :</strong> {{$item->cusPhone}}</div><br/>
         <div><strong>Email : </strong><span class="text-primary"> <?php echo $item->cusEmail ; ?></span></div><br/> 
       </div>
<div class="col-md-12">
   <hr/>
    <h4 class="text-danger">Address</h4>
    <hr/>
        <div><strong>Locality :</strong><br/>{{$item->cusLocality}}</div>
        <div><strong>Landmark :</strong><br/>{{$item->cusLandmark}}</div>
        <div><strong>City :</strong> {{$item->cusCity}} <strong>Pin Code :</strong> {{$item->cusPin}}</div>  
        <div><strong>State :</strong> {{$item->cusState}}</div>
</div>

     </div>
</div>

   </div>
     



      <div class="card mt-3">
        <div class="card-body"> 
      <div class="card-title"> 
       <div class="title">Customer Reviews</div>
     </div>

      <hr class="my-4" /> 

<div class="row">
<div class="col-md-12">
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<a class="btn card-link">  {{$data['totalRating']}} rating </a>
</div>
</div>

<div class="row">
<div class="col-md-12">

<div class="row">
<div class="col-md-2">
  5 star
</div>

<div class="col-md-8">
  <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingFiveCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

</div>
</div>
<div class="col-md-2">
  {{$data['ratingFiveCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-2">
  4 star
</div>

<div class="col-md-8">
 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingFourCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingFourCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-2">
  3 star
</div>

<div class="col-md-8">

 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingThreeCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingThreeCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-2">
  2 star
</div>

<div class="col-md-8">

 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingTwoCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingTwoCount']}}%
</div>
</div>


<div class="row">
<div class="col-md-2">
  1 star
</div>

<div class="col-md-8">
 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingOneCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div> 
</div>
</div>
<div class="col-md-2">
  {{$data['ratingOneCount']}}%
</div>

</div>
 

     </div>
   </div>
 </div>


</div>
 

  </div>

 

<div class="col-md-6 col-sm-12 col-xs-12">  
  <div class="card">
    <div class="card-header">
      <h3 class="mb-0">Order Fulfillment Chart on <span class='badge badge-info badge-pill'>{{ count($data['orders']) }}</span> Orders</h3>
    </div>
    <div class="card-body">  
      <canvas height="160" id="order_frequency"></canvas>
    </div>
  </div>


<div class="card  mt-4 ">
  <div class="card-body ">
    <div class="card-title"> 
       <h3>Orders History</h3>
     </div>
<hr class="my-4" />  
  <?php 
  //preparing graph data 
  $new_count =0;
  $completed_count =0;
  $cancelled_count =0;
  ?>
 @if(isset($data['orders']) && count($data['orders']) > 0)

   <div class="timeline timeline-xs">
      <div class="timeline timeline-xs">
         @foreach($data['orders'] as $itemO) 
         <?php 
          //preparing graph data   
          if( strcasecmp( $itemO->book_status, "completed") == 0 || strcasecmp( $itemO->book_status, "delivered") ==  0 )
          {
            $completed_count++;
          }
          else if( strcasecmp( $itemO->book_status, "canceled") == 0 || strcasecmp( $itemO->book_status, "cancelled") ==  0  ||  
            strcasecmp( $itemO->book_status, "cancel_by_client") ==  0 || strcasecmp( $itemO->book_status, "cancel_by_owner") ==  0  )
            {
              $cancelled_count++;
            }
            else 
            {
              $new_count++;
            }
        ?>
        <div class="timeline-item">
            <div class="timeline-item-marker">
            </div>
            <div class="timeline-item-content">
              <div class="timeline-item-marker-text"><strong>Order No. :</strong><a href="{{ URL::to('/admin/order/view-information') }}?orderno={{$itemO->id}}" target='_blank'><span class='badge badge-success badge-pill white'>{{$itemO->id}}</span></a></div>
              <div class="timeline-item-marker-indicator bg-green"></div>
              <strong>Order Date:</strong> {{ date('d-m-Y', strtotime($itemO->book_date))}}<br/>
              <strong>Order Status:</strong> {{$itemO->book_status}}
                
                @switch($itemO->book_status)
                  @case("new")
                  <span class='badge badge-primary'>New</span>
                  @break
                  @case("confirmed")
                  <span class='badge badge-info'>Confirmed</span>
                  @break 
                  @case("order_packed")
                  <span class='badge badge-info'>Order Packed</span>
                  @break 
                  @case("package_picked_up")
                  <span class='badge badge-info'>Package Picked Up</span>
                  @break 
                  @case("pickup_did_not_come")
                  <span class='badge badge-warning'>Pickup Didn't Come</span>
                  @break 
                  @case("in_route")
                  <span class='badge badge-success'>In Route</span>
                  @break  
                  @case("completed")
                  <span class='badge badge-success'>Completed</span>
                  @break  
                  @case("delivered")
                  <span class='badge badge-success'>Delivered</span>
                  @break  
                  @case("delivery_scheduled")
                  <span class='badge badge-success'>Delivery Scheduled</span>
                  @break  
                  @case("canceled")
                  <span class='badge badge-danger'>Order Cancelled</span>
                  @break 
                  @case("cancelled")
                  <span class='badge badge-danger'>Order Cancelled</span>
                  @break
                @endswitch
                <br/>
            </div>
          </div> 
        @endforeach
      </div>
    </div>
    
  @else
    <div class="alert alert-info text-center">No order has been placed so far!</div>
  @endif
</div>
</div>


</div>
</div> 

@endsection

@section('script')

<style>
.timeline-item-content {
    padding: 10px;
    border: 1px solid #d0cdcd;
    border-radius: 10px;
}
</style>

<script>
 const ctx = document.getElementById('order_frequency').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['New', 'Cancelled', 'Completed' ],
        datasets: [{
            label: '# of Votes',
            data: [<?php echo $new_count; ?>, <?php echo $cancelled_count; ?>, <?php echo $completed_count; ?>],

 
            backgroundColor: [ 
                'rgb(54, 162, 235)',
                'rgb(255, 99, 8)',
                'rgba(8, 220, 0 )'
            ],
            borderColor: [ 
              'rgb(54, 162, 235)',
              'rgb(255, 99, 8)',
              'rgba(8, 220, 0 )'
            ], 
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

</script>

@endsection