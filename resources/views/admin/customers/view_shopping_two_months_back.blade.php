@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>

<div class="row">
     <div class="col-md-12"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row"> 
                <div class="col-md-6">
                <div class="title">
                Customer who have shop online in App two months back
                </div> 
                </div> 
                <div class="col-md-6 text-right">
                <a href="{{URL::to('admin/customer/customer-shopping-two-months-back-to-excel')}}" class="btn btn-primary">Export to Excel</a> 
                </div> 
                </div>
              </div> 
            </div>
     </div>

     <div class="card mt-2">
            <div class="card-body">
            <table class="table"> 
                <thead> 
                    <th>Customer ID</th>
                    <th>Customer Name</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Landmark</th> 
                </thead>
                <tbody>
                   @foreach($result as $customerlist)
                    <tr>
                    <td><a   href="{{  URL::to('/admin/customer/view-complete-profile/'.$customerlist->id )}}" target="_blank"><strong>{{$customerlist->id}}</strong></a></td>
                    <td>{{$customerlist->fullname}}</td>
                    <td>{{$customerlist->phone}}</td>
                    <td>{{$customerlist->locality}}</td>
                    <td>{{$customerlist->landmark}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$result->links()}}
            </div>
        </div>
    </div> 
</div>

@endsection 

@section("script")
<script type="text/javascript">



</script>
@endsection