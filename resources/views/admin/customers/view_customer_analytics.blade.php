@extends('layouts.admin_theme_02')
@section('content')

<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
        <div class="card-header"> 
         <div class="card-title" >
         <div class="title">Customer Analytics:</div>
         </div> 
        </div>

                <div class="card-body">  
                    <div class="card-group"> 
                      <div class="col-md-6">
                      <div class="card"> 
                        <div class="card-body">
                            <div class="row">
                            <div class="col-md-8">
                               <h5>Stopped using bookTou App three months back </h5>
                            </div>

                            <div class="col-md-4 text-right">
                             <h5>  <a href="{{URL::to('admin/customer/view-customer-three-months-back')}}"><span class="badge badge-warning"> {{$analytics_one->totalCustomer}} </span></a></h5>
                            </div> 
                        </div>
                        </div> 
                      </div>
                      </div> 

                      <div class="col-md-6">
                      <div class="card"> 
                        <div class="card-body">
                            <div class="row">
                            <div class="col-md-8">
                               <h5>Shop online in bookTou app two months back</h5>
                            </div>

                            <div class="col-md-4 text-right">
                            <a href="{{URL::to('admin/customer/view-customer-shopping-two-months-back')}}"><span class="badge badge-warning">{{$analytics_two->countCustomer}}</span></a>
                            </div> 
                        </div>
                        </div> 
                      </div>
                      </div>
                    </div> 

                    <div class="col-md-6 mt-4">
                      <div class="card"> 
                        <div class="card-body">
                            <div class="row">
                            <div class="col-md-8">
                            <h5>Browse bookTou app last week,but not bought for two months back.</h5>
                            </div>

                            <div class="col-md-4 text-right">
                            <a href="{{URL::to('admin/customer/view-customer-browse-last-week')}}"><span class="badge badge-warning">{{$analytics_three->lastweekCount}}</span></a>
                            </div> 
                        </div>
                        </div> 
                      </div>
                      </div>
                </div>



            </div>
          </div>
        </div>
@endsection
@section("script")
<script> 

</script>
@endsection 