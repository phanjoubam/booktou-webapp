@extends('layouts.admin_theme_02')
@section('content') 
  <?php 

    $order_info = $data['order_info'];
    $order_items  = $data['order_items'];
    $customer = $data['customer'];
    $agent = $data['agent_info'];    
    $order_no =$order_info->id ; 
  ?> 
  
 <div class="row"> 
  @if (session('err_msg'))
    <div class="col-md-12">
      <div class="alert alert-info">
    {{ session('err_msg') }}
    </div>
    </div>
  @endif 

   <div class="col-md-8"> 
 
     <div class="card panel-default">
           <div class="card-header">
              <div class="card-title"> 
                 <h5 class="card-category">Order # {{ $order_info->id  }} OTP: <span class='badge badge-info badge-pill'>{{ $order_info->otp }}</span> 
                 @switch($order_info->book_status)

                      @case("new")
                        <span class='badge badge-primary badge-pill'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info badge-pill'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info badge-pill'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info badge-pill'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning badge-pill'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success badge-pill'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success badge-pill'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success badge-pill'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success badge-pill'>Delivery Scheduled</span>
                      @break 
                      @case("cancelled")
                        <span class='badge badge-danger badge-pill'>Order Cancelled</span>
                      @break 
 @case("cancelled")
                        <span class='badge badge-danger badge-pill'>Order Cancelled</span>
                      @break 

                      @endswitch

                      <span> Order Date:</span> <span class='badge badge-success badge-pill'>{{ date('d-m-Y h:i:a', strtotime( $order_info->book_date)) }}</span> 
                      
                      <span>Payment Mode:</span> 
          @if( strcasecmp($order_info->payment_type,"Cash on delivery") == 0   || strcasecmp($order_info->payment_type,"COD") == 0  || strcasecmp($order_info->payment_type,"POD") == 0   )
            <span class='badge badge-warning'>CASH</span>
          @else 
            <span class='badge badge-success'>ONLINE</span>
          @endif  



                    </h5>
                    <hr> 
                </div>
                <div>
                  <h5><span>Service Date:</span>
                    <span class='badge badge-warning badge-pill'>{{ date('d-m-Y', strtotime( $order_info->service_date)) }}</span>
                    <span>Time:</span>
                    <span class='badge badge-warning badge-pill'>{{ date('h:i:a', strtotime( $order_info->preferred_time)) }}</span>
                  </h5>
                </div>
            </div>
       <div class="card-body">     
          @if($data['adminFrno'] == $order_info->route_to_frno)
          @else 
          <h5>Routed to: <span class="badge badge-success badge-pill">{{$data['routed_frno']->zone}} </span></h5>
          @endif  
             
<table border='0' cellpadding='1' cellspacing='1' style='width: 100% !important; margin-left:auto; margin-right:auto; font-size:13px; ' >
                    <thead class=" text-primary">
                      <th>Sl.No</th>
                      <th>Image</th>
                      <th style='width: 200px'>Item</th>
                      <th style='width: 80px'>Gst %</th>
                      <th style='width: 80px'>Qty</th>
                      <th style='width: 50px'>Unit</th>
                      <th style='width: 140px'>Unit Price</th> 
                      <th style='width: 50px'>Packing</th> 
                      <th class='text-right' style='width: 220px'>Item + Packing = Sub-Total</th>  
                    </thead>
                    <tbody>
                      <?php $i = 0 ;
                      $item_total  =0;
                      $pack_total = 0;
                      $sub_total = 0;

                      $cgst =0.00;
                      $sgst =0.00;

                      ?>
               

               @foreach ( $order_items   as $item)

               <?php

               $i++;
               $item_total += ( $item->price * $item->qty ) ;
               $sub_total += ( $item->price * $item->qty )  +  ( $item->package_charge * $item->qty );
               $pack_total += ( $item->package_charge * $item->qty );
               

               $cgst +=  ( $item->price * $item->qty * $item->cgst ) / 100;
               $sgst +=  ( $item->price * $item->qty * $item->sgst ) / 100;


                ?>

                    <tr >
                      <td>{{$i}}</td>
                      <td>
                          <img style="width: 30px;height:30px"   src="{{ $item->photos }}"  data-src="{{ $item->photos }}"
                          data-item="{{ $item->description }}" class="mr-3 showpreview" alt="{{$item->pr_name}}">  
                      </td>
                      <td style='width: 200px'>
                        <?php echo preg_replace('~((\w+\s){4})~', '$1' . "\n", $item->pr_name ) ;?><br/>
                        <small>{{ implode(' ', array_slice(str_word_count( $item->description ,1), 0,10)) }}</small>
                        <span style='cursor: pointer;' data-src="{{ $item->photos }}"  data-item="{{ $item->description }}"  class="showpreview">[ ... ]</span>
                    </td>
                    <td>

                       {{ number_format( ($item->cgst + $item->sgst )  , 2, ".", "" )   }}
                    </td>
                      <td>
                      {{$item->qty}} <span class='badge badge-danger showconfirmdel' data-itemno="{{ $item->id }}" data-oid="{{ $order_info->id }}" title="Remove item from order"><i class="fa fa-times"></i></span> 
                    </td>   
                    <td>
                      {{$item->unit}}
                    </td> 
                    <td>
                       {{ $item->price }}  
                    </td>  
                     <td>
                       {{ $item->package_charge * $item->qty  }}  
                    </td>  

                     <td class='text-right'>
                         ( {{ $item->price }} X {{ number_format( $item->qty   , 0 , ".", "" )   }} ) +  ( {{ $item->package_charge }} X {{ number_format( $item->qty   , 0 , ".", "" )  }} )  =     {{  number_format(  ( $item->price * $item->qty )  +  ( $item->package_charge * $item->qty )  , 2, ".", "" ) }}  
                    </td> 

                    </tr>
                   @endforeach

                    <tr>

                    <td colspan="8">
                      <strong>Total Item Cost:</strong>
                    </td>
                    <td class='text-right'>
                     {{ number_format(  $item_total , 2, ".", "" )   }}
                    </td>

                   </tr>

                   <tr> 
                    <td colspan="8">
                      <strong>Total Cost: (Item Cost + Packaging Cost)</strong>
                    </td>
                   
                    <td class='text-right'>
                      {{ number_format( $order_info->total_cost, 2, ".", "" )   }} <button type='submit' data-price='{{ number_format( $order_info->total_cost , 2, ".", "" )   }}' data-key="{{ $order_info->id  }}" 
                        class='btn btn-danger btn-sm btn-xs showpriceupdate'    name='btn-update-vc'>Edit</button>
                    </td>

                   </tr> 
                   <tr> 
                    <td colspan="8">
                      <strong>To refund:</strong>
                    </td>
                   
                    <td class='text-right'>
                      {{ number_format( $order_info->refunded, 2, ".", "" )   }} <button type='submit' data-price='{{ number_format( $order_info->total_cost , 2, ".", "" )   }}' data-key="{{ $order_info->id  }}" 
                        class='btn btn-danger btn-sm btn-xs showrefund'    name='btn-update-vc'>Add/Update</button>
                    </td>



                   </tr> 

 @if( Session::get('_user_role_') >= 10000  )

                 
 @endif


                   <tr>

                    <td colspan="8">
                      <strong>Actual Delivery Charge:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format( $order_info->delivery_charge , 2, ".", "" )   }}
                    </td>

                   </tr>

                   <tr>

                    <td colspan="8">
                      <strong>(Total Cost + Delivery Charge)</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format(( $order_info->total_cost + $order_info->delivery_charge)   , 2, ".", "" )   }}
                    </td>

                   </tr>

                   @if($order_info->coupon)
                    <tr>

                    <td colspan="8">
                      <strong>Discount Coupon:</strong> <button data-widget="rem-coupon" data-key='{{ $order_info->id }}' class='btn btn-sm btn-danger showmodal'>Remove</button>
                    </td>
                    <td class='text-right'>
                        <span class='badge badge-info'>{{ $order_info->coupon }}</span> 
                    </td> 
                   </tr>
                   @endif 

                    <tr>

                    <td colspan="8">
                      <strong>Discount Applied:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format( $order_info->discount , 2, ".", "" )   }} 
                    </td> 
                   </tr>
                   
                    <tr> 
                    <td colspan="8">
                      <strong>GST:</strong>
                    </td>                   
                    <td class='text-right'>
                      {{ number_format(   $cgst + $sgst , 2 , ".", "" )   }}
                    </td>
                   </tr>
                   <tr>

                    <td colspan="8">
                      <strong>Total Payable After Discount:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format(  ( $order_info->total_cost + $cgst + $sgst + $order_info->delivery_charge) - $order_info->discount   - $order_info->refunded , 2, ".", "" )   }}
                    </td>

                   </tr>

                   <tr>

                    <td colspan="8">
                      <strong>Printable eBill:</strong>
                    </td>
                    <td class='text-right' colspan="4">

        <div class="form-row">
              <div class="col-md-10 text-right">
               <input readonly class='form-control disabled' value="{{ URL::to('/pos/orders/view-bill') }}?o={{ $order_info->id }}" />
              </div>
              <div class="col-md-2">
                <a data-widget="rem-coupon" target='_blank' href="{{ URL::to('/pos/orders/view-bill') }}?o={{ $order_info->id }}"  ><i class='fa fa-download'></i></a>

              </div>
            </div>



 

                      
                       
                    </td>

                   </tr> 



                    </tbody>


                  </table> 
              </div>
            </div>




            <div class="card  mt-3">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row"> 
                   <div class="col-md-6">
                  <div class="title">Extras</div> 
                </div> 
              </div>
                </div>
            </div>
       <div class="card-body">    
           <div class="table-responsive"> 
            <form action='{{ action("Admin\AdminDashboardController@applyCoupon") }}' method='post'>
  {{ csrf_field() }}

                  <table class="table"> 
                    <tbody>  
                   <tr> 
                    <td colspan="4">
                      <strong>Specify Voucher Code:</strong>
                    </td>
                    <td>
                       <input type='text' class='form-control form-control-sm' name='coupon' placeholder='Provide coupon code'/>
                    </td>

                    <td style='width: 120px;'> 

                       <button type='submit' class='btn btn-success btn-sm btn-block' name='btn-update-vc'>Apply</button>
                    </td> 

                   </tr>
                    </tbody> 
                  </table>
 <input type='hidden' name='order_no' value="{{ $order_no }}" /> 
</form>



 <form action='{{ action("Admin\AdminDashboardController@saveDeliveryCharge") }}' method='post'>
  {{ csrf_field() }}
    <table class="table"> 
                    <tbody>  
                    <tr> 
                    <td colspan="4">
                      <strong>Update Delivery Charge:</strong>
                    </td>
                    <td>
                       <input type='number' min='0.00' class='form-control form-control-sm' name='deliverycharge' placeholder='Update delivery charge'/>
                    </td>

                    <td style='width: 120px;'> 
                       <button type='submit' class='btn btn-success btn-sm btn-block' name='btn-update-delivery'>Save</button>
                    </td>


                   </tr>


                   <tr> 
                    <td colspan="5"> 
                    </td>
                    <td>
                      
                    </td>

                   </tr>  
                    </tbody> 
                  </table>
      <input type='hidden' name='order_no' value="{{ $order_no }}" /> 
</form>


                </div>
              </div>
            </div>

           


 </div>


 <div class="col-md-4">   
  
   <div class="card  ">
              <div class="card-header">
                <div class='row'>
                  <div class='col-md-10'>
                <h5 class="card-category">Remarks &amp; Status Update</h5> 
</div>
 <div class='col-md-2 text-right'>
<div class="dropdown ">
  <button class="btn btn-primary  " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class='fa fa-cog'></i></button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item showremmodal"   data-key='{{ $order_info->id  }}' href="#">Add Remarks</a> 
    <a class="dropdown-item showchangestatus"   data-key='{{ $order_info->id  }}' href="#">Change Status</a>
    <a class="dropdown-item btnchangezone" data-key='{{ $order_info->id  }}' href="#">Edit Source Zone</a>
    <a class="dropdown-item  btnchangetofranchise"   data-key='{{ $order_info->id  }}' href="#">Migrate</a>  
  </div>
</div>
</div>
              </div>
               </div> 
              <div class="card-body">  

              @if( isset($order_info->cust_remarks) )
                <div class='alert alert-warning'>
                  <strong >Customer Remarks</strong><br/>
                  {{ $order_info->cust_remarks }}
                </div>
                <hr/>
              @endif

              @if(isset( $data['remarks'] ) && count($data['remarks']) > 0)
                <div class="timeline timeline-xs">
                  <div class="timeline timeline-xs"> 
                    @foreach($data['remarks'] as $rem)

                        @if($rem->remark_by == 0)
                            @php 
                              $staff_name = "Admin";
                            @endphp
                        @else
                            @php 
                              $staff_name = "Untracked";
                            @endphp
                            @foreach($data['staffs'] as $staff)
                              @if($staff->id == $rem->remark_by)
                                @php 
                                  $staff_name = $staff->fullname;
                                @endphp
                                @break
                              @endif
                            @endforeach
                        @endif
                        <div class="timeline-item">
                          <div class="timeline-item-marker">
                            <div class="timeline-item-marker-text">
                              <span class='badge badge-danger badge-pill white'>{{ date('d-m-Y h:i a', strtotime( $rem->created_at)) }}</span>
                            </div>
                            <div class="timeline-item-marker-indicator bg-red"></div>
                          </div>
                          <div class="timeline-item-content">
                            {{ $rem->remarks}}<br/> 
                            <span class='badge badge-danger badge-pill'>{{ $rem->rem_type}}</span><br/>
                            <strong>Entered by</strong> <span class='badge badge-info badge-pill'>{{ $staff_name}}</span>

                          </div>
                        </div> 

                    @endforeach 
                  </div>
                </div>
                @else 
                  <p class='alert alert-info'>No remarks recorded.</p>
                @endif
 


              </div> 
            </div>


            <div class="card panel-default mt-3">
       <div class="card-header">
          <h5 class="card-category">Agent Provided Information</h5> 
        </div>
        <div class="card-body ">  
      
      @if( isset($order_info->agent_remarks) )
                <div class='alert alert-info'>
                  <strong >Agent Remarks</strong><br/>
                  {{ $order_info->agent_remarks }}
                </div>
                <hr/>
      @else 
           <div class='alert alert-info'>
                  <strong >Agent Remarks</strong><br/>
                  No remarks provided!
                </div>
                <hr/>
      @endif




            @if(isset( $data['agent_uploaded_images'] ) && count($data['agent_uploaded_images']) > 0)
                    @foreach($data['agent_uploaded_images'] as $image)
                
                      <img style="width: 80px;height:80px"   src="{{ $image->imageUrl }}"  data-src="{{ $image->imageUrl }}"
                          data-item="Upload time {{ date('d-m-Y h:i a', strtotime( $image->uploadedDate )) }}" 
                          class="mr-1 showpreview" alt="Upload time {{ date('d-m-Y h:i a', strtotime( $image->uploadedDate )) }}">  
 
                          
                      @endforeach 
                      <hr/>
                   <p>Click on an image to enlarge</p>
                @endif 
      
       </div>
    </div>



            <div class="card panel-default mt-2">
              <div class="card-header">
                <h5 class="card-category">Merchant and Customer Info</h5> 
                </div>
              <div class="card-body"> 


                 <div class="timeline timeline-xs">
                  <div class="timeline timeline-xs">
                    <div class="timeline-item">
                      <div class="timeline-item-marker">
                        <div class="timeline-item-marker-text"><span class='badge badge-danger badge-pill'>Pickup from</span></div>
                        <div class="timeline-item-marker-indicator bg-red"></div>
                      </div>
                      <div class="timeline-item-content">
                        {{$order_info->businessName}}<br/>
                        {{$order_info->businessLocality}}<br/>
                        {{$order_info->businessLandmark}}, {{$order_info->businessCity}}<br/>
                        {{$order_info->businessState}} - {{$order_info->businessPin}}<br/>
                        <i class='fa fa-phone'></i> {{$order_info->businessPhone}}
                      </div>
                    </div> 

                     <div class="timeline-item">
                      <div class="timeline-item-marker">
                        <div class="timeline-item-marker-text"><span class='badge badge-success badge-pill'>Deliver at</span></div>
                        <div class="timeline-item-marker-indicator bg-green"></div>
                      </div>
                      <div class="timeline-item-content">
                        {{ $customer->fullname }} <a class='badge badge-info' href="{{ URL::to('/admin/customer/view-complete-profile') }}/{{ $order_info->book_by }}" target='_blank'>View Profile</a><br/>
                        {{$order_info->address}}<br/>
                        {{$order_info->landmark}}<br/>
                        {{$order_info->city}}, {{$order_info->state}} - {{ $order_info->pin_code}} <br/>
                        <i class='fa fa-phone'></i> {{$customer->phone}}
                      </div>
                    </div>


                  </div>
                </div>
 

 
              </div> 
            </div>

 
 



            @if(isset($agent))
              <div class="card card-default mt-2">
              <div class="card-header">
                <h5 class="card-category">Delivery Agent</h5>
                <h4 class="card-title">{{$agent->deliveryAgentName}}</h4>
                <p>
                  <i class='fa fa-phone'></i> {{$agent->deliveryAgentPhone}}</p> 
                </div> 
              <div class="card-footer">
                 <button class='btn btn-danger btn-sm btnRemoveAgent' data-key='{{ $order_info->id   }}'>Remove</button>
              </div>
            </div>

            @else 
            
          <div class="card card-default mt-2">
              <div class="card-header">
                <h5 class="card-category">Delivery Agent</h5> 
                </div>
              <div class="card-body"> 
                 <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">Agent</span>
                    </div> 
                    <select   class='form-control  ' id="aid_{{$order_info->id }}" name='agents' >
                      @foreach($data['all_agents'] as $aitem) 
                        <option value='{{ $aitem->id }}'   >{{ $aitem->fullname }}</option>
                      @endforeach 
                    </select>
                   <div class="input-group-append"> 
                     <button class="btn btn-primary btn-assign" data-oid="{{$order_info->id}}" >Assign</button>
                    </div>
                  </div>

                  </div>
              
            </div>


            @endif




          </div>  

       </div> 
 

 
<form action="{{ action('Admin\AdminDashboardController@updateNormalOrderRemarks') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalrem" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Remarks Update</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"> 

          <div class="form-group">
          <label for="type">Remark Type</label>
          <select  class="form-control" id="type" name='type' aria-describedby="type">
            <option>Customer Feedback</option>
            <option>CC Remarks</option>
            <option>Agent Feedback</option>
            <option>Completeion Remark</option>
          </select>
           
        </div>


          <div class="form-group">
            <label for="remarks">Remarks</label>
            <textarea required class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div> 

        </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key3'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save Remarks</button>
          
        </div>
      </div>
    </div>
  </div>
</form> 


<form action="{{ action('Admin\AdminDashboardController@removeCouponDiscount') }}" method="post">
  {{ csrf_field()  }} 
 <div class="modal" id='widget-rem-coupon' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title">Discount Coupon Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body-content" style='padding: 10px;'>  
         
   <p>You are about to remove discount coupon from this order. Are you sure?</p> 
     
        <input type="hidden" id="key" name="key" >
<div class="clear"></div>
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span> 
        <button type="submit" class="btn btn-success" name="btnsave"  value='save'>Remove</button>
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Cancel</button> 
      </div>
    </div> 
  </div>
</div>
</form>


<div class="modal fade" id="modalConfDelAgent" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Admin\AdminDashboardController@removeAgentFromNormalOrder') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to remove agent from this delivery task?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key2'/> 
        <button type="submit" name="btnRemoveAgent" value="save" class="btn btn-primary btn-sm">Yes</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
 </form>
</div>

 

<form action="{{ action('Admin\AdminDashboardController@updateNormalOrderStatus') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalstatus" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Order Status Update</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             

        
    <div class="form-row"> 
      <div class="form-group col-md-6">
        <label for="status">Order Status</label>
        <select name="status" class="form-control" id="status">
            <option value='new'>Renew</option>
            <option value='delivered'>Delivered</option>
            <option value='returned'>Returned</option> 
            <option value='cancelled'>Cancelled</option> 
        </select>
      </div>
    </div>
    
    </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key5'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save Status</button>
          
        </div>
      </div>
    </div>
  </div>
</form> 




<form action="{{ action('Admin\AdminDashboardController@updateNormalOrderPrice') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalpriceupdate" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Price Correction</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             
       <div class="form-group">
          <label for="price">Update Price:</label> 
            <input type='number' step=".01" name="price" class="form-control" id="price"  />
         
       </div>


       <div class="form-group">
            <label for="remarks">Reason for price update:</label>
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div> 


 
    </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key6'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Update</button>
          
        </div>
      </div>
    </div>
  </div>
</form>
 


<form action="{{ action('Admin\AdminDashboardController@updateRefundAmount') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalrefundupdate" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Refund Amount Entry</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             
       <div class="form-group">
          <label for="price">Amount to refund:</label> 
            <input type='number' step=".01" name="refund" class="form-control" id="refund"  />
         
       </div>


       <div class="form-group">
            <label for="remarks">Reason for refund:</label>
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div> 


 
    </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key7'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
           <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Update</button>
          
        </div>
      </div>
    </div>
  </div>
</form>



<div class="modal hide fade modal_processing" id='modal_processing'   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Processing Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"> 
        <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer"> 
        <button type="button" class="btn btn-secondary btn_action" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


<div class="modal modalimgpreview" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered ">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Enlarge Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <img src="https://cdn.booktou.in/assets/image/no-image.jpg" id='imgexpand' class="img-fluid" />
        <br/>
          <div id='productdesc'></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


<form action="{{ action('Admin\AdminDashboardController@removeItemFromOrderBasket') }}" method='post'>
  {{ csrf_field() }}
<div class="modal confimdelitem" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Item Deletion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class='alert alert-danger'>
        This step is also non-recoverable. Please confirm your action.
        </p>
        <p>Item deleted will cause order price to change. Do this change only after confirming with customer. </p>



         <div class="form-group">
    <label for="actremark">Remark for action:</label>
    <textarea class="form-control" name="remarks" id="actremark" rows="3"></textarea>
  </div>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='orderno' id='key8'/> 
        <input type='hidden' name='itemno' id='key9'/> 
        <button type="submit" name='submit' value='confirm' class="btn btn-danger">Remove Selected Item</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> 
      </div>
    </div>
  </div>
</div>
</form>



<form action="{{ action('Admin\AdminDashboardController@routeOrderToFranchise') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalchangeToFranchise" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Migrate Normal Order</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             
        
    <div class="form-row"> 
      <div class="form-group col-md-6">
        <label for="frno">Select Franchise </label>
        <select name="frno" class="form-control" id="frno">
            @foreach($data['franchise'] as $fran)
            <option value='{{$fran->frno}}'>{{$fran->zone}}</option>
             @endforeach 
        </select> 
        
      </div>

      <div class="form-group col-md-12">
        <label for="remarks">Migration Remarks</label>
        <textarea name="remarks" class="form-control" id="remarks" rows='3' ></textarea> 
      </div>

    </div>
 
    </div>
        <div class="modal-footer">
          <input type="hidden" value="{{$order_info->id}}" name="orderno"> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Route Order</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>  
        </div>
      </div>
    </div>
  </div>
</form> 

  
<form  method='post' action="{{ action('Admin\AdminDashboardController@updateOrderSourceZone')}}">
  {{ csrf_field() }}
  <div class="modal modalUpdateOrderZone" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Update Original Business Source Zone</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
              
    <div class="form-row"> 
      <div class="form-group col-md-8">
        <label for="frno">Select Business Registration Zone</label>
        <select name="frno" class="form-control" id="frno"> 
            @foreach($data['franchise'] as $fran)
              <option value='{{$fran->frno}}'>{{$fran->zone}}</option>
            @endforeach 
        </select>  
      </div>

      <div class="form-group col-md-12">
        <label for="remarks">Order Zone Change Remarks (if any)</label>
        <textarea name="remarks" class="form-control" id="remarks" rows='3' ></textarea> 
      </div> 
    </div> 
    </div>
        <div class="modal-footer">
          <input type="hidden" value="{{$order_info->id}}" name="orderno"> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>  
        </div>
      </div>
    </div>
  </div>
</form> 
 



@endsection


@section('script')
 

<script>
 
$(document).on("click", ".showconfirmdel", function()
{  
  $("#key8").val($(this).attr("data-oid"));  
  $("#key9").val($(this).attr("data-itemno"));  
  $(".confimdelitem").modal("show");
}); 
 



$(document).on("click", ".showpreview", function()
{
  var itemdesc = $(this).attr("data-item");
  $("#productdesc").html( "<p class='alert alert-primary mt-3'>" + itemdesc + "</p>");
  $("#imgexpand").attr("src", $(this).attr("data-src"));  
  $(".modalimgpreview").modal("show");
}); 



$(document).on("click", ".showpriceupdate", function()
{
  $("#key6").val($(this).attr("data-key"));  
  $(".modalpriceupdate").modal("show");
}); 



$(document).on("click", ".showrefund", function()
{
  $("#key7").val($(this).attr("data-key")); 
  $(".modalrefundupdate").modal("show");

}); 



$(document).on("click", ".showchangestatus", function()
{
  $("#key5").val($(this).attr("data-key"));
  $(".modalstatus").modal("show")

}); 


$(document).on("click", ".showremmodal", function()
{
  $("#key3").val($(this).attr("data-key"));
  $(".modalrem").modal("show")

}); 


  $(document).on("click", ".btnRemoveAgent", function()
{
  $("#key2").val($(this).attr("data-key"));
  $("#modalConfDelAgent").modal("show")

});


 
$(document).on("click", ".showmodal", function()
{
  var widget = $(this).attr("data-widget");
  var key = $(this).attr("data-key");
   $("#key").val(key);
  $("#widget-" + widget ).modal("show") ;

});





$(function() {
    $('.calendar').pignoseCalendar();
});





$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ;  

    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');
 
    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
       // alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btnchangetofranchise", function()
{
  $(".modalchangeToFranchise").modal("show");

});


$(document).on("click", ".btnchangezone", function()
{
    $(".modalUpdateOrderZone").modal("show"); 
});



</script>

<style>
th 
{
  border-bottom: solid 1px #adb1b1 !important;
  color: #343434 !important;
}
td  
{
  border-bottom: dashed 1px #adb1b1 !important;
  padding-top: 5px;
  padding-bottom: 5px;
}

td img 
{
  border-radius: 50px;
}
</style>

@endsection

   
