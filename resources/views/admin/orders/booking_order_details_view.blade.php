@extends('layouts.admin_theme_02')
@section('content')
 
  <!-- <?php 

    //$order_info = $data['order_info'];
    //$order_items  = $data['order_items'];
    //$customer = $data['customer'];
    //$agent = $data['agent_info'];  
 
    //$order_no =$order_info->id ;


  ?>  -->

<?php 

    $order_info = $data['order_info'];
    $customer = $data['customer'];
    $order_type = $data['order_type'];  
    if($order_type=='booking')
     
    {
      $booking = $data['booking'];
      $staff_details = $data['staff'];
      $venue = $data['venue'];
    }

    $order_no =$order_info->id ;


  ?> 

 
<div class="row"> 
    <div class="col-md-8 grid-margin"> 
     
    <div class="card  ">
    <div class="card-body">
      <div class="card-title"> 
       <div class="title">Order Details</div>
     </div> 
     <hr/>

<h5 class="card-category">Order # {{ $order_info->id  }} 

 
 
                @switch($order_info->book_status)
                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("engaged")
                        <span class='badge badge-info'>Engaged</span>
                      @break
 
                      @case("in_queue")
                        <span class='badge badge-info'>In queue</span>
                      @break

                       @case("cancel_by_owner")
                        <span class='badge badge-danger'>Cancel by Owner</span>
                      @break

                       @case("cancel_by_client")
                        <span class='badge badge-danger'>Cancel by Client</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("no_show")
                        <span class='badge badge-warning'>No Show</span>
                      @break
                      @endswitch



 
                      <span> Order Date: {{ date('d-m-Y H:i:s', strtotime( $order_info->book_date)) }}</span> 

</h5> 
      


 

<table class="table table-bordered">
                    <thead class=" text-primary">
                        <th>Service Type</th>
                        <th>Amount</th> 
                        <th>Service fee</th>
                        <th class='text-right'>Total</th> 
                    </thead>
                    <tbody>
                      <?php $i = 0 ;

                      $sub_total = 0;
                      $total = 0;
                      ?>

               @foreach($booking as $book)
               <?php 
               $total = $book->pricing + $book->service_fee;
               $sub_total += $book->pricing + $book->service_fee;
               ?>
               <tr>
                <td>{{$book->service_name}}</td>
                <td>{{$book->pricing}}</td>
                <td>{{$book->service_fee}}</td>
                <td class='text-right'>{{$total}} </td>
              </tr>
               @endforeach

             </tbody>
             <tfoot>
     <tr>
       
       <td style="border: none"></td>
       <td style="border: none"></td>
       <td><b>Total Amount :</b></td>
       <td class="text-right">{{ number_format( $sub_total , 2, ".", "" )   }}</td>

     </tr>
   </tfoot>

  

      
                 

     
 </table>

 </div>
                      

     <div class="card-footer">
                       <a target='_blank' href="{{  URL::to('/services/print-receipt' )}}?o={{$order_no}}" class='btn btn-secondary btn-sm' name='btn_update'>Generate eBill</a>
    </div>


                </div>
          

 </div>



<div class="col-md-4">  

  @if($order_type=='booking')
<div class="card mb-3">
<div class="card-body">
    <h5 class="card-title"><span class="badge badge-warning">Staff Details for Order# {{$order_info->id}}</span></h5>

  </div>

@foreach($staff_details as $staff)
  <div class="row g-0">
    <div class="col-md-12">
       
      <div class="card">
      <div class="card-body">
      <ul class="list-group list-group-flush">

      <div class="info-block block-info clearfix">
                    <h4>Name: {{$staff->fullname}}</h4>
                    <p>Email: {{$staff->email}}<br>
                    </p>
                    
      </div>




      </ul>
      </div>
      </div>
    </div>
   
  </div>

@endforeach
</div>
@foreach($venue as $shop)
<div class="card">

@if($shop->preferred_time==null)

  <div class="card-body">
    <h5> <span class="badge badge-danger"> Venue:</span>
</h5>
    <h5 class="card-title">{{$shop->name}}:</h5>
    <h6 class="card-subtitle mb-2 text-muted">
     Shop No: <span class="badge badge-secondary">{{$shop->shop_number}}</span></h6>
    <p class="card-text">{{$shop->locality}},
      <br>
      {{$shop->city}} <br>
      <span>{{$shop->phone_pri}}<i class="fa fa-phone"></i></span> </p>
    
  </div>

@else
<div class="card-body ">
    <h5 class="card-title">{{$Address}}:</h5>
<p>:
{{$customer->locality}}<br/>
{{$customer->landmark}}<br/>
{{$customer->city}}<br/>
{{$customer->state}} - {{ $customer->pin_code}} <br/>
<i class='fa fa-phone'></i> {{$customer->phone}}</p>
  </div>

@endif

</div>

@endforeach



  @endif





<div class="board mt-1">

<div class="card ">
<div class="card-body">  
<div class="card-title">  
<div class="title"><span class="badge badge-primary">Customer Details</span></div>
</div> 
@if($customer=='')
 
@else

<h3 class="title">
  {{$customer->fullname}}
</h3>
<p>Address:<br/>
{{$customer->locality}}<br/>
{{$customer->landmark}}<br/>
{{$customer->city}}<br/>
{{$customer->state}} - {{ $customer->pin_code}} <br/>
<i class='fa fa-phone'></i> {{$customer->phone}}</p>  
@endif

                  
                  
            </div>

        
          </div>  

       </div> 
     </div>
 

</div>

  
@endsection


@section("script")

<script>

  $(document).on("click", ".btnModal001", function()
{
    var id = $(this).attr("data-id");
     $("#order_no").val( id ); 

    $("#dialogModal001").modal("show"); 

})

$(document).on("click", ".btnModal002", function()
{
    var id = $(this).attr("data-ono");
     $("#ono").val( id ); 

    $("#dialogModal002").modal("show"); 

})

  
</script>

@endsection



   