@extends('layouts.admin_theme_02')
@section('content')
 <?php 

 $total_amount=0;
 
 $total_delivery_charge = 0;
 $total_merchant_payout = 0;
 $total_booktou_commission = 0;
 $total_misc_charge = 0;
 $total_due_amount=0;
 $total_pay = 0;
 ?>

<div class="row">
     <div class="col-md-12"> 
      <div class="card panel-default"> 
           <div class="card-header"> 
              <div class="row">
                <div class="col-8">
                <label>
                @if($data['monthname'])
                   <span class="badge badge-primary"> {{$data['monthname']}} </span>

                @endif
                Order List for 
                
                @if($data['name'])
                <span class="badge badge-success">{{  $data['name']}}</span> 

                @endif
                </label> 
                <br>
                <label><span class="badge badge-warning">bookTou Commission:</span> {{(int)$data['business']->commission}}%</label>
                
                </div>
                
                <div class="col-4 text-right"> 
                <a href="{{url('/export-to-excel')}}/{{ request()->get('bin')}}" class="btn btn-primary">Export to excel</a>     
                </div>


                <div class="col-md-12">
                       
                <table class="table table-responsive">
                  
                  <thead>
                    <tr>
                    <th>Order No</th>
                    <th>Type</th>
                    <th>Source</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Delivery Charge</th>
                    <th>Commission</th>
                    <th>Misc Charge</th>
                    <th>Due by merchant</th>
                    <th>Merchant Payout</th>
                    <th>Payment Mode</th>
                    <th>Payment Target</th>
                   </tr>
                  </thead>
               
                  <tbody>
                     
            @foreach($data['allresult'] as $all)
                        <?php  
                             
                              
                             $sub_total=0;
                             $commission_percentage=5;
                             $booktou_commission=0; 
                             $merchant_payout=0;
                             $misc_charge=0;
                             $to_pay=0;
                             $delivery_charge = 0;
                             $payment_type;
                             $payment_target;
                             $total_due = 0;
                              
                        ?>
                    <tr>
                      <td>{{$all->orderNo}}</td>
                      <td>
                        @if($all->type=="pnd")
                        <span class="badge badge-dark">{{$all->type}}</span>
                        @else
                        <span class="badge badge-warning">{{$all->type}}</span>
                        @endif
                      </td>


                      <td>
                          @if($all->source=="business")

                                <span class="badge badge-primary">{{$all->source}} </span>
                          @elseif($all->source=="customer")
                                <span class="badge badge-danger">{{$all->source}} </span>
                          @elseif($all->source=="booktou")
                          <span class="badge badge-success">{{$all->source}} </span>
                          @endif 
                      </td>


                      <td>
                        @if($all->book_status=="delivered")
                            <span class="badge badge-success"> {{$all->book_status}}</span>
                        @elseif($all->book_status=="returned")
                            <span class="badge badge-danger"> {{$all->book_status}}</span>
                        @elseif($all->book_status=="new")
                            <span class="badge badge-primary"> {{$all->book_status}}</span>
                        @elseif($all->book_status=="canceled")
                            <span class="badge badge-warning"> {{$all->book_status}}</span>
                        @endif


                        


                      </td>
                      <td class="text-right">

                      
                          <!-- section for amount calculation -->
                      @if($all->book_status=="new" || $all->book_status=="canceled")

                       0

                       <?php $sub_total;  ?>

                       @else

                      {{$all->amount}}

                      <?php 

                              $sub_total = $all->amount;
                              

                              ?>

                              @endif

                              <!-- ends here -->
                            </td>
                      <td> 
                       <?php
                        
                        if ($all->book_status=="delivered"){

                          $delivery_charge = $all->deliverycharge; 
                            }elseif ($all->book_status=="new" || $all->book_status=="canceled") {
                              $delivery_charge = 0;
                            }elseif ($all->book_status=="returned" ) {
                              $delivery_charge = 0;
                            }

                        ?>

                        {{$delivery_charge}}
                      </td>
                      <td> 

                      <!-- Section for calculating commission -->
                      
                      <?php
                      if ($all->book_status=="delivered") {
                         
          if ($all->type=="normal" && $all->source=="customer" || $all->source=="booktou") {
                         
                        $amount = $all->amount;

                        $percentage = $commission_percentage / 100;

                        $final_commision = $percentage * $amount;

                        $booktou_commission = $final_commision;

                      }

                      }elseif ($all->book_status=="returned")  {

                        $booktou_commission = 0;
                      }else{
                        $booktou_commission = 0;
                      }
                      

                      ?>

                      {{$booktou_commission}}
                      <!-- calculation ends here -->

                      </td>

                      <td> 
                        <!-- Section for calculating misc charge -->
                          <?php  


                          if ($all->book_status=="returned" && $all->source=="business") 
                            {
                              $misc_charge = $all->deliverycharge;
                            }
                            /*elseif ($all->type=="normal" && $all->source=="customer" && $all->book_status=="delivered") {
                               
                               
                                  $misc_charge = $booktou_commission; 
                               
                             
                            }*/
                          ?>
                        <!-- calculation ends here -->

                        {{$misc_charge}}

                      </td>

                      <td>
                          <?php 
                          if ($all->book_status=="delivered") {
                            
                            if ($all->target_pay=="merchant") {
                                $total_due = $all->deliverycharge;
                            }else{
                                $total_due=0;
                            }
}
                            ?>

                            {{$total_due}}

                          


                      </td>

                      <td>

                      <?php 
                      if ($all->book_status=="delivered") {

                          if ($all->target_pay=="merchant")
                          {
                              $merchant_payout = 0;
                          }
                              //elseif ($all->type=="pnd" && $all->source=="customer" || $all->source=="booktou"){

                         //      $merchant_payout = $sub_total - $all->deliverycharge;

                         // }
                         else{

                          $merchant_payout = $sub_total - $booktou_commission;
                         }
                         


                      }else
                      {
                        $merchant_payout = 0;
                      }
                        
                      ?>

                      {{$merchant_payout}}

                      </td>
                      <td>


                        @if($all->ptype=="ONLINE"|| $all->ptype=="Pay online (UPI)")
                        <span class="badge badge-success">{{$all->ptype}}</span>
                        @elseif($all->ptype=="COD" || $all->ptype=="Cash on delivery" || $all->ptype=="CASH" || $all->ptype=="cash")
                        <span class="badge badge-warning">{{$all->ptype}}</span>
                        @endif
                        


                      </td>
                      <td><span class=""> 

                        <!--Merchant payout  -->
                      <?php 
                       
                          if ($all->book_status=="delivered") {

                            //payment target 
                            if ($all->type=="pnd") {

                             if ($all->target_pay=="merchant") {
                                $payment_target = "merchant";
                                echo "<span class=\"badge badge-warning\">".$payment_target."</span>";
                                }
                                elseif ($all->source=="business" && $all->ptype=="ONLINE" || $all->ptype=="Pay online (UPI)" || $all->ptype=="cash" || $all->ptype=="CASH"|| 
                                  $all->ptype=="Cash on delivery") {
                                 $payment_target = "bookTou";
                                 echo "<span class=\"badge badge-success\">".$payment_target."</span>";
                                }


                                         
                            }else{

                              if ($all->source=="customer" && $all->ptype=="ONLINE" || $all->ptype=="Pay online (UPI)"|| $all->ptype=="Cash" || $all->ptype=="Cash on delivery" 
                                || $all->ptype=="cash") {
                                $payment_target = "bookTou";
                                echo "<span class=\"badge badge-success\">".$payment_target."</span>";
                                }


                            }

                            //payment target ends here

                        }       


                            
                         
                          
                            

                         

                       ?>

                       <!-- ends here -->

                      
                      </span></td>
                      
                    </tr>

                    <?php 

                    $total_amount+= $sub_total; 
                    $total_delivery_charge+= $delivery_charge; 
                    $total_merchant_payout+= $merchant_payout;
                    $total_booktou_commission+=$booktou_commission;
                    $total_misc_charge += $misc_charge;
                    $total_due_amount += $total_due;

                    ?>
                    @endforeach

                    
<tr>
<td> </td>
<td> </td>
<td></td>
<td class="text-right">Total:</td>
<td class="text-right"> {{$total_amount}}</td>
<td> {{$total_delivery_charge}} </td>
<td> {{$total_booktou_commission}}</td>
<td> {{$total_misc_charge}}</td>
<td>{{$total_due_amount}} <span class="badge badge-primary"><small>due by merchant</small> </span></td>
<td>{{$total_merchant_payout}}</td>
<td> </td>
<td> </td>
</tr>


<!-- section for final amount-->

<tr>
<td> </td>
<td> </td>
<td></td><td> </td>
<td></td>
<td> </td>
<td> </td>
<td> </td>
<td class="text-right"><span class=" badge badge-danger">To Pay:</span></td>
<td>
<?php

    $total_pay = $total_merchant_payout -$total_booktou_commission - $total_misc_charge - $total_due_amount;

?>
{{$total_pay}} 
</td>
<td> </td>
<td> </td>
</tr>


                  </tbody>



                </table>
                  
                     
                </div> 
              </div>
            </div>
        </div>
    </div>
</div>


@endsection


@section("script")


@endsection 