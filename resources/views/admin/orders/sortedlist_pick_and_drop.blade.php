<div class="row">
     <div class="col-md-12">  
       @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif
    </div>
  </div>
    <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');   
                ?>
 
   
<div class="p-2" style='background-color: #fff !important; margin-bottom: 20px;'>
<div class="row">

 <div class='col-sm-12 col-md-4 text-right'> 
   
      <ul class="list-group">  
        <li class="list-group-item d-flex justify-content-between align-items-center">  
        @if($item->orderType=="assist")
        <span class="display-4">{{ strtoupper( $item->orderType ) }} Order #</span> 
        <a href="{{ URL::to('/admin/customer-care/assist-order/view-details/') }}/{{$item->id}}" target='_blank'>
        <span class="badge badge-primary badge-pill display-5">{{$item->id}}</span></a>  
        
        @else
        <span class="display-4">{{ strtoupper( $item->orderType ) }} Order #</span> 
        <a href="{{ URL::to('/admin/customer-care/pnd-order/view-details/') }}/{{$item->id}}" target='_blank'>
        <span class="badge badge-primary badge-pill display-5">{{$item->id}}</span></a> 
        
        @endif 
        </li> 

        <li class="list-group-item d-flex justify-content-between align-items-center"
        <?php 
              if( $interval->format('%H') >=  1  ) 
                echo 'style="background-color:#ff6e6e;color:#fff;"' ;
              else 
                  if( $interval->format('%H') < 1 )
                    echo 'style="background-color: #67cafa;color:#fff"' ;
                       
                  ?>
                  >
           Ordered On
          <span class="badge badge-primary badge-pill">{{ date('d-m-Y H:i:s', strtotime( $item->book_date )) }}</span>
      </li>


      <li class="list-group-item d-flex justify-content-between align-items-center">
           Delivery Date
          <span class="badge badge-primary badge-pill">{{ date('d-m-Y', strtotime( $item->service_date )) }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
          bookTou Fee
          <span class="badge badge-primary badge-pill">{{number_format( $item->service_fee,2) }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Item Cost
          <span class="badge badge-primary badge-pill">{{ number_format($item->total_amount,2)  }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Packaging charges (if any)
          <span class="badge badge-primary badge-pill">{{ number_format($item->packaging_cost,2)  }}</span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
           Status
          <span class="badge badge-primary badge-pill">
            @switch($item->book_status)
              @case("new")
                New
              @break
              @case("confirmed")
                Confirmed
              @break
                @case("order_packed")
                Order Packed
                      @break

                      @case("package_picked_up")
                      Package Picked Up
                      @break

                       @case("pickup_did_not_come")
                       Pickup Didn't Come
                      @break

                       @case("in_route")
                       In Route
                      @break

                       @case("completed")
                       Completed
                      @break

                      @case("delivered")
                      Delivered
                      @break

                       @case("delivery_scheduled")
                       Delivery Scheduled
                      @break 
                      @endswitch
                    </span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Payment Type
          <span class="badge badge-info badge-pill">{{ $item->pay_mode  }}</span> to 
          @if( strcasecmp($item->payment_target,"business") == 0 || 
              strcasecmp($item->payment_target,"merchant") == 0   )
            <span class='badge badge-danger badge-pill'>Merchant</span>
          @else 
                <span class='badge badge-success badge-pill'>bookTou</span>
          @endif
      </li>
   
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Order Source 

            @if($item->source == "business")
             <span class="badge badge-success badge-pill">Merchant</span>
            @else  
              @foreach($data['staffs'] as $staff_item)
                 @if( $staff_item->user_id == $item->staff_user_id )
                  <span class="badge badge-info badge-pill">{{ $staff_item->fullname }}</span>
                  @break
                 @endif
              @endforeach
            @endif 
      </li>
    

    </ul>
 
</div>



  <div class='col-sm-12 col-md-4'>
    <div class="card">
     <div class="card-body">  
        <h3>Order Details</h3> 
        <hr/>
      <span>Time Elapse</span>  <span class="badge badge-primary badge-pill">{{ $order_age }}</span> 
      <a class='badge badge-info' target='_blank' href="{{ URL::to('/shopping/where-is-my-order') }}/{{ $item->tracking_session }}">Where is it?</a>
      <hr/> 
      <strong>Order Remarks</strong>
      <hr/>
      @if($item->pickup_details !=  "") 
        {{ $item->pickup_details }}
      @else 
        Not provided
      @endif  

  <hr/>
  @if($item->order_receipt != null) 
     <div class="media">
      <img src="{{  $item->order_receipt }}"  width='35' height='45' class="mr-3" alt="Receipt"> 
      <div class="media-body"> 
        <a data-src="{{  $item->order_receipt }}" class="btn btn-success btnViewRcpt" href="#">View Receipt</a>  
      </div>
    </div>  
  @endif
 

</div>

<div class="card-footer">  
  <button type='button' class="btn btn-primary btn-sm  btnedit"  
  data-type="{{$item->orderType}}"     
  data-requestby="{{$item->request_by}}"  
  data-receipt="{{$item->order_receipt}}"  
  data-ono="{{$item->id}}" 
  data-fee="{{$item->service_fee}}" 
  data-total="{{$item->total_amount}}" 
  data-date="{{ date('d-m-Y', strtotime( $item->service_date )) }}" 
  data-fullname="{{$item->fullname}}" 
  data-phone="{{$item->phone}}" 
  data-address="{{$item->address}}" 
  data-landmark="{{$item->landmark}}" 
  data-pay_mode="{{ $item->pay_mode}}" 
  data-paytarget="{{$item->payment_target}}"  
  data-remarks="{{ $item->cust_remarks }}"
  data-pickupdetails="{{ $item->pickup_details }}" 
  data-pfullname="{{$item->pickup_name}}" 
  data-pphone="{{$item->pickup_phone}}" 
  data-paddress="{{$item->pickup_address}}" 
  data-plandmark="{{$item->pickup_landmark}}" 
  data-packcost="{{$item->packaging_cost}}"   >Edit Order</button>
<button type='button' class="btn btn-info btn-sm showremmodal"  data-key="{{$item->id}}" >Add Remarks</button> 
 
  </div>

</div>
</div>

  
  <div class='col-sm-12 col-md-4'>
    <div class="card">
        <div class='p-3'>
            <h4>Pickup & Drop Locations</h4>  
            <hr/>
       
       @if($item->orderType=="assist")       
        <strong>Pickup From:</strong><br/>   
        {{$item->pickup_name}}</br>
        {{ $item->pickup_address }}<br/>
        {{ $item->pickup_landmark }}<br/>
        <i class='fa fa-phone'></i> {{ $item->pickup_phone }}
        <hr/> 
     
    <strong>Drop To:</strong><br/>
    {{$item->drop_name }}<br/>
    {{$item->drop_address}}<br/> 
    {{$item->drop_landmark}}<br/> 
    <i class='fa fa-phone'></i> {{ $item->drop_phone }}
   
    @else
          
          <strong>Pickup From:</strong><br/>  
          <span scope="col">Business/Seller</span><br/>
          {{$item->name}}</br>
          {{ $item->pickup_address }} 
          <small><br/>
          <i class='fa fa-phone'></i> {{ $item->primaryPhone }}</small>  
          <hr/>
  
       
      <strong>Drop To:</strong><br/>
      {{$item->fullname }}<br/>
      {{$item->drop_address}}<br/> 
      {{$item->drop_landmark}}<br/> 
       <small><i class='fa fa-phone'></i> {{ $item->phone }}</small> 
     
    @endif
  
  </div>

    @foreach($data['banned_list'] as $banentry) 
     @if($banentry->phone == $item->phone && $banentry->flag != "none" ) 
       <div class='p-3'>
          <div  class='alert alert-info mt-2'>
            Cutomer is <span class='badge badge-danger'>{{ $banentry->flag }}</span>
          </div>
      </div>
      @break
    @endif
  @endforeach

  @if( $item->book_status == "new"   )  
   @foreach($data['requests_to_process'] as $reqitem) 
      @if($item->id == $reqitem->order_no )
      <div class='card '>
        <div class="card-body">
          <h5 class="card-title">Agent Requesting to handle order</h5>
            <span class='badge badge-primary'>{{ $reqitem->fullname }}</span> 
              <br/>
            <button class="btn btn-success btn-sm btnconfselfassign" 
            data-aid="{{$reqitem->id}}"  data-oid="{{$item->id}}" data-action="allow"><i class='fa fa-tick'></i> Allow</button>  
            <button class="btn btn-danger btn-sm btnconfselfassign" 
            data-aid="{{$reqitem->id}}" 
            data-oid="{{$item->id}}"  data-action="deny"><i class='fa fa-cross'></i> Deny</button> 
          </div>
      </div>
        @break
      @endif 
   @endforeach 
 @endif



 @if( $item->book_status == "new" ||  $item->book_status == "confirmed" ||   $item->book_status == 'order_packed' ) 
      <hr/>
  <div class='p-3'>
      <strong>Agent Assigned:</strong><br/>
      <div class="input-group mb-3"> 
  <select   class='form-control  ' id="aid_{{$item->id }}" name='agents[]' >
                              @foreach($data['all_agents'] as $aitem) 
                                 @if($aitem->isFree == "yes" )
                                  <option value='{{ $aitem->id }}' data-img="{{ $aitem->profile_photo }}"  >{{ $aitem->nameWithTask }}</option>
                                @endif 
                             @endforeach 
                  </select>
 <div class="input-group-append">
  <button class='btn btn-secondary btn-xs showTaskList' data-cid="aid_{{$item->id }}"  ><i class='fa fa-eye'></i></button>
   <button class="btn btn-primary btn-assign" data-oid="{{$item->id}}" >Assign</button>
  </div>
</div>
</div>
 @else
   <div class='p-3'>
      <strong>Agent Assigned:</strong><br/>
          @foreach($data['delivery_orders_list'] as $ditem) 
              @if( $item->id  == $ditem->order_no )
                <p class='text-left'>{{ $ditem->fullname  }} <br/> 
                                 @if(  $ditem->agent_ready == "yes")
                                 <span class='badge badge-success white'>TASK ACCEPTED</span>  
                                  @else 
                                  <span class='badge badge-danger white'>NO ACTION</span>
                                 @endif
                                  </p>
                @break;
                @endif  
            @endforeach 
          <button class='btn btn-danger btn-sm btnRemoveAgent' data-key='{{ $item->id }}'>Remove Agent</button>
    </div>    
 @endif 

 

    </div>
  </div> 
</div> <!-- end of row -->

</div>

<?php
  $i++; 
}
?>