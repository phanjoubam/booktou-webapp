@extends('layouts.admin_theme_02')
@section('content')
 
  <div class="row">
     <div class="col-md-12">

    <div class='card'> 
      <div class='card-body'>
        <div class="row">
                <div class="col-md-6">
                  Order Feedbacks and Remarks
                </div>
                <div class="col-md-3">

                 <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@remarksAndFeedback') }}"> 
                        {{ csrf_field() }}
                         <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Order No:</label>  
                         <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2'   name='filter_orderno' /> 

                            <button type="submit" class="btn btn-primary btn-sm my-1" value='search' name='btn_search'>Search</button>
                      </form>


                </div>
                <div class="col-md-3 text-right">
                     <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@remarksAndFeedback') }}"> 
                        {{ csrf_field() }}
                         <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

                         <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar'  
                         value="{{ date('d-m-Y', strtotime($data['last_keyword_date']))  }}" name='filter_date' /> 

                            <button type="submit" class="btn btn-primary btn-sm my-1" value='search' name='btn_search'>Search</button>
                      </form>
                </div> 
 </div> 
                </div> 
           </div>
     </div>
 </div>

   <div class="row">
     <div class="col-md-12">    
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');  

                ?>

                 <div class='card mt-2'> 
                  <div class='card-body'> 
                  <div class='row'>
                    <div class='col-md-2' 
                   <?php 
                    if( $interval->format('%H') >=  1  ) 
                      echo 'style="background-color:#ff6e6e;color:#fff; width: 200px"' ;
                    else 
                       if( $interval->format('%H') < 1 )
                        echo 'style="background-color: #67cafa;color:#fff; width: 200px"' ;
                   
                    ?> >
                    <div style='padding:10px'>
                    <strong>Order #</strong>
                    {{$item->id}}<br/>Customer: {{$item->fullname}}<br/>
                    @foreach($data['all_businesses'] as $business)
                        @if($business->id == $item->bin)
                          Merchant: {{  $business->name  }}
                          @break
                        @endif
                      @endforeach 
                </div>  
                  </div> 
                <div class='col-md-10'>
                <div style="display:inline-block;width:100%;overflow-y:auto;">
                  <ul class="timeline timeline-horizontal"> 
                    @foreach ($data['all_remarks'] as $rem)
                      @if($rem->order_no  == $item->id  ) 
                         
                              <li class="timeline-item">
                                <div class="timeline-badge primary"><i class="fa fa-bookmark"></i></div>
                                <div class="timeline-panel">
                                  <div class="timeline-heading">
                                    <h4 class="timeline-title">{{ $rem->rem_type}} by <span class='badge badge-primary'>{{ $rem->fullname}}</span></h4>
                                    <p><small class="text-muted">
                                      <i class="fa fa-clock-o "></i> {{ date( 'd-m-Y H:i a',  strtotime($rem->created_at)) }}</small>
                                    </p>
                                  </div>
                                  <div class="timeline-body">
                                    {{ $rem->remarks}} 
                                  </div>
                                </div>
                              </li>
                          
                    @endif
                  @endforeach
                    </ul>
                </div> 
                </div>  
                  
        
          </div>  
        </div>  </div> 
                         <?php
                          $i++; 
                       }

              ?> 
      <div class='card mt-2'>
      <div class='card-body'>    
          {{ $data['results']->links() }}
</div>
</div>
   </div>
 </div>
 
   

  
 
@endsection

@section("script")

<script>

$(document).on("click", ".btncancel", function()
{

    $("#orderno").val($(this).attr("data-ono"));

      $(".modal_cancel").modal("show")

});


 

$(document).on("click", ".btnchangedelivery", function()
{
  $("#orderno2").val($(this).attr("data-ono"));
  $(".changedelivery").modal("show"); 
});




$(document).on("click", ".btnconfim", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['reason'] =  $("#tareason").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/order/cancel" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_cancel").modal("hide") ;

         $("#tr" + $("#orderno").val( )).remove();
      },
      error: function( ) 
      {
       // alert(  'Something went wrong, please try again'); 
      } 

    });

       

});

  


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
             
        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 

    
    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');

  

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
        //alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id");

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsaveconfirmation").attr("data-id",  oid);
      $("#btnsaveconfirmation").attr("data-conf", confirmation ); 
      $("#confirmOrder").modal("show");   
   }  

})

$(document).on("change", "#closeconfmodal", function()
{

  
})
 

$('body').delegate('#btnsaveconfirmation','click',function(){
  
   var confirmation = $(this).attr("data-conf"); 
   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");


   $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/update-customer-confirmation" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.browser_refresh == "yes")
        {
          $(".loading_span").html(" ");
          location.reload(); 
        }    
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        //alert(  'Something went wrong, please try again'); 
      } 

    });
  
})

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 


<style type='text/css'>
.timeline,
.timeline-horizontal {
  list-style: none;
  padding: 10px;
  position: relative;
  max-width: 250px;
}
.timeline:before {
  top: 40px;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: #eeeeee;
  left: 50%;
  margin-left: -1.5px;
}
.timeline .timeline-item {
  margin-bottom: 20px;
  position: relative;
}
.timeline .timeline-item:before,
.timeline .timeline-item:after {
  content: "";
  display: table;
}
.timeline .timeline-item:after {
  clear: both;
}
.timeline .timeline-item .timeline-badge {
  color: #fff;
  width: 54px;
  height: 54px;
  line-height: 52px;
  font-size: 22px;
  text-align: center;
  position: absolute;
  top: 18px;
  left: 50%;
  margin-left: -25px;
  background-color: #7c7c7c;
  border: 3px solid #ffffff;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
}
.timeline .timeline-item .timeline-badge i,
.timeline .timeline-item .timeline-badge .fa,
.timeline .timeline-item .timeline-badge .glyphicon {
  top: 2px;
  left: 0px;
}
.timeline .timeline-item .timeline-badge.primary {
  background-color: #1f9eba;
}
.timeline .timeline-item .timeline-badge.info {
  background-color: #5bc0de;
}
.timeline .timeline-item .timeline-badge.success {
  background-color: #59ba1f;
}
.timeline .timeline-item .timeline-badge.warning {
  background-color: #d1bd10;
}
.timeline .timeline-item .timeline-badge.danger {
  background-color: #ba1f1f;
}
.timeline .timeline-item .timeline-panel {
  position: relative;
  width: 46%;
  float: left;
  right: 16px;
  border: 1px solid #c0c0c0;
  background: #ffffff;
  border-radius: 2px;
  padding: 20px;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
.timeline .timeline-item .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -16px;
  display: inline-block;
  border-top: 16px solid transparent;
  border-left: 16px solid #c0c0c0;
  border-right: 0 solid #c0c0c0;
  border-bottom: 16px solid transparent;
  content: " ";
}
.timeline .timeline-item .timeline-panel .timeline-title {
  margin-top: 0;
  color: inherit;
}
.timeline .timeline-item .timeline-panel .timeline-body > p,
.timeline .timeline-item .timeline-panel .timeline-body > ul {
  margin-bottom: 0;
}
.timeline .timeline-item .timeline-panel .timeline-body > p + p {
  margin-top: 5px;
}
.timeline .timeline-item:last-child:nth-child(even) {
  float: right;
}
.timeline .timeline-item:nth-child(even) .timeline-panel {
  float: right;
  left: 16px;
}
.timeline .timeline-item:nth-child(even) .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}
.timeline-horizontal {
  list-style: none;
  position: relative;
  padding: 20px 0px 20px 0px;
  display: inline-block;
}
.timeline-horizontal:before {
  height: 3px;
  top: auto;
  bottom: 26px;
  left: 56px;
  right: 0;
  width: 100%;
  margin-bottom: 20px;
}
.timeline-horizontal .timeline-item {
  display: table-cell;
  height: 280px;
  width: 20%;
  min-width: 320px;
  float: none !important;
  padding-left: 0px;
  padding-right: 20px;
  margin: 0 auto;
  vertical-align: bottom;
}
.timeline-horizontal .timeline-item .timeline-panel {
  top: auto;
  bottom: 64px;
  display: inline-block;
  float: none !important;
  left: 0 !important;
  right: 0 !important;
  width: 100%;
  margin-bottom: 20px;
}
.timeline-horizontal .timeline-item .timeline-panel:before {
  top: auto;
  bottom: -16px;
  left: 28px !important;
  right: auto;
  border-right: 16px solid transparent !important;
  border-top: 16px solid #c0c0c0 !important;
  border-bottom: 0 solid #c0c0c0 !important;
  border-left: 16px solid transparent !important;
}
.timeline-horizontal .timeline-item:before,
.timeline-horizontal .timeline-item:after {
  display: none;
}
.timeline-horizontal .timeline-item .timeline-badge {
  top: auto;
  bottom: 0px;
  left: 43px;
}
</style>
@endsection 