@extends('layouts.admin_theme_02')
@section('content')
 
 

   <div class="row">
     <div class="col-md-12">  
         @if (session('err_msg'))  
         <div class="alert alert-info">
          {{ session('err_msg') }}
          </div> 
        @endif
    </div>
  </div>

  <div class="row">
     <div class="col-md-12">  

      

      <?php
        $i=1;
        $date2  = new \DateTime( date('Y-m-d H:i:s') );
        foreach ($data['results'] as $item)
        {
          $date1 = new \DateTime( $item->book_date ); 
          $interval = $date1->diff($date2); 
          $order_age =  $interval->format('%a days %H hrs  %i mins');   

          $arr = explode(",", $item->address );
          $delivery_address = $arr[0]; 

          $arr = explode(".", $delivery_address );
          $delivery_address = $arr[0]; 
 

      ?>
 
          <div class="card card-contract">

            <div class="card-body">
              <div class="hori-timeline" dir="ltr">
                    <ul class="list-inline events">
                        <li class="list-inline-item event-list">
                          

                            <div class="px-4">
                              <a href="{{  URL::to('/admin/customer-care/pnd-order/view-details') }}/{{ $item->id }}" target='_blank'>
                                  <div class="event-date bg-soft-info text-white">{{ $item->id }}</div>
                              </a> 
                               <h5 class="font-size-16">Order No</h5>  
                            </div>
                        </li>
                        <li class="list-inline-item event-list">
                          

                            <div class="px-4">
                                <div class="event-date bg-soft-primary text-white">{{ $item->businessLocality }}</div>
                                <h5 class="font-size-16">{{$item->name}}</h5>
                               
                            </div>
                        </li> 
                        <li class="list-inline-item event-list">
                         
                                  <div class="pd">
                                      <div class="event-date bg-soft-success text-white">{{ $delivery_address  }}</div>
                                      
                                       <h5 class="font-size-16">{{ $item->fullname }}</h5>
                                  </div>
                        </li>

                         <li class="list-inline-item event-list">
                            <div class="px-4">
                                <div class="event-date bg-soft-warning text-warning">
                                <select   class='form-control  ' id="aid_{{$item->id }}" name='agents[]' >
                                    @foreach($data['all_agents'] as $aitem)  
                                      <option value='{{ $aitem->id }}'  >{{ $aitem->fullname }}</option> 
                                    @endforeach
                                </select>

                              </div>
                                <h5 class="font-size-16"><button class='btn btn-primary btn-primary btn-sm'>Check Routing Table</button></h5> 
                                
                            </div>
                        </li>

              </ul>
         </div>
       </div>
    </div>
 

 
<?php
  $i++; 
}
?>
</div>



  </div>

             </div>
              </div>
 </div>




              
            </div>
          </div>
        </div>  
  
 
 
 


@endsection

@section("script")

<style>
.card .card-header {
    background: white;
    padding: 0.5rem 0.81rem;
    border-bottom: 1px solid #dee2e6;
}
  .hori-timeline .events {
    border-top: 3px solid #e9ecef;
}
.hori-timeline .events .event-list {
    display: block;
    position: relative;
    text-align: center;
    padding-top: 70px;
    margin-right: 0;
}
.hori-timeline .events .event-list:before {
    content: "";
    position: absolute;
    height: 36px;
    border-right: 2px dashed #dee2e6;
    top: 0;
}
.hori-timeline .events .event-list .event-date {
    position: absolute;
    top: 38px;
    left: 0;
    right: 0;
    width: 225px;
    margin: 0 auto;
    border-radius: 4px;
    padding: 2px 4px;
}
@media (min-width: 1140px) {
    .hori-timeline .events .event-list {
        display: inline-block;
        width: 24%;
        padding-top: 45px;
    }
    .hori-timeline .events .event-list .event-date {
        top: -12px;
    }
}
 
.card {
  padding-top: 20px;
    border: none;
    margin-bottom: 24px;
    -webkit-box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
    box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
}


.bg-soft-info {
    background-color: rgb(83 88 245)!important;  
} 

.bg-soft-primary {
    background-color: rgba(64,144,203,1)!important;
}
.bg-soft-success {
    background-color: rgba(71,189,154,1)!important;
}
.bg-soft-danger {
    background-color: rgba(231,76,94,1)!important;
}
.bg-soft-warning {
    background-color: rgba(249,213,112, 1)!important;
}
 

  </style>

<script>


$(document).on("click", ".showremmodal", function()
{
  $("#key3").val($(this).attr("data-key"));
  $(".modalrem").modal("show")

}); 
  

$(document).on("click", ".addOrder", function()
{
    $("#modalneworder").modal("show");

});


$(document).on("click", ".btncancel", function()
{

    $("#orderno").val($(this).attr("data-ono"));
    $(".modal_cancel").modal("show")

});




$(document).on("click", ".btnedit", function()
{
  $("#erono").val($(this).attr("data-ono"));  
  $("#receipt").attr("src", $(this).attr("data-receipt"));   
  $("#ecname").val( $(this).attr("data-fullname") );
  $("#ecphone").val( $(this).attr("data-phone") );  
  $("#eaddress").val( $(this).attr("data-address") );  
  $("#elandmark").val( $(this).attr("data-landmark") );   
  $("#esfee").val( $(this).attr("data-fee") ); 
  $("#etotal").val( $(this).attr("data-total") ); 
  $("#eservicedate").val( $(this).attr("data-date") );  
  $("#eremarks").val( $(this).attr("data-remarks") ); 

  if($(this).attr("data-pay_mode") == "ONLINE")
    $("#paymode2").prop("checked", true);
  else
    $("#paymode1").prop("checked", true);

  $("#modalcopyreceipt").modal("show") 
});




$(document).on("click", ".btnconfim", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['reason'] =  $("#tareason").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
 

    $.ajax({
      type: 'post',
      url: api + "/v2/web/customer-care/pnd-order/cancel" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_cancel").modal("hide") ;

         $("#tr" + $("#orderno").val( )).remove();
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });
    

});

  


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
             
        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 
    json['otype'] =  'pickup' ; 

    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');

  

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
       // alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id");

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsaveconfirmation").attr("data-id",  oid);
      $("#btnsaveconfirmation").attr("data-conf", confirmation ); 
      $("#confirmOrder").modal("show");   
   }  

})

 
  

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


 

$(document).on("click", ".btnRemoveAgent", function()
{
  $("#key2").val($(this).attr("data-key"));
  $("#modalConfDelAgent").modal("show")

});


 
$(document).on("click", ".btnViewRcpt", function()
{
    var img = $(this).attr("data-src");
    $("#imgreceipt").attr("src", img );
    $("#modalViewReceipt").modal("show");

});



$(document).on("click", ".btnconfselfassign", function()
{
    var oid = $(this).attr("data-oid");
    var aid = $(this).attr("data-aid");  
    var action = $(this).attr("data-action");

    $("#confsakey").val( oid);
    $("#confsaaid").val(   aid ); 

    if(action== "allow")
    {
      $(".modalinfo").html("<p>Request to self assign will be deleted. Are you sure, you want to take this action?</p>");
    }
    else 
    {
      $(".modalinfo").html("<p>Request to self assign will be granted. Are you sure, you want to take this action?</p>");
    }

    $("#modalconfselfassign").modal("show");

});




</script> 

@endsection 