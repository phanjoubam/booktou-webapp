@extends('layouts.admin_theme_02')
@section('content')

 
  <div class="row"> 
             

       @if (session('err_msg'))
    
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg') }}
      </div>
      </div>   
    @endif


    @foreach ($data['areas'] as $item) 
		  <div class="col-md-4"> 
        <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <h2>{{ $item->area_name}}</h2>
                </div>
            </div>
  <div class="card-body">   
    {{ $item->description}}
      
 </div> 

  <div class="card-footer">
    <a href="{{ URL::to('/admin/marketing/campaign-area/view-audiences') }}?key={{ $item->area_name}}" class='btn btn-primary btn-xs'>View Target Audience</a>
  </div>
  </div>
</div>

  	@endforeach
	 
		
	</div>
 </div> 
   
 
@endsection



@section("script")

<script>
 
  
</script>

@endsection

