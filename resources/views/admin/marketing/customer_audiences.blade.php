@extends('layouts.admin_theme_02')
@section('content')

 


   <div class="row">
     <div class="col-md-12"> 
      <div class="board">
     <div class="panel panel-primary">


           <div class="row"> 
               <div class="col-md-12"> 

                 <div class="panel-heading">
              <div class="card-title">
                  <div class="title">{{ $data['title'] }}</div>
                </div>
            </div>
                
                </div>
               
          </div> 
          <br/>

<div class="table-responsive">
    <table class="table">
                    <thead class="text-primary">  
                  <tr>  
                     <th scope="col">Customer ID</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Address</th> <th scope="col">Phone</th>
                    <th scope="col">Call Log</th> 
                    <th scope="col" class='text-center'>Action</th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                { 
                ?>
  
                  <tr >  
                    <td>
                    <strong>{{$item->id}} </strong>
                  
                   <td>
                   <a class="dropdown-item btn" href="{{  URL::to('/admin/customer/view-complete-profile/'.$item->id )}}">
                    <strong>{{$item->fullname}}</strong></a>
                  </td>
                  <td>
                   {{ $item->locality}}, {{ $item->landmark}}        
                  </td> 
                   <td>
                   {{ $item->phone}}
                  </td> 

                  <td>
                    @if($item->logCount > 0 ) 
                  <span class='badge badge-success'>Present</span>
                @else 
              <span class='badge badge-warning'>Absent</span>  
            @endif</td>
              
                   

                    <td class="text-center">
                      <div class="dropdown"> 
                        <a class="btn btn-primary btn-sm btn-icon-only text-light " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-cog "></i>
                        </a> 
                        <ul class="dropdown-menu dropdown-user pull-right"> 
                          
                          <li>
                            <a class="dropdown-item btn btntakelog" href="#" data-key1="{{$item->id}}" >Save Call Log</a></li>  
                        </ul>
                    </div>          
                </td>

              </tr>
              <?php
              $i++; 
            }
      ?>
    </tbody>
  </table> 
  {{ $data['results']->appends(request()->input())->links() }}  
</div>
            </div>

          </div>
        </div>  
   </div>
 
    



<!-- edit order -->
 
<div class="modal" id='mmodalcalllog' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Save Customer Call Log Summary</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 

        <div class='loading_span'></div>

        <div class='row'>
          <div class='col-md-12'> 

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="ctime">Call Date &amp; Time:</label>
      <input  type="datetime-local"    class="form-control" id="ctime" name='ctime' value="{{ date('Y-m-d H:i:s') }}">
    </div> 
  

  </div> 
  

 <div class="form-row">
 <div class="form-group col-md-12">
      <label for="eremarks">Call Log:</label>
      <textarea  class="form-control" id="eremarks" rows='4' name='remarks'></textarea>
    </div>  </div>
  

  </div>
         
        </div> 
 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="key1" name='key1'>
        <button type="button" id='btnsavelog' value='save' class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>


@endsection

@section("script")

<script>

 
 $(document).on("click", ".btntakelog", function()
{

    $(".loading_span").html(" "); 
    $("#eremarks").val(""); 
    $("#key1").val($(this).attr("data-key1"));    
    $("#mmodalcalllog").modal("show")

});
 



$('body').delegate('#btnsavelog','click',function()
{
 

  var key1 = $("#key1").val(); 
  var ctime = $("#ctime").val(); 
  var eremarks = $("#eremarks").val(); 

 
 
    var json = {}; 
    json['key'] = key1 ;
    json['logdate'] = ctime ; 
    json['logsummary'] = eremarks ; 


    $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $.ajax({
        type: 'post',
        url: api + "v3/web/marketing/customer-call-log" ,
        data: json,
        success: function(data)
        {
          data = $.parseJSON(data);     
          $("#mmodalcalllog").modal("hide")
          location.reload();
        },
        error: function( ) 
        {
          $(".loading_span").html(" ");
          alert(  'Something went wrong, please try again'); 
        } 

      });
  

})



</script> 

@endsection 