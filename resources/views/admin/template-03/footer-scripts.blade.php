<script src="{{   URL::to('/public/theme/three')   }}/js/jquery-3.4.1.min.js"></script>
<script src="{{   URL::to('/public/theme/three')   }}/js/popper.min.js"></script>
<script src="{{   URL::to('/public/theme/three')   }}/js/bootstrap.min.js"></script>
<script src="{{   URL::to('/public/theme/three')   }}/js/metisMenu.js"></script>
<script src="{{   URL::to('/public/theme/three')   }}/vendor/count_up/jquery.waypoints.min.js"></script>
<script src="{{   URL::to('/public/theme/three')   }}/vendor/chartlist/Chart.min.js"></script>

<script src="{{   URL::to('/public/assets/vendor/select2/js/select2.js')   }}"></script> 



<script 
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9KrtxONZFci3FxO0SPZJSejUXHpvgmHI&callback=initMap&libraries=&v=weekly" 
defer></script> 
<script src="{{ URL::to('/public/assets/vendor') }}/jexcel/jexcel.js"></script> 
<script src="{{ URL::to('/public/assets/vendor') }}/jsuites/dist/jsuites.js"></script> 
<script src="{{ URL::to('/public/assets') }}/vendor/pn/js/pignose.calendar.full.min.js" type="text/javascript"></script>  
<script src="{{   URL::to('/public/assets/vendor/jquery-ui/jquery-ui.js')   }}"></script>  

<script src="{{   URL::to('/public/theme/three')   }}/js/cm.js"></script>
<script src="{{ URL::to('/public/assets') }}/admin/js/core/acore.js"></script> 

<script>

	$.toastDefaults = {
	  position: 'bottom-right', 
	  dismissible: true, 
	  stackable: true, 
	  pauseDelayOnHover: true, 
	  style: {
	    toast: '',
	    info: '',
	    success: '',
	    warning: '',
	    error: '',
	  }
  
	};


  $(document).ready(  function()
  { 
  	setInterval(checkNewOrders, 10000);


  	<?php
  	if( isset($totalPndOrders) && $totalPndOrders >=150)
  	{
  		?>
  		$('div.main-panel-dashboard').fireworks();  
  		<?php 
  	}
  	?> 

  });

 

	function checkNewOrders()
	{
		 

		if (typeof $.cookie('lid') === 'undefined'){
		 	var lid  = 0;
		} else {
		   var lid = $.cookie("lid");
		} 
	 
	  
	    var json = {};
	    json['lastScanId'] =  lid;

	    $.ajax({
	      type: 'post',
	      url: api + "v3/web/orders/scan-new-orders" ,
	      data:   json ,
	      success: function(data)
	      {
	        data = $.parseJSON(data);
	        $.cookie('lid',  data.lastId ); 
	        if( data.lastId > lid )
	        {
	        	$.toast({
		  		position:'bottom-right', 
				title: 'bookTou Notification', 
				content: 'There is a new order coming in.',
				type: 'info',
				delay: 120000,
				dismissible: true,
				  img: {
				    src: '<?php echo URL::to("/public/assets/image/notification.png"); ?>',
				    class: 'rounded',
				    title: 'BTN',
				    alt: 'BTN'
				  }
			});	

	        }
	      }
	    });
	} 
</script>
