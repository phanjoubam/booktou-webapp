<nav class="sidebar vertical-scroll ps-container ps-theme-default ps-active-y" data-ps-id="a25a1205-2f1b-14fa-909d-f43a423c78d1">
    <div class="logo d-flex justify-content-between">
        <a href="https://demo.dashboardpack.com/sales-html/index.html"><img src="./_sales-html__files/logo.png" alt=""></a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu" class="metismenu">
        <li class="">
          <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="true">
          <div class="icon_menu">
              <img src="./_sales-html__files/dashboard.svg" alt="">
        </div>
            <span>Dashboard</span>
          </a>
          <ul class="mm-collapse">
            <li><a class="" href="https://demo.dashboardpack.com/sales-html/index.html">Marketing</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/index_2.html">Default</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/index_3.html">Dark Menu</a></li>
          </ul>
        </li>
        <li class="">
            <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
              <div class="icon_menu">
                  <img src="./_sales-html__files/2.svg" alt="">
              </div>
              <span>Apps</span>
            </a>
            <ul class="mm-collapse">
              <li><a href="https://demo.dashboardpack.com/sales-html/editor.html">editor</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/mail_box.html">Mail Box</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/chat.html">Chat</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/faq.html">FAQ</a></li>
            </ul>
          </li>
        <li class="">
          <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
            
            <div class="icon_menu">
                <img src="./_sales-html__files/3.svg" alt="">
            </div>
            <span>UI Kits</span>
          </a>
          <ul class="mm-collapse">
            <li><a href="https://demo.dashboardpack.com/sales-html/colors.html">colors</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/Alerts.html">Alerts</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/buttons.html">Buttons</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/modal.html">modal</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/dropdown.html">Droopdowns</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/Badges.html">Badges</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/Loading_Indicators.html">Loading Indicators</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/State_color.html">State color</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/typography.html">Typography</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/datepicker.html">Date Picker</a></li>
          </ul>
        </li>
        <li class="">
          <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
            
            <div class="icon_menu">
                <img src="./_sales-html__files/4.svg" alt="">
            </div>
            <span>forms</span>
          </a>
          <ul class="mm-collapse">
            <li><a href="https://demo.dashboardpack.com/sales-html/Basic_Elements.html">Basic Elements</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/Groups.html">Groups</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/Max_Length.html">Max Length</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/Layouts.html">Layouts</a></li>
          </ul>
        </li>
        <li class="">
            <a href="https://demo.dashboardpack.com/sales-html/Board.html" aria-expanded="false">
              <div class="icon_menu">
                  <img src="./_sales-html__files/5.svg" alt="">
              </div>
              <span>Board</span>
            </a>
        </li>
        <li class="">
            <a href="https://demo.dashboardpack.com/sales-html/invoice.html" aria-expanded="false">
              <div class="icon_menu">
                  <img src="./_sales-html__files/6.svg" alt="">
              </div>
              <span>Invoice</span>
            </a>
        </li>
        <li class="">
            <a href="https://demo.dashboardpack.com/sales-html/calender.html" aria-expanded="false">
              <div class="icon_menu">
                  <img src="./_sales-html__files/7.svg" alt="">
              </div>
              <span>Calander</span>
            </a>
        </li>
        
        <li class="">
            <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
              
              <div class="icon_menu">
                  <img src="./_sales-html__files/8.svg" alt="">
              </div>
              <span>Products</span>
            </a>
            <ul class="mm-collapse">
              <li><a href="https://demo.dashboardpack.com/sales-html/Products.html">Products</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/Product_Details.html">Product Details</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/Cart.html">Cart</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/Checkout.html">Checkout</a></li>
            </ul>
          </li>
        <li class="">
          <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
            <div class="icon_menu">
                <img src="./_sales-html__files/8.svg" alt="">
            </div>
            <span>Icons</span>
          </a>
          <ul class="mm-collapse">
            <li><a href="https://demo.dashboardpack.com/sales-html/Fontawesome_Icon.html">Fontawesome Icon</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/themefy_icon.html">themefy icon</a></li>
          </ul>
        </li>

        <li class="">
            <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
              <div class="icon_menu">
                  <img src="./_sales-html__files/9.svg" alt="">
              </div>
              <span>Animations</span>
            </a>
            <ul class="mm-collapse">
                <li><a href="https://demo.dashboardpack.com/sales-html/wow_animation.html">Animate</a></li>
                <li><a href="https://demo.dashboardpack.com/sales-html/Scroll_Reveal.html">Scroll Reveal</a></li>
                <li><a href="https://demo.dashboardpack.com/sales-html/tilt.html">Tilt Animation</a></li>
                
            </ul>
          </li>
          <li class="">
            <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
              <div class="icon_menu">
                  <img src="./_sales-html__files/10.svg" alt="">
              </div>
              <span>Components</span>
            </a>
            <ul class="mm-collapse">
              <li><a href="https://demo.dashboardpack.com/sales-html/accordion.html">Accordions</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/Scrollable.html">Scrollable</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/notification.html">Notifications</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/carousel.html">Carousel</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/Pagination.html">Pagination</a></li>
            </ul>
          </li>

          <li class="">
            <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
              <div class="icon_menu">
                  <img src="./_sales-html__files/11.svg" alt="">
              </div>
              <span>Table</span>
            </a>
            <ul class="mm-collapse">
                <li><a href="https://demo.dashboardpack.com/sales-html/data_table.html">Data Tables</a></li>
                <li><a href="https://demo.dashboardpack.com/sales-html/bootstrap_table.html">Bootstrap</a></li>
            </ul>
          </li>
          <li class="">
            <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
              <div class="icon_menu">
                  <img src="./_sales-html__files/12.svg" alt="">
              </div>
              <span>Cards</span>
            </a>
            <ul class="mm-collapse">
                <li><a href="https://demo.dashboardpack.com/sales-html/basic_card.html">Basic Card</a></li>
                <li><a href="https://demo.dashboardpack.com/sales-html/theme_card.html">Theme Card</a></li>
                <li><a href="https://demo.dashboardpack.com/sales-html/dargable_card.html">Draggable Card</a></li>
            </ul>
          </li>


        <li class="">
          <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
            <div class="icon_menu">
                <img src="./_sales-html__files/13.svg" alt="">
            </div>
            <span>Charts</span>
          </a>
          <ul class="mm-collapse">
            <li><a href="https://demo.dashboardpack.com/sales-html/chartjs.html">ChartJS</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/apex_chart.html">Apex Charts</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/chart_sparkline.html">Chart sparkline</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/am_chart.html">am-charts</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/nvd3_charts.html">nvd3 charts.</a></li>
          </ul>
        </li>
        

        <li class="">
          <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
            <div class="icon_menu">
                <img src="./_sales-html__files/14.svg" alt="">
            </div>
            <span>Widgets</span>
          </a>
          <ul class="mm-collapse">
            <li><a href="https://demo.dashboardpack.com/sales-html/chart_box_1.html">Chart Boxes 1</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/profilebox.html">Profile Box</a></li>
          </ul>
        </li>
        

        <li class="">
          <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
            <div class="icon_menu">
                <img src="./_sales-html__files/15.svg" alt="">
            </div>
            <span>Maps</span>
          </a>
          <ul class="mm-collapse">
            <li><a href="https://demo.dashboardpack.com/sales-html/mapjs.html">Maps JS</a></li>
            <li><a href="https://demo.dashboardpack.com/sales-html/vector_map.html">Vector Maps</a></li>
          </ul>
        </li>
        <li class="">
            <a class="has-arrow" href="https://demo.dashboardpack.com/sales-html/#" aria-expanded="false">
              
              <div class="icon_menu">
                  <img src="./_sales-html__files/16.svg" alt="">
              </div>
              <span>Pages</span>
            </a>
            <ul class="mm-collapse">
              <li><a href="https://demo.dashboardpack.com/sales-html/login.html">Login</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/resister.html">Register</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/error_400.html">Error 404</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/error_500.html">Error 500</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/forgot_pass.html">Forgot Password</a></li>
              <li><a href="https://demo.dashboardpack.com/sales-html/gallery.html">Gallery</a></li>
            </ul>
          </li>

      </ul>
<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 617px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 110px;"></div></div></nav>