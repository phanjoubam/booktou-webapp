<nav class="sidebar vertical-scroll  ps-container ps-theme-default ps-active-y">
  <div class="logo d-flex justify-content-between">
    <a href="{{  URL::to('/admin' )}}"><img src="/public/assets/image/logo_dark.png" alt=""></a>
    <div class="sidebar_close_icon d-lg-none">
      <i class="ti-close"></i>
    </div>
  </div>
  <ul id="sidebar_menu">

   <li class="">
    <a  href="{{  URL::to('/admin' )}}" aria-expanded="false">

      <span>Dashboard</span>
    </a>
  </li>


  <li class="">
    <a   class="has-arrow" href="#" aria-expanded="false">

      <span>Orders</span>
    </a>
    <ul  > 
      <li   >
        <a    href="{{  URL::to('/admin/customer-care/orders/view-all' )}}">
          Normal Order
        </a>
      </li> 

      <li>
        <a href="{{  URL::to('/admin/customer-care/pick-and-drop-orders/view-all' )}}">
          Pick-And-Drop
        </a>
      </li> 
      <li>
        <a   href="{{  URL::to('/admin/orders/assists/view-all' )}}">
         Assist
       </a>
     </li> 
     <li>
      <a  href="{{  URL::to('/admin/customer-care/callback-requests' )}}">
        Callback Requests
      </a>
    </li>
    <li>
      <a  href="{{  URL::to('/admin/customer-care/quotation-requests' )}}">
        Quotation Enquiry
      </a>
    </li> 
    <li>
      <a  href="{{  URL::to('/admin/normal-orders/stats' )}}"> Order Stats</a>
    </li>
    <li>
      <a  href="{{  URL::to('/admin/orders/remarks-and-feedback' )}}"> Remarks</a>
    </li>

    <li>
      <a  href="{{  URL::to('/admin/pick-and-drop-orders/prepare-allocation-table' )}}"> Route Discovery</a>
    </li>

    <li>
      <a  href="{{  URL::to('/admin/orders/route-with-tasks-table' )}}"> Route and Tasks Table</a>
    </li>

  </ul>
</li>
<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false">

    <span>Bookings</span>
  </a>
  <ul>
    <li> <a   href="{{  URL::to('/services/view-merchant-wise-booking' )}}">
      All Bookings
    </a>
  </li>
</ul>
</li>
<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false">  
    <span>Businesses</span>
  </a>
  <ul  > 

   <li  ><a  href="{{  URL::to('/admin/customer-care/business/view-all' )}}">
     View Businesses
   </a>
 </li>

 <li  ><a   href="{{  URL::to('/admin/customer-care/business/view-promotion-businesses' )}}">
  Premium Businesses
</a>
</li>
<li  ><a  href="{{  URL::to('/admin/customer-care/business/normal-orders/best-performers' )}}">
  Best Performers
</a>
</li>

<li  ><a   href="{{  URL::to('/admin/customer-care/business/pnd-orders/best-performers' )}}">
  Best PnD Performers
</a>
</li>

</ul>
</li>



<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false">

    <span>Franchise</span>
  </a>
  <ul>
    <li><a   href="{{  URL::to('/admin/view-all-franchise' )}}">
     View Franchise
   </a></li>
 </ul>
</li>
<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false"> 
    <span>Staffs</span>
  </a>
  <ul  > 
    <li><a  href="{{  URL::to('/admin/staffs/manage-attendance' )}}">
     Staff Attendance
   </a>
 </li> 
 <li><a href="{{  URL::to('/admin/delivery-agents' )}}">
   Monthly Stats
 </a>
</li>  

<li><a href="{{  URL::to('/admin/delivery-agent/performance-snapshot' )}}">
  Agents Performance
</a>
</li> 

<li><a href="{{   URL::to('/admin/staff/enter-salary-page' )  }}">
  Staff Salary Entry
</a>
</li> 
<li><a href="{{  URL::to('/admin/staff/view-staff-salary' )}}">
 View Salary Record
</a>
</li>


<li><a  target='_blank' href="{{  URL::to('/admin/customer-care/delivery-agents-live-locations' )}}">
  Live Location
</a>
</li>

<li><a href="{{  URL::to('/admin/delivery-agent/combined-daily-collection-report' )}}">
  Daily Delivery Report
</a>
</li> 

<li><a href="{{  URL::to('/admin/customer-care/delivery-agents/monthly-report' )}}">
  Monthly Delivery Report
</a>
</li> 
</ul>
</li>

<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false">

    <span>Customers</span>
  </a>
  <ul>
   <li><a class="nav-link" href="{{  URL::to('/admin/systems/search-customers' )}}">
     All Customers
   </a>
 </li> 
 <li><a  href="{{  URL::to('/admin/customer/view-feedback' )}}">
   Order Feedback
 </a>
</li> 

</ul>
</li>
<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false"> 
    <span>Analytics</span>
  </a>
  <ul  >  

   <li><a href="{{  URL::to('/admin/analytics/products/view-frequently-browsed-products' )}}">Browser History
   </a>
 </li>
 <li><a  href="{{  URL::to('/admin/marketing/campaign-area' )}}">View Campaign Areas
 </a>
</li>
<li><a  href="{{  URL::to('/admin/systems/manage-cloud-messages' )}}">Cloud Message
</a>
</li>
<li><a  href="{{  URL::to('/admin/systems/manage-voucher-codes' )}}">Voucher Codes
</a>
</li>

</li>
<li><a href="{{  URL::to('/admin/customer-care/business/products/view-featured-products' )}}">Featured Products
</a>
</li>


</ul>
</li>

<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false">

    <span>Accounting</span>
  </a>

  <ul >
    <li><a href="{{  URL::to('/admin/accounts/view-accounts-dashboard' )}}">Accounts Snapshot
    </a>
  </li> 
  <li><a  href="{{  URL::to('/admin/accounts/add-deposit' )}}">New Deposit
  </a>
</li> 
<li><a href="{{  URL::to('/admin/accounts/add-expenditure' )}}">Expenditure Entry
</a>
</li>
<li><a href="{{  URL::to('/admin/accounts/view-expenditure' )}}">View Expenditure
</a>
</li> 
<li><a href="{{  URL::to('/admin/accounts/account-ledger-entry' )}}">Daily Ledger Entry
</a>
</li>

</ul>
</li>
<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false"> 
    <span>Sales Stats</span>
  </a>
  <ul> 
    <li><a href="{{  URL::to('/admin/report/daily-sales-and-service' )}}">Daily Sales Volume
    </a>
  </li>
  <li><a  href="{{  URL::to('/admin/report/monthly-earning' )}}">Monthly Sales &amp; Earning
  </a>
</li>
<li><a  href="{{  URL::to('/admin/billing-and-clearance/daily-sales' )}}">Daily Sales &amp; Service
</a>
</li>
<li><a href="{{  URL::to('/admin/report/sales-and-service-per-cycle' )}}">Weekly Sales &amp; Service 
</a>
</li> 
</ul>
</li>


<li class="">
  <a   class="has-arrow" href="#" aria-expanded="false"> 
    <span>Billing</span>
  </a>
  <ul> 

   <li><a href="{{  URL::to('/admin/billing-and-clearance/weekly-sales-and-services-report' )}}">Weekly Report
   </a>
 </li>

 <li><a  href="{{  URL::to('/admin/report/pending-payments' )}}">Payment Dues
 </a>
</li> 
<li><a  href="{{  URL::to('/admin/sales-and-clearance-history' )}}">Clearance History
</a>
</li> 
</ul>
</li>


<li class="">
  <a  href="{{  URL::to('/admin/systems/manage-business-categories' )}}" aria-expanded="false"> 
    <span>Systems</span>
  </a>
</li>
<li class="">
  <a  href="{{  URL::to('/admin/cms' )}}" aria-expanded="false"> 
    <span>CMS</span>
  </a>
</li> 

</ul>
</nav>