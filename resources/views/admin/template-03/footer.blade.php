<div class="footer_part">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer_iner text-center">
              <p>Copyright © {{ date('Y')}} - All right reserved. Owned by <a href="https://booktou.in">bookTou.in</a></p>
                </div>
            </div>
        </div>
    </div>
</div> 