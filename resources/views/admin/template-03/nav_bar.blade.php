<div class="container-fluid no-gutters">
        <div class="row">
            <div class="col-lg-12 p-0 ">
                <div class="header_iner d-flex justify-content-between align-items-center">
                    <div class="sidebar_icon d-lg-none">
                        <i class="ti-menu"></i>
                    </div>
                    <div class="serach_field-area d-flex align-items-center">
                            <div class="search_inner">
                                <form action="https://demo.dashboardpack.com/sales-html/#">
                                    <div class="search_field">
                                        <input type="text" placeholder="Search here...">
                                    </div>
                                    <button type="submit"> <img src="./_sales-html__files/icon_search.svg" alt=""> </button>
                                </form>
                            </div>
                            <span class="f_s_14 f_w_400 ml_25 white_text text_white">Apps</span>
                        </div>
                    <div class="header_right d-flex justify-content-between align-items-center">
                        <div class="header_notification_warp d-flex align-items-center">
                            <li>
                                <a class="bell_notification_clicker nav-link-notify" href="https://demo.dashboardpack.com/sales-html/#"> <img src="./_sales-html__files/bell.svg" alt="">
                                    
                                </a>
                                <!-- Menu_NOtification_Wrap  -->
                            <div class="Menu_NOtification_Wrap">
                                <div class="notification_Header">
                                    <h4>Notifications</h4>
                                </div>
                                <div class="Notification_body">
                                    <!-- single_notify  -->
                                    <div class="single_notify d-flex align-items-center">
                                        <div class="notify_thumb">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><img src="./_sales-html__files/2.png" alt=""></a>
                                        </div>
                                        <div class="notify_content">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><h5>Cool Marketing </h5></a>
                                            <p>Lorem ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                    <!-- single_notify  -->
                                    <div class="single_notify d-flex align-items-center">
                                        <div class="notify_thumb">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><img src="./_sales-html__files/4.png" alt=""></a>
                                        </div>
                                        <div class="notify_content">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><h5>Awesome packages</h5></a>
                                            <p>Lorem ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                    <!-- single_notify  -->
                                    <div class="single_notify d-flex align-items-center">
                                        <div class="notify_thumb">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><img src="./_sales-html__files/3.png" alt=""></a>
                                        </div>
                                        <div class="notify_content">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><h5>what a packages</h5></a>
                                            <p>Lorem ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                    <!-- single_notify  -->
                                    <div class="single_notify d-flex align-items-center">
                                        <div class="notify_thumb">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><img src="./_sales-html__files/2.png" alt=""></a>
                                        </div>
                                        <div class="notify_content">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><h5>Cool Marketing </h5></a>
                                            <p>Lorem ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                    <!-- single_notify  -->
                                    <div class="single_notify d-flex align-items-center">
                                        <div class="notify_thumb">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><img src="./_sales-html__files/4.png" alt=""></a>
                                        </div>
                                        <div class="notify_content">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><h5>Awesome packages</h5></a>
                                            <p>Lorem ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                    <!-- single_notify  -->
                                    <div class="single_notify d-flex align-items-center">
                                        <div class="notify_thumb">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><img src="./_sales-html__files/3.png" alt=""></a>
                                        </div>
                                        <div class="notify_content">
                                            <a href="https://demo.dashboardpack.com/sales-html/#"><h5>what a packages</h5></a>
                                            <p>Lorem ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="nofity_footer">
                                    <div class="submit_button text-center pt_20">
                                        <a href="https://demo.dashboardpack.com/sales-html/#" class="btn_1">See More</a>
                                    </div>
                                </div>
                            </div>
                            <!--/ Menu_NOtification_Wrap  -->
                            </li>
                            <li>
                                <a class="CHATBOX_open nav-link-notify" href="https://demo.dashboardpack.com/sales-html/#"> <img src="./_sales-html__files/msg.svg" alt="">   </a>
                            </li>
                        </div>
                        <div class="profile_info">
                            <img src="./_sales-html__files/client_img.png" alt="#">
                            <div class="profile_info_iner">
                                <div class="profile_author_name">
                                    <p>Neurologist </p>
                                    <h5>Dr. Robar Smith</h5>
                                </div>
                                <div class="profile_info_details">
                                    <a href="https://demo.dashboardpack.com/sales-html/#">My Profile </a>
                                    <a href="https://demo.dashboardpack.com/sales-html/#">Settings</a>
                                    <a href="https://demo.dashboardpack.com/sales-html/#">Log Out </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>