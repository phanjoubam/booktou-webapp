@extends('layouts.admin_theme_02')
@section('content')

<div class="panel-header panel-header-sm">
        <canvas id="bigDashboardChart"></canvas>
      </div> 
            <div class="cardbodyy">
          
 
         <div class="col-md-12">
            <div class="card">
              <div class="card-header"> 
                <h4 class="card-title"> customer Stats</h4>
              </div>
              <div class="card-body">
               
                  <table class="table">
                    <thead class=" text-primary"> 
 
                  <tr class="text-center">
                    <th scope="col">Sl No.</th>
                    <th scope="col">Order No</th>
                    <th scope="col">Order DateTime</th>
                    <th scope="col">Time Elapsed</th>
                    <th scope="col">Status</th>
                    <th scope="col">Delivery DateTime</th>
                    <th scope="col">Assigned To</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>

                     <tbody>
                <?php $i=0 ?>
                @foreach ($data['results'] as $item)
                <?php $i++ ?>
                  <tr class="text-center">
                     <td>{{$i}}</td>  
                  
                    <td>
                       {{$item->book_by}}  
                  </td>

                  <td>  {{$item->book_date}} </td>
                    <td>  {{$item->book_category}} </td>
                 
                     <td>{{$item->book_status}}</td> 
                      <td></td>  
                       <td></td>  
                                      
   
        
                    <td class="text-right">
                       
                     <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="">Profile</a>
                          <a class="dropdown-item" href="">Edit Parent Details</a>
                          <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                      </div>
                    </td>
  
                  
                         </tr>
                          @endforeach
                </tbody>
                  </table>
            
              </div>
            </div>
          </div>
        </div>  
  
   
@endsection