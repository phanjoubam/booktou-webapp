@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>

<div class="row">
     

     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-9">
                    <div class="title">
                    	<h3 class="title"><span class="badge badge-secondary">{{$business->name}}</span>
                    		<small>Top selling product for <span class="badge badge-secondary">{{$month}}</span></small></h3>
                      
                    </div> 
                </div>
     <div class="col-md-3"> 
 <form class="form-inline" method="get" action="{{action('Admin\POSAdminController@topSellingProduct')}}">
  {{  csrf_field() }}
  <input type="hidden" name="bin" value="{{$bin}}">
  <div class="form-row">
     
    <div class="col">
    	Month:
       <select  class="form-control form-control-sm" name='month' >
					      	<option value='1'>January</option>
		                    <option value='2'>Febuary</option> 
		                    <option value='3'>March</option> 
		                    <option value='4'>April</option> 
		                    <option value='5'>May</option> 
		                    <option value='6'>June</option> 
		                    <option value='7'>July</option> 
		                    <option value='8'>August</option> 
		                    <option value='9'>September</option> 
		                    <option value='10'>October</option> 
		                    <option value='11'>November</option> 
		                    <option value='12'>December</option>   
					    </select>

					    <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
    </div>
    
  </div>
</form>
   </div>
  

       </div>
                </div>
            </div>
<div class="card-body"> 
<div class="row">  
	<div class="col-md-6">	
		<div class="table-responsive table-bordered">
    	<table class="table">
        <thead class=" text-primary">
		<tr class="text-center">
      	<th scope="col" class="text-left">Product Name</th>
  		<th scope="col" class="text-left">Quantity</th> 
		</tr> 
		</thead> 
		<tbody>
		@foreach($results as $items) 
	        <tr> 
	          <td>{{$items->productName}}</td>
	          <td>{{$items->quantity}}</td> 
	    	</tr> 
	   	@endforeach
		</tbody>
		</table>
		</div>
	</div>


	<div class="col-md-6">	
		 
	</div>
</div>
</div>


  </div> 
	
	</div> 

  
  



</div>


@endsection