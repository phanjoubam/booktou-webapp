@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>

<div class="row">
     

     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <form method="get" action="{{action('Admin\POSAdminController@monthlyMerchantWiseReport')}}">
               <div class="row">
                   <div class="col-md-6">
                    <div class="title">
                    bookTou merchant wise statistics for  
                    <span class='badge badge-primary'><?php echo date("F", mktime(0, 0, 0, date('m'), 10));?></span> </div> 
                	</div>

					<div class="col-md-3 text-right"> 
	                      
	                </div> 
                    <div class="col-md-2">  
 						<select  class="form-control form-control-sm" name='month' >
					      	<option value='1'>January</option>
		                    <option value='2'>Febuary</option> 
		                    <option value='3'>March</option> 
		                    <option value='4'>April</option> 
		                    <option value='5'>May</option> 
		                    <option value='6'>June</option> 
		                    <option value='7'>July</option> 
		                    <option value='8'>August</option> 
		                    <option value='9'>September</option> 
		                    <option value='10'>October</option> 
		                    <option value='11'>November</option> 
		                    <option value='12'>December</option>   
					    </select> 
      				</div>
      				<div class="col-md-1">
						<button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search
						</button>
      				</div>

      			 </div>
      			 </form>
      			</div>



      			<div class="card-body"> 
      		<table class="table">
      			<thead>
      				<th>Merchant Name</th>
      				<th>Bin</th>
      				<th>Total Sales</th>
      				<th>Order Count</th>
      				<th class="text-right">Action</th>
      			</thead>
      			<tbody>
      				@foreach($results as $items)
      				<tr>
      					<td>{{$items->bizname}}</td>
      					<td>{{$items->bin}}</td>
      					<td>{{$items->totalSales}}</td>
      					<td>{{$items->orderCount}}</td>
      					<td class="text-right"> 
      						<a href="{{URL::to('admin/pos/monthly-sales-report')}}?bin={{$items->bin}}&year={{$year}}" class="btn btn-primary btn-primary-sm">View Details</a>

      						<a href="{{URL::to('admin/pos/top-selling-product')}}?bin={{$items->bin}}&month={{$month}}" class="btn btn-primary btn-primary-sm">Top Selling Products</a>
      					</td>
      				</tr>
      				@endforeach
      			</tbody> 
      		</table>
      		</div>
      		</div> 
      	</div>
      	 
      </div>

@endsection