@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>

<div class="row">
     

     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
               <div class="row">
               		
                   <div class="col-md-6">
                   	<form method="get" action="{{action('Admin\POSAdminController@monthlySalesReport')}}">
                  	<div class="title">
                    bookTou merchant wise POS usage statistics for @if(isset($year))<span class='badge badge-primary'>{{$year}}</span>@endif</div> 
                	</div>

					<div class="col-md-3 text-right"> 
	                      <select  class="form-control form-control-sm selectize" name='bin' >
					        @foreach($results as $items) 
					        <option value='{{$items->id}}'>{{$items->name}}({{$items->frno}})</option> 
					        @endforeach
					      </select> 
	                </div> 
                    <div class="col-md-2">  
 						<select  class="form-control form-control-sm" name='year' >
					        <?php 
					        for($i= date('Y') ; $i >= 2020; $i--)
					        { 
					        ?> 
					        <option value='{{ $i }}'>{{ $i }}</option> 
					        <?php 
					        }
					      ?>
					      </select> 
					      
      				</div>
      				<div class="col-md-1">
						<button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
      				</form>
      			</div>

       			</div>
            </div>


       <div class="card-body">   
<div class="table-responsive">
    <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
      <th scope="col" class="text-left">Period</th>
  		<th scope="col" class="text-center">Order Count</th>
  		 <th scope="col" class="text-center">Total Sales</th>  
        
		</tr> 
	</thead> 
		<tbody> 
		@foreach($report_data as $items)
		 
        <tr> 
          <td class="text-left" ><span class='badge badge-primary badge-pill'>{{$items['month']}}</span></td>
    	  <td class="text-center">{{$items['orderCount']}}</td>
    	  <td class="text-center"><span class='badge badge-success'>{{$items['totalSales']}} ₹</span></td> 
           
        </tr> 
        @endforeach 
	</tbody>
		</table> 
	</div>
 </div>


  </div> 
	
	</div> 

 <div class="col-md-12 grid-margin">
	<div class="">                
		<div class="card">
                  <div class="card-header">
                            <h4 class="card-title mb-0">Monthly Performance Graph</h4>
                   
                        </div>
                        <div class="card-body"> 
		                <canvas class="mt-5" height="120" id="sales-statistics-overview"></canvas>
		                </div>
        </div>
    </div>            
 </div>
  



</div>
@endsection 
@section("script") 
<script type="text/javascript">
	 $('.selectize').select2({
        selectOnClose: true
      }); 
</script>




<script> 

 /* bar graph */

<?php
 
  
   $months  = array();
   $earnings  = array(); 

    foreach($report_data as $item)
    {
       $months[] =  '"'.  $item['month'] . '"' ;
       $earnings[] =  $item['totalSales'];
    }
  ?> 


if ($("#sales-statistics-overview").length) { 

    var barChartCanvas = $("#sales-statistics-overview").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: {
        

        labels: [ <?php echo implode(",",  $months ); ?> ],
        datasets: [{
          label: 'Revenue',
          data: [<?php echo implode(",",  $earnings ); ?> ],
          backgroundColor: ChartColor[0],
          borderColor: ChartColor[0],
          borderWidth: 0
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Sales by month',
              fontSize: 12,
              lineHeight: 2
            },
            ticks: {
              fontColor: '#bfccda',
              stepSize: 50,
              min: 0,
              max: 150,
              autoSkip: true,
              autoSkipPadding: 15,
              maxRotation: 0,
              maxTicksLimit: 10
            },
            gridLines: {
              display: false,
              drawBorder: false,
              color: 'transparent',
              zeroLineColor: '#eeeeee'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Total Sales',
              fontSize: 12,
              lineHeight: 2
            },
            ticks: {
              display: true,
              autoSkip: false,
              maxRotation: 0,
              fontColor: '#bfccda',
              stepSize: 5000,
              min: 1000 
            },
            gridLines: {
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul>');
          for (var i = 0; i < chart.data.datasets.length; i++) {
            console.log(chart.data.datasets[i]); // see what's inside the obj.
            text.push('<li>');
            text.push('<span style="background-color:' + chart.data.datasets[i].backgroundColor + '">' + '</span>');
            text.push(chart.data.datasets[i].label);
            text.push('</li>');
          }
          text.push('</ul></div>');
          return text.join("");
        },
        elements: {
          point: {
            radius: 0
          }
        }
      }
    });
    document.getElementById('bar-traffic-legend').innerHTML = barChart.generateLegend();
  }



 
 
</script> 
@endsection