@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
<div class="col-md-12 ">
<div class="card">
          <div class="card-header">
          <div class="row">
           <div class="col-md-4"> <h5>View Premium Business</h5></div>
           <div class="col-md-8"> 
            @if (session('err_msg'))
            <div class="alert alert-info p-0">
            {{session('err_msg')}}
            </div>
            @endif 
          </div>
          </div>
          </div>
</div>
<div class="card">
<div class="card-body">
    <table class="table  table-responsive">
        <th scope="col">Category and location</th>
        <th scope="col">Phone</th> 
        <th scope="col">Name</th>
        <th scope="col">End Date</th>
        <th scope="col">Action</th>
        <tbody>
           <?php $i = 0 ?>
                    @foreach ($data['premium_info'] as $item)

             <?php $i++ ?>
                <tr>
<td style="white-space:normal;">  <span class='badge badge-primary'>{{ $item->category}}</span>
      <br/>
      {{$item->locality}}, <br/>{{$item->landmark}}, <br/>{{$item->city}}, {{$item->state}}-{{$item->pin}}
  
  </td>
                    <td>{{$item->phone_pri}}</td>
                     <td>{{$item->name}}</td>
                      <td>{{$item->ended_on}}</td> 
                    <td>
                        <button class="btn btn-primary btnedit"
                        data-id="{{$item->premiumid}}"
                        data-bin="{{$item->bin}}"
                        data-start_from="{{$item->start_from}}"
                        data-ended_on="{{$item->ended_on}}"
                        data-widget="edit">Edit</button>
                        
                        <button class="btn btn-danger btndel" 
                        data-id="{{$item->id}}" data-widget="del"
                        >Delete</button>
                    </td>
                </tr>   
             @endforeach
        </tbody>
    </table>
   
  
</div>
</div>
</div>
</div>

<!-- edit option -->
<form action="{{action('Admin\PopularcategoryController@editPremiumBusiness')}}" method="post" > 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-edit" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content ">
        <div class="modal-header">
          <h5 class="modal-title" >Edit premium business</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="card mt-2 mb-2">
            <div class="row "> 
              <div class="col-md-12">

                        <div class="col-md-4 offset-md-4">
                          <label for="category" class="form-label">End date</label>
                          <input type="text" class="calendar form-control" name="ended_on" id="ended_on">
                        </div>
                 
                   </div>                  
        
            </div>
             
        </div>
        <div class="modal-footer">
         <input type='hidden' name='key' id='key'/>

          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='save' name='btndel'>Update</button>
        </div>
      </div>
    </div>
  </div>
 </form>
<!-- edit ends here -->

<!-- delete modal -->
<form action="{{action('Admin\PopularcategoryController@deletePremiumBusiness')}}" method="post" > 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-del" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Confirm Record Deletion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">You are about to delete this record. Please confirm your action?</div>
        <div class="modal-footer">
         <input type='hidden' name='key' id='keydel'/>
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' name='btndel'>Delete</button>
        </div>
      </div>
    </div>
  </div>
 </form>
 <!-- delete ends here -->

 

@endsection

@section('script')
<script type="text/javascript">


$(".btnedit").on('click', function(){ 
      var key  = $(this).attr("data-id"); 
      $("#key").val(key); 
      $widget  = $(this).attr("data-widget"); 
      $("#start_from").val($(this).attr("data-start_from"));
      $("#ended_on").val($(this).attr("data-ended_on")); 
      $("#widget-"+$widget).modal('show'); 
}); 
 
$(".btndel").on('click', function(){ 
      var key  = $(this).attr("data-id"); 

      $("#keydel").val(key); 
      $widget  = $(this).attr("data-widget"); 
      $("#widget-"+$widget).modal('show'); 
}); 
 
</script>
<script> 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
  });
</script>
@endsection
