@extends('layouts.admin_theme_02')
@section('content')
<?php
  $product = $data['product'];
?>


 
   <div class="row">
     <div class="col-md-12"> 
        <div class="col-md-12"> 
            @if(session('err_msg')) 
              <div class="alert alert-info">
                  {{ session('err_msg') }}
              </div>
            @endif 
         </div>
</div>

      <div class="col-md-6">  
          <div class="card card-default">
              <div class="card-header"> 
                <h4 class="card-title">Product Details</h4> 
              </div>
              <div class="card-body "> 
                <div class="row">  
                  <div class="col-lg-12">
                     <strong>{{$product->pr_name}} </strong>
                  </div> 
             
                  <div class="col-lg-12">
                    <strong>{{$product->description}}</strong>
                  </div>

              </div>    
              <hr/> 
              <div class="card-body ">
              <form action="{{ action('Admin\AdminDashboardController@uploadProductImage') }}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }} 
                   
                    <div class="form-group"> 
                      <input type='file' name='photos[]' multiple placeholder='Select Product Image' />
                      <small>Upload product images</small>
                    </div>
                    
                    <div class="form-group">  
                        <input type="hidden" name='bin' value="{{  $data['bin']}}"   />
                        <input type="hidden" name='pid'  value="{{  $data['pid']}}" /> 
                        <button type="submit" name='btn_save' class="btn btn-primary btn-sm">Save</button>
                    </div>

              </form>
            </div> 
             </div> 
          </div>
</div>

<div class='col-md-6'> 
  <div class="card card-default">  
      <div class="card-header"> 
          <h4 class="card-title">Product Images</h4> 
      </div>
  <div class="card-body"> 

  <?php  
      
      $first_photo  = URL::to("/public/assets/image/no-image.jpg");
      $slides = "";
      $files =  explode(",",  $data['product']->image_url ); 
      if(count($files) > 0 )
      {
          $files=array_filter($files);
          $folder_path =  "/public/assets/image/store/bin_" .   $data['bin']  ;
          foreach($files as $file )
          {
            if(file_exists( base_path() .  $folder_path . "/" .  $file ))
            {

              $first_photo =  URL::to( $folder_path  ) . "/" .    $file  ; 

              $slides .= '<div class="carousel-item active">
                    <img width="300px" src="' . $first_photo  .  '" class="d-block w-100" alt="...">
                  </div>';


            } 
          } 
      } 
           
      ?> 

      @if($slides != '')
      <div id="carouselExampleControls" class="carousel slide" data-ride="carousel"> 
        <div class="carousel-inner">
          <?php echo  $slides ; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a> 
      </div>
      @endif
      </div>  
    </div>  
  </div>
</div>



</div>



          </div> 
      <hr/> 
       
 
 </div>
          </div>
        </div>  
   </div>
 
 



 
 </div>

@endsection  

@section("script")
         <!--Upload Image/File script-->
                <script type="text/javascript">
                  function readURL(input) {
                    if (input.files && input.files[0]) {
                      var reader = new FileReader();
            
                    reader.onload = function (e) {
                        $('#input-icon-tag').attr('src', e.target.result);
                    }
                     reader.readAsDataURL(input.files[0]);
                   }
                 }
                 $("#input-icon").change(function(){
                  readURL(this);
                });
              

 

$(document).on("click", ".btn_edit", function()
{
  var id = $(this).attr("data-id"); 
  var name = $(this).attr("data-name"); 
  var url = $(this).attr("data-url"); 
var desc = $(this).attr("data-desc"); 


    $("#category_name").val(name);
    $("#catimage").attr("src",  url); 
    $("#category_description").val(desc);


})





$(document).on("click", ".btn_del_category", function()
{
    var id = $(this).attr("data-id");
     $("#hidcid").val( id ); 

    $("#del_category").modal("show"); 

})


 </script>
             

@endsection
