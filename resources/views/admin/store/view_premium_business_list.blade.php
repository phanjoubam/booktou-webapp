 <div class="row">                 
    <div class="col-md-4">
      <label for="category" class="form-label">Business list</label>
      <select class="form-control" name="id">
       @foreach($business as $cat)
        <option value="{{$cat->id}}">
          {{$cat->name}}
        </option>
       @endforeach
      </select>
    </div>
  </div>
