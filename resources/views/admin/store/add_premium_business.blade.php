@extends('layouts.admin_theme_02')
@section('content')
<div class="row">
    <div class="col-md-8 offset-2"> 
	      <div class="card">
           
          <div class="card-header">
          <div class="row">
           <div class="col-md-4"> <h5>Add Premium Business</h5></div>
           <div class="col-md-8"> 
            @if (session('err_msg'))
            <div class="card">
            <div class="alert alert-info p-0">
            {{session('err_msg')}}
            </div>
            </div>
            @endif 
          </div>
          </div>
          </div>

               
             <div class="card-body">
                   
              <form  method="post" action="{{action('Admin\PopularcategoryController@savePremiumBusiness')}}"
              class="row g-3" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="col-md-4">
                      <label for="inputState" class="form-label">Main Module</label>
                     
                      <select  class="form-control selectType" name="id"> 
                        <option>SELECT</option>
                      @foreach ($data['mainmodule'] as $items)

                        <option value="{{$items->main}}">{{$items->main}}</option>
                         @endforeach 
                      </select>
                      
                  </div>
                   <div class="col-md-8 show-business-name">
                                          
                  </div>
               @foreach ($data['business'] as $items) 
                         @endforeach 
                
                	 <div class="col-md-5 mt-3">
                        <label  class="form-label"> Start Date:</label>
                        <input type="text" name="start_from" class="calendar form-control">
                       
                    </div>
                    <div class="col-md-5  mt-3">
                        <label  class="form-label">End Date:</label>
                        <input type="text" name="ended_on" class="calendar form-control">
                       
                    </div>

                      <div class="col-md-12 mt-3 text-center">
                       <div class="col">
                       

                       	<input type='hidden' name='bin' id='bin' value="{{$items->id}}" />
                        
                       <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">save</button>
                       </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection 
@section("script")
<script> 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
  });
</script>
<script>
     //var siteurl = "//localhost/btwebApp";

     var siteurl =  "<?php echo config('api_url') ?>";


  $('.selectType').on('change', function() { 
     
    $.ajax({ 
    url : siteurl +'/admin/business/view-premium-business-list' ,
    data: {
    'items': this.value
    },
    type: 'get', 
    success: function (response) {
    if (response.success) { 
                        
                   $('.show-business-name').empty().append(response.html);
    }
    else  {
                    
                   $('.show-business-name').empty();
          }
    }

    });
}); 
</script>



@endsection


