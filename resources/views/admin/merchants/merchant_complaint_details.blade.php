@extends('layouts.admin_theme_02')
@section('content') 
 

  <div class="row">
  <div class="col-md-8">

    <div class="card panel-default">
@if (session('err_msg'))
              <div class="alert alert-info p-0">
                {{session('err_msg')}}
              </div>
              @endif
       <div class="card-header">
         <h4 class="card-category">Merchant complaint details</h4> 
       </div>
       <div class="card-body">
      <label>Merchant complaint nature</label>
     @foreach($complaint_table as $complaint)
         <input type="text" class="form-control" name="main_module" value="{{$complaint->complaint_nature}}" readonly>
     @endforeach             
      <label>Message</label>
     @foreach($complaint_table as $complaint)
     <textarea name="complaint" class="form-control" id="textarea" rows="3" readonly>{{$complaint->complaint_details}}</textarea>
     @endforeach             
       </div>

       <div class="card-footer">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#remarkModal" data-key="{{$complaint->id}}">
  Remarks
</button>
       </div>
    </div>
    

  </div>
  <div class="col-md-4">

  <div class="card panel-default mt-2">

              <div class="card-header">
                <h5 class="card-category">Merchant Info</h5> 
                </div>
              <div class="card-body">                 
                    <div class="timeline-item">
                      @foreach($complaint_table as $complaint)
                      <div class="timeline-item-content">
                       {{$complaint->biz_name}} <br/>
                       {{ date('d-m-Y', strtotime( $complaint->complaint_date )) }}  <br/>
                       {{ date('h-i-a', strtotime( $complaint->complaint_date )) }}
                        <br/>
                        <i class='fa fa-phone'>{{$complaint->reg_phone_no}} </i> 
                        </div>
                        @endforeach
                    </div> 
                </div>
               </div>

            
               <div class="card panel-default mt-2">
                
                    <div class="card-header">
                      <div class="row">
                  <div class="col-md-10">
                  <h5 class="card-category">Remark by</h5>
                  </div> 
                  </div>

                
                <div class="card-body">                 
                  <div class="timeline-item">
                    @foreach($complaint_table as $complaint)
                    @if($complaint->remarks==null)
                    <p class='alert alert-info'>There is no remark update</h5>
                    @else
                    @foreach($profile as $user)
                    <div class="timeline-item-content">
                      
                    {{$user->fullname}}
                    <br/> 
                    {{ date('d-m-Y', strtotime( $complaint->last_updated )) }}  <br/>
                       {{ date('h-i-a', strtotime( $complaint->last_updated )) }}
                     
                     <br/>
                     <i class='fa fa-phone'>{{$user->phone}} </i>
                     <br/>
                     <textarea name="complaint" class="form-control" id="textarea" rows="3" readonly>{{$complaint->remarks}}</textarea>
                      
                   </div>

                   @endforeach

                   @endif
                   @endforeach
                 </div> 
               </div>

            </div>
          </div>
</div>
</div>



<!-- Button trigger modal -->


<!-- Modal -->
<form action="{{ action('MerchantComplaintsController@addRemarksMerchantComplaints') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal fade" id="remarkModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="remarkModalLabel">Remarks &amp; Status Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="form-group">
          <label for="status">Status Type</label>
          <select  class="form-control" id="status" name='status' aria-describedby="status">
            <option value="resolved">Resolved</option>
            <option value="spam">Spam</option>
            <option value="escalate">Escalated to Marketing</option>
          </select>
           
        </div>
        <div class="form-group">
            <label for="remarks">Remarks</label>
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="id" value="{{$complaint->id}}">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="saveRemark" value="save">Save Remarks</button>
      </div>
    </div>
  </div>
</div>
</form>



 
        
    

    

 

@endsection




   