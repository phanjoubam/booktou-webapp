@extends('layouts.admin_theme_02')
@section('content')
<?php 
$cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
    ?>


    <div class="row">
      <div class="col-md-12">
        <div class="card" >
          <div class="card-header">
          <div class="row">
            <div class="col-md-8">
              <h4>Merchant's complaint reports</h4>
              
              @if (session('err_msg'))
              <div class="alert alert-info p-0">
                {{session('err_msg')}}
              </div>
              @endif 

            </div>
          </div> 

        </div>

            <div class="card-body" >
               <div class="table-responsive table-with-menu" >
                <table class="table" style='min-height:350px !important;'>
                  <thead class=" text-primary">
                    <tr class="text-center">
                       <th scope="col">Complaint_id</th>
                      <th scope="col">Nature of complaint</th>

                      <th scope="col">Business name</th>  
                      <th scope="col">Phone</th>

                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                    $slno=1;
                    @endphp
                    @foreach($complaint_table as $complaint)
                    <tr class="text-center">
                    <td>{{$complaint->id}}</td> 

                               <td><a class='btn btn-link' target='_blank' href="{{ URL::to('/admin/merchants/view-merchant-complaints')}}?cid={{ $complaint->id }}">{{$complaint->complaint_nature}}</a></td>

             <td>{{$complaint->biz_name}}</td>

             <td>{{$complaint->reg_phone_no}}</td>
             <td>
              <div class="dropdown">
              <button class="btn btn-secondary " type="button" data-toggle="dropdown" aria-expanded="false">
                <i class='fa fa-cog'></i>
              </button>
              <div class="dropdown-menu">
                <a class="dropdown-item" target='_blank' href="{{ URL::to('/admin/merchants/view-merchant-complaints')}}?cid={{ $complaint->id }}">View Complaint</a>
              </div>
            </div>

             </td>
             
                    </tr>
                    @php
                    $slno++;
                    @endphp
                    @endforeach
                    
                    
                  </tbody>
                </table>

               </div>
              
            </div>
            @if ($complaint_table->hasPages())        

           <div class="d-flex justify-content-center">
                  {{ $complaint_table->links() }}
                </div>
          @endif
        </div>
      </div>
    </div>


<div class="modal fade" id="modal_add_promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <form method='post' action="{{ action('Admin\CarRentalServiceController@savePromotion') }}" enctype="multipart/form-data">
  {{  csrf_field() }}

  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          This is final step to add promo list.  
        </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='bin' id='bin'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
</form>
</div>
@endsection





