@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-6">
                  <div class="title">
                    Daily Sales Tartet Report for <span class='badge badge-primary'>{{ date('d-m-Y', strtotime($date)) }}</span> 

                  </div> 
                </div>

<div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-4">  
        <form class="form-inline" method="get" action="{{ action('Admin\AdminDashboardController@dailySalesReport') }}"   >
              {{  csrf_field() }}
      
      <div class="form-row">
    <div class="col-md-12">
      <input  class="form-control form-control-sm calendar" name='todayDate'  />
   
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>

       </div>
                </div>
            </div>
       <div class="card-body">   
<div class="table-responsive"> 
    <table class="table">
      <thead>
        <tr>
          <th>Sl. No.</th>
          <th>Business</th>
          <th class='text-right'>PnD Given / Target To Reach</th>
          <th class='text-right'>Normal Order / Target To Reach</th> 
        </tr>
      </thead>
      
      <tbody>
        @php 
          $totalSale = $totalFee = 0.00;
          $i=1;
        @endphp

        @foreach($businesses as $business)
           @php 
              $pndCount = $normalCount = 0 ;
            @endphp

          @foreach ($sales as $sale)
              @if($business->bin == $sale->bin)
                  @if($sale->orderType == "pnd")
                    @php
                     $pndCount = $sale->salesCount;
                    @endphp
                  @endif
                  @if($sale->orderType == "normal")
                    @php
                     $normalCount = $sale->salesCount;
                     @endphp
                  @endif
              @endif
          @endforeach 

          <tr>
              <td>{{ $i}}</td>
            <td>{{ $business->name }}</td> 
            @if($pndCount >= 6)
              <td class='text-right'><span class='badge badge-success'>{{ $pndCount }} / {{ $business->pnd_count }}</span></td>
            @else 
              <td class='text-right'><span class='badge badge-danger'>{{ $pndCount }} / {{ $business->pnd_count }}</span></td>
            @endif
            @if($normalCount >= 6)
              <td class='text-right'><span class='badge badge-success'>{{ $normalCount }} / {{ $business->normal_count }}</span></td>
            @else 
               <td class='text-right'><span class='badge badge-danger'>{{ $normalCount }} / {{ $business->normal_count }}</span></td>
            @endif
          </tr> 

          @php 
          $i++;
          @endphp
        @endforeach
 

      </tbody>

    </table>
		
	</div>
 </div>


  </div> 
	
	</div> 
</div>

 
 
 

 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

