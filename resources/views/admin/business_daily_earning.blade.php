@extends('layouts.admin_theme_02')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 


<div class="cardbodyy margin-top-bg"> 



          <div class="card-body">
            <div class="row">
       
         <div class="col-lg-12">
         <div class="card card-chart">  
              
<div class="card-body">
    <div class="card-header"> 
                <h4 class="card-title text-center">Daily Earning Report</h4>
              </div>
              <br/>

         <form class="form-inline" method="get" action="">

            {{ csrf_field() }}
              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Enter/Select Date:</label> 
             
           <input class='form-control form-control-sm my-1 mr-sm-2' type="date" name="reportDate">

                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>

  <hr class="my-4" />

 <table class="table">
      
  
  <thead class=" text-primary">
    <tr class="text-center">
      <th scope="col">Sl.No.</th>
    <th scope="col">Order No.</th>
    <th scope="col">Order By</th>
    <th scope="col">Status</th>
    <th scope="col">Total Cost</th>
    <th scope="col">Delivery Charge</th>
    <th scope="col">Delivery By</th>
    </tr>
    </thead>

    <tbody>

     <?php $i = 0 ?>
    @foreach ($data['results'] as $item)
    <?php $i++ ?> 
     <tr class="text-center">
     <td>{{$i}}</td>
     <td>{{$item->orderNo}}</td>
      <td>{{$item->orderBy}}</td>
      <td>{{$item->orderStatus}}</td>
      <td>{{$item->totalCost}}</td>
      <td>{{$item->deliveryCharge}}</td>
      <td>{{$item->deliverBy}}</td>
    </tr>
   
    

    @endforeach
   </tbody>

  </table>
</br>
<div>
   <strong>Total Earning:</strong> {{$data['totalEarning']}}
     </div>
  </div>

   
</div>    
  
   </div> 

   </div>
 </div>
</div>

     

@endsection


 
  
 