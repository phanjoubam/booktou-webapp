@extends('layouts.admin_theme_02')
@section('content')
 
<?php 
if(isset($all['resultbyId']))
{

foreach($all['resultbyId'] as $val)
{
  $prod_group = $val->product_group;
  //$prodcode = $val->pr_code;
  $prod_title = $val->title;
  $prod_description = $val->description;
  $landing_url = $val->landing_url;
  $id = $val->id;
  //$pid = $val->pid;
}

}

?>

<div class="row">
<div class="col-md-8"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Product Group Setting</div> 
                </div>
 <div class="col-md-4 text-right"> 
        
      </div>

       </div>
                </div>
            </div>
       <div class="card-body"> 

       @if (session('err_msg'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg') }}
      </div>
      </div>
    @endif


<div class="table-responsive" >
    <table class="table">
         <thead class=" text-primary">
          <tr >
      <th scope="col">Product</th>
      <th scope="col">Landing url</th>
      <th scope="col">Action</th>
          </tr>
        </thead>
  
         <tbody>

           @foreach($all['result'] as $res)
    <tr>
       
      <td>{{$res->product_group}}</td>
      <td>{{$res->landing_url}}</td>
      <td>
        <a href="{{URL::to('admin/product/enter-product-master' )}}?id={{$res->id}}" class=" btn btn-primary"><i class="fas fa-pencil-alt"></i>Edit</a> 
    <a id="deleteGroupsModal" href="" data-toggle="modal" data-target="#showModal" 
    data-key="{{$res->id}}" onclick="deleteOption({{$res->id}});" class="btn btn-danger">
    <i class="fas fa-trash-alt"></i>Delete</a>
    <!-- {{URL::to('admin/product/delete-product-group' )}}?id={{$res->id}} -->
      </td>
    </tr>
@endforeach      
         
         </tbody>
            </table>
  
    
</div>
</div> 
  </div>  
  </div> 


<form action="{{action('Admin\AdminDashboardController@deleteProductGroup')}}" method="post" >
{{csrf_field()}}
<!-- Modal HTML -->
 <div id="showModal" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header flex-column text-center">
        <h4 class="modal-title w-100 text-center"><i class="fa fa-trash text-center"></i> Are you sure? </h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <p>Do you really want to delete these records? This process cannot be undone.</p>
        <input type="hidden" value="" id="productGroupId" name="id">
      </div>
      <div class="modal-footer justify-content-center">
        <button type="submit" class="btn btn-danger" id="deleteConfirm">Delete</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="deleteCancel">Cancel</button>
      </div>
    </div>
  </div>
</div> 
<!-- end modal html -->

</form>








<div class="col-md-4"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Product Group Setting</div> 
                </div>
 <div class="col-md-4 text-right"> 
 



 </div>

       </div>
                </div>
            </div>
  <div class="card-body"> 
  @if (session('err_msg_right'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg_right') }}
      </div>
      </div>
    @endif  
    
     <form class="row g-3" method="post" 
action="{{action('Admin\AdminDashboardController@saveProductGroupMaster')}}">
      {{ csrf_field() }} 
<input type="hidden" @if(isset($id))value="{{$id}}" @else value="" @endif name="group_id">
<!-- <input type="hidden" @if(isset($pid))value="{{$pid}}" @else value="" @endif name="listed_id"> -->

      <div class="card-body">
          <div class="form-row">
            <div class="form-group col-md-12">
            <input type="text" class="form-control" id="pr_title" name="pr_title" 
            placeholder="Title" 
            @if(isset($prod_title))value="{{$prod_title}}" @else value="" @endif >        
            </div>

            <div class="form-group col-md-12">
              <input type="text" class="form-control" id="pr_group" name="pr_group" 
    placeholder="Produt group name" 
    @if(isset($prod_group))value="{{$prod_group}}" @else value="" @endif >
            </div>
            <!-- <div class="form-group">
              <input type="text" class="typeahead form-control"  name="prod_code" 
              id="productName" placeholder="Search by Product Name:" 
              @if(isset($prodcode)) value="{{$prodcode}}" @else @endif >
            </div> -->

            <div class="form-group col-md-12">
              <input type="text" class="form-control" id="landing_url" name="landing_url" 
    placeholder="Landing Url" 
    @if(isset($landing_url)) value="{{$landing_url}}" @else value="" @endif >
            </div>

            <div class="form-group col-md-12">
              <textarea class="form-control" 
    placeholder="Description" id="floatingTextarea2" 
    style="height: 100px" 
    name="pr_group_description">@if(isset($prod_description)){{$prod_description}}@endif</textarea>
            </div>

<div class="form-group col-md-12">
  @if(isset($id))
    <button type="submit" class="btn btn-primary" name="btn_save" value="Update">Update</button>
    @else
 <button type="submit" class="btn btn-primary" name="btn_save" value="Save">Save</button>
    @endif
</div>

      </div>
      </div>

    </form>

 </div> 
  </div>  
  

</div>
</div>




@endsection

@section("script")

<script>

 /*$( "#productName" ).autocomplete({

        source: function( request, response ) {

          // Fetch data
          var path = 'get-product-name';
          $.ajax({

            url:path,
            type: 'get',
            dataType: "json",
            data: {
              _token: $('input[name=_token]').val(),
               search: request.term
            },

            success: function( data ) {
               response( data );
            }
          });
        },

        select: function (event, ui) {
          $('#productName').val(ui.item.value);
           return false;
        }

      });

*/

 function deleteOption(key)
{
    $('#productGroupId').val(key);
}

</script>
@endsection 