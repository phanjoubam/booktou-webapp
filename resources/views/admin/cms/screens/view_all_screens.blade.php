@extends('layouts.admin_theme_02')
@section('content')
   
  

   <div id="result">
   <div class="row">
     <div class="col-md-12">  
       @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif
    </div>
  </div>

  <div class="p-2"  >
  <div class="row"> 




   <div class='col-sm-12 col-md-4 text-center'>  
       
  <div class="card">
  <div class="card-body">
        <div style="padding:65px 10px" class='text-center'>  
          <button class='btn btn-primary btn-rounded' data-toggle="modal" data-target="#wgt-createscreen">Create App Screen</button>
        </div>
         </div></div>
</div> 
 


    <?php 
      $i=1;
      foreach ($screens as $item)
      {  
    ?>
 
   

   <div class='col-sm-12 col-md-4 text-right'>  
      <ul class="list-group">  
        <li class="list-group-item d-flex justify-content-between align-items-center">  
     
        <span class="display-4">App Screen Details</span> 
        
     
        </li> 

       <li class="list-group-item d-flex justify-content-between align-items-center">
        Screen Name
        <a href="{{ URL::to('/admin/customer-care/pnd-order/view-details/') }}/{{$item->id}}" target='_blank'>
        <span class="badge badge-primary badge-pill display-5">{{$item->screen_name }}</span></a>
     
        </li> 


           
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Total section <span class="badge badge-primary badge-pill"></span>
 

 <form>
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInput">Total Sections</label>
      <input type="text" class="form-control form-control-sm mb-2" id="inlineFormInput" placeholder="Sections in the screen" value="{{  $item->total_sections   }}">
    </div>
     
    <div class="col-auto">
      <button type="submit" class="btn btn-primary btn-xs btn-rounded mb-2">Save</button>
    </div>
  </div>
</form>


      </li> 

      <li class="list-group-item d-flex justify-content-between align-items-center">
      <strong>Is screen visible in app?</strong>

        <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input switch" data-id='{{$item->id }}' id="switchON{{ $item->id }}"  
                 @if($item->is_published == "yes") checked @endif />
                <label class="custom-control-label" for="switchON{{$item->id }}">Yes</label>
        </div>  
      </li>

<li class="list-group-item d-flex justify-content-between align-items-center">

<a class="btn btn-success btn-rounded"   href="{{ URL::to('/admin/app-screen/manage-sections') }}?sid={{ $item->id }}">View Sections</a>

      </li>
    </ul>
 
</div>
 
 
 

<?php
  $i++; 
}
?>
</div></div>
</div> 

  

  <form method='post' action="{{ action('Admin\AppCmsController@addAppScreen') }}">
  {{  csrf_field() }}
<div class="modal fade" id="wgt-createscreen" tabindex="-1" aria-labelledby="wgt-createscreen" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create New Screen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="screename">Screen Name</label>
          <input type="text" class="form-control" name="screename" id="screename"  >
          <small id="emailHelp" class="form-text text-muted">Join two words using _ (underscore) while giving screen name. Eg. new year becomes new_year</small>
        </div>

  <div class="form-group">
    <label for="sections">Total Sections</label>
    <input type="number" step="1" min="3" class="form-control"  name="sections" id="sections">
    <small id="sectionsHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>

      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" value="save" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
   </form>


<div class="modal fade" id="modalConfDelAgent" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Admin\AdminDashboardController@removeAgentFromPnDOrder') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to remove agent from this delivery task?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key2'/> 
        <button type="submit" name="btnRemoveAgent" value="save" class="btn btn-primary btn-sm">Yes</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
 </form>
</div> 
 

@endsection

@section("script")

<script>


$(document).on("click", ".showremmodal", function()
{
  $("#key3").val($(this).attr("data-key"));
  $(".modalrem").modal("show")

}); 
  
 

$('body').delegate('#btnsaveconfirmation','click',function(){
  
   var confirmation = $(this).attr("data-conf"); 
   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");


   $.ajax({
      type: 'post',
      url: api + "v3/web/orders/send-new-pnd-assist-sms" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);   

        if(data.status_code == 7005 ){
          $("#notifstate").html("Yes");
        } 

      },
      error: function( ) 
      {
        $(".loading_span").html(" ");  
      } 

    });

    $("#confirmOrder").modal("hide");     
    

})



</script>  


@endsection 