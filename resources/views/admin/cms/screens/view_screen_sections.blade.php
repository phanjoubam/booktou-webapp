@extends('layouts.admin_theme_02')
@section('content')
   
 <div id="container">
 

   <div class="row">
     <div class="col-md-12">

      <div class="card  ">

        <div class="card-header "> 
          <div class='row'>
            <div class='col-md-10'>
              <h4><b class="text-dark">App Screen Sections for </b> 
                <span class="badge badge-primary badge-pill display-5">{{$app_screen->screen_name }}</span></h4>
            </div>
            <div class='col-md-2 text-right'>
             <button class='btn btn-primary btn-sm ' data-toggle="modal" data-target="#wgt-addsection"><i class='fa fa-plus'></i>

           </button>
         </div>
       </div>
     </div>
     
 <div class="card-body  ">

      @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif

      <table class="table">
  <thead>
    <tr> 
        <th scope="col">Leading Icon</th>               
        <th scope="col">Heading Text</th>
        <th scope="col">Layout Type</th>
        <th scope="col">Content</th>
        <th scope="col">Rows</th>
        <th scope="col">Columns</th>
        <th scope="col">Horizontal Scrolling</th>
        <th scope="col">Show In App</th>
        <th scope="col">Display Order</th>
        <th scope="col">Action</th>


    </tr>
  </thead>
  <tbody> 
    @foreach($sections as $section) 
    <tr>
      <td>
        <img src="{{ $section->leading_icon  }}" width="50px" style="white-space:normal;">
        <button class="badge badge-primary badge-pill btn-update-icon" data-key="{{ $section->id }}" style="margin-left:20px;" >
          <i class="fa fa-pencil"></i>
        </button></td>
      <td style="white-space:normal;">{{$section->heading_text}} 
        </td>                   
      <td>{{$section->layout_type}}</td>
      <td><span class='badge badge-primary badge-pill'>{{$section->content_type}}</span></td>

      <td>{{$section->row}}</td>
      <td>{{$section->col}}</td>
      <td>{{$section->horizontal_scroll}}</td>

      <td>{{$section->show_in_app}}</td>
      <td>{{$section->display_sequence}}</td>

      <td class="text-center">
        <div class="dropdown">
         <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-icon="{{ $section->id }}" 
                data-leading="{{ $section->leading_icon}}">
          <i class="fa fa-cog "></i>
        </a>
        <ul class="dropdown-menu dropdown-user pull-right"> 
          <li><a class="dropdown-item showdialogm1" 
            href="{{ URL::to('/admin/app-screen/sections/manage-contents') }}?sid={{ Request::get('sid')  }}&sectionid={{ $section->id }}" target="_blank" >View Contents</a></li>

          <li><a class="dropdown-item showdialogm1"                
                data-key="{{ $section->id }}" 
                data-layout="{{ $section->layout_type}}"
                data-head="{{ $section->heading_text}}"
                data-row="{{ $section->row}}"
                data-col="{{ $section->col}}"
                data-scroll="{{ $section->horizontal_scroll}}"
                data-display="{{ $section->display_sequence}}"
                data-show="{{ $section->show_in_app}}"              
            >Edit </a></li>
            <li><a class="dropdown-item btndel" data-id="{{$section->id}}" data-widget="del" >Delete</a>
            </li>
          </ul>
        </div>
      </td>
    </tr>

    @endforeach
  </tbody>
</table>

 
 
  </div>
</div> 
  </div>
</div>  
 </div>


<form  method="post" action="{{action('Admin\AppCmsController@addSection')}}" enctype="multipart/form-data">
  {{csrf_field()}}
 <div class="modal fade" id="wgt-addsection" tabindex="-1" aria-labelledby="wgt-addsection" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Section</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Main Module</label>
          <select type="text" class="form-control" name="main_module" >
            <option value="shopping">Shopping</option>
            <option value="booking">Booking</option>
            <option value="appointment">Appointment</option>
          </select>
          </div>
          <div class="form-group col-md-6">
            <label for="sub_module">Sub-module</label>
          <select type="text" class="form-control" name="sub_module" id="sub_module">
            <option value="fnb">FNB</option>
            <option value="ecom">ECOM</option>
          </select>
          </div>
        </div>


        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="inputEmail4">Heading Text Label</label>
            <input class="form-control" type="text"  name="heading_text" required>
          </div>

        </div>

         <div class="form-row">
          <div class="form-group col-md-8">
            <label for="layout_type">Layout Type</label>
            <select  class="form-control selectType" name="layout_type" id="layout_type">
              <option value="image-slider">image-slider</option>
              <option value="icon-grid">icon-grid</option>
              <option value="block-image">block-image</option>
              <option value="square-grid">square-grid</option>
            </select>
          </div>

            <div class="form-group col-md-4">
            <label for="layout_type">Content Type</label>
            <select  class="form-control " name="content_type" id="content_type">
              <option value="product">Shopping Product</option>
              <option value="package">Service Package</option>
              <option value="business">Business Name</option>
              <option value="text">Text</option>
              <option value="image">Image</option>
              <option value="video">Video</option>
            </select>
          </div>

        </div>
         <div class="form-row">
          <div class="form-group col-md-4">
            <label for="inputEmail4">No. of rows</label>
<input class="form-control input-number" type="number"  name="row" min="0" required>
          </div>
          <div class="form-group col-md-4">
            <label for="inputPassword4">No. of columns</label>
      <input class="form-control input-number" type="number" name="col" min="0" required>
          </div>
        </div>

         <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Allow Horizontal Scrolling</label>
      <select  class="form-control" type="text" name="horizontal_scroll" >                                
        <option value="yes">yes</option>
        <option value="no">no</option>
      </select>
          </div>
          <div class="form-group col-md-6">
            <label for="inputPassword4">Show in app</label>
            <select  class="form-control" type="text" name="show_in_app">                                
              <option value="yes">yes</option>
              <option value="no">no</option>
            </select>
          </div>
        </div>
  
      </div>
      <div class="modal-footer">
        <input type="hidden" name="sid"  value="{{ Request::get('sid') }}">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="btnsave" value="save" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</form>



<form action='{{ action("Admin\AppCmsController@updateSectionLeadingIcon") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='wgt-edticon' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Update Leading Icon</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
           <div class="row ">            
             <div class="col-md-6">
              <label for="inputState" class="form-label">Select-Image</label>
              <input class="form-control" type="file" id="photo" name="photo">
            </div>
          </div> 
        </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type="hidden" name="sid"  value="{{ Request::get('sid') }}">
        <input type="hidden" name="sectionid" id="wgt-edticonkey"  >
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Upload Icon</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>


@endsection

@section("script")

<script>


 $(document).on("click", ".btn-update-icon", function()
 {
  $("#wgt-edticonkey").val($(this).attr("data-key"));
  $("#wgt-edticon").modal("show");
});

  



</script>  


@endsection 