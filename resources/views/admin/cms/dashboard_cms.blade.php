@extends('layouts.admin_theme_02')
@section('content')
  
 

 @foreach($data['main_menu_items'] as $item)
  <div class="row">
     <div class="col-md-3 col-sm-12 col-xs-12"> 
             <div class="card">
            <div class="card-body">
                <div class='flex-container'>
                   <div class=''>
                     <span><i class='fa fa-plus'></i> {{ $item->menu_name }}</span> 
                   </div>
                   <div class=''>
                      <span class='badge badge-primary'>{{ ( $item->has_sub_menu == "yes" ? "sub-menu present" : "" ) }}</span> 
                   </div>
                   <div class=''>
                      <button class='btn btn-info btn-xs'><i class='fa fa-plus'></i></button> 
                  </div>  
               </div>   
               </div>
           </div>  

            @foreach($data['sub_menu_items'] as $sub_item)  
              @if($item->id  ==  $sub_item->menu_id)  
                  <div class="card ml-5">
                  <div class="card-body">
                              <span><i class='fa fa-plus'></i> {{ $sub_item->product_category }}</span>   
                         </div>
                  </div> 
              @endif
            @endforeach



    </div>

  </div>
  @endforeach

 

@endsection