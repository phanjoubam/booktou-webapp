@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>

 <div class="row">  
  
  <div class="col-md-4">    
        <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">
                  <div class="col-md-12">
                    Upload New Slider Image 
                  </div> 
       </div> 

                </div>
            </div>
       <div class="card-body">   
   <div class="row"> 
      <div class="col-md-12">  
  
    <form  action="{{  action('Admin\AdminDashboardController@uploadAppBanner') }}" enctype="multipart/form-data"   method="post">
      {{ csrf_field() }}

      <div class="form-row"> 
        <div class="form-group col-md-12">
          <label for="body">Business</label>
          <select class="form-control"  name='bin' >
            @foreach($data['businesses'] as $item)
              <option value='{{ $item->id}}'>{{ $item->name }}</option>
            @endforeach 
          </select>
        </div>

   </div>

  
    <div class="form-row"> 
  <div class="form-group col-md-12">
    <label for="body">Caption</label>
    <textarea   class="form-control" id="caption" name='caption' placeholder="Slider caption"></textarea>
  </div>

   </div>

   <div class="form-row">
    <div class="form-group col-md-12">
      <label for="url">Select Image:</label>
      <input type="file"  id="photo" name='photo'>
    </div> 
  </div>   
   <button type="submit" class="btn btn-success"  >Upload Slider</button> 
</form>


    </div> 
 </div> 
</div>  
</div> 
</div> 


    @foreach ($data['slides'] as $item)
    <div class="col-md-4">    
        <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">
                  <div class="col-md-10">  

                    {{$item->caption}}
                  </div>
                  <div class="col-md-2">    


                    <div class="dropdown">
          <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" 
          role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-cog "></i>
        </a> 
          <ul class="dropdown-menu dropdown-user pull-right"> 
            <li><a class="dropdown-item btnDelete" href='#' data-key="{{  $item->id }}" ><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
     </div>
      </div>
       </div>


                </div>
            </div>
       <div class="card-body">   
   <div class="row"> 
      <div class="col-md-12">  
   
     <?php 
      if($item->path=="")
      { 
         $banner = $cdn_url . "/assets/image/no-image.jpg" ; 
      }
      else
      {
        $banner = $cdn_url . $item->path   ;  
      }
      
     ?>

      <img src='{{ $banner }}' alt="{{$item->caption}}"  width="100%"> 
                </div>
     <div class="col-md-12">   
    <!--   {{$item->bin }}     
        {{$item->sequence }}
    -->
<ul class="list-group list-group-flush">
   <li class="list-group-item d-flex justify-content-between align-items-center">
      Slider Item Is Visible
       <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input btnToggleVisibility"   data-key='{{$item->id }}' id="blockSwitchON{{ $item->id }}" 
            <?php  if($item->status == "1") echo "checked";  ?>  />
            <label class="custom-control-label" for="blockSwitchON{{$item->id }}">Yes</label>
          </div> 
    </li>
</ul>

        
        
    </div> 
 </div> 
</div>  
</div> 
</div> 

  @endforeach
  
  
  </div> 
 
 
 <div class="modal fade" id="modalConfirmSlider" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Admin\AdminDashboardController@disableAppBanner') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to disable this banner from slider list?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key1'/>
        <input type='hidden' name='status' id='status'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>



<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Admin\AdminDashboardController@deleteAppBanner') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to permanently delete this banner from slider list?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key2'/> 
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>

 
@endsection



@section("script")

<script>




$(document).on("click", ".btnDelete", function()
{

    $("#key2").val($(this).attr("data-key"));
    $("#modalConfirmDelete").modal("show")

});


$(document).on("click", ".btnDisableSlide", function()
{ 
    $("#key1").val($(this).attr("data-key"));
    $("#modalConfirmSlider").modal("show")

});


$(document).on("change", ".btnToggleVisibility", function()
{
    var status   = $(this).is(":checked");
    $("#key1").val($(this).attr("data-key"));
    $("#status").val( status );  
    $("#modalConfirmSlider").modal("show") 

})

 
  

</script>


@endsection

