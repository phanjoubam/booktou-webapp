@extends('layouts.admin_theme_02')
@section('content')

 
  <div class="row">
     <div class="col-md-8"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Business Categories</div> 
                </div>
 <div class="col-md-4 text-right"> 
        
      </div>

       </div>
                </div>
            </div>
       <div class="card-body"> 

       @if (session('err_msg'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg') }}
      </div>
      </div>
    @endif


<div class="table-responsive" >
    <table class="table">
         <thead class=" text-primary">
      		<tr >
      		<th scope="col" class="text-center"></th>
      		<th scope="col" class="text-left">Category</th>  
      		<th scope="col" class="text-left">Module</th>
          <th scope="col" class="text-left">Shop count</th>
      		<th scope="col" class="text-left">Nearby</th>
          <th scope="col" class="text-left">Is Public</th>
          <th></th>
      		</tr>
      	</thead>
	
		<tbody>

			 <?php $i = 0 ?>
		@foreach ($data['results'] as $item) 
		 <tr id='tr-{{ $item->id }}'>
		 <td class="text-center"> 
      <img src='{{$item->icon_url}}' alt="{{$item->name}}" width="50px"> 
                </td>
		 <td class="text-left">{{$item->name }}</td>  
		 <td class="text-left">{{$item->main_type}}</td>
     <td class="text-left">{{$item->total_biz}}</td>
     <td class="text-left">{{$item->list_by_nearby}}</td>
     <td><div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input btnToggleBlock"   data-keys='{{$item->id }}' id="blockSwitchON{{ $item->id }}" 
            <?php if($item->is_public == "yes") echo "checked";  ?>  />
            <label class="custom-control-label" for="blockSwitchON{{$item->id }}"></label>
          </div>
                      </td>

      
		 <td class="text-center">
        <div class="dropdown">
            <a class="btn btn-primary btn-sm btn-icon-only text-light " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-cog "></i>
            </a> 
            <ul class="dropdown-menu dropdown-user pull-right"> 
                   <li><a class="dropdown-item btnEdit" href="#" data-rowid='{{ $item->id }}'   >Edit</a></li>
                   <li><a class="dropdown-item btnUpload" href="#" 
                    data-catid='{{$item->id}}'   
                    data-catname='{{$item->name}}'>Upload Photo</a></li>
                   <li><a class="dropdown-item btnDel"  data-key='{{ $item->id }}' 
                    data-widget="confirmdel"  href="#">Delete</a></li> 
                 
            </ul>
         </div>          
        </td>
		 </tr>
	@endforeach
	</tbody>
		</table>
	
		
	</div>
 </div> 
  </div>  
	</div> 
 
   <div class="col-md-4">
   <div class="row">
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Business Categories</div> 
                </div>
 <div class="col-md-4 text-right"> 
        
      </div>

       </div>
                </div>
            </div>
  <div class="card-body">   
    
    <form action="{{ action('Admin\AdminDashboardController@saveBusinessCategories') }}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }} 
      <div class="card-body ">
    <div class="row"> 
    @if (session('err_msg_right'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg_right') }}
      </div>
      </div>
    @endif
    </div>
    <div class="form-group row">
    <div class="col-md-12"> 
    <label class="control-label">Category Name:</label>
    <input type="text" required name='tb_bizcategory' id='tb_bizcategory' class="form-control boxed">
    </div>
   
      <div class="col-md-12"> 
      <label class="control-label">Main Business Type:</label>
      <select  required name='db_maintype' id='db_maintype' class="form-control boxed">
        <option value='Shopping'>Shopping</option>
        <option value='Appointment'>Appointment</option> 
      </select>
      </div>
    </div>  
                      <div class="form-row">
                      <div class="col"> 
                      <div class="form-group">  
                      <br/>
                        <input type="hidden"   name='tb_oldbizcategory' id='tb_oldbizcategory'  > 
                        <button type="submit" id='btnSave' name='btnSave' value='save' class="btn btn-primary">Save</button>
                        <button type="submit" name='btnCancel' value='save' class="btn btn-danger">Cancel</button>
                      </div>
                    </div> 
                      </div>
                      
               
         </div>
     
     </form>


 </div> 
  </div>  
  </div> 


<div class="row mt-3">
 <div class="card  ">
    <div class="card-header"> 
      <div class='row'>
        <div class='col-md-10'>
          <h4 class="card-title text-center">Meta Key Configuration</h4>
        </div>
        <div class='col-md-2 text-right'>
         <button class='btn btn-primary btn-sm add_metakey' data-toggle="modal" data-target="#modalepg" 
        ><i class='fa fa-plus'></i>
   
         </button>
        </div>
      </div>
    </div>
    <div class="card-body text-center">
      <strong>Meta key list:</strong>
      <div class="table-responsive table table-bordered ">
        <table class="table"> 
          <thead class=" text-primary">
            <tr >
              <th scope="col">Sl no.</th>
              <th scope="col">Meta key</th>
              <th scope="col">Meta key text </th>
              <th scope="col">Edit </th>  
            </tr>   
          </thead> 
          <tbody>
              @php
              $slno=1;
              @endphp
          @foreach($data['metakeys'] as $meta)         
            <tr>
              <td>{{$slno}}</td>       
              <td >{{$meta->meta_key}}</td>
               <td>{{$meta->meta_key_text}}</td>
              <td>
               <div class="dropdown">
                <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog "></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item metakeyEdit"  data-widget="edit"
                    data-key="{{$meta->id}}" 
                   data-metakey="{{$meta->meta_key}}"
                   data-metatext="{{$meta->meta_key_text}}"
                  >Edit</a>
                  <a class="dropdown-item metakeyDelete" data-id="{{$meta->id}}" data-widget="del">Delete</a>
                </div>
              </div>
              </td>          
            </tr>
            <?php
            $slno++;
            ?>
         @endforeach
          </tbody>
        </table>
      
      </div>   
    </div>
  </div>  
</div>

</div> 
</div>

<form action='{{ action("Admin\CarRentalServiceController@addMetaKey") }}' method='post' enctype="multipart/form-data">
  {{ csrf_field() }}
 <div class="modal" id='addMetaKey' tabindex="-1" role="dialog" >
  <div class="modal-dialog  modal-dialog-centered" >    
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title">Add technical specification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >        
        <div class="row ">         
         <div class="col-md-6">
          <label class="form-label">Meta Key</label>
          <input type="text" name="metakey"  class="form-control">
         </div>
         <div class="col-md-6">
          <label class="form-label">Meta Key Text</label>
          <input type="text" name="metakeytext"  class="form-control">
         </div>                 
      </div>

      <div class="modal-footer mt-1"> 
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  
  </div>
</div>
</div>
</form> 
<form action='{{ action("Admin\CarRentalServiceController@editMetaKey") }}' method='post' enctype="multipart/form-data">
  {{ csrf_field() }}
 <div class="modal" id='metaEdit' tabindex="-1" role="dialog" >
  <div class="modal-dialog  modal-dialog-centered" >    
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title">Edit Meta Key Configuration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >        
        <div class="row "> 
         <div class="col-md-6">
          <label class="form-label">Meta Key</label>
          <input type="text" name="meta_key" id="meta_key" class="form-control">
         </div>
         <div class="col-md-6">
          <label class="form-label">Meta key text</label>
          <input type="text" name="meta_keytext" id="meta_keytext" class="form-control">
         </div>                 
      </div>
      <div class="modal-footer mt-1"> 
        <input type='hidden' name='keys' id='keys' />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  
  </div>
</div>
</div>
</form>
<div class="modal fade" id="metadelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <form method='post' action="{{ action('Admin\CarRentalServiceController@deleteMetaKey') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to delete this business category?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='metadel' id='metadel'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
        <button type="submit" name="btnDel" value="delete" class="btn btn-danger btn-sm">Delete</button>
      </div>
    </div>
  </div>
 </form>
</div>

<div class="modal fade" id="widget-confirmdel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <form method='post' action="{{ action('Admin\AdminDashboardController@removeBusinessCategories') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to delete this business category?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
        <button type="submit" name="btnDel" value="delete" class="btn btn-danger btn-sm">Delete</button>
      </div>
    </div>
  </div>
 </form>
</div>


<div class="modal fade" id="widget-photo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<form method='post' action="{{action('Admin\AdminDashboardController@uploadBusinessCategoriesPhoto')}}"
       enctype="multipart/form-data">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload Photo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="row">
          <div class="col-md-12">
            <label class="control-label">Category Name:</label>
            <p id="catname" class="badge badge-primary"></p>
          </div>
          <div class="col-md-12">
          <div class="card card-body">
            <label class="control-label">Category Photo:</label>
            <input type="file" required name='photo' id='photo' class="form-control">
          </div>
         </div>
       </div>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='catid' id='catid'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary btn-sm">Upload</button>
      </div>
    </div>
  </div>
 </form>
</div>

@endsection
@section("script")

<script>
   $(document).on("click", ".add_metakey", function()
  {
    
    $("#addMetaKey").modal("show");
  });

  $(document).on("click", ".metakeyEdit", function()
  {   
    $("#keys").val( $(this).attr("data-key"));
    $("#meta_key").val( $(this).attr("data-metakey"));
    $("#meta_keytext").val( $(this).attr("data-metatext")); 
    $("#metaEdit").modal("show")
  });
  $(".metakeyDelete").on("click", function()
  {
    $("#metadel").val($(this).attr("data-id"));
    $("#metadelete").modal("show");
  });

  $(document).on("click", ".btn_add_promo", function()
  {
    $("#bin").val($(this).attr("data-bin"));
    $("#modal_add_promo").modal("show");
  });
  $(".btnEdit").on('click', function(){
      $rowid = $(this).attr("data-rowid");  
      $("#tb_bizcategory").val($("#tr-" + $rowid + " td:nth-child(2)").html() ); 
      $("#tb_oldbizcategory").val($("#tr-" + $rowid + " td:nth-child(2)").html() ); 
      $("#db_maintype").val($("#tr-" + $rowid + " td:nth-child(3)").html() );   
 
      $("#btnSave").val('update');
      $("#btnSave").html('Update');

  });
      
    
    
  $(".btnDel").on('click', function(){ 
      var key  = $(this).attr("data-key");  
      $("#key").val(key); 
      $widget  = $(this).attr("data-widget");  
      $("#widget-" + $widget).modal('show'); 
  });
    
  $(".btnUpload").on('click', function(){ 
      var key  = $(this).attr("data-catid");  
      $("#catid").val(key); 
      $("#catname").text($(this).attr("data-catname"));
      $("#widget-photo").modal('show'); 
  });


  $('body').delegate('.btnToggleBlock','click',function(){
  
   var id = $(this).attr("data-keys"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['id'] =  id ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/category/is-public-toggle" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})
  
</script>

@endsection

