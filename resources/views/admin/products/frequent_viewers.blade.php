@extends('layouts.admin_theme_02')
@section('content')
<?php 
  $cdn_url =    config('app.app_cdn') ;  
  $cdn_path =     config('app.app_cdn_path') ;  
?>

  
   <div class="row">
     <div class="col-md-8">  
     <div class="card card-primary">
 
             <div class="card-header">
                  <div class="title">{{ $data['title'] }}</div>
                </div>
          
<div class="card-body">
<div class="table-responsive">
    <table class="table">
                    <thead class="text-primary">  
                  <tr>   
                    <th scope="col"  width='250px'>Full Name</th> 
                    <th scope="col">Phone</th> 
                    <th scope="col">View Count</th> 
                    <th scope="col" class='text-center'>Action</th>  
                  </tr>
                </thead>
                <tbody>
        @foreach($data['view_logs'] as $view_item)  
          <?php 

                  $i=1; 
                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['profiles'] as $item)
                {
                ?>
                @if($item->id == $view_item->member_id ) 
                  <tr >  
                 
                  
                   <td width='250px'>
                    <strong>{{$item->fullname}}</strong><br/>  
                   {{ $item->locality}}, {{ $item->landmark}}        
                  </td> 
                    <td>{{  $item->phone }}</td>  
                  <td>{{  $view_item->viewCount }}</td>  
                    <td class="text-center">
                      <div class="dropdown"> 
                        <a class="btn btn-primary btn-sm btn-icon-only text-light " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-cog "></i>
                        </a> 
                        <ul class="dropdown-menu dropdown-user pull-right"> 
                           @if($item->firebase_token !=  "")
                              <li> <a class="dropdown-item btn btn-notify" data-id="{{ $item->id }}">Post Message</a></li>
                          @endif
                          <li><a class="dropdown-item btn" href="{{  URL::to('/admin/customer/view-complete-profile/'.$item->id )}}">View Profile</a></li> 
                        </ul>
                    </div>          
                </td> 
              </tr>
                @break
               @endif 
              <?php
              $i++; 
            }
      ?>
       
    @endforeach
    </tbody>
  </table> 
  {{ $data['profiles']->appends(request()->input())->links() }}  
</div>
            </div>
  </div>
          </div>
    <div class="col-md-4">  

      <?php   
    
      $first_photo  = $cdn_url . "/assets/image/no-image.jpg" ;

      $all_photos = array(); 
      $files =  explode(",",  $data['product']->photos ); 

      if(count($files) > 0 )
      {
          $files=array_filter($files);
          $folder_path =  $cdn_path ."/assets/image/store/bin_" .   $data['product']->bin .  "/";
          foreach($files as $file )
          {
            if(file_exists(  $folder_path .   $file ))
            {
              $first_photo = $cdn_url .   "/assets/image/store/bin_" .   $data['product']->bin .  "/"  .    $file  ;
              break;
            } 
          } 
      } 
  
           
      ?> 
  

<div class="card"  >
  <div class="card-body">
    <h2 class="h4" >Product Viewed</h2>
    <hr/>

      <div class="media">
  <img src="{{ $first_photo   }}" width='50px' class="mr-3" alt="{{ $data['product']->pr_name }}">
  <div class="media-body">
    <h4 class="mt-0">{{ $data['product']->pr_name }}</h4>
    {{ $data['product']->description }}
    <br/>

    <a href="{{ URL::to('/shop/view-product-details/whole-roast-chicken') }}/{{ $data['product']->pr_code }}" target="_blank">View in Store Front</a>
  </div> 

  </div>
</div>




     
</div>




    </div>
   </div>
 
    
 

 <div class="modal" id='notifyModal' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Send Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="height: 400px;overflow-y: scroll;">
         
    <div class="table-responsive">
    <table class="table">
                    <thead class=" text-primary">  
                  <tr class="text-center" style='font-size: 12px'>  
                     <th scope="col"></th>
                    <th scope="col">Title</th>
                    <th scope="col">Body</th> 
                    <th scope="col" class='text-center'></th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 
 
                
               
                          ?>
                </tbody>
                  </table>
            
              </div>

  
   <input type="hidden" id='hidcid' name='hidcid' /> 
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span> 
        <button type="button" class="btn btn-success" id="btn-notify" data-conf="" data-id="" >Send Message</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
 


  </div>
</div>
 



@endsection

@section("script")

<script>

 
 
$(document).on("click", ".btn-notify", function()
{
  $cid = $(this).attr('data-id');

  $("#hidcid").val($cid); 
  $("#notifyModal").modal("show") 


});





$('body').delegate('#btn-notify','click',function()
{

  var cmid ; 
  $('.cmids').each(function() 
  {
    if($(this).is(':checked'))
    {
      cmid =  $(this).val();
    }
  });

  var mid = $("#hidcid").val(); 
 

  if(cmid > 0 && mid > 0)
  {
    var json = {}; 
    json['cmid'] = cmid ;
    json['mid'] = mid ; 
    $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $.ajax({
        type: 'post',
        url: api + "v2/web/customer-care/notification/send-cloud-message" ,
        data: json,
        success: function(data)
        {
          data = $.parseJSON(data);   
          $(".loading_span").html(" ");
          $("#notifyModal").modal("hide") 
        },
        error: function( ) 
        {
          $(".loading_span").html(" ");
          alert(  'Something went wrong, please try again'); 
        } 

      });
  }
  else 
  {
    alert("No receipt or message selected!");
  } 

})



</script> 

@endsection 