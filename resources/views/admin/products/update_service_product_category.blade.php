@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; // cnd - /var/www/html/api/public
?>

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card panel-default">
           <div class="card-header ">
              <div class="card-title">  
          <div class="title">Manage Service Categories</div>
        </div> 
 </div> 

  <div class="card-body"> 
             
              @if (session('err_msg'))
             
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
               
              @endif 

              <form method='post' action="{{ action('Admin\AdminDashboardController@updateServiceCategory') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                 
                <div class="pl-lg-4"> 
                  <div class="row">
                  <div class="col-lg-8">
                      <div class="row">
                      

                     <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="busi_category">Sub Module:</label>
                      <select id="service_category" class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='submodule'> 
                        @foreach ($data['booktable_categories'] as $item)
                        <option value='{{ $item->sub_module }}'>{{ $item->sub_module_name }}</option>  
                        @endforeach

                 </select>

                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="category_name">Service Category Name:</label>
                        <input type="text" id="category_name" name='category' class="form-control form-control-alternative" placeholder="Enter Category Name" value="">

                      </div>
                    </div>


                    </div>

                    <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">  
                      <label class="form-control-label" for="category_description">Description:</label>
                      <textarea type="text" id="category_description" class="form-control form-control-alternative" placeholder="Type some description here" name="description"></textarea>
                      
                      </div>
                    </div>
                  </div>

                  </div>
                  <div class="col-lg-3">
                
                    <div class="form-group">
                    <img id="catimage" src="{{ $cdn_url }}/assets/image/no-image.jpg"  alt="..." height="160px" width="250px">
                    </div>

                    <div class="form-group text-center">
                     <label class="form-control-label" for="category-icon">Upload Icon</label> 
                      <input id="input-icon" class="form-control"
                      name="photo" type="file" />
                      </div>
                  </div> 
                  </div> 
                   <input type="hidden" name="catid" id="catid">  
                  <hr/>
                  <button type="submit"  name='btnsave' class="btn btn-primary"  value='save'>Save</button>
                 
              </div>
 
                 
              </form>
            </div>
          </div>
 
            <div class="card card-default mt-5"> 




              <div class="card-body"> 
                 <div class="card-title">
                <div class="row">
                  <div class="col-lg-7">
                    <h5>Service Categories</h5>
                  </div>
                  <div class="col-lg-5 text-right">
                     

                  </div>
                </div>
                     
            </div>



                    <div class="table-responsive"> 
                  <table class="table">
                    <thead class=" text-primary"> 
 
                  <tr>  
                    <th scope="col">Icon</th>
                    <th scope="col">Category Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Business Category</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
              <tbody>
                  <?php $i = 0;  ?>

                  @foreach ($data['service_categories'] as $item)

                 <?php $i++ ?>

                  <tr > 
                      
                   <td class="text-center">
   

                      <img src='{{ $item->icon_url }}' id="input-photo-tag"   height="40px" width="40px">

                  </td>
                    <td>
                      {{$item->category}}
                  </td>
                    <td>
                       {{$item->cat_des}}
                  </td>
                  <td>
                       {{$item->bookable_category}}
                  </td>
                   
                   <td>

                    <div class="dropdown"> 
                       <a class="dropdown-toggle btn btn-primary btn-sm" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-cog fa-fw"></i> 
                    </a>
                    <ul class="dropdown-menu">  
                      <li><a href='#' class="dropdown-item btn_edit" data-id="{{$item->id}}" data-desc="{{$item->cat_des}}" data-name="{{$item->category}}" data-url="{{ $item->icon_url }}"  data-cat="{{$item->bookable_category}}"   >Edit</a></li>
                        <li><a href='#' class="dropdown-item btn_del_category" data-id="{{$item->id}}"  >Delete</a></li>
                     </ul>
                      </div>
                    </td>

                         </tr>
             @endforeach
                </tbody>
                  </table>
             </div>
              </div>
            </div>



          </div>
        </div>



<div class="modal fade" id="del_category" tabindex="-1" role="dialog" aria-labelledby="del_box" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="del_box"><strong>Confirm Service Category Removal</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <p>This operation is not recoverable. If there are products under it, delete operation will fail. Are you sure about this operation?</p>
      </div>
      <div class="modal-footer">
        <form method='post' action="{{ URL::to('/admin/services/category/remove') }}">
       {{ csrf_field() }}
     <input type='hidden'  id='hidcid' name='key'/>
     <button class="btn btn-danger"  name='btn_confirm' value="delete">Delete</button>
      <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
    </form>
      </div>
    </div>
  </div>
</div>



   @endsection  

@section("script")
         <!--Upload Image/File script-->
                <script type="text/javascript">
                  function readURL(input) {
                    if (input.files && input.files[0]) {
                      var reader = new FileReader();
            
                    reader.onload = function (e) {
                        $('#catimage').attr('src', e.target.result);
                    }
                     reader.readAsDataURL(input.files[0]);
                   }
                 }
                 $("#input-icon").change(function(){
                  readURL(this);
                });
              

 

$(document).on("click", ".btn_edit", function()
{
  var id = $(this).attr("data-id"); 
  var name = $(this).attr("data-name"); 
  var url = $(this).attr("data-url"); 
  var desc = $(this).attr("data-desc");
  var category = $(this).attr("data-cat");
 

    $("#catid").val(id);
    $("#category_name").val(name);
    $("#catimage").attr("src",  url); 
    $("#category_description").val(desc);
    $("#service_category").val(category);


})





$(document).on("click", ".btn_del_category", function()
{
    var id = $(this).attr("data-id");
     $("#hidcid").val( id ); 

    $("#del_category").modal("show"); 

})


 </script>
             

@endsection
