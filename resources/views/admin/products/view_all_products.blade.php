@extends('layouts.admin_theme_02')
@section('content')


<div class="row">


 <div class="col-md-12">

   @if (session('err_msg'))
             
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
               
              @endif 
        
        <div class="card card-default">
       <div class="card-header">
       <div class="row">
          <div class="col-md-3"> 
                <h4 class="card-title ">Product Lists</h4>
       </div>

       <div class="col-md-9">
       <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@viewAllProducts') }}"> 
            {{ csrf_field() }}
            <label for="inlineFormCustomSelectPref">Product Name/Code:</label> 

             <input  type="text" class='form-control form-control-sm my-1 mr-sm-2 '  
             value="" name='search_filter' placeholder="Type here" />
              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Business Category:</label> 
              <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='search_key'>

                  <option value='all'>All</option> 
                
                    @foreach ($data['businessCategory'] as $itemBC)

                    <option value='{{$itemBC->name}}'>{{$itemBC->name}}</option> 
                 @endforeach

                </select>
 
                <button type="submit" class="btn btn-primary my-1" value='search' name='btnSearch'>Search</button>
            </form>
       </div>
       </div>
       
       </div> 
  
    <div class='card-body'>
  
  <div class="flex-container">
  @php 
  $position = 0 
  @endphp 
    @foreach ($data['products'] as $item)
    
 <div class="flex-child card-product-image"   data-pos="{{ $position }}"> 
    
    <?php  
      $first_photo  = URL::to("/public/assets/image/no-image.jpg");

      $all_photos = array(); 
      $files =  explode(",",  $item->photos ); 

      if(count($files) > 0 )
      {
          $files=array_filter($files);
          $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .  "/";
          foreach($files as $file )
          {
            if(file_exists( base_path() .  $folder_path .   $file ))
            {
              $first_photo =  URL::to( $folder_path  ) . "/" .    $file  ;
              break;
            } 
          } 
      } 
           
      ?>
 
     
          

      <img src="{{ $first_photo   }}" alt="{{$item->pr_name}}"   height='200px'  >  
      <div class='rowd'>
      <div class='col-md-12d ml-ld text-center'>
      <strong class="text-center">{{$item->pr_name}}</strong><br/>  

 
  <div class="hover-menu" id="menu-{{  $position }}"> 
    <ul class="nav nav-pills justify-content-center">
  <li class="nav-item">
    <a class="nav-link  btnModal001" href="#" data-pid="{{$item->id}}" href='#' >Add Promo</a>
  </li>
  <li class="nav-item">
    <a class="nav-link showTaskList" href="#" data-pname="{{$item->pr_name}}" data-img="{{$first_photo}}" 
                            data-desc="{{$item->description}}"
                            data-stck="{{$item->stock_inhand}}"
                            data-price="{{$item->unit_price}}"
                            data-cat="{{$item->category}}"
                            href="#">View Details</a>
  </li>
 
</ul>
 </div> 

</div>
</div> 
  </div> 
 
 
 @php 
  $position++
  @endphp 
  @endforeach 
</div>

 <div class="row">
     <div class="col-md-12">
<hr/>
        {{    $data['products']->appends(request()->input())->links()  }}


     </div>

  </div> 
  </div>
</div>

</div>
</div>



<!--Add promo modal -->
<div class="modal fade" id="dialogModal001" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

   <form method='post' action="{{ action('Admin\AdminDashboardController@saveAsFeatureProduct') }}" enctype="multipart/form-data">
  {{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p class="text-center">
          This is final step for add product to promotion lists. No rollback possible.
          Are you sure about this operation? 
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='pr_id' id='pr_id'/>
       <button type="submit" name="btnSave" value="save" class="btn btn-primary">Add</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button> 
      </div>
    </div>
  </div>
</form>

</div>


<!--Product details modal -->

<div class="modal fade" id="modalTaskList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
  
<h5 class="modal-title" id="exampleModalLongTitle">Product Details</h5>

 
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content_area">
         <div class='loading'>


          <div class="row">
            <div class="col-md-6">
         <div>Product Name : <strong id="pname"></strong> </div>
         <div>Description : <strong id="desc"></strong> </div>
         <div>Current Stock : <strong id="stck"></strong> </div>
         <div>Unit Price : <strong id="price"></strong> </div>
         <div>Category : <strong id="cat"></strong> </div>
 
              </div> 
               <div class="col-md-6">
                
                  <div class="profile-pic">
                  <img src='{{ URL::to("/public/assets/image/no-image.jpg") }}' id='apimg' height="150px" width="250px" class="img-rounded" style='margin-right:10px; display:inline-block'/>
                
                </div>
              </div> 
  
            </div>

        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



@endsection

@section("script")

<script>

  $(document).on("click", ".btnModal001", function()
{
    var pid = $(this).attr("data-pid");
     $("#pr_id").val( pid );

    $("#dialogModal001").modal("show"); 

});


$(document).on("click", ".showTaskList", function()
{
    var pname = $(this).attr("data-pname");
    var img = $(this).attr("data-img");
    var desc = $(this).attr("data-desc");
    var stck = $(this).attr("data-stck");
    var price = $(this).attr("data-price");
    var cat = $(this).attr("data-cat");
     $("#pname").text( pname );
     $("#desc").text( desc );
     $("#stck").text( stck );
     $("#price").text( price );
     $("#cat").text( cat );

    $("#apimg").attr('src', img); 

    $('#modalTaskList').modal('show'); 
});


$(".hover-menu").hide();
 
$('.card-product-image').on('mouseover',function(){ 
    var pos = $(this).attr("data-pos"); 
    $(this).find("#menu-" + pos ).show();
});

$('.card-product-image').on('mouseout',function(){ 
    var pos = $(this).attr("data-pos");
    $(this).find("#menu-" + pos ).hide();
});

</script>


@endsection  
