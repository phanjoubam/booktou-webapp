@extends('layouts.admin_theme_02')
@section('content')
 

 <div class="row">
   <div class="col-md-12"> 

      @if (session('err_msg'))
        <div class="alert alert-info">
            {{ session('err_msg') }}
        </div>
      @endif 

    <div class="card card-default">
      <div class="card-header">
        <div class="row">
         <div class="col-md-7">
           <h5>{{ $data['title'] }}</h5>
         </div>

         <div class="col-md-5 text-right"> 
                  <form class="form-inline" method="post" action="{{  URL::to('/admin/customer-care/business/products/view-promotion-lists')}}">
                      {{ csrf_field() }}
              <label for="inlineFormCustomSelectPref">Todays Deal:</label> 
            <select   class='form-control form-control-sm custom-select my-1 mr-sm-2'   name='search_key'>
              <option value='all'>--Select Any--</option> 
              <option value='yes'>Yes</option> 
              <option value='no'>No</option> 
            </select>
                        <button type="submit" class="btn btn-primary btn-xs my-1" value='search' name='btn_search'>Search</button>
                  </form>
   
             </div>
      </div> 
   
  </div>


  <div class="card">
    <div class="card-body">

  <div class="table-responsive">
  <table class="table" style='width: 100%;  '> 
    <thead class=" text-primary">
      <tr >
        <th >Image</th>
        <th style='width: 450px'>Item </th>  
        <th >Price</th>
        <th >Category</th> 
        <th>View Count</th>
        <th >Action</th>
      </tr>
    </thead> 
    <tbody> 
    @foreach ($data['promotionProducts'] as $itemP) 
     <tr >
     <td>

    <?php  
 
    
     $cdn_url =    config('app.app_cdn') ;  
     $cdn_path =     config('app.app_cdn_path') ;  

      $first_photo  = $cdn_url . "/assets/image/no-image.jpg" ;

      $all_photos = array(); 
      $files =  explode(",",  $itemP->photos ); 

      if(count($files) > 0 )
      {
          $files=array_filter($files);
          $folder_path =  $cdn_path ."/assets/image/store/bin_" .   $itemP->bin .  "/";
          foreach($files as $file )
          {
            if(file_exists(  $folder_path .   $file ))
            {
              $first_photo = $cdn_url .   "/assets/image/store/bin_" .   $itemP->bin .  "/"  .    $file  ;
              break;
            } 
          } 
      } 
  
           
      ?> 

     <img src="{{ $first_photo   }}" alt="..." height="50px" width="50px"> 
<br/><span class='badge badge-primary'>{{ $itemP->id }}</span>
    </td>
     <td style='width: 450px'>
@foreach ($data['businesses'] as $business) 
      
      @if($business->id == $itemP->bin )
      <span class='badge badge-primary'>{{ $business->name }}</span><br/>
      @break
      @endif

      @endforeach 
      {{ $itemP->pr_name }}  

    </td> 
     
      <td>{{$itemP->unit_price}}</td>
   <td>{{$itemP->category}}</td>
   <td>{{ $itemP->viewCount }}</td>
      <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow text-center">  
        <a class="dropdown-item" data-pid="{{$itemP->id}}" href="{{ URL::to('/admin/analytics/products/most-viewed-customers') }}?prid={{  $itemP->id }}">Who Viewed this?</a>  
                               
                        </div>
                      </div>
                  
        </td>
 
    </tr>
    @endforeach
   </tbody>

  </table>


   <div class="d"> 

        {{    $data['promotionProducts']->appends(request()->input())->links()  }}
 
     </div> 


 </div>

  </div>

   
</div>    
  
   </div> 
 </div>

</div>



<!--delete promo modal -->
<div class="modal fade" id="dialogModal001" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

   <form method='post' action="{{ action('Admin\AdminDashboardController@deleteFeaturedProduct') }}" enctype="multipart/form-data">
  {{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p class="text-center">
          This is final step for delete product from featured lists. Are you sure about this operation? 
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='promo_id' id='promo_id'/>
       <button type="submit" name="btnDelete" value="delete" class="btn btn-primary">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        
      </div>
    </div>
  </div>
</form>

</div>

<!--Add todays deal modal -->
<div class="modal fade" id="dialogModal002" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

   <form method='post' action="{{ action('Admin\AdminDashboardController@updateTodaysDeal') }}" enctype="multipart/form-data">
  {{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p class="text-center">
          You selected a product to promote as Today's Deal.
          Are you sure about this operation? 
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='promo_id' id='pid'/>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
       
      </div>
    </div>
  </div>
</form>

</div>

<!--Add todays deal modal -->
<div class="modal fade" id="dialogModal003" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

   <form method='post' action="{{ action('Admin\AdminDashboardController@updateTodaysDeal') }}" enctype="multipart/form-data">
  {{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p class="text-center">
          You have selected to remove a product from today's deal list. Are you sure about this operation?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='promo_id' id='pr_id'/>
        <button type="submit" name="btnUpdate" value="update" class="btn btn-primary">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
</form>

</div>



@endsection

@section("script")

<script>

  $(document).on("click", ".btnModal001", function()
{
    var pid = $(this).attr("data-pid");
     $("#promo_id").val( pid );

    $("#dialogModal001").modal("show"); 

});

  $(document).on("click", ".btnModal002", function()
{
    var id = $(this).attr("data-id");
     $("#pid").val( id );

    $("#dialogModal002").modal("show"); 

});

  $(document).on("click", ".btnModal003", function()
{
    var id = $(this).attr("data-prid");
     $("#pr_id").val( id );

    $("#dialogModal003").modal("show"); 

});

</script>



     

@endsection

 
  
 