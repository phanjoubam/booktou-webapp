<?php
                    $advance = 0;
                    $difference =0;
                    $total_orders =0;
                    $total_earning=0;
?>


<h3> <span>{{$data['agents']->fullname}} ({{$data['agents']->phone}})</span></h3>
  

    <table>
    <thead> 
      <tr>
        <th class="" colspan="26" style="text-align: left;background: #efefef;font-size: 20em;">
    <h3><b>Daily Orders Count Report For:
          <span>
            {{$data['month']}} , {{$data['year']}} 
            </span> </b></h3>

    </th>
</tr>  
                  <tr>  
                    
                    
                    <th>Date</th>
                    <th>Order Type</th>
                    <th>Order Count</th> 
                    <th>Earned for bookTou</th> 
                    <th>Advance</th>
                    <th>Difference</th>
                </tr>
    </thead>


    <tbody>

                @foreach($data['orders'] as $item) 
                 <tr>
                    <td>{{date('d-m-Y', strtotime($item->serviceDate))}}</td>
                    
                    <td>
                      @if($item->type=='pnd')
                        <span>{{$item->type}}</span>
                        @elseif($item->type=='assist')
                    <span >{{$item->type}}</span>
                    @else
                        <span>{{$item->type}}</span>
                        @endif




  
                    </td>
                    <td>{{$item->orderCount}}</td>
                    <td>{{$item->total_earned}}</td>
                    <td>{{$advance}}</td>
                    <td>
                        {{$difference = $item->total_earned - $advance}}
                    </td>

                 </tr>
                 <?php
                    $total_orders+= $item->orderCount;
                    $total_earning+= $item->total_earned;
                 ?>
                 

                 @endforeach

                 <tr>
                        <td></td>
                        <td></td>
                        <td>
                        <span>Total Orders:</span> {{$total_orders}}</td>
                        <td><span>Total Earning:</span> {{$total_earning}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>


                    

                
                  </table> 