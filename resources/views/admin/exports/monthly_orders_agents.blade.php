<?php  
$total_earning_for_bookTou = 0;
$total_pnd =0;
$total_normal =0;
$total_assist =0;
?>

 


<table class="table">
                    <thead class=" text-primary"> 


                    <tr><th class="" colspan="26" style="text-align: left;background: #efefef;font-size: 20em;">
    <h3><b>All Delivery Agents Orders Report For: 
            <span class="badge badge-warning">
              {{$data['month']}} , {{$data['year']}}
            </span></b></h3>

  </th></tr> 
                  <tr  style='font-size: 12px'>  
                    
                    <th>Agent ID</th>
                    <th class="text-left">Agent Name</th>
                    <th scope="col">Normal</th> 
                    <th scope="col">PND</th> 
                    <th scope="col">Assits</th> 
                    <th scope="col">Earned for bookTou</th> 
                    <th scope="col">Advance</th>
                    <th scope="col">Difference</th>
                </tr>
                </thead>

                <tbody>

                @foreach($data['agents'] as $agent) 
                 <tr>
                  <td class="text-center">
                  {{$agent->id}}
                  </td>
                  <td class="text-left">{{$agent->fullname}}</td>
                  <?php
                        $pndOrders =0;
                        $assitsOrders=0;
                        $normalOrders=0;
                        $advance = 0;
                        $difference =0;
                        $earningforbookTou=0;

                      foreach($data['orders'] as $item) 
                      {

                        if( $item->member_id == $agent->id )
                        {
                          
                              if($item->type == "normal"){
                   
                                  $normalOrders = $item->orderCount; 

                                  $earningforbookTou += $item->total_earned ;
                                  $difference = $earningforbookTou - $advance;

                              }elseif($item->type == "pnd"){
                                  
                                  $pndOrders = $item->orderCount;

                                  $earningforbookTou += $item->total_earned ;
                                  $difference = $earningforbookTou - $advance;

                              }elseif($item->type == "assist"){
                    
                                  $assitsOrders = $item->orderCount; 

                                  $earningforbookTou += $item->total_earned ;
                                  $difference = $earningforbookTou - $advance;
                              } 
                              
                        }

                            

                         

                      }
                   
                $total_pnd+=$pndOrders;
                $total_assist+=$assitsOrders;
                $total_normal+=$normalOrders;
                $total_earning_for_bookTou += $earningforbookTou;

                 ?>
                  

 

                  <td class="text-center">{{$normalOrders}}</td>
                  <td class="text-center">{{$pndOrders}}</td>
                  <td class="text-center">{{$assitsOrders}}</td>


                  <td class="text-center">{{$earningforbookTou}}</td>
                  <td class="text-center">{{$advance}}</td>
                  <td class="text-center">
                    {{$difference}}
                  </td>

                 </tr>

                 @endforeach

                 <tr>
                    <td></td>
                    <td></td>
                    <td>
                      <span class="badge badge-success">Normal:</span>
                      {{$total_normal}}
                    </td>
                    <td>
                      <span class="badge badge-warning">Pnd:</span>
                      {{$total_pnd}}
                    </td>
                    <td class="text-center">
                    <span class="badge badge-info">Assits:</span>
                    {{$total_assist}}
                    </td>
                    <td class="text-center"><span class="badge badge-primary">Total:</span>  {{$total_earning_for_bookTou}}</td>
                     
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
 </table> 