<?php 
$total_taxable_amount=0;
$total_bill_amount=0;
$total_delivery_charges=0;
$total_amount_from_customer=0;
$total_gst_tcs = 0;
$total_income_tax_tds = 0;
$total_commission=0;
$total_payable_to_merchant = 0;
$total_booktou_income = 0;
$total_output_cgst = 0;
$total_output_sgst = 0;
$total_gross_revenue_for_booktou = 0;
$annual_income=0;
$tax= 500000;
?>


 
<table class="table">

<thead>
  <tr><th class="" colspan="26" style="text-align: left;background: blue;font-size: 20em;">
    <h3><b>Taxes Report For </b>{{$month}}  {{$year}}</h3>

  </th></tr>
 <tr>
  <th style="text-align: center; font-weight: bold;">Order No</th>
  <th style="text-align: center; font-weight: bold;">Order Date</th>
  <th style="text-align: center; font-weight: bold;">Order Type</th>
  <th style="text-align: center; font-weight: bold;">Order Status</th>
  <th style="text-align: center; font-weight: bold;">Merchant</th>
  <th style="text-align: center; font-weight: bold;">Supply</th>
  <th style="text-align: center; font-weight: bold;">Constitution</th>
  <th style="text-align: center; font-weight: bold;">PAN</th>
  <th style="text-align: center; font-weight: bold;">GST</th>
  <th style="text-align: center; font-weight: bold;">A(Taxable value)</th>
  <th style="text-align: center; font-weight: bold;">B(CGST)</th>
  <th style="text-align: center; font-weight: bold;">C(SGST)</th>
  <th style="text-align: center; font-weight: bold;">D(Total Bill Value)</th>
  <th style="text-align: center; font-weight: bold;">E(Delivery Charges)</th>
  <th style="text-align: center; font-weight: bold;">F(Amount Received from customer)</th>
  <th style="text-align: center; font-weight: bold;">Payment Mode</th>
  <th style="text-align: center; font-weight: bold;">Date of Receipt</th>
  <th style="text-align: center; font-weight: bold;">GST TCS</th>
  <th style="text-align: center; font-weight: bold;">Income tax TDS </th>
  <th style="text-align: center; font-weight: bold;">Commission from Merchant</th>
  <th style="text-align: center; font-weight: bold;">Payable to Merchant</th>
  <th style="text-align: center; font-weight: bold;">Date of Payment</th>
  <th style="text-align: center; font-weight: bold;">Booktou Income</th>
  <th style="text-align: center; font-weight: bold;">Output CGST</th> 
  <th style="text-align: center; font-weight: bold;">Output SGST</th>
  <th style="text-align: center; font-weight: bold;">Gross Revenue for Booktou</th>
  <th style="text-align: center; font-weight: bold;">Customer GSTN, if any</th>
</tr>
</thead>


<tbody>

 
    @foreach($orderDetails as $orders)
  <tr>
    <td style="text-align: center;">{{$orders->orderNo}}</td>
    <td style="text-align: center;">{{\Carbon\Carbon::parse($orders->orderDate)->format('d/m/Y')}}</td>
    <td style="text-align: center;">
      @if($orders->type=="Normal")
      <span class="badge badge-success">{{$orders->type}}</span>
      @else
      <span class="badge badge-primary">{{$orders->type}}</span>
      @endif
      

    </td>
    <td style="text-align: center;"> 
      @if ($orders->orderStatus=="delivered")
      <span class="badge badge-success">{{$orders->orderStatus}}</span>
      @else
      <span class="badge badge-danger">{{$orders->orderStatus}}</span>
      @endif
    </td>
    <td style="text-align: center;">{{$orders->name}}</td>
    <td style="text-align: center;">{{$orders->category}}</td>
    <td style="text-align: center;">{{$orders->name}}</td>
    <td style="text-align: center;">NA</td>
    <td style="text-align: center;">
      <!-- checking gst number if exist -->
      
      @if ($orders->orderStatus=="delivered")  
      <?php
      //checking only delivered goods and services by booktou
     
         
        if ($orders->gst=="" || $orders->gst=="NA") {
          
          $gst="NA";
          $cgst = 0;
          $sgst = 0;
        }else{

          $gst = $orders->gst;
          $cgst = 25;
          $sgst = 25;
        }
      ?>
      {{$gst}}
      @endif

    </td>
    <td style="text-align: center;">
      @if ($orders->orderStatus=="delivered")  
      {{$taxableAmount = $orders->total_amount}}
      @endif

    </td>
    <td style="text-align: center;">
      <!-- checking gst for pnd normal orders -->
      @if ($orders->orderStatus=="delivered")
      <?php 

      if($orders->type=="pnd"){
        $cgst = 0;
        $sgst = 0;
      }
   

      ?>
      {{$cgst}}
      @endif
    
      <!-- checking ends here for cgst pnd normal orders -->
    </td>
    <td style="text-align: center;">
    @if ($orders->orderStatus=="delivered")
     <!-- checking sgst for pnd normal orders -->
     
      {{$sgst}}
    @endif 
      <!-- checking ends here for sgst pnd normal orders -->

    </td>
    <td style="text-align: center;">
      @if ($orders->orderStatus=="delivered")
      <?php 

       
      $total_bill = $taxableAmount + $cgst+$sgst;

      ?>
    {{$total_bill}}
    @endif

   </td>
    <td style="text-align: center;">
    @if ($orders->orderStatus=="delivered")
      {{$orders->delivery_charge}}
    @endif
  </td>
    <td style="text-align: center;">
    @if ($orders->orderStatus=="delivered")
      {{$total_bill + $orders->delivery_charge}}
    @endif
    </td>
    <td style="text-align: center;"> 
      @if ($orders->orderStatus=="delivered")
      {{$orders->payment_type}}
      @endif
     </td>


    <td style="text-align: center;">
<!-- payment receipt date -->

      @if ($orders->orderStatus=="delivered")
      <?php 
       
      if ($orders->payment_type=="Pay online (UPI)" || $orders->payment_type=="ONLINE") {

         $paymentreceiptdate =$orders->orderDate;

      }else if ($orders->payment_type=="Cash on delivery" || $orders->payment_type=="CASH" || 
                $orders->payment_type=="cash")
      {
         $paymentreceiptdate =$orders->payDate;
      } 


      ?>
      {{ \Carbon\Carbon::parse($paymentreceiptdate)->format('d/m/Y')}}
      <!-- {{ \Carbon\Carbon::parse($orders->payDate)->format('d/m/Y')}} -->
      @endif
      

<!-- payment receipt date ends here -->
    </td>
    <td style="text-align: center;">
      @if ($orders->orderStatus=="delivered")
      <?php 
      if ($orders->gst=="" || $orders->gst=="NA") {
          
          $gst_tcs = 0; 
      }else{
        $gst_tcs = $taxableAmount * 1/100;
      }
          


      ?>
      {{$gst_tcs}}
      @endif
    </td>
    <td style="text-align: center;">

      @if ($orders->orderStatus=="delivered")
       <?php
        
        foreach ($binTotalAmount as $amount) {
         if ($amount->bin==$orders->bin) {
            
            $annual_income=$amount->taxableAmount;
         }
        }

        if ($annual_income>$tax) {
           $income_tax =$taxableAmount*.01;
        }else{
           $income_tax = 0;
        }
        
        $income_tax_tds = $income_tax;

      ?>
      {{$income_tax_tds}}   

      @endif
    </td>
    <td style="text-align: center;">
    @if ($orders->orderStatus=="delivered")
    <?php 

                        //$percentage = $orders->commission / 100;

                        switch ($orders->type) {
                          case 'Normal':
                             $percentage = $orders->commission / 100;
                            break;
                          case 'pnd':

                              if ($orders->source=="booktou" || "customer") {
                                 foreach ($merchant as $biz) {
                                   if ($biz->id==$orders->bin) {
                                      
                                      $percentage = $biz->commission / 100;
                                   }
                                  }
                              }
                              if ($orders->source=="business") {
                                $percentage=0;
                              }
                            
                            break;
                            case 'assist':
                                      $percentage = 0;
                            break;
                           
                        }

                        $final_commision = $percentage * $total_bill;

                        $booktou_commission = $final_commision;
    ?>
                        {{number_format($booktou_commission)}}
    @endif
    </td>
    <td style="text-align: center;">
      @if ($orders->orderStatus=="delivered")
      <!-- 
      payable to merchant
     -->
      <?php 

      $payable_to_merchant = $total_bill - $gst_tcs-$income_tax_tds-$booktou_commission;

      ?>
      {{number_format($payable_to_merchant,2)}}
      <!-- 
       payable to merchant ends here
     -->
     @endif
    </td>


    <td style="text-align: center;">
      @if ($orders->orderStatus=="delivered")
      {{ \Carbon\Carbon::parse($orders->receiptDate)->format('d/m/Y')}} 
      @endif
    </td>


    <td style="text-align: center;">
      @if ($orders->orderStatus=="delivered")
       <!-- 
      calculating booktou income
     -->
      <?php
            $percentage_income = 18/100;
            $total_percent = 1+$percentage_income;

            $chargesncommission = $orders->delivery_charge + $booktou_commission;



            $total_income = $chargesncommission/$total_percent;



      ?>
       {{number_format($total_income,2)}}

      <!-- 
      calculating booktou income ends here
     -->
     @endif
    </td>

    <td style="text-align: center;"> 
      @if ($orders->orderStatus=="delivered")
      <!-- 
      calculating output cgst i*9%
      
     
     -->
    <?php 
        //$booktou_commission;

        $outputcgst= 9/100;

        $total_outputcgst = $total_income*$outputcgst;

    ?>

    {{number_format($total_outputcgst,2)}}

    @endif
    </td>
    <td style="text-align: center;">
      @if ($orders->orderStatus=="delivered")

      <?php 

        $total_outputsgst = $total_outputcgst;
      ?>
    {{number_format($total_outputsgst,2)}}

    @endif

  </td>

    <td style="text-align: center;">
      @if ($orders->orderStatus=="delivered")
      {{number_format($total_income + $total_outputcgst + $total_outputsgst,2)}}
      @endif
    </td>
    <td style="text-align: center;"></td>
  </tr>

@if ($orders->orderStatus=="delivered")
<!-- final total section -->
<?php 
$total_taxable_amount+=$taxableAmount;
$total_bill_amount+=$total_bill;
$total_delivery_charges +=$orders->delivery_charge;
$total_amount_from_customer+=$orders->total_amount + $orders->delivery_charge;
$total_gst_tcs+=$gst_tcs;
$total_income_tax_tds+=$income_tax_tds;
$total_commission+=$booktou_commission;
$total_payable_to_merchant+=$payable_to_merchant;
$total_booktou_income+=$total_income;
$total_output_cgst+=$total_outputcgst;
$total_output_sgst+=$total_outputsgst;
$total_gross_revenue_for_booktou+=$total_income + $total_outputcgst + $total_outputsgst;

?>
@endif
<!-- final total ends here -->



@endforeach



<!-- to pay section -->
<tr>
  
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;">
     <!-- total bill value -->
    {{number_format($total_taxable_amount,2)}}
    
    <!-- calculation ends here -->
 

  </td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;">
    <!-- total bill value -->
    {{number_format($total_bill_amount,2)}}
    
    <!-- calculation ends here -->
  </td>
  <td style="text-align: center;">
  {{number_format($total_delivery_charges,2)}}
 
  </td>
  <td style="text-align: center;">
  {{number_format($total_amount_from_customer,2)}}
  
  </td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;">
   {{number_format($total_gst_tcs,2)}}
   

  </td>
  <td style="text-align: center;">
    {{number_format($total_income_tax_tds,2)}}
    
  </td>
  <td style="text-align: center;">
    {{number_format($total_commission,2)}}
    
  </td>
  <td style="text-align: center;">
    {{number_format($total_payable_to_merchant,2)}}
    
  </td>
  <td style="text-align: center;"></td>
  <td style="text-align: center;">
    {{number_format($total_booktou_income,2)}}
    
  </td>
  <td style="text-align: center;">
    {{number_format($total_output_cgst,2)}}
   
  </td> 
  <td style="text-align: center;">
    {{number_format($total_output_sgst,2)}}
    
  </td>
  <td style="text-align: center;">
    {{number_format($total_gross_revenue_for_booktou,2)}}

  </td>
  <td style="text-align: center;"></td>
   

</tr>

<!-- pay section ends here -->


<!-- pay section ends here -->


</tbody>
</table>