@extends('layouts.admin_theme_02')
@section('content')

<div class="row">
    <div class="col-md-12">  
     	<div class="card">
            <div class="row m-3">
            <div class="col-md-4">
     					 <div class="card">
                            <div class="card-body">
                                  @foreach($data['donor'] as $don)  
             
                <strong class="pb-1">Donor Name</strong>
           
                                  <br><br>
                                    <label>{{$don->name}}</label>
                                    <br>
                                    <label>{{$don->phone}}</label>
                                    <br>
                                    <label>{{$don->address}}</label>
                                    <br>


                                @endforeach
                            </div>              
                         </div>
     			</div>

                <div class="col-md-4">
                       <div class="card">
                            <div class="card-body">

                            
                               @if($data['beneficiary']->isNotEmpty())
                                   
                                    @foreach($data['beneficiary'] as $ben)  
             
                                  <strong class="pb-1">Beneficiary Name</strong>
           
                                  <br><br>
                                    <label>{{$ben->name}}</label>
                                    <br>
                                    <label>{{$ben->phone}}</label>
                                    <br>
                                    <label>{{$ben->address}}</label>
                                    <br>


                            @endforeach
                                @else

                                <strong class="pb-1">Beneficiary Name</strong>

                                  <br><br>
                                    <label>No Beneficiary added!</label>

                                @endif
                            
                            </div>              
                         </div>
                </div>  

                <div class="col-md-4">
                      <div class="card">
                            <div class="card-body">
                                <div class="p-2" style="background: #fa174c;color: #fff;"><strong class="pb-1">Items/Money</strong></div>
                                
                                <br>
                                   @foreach($data['donorlist'] as $item) 
                                    <span>Donation mode:</span>
                                    <label class="badge badge-info">{{$item->donation_mode}}</label>
                                    <br>
                                    <span>Donation type:</span>
                                    <label class="badge badge-info">{{$item->donation_type}}</label>
                                    <br>

                                    
                                    @if($item->donation_mode=="money")
                                    <span>Amount:</span> <label class="badge badge-info">{{$item->amount}}</label>

                                    @else
                                    <span>Description:</span><label class="badge badge-info">{{$item->items}}</label>
                                    @endif

                                    @endforeach
                                 
                                 
                            </div>              
                         </div>
                </div>  		 
</div>
     	</div>
    </div>
</div>
  
@endsection

@section("script")
<script>


</script>
@endsection 