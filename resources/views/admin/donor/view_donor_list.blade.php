@extends('layouts.admin_theme_02')
@section('content')

<div class="row">
     <div class="col-md-12">  
     	<div class="card">

     		<div class="card-body">
     			<p>List of Donors donated for covid fund:</p>
     			<table class="table">
     				<thead>
     					<th>Name</th>
     					<th>Address</th>
     					<th>Phone</th>
     					<!-- <th>Category</th> -->
     					<th>Details</th>
     				</thead>

     				<tbody>
     					@foreach($data['donor'] as $donor)
     					<tr>
     						<td>{{$donor->name}}</td>
     						<td>{{$donor->address}}</td>
     						<td>{{$donor->phone}}</td>
     						<!-- <td>{{$donor->category}}</td> -->
     						<td>
     							
     							<div class="dropdown">
  

                     <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a>



  <ul class="dropdown-menu dropdown-user pull-right"> 
    
    <li><a class="dropdown-item" href="{{ URL::to('/admin/donor/list-donor-details') }}/{{$donor->id}}">View Details</a></li>
    <!-- <li><a class="dropdown-item" href="{{URL::to('/admin/customer-care/business/view-products')}}/{{$donor->id}}"> Items Donated</a></li> -->
     
     
                     
       
      
 
     
                     
  </ul>
         </div>




     						</td>
     					</tr>
     					@endforeach
     				</tbody>

     			</table>


     		</div>
     	</div>
     </div>
 </div>
@endsection

@section("script")
<script>


</script>
@endsection 