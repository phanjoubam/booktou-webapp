@extends('layouts.admin_theme_02')
@section('content')

<?php 

$total_bill_amount=0;
$total_delivery_charges=0;
$total_amount_from_customer=0;
$total_gst_tcs = 0;
$total_income_tax_tds = 0;
$total_commission=0;
$total_payable_to_merchant = 0;
$total_booktou_income = 0;
$total_output_cgst = 0;
$total_output_sgst = 0;
$total_gross_revenue_for_booktou = 0;
$annual_income=0;
$tax= 500000;
?>

<style type="text/css">
  
   
</style>

<div class="row">
     <div class="col-md-12"> 
     	
      <!-- card section starts from here -->

      <div class="card card-default">
           <div class="card-header sticky-top">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-4">
                  <div class="title">
                  <h5> Monthly Tax Details for <span class="badge bg-warning">{{ $month  }}, {{ $year }}
                  </span> </h5>
                   
                  </div> 
                </div>
                <div class="col-md-2">
                  
                   
                   <a href="{{url('/admin/accounting/export-merchant-taxes')}}" class="btn btn-primary"
                   target="_blank">Export to excel</a>
   
                </div>

                <div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-4">  
        <form class="form-inline" method="get" 
        action="{{ action('Admin\AdminDashboardController@prepareMonthlyTaxes') }}"   >
              {{  csrf_field() }}
      
      <div class="form-row">
    <div class="col-md-12">
        <select name='month' class="form-control form-control-sm  ">
          <option value='1'>January</option>
          <option value='2'>February</option>
          <option value='3'>March</option>
          <option value='4'>April</option>
          <option value='5'>May</option>
          <option value='6'>June</option>
          <option value='7'>July</option>
          <option value='8'>August</option>
          <option value='9'>September</option>
          <option value='10'>October</option>
          <option value='11'>November</option>
          <option value='12'>December</option>

        </select>
       <select name='year' class="form-control form-control-sm  ">
          <?php 
          for($i=2021; $i >= 2020; $i--)
          {
            ?> 
            <option value='{{ $i }}'>{{ $i }}</option>
          <?php 
          }
          ?>

        </select>
   
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>

       </div>
<br>
<div class="row">

        <div class="col-2">
        <span class="badge badge-primary">Overall Total Bill Amount:</span> {{number_format($totalbill,2)}} 

        </div>

        <div class="col-2">
        <span class="badge badge-primary">Amount From Customer:</span> {{number_format($totalamountcustomer,2)}} 

        </div>

        <div class="col-2">
        <span class="badge badge-primary">Payable to Merchant:</span> {{number_format($total_merchant_pay,2)}} 

        </div>
        <div class="col-2">
        <span class="badge badge-success">bookTou total Commission:</span> {{number_format($booktou_commission,2)}} 

        </div>
        <div class="col-2">
        <span class="badge badge-success">bookTou total Income:</span> {{number_format($booktou_income,2)}} 

        </div> 

        <div class="col-2">
        <span class="badge badge-success">bookTou Gross Revenue:</span> {{number_format($gross_revenue,2)}} 

        </div>
        
       

      </div>
                </div>




            </div>


<!-- record display section starts from here -->
<div class="card-body">
  <div class="table-responsive" >
<table class="table table-striped " >
 

<thead>
  <tr>
  <th>Order No</th>
  <th>Order Date</th>
  <th>Order Type</th>
  <th>Order Status</th>
  <th>Merchant</th>
  <th>Supply</th>
  <th>Constitution(Individual/HUF/Firm/Company)</th>
  <th>PAN</th>
  <th>GST</th>
  <th>A(Taxable value)</th>
  <th>B(CGST)</th>
  <th>C(SGST)</th>
  <th>D(Total Bill Value)</th>
  <th>E(Delivery Charges)</th>
  <th>F(Amount Received from customer)</th>
  <th>Payment Mode</th>
  <th>Date of Receipt</th>
  <th>(G)GST TCS @1% (A*1%) </th>
  <th>(H)Income tax TDS @1% (A*1%) </th>
  <th>(I) Commission from Merchant</th>
  <th>(J) Payable to Merchant(D-G-H-I)</th>
  <th>Date of Payment</th>
  <th>(K) Booktou Income ((E+I)/(1+18%))</th>
  <th>(L) Output CGST(I*9%)</th> 
  <th>(M) Output SGST(I*9%)</th>
  <th>(N) Gross Revenue for Booktou (K+L+M) or (E+S)</th>
  <th>Customer GSTN, if any</th>
</tr>
</thead>

<tbody>

 
  @foreach($orderDetails as $orders)
  <tr>
    <td>{{$orders->orderNo}}</td>
    <td>{{\Carbon\Carbon::parse($orders->orderDate)->format('d/m/Y')}}</td>
    <td>
      @if($orders->type=="Normal")
      <span class="badge badge-success">{{$orders->type}}</span>
      @else
      <span class="badge badge-primary">{{$orders->type}}</span>
      @endif
      

    </td>
    <td> 
      @if ($orders->orderStatus=="delivered")
      <span class="badge badge-success">{{$orders->orderStatus}}</span>
      @else
      <span class="badge badge-danger">{{$orders->orderStatus}}</span>
      @endif
    </td>
    <td>{{$orders->name}}</td>
    <td>{{$orders->category}}</td>
    <td>{{$orders->name}}</td>
    <td>NA</td>
    <td>
      <!-- checking gst number if exist -->
      
      @if ($orders->orderStatus=="delivered")  
      <?php
      //checking only delivered goods and services by booktou
     
         
        if ($orders->gst=="" || $orders->gst=="NA") {
          
          $gst="NA";
          $cgst = 0;
          $sgst = 0;
        }else{

          $gst = $orders->gst;
          $cgst = 25;
          $sgst = 25;
        }
      ?>
      {{$gst}}
      @endif

    </td>
    <td>
      @if ($orders->orderStatus=="delivered")  
      {{$taxableAmount = $orders->total_amount}}
      @endif

    </td>
    <td>
      <!-- checking gst for pnd normal orders -->
      @if ($orders->orderStatus=="delivered")
      <?php 

      if($orders->type=="pnd"){
        $cgst = 0;
        $sgst = 0;
      }
   

      ?>
      {{$cgst}}
      @endif
    
      <!-- checking ends here for cgst pnd normal orders -->
    </td>
    <td>
    @if ($orders->orderStatus=="delivered")
     <!-- checking sgst for pnd normal orders -->
     
      {{$sgst}}
    @endif 
      <!-- checking ends here for sgst pnd normal orders -->

    </td>
    <td>
      @if ($orders->orderStatus=="delivered")
      <?php 

       
      $total_bill = $taxableAmount + $cgst+$sgst;

      ?>
    {{$total_bill}}
    @endif

   </td>
    <td>
    @if ($orders->orderStatus=="delivered")
      {{$orders->delivery_charge}}
    @endif
  </td>
    <td>
    @if ($orders->orderStatus=="delivered")
      {{$total_bill + $orders->delivery_charge}}
    @endif
    </td>
    <td> 
      @if ($orders->orderStatus=="delivered")
      {{$orders->payment_type}}
      @endif
     </td>


    <td>
<!-- payment receipt date -->

      @if ($orders->orderStatus=="delivered")
      <?php 
       
      if ($orders->payment_type=="Pay online (UPI)" || $orders->payment_type=="ONLINE") {

         $paymentreceiptdate =$orders->orderDate;

      }else if ($orders->payment_type=="Cash on delivery" || $orders->payment_type=="CASH" || 
                $orders->payment_type=="cash")
      {
         $paymentreceiptdate =$orders->payDate;
      } 


      ?>
      {{ \Carbon\Carbon::parse($paymentreceiptdate)->format('d/m/Y')}}
      <!-- {{ \Carbon\Carbon::parse($orders->payDate)->format('d/m/Y')}} -->
      @endif
      

<!-- payment receipt date ends here -->
    </td>
    <td>
      @if ($orders->orderStatus=="delivered")
      <?php 
      if ($orders->gst=="" || $orders->gst=="NA") {
          
          $gst_tcs = 0; 
      }else{
        $gst_tcs = $taxableAmount * 1/100;
      }
          


      ?>
      {{$gst_tcs}}
      @endif
    </td>
    <td>

      @if ($orders->orderStatus=="delivered")
       <?php

        foreach ($binTotalAmount as $amount) {
         if ($amount->bin==$orders->bin) {
            
            $annual_income=$amount->taxableAmount;
         }
        }

        $income_tax = ($annual_income > $tax) ? $taxableAmount*.01 : 0;
         
        $income_tax_tds = $income_tax;

      ?>
      {{$income_tax_tds}}   

      @endif
    </td>
    <td>
    @if ($orders->orderStatus=="delivered")
    <?php 

                        //$percentage = $orders->commission / 100;
                      switch ($orders->type) {
                          case 'Normal':
                             $percentage = $orders->commission / 100;
                            break;
                          case 'pnd':

                              if ($orders->source=="booktou" || "customer") {
                                 foreach ($business as $biz) {
                                   if ($biz->id==$orders->bin) {
                                      
                                      $percentage = $biz->commission / 100;
                                   }
                                  }
                              }
                              if ($orders->source=="business") {
                                $percentage=0;
                              }
                            
                            break;
                            case 'assist':
                                      $percentage = 0;
                            break;
                           
                      }

                        $final_commision = $percentage * $total_bill;

                        $booktou_commission = $final_commision;
    ?>
                        {{number_format($booktou_commission)}}
    @endif
    </td>
    <td>
      @if ($orders->orderStatus=="delivered")
      <!-- 
      payable to merchant
     -->
      <?php 

      $payable_to_merchant = $total_bill - $gst_tcs-$income_tax_tds-$booktou_commission;

      ?>
      {{number_format($payable_to_merchant,2)}}
      <!-- 
       payable to merchant ends here
     -->
     @endif
    </td>


    <td>
      @if ($orders->orderStatus=="delivered")
      {{ \Carbon\Carbon::parse($orders->receiptDate)->format('d/m/Y')}} 
      @endif
    </td>


    <td>
      @if ($orders->orderStatus=="delivered")
       <!-- 
      calculating booktou income
     -->
      <?php
            $percentage_income = 18/100;
            $total_percent = 1+$percentage_income;

            $chargesncommission = $orders->delivery_charge + $booktou_commission;



            $total_income = $chargesncommission/$total_percent;



      ?>
       {{number_format($total_income,2)}}

      <!-- 
      calculating booktou income ends here
     -->
     @endif
    </td>

    <td>
      @if ($orders->orderStatus=="delivered")
      <!-- 
      calculating output cgst i*9%
      
     
     -->
    <?php 
        //$booktou_commission;

        $outputcgst= 9/100;

        $total_outputcgst = $total_income*$outputcgst;

    ?>

    {{number_format($total_outputcgst,2)}}

    @endif
    </td>
    <td>
      @if ($orders->orderStatus=="delivered")

      <?php 

        $total_outputsgst = $total_outputcgst;
      ?>
    {{number_format($total_outputsgst,2)}}

    @endif

  </td>

    <td>
      @if ($orders->orderStatus=="delivered")
      {{number_format($total_income + $total_outputcgst + $total_outputsgst,2)}}
      @endif
    </td>
    <td></td>
  </tr>

@if ($orders->orderStatus=="delivered")
<!-- final total section -->
<?php 

$total_bill_amount+=$total_bill;
$total_delivery_charges +=$orders->delivery_charge;
$total_amount_from_customer+=$orders->total_amount + $orders->delivery_charge;
$total_gst_tcs+=$gst_tcs;
$total_income_tax_tds+=$income_tax_tds;
$total_commission+=$booktou_commission;
$total_payable_to_merchant+=$payable_to_merchant;
$total_booktou_income+=$total_income;
$total_output_cgst+=$total_outputcgst;
$total_output_sgst+=$total_outputsgst;
$total_gross_revenue_for_booktou+=$total_income + $total_outputcgst + $total_outputsgst;

?>
@endif
<!-- final total ends here -->



@endforeach


<!-- to pay section -->
<tr>
  
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td>
    <!-- total bill value -->
    {{number_format($total_bill_amount,2)}}
    <span class="badge badge-primary"> 
    Total Bill
    </span>
    <!-- calculation ends here -->
  </td>
  <td>
  {{number_format($total_delivery_charges,2)}}
  <span class="badge badge-primary">
  Delivery charges
  </span>
  </td>
  <td>
  {{number_format($total_amount_from_customer,2)}}
  <span class="badge badge-primary">
    Received from customer
  </span>
  </td>
  <td></td>
  <td></td>
  <td>
   {{number_format($total_gst_tcs,2)}}
   <span class="badge badge-primary">
    <small>GST TCS</small>
  </span>

  </td>
  <td>
    {{number_format($total_income_tax_tds,2)}}
    <span class="badge badge-primary">
    <small>TDS</small>
  </span>
  </td>
  <td>
    {{number_format($total_commission,2)}}
    <span class="badge badge-primary">
    <small>bookTou Commission</small>
  </span>
  </td>
  <td>
    {{number_format($total_payable_to_merchant,2)}}
    <span class="badge badge-success">
    <small>Payable to merchant</small>
  </span>
  </td>
  <td></td>
  <td>
    {{number_format($total_booktou_income,2)}}
    <span class="badge badge-primary">
    <small>bookTou income</small>
  </span>
  </td>
  <td>
    {{number_format($total_output_cgst,2)}}
    <span class="badge badge-primary">
    <small>Output CGST</small>
  </span>
  </td> 
  <td>
    {{number_format($total_output_sgst,2)}}
    <span class="badge badge-primary">
    <small>Output SGST</small>
  </span>
  </td>
  <td>
    {{number_format($total_gross_revenue_for_booktou,2)}}
<span class="badge badge-secondary">
    <small>Gross Revenue</small>
  </span>
  </td>
  <td></td>
   

</tr>


<!-- pay section ends here -->


</tbody>

 </table>
 <br>
{{$orderDetails->withQueryString()->links()}}

</div>
     </div>

<!-- card section ends here -->



</div>
<!-- record display section ends here -->
</div>
</div>
@endsection

@section("script")


@endsection