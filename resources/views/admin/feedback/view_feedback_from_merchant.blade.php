@extends('layouts.admin_theme_02')
@section('content')

<div class="row">
<div class="col-md-12">

<div class="card">
<div class="card-header">
<h3>View Merchant Feedback <span class="badge badge-warning">{{$month}}, {{date('Y')}}</span></h3>
</div>
<div class="card-body">

<form method="get" action="{{action('Admin\AdminDashboardController@viewMerchantFeedback')}}">
 <div class="form-group">

  	<div class="row">
  	<div class="col-md-4"> 
    
    <select name ="bin" class="selectize" id="bin">


                      @foreach($businesses as $bitem)

                          <option <?php if( request()->bin  == $bitem->id ) echo "selected"; ?> 
                          value="{{ $bitem->id  }}">{{ $bitem->name }}</option>
                      @endforeach


    </select>
  	</div>
	<div class="col-md-2">
		 <select name='month' class="form-control form-control-sm  ">
          <option <?php if( $month  == 1 ) echo "selected"; ?> value='1'>January</option>
          <option <?php if( $month== 2 ) echo "selected"; ?>  value='2'>February</option>
          <option <?php if( $month == 3 ) echo "selected"; ?> value='3'>March</option>
          <option <?php if( $month== 4 ) echo "selected"; ?> value='4'>April</option>
          <option <?php if( $month == 5 ) echo "selected"; ?> value='5'>May</option>
          <option <?php if( $month == 6 ) echo "selected"; ?> value='6'>June</option>
          <option <?php if( $month == 7 ) echo "selected"; ?> value='7'>July</option>
          <option <?php if( $month == 8 ) echo "selected"; ?> value='8'>August</option>
          <option <?php if( $month == 9 ) echo "selected"; ?> value='9'>September</option>
          <option <?php if($month == 10 ) echo "selected"; ?> value='10'>October</option>
          <option <?php if( $month== 11 ) echo "selected"; ?> value='11'>November</option>
          <option <?php if( $month == 12 ) echo "selected"; ?> value='12'>December</option> 
        </select>
	</div>
  	 <div class="col-md-2">
  	 	<button type="submit" class="btn btn-primary">Search</button>
  	 </div>
     
     <div class="col-md-2"></div>
     <div class="col-md-2"></div>
     
     </div>
  </div>
</form>



 
</div>
</div>
</div>
</div>



@if( count($feedback ) > 0 ) 
<div class="row mt-2">
	<div class="col-md-12">
	<div class="card">
	<div class="card-body" > 
<table class="table table-bordered">
 	<thead>
 	<tr>	
 		<th>Source</th>
 		<th>Remarks</th>
 		<th>Feedback</th>
 	</tr>
 	</thead>

 	<tbody>
 		@foreach($feedback as $fb)
 		<tr>
 			<td>{{$fb->source}}</td>
 			<td>{{$fb->remarks}}</td>
 			<td>{{$fb->feedback}}</td>
 		</tr>
 		@endforeach
 	</tbody>

 </table> 
   </div>
</div>
</div>
</div>
  @endif

@endsection
@section("script")

<script>


 
 

 


</script> 

@endsection 