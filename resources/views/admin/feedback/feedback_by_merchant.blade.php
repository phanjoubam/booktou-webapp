@extends('layouts.admin_theme_02')
@section('content')


<div class="row">
<div class="col-md-12">

<div class="card">
<div class="card-header">
<h3>Merchant Feedback Form</h3>
</div>
<div class="card-body">

<form method="post" action="{{action('Admin\AdminDashboardController@saveMerchantFeedback')}}">
	{{csrf_field()}}
  <div class="form-group">

  	<div class="row">
  	<div class="col-md-4"> 
    
    <select name ="bin" class="selectize" id="bin">


                      @foreach($businesses as $bitem)
                          <option value="{{ $bitem->id  }}" selected>{{ $bitem->name }} ({{ $bitem->frno }})</option>
                      @endforeach


    </select>
  	</div>

  	<div class="col-md-4"> 
    
    <select name ="source" class="form-control" id="source">
    <option value="" > Select Source</option>
 	<option value="whatsapp" > WhatsApp</option>
 	<option value="facebook" >Facebook </option>
 	<option value="instagram" >Instagram </option>
 	<option value="sms" >SMS </option>
 	<option value="phone" >Phone </option>
  	</select>
  	</div>
     
    <div class="col-md-4"> 
    
    <input type="text" class="form-control" id="remarks" aria-describedby="remarks" placeholder="Remarks" name="remarks">
  	</div>
     </div>
  </div>
  <div class="form-group">
     
    <textarea class="form-control" id="feedback" placeholder="Feedback" name="comment"></textarea>
  </div>
   
  <button type="submit" class="btn btn-primary">Save</button>
</form>

</div>
</div>
</div>
</div>








@endsection

@section("script")

<script>
 
    
 $('.selectize').select2({
        selectOnClose: true
      });

 

      

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
</script> 

@endsection 