@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 
 

<div class="cardbodyy margin-top-bg"> 

  <div class="col-md-12">
     <div class="card">
       <div class="card-header">

          <form class="form-inline" method="post" action="{{  action('Admin\AdminDashboardController@viewAllOrders') }}">

            {{ csrf_field() }}
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar'  
             value="{{ date('d-m-Y', strtotime($data['last_keyword_date']))  }}" name='filter_date' />

              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Status:</label> 
                <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='filter_status' >
                  <option value='-1'>All</option> 
                    <option value='new'>New</option> 
                    <option value='confirmed' <?php if($data['last_keyword']  == "confirmed") echo "selected";  ?>>Confirmed</option> 
                      <option value='delivery_scheduled' <?php if($data['last_keyword']  == "delivery_scheduled") echo "selected";  ?>>Wating Agent Pickup</option>  
                    <option value='order_packed' <?php if($data['last_keyword']  == "order_packed") echo "selected";  ?>>Packed and Ready for delivery</option>    
                    <option value='in_route' <?php if($data['last_keyword'] == "in_route") echo "selected";  ?>>In Route</option>    

                </select>

                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
            </form>

            <hr/>
       </div>

              <div class="card-body"> 
                  <table class="table">
                    <thead class=" text-primary"> 
 
                  <tr class="text-center" style='font-size: 12px'>  
                    <th scope="col">Customer</th>
                     <th scope="col">Seller/Shop</th>
                    <th scope="col">Time Elapsed</th> 
                    <th scope="col">OTP</th> 
                    <th scope="col">Order Status</th>  
                    <th scope="col">Customer Confirmation</th> 
                    <th scope="col">Assigned To</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');  

                ?>
  
                  <tr id="tr{{$item->id }}">  
                   <td 
                   <?php 
                    if( $interval->format('%H') >=  1  ) 
                      echo 'style="background-color:#ff6e6e;color:#fff"' ;
                    else 
                       if( $interval->format('%H') < 1 )
                        echo 'style="background-color: #67cafa;color:#fff"' ;
                   
                    ?> >
                    <strong>Order #</strong>
                    {{$item->id}}<br/>{{$item->fullname}}<br/><small><i class='fa fa-phone'></i> {{ $item->phone }}</small>
                  </td>
                  <td>
                  @foreach($data['all_businesses'] as $business)
                    @if($business->id == $item->bin)
                      {{  $business->name  }}
                      @break
                    @endif
                  @endforeach

                  </td>

                    <td>  {{ $order_age }} </td>

                    
 
                  <td><span class='badge badge-info'>{{ $item->otp }}</span></td>
                     <td>

                      @switch($item->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>  
                     <td>
                      <?php  
                      if($item->book_status != "new")
                      {
                        echo "Confirmed By Order";
                      }
                      else 
                      {
                        ?>
<div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input switch"   data-id='{{$item->id }}' id="switchON{{ $item->id }}"  
          <?php if($item->is_confirmed == "yes") echo "checked";  ?>  />
          <label class="custom-control-label" for="switchON{{$item->id }}">Confirmed By Call</label>
     </div>

                        <?php 
                      }
                      ?>
    

 
                   </td>


                       <td>
                        <div class='form-flex'>
                          @if( $item->book_status == "new" ||  $item->book_status == "confirmed" )

                            <select   class='form-control form-control-sm' id="aid_{{$item->id }}" name='agents[]' >
                            @foreach($data['all_agents'] as $aitem) 
                               @if($aitem->isFree == "yes" )
                                <option value='{{ $aitem->id }}' data-img="{{ $aitem->profile_photo }}"  >{{ $aitem->nameWithTask }}</option>
                              @endif 
                            @endforeach 
                        </select>  &nbsp;
                        <button class='btn btn-secondary btn-xs showTaskList' data-cid="aid_{{$item->id }}"  ><i class='fa fa-eye'></i></button>

                         
                        @else

                          @foreach($data['delivery_orders_list'] as $ditem) 
                                @if( $item->id  == $ditem->order_no )
                                 {{ $ditem->fullname  }}  
                                  @break;
                              @endif  
                          @endforeach
                        @endif 
                          
                       
                        </div>
 
                       </td>  
                                      
   
        
                    <td class="text-right">
                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <button class="dropdown-item btn-assign" data-oid="{{$item->id}}" >Assign Agent</button>  
                          <a class="dropdown-item" href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{$item->id}}">View Order Details</a> 

                             <button  type="button" class="dropdown-item btncancel" data-ono="{{$item->id}}">Cancel Order</button>  


                        </div>
                      </div>
                    </td>
   
                         </tr>
                         <?php
                          $i++; 
                       }

                          ?>
                </tbody>
                  </table>
            
              </div>
            </div>
          </div>
        </div>  
  

 
<div class="modal hide fade modal_cancel"   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Confirm Order Cancellation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body "> 
            <div class='confloading'>
        </div> 
        <div class="form-group"> 
    <textarea  placeholder='Reason for cancelling' class="form-control form-control-sm  " id="tareason" name="tareason" rows='5'></textarea>

  </div>
  

      </div>
      <div class="modal-footer">
        <input type='hidden' id='orderno' name="orderno" />
        <button type="button" class="btn btn-primary btnconfim" >Cancel</button> 
        <button type="button" class="btn btn-secondary" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
 


<div class="modal hide fade modal_processing"   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Processing Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"> 
        <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn_action" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modalTaskList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
  <img src='{{ URL::to("/public/assets/image/no-image.jpg") }}' id='apimg' width="40px" height="40px" class="img-rounded" style='margin-right:10px; display:inline-block'/>
<h5 class="modal-title" id="exampleModalLongTitle">Delivery Tasks for <strong id='span_anm'></strong></h5>
  

        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content_area">
         <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


<div class="modal" id='confirmOrder' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Customer Order By Call</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class='alert alert-danger'>THIS ACTION CANNOT BE REVERSED!</p>
        <p>

          Have you called back the customer to confirm this order?
          <br/>

          If you haven't call back, pleaes do call and confirm first.
          <br/> 
        </p>
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <button type="button" class="btn btn-success" id="btnsaveconfirmation" data-conf="" data-id="" >Save Changes</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>

 
@endsection

@section("script")

<script>

$(document).on("click", ".btncancel", function()
{

    $("#orderno").val($(this).attr("data-ono"));

      $(".modal_cancel").modal("show")

});




$(document).on("click", ".btnconfim", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['reason'] =  $("#tareason").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/order/cancel" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_cancel").modal("hide") ;

         $("#tr" + $("#orderno").val( )).remove();
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });

       

});

  


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
             
        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 

    
    $('.modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('.modal_processing').modal({
      show:true,
      keyboard: false,
      backdrop: 'static'
    });


    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('.modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id");

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsaveconfirmation").attr("data-id",  oid);
      $("#btnsaveconfirmation").attr("data-conf", confirmation ); 
      $("#confirmOrder").modal("show");   
   }  

})

$(document).on("change", "#closeconfmodal", function()
{

  
})
 

$('body').delegate('#btnsaveconfirmation','click',function(){
  
   var confirmation = $(this).attr("data-conf"); 
   var oid = $(this).attr("data-id");
   var json = {}; 
   json['oid'] = oid ;
   json['confirmation'] = confirmation ; 

   $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");


   $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/update-customer-confirmation" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
        if(data.browser_refresh == "yes")
        {
          $(".loading_span").html(" ");
          location.reload(); 
        }    
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  
})

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 