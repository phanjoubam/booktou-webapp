<script src="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/js/vendor.bundle.base.js"></script> 
<script src="{{ URL::to('/public/assets/admin') }}/js/core/bootstrap.min.js"></script> 
<script src="{{ URL::to('/public/assets/admin') }}/js/core/popper.min.js"></script>
<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/graph.js"></script>  
<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/jquery.cookie.js" type="text/javascript"></script> 
<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/off-canvas.js"></script> 
<script src="{{ URL::to('/public/assets/vendor/fw/jquery.fireworks.js') }}"></script> 
<script src="{{   URL::to('/public/assets/vendor/select2/js/select2.js')   }}"></script>
<script src="{{ URL::to('/public/assets/vendor') }}/jexcel/jexcel.js"></script> 
<script src="{{ URL::to('/public/assets/vendor') }}/jsuites/dist/jsuites.js"></script> 
<script src="{{ URL::to('/public/assets') }}/vendor/pn/js/pignose.calendar.full.min.js" type="text/javascript"></script>  
<script src="{{   URL::to('/public/assets/vendor/jquery-ui/jquery-ui.js')   }}"></script>
<script src="{{ URL::to('/public/assets') }}/admin/js/core/bnamodule.js?v={{ time() }}"></script>