@extends('layouts.admin_theme_02')
@section('content')

 <?php 
$order_info = $data['order_info'];
$order_items  = $data['order_items'];
$customer = $data['customer'];
$agent = $data['agent_info'];
 
?>



  <div class="row">
     <div class="col-md-12">
 
     <div class="card panel-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row"> 
                   <div class="col-md-6">
                  <div class="title">Order Details</div> 
                </div>

                <div class="col-md-6 text-right"> 
                </div> 
              </div>
                </div>
            </div>
       <div class="card-body">  
  
          <div class="col-lg-6">
            <div class="card card-chart first-body">
              <div class="card-header"> 
                <h4 class="card-title">Customer Info</h4> 
              </div>
              <div class="card-body ">

                      <div class="row"> 
        <div class="col-lg-3">
         <strong>Customer Name :</strong>
      </div> 
   <div class="col-lg-9">
     {{$customer->customerName}} 
      </div> 
       <div class="col-lg-3">
        <strong>Customer Address :</strong>
          </div> 
        <div class="col-lg-9">
      {{$customer->customerAddress}}
   </div> 
     
     
     <div class="col-lg-3"> 
      <strong>Phone :</strong>
      </div> 
   <div class="col-lg-9">
  {{$customer->phone}}
     </div> 

  <div class="col-lg-3">
  <strong>Alternate Phone :</strong>
  </div>

  <div class="col-lg-9">
 
  </div>
   
</div>  

               
              </div> 
              
            </div>
        

          </div>



          <div class="col-lg-4 second-body">
            <div class="card card-chart">
              <div class="card-header"> 
                <h4 class="card-title">Business Info</h4>
                 
              </div>
              <div class="card-body body1">

                      <div class="row"> 
  
     <div class="col-lg-5">
      <strong>Store Name :</strong>
       </div> 
      <div class="col-lg-7">
      {{$customer->storeName}} 
   </div> 
     <div class="col-lg-5">
      <strong>Store Address : </strong>
       </div> 
     <div class="col-lg-7"> 
      {{$customer->locality}}
   </div> 
     <div class="col-lg-5"> 
      <strong>Phone :</strong>
      </div> 
   <div class="col-lg-7">
  {{$customer->phone}}
     </div> 
 
   
</div>  
                <div class="chart-area">
                  <canvas id="lineChartExampleWithNumbersAndGrid"></canvas>
                </div>
              </div>
            
            </div>
 </div>
       
       
           
                    
    <div class="cardbodyy newpad"> 
         <div class="col-md-12">
            <div class="card card2">
              <div class="card-header"> 
                <h4 class="card-title"> Order Stats</h4>
              </div>
              <div class="card-body">
           <div class="col-md-12"> 
      <div class="pl-lg-4"> 
 
      <div class="row"> 
       <div class="col-lg-8">  
  
  <div class="col-lg-3">
  <strong>Order Details :</strong>
  </div>

  <div class="col-lg-9">
    {{$customer->orderItems}}, {{$customer->orderUnit}}, {{$customer->orderCategory}}.
  </div>


  </div>
 
    <div class="col-lg-4">
      
                    <div class="row">
                     <div class="col text-center">  
                    <div class="form-group">
                     <img src='http://localhost/booktouadmin/<?php if($customer->photo==""||"Null"){ echo "public/blank_photo.jpg"; }else{
                     echo $customer->photo ;}?>'
                     id="input-photo-tag" alt="..." height="160px" width="280px">
                    </div>
                  </div>
                  <div class="col text-center"> 
                 
                    </div>
                      </div>
          
                  </div>
   
</div>    
  
   </div> 
  
   

     <hr class="my-4" />
  <div class="pl-lg-4">
 <div class="row">
 
  <div class="col-lg-3">
   <strong>Total Amount:</strong>
     </div>

   <div class="col-lg-9">
 
   </div>    
      </div>   
  
 
    </div> 
     
   </div>
  </div>
    </div>
       </div>
 
 </div> 

  
      

 </div>
        

@endsection

@section("script")

<script>
 

</script> 

@endsection 
 
  
 