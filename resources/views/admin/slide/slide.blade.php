@extends('layouts.admin_theme_02')
@section('content')
 
 <div class="row">
  
  @if (session('err_msg'))
  <div class="col-md-12">  
        
        <div class="alert alert-info">
        {{ session('err_msg') }}
        </div>
        </div>

<div class='mt-3'></div> 
      @endif
 



<div class="col-md-4 "> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-8">
                  <div class="title">Sliding Photo Configuration</div> 
                </div>
 <div class="col-md-4 text-right"> 
        
      </div>

       </div>
                </div>
            </div>
  <div class="card-body">   
    
    <form action="{{action('Admin\AdminDashboardController@photoSlide') }}"  enctype="multipart/form-data" method="post" >
      {{ csrf_field() }} 
      <div class="card-body ">
    <div class="row"> 
    @if (session('err_msg_right'))
      <div class="col-md-12"> 
      <div class="alert alert-info">
      {{ session('err_msg_right') }}
      </div>
      </div>
    @endif
    </div>

    <div class="form-group row">
    <label for="bin" class="col-sm-4 col-form-label">Business</label>
    <div class="col-sm-8">
      <select name="bin" class="form-control" id='bin'>
           @foreach( $businesses  as $business)
              <option value='{{ $business->id }}'>{{ $business->name }}</option>    
           @endforeach
      </select>
    </div>
  </div>

   <div class="form-group row">
    <label for="position" class="col-sm-4 col-form-label">Display Position</label>
    <div class="col-sm-8">
        <input type="number"  id='position' name='sliding_position' value="0" class="form-control">
    </div>
  </div>

 <div class="form-group row">
    <label for="photo" class="col-sm-4 col-form-label">Select Image</label>
    <div class="col-sm-8">
        <input type="file"  id="photo" name='photo' >
    </div>
  </div>
 
     
 
                      <div class="form-row">
                      <div class="col"> 
                      <div class="form-group">  
                      <br/>
                        <button type="submit" class="btn btn-primary">Save</button> 
                        <button type="submit" name='btnCancel' value='save' class="btn btn-danger">Cancel</button>
                      </div>
                    </div> 
                      </div>
                      
               
         </div>
     
     </form>


 </div> 
  </div>  
  
 </div>  




    @foreach ( $slide  as $item)
    <div class="col-md-4 mt-4">    
        <div class="card ">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">


                  <div class="col-md-10">   
                   <strong>Display Position</strong> {{$item->position}}
                  </div>
                  <div class="col-md-2">    


                    <div class="dropdown">
          <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" 
          role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-cog "></i>
        </a> 
          <ul class="dropdown-menu dropdown-user pull-right"> 
            <li><a class="dropdown-item btnDelete" href='#' data-key="{{  $item->id }}" ><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
     </div>
      </div>
       </div>
 
                </div>
            </div>
       <div class="card-body">   
     <div class="row"> 
        <div class="col-md-12">
          <img src="{{ URL::asset($item->image_url) }}"   width="100%">
        </div> 
     
     <div class='col-md-12  '>
    <hr/>
    <form action="{{ action('Admin\AdminDashboardController@updateSlidePosition') }}" method="post">
 {{  @csrf_field() }} 

  <div class="form-row align-items-center">
    <div class="col-auto">
      Display Position:
    </div>
    <div class="col-auto">
      <label class="sr-only" for="position{{  $item->id }}">Display Position</label>
      <input type="number" min="0" max="10" value="{{  $item->position }}" class="form-control mb-2" id="position{{  $item->id }}" placeholder="Slider display position"  name='position'>
    </div>

  

   
    <div class="col-auto">
      <input type="hidden" value="{{  $item->id }}"   name='key'>
      <button type="submit" value='update' class="btn btn-primary btn-sm mb-2">Update</button>
    </div>
  </div>
</form>

 </div> 

 <div class='col-md-12 mt-2'>  <hr/> 
    <ul class="list-group list-group-flush">
   <li class="list-group-item d-flex justify-content-between align-items-center">
      Slider Item Is Visible
       <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input btnChangeVisility"   data-key='{{$item->id }}' id="blockSwitchON{{ $item->id }}" 
            <?php  if($item->published == "yes") echo "checked";  ?>  />
            <label class="custom-control-label" for="blockSwitchON{{$item->id }}">Yes</label>
          </div> 
    </li>
</ul>


   </div>

     </div>


  </div>  
</div> 
</div> 

  @endforeach

  

</div>
 

 <form action="{{ action('Admin\AdminDashboardController@deleteSlide') }}" method="post">
 {{  @csrf_field() }}
  
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" aria-labelledby="modalConfirmDelete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalConfirmDelete">Confirm Slider Deletion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure? you want to delete particular photo!
         
      </div>
      <div class="modal-footer">
        <input type="hidden" value="" id="key1" name="key">
        <button type="submit" class="btn btn-primary">Proceed</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> 
 
</form>



 <div class="modal fade" id="modalConfirmSlider" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Admin\AdminDashboardController@toggleSliderStatus') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirm Slider Image Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to update slider image visibility for website?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key2'/>
        <input type='hidden' name='status' id='status'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>


@endsection
@section("script")

<script type="text/javascript">
 

$(document).on("click", ".btnDelete", function()
{

    $("#key1").val($(this).attr("data-key"));
    $("#modalConfirmDelete").modal("show")

});


 


$(document).on("change", ".btnChangeVisility", function()
{
    var status   = $(this).is(":checked");
    $("#key2").val($(this).attr("data-key"));
    $("#status").val( status );  
    $("#modalConfirmSlider").modal("show") 

})


 
</script>
@endsection