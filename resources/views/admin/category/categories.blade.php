@extends('layouts.admin_theme_02')
@section('content')
<div class="row"> 
  <div class="col-md-12">
   <div class="card card-default">
     <div class="card-header"> 
       <div class="card-title" >
         <div class="title">Category List:</div>
       </div>       
     </div>
     <div class="card-body"> 
       <div class="card-group grid" id="listofimage">
         @foreach($data as $item)
         <div class="col-md-2 mb-10">
          <div class="card ">
            <div class="card-body mb-10 well span2 tile">
             <h7 class="text-center"><small>{{$item->display_order}}</small>.<small>{{$item->name}}</small></h7> 
             <hr>
             <img src="{{$item->icon_url}}" width="100px" class="img-fluid rounded-circle img{{$item->id}} nextimage 
             alt="{{$item->display_order}}" 
             data-key="{{$item->id}}" 
             data-order="{{$item->display_order}}"  
             data-index="{{$loop->iteration}}" 
             data-newindex="">
           </div>
         </div>
       </div>
       @endforeach
     </div>
   </div>
 </div>
</div>
</div> 
@endsection
@section("script")
<script>
  $(".grid").sortable({
    tolerance: 'pointer',
    revert: 'invalid',
    placeholder: 'span2 well placeholder tile',
    forceHelperSize: true,
        //function to call ajax parts
        stop: function(event, ui) {
          var key = ui.item.find('img').attr("data-key");

          var counter= 1;
          $("#listofimage img").each(function () {

            newId = $(this).attr("data-index") + counter;
            $(this).attr("data-newindex", counter);
            counter++;

          });  

          var order_list = $(this).find('.img'+key).attr('data-order');
          var generated_index = $(this).find('.img'+key).attr('data-newindex');

          $.ajax({
           url: '/admin/system/update-categories-by-drag',
           type: 'get',
           data: {'code': key,
           'orderlist':order_list,
           'neworderlist':generated_index
         },
         dataType: 'json',
         contentType: 'application/json',
         success: function(response){ 
           if (response.success) {
            $("#listofimage").empty().append(response.html);
          }else{
           alert("failed");
         }
       }
     });
        }

        //ends here

      });

    </script> 

    @endsection 