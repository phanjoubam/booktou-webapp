 @foreach($data as $item)
 

 <div class="col-md-2 ">
  <div class="card ">

    <div class="card-body mb-10 well span2 tile">
       
     <h7 class="text-center"><small>{{$item->display_order}}</small>.<small>{{$item->name}}</small></h7> 
       <hr>
     <img src="{{$item->icon_url}}" class="img-fluid rounded-circle img{{$item->id}} nextimage" 
     alt="{{$item->display_order}}" 
     data-key="{{$item->id}}" 
     data-order="{{$item->display_order}}"  
     data-index="{{$loop->iteration}}" 
     data-newindex="">
     
  </div>

   </div>
  </div>
 
 
@endforeach