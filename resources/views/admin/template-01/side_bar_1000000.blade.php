 <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            
            <li class="nav-item">
              <a class="nav-link" href="{{  URL::to('/admin' )}}">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Orders</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu"> 
                  <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/customer-care/orders/view-all' )}}">
                    Normal Orders
                  </a>
                </li>
         
        

           <li class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/pick-and-drop-orders/view-all' )}}">
              Pick-And-Drop (PnD)
            </a>
          </li> 
 <li class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/orders/assists/view-all' )}}">
             Assist Orders
            </a>
          </li> 

          
           <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/customer-care/callback-requests' )}}">
                    Callback Requests
                  </a>
                </li>
 <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/customer-care/quotation-requests' )}}">
                    Quotation Enquiry
                  </a>
                </li> 
          <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/normal-orders/stats' )}}"> Order Stats Reports</a>
          </li>
<li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/orders/remarks-and-feedback' )}}"> Remarks and Feedbacks</a>
          </li>

          <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/pick-and-drop-orders/prepare-allocation-table' )}}"> Route Discovery Checker</a>
          </li>

  <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/orders/route-with-tasks-table' )}}"> Route and Tasks Table</a>
          </li>

                </ul>
              </div>
            </li>


<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-booking" aria-expanded="false" aria-controls="ui-booking">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Bookings</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-booking">
                <ul class="nav flex-column sub-menu">
                 <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/booking' )}}">
                   Bookings  Dashboard</a>
                </li> 
                  <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/services/view-merchant-wise-booking' )}}">
                    All Bookings
                  </a>
                </li>
                <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/services/sns-business-list' )}}">
                   Service Category
                  </a>
                </li> 
                <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/services/view-oncall-orders' )}}">
                   On-Call Booking
                  </a>
                </li>

                </ul>
              </div>
   </li>




<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-booking" aria-expanded="false" aria-controls="ui-booking">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Event Tickets &amp; Campaign</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-booking">
                <ul class="nav flex-column sub-menu">
 

                <li class="nav-item">
                  <a class="nav-link" href="{{  URL::to('/admin/ticket-count' )}}">
                    <i class="menu-icon typcn typcn-document-text"></i>
                    <span class="menu-title">Event Ticketing</span>
                  </a>
                </li>

                <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/events/ticketing/send-whatsapp-event-invite' )}}">
                    Sent Whatsapp Campaign
                  </a>
                </li>
              </ul>
         
         </div>
   </li>





   <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#ui-appconfig" aria-expanded="false" aria-controls="ui-appconfig">
      <i class="menu-icon typcn typcn-coffee"></i>
      <span class="menu-title">App Screens Configs</span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="ui-appconfig">
      <ul class="nav flex-column sub-menu">   


              <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/app-screen/manage-app-screen' )}}">
                   Manage Screen
                  </a>
                </li>





                <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/app-landing-screen-sections' )}}">
                   Landing Screen Sections
                  </a>
                </li>


                <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/manage-landing-section-contents' )}}">
                   Screen Section Contents
                  </a>
                </li>
               

               
       <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/manage-landing-screen-sections-editor' )}}">
                   CRS Section Editor
                  </a>
                </li>


       <li  class="nav-item">
        <a class="nav-link"  href="{{  URL::to('/admin/customer-care/all-car-rental-service' )}}">
         All CRS Services
       </a>
     </li>

   </ul>
 </div>
</li>







<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-02" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Businesses</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-02">
                <ul class="nav flex-column sub-menu"> 
          
         <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/view-all' )}}">
             View Businesses
            </a>
          </li>

          <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/view-promotion-businesses' )}}">
              Premium Businesses
            </a>
          </li>
          <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('admin/business/add-premium-business' )}}">
            Add Premium Businesses
            </a>
          </li>
           <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('admin/business/view-premium-business' )}}">
            View Premium Businesses
            </a>
          </li>
          <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/normal-orders/best-performers' )}}">
              Best Performers
            </a>
          </li>
  
         <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/pnd-orders/best-performers' )}}">
              Best PnD Performers
            </a>
          </li>
         <!--  <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/user-password-add' )}}">
             Add Password
            </a>
          </li>
          <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/admin/view-user-password' )}}">
             Change Password
            </a>
          </li> -->
                 <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/customer-care/car-rental-services-view-all' )}}">
                   Car Rental Service
                  </a>
                </li>

                <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/merchants/view-all-complaints' )}}">
                   Merchant's complaint
                  </a>
                </li>
                </ul>
              </div>
            </li>

<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-012" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">POS</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-012">
                <ul class="nav flex-column sub-menu"> 
                <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/pos/merchant-wise-report' )}}">
                     POS Merchant
                  </a>
                </li>
              </ul>
              </div>
            </li>

<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-010" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Franchise</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-010">
                <ul class="nav flex-column sub-menu"> 
                <li  class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/view-all-franchise' )}}">
                     View Franchise
                  </a>
                </li>
              </ul>
              </div>
            </li>
  
<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-03" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Agents &amp; Staffs</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-03">
                <ul class="nav flex-column sub-menu"> 
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/staffs/manage-attendance' )}}">
               Staff Attendance
            </a>
          </li> 
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/agents/active-attendance' )}}">
               Active Agents Attendance
            </a>
          </li>
        <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/delivery-agents' )}}">
               Monthly Stats
            </a>
          </li>  
 
        <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/delivery-agent/performance-snapshot' )}}">
              Agents Performance
            </a>
          </li> 
              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/staff/enter-salary-page' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i>Staff Salary Entry
                </a>
              </li>


              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/staff/view-staff-salary' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View Salary Record
                </a>
              </li>
              

<li class="nav-item">
            <a class="nav-link"  target='_blank' href="{{  URL::to('/admin/customer-care/delivery-agents-live-locations' )}}">
              Live Location
            </a>
          </li>

 <li class="nav-item">    
   <a class="nav-link" href="{{  URL::to('/admin/delivery-agent/combined-daily-collection-report' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Delivery Report
                </a>
          </li> 

<li class="nav-item">    
   <a class="nav-link" href="{{  URL::to('/admin/customer-care/delivery-agents/monthly-report' )}}">
                  <i class="now-ui-icons ui-1_bell-54"></i> Monthly Delivery Report
                </a>
          </li> 
          <li class="nav-item">    
   <a class="nav-link" href="{{  URL::to('/admin/agents/agent-advance-amount-entry' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Advance Amount Entry
                </a>
          </li> 

              <li class="nav-item">    
   <a class="nav-link" href="{{  URL::to('/admin/agents/view-agent-advance-history' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i>View Advance Amount
                </a>
          </li>
                </ul>



 




              </div>
            </li>

<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-cst" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Customers</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-cst">
                <ul class="nav flex-column sub-menu"> 
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/systems/search-customers' )}}">
               All Customers
            </a>
          </li> 
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/customer/view-feedback' )}}">
               Order Feedback
            </a>
          </li> 
          <li class="nav-item">
            <a class="nav-link" href="{{URL::to('/admin/customer/view-customer-analytics')}}">
               Customer Analytics
            </a>
          </li> 
                </ul>
              </div>
            </li>

 <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-05" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Marketing &amp; Analytics</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-05">
                <ul class="nav flex-column sub-menu">  

                   <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/analytics/products/view-frequently-browsed-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Frequently Browsed Products
                </a>
              </li>


 <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/marketing/campaign-area' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View Campaign Areas
                </a>
              </li>

 <li class="nav-item">
                <a  class="nav-link"  href="{{  URL::to('/admin/systems/manage-cloud-messages' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Cloud Message
                </a>
              </li>

       <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/systems/manage-voucher-codes' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Voucher Codes
                </a>
              </li>
 <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/products/view-all-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Manage Featured Products
                </a>
              </li>

              
<li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/customer-care/business/products/view-featured-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View Featured Products
                </a>
              </li>


                </ul>
              </div>
            </li>


<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-04" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
               <span class="menu-title">Accounting</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-04">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item">
                    <a class="nav-link"  href="{{  URL::to('/admin/accounts/view-accounts-dashboard' )}}">
                      <i class="now-ui-icons ui-1_bell-53"></i> Accounts Snapshot
                    </a>
                  </li> 

                  <li class="nav-item">
                    <a class="nav-link"  href="{{  URL::to('admin/accounts/gst-itr-search')}}">
                      <i class="now-ui-icons ui-1_bell-53"></i> GST/ITR
                    </a>
                  </li>  

                  <li class="nav-item">
                    <a class="nav-link"  href="{{  URL::to('/admin/accounts/add-deposit' )}}">
                      <i class="now-ui-icons ui-1_bell-53"></i> New Deposit
                    </a>
                  </li> 

                  <li class="nav-item">
                    <a class="nav-link"  href="{{  URL::to('/admin/accounts/add-expenditure' )}}">
                      <i class="now-ui-icons ui-1_bell-53"></i> Expenditure Entry
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link"  href="{{  URL::to('/admin/accounts/view-expenditure' )}}">
                      <i class="now-ui-icons ui-1_bell-53"></i> View Expenditure
                    </a>
                  </li>


                   <li class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/accounts/account-ledger-entry' )}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> Daily Ledger Entry
                  </a>
              </li>

              <li class="nav-item">
                  <a class="nav-link"  href="{{  URL::to('/admin/accounts/search-missing-order' )}}">
                    <i class="now-ui-icons ui-1_bell-53"></i> Add/Update Missing Order
                  </a>
              </li>


        
 <!--
            <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/report/daily-sales-and-service-for-merchant' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Sales by bookTou
                </a>
            </li>

           
         <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/accounting/prepare-monthly-taxes' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Monthly Accounts Report
                </a>
              </li>

            -->
  

              
                </ul>
              </div>
            </li>


<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-04" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Sales Stats</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-04">
                <ul class="nav flex-column sub-menu">  

        <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/report/daily-sales-and-service' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales Volume
                </a>
            </li>
             <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/report/monthly-earning' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Monthly Sales &amp; Earning
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/billing-and-clearance/daily-sales' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Daily Sales &amp; Service
                </a>
              </li>

               <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/report/sales-and-service-per-cycle' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Weekly Sales &amp; Service 
                </a>
            </li>
 
                </ul>
              </div>
            </li>



<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-07" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Billing &amp; Clearance</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-07">
                <ul class="nav flex-column sub-menu"> 
          
             <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/billing-and-clearance/weekly-sales-and-services-report' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Weekly Report
                </a>
              </li>
               <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/billing-and-clearance/merchant-clearance-billing-reports' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Merchant clearance
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/report/pending-payments' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Payment Dues
                </a>
              </li> 

              <li class="nav-item">
                <a class="nav-link"  href="{{  URL::to('/admin/sales-and-clearance-history' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Clearance History
                </a>
              </li>



                </ul>
              </div>
            </li>

<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-08" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">App Section</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-08">
              <ul class="nav flex-column sub-menu"> 
              <li class="nav-item">
                <a class="nav-link"  href="{{URL::to('/admin/systems/app-section')}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Add App Section
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  href="{{URL::to('/admin/systems/view-app-section')}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View App Section
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  href="{{URL::to('/admin/systems/app-section-details')}}">
                  <i class="now-ui-icons ui-1_bell-53"></i>App Section Details
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  href="{{URL::to('/admin/systems/view-app-section-details')}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View Section Details
                </a>
              </li>
              </ul>
              </div>
</li>


          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/systems/manage-business-categories' )}}" >
              Systems
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{  URL::to('/admin/cms' )}}">
                      CMS
            </a> 
          </li> 
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-09" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Rating </span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-09">
              <ul class="nav flex-column sub-menu"> 
              <li class="nav-item">
                <a class="nav-link"  href="{{URL::to('/admin/systems/app-section')}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Rating Section
                </a>
              </li>             
              </ul>
              </div>
                </li>

                <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-09" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Category </span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-09">
              <ul class="nav flex-column sub-menu"> 
              <li class="nav-item">
                <a class="nav-link"  href="{{URL::to('/admin/systems/add-popular-category')}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Add Category
                </a>
              </li> 
              <li class="nav-item">
                <a class="nav-link"  href="{{URL::to('/admin/systems/popular-category-record')}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View Category Details
                </a>
              </li>            
              </ul>
              </div>
                </li>

          </ul>
        </nav>
