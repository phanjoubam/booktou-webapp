<script src="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/js/vendor.bundle.base.js"></script>  
<script src="{{ URL::to('/public/assets/admin') }}/js/core/bootstrap.min.js"></script> 
<script src="{{ URL::to('/public/assets/admin') }}/js/core/popper.min.js"></script>
<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/graph.js"></script>  
<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/jquery.cookie.js" type="text/javascript"></script> 
<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/off-canvas.js"></script>
<script src="{{   URL::to('/public/assets/vendor/select2/js/select2.js')   }}"></script>
<script src="{{ URL::to('/public/assets/vendor') }}/jexcel/jexcel.js"></script> 
<script src="{{ URL::to('/public/assets/vendor') }}/jsuites/dist/jsuites.js"></script> 
<script src="{{ URL::to('/public/assets') }}/vendor/pn/js/pignose.calendar.full.min.js" type="text/javascript"></script>  
<script src="{{ URL::to('/public/assets') }}/admin/js/core/acore.js"></script> 
<script src="{{   URL::to('/public/assets/vendor/jquery-ui/jquery-ui.js')   }}"></script> 
 
 


@if(Session::has('__member_id_'))
   
<script type="module">


    import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.10.0/firebase-app.js'
    import { getAnalytics } from 'https://www.gstatic.com/firebasejs/9.10.0/firebase-analytics.js'  
    import {  getMessaging, getToken  } from 'https://www.gstatic.com/firebasejs/9.10.0/firebase-messaging.js'

 
 
	const firebaseConfig = {
		apiKey: "AIzaSyC-FH-ZY6Uv-r07slY11O0BYH1twtcQJA8",
		authDomain: "aptclassfcm.firebaseapp.com",
		projectId: "aptclassfcm",
		storageBucket: "aptclassfcm.appspot.com",
		messagingSenderId: "341747945802",
		appId: "1:341747945802:web:048c9584f03555d6e9b1b3",
		measurementId: "G-0BGEYE8G4Q"
	};

	// Initialize Firebase
	const app = initializeApp(firebaseConfig);
	const analytics = getAnalytics(app);
	const messaging = getMessaging(app); 
 	var aurl = "<?php echo config('app.url'); ?>";
	getToken(messaging, { vapidKey: 'BDIM1dbKX3lA5-rijCb0g_5YY2ffawEAruhb1CR9au2-8vjUZhbKMJU0XllVAYcNHpHU3x2zvEh6KwL_XqzYESU' }).then((currentToken) => {

		if (currentToken) {

			var json = {};
		    json['memkey'] = "{{  Session::get('__member_id_')  }}"; 
		    json['fcmtoken'] = currentToken;
		    json['_token'] = "{{csrf_token()}}";

		    $.ajax({
		    	url : url +'/fcm/update-user-token',
		    	data:  json,
		    	type: 'post', 
		    	success: function (response) { }
		    });

		}
		else
		{
			console.log('No registration token available. Request permission to generate one.');
	    
		}
		
	}).catch((err) => {
		console.log('An error occurred while retrieving token. ', err);

	});

</script>


@endif


<script>


	$.toastDefaults = {
	  position: 'bottom-right', 
	  dismissible: true, 
	  stackable: true, 
	  pauseDelayOnHover: true, 
	  style: {
	    toast: '',
	    info: '',
	    success: '',
	    warning: '',
	    error: '',
	  }
  
	};


  $(document).ready(  function()
  { 
  	setInterval(checkNewOrders, 10000);


  	<?php
  	if( isset($totalPndOrders) && $totalPndOrders >=150)
  	{
  		?>
  		$('div.main-panel-dashboard').fireworks();  
  		<?php 
  	}
  	?> 

  });

var url = "<?php echo config('app.url') ?>"; 


$('.btnbusiness').on('change', function() { 
    // alert(this.value); 
    var json = {};
    json['items'] = this.value;
    json['_token'] = "{{csrf_token()}}";    

    
    $.ajax({
    url : url +'/admin/systems/list-business-product' ,
    data:  json,
    type: 'post',

    success: function (response) {
    if (response.success) { 
                //alert(response.code);
                if(response.code==2000)
                {
                	$('.productlist').html(response.html);
                }else{
                       // $('.productlist').empty();
                }                   
    }
    else  {                    
            alert('Something went wrong!');
                    
          }
    }

    });
});

	function checkNewOrders()
	{
		 

		if (typeof( $.cookie('lid') === 'undefined') ) {
		 	var lid  = 0;
		} else {
		   var lid = $.cookie("lid");
		} 
	 
	  
	    var json = {};
	    json['lastScanId'] =  lid;

	    $.ajax({
	      type: 'post',
	      url: api + "v3/web/orders/scan-new-orders" ,
	      data:   json ,
	      success: function(data)
	      {
	        data = $.parseJSON(data);
	        $.cookie('lid',  data.lastId ); 
	        if( data.lastId > lid )
	        {
	        	$.toast({
		  		position:'bottom-right', 
				title: 'bookTou Notification', 
				content: 'There is a new order coming in.',
				type: 'info',
				delay: 120000,
				dismissible: true,
				  img: {
				    src: '<?php echo URL::to("/public/assets/image/notification.png"); ?>',
				    class: 'rounded',
				    title: 'BTN',
				    alt: 'BTN'
				  }
			});	

	        }
	      }
	    });
	}
 
 


</script>

 
 