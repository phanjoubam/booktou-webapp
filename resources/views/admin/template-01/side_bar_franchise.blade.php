 <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            
            <li class="nav-item">
              <a class="nav-link" href="{{  URL::to('/franchise' )}}">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
             

            



<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-02" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Businesses</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-02">
                <ul class="nav flex-column sub-menu"> 
          
         <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/customer-care/business/view-all' )}}">
               View Businesses
            </a>
          </li>
</ul>
</div>
</li>


<li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-item-03" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Products</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-item-03">
                <ul class="nav flex-column sub-menu"> 
          
         <li  class="nav-item">
            <a class="nav-link"  href="{{  URL::to('/franchise/view-all-products' )}}">
               View Products
            </a>
          </li>
</ul>
</div>
</li>

 



 
 
   

 



 

           
          
          </ul>
        </nav>