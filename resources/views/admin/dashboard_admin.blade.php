<?php

if( strcasecmp( request()->query('theme'), 'new') == 0)
{

 $theme = "layouts.admin_theme_03";  
}
else 
{
  $theme = "layouts.admin_theme_02";   
}

?> 
@extends( $theme )
@section('content')

        
        <div class="row">
          <div class="col-lg-3 col-md-6  ">


            <div class="white_card mb_2">


              <div class="white_card_body">
                <div class="card">
                 <div class="card-body">
                  <div class="d-flex">
                    <div class="wrapper">
                      <h3 class="mb-0 font-weight-semibold">@if( isset ($data['pnd_assists_revenue']->serviceFee ) ) 
                        {{ $data['pnd_assists_revenue']->serviceFee }}
                        @else 
                        0.00
                      @endif &#x20b9;</h3>
                      <h5 class="mb-0 font-weight-medium text-primary">PnD Revenue</h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-3 col-md-6 ">

          <div class="white_card mb_2"> 

            <div class="white_card_body">
              <div class="card">
               <div class="card-body">
                <div class="d-flex">
                  <div class="wrapper">
                    <h3 class="mb-0 font-weight-semibold">@if( isset ($data['sales']->serviceFee )) 
                     {{ $data['sales']->serviceFee }}
                     @else 
                     0.00
                   @endif &#x20b9;</h3>
                   <h5 class="mb-0 font-weight-medium text-primary">bookTou Earning</h5>
                   <p class="mb-0 text-muted"></p>
                 </div>
              </div>
            </div>
          </div> 
        </div>
      </div>



    </div>
    <div class="col-lg-3 col-md-6 ">
      <div class="white_card mb_2"> 
        <div class="white_card_body">
          <div class="card">
           <div class="card-body">
            <div class="d-flex">
              <div class="wrapper">
                <h3 class="mb-0 font-weight-semibold">{{ count($data['active_users']) }} Active</h3>
                <h5 class="mb-0 font-weight-medium text-primary">
                  <a href="{{ URL::to('/admin/active-users-activity-history') }}" target='_blank'>Customers</a>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>

  <div class="col-lg-3 col-md-6  ">
    <div class="white_card mb_2"> 

      <div class="white_card_body">
        <a href="{{ URL::to('/admin/orders/remarks-and-feedback') }}" target="_blank">
         <div class="card bg-info white">
           <div class="card-body">
            <div class="d-flex ">
              <div class="wrapper">
                <h3 class="mb-0 font-weight-semibold">{{ $data['total_remark_count']  }}</h3>
                <h5 class="mb-0 font-weight-medium text-primary white">Remarks Recorded</h5>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>



</div>


</div>






@php 
$target = 20; 
$totalOrders = 0; 
$totalPndOrders = 0; 
$totalAssistOrders = 0;
$totalOrdersCompleted =0 ;
$totalPndOrdersCompleted = 0; 
$totalAssistCompleted = 0; 
@endphp




<div class="row mt-5">
  <div class="col-xs-6 col-md-3 grid-margin stretch-card">
    <div class="card card-default">
      <div class="card-body easypiechart-panel">
        <h4>Normal Orders</h4>
        <div class="easypiechart" id="easypiechart-orange" data-percent="{{ (   $totalOrders  * 100 ) /  80  }}" >
          
            <span class="percent h3">
              <a href="{{ URL::to('/admin/customer-care/orders/view-all') }}"> 
              <span class='badge badge-primary'>{{ $data['normal_orders'] }} active</span>
              </a> 
              <span class='badge badge-warning'>60 target</span>
              <a href="{{ URL::to('admin/customer-care/orders/view-completed') }}"> 
              <span class='badge badge-success'>{{$data['normal_completed_orders']}} done</span>
            </a>
            </span>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xs-6 col-md-3">
    <div class="card card-default">
      <div class="card-body easypiechart-panel">
        <h4>Pick-and-Drop Orders</h4>
        <div class="easypiechart" id="easypiechart-teal" data-percent="{{ (   $totalPndOrders  * 100 ) / 170  }}" >
         
            <span class="percent h3">
               <a href="{{ URL::to('/admin/customer-care/pick-and-drop-orders/view-all') }}"> 
              <span class='badge badge-primary'>{{$data['pick_and_drop']}} active</span> 
            </a>
              <span class='badge badge-warning'>150 target</span> 
              <a href="{{ URL::to('/admin/customer-care/pick-and-drop-orders/view-completed') }}">
              <span class='badge badge-success'>{{ $data['total_completed_pick_drop'] }} done</span>
            </a>
            </span>
          </a>
        </div>
      </div>
    </div>
  </div> 

  <div class="col-xs-6 col-md-3">
    <div class="card card-default">
      <div class="card-body easypiechart-panel">
        <h4>Assist Orders</h4>
        <div class="easypiechart" id="easypiechart-teal" data-percent="{{ (   $totalPndOrders  * 100 ) / 50  }}" >
           
            <span class="percent h3">
              <a href="{{ URL::to('/admin/orders/assists/view-all') }}">
              <span class='badge badge-primary'>{{ $data['total_assist_order'] }} active</span>
              </a> 
              <span class='badge badge-warning'>50 target</span>
              <a href="{{ URL::to('admin/orders/assists/view-all/view-completed') }}"> 
              <span class='badge badge-success'>{{$data['total_completed_assist_order'] }} done</span>
            </a>
            </span>
        </div>
      </div>
    </div>
  </div> 

  <div class="col-xs-6 col-md-3">
    <div class="card bg-danger card-default">
      <div class="card-body easypiechart-panel">
        @if(date('d') <=15) 
        <h4 class='white'>Min Target till 15th, {{ date('F') }}</h4>
        <div class="easypiechart"     >
          <span class="percent h3 white">
            {{ $data['pnd_income_achieved']  }} of {{ ( $data['sales_target'] != null ) ? $data['sales_target']->min / 2 : 150000 }} ₹
          </span> 
        </div>
        @else
        <h4 class='white'>Min Target till {{ cal_days_in_month(CAL_GREGORIAN , date('m'),  date('Y') ) }}, {{ date('F') }}</h4>
        <div class="easypiechart"     >
          <span class="percent h3 white">
            {{ $data['pnd_income_achieved']  }} of {{ ( $data['sales_target'] != null ) ? $data['sales_target']->min   : 300000 }} ₹
          </span> 
        </div> 
        @endif
      </div>
    </div>
  </div>


</div>

<div class="row">    
 <div class="col-md-6"> 
  <div class="card card-default">
    <div class="card-header">
      New Customer Signup
    </div>
    <div class="card-body">
      <div class="table-responsive">
       @if(count($data['new_customers'] ) > 0 ) 

       <table class="table table-striped">
        <thead>
          <tr> 
            <th>Customer Name</th>
            <th>Phone</th>
            <th>OTP</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data['new_customers'] as $cust)
          <tr> 
            <td class="text-left">
              <a href="{{ URL::to('/admin/customer/view-complete-profile') }}/{{  $cust->id  }}" target='_blank'>
                {{$cust->fullname }}
              </a> 
            </td>
            <td class="text-left">{{$cust->phone }}</td>
            <td class="text-left"><span class='badge badge-primary'>{{ $cust->otp == -1 ?  "Signup Complete" : $cust->otp  }}</span></td>
            <td class="td-actions text-right">
              {{$cust->locality}}, {{$cust->landmark }}
            </td>
          </tr>

          @endforeach
        </tbody>
      </table>

      @else 
      <p class='alert alert-info'>No New signup today.</p>
      @endif 

    </div>
  </div>
</div> 

<!--    Striped Rows Table  -->
<div class="card card-default mt-4">
  <div class="card-header">
    PnD Customers Converted to User
  </div>
  <div class="card-body">
    <div class="table-responsive">
     @if(count($data['migrated_users'] ) > 0 ) 

     <table class="table table-striped">
      <thead>
        <tr> 
          <th>Customer Name</th>
          <th>Phone</th>
          <th>Address</th>
        </tr>
      </thead>
      <tbody>
        @foreach($data['migrated_users'] as $cust)
        <tr> 
          <td class="text-left">{{$cust->fullname }}</td>
          <td class="text-left">{{$cust->phone }}</td>
          <td class="td-actions text-right">
            {{$cust->locality}}, {{$cust->landmark }}
          </td>
        </tr>

        @endforeach
      </tbody>
    </table>

    @else 
    <p class='alert alert-info'>No New PnD User Migration Today.</p>
    @endif 

  </div>
</div>
</div> 
</div>


<div class="col-md-6 grid-margin main-panel-dashboard ">


  <div class="card">
    <div class="card-header">
      <h4 class="card-title mb-0">Sales Performance Graph</h4>

    </div>
    <div class="card-body"> 

      <canvas class="mt-5" height="120" id="sales-statistics-overview"></canvas>
    </div>
  </div>

  <div class="card mt-4">
    <div class="card-header">
      <h4 class="card-title mb-0">Order Cancellation Rate</h4>
    </div>
    <div class="card-body">  
     <canvas id="barCancelChart" style="min-height:250px"></canvas> 
   </div>
 </div>

 <div class="card mt-4">
  <div class="card-header">
    <h4 class="card-title mb-0">bookTou Earning Graph</h4>

  </div>
  <div class="card-body"> 
    <div class="d-flex justify-content-between align-items-center pb-4">
      <h4 class="card-title mb-0">Delivery Revenue</h4>
      <div id="bar-traffic-legend"></div>
    </div>

    <canvas id="barChart" style="height:350px"></canvas>

  </div>
</div>

</div>
</div>

@endsection



@section("script")

<script> 


  $(function() {
    'use strict'; 

    var lineStatsOptions = {
      scales: {
        yAxes: [{
          display: false
        }],
        xAxes: [{
          display: false
        }]
      },
      legend: {
        display: false
      },
      elements: {
        point: {
          radius: 0
        },
        line: {
          tension: 0
        }
      },
      stepsize: 100
    }



    if ($('#sales-statistics-overview').length) {
      var salesChartCanvas = $("#sales-statistics-overview").get(0).getContext("2d");
      var gradientStrokeFill_1 = salesChartCanvas.createLinearGradient(0, 0, 0, 450);
      gradientStrokeFill_1.addColorStop(1, 'rgba(255,255,255, 0.0)');
      gradientStrokeFill_1.addColorStop(0, 'rgba(102,78,235, 0.2)');
      var gradientStrokeFill_2 = salesChartCanvas.createLinearGradient(0, 0, 0, 400);
      gradientStrokeFill_2.addColorStop(1, 'rgba(255, 255, 255, 0.01)');
      gradientStrokeFill_2.addColorStop(0, '#14c671');

      <?php 
      
      $dates = array(); 
      $normal_orders  = array();
      $pnd_orders  = array();
      foreach($data['pnd_sales_chart_data'] as $item)
      {

        $dates[] =  '"'.  $item->serviceDate . '"' ;
        $pnd_orders[] =  $item->pndOrders ;
      }


      foreach($data['normal_sales_chart_data'] as $item)
      { 
        $normal_orders[] =  $item->normalOrder ;
      }

      ?>  

      var data_1_1 = [ <?php echo implode(',', $pnd_orders) ; ?> ];
      var data_1_2 = [ <?php echo implode(',', $normal_orders) ; ?> ];
      var areaData = {
        labels: [ <?php echo implode(",",  $dates ); ?>],
        datasets: [{
          label: 'PnD Orders',
          data: data_1_1,
          borderColor: infoColor,
          backgroundColor: gradientStrokeFill_1,
          borderWidth: 2
        }, {
          label: 'Normal Orders',
          data: data_1_2,
          borderColor: successColor,
          backgroundColor: gradientStrokeFill_2,
          borderWidth: 2
        }]
      };
      var areaOptions = {
        responsive: true,
        animation: {
          animateScale: true,
          animateRotate: true
        },
        elements: {
          point: {
            radius: 3,
            backgroundColor: "#fff"
          },
          line: {
            tension: 0
          }
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        legend: false,
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul>');
          for (var i = 0; i < chart.data.datasets.length; i++) { 
            text.push('<li>');
            text.push('<span style="background-color:' + chart.data.datasets[i].borderColor + '">' + '</span>');
            text.push(chart.data.datasets[i].label);
            text.push('</li>');
          }
          text.push('</ul></div>');
          return text.join("");
        },
        scales: {
          xAxes: [{
            display: false,
            ticks: {
              display: false,
              beginAtZero: false
            },
            gridLines: {
              drawBorder: false
            }
          }],
          yAxes: [{
            ticks: {
              max: 170,
              min: 0,
              stepSize: 50,
              fontColor: "#858585",
              beginAtZero: false
            },
            gridLines: {
              color: '#e2e6ec',
              display: true,
              drawBorder: false
            }
          }]
        }
      }
      var salesChart = new Chart(salesChartCanvas, {
        type: 'line',
        data: areaData,
        options: areaOptions
      });
      document.getElementById('sales-statistics-legend').innerHTML = salesChart.generateLegend(); 
    } 

  }) ;



  /* bar graph */

  <?php

  $day = $pnd_revenue = array();
  foreach($data['pnd_sales_earning_data'] as $item)
  {
    $day[] =  '"'.  date('d, M', strtotime($item->serviceDate)) . '"' ;
    $pnd_revenue[] =  $item->totalRevenue ;
  }



  ?> 


  if ($("#barChart").length) {

    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: {


        labels: [ <?php echo implode(",",  $day ); ?> ],
        datasets: [{
          label: 'Revenue',
          data: [<?php echo implode(",",  $pnd_revenue ); ?> ],
          backgroundColor: ChartColor[0],
          borderColor: ChartColor[0],
          borderWidth: 0
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Sales by date',
              fontSize: 12,
              lineHeight: 2
            },
            ticks: {
              fontColor: '#bfccda',
              stepSize: 50,
              min: 0,
              max: 150,
              autoSkip: true,
              autoSkipPadding: 15,
              maxRotation: 0,
              maxTicksLimit: 10
            },
            gridLines: {
              display: false,
              drawBorder: false,
              color: 'transparent',
              zeroLineColor: '#eeeeee'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Earning by delivery',
              fontSize: 12,
              lineHeight: 2
            },
            ticks: {
              display: true,
              autoSkip: false,
              maxRotation: 0,
              fontColor: '#bfccda',
              stepSize: 400,
              min: 0 
            },
            gridLines: {
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul>');
          for (var i = 0; i < chart.data.datasets.length; i++) { 
            text.push('<li>');
            text.push('<span style="background-color:' + chart.data.datasets[i].backgroundColor + '">' + '</span>');
            text.push(chart.data.datasets[i].label);
            text.push('</li>');
          }
          text.push('</ul></div>');
          return text.join("");
        },
        elements: {
          point: {
            radius: 0
          }
        }
      }
    });
    document.getElementById('bar-traffic-legend').innerHTML = barChart.generateLegend();
  }



  /* cancellation rate graph */ 
  <?php 
  $day = $cancel_rate = array();
  foreach($data['order_cancel_rate'] as $item)
  {
    $day[] =  '"'.  date('d, M', strtotime($item->serviceDate)) . '"' ;
    $cancel_rate[] =  $item->totalCancelled ;
  } 
  ?> 




  if ($("#barCancelChart").length) {

    var barCancelChartCanvas = $("#barCancelChart").get(0).getContext("2d");
    var barCancelChart = new Chart(barCancelChartCanvas, {
      type: 'bar',
      data: {


        labels: [ <?php echo implode(",",  $day ); ?> ],
        datasets: [{
          label: 'Cancellation Rate',
          data: [<?php echo implode(",",  $cancel_rate ); ?> ],
          backgroundColor: '#ff0000',
          borderColor: '#ff0000',
          borderWidth: 0
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Order dates',
              fontSize: 12,
              lineHeight: 2
            },
            ticks: {
              fontColor: '#bfccda',
              stepSize: 50,
              min: 0,
              max: 150,
              autoSkip: true,
              autoSkipPadding: 15,
              maxRotation: 0,
              maxTicksLimit: 10
            },
            gridLines: {
              display: false,
              drawBorder: false,
              color: 'transparent',
              zeroLineColor: '#eeeeee'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Cancellation Count',
              fontSize: 12,
              lineHeight: 2
            },
            ticks: {
              display: true,
              autoSkip: false,
              maxRotation: 0,
              fontColor: '#bfccda',
              stepSize: 5,
              min: 0 
            },
            gridLines: {
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul>');
          for (var i = 0; i < chart.data.datasets.length; i++) { 
            text.push('<li>');
            text.push('<span style="background-color:' + chart.data.datasets[i].backgroundColor + '">' + '</span>');
            text.push(chart.data.datasets[i].label);
            text.push('</li>');
          }
          text.push('</ul></div>');
          return text.join("");
        },
        elements: {
          point: {
            radius: 0
          }
        }
      }
    });
    document.getElementById('bar-cancel-legend').innerHTML = barChart.generateLegend();
  }


</script> 
@endsection