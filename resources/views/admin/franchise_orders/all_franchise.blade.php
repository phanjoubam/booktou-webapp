@extends('layouts.admin_theme_02')
@section('content')

<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 

         <div class="card-title" >
         <div class="title">Franchise List:</div>
        </div> 
       
       </div>

       <div class="card-body"> 

        
                 
       		 <div class="card-group">


 @foreach($data as $item)
  

 <div class="col-md-6">
  <div class="card">

  	<div class="card-body">
  		<div class="row">
		<div class="col-md-8">
		   <h3>{{$item->zone}}</h3>
		</div>

		<div class="col-md-4 text-right">
			<div class="dropdown">
 				<a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                </a>
                <ul class="dropdown-menu dropdown-user pull-right"> 
    			
    			<li><a class="dropdown-item" href="{{ URL::to('/admin/show-franchise/franchise-monthly-orders-details')}}?franchise={{$item->frno}}">Monthly Orders Transaction</a></li>
				
    			<li><a class="dropdown-item" 
    			href="{{ URL::to('/admin/show-franchise/franchise-daily-orders-details') }}?franchise={{$item->frno}}">Daily Orders Transaction</a></li>

          <li><a class="dropdown-item" 
          href="{{ URL::to('/admin/show-franchise/view-routed-order')}}?franchise={{$item->frno}}">View Routed Orders</a></li>

				</ul>

			</div>
		</div> 
</div>
   <hr>
   <p>Phone: <span>{{$item->phone}}</span></p>
   <p>Status: <span>{{$item->current_status}}</span></p>

    </div>

  </div>
</div>
 
@endforeach
</div>

              </div>
            </div>
          </div>
        </div>








 





@endsection
@section("script")
<script>


 
 

 


</script>
@endsection 