@extends('layouts.admin_theme_02')
@section('content')

<?php  
$totalOnline = $totalPnd = $totalAssist = $totalAmount = 0.00;
$normalCount= $pndCount = $assistCount =0;
$ntype ="normal"; 
$ptype = "pnd";
$atype ="assist";
?>

@foreach($all_orders as $normal)

 @if($normal->status == "delivered" || $normal->status == "completed") 

                  <!-- checking normal order -->

                        @if($normal->type == "normal")
               						@php	

               						$totalOnline += $normal->amount;  
               						 
               						$normalCount++;
               						@endphp
                    <!-- normal order check ends here -->
                    @elseif($normal->type=="pnd")

                    <!-- pnd section checks starts from here -->
                    @php 
                    	$totalPnd += $normal->amount;  
                    	 
                    	$pndCount++;
                    @endphp
                     <!--  pnd section ends here -->
					
                          @else  
                          
                           @php 
                           $totalAssist += $normal->amount; 
                           
                           $assistCount++;
                           @endphp  
                            <!-- pnd section ends here -->
                            @endif
 


           @endif


@endforeach
<div class="row"> 
	<div class="col-md-12 mb-2">
   <div class="card card-body"> 

         <div class="card-title" >
         <div class="row">
         	<div class="col-md-8">
          <div class="title" > <h5>Routed Orders Dashboard For <span class="badge badge-warning">
           {{$selected_franchise->zone}}
          </span>:
           @if(isset($selected_franchise))

           <span class="badge badge-info">
           {{$monthname}} , {{$year}} 
          </span> 
          
          @endif
          </h5> 

           
          	
          </div>
      </div>
      	<div class="col-md-4">
      	<div class="row">
      		 <form class="form-inline" method="get" 
        action="{{ action('Admin\AdminDashboardController@viewRoutedOrderDashboard') }}"   >
              {{csrf_field()}}
              <input type="hidden" value="{{request()->get('franchise')}}" name="franchise">
          <div class="col-md-4" >  

          <select name='month' class="form-control form-control-sm  ">
          <option value='1'>January</option>
          <option value='2'>February</option>
          <option value='3'>March</option>
          <option value='4'>April</option>
          <option value='5'>May</option>
          <option value='6'>June</option>
          <option value='7'>July</option>
          <option value='8'>August</option>
          <option value='9'>September</option>
          <option value='10'>October</option>
          <option value='11'>November</option>
          <option value='12'>December</option>

          </select>
          </div>

          <div class="col-md-4 ml-3">
         <select name='year' class="form-control form-control-sm  ">
          <?php 
                  for($i=date('Y'); $i >=2020; $i--)
                   {
                    ?>
                  <option value='{{ $i }}'>{{ $i }}</option>
                   <?php 
                 }
                 ?>

        </select>

          </div>
          <div class="col-md-2">
          	<button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
          </div>
		 </form>
      	 </div>
		</div>       
      	 </div>
        </div> 
       
       </div>
   </div>
<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper">
                          	<h3>
                          	{{$totalOnline}} &#x20b9;
                          	</h3>
                            <a target="_blank" 
                            href="{{URL::to('admin/show-franchise/routed-order-details')}}?franchise={{request()->get('franchise')}}&year={{$year}}&month={{$month}}&otype={{$ntype}}">
                            	<h5 class="mb-0 font-weight-medium text-primary">Normal Orders</h5>
                            </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>

<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          	<h3>
                          	{{$totalPnd}} &#x20b9;
                            </h3>
                <a target="_blank" href="{{ URL::to('admin/show-franchise/routed-order-details')}}?franchise={{request()->get('franchise')}}&year={{$year}}&month={{$month}}&otype={{$ptype}}"> 
                <h5 class="mb-0 font-weight-medium text-primary">PND Orders</h5></a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          	<h3>
                          	 {{$totalAssist}} &#x20b9;
                          	</h3>
                          <a target="_blank" href="{{ URL::to('admin/show-franchise/routed-order-details')}}?franchise={{request()->get('franchise')}}&year={{$year}}&month={{$month}}&otype={{$atype}}">
                          <h5 class="mb-0 font-weight-medium text-primary">Assist Orders</h5>
                          </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          <h3> 	 
                          	{{ $totalOnline + $totalPnd +$totalAssist}} &#x20b9;

                          </h3>
                   <a target="_blank" href="{{URL::to('admin/show-franchise/routed-order-details')}}?franchise={{request()->get('franchise')}}&year={{$year}}&month={{$month}}&otype="><h5 class="mb-0 font-weight-medium text-primary">Total Amount </h5>
                           </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6 mt-2">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper">
                          	<h3>
                          	<span class="badge badge-info">{{$normalCount}}</span>
                          	</h3>
                            <a target="_blank" 
                            href="{{URL::to('admin/show-franchise/routed-order-details')}}?franchise={{request()->get('franchise')}}&year={{$year}}&month={{$month}}&otype={{$ntype}}">
                            	<h5 class="mb-0 font-weight-medium text-primary">No Of Normal Orders</h5>
                            </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>

<div class="col-lg-3 col-md-6 mt-2">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          	<h3>
                          	 <span class="badge badge-info">{{$pndCount}}</span>
                            </h3>
                <a target="_blank" href="{{ URL::to('admin/show-franchise/routed-order-details')}}?franchise={{request()->get('franchise')}}&year={{$year}}&month={{$month}}&otype={{$ptype}}"> 
                <h5 class="mb-0 font-weight-medium text-primary">No Of PND Orders</h5></a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6 mt-2">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          	<h3>
                          	 <span class="badge badge-info"> {{$assistCount}} </span>
                          	</h3>
                          <a target="_blank" href="{{ URL::to('admin/show-franchise/routed-order-details')}}?franchise={{request()->get('franchise')}}&year={{$year}}&month={{$month}}&otype={{$atype}}">
                          <h5 class="mb-0 font-weight-medium text-primary">No Of Assist Orders</h5>
                          </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6 mt-2">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          <h3> 	 
                          	<span class="badge badge-info"> {{$normalCount + $pndCount + $assistCount}}
</span>
                          </h3>
                   <a target="_blank" href="{{URL::to('admin/show-franchise/routed-order-details')}}?franchise={{request()->get('franchise')}}&year={{$year}}&month={{$month}}&otype="><h5 class="mb-0 font-weight-medium text-primary">Total Orders </h5>
                           </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>

 

</div>


@endsection
@section("script") 
<script> 
 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
}); 
</script> 
@endsection 