@extends('layouts.admin_theme_02')
@section('content')
<?php
$total_amount =0;
?>

<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 

         <div class="card-title" >
         <div class="row">
         	<div class="col-md-6">
          <div class="title" >
            <h3>Routed Orders for <span class="badge badge-warning">{{$data['selected_franchise']->zone}}</span>:
           @if(isset($data['selected_franchise']))
           <span class="badge badge-info">
           {{$data['monthname']}} , {{$data['year']}} 
          </span>
          @endif
          </h3> 

           
          	
          </div>
      </div>
      	<div class="col-md-6">
      	 
		    </div>       
      	 </div>
        </div> 
       
       </div>
       <div class="card-body"> 
                 <div class="table-responsive">
                  <table class="table table-bordered">
                <thead  class="thead-dark">  
                <tr class="text-center" style='font-size: 12px'>  
                    <th>Order No</th>
                    <th>Order Type</th>
                    <th>Order Date</th> 
                    <th>Payment Info</th> 
                    <th>Status</th>  
                    <th class="text-right">Amount</th>
                </tr>
                </thead>

                @if(isset($data['orders']))

                @foreach($data['orders'] as $item)
        <tbody>
          <tr>
            <td class="text-center">

               
              @switch ($item->type)  
                @case ('normal')
                  <a target='_blank' href="{{URL::to('/admin/customer-care/order/view-details')}}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                @break
                
                @case ('pnd')
                  <a target='_blank' href="{{URL::to('/admin/customer-care/pnd-order/view-details')}}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                @break

                @case ('assist')
                  <a target='_blank' href="{{URL::to('/admin/customer-care/assist-order/view-details')}}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                @break
              @endswitch
               


            </td>
            <td class="text-center">{{$item->type}}</td>
            <td class="text-center">{{date('d-m-Y', strtotime($item->serviceDate))}}</td>
            <td class="text-center">{{$item->status}}</td>

            <th>
            @if( strcasecmp($item->paymentMode, "pay online (upi)") == 0  || strcasecmp($item->paymentMode, "online") == 0    ) 
              <span class='badge badge-danger'>ONLINE</span> 
            @else
               <span class='badge badge-info'>CASH</span> 
            @endif 
             
             {{$item->paymentTarget}}</th> 

            <td class="text-right">{{$item->amount}}</td> 
          </tr>
                </tbody>
               <?php 
               $total_amount +=$item->amount; 
               ?>

                @endforeach
                @endif
                <tfoot>
                  <tr>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right"><span class="badge badge-primary">Total Amount:</span> {{number_format($total_amount, 2)}}</td>
                    
                  </tr>
                </tfoot>
               </table>
              </div>
              </div>
            </div>
          </div>
        </div>




@endsection

@section("script")

<script>


 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

 

 


</script> 

@endsection 