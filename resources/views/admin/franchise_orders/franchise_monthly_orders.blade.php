@extends('layouts.admin_theme_02')
@section('content')
<?php
$total_amount =0;
?>

<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 

         <div class="card-title" >
         <div class="row">
         	<div class="col-md-6">
          <div class="title" > <h3>Monthly Orders:
           @if(isset($data['selected_franchise']))

           <span class="badge badge-info">
           {{$data['monthname']}} , {{$data['year']}} 
          </span>
          <span class="badge badge-warning">
           {{$data['selected_franchise']->zone}}
          </span>

           
          
          @endif
          </h3> 

           
          	
          </div>
      </div>
      	<div class="col-md-6">
      	<div class="row">
      		 <form class="form-inline" method="get" 
        action="{{ action('Admin\AdminDashboardController@franchiseMonthlyOrdersDetails') }}"   >
              {{csrf_field()}}
              <input type="hidden" value="{{request()->get('franchise')}}" name="franchise">
          <div class="col-md-6" >  

          <select name='month' class="form-control form-control-sm  ">
          <option value='1'>January</option>
          <option value='2'>February</option>
          <option value='3'>March</option>
          <option value='4'>April</option>
          <option value='5'>May</option>
          <option value='6'>June</option>
          <option value='7'>July</option>
          <option value='8'>August</option>
          <option value='9'>September</option>
          <option value='10'>October</option>
          <option value='11'>November</option>
          <option value='12'>December</option>

          </select>
          </div>

          <div class="col-md-4">
            
             <select name='year' class="form-control form-control-sm  ">
          <?php 
          for($i= date('Y') ; $i >= 2020; $i--)
          {
            ?> 
            <option value='{{ $i }}'>{{ $i }}</option>
          <?php 
          }
          ?>

        </select>
          </div>
          <div class="col-md-2">
          	<button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
          </div>
		 </form>
      	 </div>
		</div>       
      	 </div>
        </div> 
       
       </div>
       <div class="card-body"> 
                 <div class="table-responsive">
                  <table class="table table-bordered">
                <thead  class="thead-dark">  
                <tr class="text-center" style='font-size: 12px'>  
                    <th>Order No</th>
                    <th>Order Type</th>
                    <th>Order Date</th> 
                    <th>Status</th> 
                    <th class="text-right">Amount</th>
                </tr>
                </thead>

                @if(isset($data['orders']))

                @foreach($data['orders'] as $item)
        <tbody>
          <tr>
            <td class="text-center">

               
              @switch ($item->type)  
                @case ('normal')
                  <a target='_blank' href="{{URL::to('/admin/customer-care/order/view-details')}}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                @break
                
                @case ('pnd')
                  <a target='_blank' href="{{URL::to('/admin/customer-care/pnd-order/view-details')}}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                @break

                @case ('assist')
                  <a target='_blank' href="{{URL::to('/admin/customer-care/pnd-order/view-details')}}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                @break
              @endswitch
               


            </td>
            <td class="text-center">{{$item->type}}</td>
            <td class="text-center">{{date('d-m-Y', strtotime($item->serviceDate))}}</td>
            <td class="text-center">{{$item->status}}</td>
            <td class="text-right">{{$item->amount}}</td>
            
          </tr>
                </tbody>
               <?php 
               $total_amount +=$item->amount; 
               ?>

                @endforeach
                @endif
                <tfoot>
                  <tr>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right"><span class="badge badge-primary">Total Amount:</span> {{number_format($total_amount, 2)}}</td>
                    
                  </tr>
                </tfoot>
               </table>
              </div>
              </div>
            </div>
          </div>
        </div>




@endsection

@section("script")

<script>


 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

 

 


</script> 

@endsection 