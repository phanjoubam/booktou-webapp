@extends('layouts.admin_theme_02')
@section('content')
<div class="row"> 
  <div class="col-md-8 offset-md-2">
     <div class="card card-default">  
         <div class="card-header">
           Add Agent Advance Amount 
        </div> 
        <div class="card-body">
        	<form action="{{action('Admin\AdminDashboardController@addAgentAdvanceAmountEntry') }}" method="post">
            {{csrf_field()}}
        	<div class="row">
                <div class="col-md-4 mb-2"> 
                    <select name='staffName' class="form-control form-control-sm" required>
                        <option value="">--Select Staff--</option>
                    @foreach($staff as $items)
                    <option value="{{$items->id}}" <?php if (request()->get('staffName')==$items->id) { echo "selected"; }?>>{{$items->fullname}}
                    </option>
                    @endforeach

                    </select>
                </div> 
            		<div class="col-md-4 mb-2">
            			 
            			 <select name="category" class="form-control" required>
                    <option value="fuel">Fuel</option>
                    <option value="advance">Advance Amount</option>   
                   </select>
            			 
                </div>

                <div class="col-md-4 mb-2">
                       <input type="text" name="amount" class="form-control" placeholder="0.00" required> 
                </div>

                <div class="col-md-12 mb-2">
                  <textarea class="form-control" name="details" required placeholder="Details for the requested amount"></textarea>
                </div>
                <div class="col-md-4 mb-2">
                    <input type="text" class="calendar form-control" required name="requestdate" placeholder="request date">
                </div>
        		 <div class="col-md-12"><button class="btn btn-primary" type="submit">Save</button></div>
        	</div> 
        </form>
        </div>

 
</div>
</div>
</div>
@endsection
@section("script")
 <script>
   $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
 </script>
@endsection