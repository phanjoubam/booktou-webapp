@extends('layouts.admin_map')
@section('content')

 
 <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-body">
               
                 <div class="row">
                     
                        <div class="form-group">
                            <div class="form-row">
                            <div class="col-12">
                                   <button type="button" class="btn btn-primary" 
                                   data-key="" id="show_staff_list">Add Location</button>
                            </div>
                            <div class="col-4">
                           
                            </div>
                          </div>
                        </div>

                     
                  
             </div>
         </div>
     </div>
 </div>
</div>

@if(!$data['locationlist']->isEmpty())
<div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-body">
               <table class="table">
                   <thead>
                       <th>Staff Name</th>
                       <th>Location</th> 
                   </thead>
                   @foreach($data['locationlist'] as $list)
                   <tbody>
                       <tr>
                        <td>{{$list->fullname}}</td>
                        <td>{{$list->location}}</td>
                       </tr>
                   </tbody>
                   @endforeach

               </table>
                  
         </div>
     </div>
 </div>
</div>
@endif






 

<div class="modal" tabindex="-1" id="showstafflist">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Update Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method='post' action="{{ action('Admin\AdminDashboardController@saveUdpateStaffLocation') }}">
        {{  csrf_field() }}
      <div class="modal-body">
        <div class="form-group">
        <label>Select agent</label>
        <select name="select_agent" value="" id="agentList" class="form-control">
            @foreach($data['agents'] as $agent)  
            <option value="{{$agent->agentid}}">{{$agent->fullname}}</option>
            @endforeach
        </select>
        </div>
        <div class="form-group">
            <label>Enter location</label>
        <input type="text" name="stafflocation" id="staffLocation" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>




@endsection

@section("script")

<script>

   

 
$(document).on("click", "#show_staff_list", function()
{
   

     $("#showstafflist").modal("show")

});

</script> 

@endsection 