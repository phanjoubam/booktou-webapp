@extends('layouts.admin_theme_02')
@section('content')
<?php $total_amount=0; ?>
<div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 


         <div class="card-title">
          <div class="title"> View agent wise advance history </div>
        </div>

        <div class="card-body">
        	<form action="{{action('Admin\AdminDashboardController@viewAgentAdvanceHistory') }}" method="get">
        	<div class="row">
                <div class="col-md-2">
                     
                    <!-- <input type="date" name="filter_date" id="filter_date" class="form-control"> -->
                    <select name='staffName' class="form-control form-control-sm" required>
                        <option value="">--Select Staff--</option>
                    @foreach($staff as $items)
                    <option value="{{$items->id}}" <?php if (request()->get('staffName')==$items->id) { echo "selected"; }?>>{{$items->fullname}}
                    </option>
                    @endforeach

                    </select>
                </div> 
        		<div class="col-md-2">
        			 
        			 
        			<select name='month' class="form-control form-control-sm  ">
                      <option value='1'>January</option>
                      <option value='2'>February</option>
                      <option value='3'>March</option>
                      <option value='4'>April</option>
                      <option value='5'>May</option>
                      <option value='6'>June</option>
                      <option value='7'>July</option>
                      <option value='8'>August</option>
                      <option value='9'>September</option>
                      <option value='10'>October</option>
                      <option value='11'>November</option>
                      <option value='12'>December</option>

                    </select>
                </div>
                <div class="col-md-2">
                    <select name='year' class="form-control form-control-sm  ">
                      <?php 
                      for($i=date('Y'); $i >= 2020; $i--)
                      {
                        ?> 
                        <option value='{{ $i }}'>{{ $i }}</option>
                      <?php 
                      }
                      ?>

                    </select>
                </div>
        		 <div class="col-md-2"><button class="btn btn-primary">Search</button></div>
        	</div> 
        </form>
        </div>

    </div>
</div>

<br>
            @if(isset($result))
                @if(count($result)>0)
            <div class="card card-body">
            <div class="row">
                <div class="col-md-6">
                <p class=""><h5><b>Agents advance history</b></h5></p>
                </div>
                <div class="col-md-6">
                   <span class="badge badge-primary badge-pill  m-2"> Fuel:</span> {{$fuel}} 
                   <span class="badge badge-primary badge-pill  m-2">Advance:</span>{{$salary}}
                   <span class="badge badge-primary badge-pill  p-2 m-2">Total Amount:</span>{{$fuel + $salary}}
                </div>
            </div>
            <hr>
            <table class="table">
                
                    <thead>
                        <tr> 
                            <th>Category</th>  
                            <th>Details</th>
                            <th>Request On</th>
                            <th>Paid On</th>
                            <th>Status</th> 
                            <th>Amount</th>
                        </tr>
                    </thead>
            
                    <tbody>
                        @foreach($result as $items)
                        <tr>
                            <td> {{$items->type}} </td> 
                            <td> {{$items->details}}</td>
                            <td> {{date('Y-m-d'),$items->advanceRequestDate}}</td>
                            <td> {{date('Y-m-d'),$items->advanceTakenDate}}</td>
                            <td> @if($items->requestStatus=="pending")
                                        <span class="badge badge-danger">{{$items->requestStatus}}</span>
                                    @else
                                        <span class="badge badge-success">{{$items->requestStatus}}</span>
                                 @endif
                            </td> 
                            <td> {{$items->amount}} </td> 
                        </tr>
                            <?php $total_amount += $items->amount ;?>
                        @endforeach

                        <tr>
                            <td colspan="5" class="text-right"><span class="badge badge-primary badge-pill">Total Amount:</span></td>
                            <td colspan="2" class="text-left">{{$total_amount}}</td>
                        </tr>
                    </tbody>
            </table>
            </div>
            @else
            <div class="alert alert-info">
                <p>No record found!</p>
            </div>
            @endif
            @endif
</div>
</div> 
@endsection
@section("script")
 
@endsection
