<?php 
    $total =0;   
    $agent = $data['agent_info'];
    $deposited =false; 
?>

@extends('layouts.admin_theme_02')
@section('content') 

 <div class="row">
   <div class="col-md-12"> 
  
   <div class="card "> 
    <div class="card-header">
          
           <div class="row">
            <div class="col-md-7">
                  <h3>{{ date('F', mktime(0, 0, 0,   $data['month'] , 10)) }} @switch($data['cycle'])
                @case(1)
                  <span class='badge badge-info'>First Cycle</span>
                @break
                @case(2)
                  <span class='badge badge-info'>Second Cycle</span>
                @break
                @case(3)
                  <span class='badge badge-info'>Third Cycle</span>
                @break
                @case(4)
                  <span class='badge badge-info'>Fourth Cycle</span>
                @break
                @case(5)
                  <span class='badge badge-info'>Last Cycle</span>
                @break

              @endswitch collection for {{ $agent->fullname  }}</h3> 
            </div>
 
 <div class="col-md-5 text-right">  

         <form class="form-inline" method="get" action="{{ action('Admin\AdminDashboardController@deliveryAgentMonthlyCollectionReport') }}"   >
      <div class="form-row">
       
        <div class="col-md-12"> 
           <select name='cycle' class="form-control form-control-sm  ">
          <option value='1' <?php if( $data['cycle'] == 1 ) echo "selected"; ?> >1st Week</option>
          <option value='2' <?php if( $data['cycle'] == 2 ) echo "selected"; ?>>2nd Week</option>
          <option value='3' <?php if( $data['cycle'] == 3 ) echo "selected"; ?>>3rd Week</option>
          <option value='4' <?php if( $data['cycle'] == 4 ) echo "selected"; ?> >4th Week</option>
          <option value='5' <?php if( $data['cycle'] == 5 ) echo "selected"; ?>>last Week</option>
        </select>

          <select name='month' class="form-control form-control-sm  ">
          <option <?php if( date('m') == 1 ) echo "selected"; ?> value='1'>January</option>
          <option <?php if( date('m') == 2 ) echo "selected"; ?>  value='2'>February</option>
          <option <?php if( date('m') == 3 ) echo "selected"; ?> value='3'>March</option>
          <option <?php if( date('m') == 4 ) echo "selected"; ?> value='4'>April</option>
          <option <?php if( date('m') == 5 ) echo "selected"; ?> value='5'>May</option>
          <option <?php if( date('m') == 6 ) echo "selected"; ?> value='6'>June</option>
          <option <?php if( date('m') == 7 ) echo "selected"; ?> value='7'>July</option>
          <option <?php if( date('m') == 8 ) echo "selected"; ?> value='8'>August</option>
          <option <?php if( date('m') == 9 ) echo "selected"; ?> value='9'>September</option>
          <option <?php if( date('m') == 10 ) echo "selected"; ?> value='10'>October</option>
          <option <?php if( date('m') == 11 ) echo "selected"; ?> value='11'>November</option>
          <option <?php if( date('m') == 12 ) echo "selected"; ?> value='12'>December</option> 
        </select>
        <input readonly name='year' class="form-control form-control-sm  " value="{{ date('Y') }}" /> 
        <input  type='hidden'  name='aid'  value="{{ $agent->id }}" />  
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button> 
        </div> 
    </div>
  
      </form>
</div>

       </div> 
        
   
  </div>

       <div class="card-body"> 
   
      <table class="table table-bordered mb-3">
          <tr class="text-center" style='font-size: 12px'>  
            <th scope="col" class="text-center" >Order No.</th> 
            <th scope="col" class="text-left">Merchant/Customer</th>
            <th scope="col" class="text-center" >Delivery Date</th>
            <th class='text-right'>Order Type</th>    
            <th class="text-center">Order Status</th>  
            <th class="text-right"  width='120px'>Amount</th>                
            <th class="text-right"  width='120px'>Service Fee</th> 
            <th class="text-right"  width='120px'>Commission</th> 
            <th class="text-center" width='120px'>Select</th> 
          </tr>
   
            @php
              $i=1;  
              $totalServiceFee = $totalCommission =  $totalDeposited = $totalcashamount = $totalonlineamount = 0.0; 
            @endphp 

            @foreach ($data['all_orders'] as $item)
              @php 
                $amountCollected = 0.00;
              @endphp 
              @if($item->member_id == $agent->id )
                <tr >
                  <td class="text-center">
                    @if($item->orderType == "normal")
                    <a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{ $item->orderNo }}">{{ $item->orderNo }}</a> 
                    @elseif($item->orderType == "pnd")
                    <a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                    @else
                    <a target='_blank'  href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$item->orderNo }}">{{$item->orderNo }}</a>
                  @endif
                </td> 
                <td>{{$item->orderBy}}</td>
                <td>{{ date('d-m-Y', strtotime($item->service_date))}}</td>  
                <td class='text-center'>
                    @switch( strtolower($item->orderType) ) 
                          @case("pnd")
                            <span class='badge badge-warning'>PnD</span>
                            @break
                          @case("normal")
                            <span class='badge badge-success'>Normal</span>
                            @break

                          @case("assist")
                            <span class='badge badge-info'>Assist</span>
                            @break
  
                        @endswitch  
                </td>
                <td class="text-center"> 
                      @switch($item->orderStatus)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break
                      @case("returned")
                        <span class='badge badge-danger'>Returned</span>
                      @break

                      @case("cancelled")
                      @case("canceled")
                        <span class='badge badge-danger'>Cancelled</span>
                      @break
                       @case("in_route")
                        <span class='badge badge-info'>In Route</span>
                      @break

                      @case("completed")
                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
 
                        <?php 

                          if(  $item->source == "booktou" || $item->source == "customer"  )
                          {
                              if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00;
                                $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                              else //cash collection
                              {
                                $amountCollected = $item->totalAmount + $item->serviceFee ;
                                $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                          }
                          else if(  $item->source == "business" || $item->source == "merchant"  )
                          {
                            if( strcasecmp($item->payMode, "online" ) == 0  || 
                                  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                              {
                                $amountCollected = 0.00; 
                              }
                              else //cash collection
                              {
                                $amountCollected = $item->totalAmount + $item->serviceFee ;
                                $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;
                              }
                          }

                         ?>
 
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-info'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>   
                     
                   
                    <td class='text-right'  width='120px'>{{ number_format( $amountCollected, 2, '.', '')  }}</td>
                    <td class='text-right'  width='120px'>{{ number_format( $item->serviceFee, 2, '.', '')  }}</td> 
                    @php 
                      $order_comm = 0.00;
                    @endphp
                    @foreach($data['order_commissions'] as $commission) 
                          @if($item->orderNo == $commission->order_no) 
                            @php 
                              $order_comm = $commission->commission;
                            @endphp
                            @break
                          @endif
                    @endforeach

                    <td class='text-right'  width='120px'>{{ number_format(  $order_comm , 2, '.', '')  }}</td>

                  @php 
                    $totalCommission += $order_comm 
                  @endphp

                    <td class='text-center'  width='120px'>
                      @if($item->isDeposited =="yes")
                        @if($amountCollected  <=0 )
                          <span class='badge badge-danger badge-pill'>Ignored</span>
                        @else
                          @php
                            $deposited =true;
                            $totalDeposited += $amountCollected;
                          @endphp  
                          <span class='badge badge-success badge-pill'>Deposited</span>
                        @endif
                      @else 
                      <input type='checkbox' class='select' data-value="{{ $amountCollected}}" name='ordernos[]'  @if($amountCollected==0)  checked readonly  @endif value="{{  $item->orderNo }}" />
                      @endif
                     </td> 
 


                  </tr> 
                  @endif

                @php 
                  $totalServiceFee += $item->serviceFee; 
                  $i++;
                @endphp
            @endforeach
            
            <tr class="text-center" style='font-size: 12px'>  
            <th scope="col" class="text-center" >Order No.</th> 
            <th scope="col" class="text-left">Merchant/Customer</th>
            <th scope="col" class="text-center" >Delivery Date</th>
            <th class='text-right'>Order Type</th>    
            <th class="text-center">Order Status</th>  
            <th class="text-right"  width='120px'>Amount</th>                
            <th class="text-right"  width='120px'>Service Fee</th> 
            <th class="text-right"  width='120px'>Commission</th> 
            <th class="text-center" width='120px'>Select</th> 
          </tr>
            @if( $deposited )
              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="5" class="text-right">
                <h4>Total to collect from {{ $agent->fullname  }}</h4>
              </th>
              <th scope="col" colspan="1"  > 
                <input type="text" readonly class="form-control form-control-sm form-control-block "  value="{{ number_format( $totalcashamount ,2, '.', '') }}"  > 
              </th> 
              <th scope="col" colspan="1"  > 
                <input type="text" readonly class="form-control form-control-sm form-control-block "  value="{{ number_format( $totalServiceFee ,2, '.', '') }}"  > 
              </th> 

              <th scope="col" colspan="1"  > 
                <input type="text" readonly class="form-control form-control-sm form-control-block "  value="{{ number_format( $totalCommission ,2, '.', '') }}"  > 
              </th> 

               
              <th> </th>
              </tr>  
              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="5" class="text-right">
                <h4>Total deposited by {{ $agent->fullname  }}</h4></th>
              <th scope="col" colspan="1"  > 
                 <input type="text" readonly class="form-control form-control-sm form-control-block  "  value="{{ number_format( $totalDeposited,2, '.', '') }}"  > 
              </th>  
              <th colspan='3'></th>
              </tr> 
            @else 

              <tr  style='font-size: 12px'>  
              <th scope="col" colspan="5" class="text-right">
                <h4>Total to collect from {{ $agent->fullname  }}</h4></th>
              <th scope="col" colspan="1"  > 
                <input type="text" readonly class="form-control form-control-sm form-control-block is-valid"  value="{{ number_format( $totalcashamount ,2, '.', '') }}"  > 
              </th>  
              <th>
                <input type="text" id='totalcollected' name='totalcollected' class="form-control form-control-sm form-control-block  "    > 
                </th>
                <th scope="col" colspan='3'  >  

                </th>
              </tr>     

               

            @endif 
       </table>
   
 
     </div>
    </div>
 </div> 
</div>  

@endsection


 @section("script")

<script>
  
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

$('input[type=checkbox].select').change(function() {
    
    var totalAmt= 0.00;
    $('input[type=checkbox].select').each(function () {
       totalAmt +=  (this.checked) ? parseFloat(  $(this).attr("data-value") ) :  parseFloat(0.00); 
    });

    $("#totalcollected").val(totalAmt); 
 });



</script> 

<style>
input[type="checkbox"][readonly] {
  pointer-events: none;
}
</style>
@endsection 