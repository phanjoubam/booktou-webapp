<?php 
    $total =0;  
?>

@extends('layouts.admin_theme_02')
@section('content')
 

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
          
           <div class="row">
            <div class="col-md-5">
                  <h2><h3>Agents Service Log for {{ date('F', mktime(0, 0, 0,   $data['month'] , 10)) }}</h3></h2> 
            </div>

<div class="col-md-2 text-right">Filter Search:</div> 
 <div class="col-md-5 text-right">  

         <form class="form-inline" method="get" action="{{ action('Admin\AdminDashboardController@getDeliveryAgents') }}"   >
      <div class="form-row">
       
        <div class="col-md-12"> 
          <select name='month' class="form-control form-control-sm  ">
          <option <?php if( date('m') == 1 ) echo "selected"; ?> value='1'>January</option>
          <option <?php if( date('m') == 2 ) echo "selected"; ?>  value='2'>February</option>
          <option <?php if( date('m') == 3 ) echo "selected"; ?> value='3'>March</option>
          <option <?php if( date('m') == 4 ) echo "selected"; ?> value='4'>April</option>
          <option <?php if( date('m') == 5 ) echo "selected"; ?> value='5'>May</option>
          <option <?php if( date('m') == 6 ) echo "selected"; ?> value='6'>June</option>
          <option <?php if( date('m') == 7 ) echo "selected"; ?> value='7'>July</option>
          <option <?php if( date('m') == 8 ) echo "selected"; ?> value='8'>August</option>
          <option <?php if( date('m') == 9 ) echo "selected"; ?> value='9'>September</option>
          <option <?php if( date('m') == 10 ) echo "selected"; ?> value='10'>October</option>
          <option <?php if( date('m') == 11 ) echo "selected"; ?> value='11'>November</option>
          <option <?php if( date('m') == 12 ) echo "selected"; ?> value='12'>December</option> 
        </select>
        <input readonly name='year' class="form-control form-control-sm  " value="{{ date('Y') }}" /> 
      
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button> 
        </div> 
    </div>
  
      </form>
</div>

       </div> 
        
   
  </div>
 
       <div class="card-body"> 
        @php
          $slno = 1; 
          $sumTotal =  0;
          $collectionTotal=0;

          $grossTotalNormal =  0;
          $grossTotalPnD =  0;
          $grossTotalAssist  =  0;
          $grossDeposit =0;

        @endphp 
  

      <table class="table  ">  
        <tr class="text-center" style='font-size: 12px'>  
          <th scope="col" class="text-center" >Sl. No.</th> 
          <th scope="col" class="text-left">Agent Name</th> 
          <th class='text-right'>Normal</th>    
          <th class="text-center">PnD</th> 
          <th class='text-center'>Assist</th>
          <th class="text-center">Amount</th>                     
          <th class='text-center'>Deposits</th> 
          <th class="text-center">More</th> 
        </tr>
        @foreach($data['agents'] as $agent)
          @php
            $totalcashamount = $totalonlineamount = 0.0;  
            $totalNormals = $totalPnds = $totalAssists =0;
            $totalDeposits = 0;
          @endphp 
          <tr>
            <td>{{ $slno }}</td>
            <td>{{ $agent->fullname }} ( {{ $agent->phone }} )</td>
              

            @foreach ($data['all_orders'] as $item)  


              @if( $item->member_id == $agent->id ) 
                @if( strcasecmp($item->orderStatus, "completed") == 0 || strcasecmp($item->orderStatus, "delivered") == 0) 

                  @if($item->orderType == "normal")
                    @php 
                      $totalNormals++; 
                      $grossTotalNormal++;
                    @endphp  
                  @elseif($item->orderType == "pnd")
                    @php 
                      $totalPnds++;
                      $grossTotalPnD++;
                    @endphp
                  @elseif($item->orderType == "assist")
                    @php 
                      $totalAssists++; 
                       $grossTotalAssist++;
                    @endphp
                  @endif 
 
                  <?php 
                      if(  $item->source == "booktou" || $item->source == "customer"  )
                      {
                        if( strcasecmp($item->payMode, "online" ) == 0  ||  strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                        {
                          $totalonlineamount  +=  $item->totalAmount + $item->serviceFee ; 
                        }
                        else //cash collection
                        {
                          $totalcashamount  +=  $item->totalAmount + $item->serviceFee ; 
                          
                          if($item->is_deposited == "yes")
                          {
                            $totalDeposits +=  $item->totalAmount + $item->serviceFee ;
                            $collectionTotal +=  $item->totalAmount + $item->serviceFee ;
                          }

                        } 
                      }
                      else if(  $item->source == "business" || $item->source == "merchant"  )
                      {
                        if( strcasecmp($item->payMode, "online" ) == 0  || 
                          strcasecmp($item->payMode, "Pay online (UPI)" ) == 0 )
                        {
                          $totalonlineamount  += 0.00;  
                        }
                        else //cash collection
                        { 
                          $totalcashamount  +=  $item->totalAmount + $item->serviceFee ;

                          if($item->is_deposited == "yes")
                          {
                            $totalDeposits +=  $item->totalAmount + $item->serviceFee ;
                            $collectionTotal +=  $item->totalAmount + $item->serviceFee ;
                          }

                        } 
                      }
                    ?>
                @endif
              @endif

            @endforeach 
            @php
              $sumTotal += $totalcashamount;
              $slno++;
            @endphp 
          <td class="text-right">{{ $totalNormals }}</td>
          <td class="text-right">{{ $totalPnds }}</td>
          <td class="text-right">{{ $totalAssists }}</td>
          
          
            @if($totalcashamount > $totalDeposits) 
               <td class='text-right'><span class='badge  badge-pill badge-warning'>{{ number_format( $totalcashamount ,2, '.', '') }}</span></td>  
               <td class="text-right"><span class='badge  badge-pill  badge-danger'>{{ number_format( $totalDeposits ,2, '.', '') }}</span></td>
            @elseif($totalcashamount == $totalDeposits)
               <td class='text-right'><span class='badge badge-success badge-pill'>{{ number_format( $totalcashamount ,2, '.', '') }}</span></td> 
               <td class="text-right"><span class='badge  badge-success  badge-pill'>{{ number_format( $totalDeposits ,2, '.', '') }}</span></td>
            @else
                <td class='text-right'><span class='badge badge-success badge-pill'>{{ number_format( $totalcashamount ,2, '.', '') }}</span></td> 
                <td class="text-right"><span class='badge  badge-warning badge-info'>{{ number_format( $totalDeposits ,2, '.', '') }}</span></td>
            @endif 

          
          <td>
            <a href="{{ URL::to('/admin/delivery-agent/monthly-collection-report')}}?aid={{ $agent->id }}&filter_date={{ $data['month'] }}" 
              class='btn btn-primary btn-xs' target='_blank'>Details</a>
          </td> 
        </tr> 
    @endforeach
    <tr>
      <th colspan='2' class='text-right'>Total:</th> 
      <th class='text-right'>{{$grossTotalNormal }}</th>
      <th class='text-right'>{{ $grossTotalPnD}}</th>
      <th class='text-right'>{{ $grossTotalAssist}}</th>
      <th class='text-right'>{{ number_format( $sumTotal ,2, '.', '') }}</th>
      <th class='text-right' style='width: 130px'>
        <input type="text" class="form-control form-control-sm is-valid" readonly value="{{  number_format( $collectionTotal ,2, '.', '') }}"/>
      </th>
      <th colspan=''></th>
    </tr>
  

    </table>

     </div>
    </div>
 </div> 
</div>  

@endsection


 @section("script")

<script>
  
 $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 