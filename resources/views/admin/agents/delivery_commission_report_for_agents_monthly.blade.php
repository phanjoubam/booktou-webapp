@extends('layouts.admin_theme_02')
@section('content')
<?php  
$total_commission = 0;
$total_amount = 0; 
?>

 <div class="row"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header"> 

         <div class="card-title">
         <div class="row">
          <div class="col-md-9">
          <div class="title">Delivery Agents Commission Orders Report For: 
            <span class="badge badge-warning">
              {{$data['month']}} , {{$data['year']}}
            </span> 
          </div>
      </div>

      <div class="col-md-3 text-right">  
          
      </div>
         </div>
        </div> 
       
       </div>
       <div class="card-body "> 
        <div class="row" style="background-color: #434343; color: #fff;">
        <div class="col-md-6">
                    <h1 class="p-3"> 
                       <a class="btn btn-primary" href="{{ URL::to('/admin/export-agent-commission-report')}}/{{ $data['year'] }}/{{$data['mont']}}?member={{request()->get('member')}}">Export To Excel</a>
                    </h1>
        </div>

         <div class="col-md-6">
          <h3 class="p-3 text-right"> 
            <span class="badge badge-primary">{{$data['agents']->fullname}} ({{$data['agents']->phone}}) </span>
          </h3>
        </div>
      </div>
                 <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">  
                      <tr class="text-center" style='font-size: 12px'>  
                        <th scope="col">Order No</th> 
                        <th scope="col">Service Date</th> 
                        <th class="text-right">Amount</th> 
                        <th class="text-right">Commission</th>  
                    </tr>
                    </thead>
                <tbody>

                @foreach($data['orders'] as $items) 
                <?php

                  $total_amount+= $items->totalAmount;
                  $total_commission+= $items->commission;

                ?>
                 <tr>
                  <td class="text-center">{{$items->order_no}}</td>
                  <td class="text-center">{{date('Y-m-d',strtotime($items->service_date))}}</td>
                  <td class="text-right">{{$items->totalAmount}}</td>
                  <td class="text-right">{{$items->commission}}</td> 
                 </tr> 
                @endforeach


                </tbody>
                <tfoot>
                   <tr>
                      <td></td>
                      <td></td>
                      <td class="text-right">Total Amount: <span class="badge badge-primary">{{$total_amount}}</span></td>
                      <td class="text-right">Total Commission: <span class="badge badge-primary"> {{$total_commission}}</span></td>     
                   </tr>
                </tfoot>
                </table>
              </div>
              </div>
            </div>
          </div>
        </div>  
  
    



@endsection

@section("script")

<script>


 
 

 


</script> 

@endsection 