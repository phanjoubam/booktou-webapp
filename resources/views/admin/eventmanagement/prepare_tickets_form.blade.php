@extends('layouts.admin_theme_02')
@section('content')
 

@if (session('err_msg'))
    <div class="col-md-12 alert alert-success">
        {{ session('err_msg') }}
    </div>
@endif
<div class="row"> 

    
  <div class='col-md-6'>

                   
<form action="{{ action('Admin\EventmManagementController@generateTicketQrcodes') }}" method="post">
    {{csrf_field()}} 

<div class="card mt-4">
  <div class="card-header">
    <h4>Ticket Generation</h4>
  </div>
  <div class="card-body">

<div class="form-row">
      <div class="col-md-4 mt-2">
    <label for="prefix">Ticket Prefix:</label>
    <input type="text" required class="form-control" maxlength="3" required id="prefix" name="prefix" >
    </div>
 
      <div class="col-md-8  mt-2">
    <label for="startfrom">Starting No:</label>
    <input type="number"  required class="form-control" id="startfrom" name="startfrom"  maxlength="5" placeholder='Starting ticket serial no.'>
    <small>Max 500 QR codes can be generated at one time</small>
  </div>

 
  </div>
 

<div class="form-row">

      <div class="col-md-5 mt-2">
    <label for="prefix">Ticket Valid Date:</label>
    <input type="text" required class="form-control calendar"  id="validdate" name="validdate" >
    </div>
 
    <div class="col-md-4 mt-2">
    <label for="cost">Ticket Price:</label>
    <input type="number" required class="form-control"  id="cost" name="cost" >
    </div>


 </div>
 
  

  </div>

    <div class="card-footer text-right">
    
              <div class="d-flex mb-3">
           
        <div class="p-2 flex-fill-right">
          <button type="submit" class="btn btn-primary btn-xss btn-rounded" value="save" name="save">Generate Ticket QR Images</button> 

        </div> 
    </div>

    </div>
</div>
</form>

 


                </div>
    </div> 
     
    

@endsection
 

@section("script") 

<script> 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

$(document).on("click", ".btnchangebanner", function()
{ 
    $('#modfileupload').modal('show');
})
 
$(document).on("click", ".btnpangstfssai", function()
{ 
    $('#modpangstfssai').modal('show');
})

 

 $(document).on("click", ".btn_upd_comm", function()
{
  var wgtno = $(this).attr("data-wgtno"); 
 
  $("#wgt" + wgtno).modal("show");

});
 

 $(document).on("click", ".btnPaymentUpdateDays", function()
  {
   
  $("#widget-p").modal("show");

  });


  $(document).on("click", ".btnUpdatePhone", function()
  {
   
  $("#widget-phone").modal("show");

  });



  $(document).on("click", ".btnshowbluetick", function()
  { 
      $('#wgtbluetick').modal('show');
  });
 
  $(document).on("click", ".btnshowmerchant", function()
  { 
      $('#wgtpos').modal('show');
  });

  $(document).on("click", ".btnshowpayment", function()
  { 
      $('#wgtpospayment').modal('show');
  });

  $(document).on("click", ".btnshowtakeaway", function()
  { 
      $('#wgtselfpickup').modal('show');
  });
  

$(document).on("click",".splitpay",function()
{
  $("#splitpaysetup").modal("show");
})
</script> 


@endsection




 
  
 