@extends('layouts.admin_theme_02')
@section('content')
 
<?php 
    $cdn_url =   env('APP_CDN') ;  
    $cdn_path =   env('APP_CDN_PATH') ; // cnd - /var/www/html/api/public

?>

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">
         <div class="col-md-7">
           <h5>Products Listed Under <span class='badge badge-primary'>{{  $data['business']->name }}</span></h5>
         </div>

         <div class="col-md-5 text-right">
           <form class="form-inline" method="post" action="{{  action('Admin\AdminDashboardController@searchBusinessProducts') }}">
            {{ csrf_field() }}
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Category:</label> 
            <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='search_key'>
              <option value='all'>-- ALL PRODUCTS --</option> 
              @foreach ($data['category'] as $item)
                <option value='{{$item->category_name}}'>{{$item->category_name}}</option> 
              @endforeach
            </select>
            <input type="hidden" value="{{ $data['bin'] }}" name="busi_id">
            <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
          </form>  
        </div> 
      </div> 


                </div>
            </div>
       <div class="card-body"> 
  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif
  <div class="table-responsive">
  <table class="table"> 
    <thead class=" text-primary">
      <tr >
        <th scope="col">Image</th>
        <th scope="col">Item </th> 
        <th scope="col">Stock</th>
        <th scope="col">Max Order</th> 
        <th scope="col">Price</th>
        <th scope="col">Discount</th>
        <th scope="col">Category</th>
        <th scope="col">Is Featured?</th>
        <th scope="col">Action</th>
      </tr>
    </thead> 
    <tbody> 
    @foreach ($data['products'] as $itemP)  
     <tr >
     <td> 
      <img src="{{ $itemP->image  }}" alt="..." height="50px" width="50px"> 
    </td>
     <td style="white-space: normal;"><span class='badge badge-primary'>{{ $itemP->id }}</span><br/>
      {{ $itemP->pr_name }}<br/>
      {{  $itemP->description }}</td> 
      <td>{{$itemP->stock_inhand}}</td>
      <td>{{$itemP->max_order}}</td> 
      <td>{{$itemP->unit_price}}</td>
      <td>{{$itemP->discount}}</td>
   <td>{{$itemP->category}}</td>

<td>
    @php 
       $found = false
    @endphp 
     @foreach ($data['promotion_products'] as $promoitem) 
        @php 
         $found = false
      @endphp
       @if($promoitem->pr_code == $itemP->pr_code)
          <span class='badge badge-info'>Featured</span> 
           @if($promoitem->todays_deal== "yes")
             <span class='badge badge-danger'>Today's Deal</span>  
           @endif
           @php 
           $found = true
           @endphp  
        @break
      @endif 
   @endforeach 

   @if(!$found)
      <span class='badge badge-warning'>Not-Featured</span> 
   @endif 
   </td>
      <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"> 
                          <a class="dropdown-item"  href="{{ URL::to('/admin/customer-care/business/view-product-photo') }}/{{ $itemP->bin }}/{{ $itemP->id  }}">Manage Images</a> 
                          <a class="dropdown-item showdialogm1"
                           data-name="{{ $itemP->pr_name }}"
                           data-key="{{ $itemP->prsubid }}" 
                           data-photo="{{ $itemP->image}}">Add To Promotion</a>

                          <a class="dropdown-item showdialogm2" 
                          data-discount="{{ $itemP->discount }}"
                          data-price="{{ $itemP->unit_price }}"
                          data-key="{{ $itemP->prsubid }}"
                          data-bsin="{{ $itemP->bsin }}"
                          data-photo="{{ $itemP->image}}">Update Discount</a>   
                          <a class="dropdown-item showdialogm3" 
                          data-category="{{ $itemP->category }}"                          
                           data-bin="{{ $itemP->bin }}"
                            data-bsin="{{ $itemP->bsin }}"
                             data-key="{{ $itemP->id }}"
                          >Add Popular Category</a>
                          <a class="dropdown-item"  href="{{ URL::to('/admin/customer-care/business/view-discount-products') }}">View Discounted Products</a> 
                           
                        
                        </div>
                      </div>
                  
        </td>
 
    </tr>
    @endforeach
   </tbody>

  </table>

  {{ $data['products']->links() }}

 </div>

  </div>

   
</div>    
  
   </div> 
 </div>

 <form action='{{ action("Admin\AdminDashboardController@addToPromotion") }}' method='post'>
  {{ csrf_field() }}
 <div class="modal" id='widget-m1' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Selected Product To Promotion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
 <div class="form-row"> 
 <div class="col-md-6"> 
                      <img src="" id='primage' height='auto' width="380px">
                      </div>
 <div class="col-md-6"> 

  <label class="control-label"><span id="span_prname" ></span></label> 
  <label for="yesno" class="form-label">Include selected product under TODAY's DEAL too?</label>
 <select class="form-control form-control-sm boxed " id='todaysdeal' name='todaysdeal' >
                        <option>Yes</option>
                        <option>No</option>                       
                        </select> 
                      </div>
                      </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='prsubid' id='hidprid' />
        <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>
<!--  -->

<form action='{{ action("Admin\AdminDashboardController@updatePrice") }}' method='post'>
  {{ csrf_field() }}
  

 <div class="modal" id='widget-m2' tabindex="-1" role="dialog" >
  <div class="modal-dialog modal-xl modal-dialog-centered" >
    
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title">Update Discount Price</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >        
        <div class="row"> 
          <div class="col-md-6"> 
            <img src="" id='disimage' height='auto' width="480px">
          </div>          
          <div class="col-md-4" >                     

           <div class="form-group">
             <label for="labelprice">Price</label>
            <!--  <input class="form-control" readonly="readonly" name="brutto" type="text" id="brutto"> -->
             <label  class="form-label labelprice"></label>
             <input   class="form-control"  name="price" type="hidden" id="price" >
          </div>

          <div class="form-group">
             <label for="Discount">Discount</label>
             <input class="form-control" name="Discount" type="text" id="Discount">
          </div>
          <div class="form-group">
             <label for="percentage">Discount %:</label>
             <input class="form-control"  name="percentage" type="text" id="percentage">
          </div>

          <div class="form-group">
             <label for="deduction">Amount after deduction:</label>
             <input class="form-control"   type="text" id="deduction">
          </div>

          <div class="row">
              <div class="col-md-12" >                     
          <div class="form-group">
             <label >Main Module</label>
             <select class="form-control form-control-sm boxed " id='mainmodule' name='mainmodule' >
                  @foreach($data['categories'] as $cat)
                            
                            <option value="{{$cat->main}}">
                              {{$cat->main}}
                            </option>
                           @endforeach                       
                </select> 
          </div>
          <div class="form-group">
             <label for="category">Category</label>
              <input class="form-control" value="{{$itemP->category}}" type="text" name="category" id="category">
          </div>

          <div class="form-group">
                <label  class="form-label">Is-sponsored</label>
               <select class="form-control form-control-sm boxed " id='is_sponsored' name='isSponsored' >
                  <option>No</option>
                  <option>Yes</option>                       
                </select> 
            </div>

          </div>
          </div>
          </div>
          </div>  
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='key' id='discountkey' />
        <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  
  </div>
</div>
</form>

<!--  -->
<form action='{{ action("Admin\AdminDashboardController@addPopularProducts") }}' method='post'>
  {{ csrf_field() }}
  

 <div class="modal" id='widget-m3' tabindex="-1" role="dialog" >
  <div class="modal-dialog  modal-dialog-centered" >    
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title">Add Popular Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >        
        <div class="row "> 
           <div class="row "> 
         <div class="col-md-6">
          <label for="category" class="form-label">Category</label>
          <select class="form-control" name="category" id="category">
           @foreach($data['popular_category'] as $cat)

           <option value="{{$cat->category}}">
            {{$cat->category}}
          </option>
          @endforeach
        </select>
      </div>
      <div class="col-md-6">
        <label for="display_name" class="form-label">Category</label>
        <select class="form-control" name="display_name" id="display_name">
         @foreach($data['popular_category'] as $cat)

         <option value="{{$cat->display_name}}">
          {{$cat->display_name}}
        </option>
        @endforeach
      </select>
    </div>


  </div>

      <div class="modal-footer mt-1"> 
        <span class='loading_span'></span>
        <input type='hidden' name='bsin' id='bsin' />
        <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  
  </div>
</div>
</div>
</form>

<!--  -->
<!-- <form action='{{ action("Admin\AdminDashboardController@addDiscountedProducts") }}' method='post'>
  {{ csrf_field() }}
  

 <div class="modal" id='widget-m4' tabindex="-1" role="dialog" >
  <div class="modal-dialog  modal-dialog-centered" >    
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title">Add Discounted Products</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >        
        <div class="row "> 
         <div class="col-12">
            <div class="row">
              <div class="col-6">
                  <label  class="form-label">Main Module</label>
                 <input class="form-control" type="text" id="mainmodule" name="mainmodule">
              </div>
             <div class="col-6">
                <label  class="form-label">Category</label>
                            
               <input class="form-control" value="{{$itemP->category}}" type="text" name="category" id="category">
                
            </div>
            </div>
            <div class="col-6 ">
                <label  class="form-label">Is-sponsored</label>
               <input class="form-control" type="text" id="is_sponsored" name="isSponsored">
            </div>
          </div>                                  
         </div>

      <div class="modal-footer mt-1"> 
        <span class='loading_span'></span>
        <input type='hidden' name='keys' id='keys' />
        
        <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  
  </div>
</div>
</div>
</form>
 -->
@endsection

@section("script")
<!-- <script type="text/javascript">
$(function() {
  $("#price, #Discount").change(function() { // input on change
    var result = parseFloat(parseInt($("#Discount").val(), 10)/parseInt($("#price").val(), 10)) * 100 ;
    $('#percentage').val(result||''); //shows value in "#rate"
  })
});
</script> -->
 

<script>

$(document).on("click", ".showdialogm1", function()
{
    $("#span_prname").html( $(this).attr("data-name"));
    $("#hidprid").val( $(this).attr("data-key"));
    $("#primage").attr("src", $(this).attr("data-photo")); 
                          
    $("#widget-m1").modal("show")

});

$(document).on("click", ".showdialogm2", function()
{
    $("#discount").val( $(this).attr("data-discount"));
    $(".labelprice").text( $(this).attr("data-price"));
    $("#price").val( $(this).attr("data-price"));
    $("#discountkey").val( $(this).attr("data-key"));
    $("#disimage").attr("src", $(this).attr("data-photo")); 
                          
    $("#widget-m2").modal("show")

});

// $(".showdialogm4").on('click', function(){ 
//       var key  = $(this).attr("data-key"); 
//       $("#key").val(key); 
//       $widget  = $(this).attr("data-widget");  
//       $("#category").val($(this).attr("data-category"));
//       $("#bin").val($(this).attr("data-bin"));
//        $("#bsin").val($(this).attr("data-bsin")); 
//       $("#widget-m4"+$widget).modal('show'); 
// });

$(document).on("click", ".showdialogm3", function()
{    
    $("#bin").html( $(this).attr("data-bin"));
    $("#bsin").html( $(this).attr("data-bsin"));    
    $("#bsin").val( $(this).attr("data-key"));                          
    $("#widget-m3").modal("show")

});



// $(document).on("click", ".showdialogm4", function()
// {    
//     $("#bin").val( $(this).attr("data-bin"));
//     $("#keys").val( $(this).attr("data-bsin"));    
//     $("#keys").val( $(this).attr("data-key"));                        
//     $("#widget-m4").modal("show")

// });

$('#Discount').on('keyup', function () {
  var Discount = parseFloat($("#Discount").val()); 
  var labelprice =  $('#price').val();  

  $('labelprice').text(labelprice);
  $('#deduction').val(parseFloat(labelprice-Discount));
  $("#percentage").val(parseFloat(Discount)/parseInt(labelprice) * 100).toFixed(2);
  
});

  </script>

 @endsection