@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-6">
                  <div class="title">Normal Sales Performers</div> 
                </div>

<div class="col-md-2 text-right"> 
                      Report Period:
                      </div> 
                    <div class="col-md-4">  
        <form class="form-inline" method="get" action="{{ action('Admin\AdminDashboardController@viewBestPerformers') }}" enctype="multipart/form-data">
              {{  csrf_field() }}
      
      <div class="form-row">
    <div class="col-md-5">
      <select  class="form-control form-control-sm" name='month' >
        <?php 
        for($i=1; $i <= 12; $i++)
        { 
        ?> 
        <option value='{{ $i }}'>{{ date('F', mktime(0, 0, 0, $i , 10))  }}</option> 
        <?php 
        }
      ?>
      </select>
    </div>
    <div class="col-md-5">
      <select  class="form-control form-control-sm" name='year' >
        <?php 
        for($i=2020; $i <= date('Y'); $i++)
        { 
        ?> 
        <option value='{{ $i }}'>{{ $i }}</option> 
        <?php 
        }
      ?>
      </select> 
    </div>

    <div class="col-md-2">
 <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
    </div>

  </div>
 

    
      </form>
      </div>

       </div>
                </div>
            </div>
       <div class="card-body">   
<div class="table-responsive">
    <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
		<th scope="col"></th>
		<th scope="col">Name and No.</th>  
		<th scope="col">Phone</th> 
    <th scope="col" class='text-right'>Sales Count</th>  
    <th scope="col" class='text-right'>Total Sales</th>  
		<th scope="col">Action</th>
		</tr>
	</thead>
	
		<tbody>

			 <?php 

       $i = 0;
foreach($data['sales'] as $sitem)
 {
    $i++  ;    
    foreach ($data['results'] as $item)
		{
      if($sitem->bin == $item->id)
      {
        $saleCount = $sitem->saleCount;
        $totalSales =  $sitem->totalSales; 
                    
     ?>


		 <tr  >
		 <td>
     <?php 
      if($item->banner=="")
      { 
         $banner = $cdn_url . "/assets/image/no-image.jpg" ; 
      }
      else
      {
        $banner = $cdn_url . $item->banner   ;  
      }
      
     ?>

      <img src='{{ $banner }}' alt="{{$item->name}}" height="60px" width="60px"> 
                </td>
		 <td>{{$item->name}} ({{$item->shop_number}}) 
      <br/>
      {{$item->locality}}, <br/>{{$item->landmark}}, <br/>{{$item->city}}, {{$item->state}}-{{$item->pin}}
      <br/>
     @if( $item->latitude != 0 and $item->latitude != null && 
    $item->longitude != 0 and $item->longitude != null   )

    <a href='https://www.google.com/maps?q={{$item->latitude}},{{$item->longitude}}' target='_blank' class='btn btn-success btn-xs'>View Location</a>

    @endif 
  </td>
		 <td>{{$item->phone_pri}}</td>
     <td class='text-right'>{{ $saleCount  }}</td>
     <td class='text-right'>{{ $totalSales  }}</td>
    

		 <td class="text-center">

                       
                     <div class="dropdown"> 
                     <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a>

 

  <ul class="dropdown-menu dropdown-user pull-right"> 
    <li><a class="dropdown-item" href="{{ URL::to('/admin/customer-care/business/view-profile') }}/{{$item->id}}">Business Profile</a></li> 
    <li><a class="dropdown-item" href="{{ URL::to('/admin/analytics/business/normal-orders-break-down') }}?bin={{$item->id}}">Sales Breakdown</a></li> 
                
            </ul>
         </div>          
        </td>
		 </tr>
	
  <?php 
    break;
    }
   }
  }

?>
	</tbody>
		</table>
	 
  
		
	</div>
 </div>


  </div> 
	
	</div> 
</div>

 


<div class="modal fade" id="modal_add_promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <form method='post' action="{{ action('Admin\AdminDashboardController@saveBusinessPromotion') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          This is final step to add promo list.  
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='bin' id='bin'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>
 

 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 


</script>


@endsection

