@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default" >
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">

                 <div class="col-md-8">
                <div class="title">Unpaid Business List</div> 
                </div> 
                </div>
                </div>
            </div>
    <div class="card-body" >   
    <div class="table-responsive table-with-menu" >
    <table class="table" style='min-height:350px !important;'>
         <thead class=" text-primary">
    <tr class=""> 
      <th scope="col">Name and Location</th>  
    <th scope="col">Phone</th>
    <th scope="col">Due Amount</th>
    <th scope="col">Commission %</th>
    <th scope="col">Action</th>
    </tr>
  </thead>
  
    <tbody>

      <?php $total_due_amount =0; ?>

       <?php $i = 0 ?>
    @foreach ($data['results'] as $item)
    <?php 
    $i++  ;
    $found = false;

     
                          
     ?>


     <tr>
      
     <td>{{$item->name}} ({{$item->shop_number}}) <span class='badge badge-primary'>{{ $item->category}}</span>
      <br/>
      {{$item->locality}}, <br/>{{$item->landmark}}, <br/>{{$item->city}}, {{$item->state}}-{{$item->pin}}
      <br/>
     @if( $item->latitude != 0 and $item->latitude != null && 
    $item->longitude != 0 and $item->longitude != null   )

    <a href='https://www.google.com/maps?q={{$item->latitude}},{{$item->longitude}}' target='_blank' class='btn btn-success btn-xs'>View Location</a>

    @endif 
  </td>
     <td>{{$item->phone_pri}}</td>

       

     <?php 
     foreach ($data['booking_list'] as $book) {
       
      if ($item->id == $book->bin) {
            echo "<td>$book->total_due</td>";
         }   
    

    } 
      ?>
 
  <td>{{ $item->commission }} %</td>
       


     <td>

         <a href="{{URL::to('admin/business/sales-and-clearance-report-by-days')}}/{{$item->id}}"><span class="badge badge-danger">VIEW UNPAID ORDERS</span></a>             
                                
        </td>
     </tr>

      
  @endforeach
  </tbody>
    </table>
  

  {{ $data['results']->links() }}
  
    
  </div>
 </div>


  </div> 
  
  </div> 
</div>

 


<div class="modal fade" id="modal_add_promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <form method='post' action="{{ action('Admin\AdminDashboardController@saveBusinessPromotion') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          This is final step to add promo list.  
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='bin' id='bin'/>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="save" class="btn btn-primary btn-sm">Yes</button>
      </div>
    </div>
  </div>
 </form>
</div>
 





@endsection



@section("script")

<script>

 


$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});



  
 


$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 


</script>


@endsection

