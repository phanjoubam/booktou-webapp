@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                <div class="col-md-12">
                  <div class="title">Search Business by Character</div> 
                  <nav aria-label="">
                    <ul class="pagination pagination-sm">
                      <li class="page-item"><a class="page-link" name="btn_search" 
                      href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=a">A</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=b">B</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=c">C</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=d">D</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=e">E</a></li>
                      <li class="page-item"><a class="page-link"  
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=f">F</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=g">G</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=h">H</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=i">I</a></li>
                      <li class="page-item"><a class="page-link"
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=j">J</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=k">K</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=l">L</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=m">M</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=n">N</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=o">O</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=p">P</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=q">Q</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=r">R</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=s">S</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=t">T</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=u">U</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=v">V</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=w">W</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=x">X</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=y">Y</a></li>
                      <li class="page-item"><a class="page-link" 
                        href="{{ URL::to('/admin/customer-care/all-business-details') }}?key=z">Z</a></li>
                       

                    </ul>
                  </nav>
                </div>


                   

       </div>
                </div>
            </div>
      


     
      <div class="card-body">
      <table class="table">
        <tr><thead>
          <th>Name</th>
          <th>Address & No</th>
          <th data-sortable="true" data-sort-order="desc" data-sort-name="Pick and Drop">All Orders</th>
          <th>Action</th>
        </thead>
        </tr>
       
      @if(isset($data['results']))
       @foreach($data['results'] as $business) 
 <?php $i = 0 ?>
        <?php 
    $i++  ;
    $found = false;

    foreach($data['promo_businesses'] as $plist)
    {
      if($plist->bin == $business->id ) 
                              {
                                $found= true;break;
                              }
                            }
                          
     ?>
        <tbody>
        <tr>
          <td>{{$business->name}}</td>
          <td>
            {{$business->name}} ({{$business->shop_number}}) <span class='badge badge-primary'>{{ $business->category}}</span>
      <br/>
      {{$business->locality}}, <br/>{{$business->landmark}}, <br/>{{$business->city}}, {{$business->state}}-{{$business->pin}}
      <br/>
     @if( $business->latitude != 0 and $business->latitude != null && 
    $business->longitude != 0 and $business->longitude != null   )

    <a href='https://www.google.com/maps?q={{$business->latitude}},{{$business->longitude}}' target='_blank' class='btn btn-success btn-xs'>View Location</a>

    @endif 
    </td>
          
        <td>
          

          @foreach($data['bookings'] as $book)
           
            @if($book->id == $business->id)

            @php  
            $pndOrderNo = 0;
            $normalOrderNo=0;
            $OrderNo=0;

            @endphp

            @if($book->type=="pnd")
               
              
            <label class="mb-3">  
              Pick and Drop <a href="{{URL::to('/admin/business/orders-by-type')}}/{{$book->type}}/{{$business->id}}">
                <span class='badge badge-primary'> {{$book->OrderNo}}</span>
              </a></label> <br>
              
               @else
               
              <label>Normal Order 
                <a href="{{URL::to('/admin/business/orders-by-type')}}/{{$book->type}}/{{$business->id}}">
                <span class='badge badge-warning'> {{$book->OrderNo}}</span>
              </a></label> 
              @endif

               
              
            @endif

              
             
            
              @endforeach
        </td>

        <td>
          
          <div class="dropdown">
  

                     <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a>



  <ul class="dropdown-menu dropdown-user pull-right"> 
    <li><a class="dropdown-item" href="{{ URL::to('/admin/customer-care/business/view-profile') }}/{{$business->id}}">Business Profile</a></li>
    <li><a class="dropdown-item" href="{{URL::to('/admin/customer-care/business/view-products')}}/{{$business->id}}">Products</a></li>
    <li><a class="dropdown-item" href="{{ URL::to('/admin/customer-care/business/get-daily-earning/') }}/{{$business->id}}">Daily Earning Report</a></li>
 
    
    <li><a class="dropdown-item" href="{{ URL::to('/admin/business/sales-and-clearance-report') }}?bin={{$business->id}}">Monthly Sales Report</a></li>
    <li><a class="dropdown-item" href="{{ URL::to('/admin/business/sales-and-clearance-history') }}?bin={{$business->id}}">Monthly Clearance History</a></li>
    <li><a class="dropdown-item" href="{{ URL::to('/admin/business/sales-and-service-performance') }}?bin={{$business->id}}">Monthly Performance</a></li>

    <li><a class="dropdown-item" href="{{ URL::to('/admin/customer-care/business/view-importable-product/') }}/{{$business->id}}">View Importable Products</a></li>
                    @if( !$found )
                    <li><a href='#' class="dropdown-item btn_add_promo"  data-bin="{{$business->id }}"  >Promote Business</a></li>
                    @endif

    @foreach($data['qr_menu'] as $qrlist)
      @if($qrlist->bin == $business->id)
        <li><a target='_blank' href="{{ URL::to('/shopping/browse-menu') }}?mcode={{ $business->merchant_code}}" class="dropdown-item "    >QR Menu</a></li>
         @break
      @endif 
    @endforeach
     
                     
  </ul>
         </div>




        </td>
        



        </tr>          
        </tbody>
        @endforeach
       @endif

       </table> 
      </div>
      
</div>
</div>
</div>

 

@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 


</script>


@endsection

