@extends('layouts.admin_theme_02')
@section('content')
  @php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
    $business = $data['business']; 
    $business_pay = $data['biz_pay_cycle'];

    $commission =  $pnd_commission =  "0";
    $commstarts = $pndcommstarts  = date('d-m-Y');

    $comm_exists = false;
    if(isset($data['commissions']))
    {
      $comm_exists = true;
      $commission = $data['commissions']->commission;
      $pnd_commission = $data['commissions']->pnd_commission;
      $commstarts = date('d-m-Y', strtotime($data['commissions']->comm_effective_from));
      $pndcommstarts = date('d-m-Y', strtotime($data['commissions']->pndcomm_effective_from));
    }

  @endphp

@if (session('err_msg'))
    <div class="col-md-12 alert alert-success">
        {{ session('err_msg') }}
    </div>
@endif
<div class="row"> 

     <div class='col-md-6' > 

     <div class="card  ">
                        <div class="card-header"> 
                          <div class="row">
               <div class='col-md-10'> 
                <h4  >{{ $business->name}}</h4> 
               </div>
                <div class="col-md-2 text-right">
                  
                  <div class="dropdown">
                     <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a>

                  <ul class="dropdown-menu dropdown-user pull-right"> 
                  <li><a class="dropdown-item btnpangstfssai" 
                href="#">
                PAN/GST/FSSAI</a>
                  </li>

                  <li><a class="dropdown-item btnPaymentUpdateDays" 
                href="#">
                Enter Payment Days</a>
                  </li>
 
<li><a class="dropdown-item btnchangebanner"  href="#">Change Banner</a></li>


<li><a class="dropdown-item btnshowbluetick" 
  data-blue="{{ $business->is_verified }}" href="#">Verified Tick</a>
</li>
@if($business->pos_merchant=="yes")                  
<li><a class="dropdown-item btnshowpayment" 
  data-merchant="{{ $business->id }}" href="#">POS Monthly Payment</a>
</li>
@else 
<li><a class="dropdown-item btnshowmerchant" 
  data-pos="{{ $business->pos_merchant }}" href="#">POS Subscription</a>
</li>
@endif
<li><a class="dropdown-item btnshowtakeaway" 
  href="#">Self Pick Up Service</a>
                  </li>

                  </ul>
                  </div>

                </div>
                </div>
                        </div>

                        <div class="card-body  ">
                        
                        <div class='row'>

                          <div class='col-md-12'>
<img src="{{ $cdn_url . $business->banner  }}" class="img-fluid" alt="{{$business->name}}"> <hr/>
                          </div>
                          <div class='col-md-12'>

                             {{$business->locality}}, {{$business->landmark}}, {{$business->city}}, {{$business->state}}-{{$business->pin}}</br>
                            <strong>Primary Phone :</strong> {{$business->phone_pri}} 
                            <button class="btn btn-primary btn-sm btnUpdatePhone">
                              <i class="fa fa-pencil"></i>
                            </button>
                            </br>
                            <strong>Alternate Phone :</strong> {{$business->phone_alt}}</br>
                            <strong>Is Open :</strong> {{$business->is_open}} </br> 

                            <hr/>

                            <div class='row'>
                                <div class='col-md-6'>
                                  <strong>Verified Business: </strong> 
                                  @if( $business->is_verified == "yes" )
                                    <i class='fa fa-check-circle fa-2x' style='color: blue'></i> 
                                  @else  
                                    <i class='fa fa-times   fa-2x' style='color: red'></i>   
                                  @endif
                                </div>
                                <div class='col-md-6'>
                                  <strong>Self Pickup Business</strong> 
                                   @if( $business->allow_self_pickup == "yes" )  
                                    <i class='fa fa-check-circle fa-2x' style='color: green'></i>      
                                    @else
                                    <i class='fa fa-times   fa-2x' style='color: red'></i>   
                                   @endif
                                </div>

                            </div>
 
                            <hr>
                            

                          </div>


                        </div>


                   

                    </div>

                    </div>
                </div>

                <div class='col-md-6'>

                    <div class="card  ">
                        <div class="card-header"> 
                          <div class='row'>
                            <div class='col-md-12'>
                          <h4 class="heading-3">Commission Structure</h4>
                           </div>
                            
                        </div>
                      </div>
                       <div class="card-body text-center">

            @if($comm_exists) 
                        <div class='row'>
                            <div class='col-md-6'>
                          <div class="jumbotron">
                            <span>Normal/Service Order</span>
            <h1 class="display-4">{{  $commission }} %</h1>
            <strong>Effective from </strong><span class="badge badge-primary badge-pill">{{  $commstarts }}</span>
          </div>
          </div>
 

           <div class='col-md-6'>
              <div class="jumbotron">
                <span>PnD Order</span>
              <h1 class="display-4">{{ $pnd_commission }} %</h1>
                <strong>Effective from </strong><span class="badge badge-primary badge-pill">{{  $pndcommstarts  }}</span>

            </div>
          </div> 
          </div>
          <div class="row">
            <div class='col-md-12'>
              <br/>
              <p>In order to update new commission structure, you have to archive existing commission structure.<br/>
                <br/>
              <button class='btn btn-danger btn-rounded btn-sm' data-toggle="modal" data-target="#wgt-confirmarchive">Archive Existing Commission</button>
            </p>
            </div> 
          </div>

          @else
            <div class="jumbotron">
                <span>No Active Commission Structure</span><br/><br/>
                <button class='btn btn-primary btn-rounded btn_upd_comm' data-wgtno="1"  >Add Commission</button>
                </div> 
          @endif

                    </div>


                  </div>


 <div class="card  mt-2">
  <div class="card-header">
    <h2>Additional Information</h2>
  </div>

  <div class="card-body ">

  <strong>PAN:</strong> {{ $business->pan_no == null ? "NOT PROVIDED" : $business->pan_no }} </br>

                            @if( $business->non_gst_enrolled == "yes" )
                              <strong>GSTIN:</strong> Merchant submitted Non-GST Enrollment Form</br>
                            @else  
                              <strong>GSTIN:</strong> {{ $business->gstin == null ? "NOT PROVIDED" : $business->gstin }} </br>
                            @endif


                            @if( $business->fssai != null )
                              <strong>FSSAI:</strong> $business->fssai </br>
                            @endif 
@if(isset($data['bank_info']))
<hr/>

<strong>Account No.:</strong> {{ $data['bank_info']->bank_acc }}</br>
<strong>IFSC:</strong> {{ $data['bank_info']->ifsc_code }}</br>
<strong>Bank Name:</strong> {{ $data['bank_info']->bank_name }}</br>
<strong>RazorPay Merchant ID.:</strong> {{ $data['bank_info']->rzp_acc_id }}</br>


@endif

  </div>

    <div class="card-footer">
      <button class="btn btn-primary btn-rounded btn-sm splitpay"  >Update Bank Information</button>
    </div>
</div>



                </div>
    </div> 
     

<form action="{{ action('Admin\AdminDashboardController@uploadBannerImage') }}" method="post"  enctype="multipart/form-data" >
    {{ csrf_field() }}
   <div class="modal fade" id="modfileupload" tabindex="-1" role="dialog" aria-labelledby="del_box" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="del_box"><strong>Upload Banner Image</strong></h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body modal_msg">
       <div class="input-group">
  <div class="custom-file">
    <input type="file" name='photo' class="custom-file-input" id="fileselector" aria-describedby="btnupload">
    <label class="custom-file-label" for="fileselector">Choose file</label>
  </div>
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="submit" name='btnupload' value='upload' id="btnupload">Upload</button>
  </div>
</div> <input type="hidden" name='key' value="{{ $data['bin'] }}" >
         </div>
        
       </div>
     </div>
   </div>

 </form>


<form action="{{ action('Admin\AdminDashboardController@updatePanGstFssai') }}" method="post">
    {{ csrf_field() }}
   <div class="modal fade" id="modpangstfssai" tabindex="-1" role="dialog" aria-labelledby="del_box" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="del_box"><strong>Update PAN/GST/Fssai No</strong></h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body modal_msg">


          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="pan_no">PAN:</label>
              <input type="text" name="panNo" id="pan_no" class="form-control" 
     required="Pan no is compulsory" 
     @if($business->pan_no=="")value=""@else value="{{$business->pan_no}}"@endif>
            </div>
            <div class="form-group col-md-6">
              <label for="fssai_no">FSSAI:</label>
              <input type="text" name="fssaiNo" id="fssai_no" class="form-control"
     @if($business->fssai=="")value=""@else value="{{$business->fssai}}"@endif>
            </div>
          </div>

          <div class="form-row">
    <div class="form-group col-md-6">
      <label for="gst_no">GSTIN:</label>
      <input type="text" name="gstNo" id="gst_no" class="form-control"
     @if($business->gstin=="")value=""@else value="{{$business->gstin}}"@endif>
    </div>
    
  </div>
  
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="enrollednongst" name="enrollednongst">
      <label class="form-check-label" for="enrollednongst">
        Non-GST Enrolled
      </label>
    </div>
  </div>


  

<input type="hidden" name='key' value="{{ $data['bin'] }}" >
     <button class="btn btn-primary mt-3" type="submit" name='btnUpdatePan' value='update' 
     id="btnUpdatePan">Update</button>

       
         </div>
        
       </div>
     </div>
   </div>

 </form>

 

<form action="{{ action('Admin\AdminDashboardController@updateCommission') }}" method="post">
    {{ csrf_field() }}

   <div class="modal fade" id="wgt1" tabindex="-1" role="dialog" aria-labelledby="comm_update" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="comm_update"><strong>Update Commission Percentage</strong></h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body modal_msg">
  

        <div class="form-row">
    <div class="form-group col-md-6">
      <label for="comm">Normal Order/Service Commission</label>
            <input type="number" min='0' step='.01' max='100' name="comm"   id="comm"
            value="{{   $commission  }}" class="form-control" />
    </div>
    <div class="form-group col-md-6">
      <label for="commvalidfrom">Valid from</label>
      <input type="text" class="form-control calendar" id="commvalidfrom" name="commvalidfrom" value="{{ $commstarts }}">
    </div>
  </div>

 <div class="form-row">
    <div class="form-group col-md-6">
      <label for="pndcommission">Pick-And-Drop Commission</label>
      <input type="number" min='0' step='.01' max='100' class="form-control" id="pndcommission" name="pndcommission" value="{{  $pnd_commission  }}">
    </div>
    <div class="form-group col-md-6">
      <label for="pndcommvalidfrom">Valid from</label>
      <input type="text" class="form-control calendar" id="commvalidfrom" name="pndcommvalidfrom" value="{{ $pndcommstarts }}">
    </div>
  </div>


   <div class="form-row"> 
       <div class="form-check">
        <input class="form-check-input" type="checkbox"  name="markactive" id="activestatus">
        <label class="form-check-label" for="activestatus">
          Mark as active commission structure
        </label>
      </div>
    
  </div>


</div>
<div class="modal-footer ">
    <input type="hidden" name='key'  value="{{ $business->id }}" > 
     <button class="btn btn-primary mt-3" type="submit" name='btnupdate' value='update' 
     id="btnUpdatePan">Save</button>
   </div>
       </div>
     </div>
   </div>

 </form>



<!-- payment update days entering page -->
<form action="{{ action('Admin\AdminDashboardController@updatePaymentCycle') }}" method="post">
    {{ csrf_field() }}
<div class="modal" tabindex="-1" id="widget-p">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Payment Days</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="form-row">
            <div class="form-group col-md-6">
              <label for="payment_days">Payment Type:</label>
               
                

              <label for="payment_days">Payment Cycle:</label>
              

            </div>
            </div>
        <input type="hidden" name="bin" id="businessid" value="{{ $business->id }}">
        <button type="submit" class="btn btn-primary text-left">Update</button>
      </div>
      
    </div>
  </div>
</div>
</form>


<!-- update phone no -->
<form action="{{ action('Admin\AdminDashboardController@updateBusinessPhoneNo') }}" method="post">
    {{ csrf_field() }}
<div class="modal" tabindex="-1" id="widget-phone">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Update Phone No</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="form-row">
            <div class="form-group col-md-6">
              <label for="payment_days">Phone No:</label>
                <input type="text" class="form-control" name="n_phoneNo" id="n_phoneNo">
            </div>
            </div>
        <input type="hidden" name="bin" id="businessid" value="{{$business->id}}">
        <input type="hidden" name="oldphone_no" id="phoneNo" value="{{$business->phone_pri}}">
        <button type="submit" class="btn btn-primary text-left">Update</button>
      </div>
    </div>
  </div>
</div>
</form>




<!-- update verification tick -->
<form action="{{ action('Admin\AdminDashboardController@updateVerificationTick') }}" method="post">
    {{ csrf_field() }}
<div class="modal" tabindex="-1" id="wgtbluetick">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Verified Business Marker</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="form-row">
            <div class="form-group col-md-6">
              <label for="tick">Business QoS Verification Type:</label>
                <select  class="form-control" name="verifiedtick" id="tick">
                  <option value='yes'>Blue Tick</option>
                  <option value='no'>Gray Tick</option>
                </select>

            </div>
            </div>
        <input type="hidden" name="bin" id="businessid" value="{{ $business->id}}"> 
        <button type="submit" class="btn btn-primary text-left">Update</button>
      </div>
      
    </div>
  </div>
</div>
</form>


<!-- update pos merchant -->
<form method="post" action="{{action('Admin\AdminDashboardController@updatePosMerchant')}}">
    {{ csrf_field() }}
<div class="modal" tabindex="-1" id="wgtpos">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">POS Subscription</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="form-row">
            <div class="form-group col-md-4">
                <label for="pos">POS Merchant:</label>
                <select  class="form-control" name="pos_merchant" id="pos">
                  <option value='yes'>Registered</option>
                  <option value='no'>Not Registered</option>
                </select> 
            </div>

            <div class="form-group col-md-4">
              <label for="subscription">Subscription Date:</label>
                 <input type="text" class="calendar form-control" name="subscription_date">
            </div>

            <div class="form-group col-md-4">
              <label for="amount">Amount:</label>
                 <input type="text" class="form-control" name="subscription_amount">
            </div>

            </div>
        <input type="hidden" name="bin" id="businessid" value="{{ $business->id}}"> 
        <button type="submit" class="btn btn-primary text-left">Save</button>
      </div>
      
    </div>
  </div>
</div>
</form>

<!-- update pos payment merchant -->
<form method="post" action="{{action('Admin\AdminDashboardController@updatePosPayment')}}">
    {{ csrf_field() }}
<div class="modal" tabindex="-1" id="wgtpospayment">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">POS Monthly Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="form-row">
            <div class="form-group col-md-4">
              <label for="subscription">Year:</label>
              <select name='year' class="form-control form-control-sm" required>
                <?php 
                for($i=date('Y'); $i >= 2020; $i--)
                {
                    echo "<option value='$i'>" . $i . "</option>";
                }
                ?> 
              </select>    
            </div>
            <div class="form-group col-md-4">
                <label for="pos">Month:</label>
                <select name='month' class="form-control form-control-sm" required>
                  <option  value='1'>January</option>
                  <option  value='2'>February</option>
                  <option  value='3'>March</option>
                  <option  value='4'>April</option>
                  <option  value='5'>May</option>
                  <option  value='6'>June</option>
                  <option  value='7'>July</option>
                  <option  value='8'>August</option>
                  <option  value='9'>September</option>
                  <option  value='10'>October</option>
                  <option  value='11'>November</option>
                  <option  value='12'>December</option> 
                </select>
            </div> 
            <div class="form-group col-md-4">
              <label for="amount">Amount:</label>
              <input type="text" class="form-control" name="amount" placeholder="0.00" required>
            </div>
<div class="form-group col-md-12">
              <label for="remarks">Remarks:</label>
               <textarea class="form-control" name="remarks"></textarea>   
            </div>
            </div>
        <input type="hidden" name="bin" id="businessid" value="{{$business->id}}"> 
        <button type="submit" class="btn btn-primary text-left">Save</button>
      </div>
      
    </div>
  </div>
</div>
</form>

<!-- update selfpickupservice for merchant -->
<form method="post" action="{{action('Admin\AdminDashboardController@enableSelfPickupService')}}">
    {{ csrf_field() }}
<div class="modal" tabindex="-1" id="wgtselfpickup">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Enable/Disable Self Pick Up Service</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="form-row">
            <div class="form-group col-md-6">
              <label for="tick">Self Pick Up Service:</label>
                <select  class="form-control" name="selfpickupservice" id="selfpickupservice">
                  <option value='yes'>Enable</option>
                  <option value='no'>Disable</option>
                </select>
            </div> 
            </div>
        <input type="hidden" name="bin" id="businessid" value="{{$business->id}}"> 
        <button type="submit" class="btn btn-primary text-left">Save</button>
      </div>
      
    </div>
  </div>
</div>
</form>


<form action="{{ action('Admin\AdminDashboardController@updateBankAccountInfo') }}" method="post"  enctype="multipart/form-data" >
    {{ csrf_field() }}
   <div class="modal fade" id="splitpaysetup" tabindex="-1" role="dialog" aria-labelledby="del_box" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="del_box"><strong>Split payment setup</strong></h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body ">
       <div class="input-group">
  <div class="col-md-6">
    <label class="form-label" >Bank account no.:</label>
    <input type="text" name='bank_acc' class="form-control">    
  </div>
  <div class="col-md-6">
    <label class="form-label" >IFSC code:</label>
    <input type="text" name='ifsc_code' class="form-control">    
  </div>
  <div class="col-md-6">
    <label class="form-label" >Bank name:</label>
    <input type="text" name='bank_name' class="form-control">    
  </div>
  <div class="col-md-6">
    <label class="form-label" >Razor pay:</label>
    <input type="text" name='rzp_acc_id' class="form-control">    
  </div>
</div> 
 </div>
  <div class="modal-footer">
  <div class="col-md-12 mt-4 text-center">
       <div class="col-md-12">
        <input type="hidden" name='key' value="{{ $data['bin'] }}" >
   
        <button class="btn btn-primary col-md-12" type="submit" name="btnsave" value="save">Save</button>
      </div>
  </div>
    </div>
       
        
       </div>
     </div>
   </div>

 </form>


<form action="{{ action('Admin\AdminDashboardController@archiveActiveCommission') }}" method="post"  enctype="multipart/form-data" >
    {{ csrf_field() }}
 <div class="modal fade" id="wgt-confirmarchive" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Archive Existing Commission Structure</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Business agreed commission structure will be archived. Future orders will not have any commission deducted unless new 
        commission structure is provided. Proceed with caution.</p>
      </div>
      <div class="modal-footer">
        <input type="hidden" name='key' value="{{ $data['bin'] }}" >
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" value="submit" class="btn btn-danger">Confirm Archival Process</button>
      </div>
    </div>
  </div>
</div>
</form>

@endsection
 

@section("script") 

<script> 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

$(document).on("click", ".btnchangebanner", function()
{ 
    $('#modfileupload').modal('show');
})
 
$(document).on("click", ".btnpangstfssai", function()
{ 
    $('#modpangstfssai').modal('show');
})

 

 $(document).on("click", ".btn_upd_comm", function()
{
  var wgtno = $(this).attr("data-wgtno"); 
 
  $("#wgt" + wgtno).modal("show");

});
 

 $(document).on("click", ".btnPaymentUpdateDays", function()
  {
   
  $("#widget-p").modal("show");

  });


  $(document).on("click", ".btnUpdatePhone", function()
  {
   
  $("#widget-phone").modal("show");

  });



  $(document).on("click", ".btnshowbluetick", function()
  { 
      $('#wgtbluetick').modal('show');
  });
 
  $(document).on("click", ".btnshowmerchant", function()
  { 
      $('#wgtpos').modal('show');
  });

  $(document).on("click", ".btnshowpayment", function()
  { 
      $('#wgtpospayment').modal('show');
  });

  $(document).on("click", ".btnshowtakeaway", function()
  { 
      $('#wgtselfpickup').modal('show');
  });
  

$(document).on("click",".splitpay",function()
{
  $("#splitpaysetup").modal("show");
})
</script> 


@endsection




 
  
 