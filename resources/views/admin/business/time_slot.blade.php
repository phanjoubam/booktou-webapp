@extends('layouts.admin_theme_02')
@section('content')
<?php 
$cdn_url =    config('app.app_cdn') ;  
$cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>

<div class="row">
 <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header">
          <div class="card-title"> 
             <div class="row">
               <div class="col-md-8">
                  <div class="title">Time Slot Listing <span class='badge badge-primary'>{{ $business->id }}</span></div> 
              </div>
 
              <div class="col-md-4">  
                <form class="form-inline" method="post" action="{{  URL::to('/admin/business/time-slot-listing-search')    }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                  <div class="form-row">
                    <div class="col-md-5">
                        <select name="month" class="form-control form-control-sm">
                          <option <?php if( $month  == 1 ) echo "selected"; ?> value='1'>January</option>
                          <option <?php if( $month== 2 ) echo "selected"; ?>  value='2'>February</option>
                          <option <?php if( $month == 3 ) echo "selected"; ?> value='3'>March</option>
                          <option <?php if( $month== 4 ) echo "selected"; ?> value='4'>April</option>
                          <option <?php if( $month == 5 ) echo "selected"; ?> value='5'>May</option>
                          <option <?php if( $month == 6 ) echo "selected"; ?> value='6'>June</option>
                          <option <?php if( $month == 7 ) echo "selected"; ?> value='7'>July</option>
                          <option <?php if( $month == 8 ) echo "selected"; ?> value='8'>August</option>
                          <option <?php if( $month == 9 ) echo "selected"; ?> value='9'>September</option>
                          <option <?php if($month == 10 ) echo "selected"; ?> value='10'>October</option>
                          <option <?php if( $month== 11 ) echo "selected"; ?> value='11'>November</option>
                          <option <?php if( $month == 12 ) echo "selected"; ?> value='12'>December</option> 
                        </select>                                  
                  </div>
                  <div class="col-md-5">
                      <select  class="form-control form-control-sm" name='year' >
                        <?php 
                      for($i=2022; $i >= 2020; $i--)
                      {
                        ?> 
                        <option value='{{ $i }}'>{{ $i }}</option>
                      <?php 
                      }
                      ?>
                    </select> 
                </div>
                <div class="col-md-2">
                  <input type="hidden" value="{{ $business->id }}" name="bin">
                 <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
             </div>
         </div>
     </form>
 </div>
</div>
</div>
</div>
<div class='col-md-12'>  
 
 
<div class="card-body  ">
  @foreach($sales as $days) 
  <table class='table table-bordered mt-3'>
    <tr>
      <th colspan="4" class='text-left'>Month:{{ $days->month }}</th>
  </tr>
  <tr>
      <th >Day</th>
      <th >Start Time</th>
      <th >End Time</th>
      <th >Action</th>
  </tr>
  @foreach($time_slots as $duration)
  @if( $days->bin == $duration->bin)
  <tr>
     <td>{{ $duration->day_name  }}</td>
     <td>
        {{ $duration->start_time  }}
    </td>
    <td>
        {{ $duration->end_time  }}
    </td>
    <td>
     <div class="dropdown  ">
      <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog "></i></a>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"></div>
  </div>
</td>

</tr> 
@endif
@endforeach
</table>
@endforeach 
{{ $time_slots->links() }}

</div>    
</div>


</div> 

</div> 
</div>

@endsection
@section("script")

<script>

</script>


@endsection

