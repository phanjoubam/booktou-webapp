@extends('layouts.admin_theme_02')
@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path');
?>

<div class="card mt-2">
   <div class="p-4">
  <div class='row'>
    <div class='col-md-8'>
      <h4>Booking/Appointment Businesses</h4>
    </div>
    <div class='col-md-4 text-right'>
    <form class="form-inline"   method="get" action="{{ action('Admin\ServiceBookingAppointmentController@searchBusiness') }}" enctype="multipart/form-data">
      {{  csrf_field() }}
      <div class="form-row">
        <div class="col-md-12">
          <input type="text" name ="search_key" class="form-control form-control-sm"  />
          <select name ="blocked" class="form-control form-control-sm"  >
            <option value="yes" >Blocked</option>
            <option value="no" selected >Active</option>
          </select>

          <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
        </div>

      </div>
    </form>
    </div> 
  </div>
</div> 
</div> 

  
  <div class="row mt-3">

    <div class='col-md-12'>
      @if (session('err_msg'))
      <div class="alert alert-info p-0">
        {{session('err_msg')}}
      </div>
      @endif
    </div>
      @php
        $i = 0;
      @endphp

		@foreach ($data['results'] as $business)

  		@php
        $i++  ;
        $found = false;
      @endphp
      <div class="col-md-4">
        <div class="card  ">
          <div class="card-body  ">
            <h5 class="mt-0">{{ $business->name}}</h5>
            <hr/>
            <div class="media">
              <img src="{{ $cdn_url . $business->banner  }}" width="120px" class="mr-3 img-rounded" alt="{{ $business->name}}">
              <div class="media-body">
                <p>{{$business->locality}},<br/>
                {{$business->landmark}},<br/>
                {{$business->city}} - {{$business->state}}
                {{$business->pin}}</p>
              </div>
            </div>

 


            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="paymode">Primary Phone - {{$business->phone_pri}}</label> 
              </div>

              <div class="form-group col-md-12">
                <label for="paymode">Alternate Phone - {{$business->phone_alt}}</label> 
              </div>

              <div class="form-group col-md-12">
                <label for="paymode">Is Open - <span class='badge badge-pill badge-primary'>{{$business->is_open}}</span></label> 
              </div>
  
            </div>
          </div>
          <div class="card-footer">

      <a class="btn btn-primary btn-rounded" href="{{URL::to('/admin/services/business/package/search')}}?bin={{$business->id}}">Packages</a>

     </div>
     </div>
   
</div>

  @endforeach
 
  {{ $data['results']->links() }}
</div>



@endsection



@section("script")

<script>
 $(document).on("click", ".showwgt03", function()
{
  $("#businessid").val($(this).attr("data-bin"));
  $("#wgt03").modal("show");
});
 // .................meena............

$(document).on("click", ".showwgt02", function()
{
  $("#key02").val($(this).attr("data-bin"));
  $("#wgt02").modal("show");
});





$(document).on("click", ".btn_add_promo", function()
{
  $("#bin").val($(this).attr("data-bin"));
  $("#modal_add_promo").modal("show");
});







$('body').delegate('.btnToggleBlock','click',function(){

   var bin  = $(this).attr("data-id");
   var status = $(this).is(":checked");
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;




   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

      },
      error: function( )
      {
        $(".loading_span").html(" ");
        alert(  'Something went wrong, please try again');
      }

    });



})






$('body').delegate('.btnToggleClose','click',function(){

   var bin  = $(this).attr("data-id");
   var status = $(this).is(":checked");

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;


   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);
      },
      error: function( )
      {
        $(".loading_span").html(" ");
        alert(  'Something went wrong, please try again');
      }

    });



})




</script>


@endsection
