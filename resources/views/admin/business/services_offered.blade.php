@extends('layouts.admin_theme_02')
@section('content')

<?php 
$cdn_url =   env('APP_CDN') ;  
    $cdn_path =   env('APP_CDN_PATH') ; // cnd - /var/www/html/api/public

    ?>

    <div class="row">
     <div class="col-md-12"> 

       <div class="card card-default">
         <div class="card-header">
          <div class="card-title"> 
            <div class="row">
             <div class="col-md-7">
               <h5>Service Listed Under <span class='badge badge-primary'>{{  $data['business']->name }}</span></h5>
             </div>
           </div> 
         </div>
       </div>
       <div class="card-body">
        @if (session('err_msg'))
        <div class="col-md-12">
          <div class="alert alert-info">
           {{session('err_msg')}}
         </div>
       </div>
       @endif 
       <div class="table-responsive">
        <table class="table"> 
          <thead class=" text-primary">
            <tr >
              <th scope="col">Image</th>
              <th scope="col">Item </th> 
              <th scope="col">Price</th>
              <th scope="col">Discount</th>
              <th scope="col">Service Category</th>
              <th scope="col">Service type</th>
              <th scope="col">Gender</th>
              <th scope="col">Action</th>
            </tr>
          </thead> 
          <tbody> 
            @foreach ( $data['services'] as $item)  
            <tr >
             <td> 
             </td>
             <td style="white-space: normal;"><span class='badge badge-primary'>{{ $item->id }}</span><br/>
              {{ $item->srv_name }}<br/>     
              <td>{{$item->pricing}}</td>
              <td>{{$item->discount}}</td> 
              <td>{{$item->service_category}}</td>
              <td>{{$item->service_type}}</td>
              <td>{{$item->gender}}</td>
              <td class="text-center">

                <div class="dropdown">
                  <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog "></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">  
                    <a class="dropdown-item showdialogm1"
                    data-name="{{ $item->service_category }}" 
                    data-key="{{ $item->id }}" 
                    data-photo="{{ $item->photos}}"
                    >Add To photos</a>
                  </div>
                </div>                  
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $data['services']->links() }}
      </div>
    </div>
  </div>    
</div> 
</div>
<form action='{{ action("Admin\AdminDashboardController@addPhotosServiceProducts") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='widget-m1' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Selected Product To Promotion</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
           <div class="col-md-6">
            <label  class="form-label">Category-Image</label>
            <input class="form-control" type="file" id="photo" name="photo">
            <input  type="hidden" id="serviceCategory" name="serviceCategory">
          </div>
        </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='key' id='key' />
        <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>
@endsection
@section("script")
<script>
  $(document).on("click", ".showdialogm1", function()
  {
    $("#serviceCategory").val( $(this).attr("data-key"));
    $("#key").val( $(this).attr("data-key"));
    $("#photo").attr("src", $(this).attr("data-photo")); 

    $("#widget-m1").modal("show")
  });
</script>

@endsection