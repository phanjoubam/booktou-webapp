@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 



<div class="cardbodyy margin-top-bg"> 


  <div class="col-md-12"> 
 <div class="card"> 
 	  <div class="card-header"> 
 <div class="row">
   <div class="col-md-6">
    <h3>Verify and Import Products</h3>
   </div>
    <div class="col-md-6 text-right"> 
      <h3>Selected Business  <span class='badge badge-info'>{{ $data['business']->name  }}</span></h3>
      </div>
  </div>    
  
            <hr/>
       </div>



 	<div class="card-body"> 
 
 <form method="post" action="{{  action('Admin\AdminDashboardController@saveImportableProducts') }}">
            {{ csrf_field() }}  
       <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
		<th scope="col"></th>
		<th scope="col">Name</th> 
		<th scope="col">Description</th>
		<th scope="col">Unit</th>
    <th scope="col">Unit Value</th>
		<th scope="col">Price</th>
    <th scope="col">Stock</th>
    <th scope="col">Category</th>
		</tr>
	</thead>
	
		<tbody>

			 <?php $i = 0 ?>
		@foreach ($data['products'] as $item)
  

		 <tr  >
		 <td><input name='cb_select[]' value="{{ $i }}"  type='checkbox' class='cbselector' /></td>
		 <td><input name='pr_name[]' class='form-control form-control-sm' value='{{$item->pr_name}}' /></td> 
		 <td><input name='pr_desc[]' class='form-control form-control-sm'value='{{$item->description}}' /></td>
		 <td><input name='pr_unit[]' class='form-control form-control-sm'value='{{$item->unit_value}}' /></td>
     <td><input name='pr_unitname[]' class='form-control form-control-sm'value='{{$item->unit_name}}' /></td>
     <td><input name='pr_price[]' class='form-control form-control-sm'value='{{$item->unit_price}}' /></td>
     <td><input name='pr_stock[]' class='form-control form-control-sm'value='{{$item->stock}}' /></td>
     <td>
      <select name='pr_category[]' class='form-control form-control-sm'  >
        @foreach($data['category'] as $citem)

          <option  <?php if( strcasecmp($citem->category_name , $item->category) ==0 ) echo "selected" ; ?> >{{ $citem->category_name }}</option>

        @endforeach 
     </select>

     <input name='id[]' type='hidden' value='{{$item->id}}' />

     </td>  
		 </tr>
     <?php  
       $i++  ;
      ?>
	@endforeach


   <tr  >
    <td><input id='cb_select_all'  type='checkbox' /></td>

     <td colspan='7'>
  

    <input name='bin'  value='{{ $data["bin"] }}' type='hidden' />


      <button name='btn_save' value='save' type='submit' class='btn btn-primary'>Import Selected Products</button></td>
     
     </td> 
 
     </tr>
 
	</tbody>
		</table>
	 </form>

	</div>
 
	</div> 
</div>

</div>

 
 
@endsection

 
 @section("script")
 
  
 <script>
 
 
 $('#cb_select_all').change(function () {
    
    var checked = $(this).is(":checked"); 

    $(".cbselector"). prop("checked", checked);

 });



</script> 

@endsection 
