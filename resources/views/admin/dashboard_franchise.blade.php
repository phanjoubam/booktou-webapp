@extends('layouts.admin_theme_04')
@section('content')
 
 <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-3 col-md-6">
                        <div class="d-flex">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold">@if( isset ($data['sales']->totalSold ) ) 
                  {{ $data['sales']->totalSold }}
                @else 
                  0.00
                @endif &#x20b9;</h3>
                            <h5 class="mb-0 font-weight-medium text-primary">Total Sales</h5>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          <div class="wrapper my-auto ml-auto ml-lg-4">
                            <canvas height="50" width="100" id="stats-line-graph-1"></canvas>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
                        <div class="d-flex">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold">@if( isset ($data['sales']->agentPayable )) 
                       {{ $data['sales']->agentPayable }}
                    @else 
                      0.00
                    @endif &#x20b9;</h3>
                            <h5 class="mb-0 font-weight-medium text-primary">Agent Commission</h5>
                            <p class="mb-0 text-muted"></p>
                          </div>
                          <div class="wrapper my-auto ml-auto ml-lg-4">
                            <canvas height="50" width="100" id="stats-line-graph-2"></canvas>
                          </div>
                        </div>
                      </div>

                  
 
              
                      <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
                        <div class="d-flex">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold">@if( isset ($data['sales']->serviceFee )) 
                       {{ $data['sales']->serviceFee }}
                    @else 
                      0.00
                    @endif &#x20b9;</h3>
                            <h5 class="mb-0 font-weight-medium text-primary">bookTou Earning</h5>
                            <p class="mb-0 text-muted"></p>
                          </div>
                          <div class="wrapper my-auto ml-auto ml-lg-4">
                            <canvas height="50" width="100" id="stats-line-graph-3"></canvas>
                          </div>
                        </div>
                      </div>
                    

                    <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
                        <div class="d-flex">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold">{{   count($data['active_users']) }} Active</h3>
                            <h5 class="mb-0 font-weight-medium text-primary">
                              <a href='{{ URL::to('/admin/active-users-activity-history') }}' target='_blank'>Customers</a>
                            </h5>
                            <p class="mb-0 text-muted"></p>
                          </div>
                          <div class="wrapper my-auto ml-auto ml-lg-4">
                            <canvas height="50" width="100" id="stats-line-graph-4"></canvas>
                          </div>
 
                        </div>
                      </div>


                    </div>
                  </div>
                </div>
              </div>
            </div>

 
 
      
@php 
    $target = 20; 
    $totalOrders = 0; 
    $totalPndOrders = 0; 
    $totalOrdersCompleted =0 ;
    $totalPndOrdersCompleted = 0; 
@endphp

@foreach( $data['normal_orders'] as $pnditem ) 
  @if( $pnditem->book_status != "cancel_by_client" && $pnditem->book_status != "cancel_by_owner"   && 
  $pnditem->book_status != "returned"  && $pnditem->book_status  != "canceled"    )  
       @php      
$totalOrders++;  

          
      @endphp  
@endif
  @if( $pnditem->book_status == "completed" || $pnditem->book_status == "delivered"   )  
    @php 
      $totalOrdersCompleted++; 

    @endphp 
  @endif  
@endforeach


@foreach( $data['pick_and_drop'] as $pnditem ) 
  @if( $pnditem->book_status != "cancel_by_client" && $pnditem->book_status !="cancel_by_owner"  &&
      $pnditem->book_status != "returned"  &&  $pnditem->book_status  !=  "canceled"  )  
       @php      
          $totalPndOrders++; 
      @endphp  
  
  @endif

  @if( $pnditem->book_status == "completed" || $pnditem->book_status == "delivered"   )  
    @php 
      $totalPndOrdersCompleted++;

    @endphp 
  @endif  
@endforeach


 <div class="row">   
      <div class="col-xs-6 col-md-3 grid-margin stretch-card">
        <div class="card card-default">
          <div class="card-body easypiechart-panel">
            <h4>Normal Orders</h4>
            <div class="easypiechart" id="easypiechart-orange" data-percent="{{ (   $totalOrders  * 100 ) /  30  }}" >
              <a href="{{ URL::to('/admin/customer-care/orders/view-all') }}"> 
              <span class="percent h1">{{ $totalOrders }}/30</span>
            </a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-6 col-md-3">
        <div class="card card-default">
          <div class="card-body easypiechart-panel">
            <h4>Pick-and-Drop Orders</h4>
            <div class="easypiechart" id="easypiechart-teal" data-percent="{{ (   $totalPndOrders  * 100 ) /  70  }}" >
              <a href="{{ URL::to('/admin/customer-care/pick-and-drop-orders/view-all') }}"> 
                <span class="percent h1">{{ $totalPndOrders  }}/70</span></a>
            </div>
          </div>
        </div>
      </div> 

    <div class="col-xs-6 col-md-3">
        <div class="card card-default">
          <div class="card-body easypiechart-panel">
            <h4>Completed Orders</h4>
            <div class="easypiechart" id="easypiechart-red" data-percent="{{ count($data['active_users']) }}" > 
              <span class="percent h2">{{ $totalPndOrdersCompleted }} Pnd / {{ $totalOrdersCompleted }} Normal</span> 
                
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-6 col-md-3">
        <div class="card bg-danger card-default">
          <div class="card-body easypiechart-panel">
            <h4 class='white'>Min Target till 15th, {{ date('F') }}</h4>
            <div class="easypiechart"     >
                <span class="percent h3 white">
                {{ $data['pnd_income_achieved']  }} of {{ ( $data['sales_target'] != null ) ? $data['sales_target']->min / 2 : 50000 }} ₹
                </span> 
            </div>
          </div>
        </div>
      </div>


    </div> 

 

   <div class="row">    
     <div class="col-md-6"> 
                    <div class="card card-default">
                        <div class="card-header">
                            New Customer Signup
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                               @if(count($data['new_customers'] ) > 0 ) 

                                <table class="table table-striped">
                                    <thead>
                                        <tr> 
                                            <th>Customer Name</th>
                                            <th>Phone</th>
                                             <th>OTP</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data['new_customers'] as $cust)
                                        <tr> 
                                          <td class="text-left">
                                            <a href="{{ URL::to('/admin/customer/view-complete-profile') }}/{{  $cust->id  }}" target='_blank'>
                                              {{$cust->fullname }}
                                            </a> 
                                          </td>
                                           <td class="text-left">{{$cust->phone }}</td>
                                            <td class="text-left"><span class='badge badge-primary'>{{ $cust->otp == -1 ?  "Signup Complete" : $cust->otp  }}</span></td>
                                          <td class="td-actions text-right">
                                            {{$cust->locality}}, {{$cust->landmark }}
                                          </td>
                                        </tr>
                                         
                                        @endforeach
                                    </tbody>
                                </table>

                                 @else 
                                <p class='alert alert-info'>No New signup today.</p>
                                @endif 

                            </div>
                        </div>
                    </div> 
                 
                      <!--    Striped Rows Table  -->
                    <div class="card card-default mt-4">
                        <div class="card-header">
                            PnD Customers Converted to User
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                               @if(count($data['migrated_users'] ) > 0 ) 

                                <table class="table table-striped">
                                    <thead>
                                        <tr> 
                                            <th>Customer Name</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data['migrated_users'] as $cust)
                                        <tr> 
                                          <td class="text-left">{{$cust->fullname }}</td>
                                           <td class="text-left">{{$cust->phone }}</td>
                                          <td class="td-actions text-right">
                                            {{$cust->locality}}, {{$cust->landmark }}
                                          </td>
                                        </tr>
                                         
                                        @endforeach
                                    </tbody>
                                </table>

                                 @else 
                                <p class='alert alert-info'>No New signup today.</p>
                                @endif 

                            </div>
                        </div>
                    </div> 
                </div>
 
 
              <div class="col-md-6 grid-margin  main-panel-dashboard">
                

                <div class="card">
                  <div class="card-header">
                            <h4 class="card-title mb-0">Sales Performance Graph</h4>
                   
                        </div>
                        <div class="card-body"> 
                    
                    <canvas class="mt-5" height="120" id="sales-statistics-overview"></canvas>
                  </div>
                </div>
 

              </div>
             
  


              </div>  
 
 

@endsection



@section("script")

<script> 

 
$(function() {
  'use strict'; 

    var lineStatsOptions = {
      scales: {
        yAxes: [{
          display: false
        }],
        xAxes: [{
          display: false
        }]
      },
      legend: {
        display: false
      },
      elements: {
        point: {
          radius: 0
        },
        line: {
          tension: 0
        }
      },
      stepsize: 100
    }



    if ($('#sales-statistics-overview').length) {
      var salesChartCanvas = $("#sales-statistics-overview").get(0).getContext("2d");
      var gradientStrokeFill_1 = salesChartCanvas.createLinearGradient(0, 0, 0, 450);
      gradientStrokeFill_1.addColorStop(1, 'rgba(255,255,255, 0.0)');
      gradientStrokeFill_1.addColorStop(0, 'rgba(102,78,235, 0.2)');
      var gradientStrokeFill_2 = salesChartCanvas.createLinearGradient(0, 0, 0, 400);
      gradientStrokeFill_2.addColorStop(1, 'rgba(255, 255, 255, 0.01)');
      gradientStrokeFill_2.addColorStop(0, '#14c671');
  
      <?php 
        
        $dates = array(); 
        $normal_orders  = array();
        $pnd_orders  = array();
        foreach($data['pnd_sales_chart_data'] as $item)
        {
          
          $dates[] =  '"'.  $item->serviceDate . '"' ;
          $pnd_orders[] =  $item->pndOrders ;
        }


        foreach($data['normal_sales_chart_data'] as $item)
        { 
          $normal_orders[] =  $item->normalOrder ;
        }

      ?>  

      var data_1_1 = [ <?php echo implode(',', $pnd_orders) ; ?> ];
      var data_1_2 = [ <?php echo implode(',', $normal_orders) ; ?> ];
      var areaData = {
        labels: [ <?php echo implode(",",  $dates ); ?>],
        datasets: [{
          label: 'PnD Orders',
          data: data_1_1,
          borderColor: infoColor,
          backgroundColor: gradientStrokeFill_1,
          borderWidth: 2
        }, {
          label: 'Normal Orders',
          data: data_1_2,
          borderColor: successColor,
          backgroundColor: gradientStrokeFill_2,
          borderWidth: 2
        }]
      };
      var areaOptions = {
        responsive: true,
        animation: {
          animateScale: true,
          animateRotate: true
        },
        elements: {
          point: {
            radius: 3,
            backgroundColor: "#fff"
          },
          line: {
            tension: 0
          }
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        legend: false,
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul>');
          for (var i = 0; i < chart.data.datasets.length; i++) { 
            text.push('<li>');
            text.push('<span style="background-color:' + chart.data.datasets[i].borderColor + '">' + '</span>');
            text.push(chart.data.datasets[i].label);
            text.push('</li>');
          }
          text.push('</ul></div>');
          return text.join("");
        },
        scales: {
          xAxes: [{
            display: false,
            ticks: {
              display: false,
              beginAtZero: false
            },
            gridLines: {
              drawBorder: false
            }
          }],
          yAxes: [{
            ticks: {
              max: 170,
              min: 0,
              stepSize: 50,
              fontColor: "#858585",
              beginAtZero: false
            },
            gridLines: {
              color: '#e2e6ec',
              display: true,
              drawBorder: false
            }
          }]
        }
      }
      var salesChart = new Chart(salesChartCanvas, {
        type: 'line',
        data: areaData,
        options: areaOptions
      });
      document.getElementById('sales-statistics-legend').innerHTML = salesChart.generateLegend(); 
    }
  


 
}) ;


 
 

</script> 
@endsection