@extends('layouts.admin_theme_02')
@section('content')

 


   <div class="row">
     <div class="col-md-12"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title">
                  <div class="title">{{ $data['title'] }}</div>
                </div>
            </div>
       <div class="card-body"> 

        <div class="row"> 
               <div class="col-md-8"> 
                   
                </div>
               <div class="col-md-4 text-right"> 
                   <button type="button" class="btn btn-success btn-xs btn-add-voucher" data-widget="addvoucher"   >Add Voucher</button>
               </div>
          </div> 
      <hr/> 
      @if( count(  $data['results'] ) > 0 )
   <div class="table-responsive">
    <table class="table">
                    <thead class=" text-primary">  
                  <tr class="text-center" style='font-size: 12px'>  
                    <th scope="col">Voucher CODE</th>
                    <th scope="col">Service Fee Discount</th>
                    <th scope="col">Delivery Commission Discount</th> 
                    <th scope="col">Seller Discount</th> 
                    <th scope="col" colspan="2" class='text-center'>Action</th>  
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1; 
                
                foreach ($data['results'] as $item)
                { 
                ?>
  
                  <tr>  
                  <td class="text-center">
                    <strong>{{$item->code}} </strong>
                  </td>
                  <td class="text-center">
                    <strong>{{$item->service_fee_discount}}</strong>
                  </td>
                  <td class="text-center">
                    <strong>{{$item->agent_payable_discount}}</strong>
                  </td>
                  <td class="text-center">
                    <strong>{{$item->total_cost_discount}}</strong>
                  </td>

                    
                       <td class='text-center'>
                          <button class='btn btn-sm btn-success btn-notify' data-id="{{ $item->id }}"><i class='fa fa-bell'></i></button>
                       </td>

                       <td>
                         <div class="dropdown">
                             <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fa fa-cog "></i>
                             </a>

                            <ul class="dropdown-menu dropdown-user pull-right"> 
                            <li>
                            <a class="dropdown-item btnupdateVoucher"  href="#" data-id="{{ $item->id }}"
                            data-business="{{$item->bin}}"     
                            data-category="{{$item->biz_category}}" 
                            data-voucher="{{$item->code}}"     
                            data-seller="{{$item->total_discount}}" 
                            data-coupon-valid="{{date('d-m-Y',strtotime($item->valid_from))}}" 
                            data-coupon-expired="{{date('d-m-Y',strtotime($item->ends_on))}}" 
                            data-start-time="{{date('H:i:s',strtotime($item->start_time))}}" 
                            data-end-time="{{date('H:i:s',strtotime($item->end_time))}}" 
                            data-description="{{$item->description}}"
                            >
                              Update Voucher
                            </a>
                            </li> 

                                </ul>
                          </div>

                       </td>  
                                 
                         </tr>
                         <?php
                          $i++; 
                       }

                          ?>
                </tbody>
                  </table>
           </div>

        @else 

          <p class='alert alert-info'>No voucher codes added yet!</p>

        @endif 


            </div>
          </div>
        </div>  
   </div>
 
    
 
<form action="{{ action('Admin\AdminDashboardController@saveCoupon') }}" method="post"> 
  
  {{ csrf_field()  }}

 <div class="modal" id='widget-addvoucher' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title">Add New Voucher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="business">Select Business:</label>
      <select  type="email" class="form-control" id="business" name='business'>
        <option value='0'>All Businesses</option>
        @foreach($data['businesses'] as $item)
          <option value='{{ $item->id }}'>{{ $item->name }}</option>
        @endforeach
      </select>
  </div>

<div class="form-group col-md-6">
      <label for="category">Business Category:</label>
      <select  type="email" class="form-control" id="category" name='category'>
        <option value='all'>All Categories</option>
        @foreach($data['biz_categories'] as $item)
          <option value='{{ $item->name }}'>{{ $item->name }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group col-md-6"> 
       <label for="voucherCode">Voucher Code:</label>
      <input type="text" class="form-control" id="voucherCode" name="voucherCode" required> 
    </div>
     <div class="form-group col-md-4">
      <label for="discountSeller">Seller Discount</label>
      <input type="number" class="form-control" id="discountSeller" name="discountSeller" required> 
    </div>

    <div class="form-group col-md-6"> 
       <label for="validdate">Coupon Valid from:</label>
       <input type="text" class="form-control calendar" id="validdate" name="validdate" required> 
    </div>
 <div class="form-group col-md-6"> 
       <label for="enddate">Coupon Expires On:</label>
       <input type="text" class="form-control calendar" id="enddate" name="enddate" required> 
    </div>

    <div class="form-group col-md-6"> 
       <label for="validtime">Validity Start Time:</label>
       <input type="time" class="form-control" id="validtime" name="validtime" required value='00:00 AM'> 
    </div>
 <div class="form-group col-md-6"> 
       <label for="validendtime">Validity End Time:</label>
       <input type="time" class="form-control " id="validendtime" name="validendtime" required value='11:59 PM'> 
    </div>


    <div class="form-group col-md-12">
    <label for="description">Description</label>
    <textarea rows='4' class="form-control" id="description" name="description"></textarea>
  </div> 
 
  </div> 
    
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span> 
        <button type="submit" class="btn btn-success"   name='btnsave' value='save' >Save</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div> 
  </div>
</div>
</form>
 

<!-- update voucher code -->

<form action="{{ action('Admin\AdminDashboardController@updateCoupon') }}" method="post"> 
  
  {{ csrf_field()  }}

 <div class="modal" id='widget-updatevoucher' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title">Update Voucher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  <div class="modal-body">
    <div class="form-row">
    <div class="form-group col-md-6">
      <label for="business">Select Business:</label>
      <select  type="email" class="form-control" id="business1" name='business'>
        <option value='0'>All Businesses</option>
        @foreach($data['businesses'] as $item)
          <option value='{{ $item->id }}'>{{ $item->name }}</option>
        @endforeach
      </select>
  
    </div>

    <div class="form-group col-md-6">
      <label for="category">Business Category:</label>
      <select  type="email" class="form-control" id="category1" name='category'>
        <option value='all'>All Categories</option>
        @foreach($data['biz_categories'] as $item)
          <option value='{{ $item->name }}'>{{ $item->name }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group col-md-6"> 
       <label for="voucherCode">Voucher Code:</label>
      <input type="text" class="form-control" id="voucherCode1" name="voucherCode" required> 
    </div>
    <div class="form-group col-md-4">
      <label for="discountSeller">Seller Discount</label>
      <input type="number" class="form-control" id="discountSeller1" name="discountSeller" required> 
    </div>

    <div class="form-group col-md-6"> 
       <label for="validdate">Coupon Valid from:</label>
       <input type="text" class="form-control calendar" id="validdate1" name="validdate" required> 
    </div>
    <div class="form-group col-md-6"> 
       <label for="enddate">Coupon Expires On:</label>
       <input type="text" class="form-control calendar" id="enddate1" name="enddate" required> 
    </div>

    <div class="form-group col-md-6"> 
       <label for="validtime">Validity Start Time:</label>
       <input type="time" class="form-control" id="validtime1" name="validtime" required value='00:00 AM'> 
    </div>
    <div class="form-group col-md-6"> 
       <label for="validendtime">Validity End Time:</label>
       <input type="time" class="form-control " id="validendtime1" name="validendtime" required value='11:59 PM'> 
    </div>


    <div class="form-group col-md-12">
    <label for="description">Description</label>
    <textarea rows='4' class="form-control" id="description1" name="description"></textarea>
    </div> 
 
    </div> 
    </div>

      <div class="modal-footer"> 
      <span class='loading_span'></span>
      <input type="hidden" value="" id="voucher" name="voucher">
      <button type="submit" class="btn btn-success"   name='btnupdate' value='update' >Update</button> 
      <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div> 
  </div>
</div>
</form>


<!-- coupon update ends here -->





@endsection

@section("script")

<script>

 
 
$(document).on("click", ".btn-add-voucher", function()
{
  var id = $(this).attr("data-widget");

  $("#widget-" +id ).modal("show") ;

});





$('body').delegate('#btn-notify','click',function()
{

  var cmid ; 
  $('.cmids').each(function() 
  {
    if($(this).is(':checked'))
    {
      cmid =  $(this).val();
    }
  });

  var mid = $("#hidcid").val(); 
 

  if(cmid > 0 && mid > 0)
  {
    var json = {}; 
    json['cmid'] = cmid ;
    json['mid'] = mid ; 
    $(".loading_span").html("<img width='90px' src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $.ajax({
        type: 'post',
        url: api + "v2/web/customer-care/notification/send-cloud-message" ,
        data: json,
        success: function(data)
        {
          data = $.parseJSON(data);   
          $(".loading_span").html(" ");
          $("#notifyModal").modal("hide") 
        },
        error: function( ) 
        {
          $(".loading_span").html(" ");
          alert(  'Something went wrong, please try again'); 
        } 

      });
  }
  else 
  {
    alert("No receipt or message selected!");
  } 

})



$(document).on("click", ".btnupdateVoucher", function()
{
  var id = $(this).attr("data-id");

   $("#business1").val( $(this).attr("data-business") );
   $("#category1").val( $(this).attr("data-category") );
   $("#voucherCode1").val( $(this).attr("data-voucher") );
   $("#discountSeller1").val( $(this).attr("data-seller") );
   $("#validdate1").val( $(this).attr("data-coupon-valid") );
   $("#enddate1").val( $(this).attr("data-coupon-expired") );
   $("#validtime1").val( $(this).attr("data-start-time") );
   $("#validendtime1").val( $(this).attr("data-end-time") );
   $("#description1").val( $(this).attr("data-description") );
   $("#voucher").val(id);
   $("#widget-updatevoucher").modal("show");





});



$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
</script> 

@endsection 