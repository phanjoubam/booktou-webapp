@extends('layouts.admin_theme_hollow')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
  $normal = $order_info; 
?>
 
    
  
        @php  

          $orderCost = $packagingCost = $commission =   $deliveryFee =  $commissionPc = $dueOnMerchant = 0.00; 
          $totalOrderCost  = $totalPackagingCost = $totalDueOnMerchant  = $totalCommission = $totalDeliveryFee = 0.00;  
          $totalCashMerchant = $totalOnlineMerchant =     0.00; 
          $totalCashbookTou = $totalOnlinebookTou = 0.00;
          $totalCommCashbookTou =  $totalCommOnlinebookTou =   0.00;
          $totalDeliveryCashbookTou = $totalDeliveryOnlinebookTou =  0.00;  
          $totalCommCashMerchant = $totalDeliveryCashMerchant = 0.00;
          $totalCommOnlineMerchant= $totalDeliveryOnlineMerchant =     0.00;
          $totalPayable= 0.00;

          $type ="none";

        @endphp 
           

          @php
            $tempPackagingCost = 0.00;
          @endphp

          @if($normal->bookingStatus == "delivered")
           
              @if($normal->orderType == "normal")

                @foreach($cart_items as $cart)
                    @if($normal->id == $cart->order_no) 
                      @php 
                        $tempPackagingCost += $cart->qty * $cart->package_charge;
                      @endphp 
                    @endif
                @endforeach

                @php 
                  $normal->packingCost  = $tempPackagingCost;
                @endphp

             
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0 )
                      @php
                        $type ="CashFree India Pvt Ltd.";
                      @endphp  
                    @else 
                      @php
                        $type ="CASH";
                      @endphp 
                       
                    @endif 
               
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0 )
                      @php 
                        $commissionPc = ($business == null) ? 0 : $business->commission;
                        $orderCost = $normal->orderCost;
                        $deliveryFee = $normal->deliveryCharge; 
                        $packagingCost = $normal->packingCost; 
                        $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                        $totalOrderCost += $orderCost;
                        $totalPackagingCost += $packagingCost;   
                      @endphp
                    @else 
                      @php 
                        $commissionPc = ($business == null) ? 0 : $business->commission;
                         $orderCost = $normal->orderCost;
                        $deliveryFee = $normal->deliveryCharge; 
                        $packagingCost = $normal->packingCost; 
                        $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                        $totalOrderCost += $orderCost;
                        $totalPackagingCost += $packagingCost;   
                      @endphp
                    @endif 
 

                   @if( strcasecmp($normal->paymentType , "online") == 0 || 
                        strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                    
                    @php 
                      $totalOnlinebookTou +=  $orderCost; //packing cost already included 
                      $totalDeliveryOnlinebookTou +=   $deliveryFee;
                      $totalCommOnlinebookTou +=  $commission;  
                      $dueOnMerchant = $deliveryFee  + $commission;   
                      $totalPayable +=  $orderCost;
                    @endphp  

                   @else 

                    @php 
                      $totalCashbookTou +=  $orderCost  ;  //packing cost already included
                      $totalDeliveryCashbookTou +=   $deliveryFee;
                      $totalCommCashbookTou +=  $commission;   
                      $dueOnMerchant =  0.00;  
                      $totalPayable +=  $orderCost;
                    @endphp

                  @endif 
                

                @elseif($normal->orderType == "pnd") 

                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                      @php 
                        $type = "CASH";
                        $commissionPc = ($business == null) ? 0 : $business->commission;  
                      @endphp 
                      
                      @if( strcasecmp($normal->paymentType , "online") == 0  )   

                          @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                            strcasecmp($normal->paymentTarget , "business") == 0 )  

                            @php  
                              $type = "CASH";
                               $orderCost = $normal->orderCost;
                              $deliveryFee = $normal->deliveryCharge; 
                              $packagingCost = $normal->packingCost; 
                              $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;    
                            @endphp

                            @php
                              $totalOnlineMerchant +=  $orderCost; 
                              $totalCommOnlineMerchant  +=  $commission; 
                              $totalDeliveryOnlineMerchant +=   $deliveryFee;  
                              $dueOnMerchant = $deliveryFee  + $commission;   
                            @endphp 
                          @else 
                            @php  
                              $type = "CASH";
                              $orderCost = $normal->orderCost;
                              $deliveryFee = $normal->deliveryCharge; 
                              $packagingCost = $normal->packingCost; 
                              $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;    
                            @endphp

                              @php
                                $totalOnlinebookTou +=  $orderCost; 
                                $totalDeliveryOnlinebookTou +=   $deliveryFee;
                                $totalCommOnlinebookTou += $commission;
                                $dueOnMerchant =  0.00; 
                                $totalPayable +=  $orderCost;
                              @endphp  
                          @endif   

                      @else

                          @php  
                            $type = "CASH";
                            $orderCost = $normal->orderCost;
                            $deliveryFee = $normal->deliveryCharge; 
                            $packagingCost = $normal->packingCost; 
                            $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                            $totalOrderCost += $orderCost;
                            $totalPackagingCost += $packagingCost;    
                          
                            $totalCashbookTou +=  $orderCost; 
                            $totalDeliveryCashbookTou +=   $deliveryFee;
                            $totalCommCashbookTou +=  $commission;  
                            $dueOnMerchant =  0.00; 
                            $totalPayable +=  $orderCost;
                          @endphp 

                      @endif

                 
                  @else 
  
                      @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        

                          @if(  strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                                strcasecmp($normal->paymentTarget , "business") == 0 )   
                            @php
                              $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = $normal->orderCost;
                              $deliveryFee = $normal->deliveryCharge; 
                              $packagingCost = $normal->packingCost; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;

                              $totalOnlineMerchant +=  $orderCost; 
                              $totalCommOnlineMerchant  +=  $commission; 
                              $totalDeliveryOnlineMerchant +=   $deliveryFee;  
                              $dueOnMerchant = $deliveryFee  + $commission;   
                            @endphp  
                          @else 
                            @php
                               $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = $normal->orderCost;
                              $deliveryFee = $normal->deliveryCharge; 
                              $packagingCost = $normal->packingCost; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;   


                              $totalOnlinebookTou +=  $orderCost; 
                              $totalDeliveryOnlinebookTou +=   $deliveryFee;
                              $totalCommOnlinebookTou += $commission; 
                              $dueOnMerchant =  0.00; 
                              $totalPayable +=  $orderCost;
                            @endphp  
                          @endif  
                       @else

                            @php
                              $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = $normal->orderCost;
                              $deliveryFee = $normal->deliveryCharge; 
                              $packagingCost = $normal->packingCost; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;   
                              $totalCashbookTou +=  $orderCost; 
                              $totalDeliveryCashbookTou +=   $deliveryFee;
                              $totalCommCashbookTou +=  $commission;
                              $dueOnMerchant =  0.00; 
                              $totalPayable +=  $orderCost;
                            @endphp     
                        @endif  

                  @endif

        
                 
                @elseif($normal->orderType == "assist")
  

                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0) 
                      
                    @if( strcasecmp($normal->paymentType , "online") == 0  )  
                        @php
                          $type = "CASH";
                          $commission = $commissionPc =   0  ;
                          $orderCost = $normal->orderCost;
                          $deliveryFee = $normal->deliveryCharge; 
                          $packagingCost = $normal->packingCost;  
                          $totalOrderCost += $orderCost;
                          $totalPackagingCost += $packagingCost;    
                        @endphp

                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                        strcasecmp($normal->paymentTarget , "business") == 0 ) 
                          
                          @php
                            $type = "CASH";
                            $totalOnlineMerchant +=  $orderCost; 
                            $totalCommOnlineMerchant  +=  $commission; 
                            $totalDeliveryOnlineMerchant +=   $deliveryFee; 
                            $dueOnMerchant = $deliveryFee  + $commission;  
                          @endphp

                        @else

                            @php
                              $type = "CASH";
                              $totalOnlinebookTou +=  $orderCost; 
                              $totalDeliveryOnlinebookTou +=   $deliveryFee;
                              $totalCommOnlinebookTou += $commission;
                              $dueOnMerchant =  0.00;  
                              $totalPayable +=  $orderCost;
                            @endphp 

                        @endif   

                    @else
                          
                          @php
                            $type = "CASH"; 
                            $commission = $commissionPc =   0  ;
                             $orderCost = $normal->orderCost;
                              $deliveryFee = $normal->deliveryCharge; 
                              $packagingCost = $normal->packingCost; 
                            $totalOrderCost += $orderCost;
                            $totalPackagingCost += $packagingCost;     

                            $totalCashbookTou +=  $orderCost; 
                            $totalDeliveryCashbookTou +=   $deliveryFee;
                            $totalCommCashbookTou +=  $commission;  
                            $dueOnMerchant =  0.00; 
                            $totalPayable +=  $orderCost;
                          @endphp 
                        
                    @endif
 
 
                  @endif

                 
                    @php
                      $type = "CASH"; 
                     @endphp 
                  
                @endif   

      
             @php
                  $totalDueOnMerchant += $dueOnMerchant;  
                  $totalCommission += $commission  ; 
                  $totalDeliveryFee += $deliveryFee  ; 
              @endphp
           @else
               @php
                  $orderCost = 0.00;  
                  $commission = 0.00;  
                  $deliveryFee = 0.00;  
              @endphp 
           @endif  
 
 
   <div class="row"> 
  <div class='col-md-7'>
        <div class="card">
         <div class="card-body"> 
          <div class='row'>
            <div class='col-md-10'>
              <h4>Credit Voucher Entry for Order # {{  $orderno }} of date {{ date('d-m-Y', strtotime($normal->book_date)) }}</h4> 
            </div>
            <div class='col-md-2'>
              <div class="btn-group" role="group" aria-label="Basic example">
                <a type="button" class="btn btn-secondary" 
                href="{{ URL::to('/admin/accounting/prepare-voucher-entry?o=') }}{{ $orderno - 1 }}">
                  <i class='fa fa-chevron-left'></i>
                </a> 
                <a type="button" class="btn btn-secondary" 
                href="{{ URL::to('/admin/accounting/prepare-voucher-entry?o=') }}{{ $orderno + 1 }}">
                  <i class='fa fa-chevron-right'></i>
                </a>
              </div>
            </div>
          </div>
            <hr/>
  <form action="{{ action('Admin\AccountsAndBillingController@updateVoucherEntry') }}" method="post"> 
    {{ csrf_field() }} 

<div class="form-group row">
    <label  class="col-sm-1 col-form-label col-form-label-sm text-right">Type</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm" readonly 
      value="{{ $type }}"  >
    </div>

    <label  class="col-sm-1 col-form-label col-form-label-sm text-right">Amt</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm  text-right" readonly 
      value="{{  number_format( $orderCost + $deliveryFee, 2 )  }}"  >
    </div>

    <label  class="col-sm-1 col-form-label col-form-label-sm  text-right">Comm</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm text-right" readonly 
      value="{{  number_format( $commission, 2 )   }}"  >
    </div>


     <label  class="col-sm-1 col-form-label col-form-label-sm text-right">Dlvry</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm  text-right" readonly 
      value="{{  number_format( $deliveryFee, 2 )  }}"  >
    </div> 
  </div>  
  <hr/> 
  @php 

    $totalcash =$orderCost + $deliveryFee;
    $merchantliability = $orderCost - $commission ;
    $comm_revenue = $commission - ( 0.18 * $commission); 
    $delivery_revenue = $deliveryFee - ( 0.18 * $deliveryFee); 
    $output_cgst = $output_sgst =  ( ( 0.18 * $commission) + ( 0.18 * $deliveryFee) ) / 2; 

  @endphp
      <table class="table" max-width='100%;'>
      <thead>
       <tr> 
          <th   >Account Title</th>  
          <th  >Account Type</th>  
          <th class='text-center'>Dr.</th>
          <th class='text-center'>Cr.</th>   
        </tr>  
      </thead>
      <tbody>

        <tr> 
          <td  width='220px;'> 
            <input readonly type="text" class="form-control"  value='Cash A/c'>
          </td>   
         <td>
            Asset
          </td>  
          <td  class='text-center' width='150px;'>
            <input  type="text" class="form-control form-control-sm" name="tbcasset" value="{{ $totalcash }}">
          </td>  
          <td  class='text-center' width='150px;'>
          </td>   
        </tr>  

        <tr> 
          <td  width='220px;'>
            <input readonly type="text" class="form-control"   value='Merchant A/c'>
          </td>   
         <td>
            Liablity
          </td>  
          <td  class='text-center' width='150px;'>
            
          </td>  
          <td  class='text-center' width='150px;'>
            <input  type="text" class="form-control"  name="tbmerlbt" value="{{ $merchantliability  }}">
          </td>   
        </tr> 

        <tr> 
          <td  width='220px;'>
            <input readonly type="text" class="form-control"  value='Commission Income A/c'>
          </td>   
         <td>
            Revenue
          </td>  
         <td  class='text-center' width='150px;'>
            
          </td>  
          <td  class='text-center' width='150px;'>
            <input  type="text" class="form-control"  name="tbcommin"  value="{{  $comm_revenue  }}">
          </td>   
        </tr> 
         <tr> 
          <td  width='220px;'>
            <input readonly type="text" class="form-control"   value='Delivery Charge A/c'>
          </td>   
         <td>
            Revenue
          </td>  
         <td  class='text-center' width='150px;'> 
          </td>  
          <td  class='text-center' width='150px;'>
            <input  type="text" class="form-control"  name="tbdelin"    value="{{  $delivery_revenue }}">
          </td>   
        </tr> 


         <tr> 
         <td  width='220px;'>
            <input readonly type="text" class="form-control"    value='Output CGST A/c'>
          </td>   
         <td>
            Liability
          </td>  
          <td>
            
          </td>  
          <td> 
            <input  type="text" class="form-control"  name="tbcgstlbt"  value="{{ $output_cgst }}">
          </td>   
        </tr>

        <tr> 
          <td  width='220px;'>
            <input readonly type="text" class="form-control"   value='Output SGST A/c'  >
          </td>   
         <td>
            Liability
          </td>  
          <td>
            
          </td>  
          <td> 
            <input  type="text" class="form-control"  name="tbsgstlbt" value="{{  $output_sgst    }}">
          </td>   
        </tr> 
        <tr> 
          <td> 
          </td>   
         <td>  
          </td>  
          <td> 
          </td>  
          <td> 
            <input  type="hidden"  name="o" value="{{  $normal->id    }}"> 
             <button type="submit" class="btn btn-primary" name="btnsave" value='save'>Save Voucher</button>
          </td>   
        </tr>  
      </tbody>
    </table>

    
    </form>

    </div>
    </div>
  </div>

 <div class='col-md-5'>
  <div class='card'>
      <div class='card-body'>
         <h4>Order Jounal # {{  $orderno }}</h4>  

         <table class='table'>
          <tr>
              <th></th>
              <th>Particulars</th> 
          </tr>
      @php 
         $actitle =  "na";
      @endphp
      @foreach($balance_sheet as $item)
 
        @foreach($account_types as $actype)
          @if($actype->key_name  ==   $item->ac_key_name)
              @php 
                $actitle =  $actype->ac_account;
                @endphp
              @break
          @endif  
        @endforeach

            @if( $item->rule == "dr")
              <tr> 
              <td>Dr</td>
              <td> 
                  <strong>{{ $actitle }}</strong><br/>
                  <i>Current Balance: {{ $item->debit }} <strong>Dr</strong></i>
              </td>
             
            </tr> 
            @else
              <tr> 
              <td>Cr</td>
              <td> 
                  <strong>{{ $actitle }}</strong><br/>
                  <i>Current Balance: {{ $item->credit }} <strong>Cr</strong></i>
              </td>
             
              
            </tr> 
            @endif
            
      @endforeach
      </table>

      </div>
  </div>

 </div>
 </div> 

  </div> 
 
 

 
@endsection



@section("script")

<script>
 
  

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

