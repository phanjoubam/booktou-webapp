@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>


  <div class="row">
     

     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-7">
                    <div class="title">
                      bookTou Merchant Wise Sales Summary Report for  
                      <?php $date_selected =  date('Y-m-d',strtotime($data['today']));?>
                    <span class="badge badge-info">{{$date_selected}}</span> 
                   <?php  $frno =   $data['fr_nos'];?>

                    @if($data['fr_nos']=="0")
                   <span class="badge badge-warning">bookTou-HQ</span>
                    @else

                    @foreach($data['franchise'] as $items) 
         
                    @if($data['fr_nos'] == $items->frno)
            
                    <span class="badge badge-warning">bookTou-{{$items->franchiseName}}</span>
        
                    @endif

                    @endforeach

                    @endif
                    

                    

                    </div> 
                </div>
     <div class="col-md-5"> 
 <form class="form-inline" method="get" action="{{ action('Admin\AdminDashboardController@dailyDueMerchantwise') }}"   >
              {{  csrf_field() }}
  <div class="form-row">

    <div class="col">
        
           <input name='reportDate' class='form-control form-control-sm calendar'>
    </div>
    
    <div class="col"> 
      <select  class="form-control form-control-sm" name='year' >
        <?php 
        for($i= date('Y') ; $i >= 2020; $i--)
        { 
        ?> 
        <option value='{{ $i }}'>{{ $i }}</option> 
        <?php 
        }
      ?>
      </select> 
    </div>
    <div class="col">
      <select name="frno" class="form-control">
        <option value="0">HQ</option>
        @foreach($data['fr_list'] as $franchiseList)
        <option value="{{$franchiseList->frno}}">{{$franchiseList->zone}}</option>
        @endforeach
   </select>
    </div>
    <div class="col">
      <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
    </div>
  </div>
</form>
   </div>
  

       </div>
                </div>
            </div>
       <div class="card-body">   
<div class="table-responsive">
    <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
      <!-- <th>Bin</th> -->
      <th scope="col" class="col-2 text-left">Business</th>
      <th scope="col" class="text-center">Total Sales</th>
  		<th scope="col" class="text-center">Delivery Commission</th>  
  		<th scope="col" class="text-center">PnD Order Sales</th>  
      <th scope="col" class="text-center">PnD Delivery Commission</th>   
      <th scope="col" class="text-center">Total bookTou Earning</th>   
      <th scope="col" class="text-center">Action</th>
		</tr> 
	</thead> 
		<tbody>

			 <?php
        $pndOrder = $normalOrder = $assistOrder = $sales_commission = $pnd_commission = 0;
        $totalSale =  $totalDelivery  =  $totalPnd  =  $totalPnDCommission  = 0.0;
        $overallTotal = 0.0; 
        $monthPos = 0;
        $year =  date('Y');
       ?> 
       


      @foreach($data['allincome'] as $item) 
        
       <!--  @php
        $pndOrder = $normalOrder = $assistOrder = $sales_commission = $pnd_commission = 0;
        @endphp -->

       
       <tr> 
        <!-- <td>{{$item->bin}}</td> -->
        <td class="text-left" >
          <h7>
        {{$item->businessName}}

        </h7> 

      </td>
      
    		  <td class="text-right">
            <?php

              if ($item->orderType=="normal") {
                 $pndOrder=0;
                 $normalOrder = $item->totalSale;
                 $sales_commission = $item->deliveryCommission;
                 $pnd_commission=0;
                  
              }elseif ($item->orderType=="pnd") {
                 $pndOrder = $item->totalSale;
                 $normalOrder=0;
                 $pnd_commission = $item->deliveryCommission;
                 $sales_commission=0;
                  
              }else{

                 $pndOrder = $item->totalSale;;
                 $normalOrder=0;
                 $pnd_commission = $item->deliveryCommission;
                 $sales_commission=0;
                  
              }

        $totalSale +=   $normalOrder;
        $totalDelivery +=  $sales_commission;
        $totalPnd  +=  $pndOrder;
        $totalPnDCommission  += $pnd_commission;
        $overallTotal += $sales_commission+$pnd_commission; 
              ?>

          {{$normalOrder}}
           ₹</td>
    		  <td class="text-right"><span class='badge badge-success'>{{$sales_commission}} ₹</span></td>
         <td class="text-right"> {{$pndOrder}} ₹</td>
          <td class="text-right"><span class='badge badge-success'> {{$pnd_commission}} ₹</span></td>
          <td class="text-right">
            <span class='badge badge-success'>
              {{$pnd_commission + $sales_commission}} ₹
            </span></td>
          <td class="text-right">
            <a href="{{URL::to('/admin/report/daily-sales-and-service-merchantwise')}}?bin={{$item->bin}}&=&reportDate={{$date_selected}}&frno={{$frno}}" 
            target='_blank' class='btn btn-primary btn-xs'>More...</a>
          </td>
    		 </tr>



     @endforeach
 
	 <tr> 
         <!--  <td></td> -->
           
          <td class="text-center" >Total</td>
          <td class="text-right">{{ $totalSale  }} ₹</td>
          <td class="text-right"><span class='badge badge-success'>{{ $totalDelivery  }} ₹</span></td>
         <td class="text-right">{{ $totalPnd  }} ₹</td>
          <td class="text-right"><span class='badge badge-success'>{{ $totalPnDCommission  }} ₹</span></td>
          <td class="text-right"><span class='badge badge-success'>{{ $overallTotal  }} ₹</span></td>
          <td class="text-right">
              
          </td>

         </tr>
	</tbody>
		</table>
	  
		
	</div>
 </div>


  </div> 
	
	</div> 

  



</div>

  

 
@endsection
 

@section("script") 

<script> 

  
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
 
 
</script> 


@endsection

