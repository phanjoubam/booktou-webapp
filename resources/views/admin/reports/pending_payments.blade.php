@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-6">
                  <div class="title">
                    Pending Payments for <span class='badge badge-primary'>{{ $month }}, {{ $year }}</span> 

                  </div> 
                </div>

<div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-4">  
        <form class="form-inline" method="get" action="{{ action('Admin\AdminDashboardController@pendingPayments') }}"   >
              {{  csrf_field() }}
      
      <div class="form-row">
    <div class="col-md-12">
        <select name='month' class="form-control form-control-sm  ">
          <option value='1'>January</option>
          <option value='2'>February</option>
          <option value='3'>March</option>
          <option value='4'>April</option>
          <option value='5'>May</option>
          <option value='6'>June</option>
          <option value='7'>July</option>
          <option value='8'>August</option>
          <option value='9'>September</option>
          <option value='10'>October</option>
          <option value='11'>November</option>
          <option value='12'>December</option>

        </select>
       <select name='year' class="form-control form-control-sm  ">
          <?php 
          for($i=2021; $i >= 2020; $i--)
          {
            ?> 
            <option value='{{ $i }}'>{{ $i }}</option>
          <?php 
        }
        ?>

        </select>
   
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>

       </div>
                </div>
            </div>
       <div class="card-body">   
<div class="table-responsive"> 
    <table class="table">
      <thead>
        <tr>
          <th>BIN</th>
          <th>Business</th> 
          <th class='text-right'>Pending Amount</th> 
          <th class='text-right'>Action</th>  
        </tr>
      </thead>
      
      <tbody>
      
          @foreach ($result as $item)
          <tr>
            <td>{{ $item->id  }}</td>
            <td>{{ $item->name }} <span class='badge badge-primary'>{{ $item->category }}</span>

            <td class='text-right'>
             @if( $item->totalDue >= 10000 )
              <span class='badge badge-danger'>{{ $item->totalDue }}</span> 
             @elseif(  $item->totalDue >= 5000  )
              <span class='badge badge-warning'>{{ $item->totalDue }}</span>
             @else 
              <span class='badge badge-info'>{{ $item->totalDue }}</span>
             @endif
            </td> 
            <td class='text-right'>
              
              <div class="dropdown">

                       <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a> 
 
                      <ul class="dropdown-menu dropdown-user pull-right"> 
                          <li><a class="dropdown-item btn btn-link" 
                            href="{{ URL::to('/admin/business/sales-and-clearance-report')}}?bin={{$item->id}}">View Details</a>
                          </li> 
                            
                        
                           </ul>
                      </div>
            </td>
          </tr>
        @endforeach
       

      </tbody>

    </table>
		
	</div>
 </div>


  </div> 
	
	</div> 
</div>
 

 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

