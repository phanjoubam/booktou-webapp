@extends('layouts.admin_theme_hollow')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  


 	$normal = $order_info; 
?>

 

  <div class="row">
     <div class="col-md-10 offset-md-1"> 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">
                 <div class="col-md-7">
                  <h2 >
                    Order Jounal No. <span class='badge badge-primary'>{{  $orderno }}</span>  
                  </h2> 
                </div>   
                <div class="col-md-5 text-right">
                   Date:  {{  date('d-m-Y', strtotime( $normal->service_date)) }}<br/>
                   {{  date('l', strtotime( $normal->service_date)) }}

                </div> 


       </div>
                </div>
            </div>
       <div class="card-body" style='overflow-x: none;'>    
    <table class="table" max-width='100%;'>
      <thead>
       <tr> 
          <th>Particulars</th>   
          <th>Debit</th>
          <th>Credit</th>   
        </tr>  
      </thead>
      
      <tbody>
        @php  

          $orderCost = $packagingCost = $commission =   $deliveryFee =  $commissionPc = $dueOnMerchant = 0.00; 
          $totalOrderCost  = $totalPackagingCost = $totalDueOnMerchant  = $totalCommission = $totalDeliveryFee = 0.00;  
          $totalCashMerchant = $totalOnlineMerchant =     0.00; 
          $totalCashbookTou = $totalOnlinebookTou = 0.00;
          $totalCommCashbookTou =  $totalCommOnlinebookTou =   0.00;
          $totalDeliveryCashbookTou = $totalDeliveryOnlinebookTou =  0.00;  
          $totalCommCashMerchant = $totalDeliveryCashMerchant = 0.00;
          $totalCommOnlineMerchant= $totalDeliveryOnlineMerchant =     0.00;
          $totalPayable= 0.00; 
          
        @endphp
        @php 
        	$business = null;
        @endphp 

        @php
        	$tempPackagingCost = 0.00;
        @endphp

          @if($normal->bookingStatus == "delivered")
            <tr>
              @if($normal->orderType == "normal")

                @foreach($cart_items as $cart)
                    @if($normal->id == $cart->order_no) 
                      @php 
                        $tempPackagingCost += $cart->qty * $cart->package_charge;
                      @endphp 
                    @endif
                @endforeach

                @php 
                  $normal->packingCost  = $tempPackagingCost;
                @endphp  

                	@if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0 )
                      @php 
                        $commissionPc = ($business == null) ? 0 : $business->commission;
                        $orderCost = $normal->orderCost;
                        $deliveryFee = $normal->deliveryCharge; 
                        $packagingCost = $normal->packingCost; 
                        $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                        $totalOrderCost += $orderCost;
                        $totalPackagingCost += $packagingCost;   
                      @endphp
                    @else 
                      @php 
                        $commissionPc = ($business == null) ? 0 : $business->commission;
                        $orderCost = $normal->orderCost;
                        $deliveryFee = 30; 
                        $packagingCost = $normal->packingCost; 
                        $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                        $totalOrderCost += $orderCost;
                        $totalPackagingCost += $packagingCost;   
                      @endphp
                    @endif 
 

                   @if( strcasecmp($normal->paymentType , "online") == 0 || 
                        strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                    
                    @php 
                      $totalOnlinebookTou +=  $orderCost; //packing cost already included 
                      $totalDeliveryOnlinebookTou +=   $deliveryFee;
                      $totalCommOnlinebookTou +=  $commission;  
                      $dueOnMerchant = $deliveryFee  + $commission;   
                      $totalPayable +=  $orderCost;
                    @endphp  

                   @else 

                    @php 
                      $totalCashbookTou +=  $orderCost  ;  //packing cost already included
                      $totalDeliveryCashbookTou +=   $deliveryFee;
                      $totalCommCashbookTou +=  $commission;   
                      $dueOnMerchant =  0.00;  
                      $totalPayable +=  $orderCost;
                    @endphp 
                  @endif 
                  <td>
                 	Dr Cash<br/>
                 	Current Balance
                 </td>
                  <td class='text-right'>{{ number_format( $orderCost, 2 )   }}</td>  
                  <td class='text-right'>{{ number_format( $deliveryFee, 2 )  }}</td>  
                  <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 


                @elseif($normal->orderType == "pnd")
  
                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                      @php 
                        $type = "CASH";
                        $commissionPc = ($business == null) ? 0 : $business->commission;  
                      @endphp 
                      
                      @if( strcasecmp($normal->paymentType , "online") == 0  )   

                          @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                            strcasecmp($normal->paymentTarget , "business") == 0 )  

                            @php  
                              $type = "CASH";
                              $orderCost = 0;
                              $deliveryFee = 30; 
                              $packagingCost = 0; 
                              $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;    
                            @endphp

                            @php
                              $totalOnlineMerchant +=  $orderCost; 
                              $totalCommOnlineMerchant  +=  $commission; 
                              $totalDeliveryOnlineMerchant +=   $deliveryFee;  
                              $dueOnMerchant = $deliveryFee  + $commission;   
                            @endphp 
                          @else 
                            @php  
                              $type = "CASH";
                              $orderCost = $normal->orderCost;
                              $deliveryFee = $normal->deliveryCharge; 
                              $packagingCost = $normal->packingCost; 
                              $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;    
                            @endphp

                              @php
                                $totalOnlinebookTou +=  $orderCost; 
                                $totalDeliveryOnlinebookTou +=   $deliveryFee;
                                $totalCommOnlinebookTou += $commission;
                                $dueOnMerchant =  0.00; 
                                $totalPayable +=  $orderCost;
                              @endphp  
                          @endif   

                      @else

                          @php  
                            $type = "CASH";
                            $orderCost = 0;
                            $deliveryFee = 30; 
                            $packagingCost = 0; 
                            $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                            $totalOrderCost += $orderCost;
                            $totalPackagingCost += $packagingCost;    
                          
                            $totalCashbookTou +=  $orderCost; 
                            $totalDeliveryCashbookTou +=   $deliveryFee;
                            $totalCommCashbookTou +=  $commission;  
                            $dueOnMerchant =  0.00; 
                            $totalPayable +=  $orderCost;
                          @endphp 

                      @endif

                 
                  @else 
  
                      @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        

                          @if(  strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                                strcasecmp($normal->paymentTarget , "business") == 0 )   
                            @php
                              $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = 0 ;
                              $deliveryFee = 30; 
                              $packagingCost = 0; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;

                              $totalOnlineMerchant +=  $orderCost; 
                              $totalCommOnlineMerchant  +=  $commission; 
                              $totalDeliveryOnlineMerchant +=   $deliveryFee;  
                              $dueOnMerchant = $deliveryFee  + $commission;   
                            @endphp  
                          @else 
                            @php
                               $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = 0 ;
                              $deliveryFee = 30; 
                              $packagingCost = 0; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;   


                              $totalOnlinebookTou +=  $orderCost; 
                              $totalDeliveryOnlinebookTou +=   $deliveryFee;
                              $totalCommOnlinebookTou += $commission; 
                              $dueOnMerchant =  0.00; 
                              $totalPayable +=  $orderCost;
                            @endphp  
                          @endif  
                       @else

                            @php
                              $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = 0;
                              $deliveryFee =  30; 
                              $packagingCost = 0; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;   
                              $totalCashbookTou +=  $orderCost; 
                              $totalDeliveryCashbookTou +=   $deliveryFee;
                              $totalCommCashbookTou +=  $commission;
                              $dueOnMerchant =  0.00; 
                              $totalPayable +=  $orderCost;
                            @endphp     
                        @endif  

                  @endif
 
                 <td>
                 	Dr Cash<br/>
                 	Current Balance
                 </td>
                  <td class='text-right'>{{ number_format($orderCost + $deliveryFee,2)  }}</td>  
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td>  
                   <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 

                @elseif($normal->orderType == "assist")
 
                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0) 
                      
                    @if( strcasecmp($normal->paymentType , "online") == 0  )  
                        @php
                          $commission = $commissionPc =   0  ;
                          $orderCost = $normal->orderCost;
                          $deliveryFee = $normal->deliveryCharge; 
                          $packagingCost = $normal->packingCost;  
                          $totalOrderCost += $orderCost;
                          $totalPackagingCost += $packagingCost;    
                        @endphp

                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                        strcasecmp($normal->paymentTarget , "business") == 0 ) 
                          
                          @php
                            $totalOnlineMerchant +=  $orderCost; 
                            $totalCommOnlineMerchant  +=  $commission; 
                            $totalDeliveryOnlineMerchant +=   $deliveryFee; 
                            $dueOnMerchant = $deliveryFee  + $commission;  
                          @endphp

                        @else

                            @php
                              $totalOnlinebookTou +=  $orderCost; 
                              $totalDeliveryOnlinebookTou +=   $deliveryFee;
                              $totalCommOnlinebookTou += $commission;
                              $dueOnMerchant =  0.00;  
                              $totalPayable +=  $orderCost;
                            @endphp 

                        @endif   

                    @else
                          
                          @php
                            $commission = $commissionPc =   0  ;
                            $orderCost = 0;
                            $deliveryFee = 30; 
                            $packagingCost = 0;  
                            $totalOrderCost += $orderCost;
                            $totalPackagingCost += $packagingCost;     

                            $totalCashbookTou +=  $orderCost; 
                            $totalDeliveryCashbookTou +=   $deliveryFee;
                            $totalCommCashbookTou +=  $commission;  
                            $dueOnMerchant =  0.00; 
                            $totalPayable +=  $orderCost;
                          @endphp 
                        
                    @endif 
                    
                  @endif
                 <td>
                 	Dr Cash<br/>
                 	Current Balance
                 </td>
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>   
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td>  
                  <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 


                @endif   

              </tr>
             @php
                  $totalDueOnMerchant += $dueOnMerchant;  
                  $totalCommission += $commission  ; 
                  $totalDeliveryFee += $deliveryFee  ; 
              @endphp
           @else

            <tr> 
              <td>
                 	Dr Cash<br/>
                 	Current Balance
              </td>
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td>  
              <td class='text-right'>0.00</td>  
          </tr>
        @endif 
     
         
        <tr>
        	<td></td>
             <td class='text-right'>0.00</td>  
              <td class='text-right'>0.00</td> 
        </tr> 
<tfoot>
     
</tfoot>

      </tbody> 
    </table> 
 

 </div> 
  </div> 
  
  </div> 
</div>
 </div> 
 

 

 
@endsection



@section("script")

<script>
 

</script>  


@endsection

