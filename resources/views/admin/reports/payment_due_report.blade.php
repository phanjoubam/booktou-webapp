@extends('layouts.admin_theme_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>


  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header"> 
                 <div class="row">

                   <div class="col-md-6">
                  <div class="title">
                    Unpaid Dues for merchants till <span class='badge badge-primary'>{{ $endDay }}-{{ $month }}-{{ $year }}</span> 

                  </div> 
                </div>

<div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-4">  
        <form class="form-inline" method="get" action="{{ action('Admin\AccountsController@viewPaymentDueMerchants') }}"   >
              {{  csrf_field() }}
      
      <div class="form-row">
    <div class="col-md-12">
        <select name='month' class="form-control form-control-sm  ">
          <option value='1'>January</option>
          <option value='2'>February</option>
          <option value='3'>March</option>
          <option value='4'>April</option>
          <option value='5'>May</option>
          <option value='6'>June</option>
          <option value='7'>July</option>
          <option value='8'>August</option>
          <option value='9'>September</option>
          <option value='10'>October</option>
          <option value='11'>November</option>
          <option value='12'>December</option>

        </select>
       <select name='year' class="form-control form-control-sm  ">
          <?php 
          for($i=2021; $i >= 2020; $i--)
          {
            ?> 
            <option value='{{ $i }}'>{{ $i }}</option>
          <?php 
        }
        ?>

        </select>
   
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>

       </div>
            
            </div>
       <div class="card-body">    
    <table class="table">
      <thead>
        <tr>
          <th>BIN</th>
          <th>Business</th> 
          <th class='text-right'>PnD Amount</th> 
          <th class='text-right'>Normal Sales Amount</th> 
          <th class='text-right'>Sub-Total</th> 
          <th class='text-right'>Action</th>  
        </tr>
      </thead>
      
      <tbody>
      @php
        $grossTotal =0.00;
      @endphp
          @foreach ($businesses as $item)
          <tr>
            <td>{{ $item->id  }}</td>
            <td>{{ $item->name }} 

              @php
                  $totalPnd =0.00;
              @endphp

              @foreach ($pnd_sales as $pnd)
                @if($pnd->request_by == $item->id )
                  @php
                    $totalPnd += $pnd->totalAmount;
                  @endphp
                  @break
                @endif 
              @endforeach

             <td class='text-right'>{{  $totalPnd }}</td> 

              @php
                  $totalNormal =0.00;
              @endphp

              @foreach ($normal_sales as $normal)
                @if($normal->bin == $item->id )
                  @php
                    $totalNormal += $normal->totalAmount;
                  @endphp
                  @break
                @endif 
              @endforeach


             <td class='text-right'>{{ $totalNormal }}</td>  
    

            @php
              $totalAmount = $totalPnd + $totalNormal; 
              $grossTotal +=$totalAmount;
            @endphp

          <td class='text-right'>{{ $totalAmount }}</td>  

           
            <td class='text-right'>
              
              <div class="dropdown">

                       <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a> 
 
                      <ul class="dropdown-menu dropdown-user pull-right"> 
                          <li><a class="dropdown-item btn btn-link" target='_blank'
                            href="{{ URL::to('/admin/business/sales-and-clearance-report')}}?bin={{$item->id}}">View Details</a>
                          </li> 
                            
                        
                           </ul>
                      </div>
            </td>
          </tr>
        @endforeach  
        <tr>
          <th colspan='4' class='text-right'>Total:</th>
          <th class='text-right'>{{ $grossTotal }}</th>  
          <th></th>
        </tr> 
      </tbody>

    </table>
 
 </div>


  </div> 
	
	</div> 
</div> 
 
@endsection



@section("script")

<script>

$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

