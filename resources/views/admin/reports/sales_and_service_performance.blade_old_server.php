@extends('layouts.admin_theme_02')
@section('content')


<div class=" row ">
    <div class="col-md-8">
<div class="card">
    <div class="card-body">
  <h2>Performance Report for {{ date('F, Y') }}</h2>
</div></div>
</div>

<div class="col-md-4">
<div class="card">
    <div class="card-body"><h2>Total Sales: {{$sales}}</h2></div></div>
</div>
 
 </div>
 

<div class="row ">
   
 <div class="col-md-4"> 
    <div class="card mt-3">
    	<div class="card-header text-center">
    	<h3 class="card-title">Total Sales Volume</h3>
    </div>
<canvas id="doughnut_chart"></canvas> 
	
</div> 

</div>

 <div class="col-md-4"> 
<div class="card mt-3">
<div class="card-header text-center"><h3 class="text-center">Revenue Report</h3></div>	
<canvas id="revenue_chart"></canvas>
</div>
</div>


 <div class="col-md-4"> 
    <div class="card mt-3">
    <div class="card-header text-center"><h3 class="text-center">Graph/History</h3></div>	
    <canvas id="graph_chart"></canvas>
    </div> 
</div>



<div class="col-md-12">
<div class="card mt-3">
    <div class="card-body">
        <div class="card-header text-center">
<h3>
    <span class="">Product List</span></h3>
</div>  
<table class="table table-striped table-hover">
    <thead>
        
        <th>Product Name</th>
        <th>Qnty.</th>
        <th>CGST</th>
        <th>SGST</th>
        <th>Amount</th>

    </thead>
 
@php 
$total = 0;
$unit=0;
$cgst=0;
$sgst=0;
@endphp
@foreach ($serv_list as $result)
 
 @php
 $totalUnitPrice = sprintf("%.2f", $result->actualPrice);
 @endphp

<tr>

<td>{{    $result->pr_name  }}</td>
<td>{{$result->quantity}}</td>
<td>{{$result->cGST}}</td> 
<td>{{$result->sGST}}</td> 
<td>{{$totalUnitPrice}}</td>

</tr>
@php 

$cgst += $result->cGST;
$sgst += $result->sGST;
$total += $result->actualPrice;
@endphp
@endforeach

<tr>
<td></td>
<td></td>
<td></td>
<td><span class="badge badge-primary">Total amount: </span></td>
<td><span class="badge badge-secondary">Rs. {{$total + $cgst + $sgst}}</span></td> 
</tr>

</table>


</div>
</div>

</div>


</div>

 
@endsection

@section("script")

<script>
 
	
	var ctx = document.getElementById('revenue_chart').getContext('2d');
	var chart = new Chart(ctx, {
	// The type of chart we want to create
    type: 'line',
    // The data for our dataset
    data: {

    	labels: <?php echo json_encode($revmonth); ?>,
        datasets: [
    	{
            label: 'Amount',
            backgroundColor: '#ec1c305e',
            borderColor: 'rgb(255,10,13,255)',
            data: <?php echo json_encode($revdata); ?>,
            
            
        }

        ]
       

    },
    // Configuration options go here
    options: {}
});


	var ctx2 = document.getElementById('graph_chart').getContext('2d');
	
	var chart = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: <?php echo json_encode($revmonth); ?>,
        datasets: [{
            label: 'Sales history',
            backgroundColor: 'rgb(209, 236, 241)',
            borderColor: 'rgb(129, 181, 230)',
            data: <?php echo json_encode($revdata); ?>
        }]
    },

    // Configuration options go here
    options: {}
});

	


	var ctx4 = document.getElementById('doughnut_chart').getContext('2d');
	var chart = new Chart(ctx4, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: ['Total sales'],
        datasets: [{
            label: 'Total Sales Volume',
            backgroundColor:['rgb(112, 250, 112)'],
            borderColor: 'rgb(252, 186, 3)',
            data: [<?php echo json_encode($total);?>]
        }]
    },

    // Configuration options go here
    options: {}
});

</script> 

@endsection 