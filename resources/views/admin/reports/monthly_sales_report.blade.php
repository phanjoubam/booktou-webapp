@extends('layouts.admin_theme_hollow')
@section('content')

<?php 

$total_bill_amount=0;
$total_delivery_charges=0;
$total_amount_from_customer=0;
$total_gst_tcs = 0;
$total_income_tax_tds = 0;

$total_payable_to_merchant = 0;
$total_booktou_income = 0;
$total_output_cgst = 0;
$total_output_sgst = 0;
$total_gross_revenue_for_booktou = 0;
$annual_income=0;
$tax= 500000;

$commission = 0.00;
$total_commission=0;
$non_gst_enrolled = 'no';
$commission = 0; 
?>

<style type="text/css">
  
   
</style>

<div class="row">
     <div class="col-md-12"> 
     	
      <!-- card section starts from here -->

      <div class="card card-default">
           <div class="card-header sticky-top">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-4">
                  <div class="title">
                  <h5>Sales Report for <span class="badge bg-warning">{{ $month  }}, {{ $year }}
                  </span> </h5>
                   
                  </div> 
                </div>
                <div class="col-md-2">
                  
                   
                   <a href="{{url('/admin/accounting/export-merchant-taxes')}}" class="btn btn-primary"
                   target="_blank">Export to excel</a>
   
                </div>

                <div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-4">  
        <form class="form-inline" method="get" 
        action="{{ action('Admin\AdminDashboardController@monthlySalesReportExporter') }}"   >
              {{  csrf_field() }}
      
      <div class="form-row">
    <div class="col-md-12">
        <select name='month' class="form-control form-control-sm  ">
          <option value='1'>January</option>
          <option value='2'>February</option>
          <option value='3'>March</option>
          <option value='4'>April</option>
          <option value='5'>May</option>
          <option value='6'>June</option>
          <option value='7'>July</option>
          <option value='8'>August</option>
          <option value='9'>September</option>
          <option value='10'>October</option>
          <option value='11'>November</option>
          <option value='12'>December</option> 
        </select>
       <select name='year' class="form-control form-control-sm  ">
          <?php 
          for($i=2021; $i >= 2020; $i--)
          {
            ?> 
            <option value='{{ $i }}'>{{ $i }}</option>
          <?php 
          }
          ?>

        </select>
   
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>

       </div>
<br> 
 </div>
</div>


<!-- record display section starts from here -->
<div class="card-body">
  <div class="table-responsive" >
<table class="table table-striped " >
 

<thead>
  <tr>
  <th>Order No</th>
  <th class='text-center'>Order Date</th>
  <th class='text-center'>Order Type</th>
  <th>Order Status</th>
  <th>Merchant</th>
  <th>Supply</th>
  <th>Constitution</th>
  <th>PAN</th>
  <th>GST</th>
  <th>A(Taxable value)</th>
  <th>B(CGST)</th>
  <th>C(SGST)</th>
  <th>D(Total Bill Value)</th>
  <th>E(Delivery Charges)</th>
  <th>F(Amount Received from customer)</th>
  <th>Payment Mode</th>
  <th>Date of Receipt</th>
  <th>(G)GST TCS @1% (A*1%) </th>
  <th>(H)Income tax TDS @1% (A*1%) </th>
  <th>(I) Commission from Merchant</th>
  <th>(J) Payable to Merchant(D-G-H-I)</th>
  <th>Date of Payment</th>
  <th>(K) Booktou Income ((E+I)/(1+18%))</th>
  <th>(L) Output CGST(I*9%)</th> 
  <th>(M) Output SGST(I*9%)</th>
  <th>(N) Gross Revenue for Booktou (K+L+M) or (E+S)</th>
  <th>Customer GSTN, if any</th>
</tr>
</thead>

<tbody>

 
  @foreach($all_orders as $order)
      @php
        $found = false; 
        $bin = 0;
        $sellAmount = 0.00;
        $deliveryCharge = 0.00;

        $amountReceive = 0.0;
        $payMode ="---";
        $date = "---";
        $tcs = 0.0;
        $tds = 0.0;
        $sellerPayable = 0.00;
        $payDate = "---";
        $hasRecord = false ;
        $non_gst_enrolled = 'no';
        $gstin = ""; 
      @endphp

    <tr>
      <td>{{$order->id}}</td>
      <td>{{ date( 'd-m-Y', strtotime(  $order->entry_date) )}}</td> 
      
      @if($order->type == "normal")
        <td class='text-center'> 
          <span class="badge badge-info">{{$order->type}}</span>
        </td> 
          
        @foreach($normal_orders as $normal_order)  
          @if($normal_order->id == $order->id) 
            @php 
              $found = true; 
              $status = $normal_order->book_status;
              $bin = $normal_order->bin;

              $sellAmount = $normal_order->seller_payable;
              $deliveryCharge = $normal_order->delivery_charge ; 

              $amountReceive = $sellAmount + $deliveryCharge;
              $payMode = $normal_order->payment_type ; 
              $date = date('d-m-Y', strtotime($normal_order->book_date)) ;
             
              $hasRecord = true; 
            @endphp

            @break 
          @endif


        @endforeach
    

        @if( !$found ) 
          @php
              $status = "cancelled";
          @endphp 
        @endif

        <td>
              @if($status == "canceled"  || $status == "cancelled") 
                  <span class="badge badge-danger">Cancelled</span> 
              @else
                  <span class="badge badge-primary">{{  $status  }}</span> 
              @endif 
        </td>
 

      @elseif($order->type == "pnd")
        <td class='text-center'>
          <span class="badge badge-primary">{{$order->type}}</span>
        </td>  

        @foreach($pnd_orders as $pnd_order)
          @if($pnd_order->id == $order->id)
            <td>
              @if( $pnd_order->book_status  == "canceled"  || $pnd_order->book_status == "cancelled") 
                  <span class="badge badge-danger">Cancelled</span> 
              @else
                  <span class="badge badge-primary">{{  $pnd_order->book_status  }}</span> 
              @endif
            </td> 
            

              @if($pnd_order->pay_mode == "online")
                 @php
                    $sellAmount = $pnd_order->total_amount;
                    $deliveryCharge = $pnd_order->service_fee ; 
                 @endphp
              @else
                 @php
                  $sellAmount = 0.00;
                  $deliveryCharge = 20.00 ;

                 @endphp
              @endif 

              @php 
                $bin = $pnd_order->request_by;  
                $amountReceive = $sellAmount + $deliveryCharge ;
                $payMode = $pnd_order->pay_mode ; 
                $date = date('d-m-Y', strtotime($pnd_order->book_date)) ;
              
                $hasRecord = true; 
              @endphp


            @break
          @endif
        @endforeach

      @elseif($order->type == "assist")
        <td class='text-center'>
             <span class="badge badge-warning">{{$order->type}}</span> 
        </td>   

        @foreach($assist_orders as $assist_order)
          @if($assist_order->id == $order->id)
            <td>
              @if( $assist_order->book_status  == "canceled"  || $assist_order->book_status == "cancelled") 
                  <span class="badge badge-danger">Cancelled</span> 
              @else
                  <span class="badge badge-primary">{{  $assist_order->book_status  }}</span> 
              @endif
            </td>
     

              @if($assist_order->pay_mode == "online")
                 @php
                    $sellAmount = $assist_order->total_amount;
                    $deliveryCharge = $assist_order->service_fee ;
                 @endphp
              @else
                 @php
                    $sellAmount = 0.00;
                    $deliveryCharge = 10.00 ; 
                 @endphp
              @endif

              @php  
                $bin = $pnd_order->request_by; 
                $amountReceive = $sellAmount + $deliveryCharge ;
                $payMode = $assist_order->pay_mode ; 
                $date = date('d-m-Y', strtotime($assist_order->book_date)) ;  
                $hasRecord = true; 
              @endphp 

            @break

          @endif

        @endforeach

          
      @else

          @php

            $hasRecord = false ; 

          @endphp

         
      @endif
  

      @if($hasRecord)

        <!-- rest of the columns -->
        <!-- business section --> 
        @foreach($businesses as $business)
          @if($business->id ==  $bin )
            <td>{{  $business->name }}</td>  
            <td>{{  $business->category }}</td> 
            <td>Not Provided</td> 
            <td>{{  $business->pan_no == "" ? "Not Provided" : $business->pan_no  }}</td> 
            <td>{{  $business->gstin == "" ? "Not Provided" : $business->gstin }}</td> 

            @php
              $non_gst_enrolled =  $business->non_gst_enrolled;
              $gstin =  $business->gstin;

              $commission=  $business->commission;

            @endphp 
          @endif
        @endforeach

        <td>{{  $sellAmount  }}</td> 
        <td>0.00</td> 
        <td>0.00</td>  
        <td>{{  $sellAmount  }}</td>
        <td>{{  $deliveryCharge  }}</td>  
        <td>{{  $amountReceive  }}</td> 
        <td>{{  $payMode  }}</td> 
        <td>{{  $date  }}</td>


        @if($non_gst_enrolled == "no" && $gstin != "" )
          <td>{{  $sellAmount/100 * 1 }}</td> 

        @else
          <td>{{ 0.00 }}</td>  
        @endif

          <td>{{ 0.00 }}</td>  
          <td>{{ $commission / 100 * $sellAmount  }}</td>  



      @else 
        <td>---</td>
        <td>---</td> 
        <td>---</td>
        <td>---</td>
        <td>---</td>   
        <td>---</td>
        <td>---</td> 
        <td>---</td>
        <td>---</td>
        <td>---</td> 
        <td>---</td> 
        <td>---</td> 
        <td>---</td> 
        <td>---</td> 
 
      @endif  
  
  

    </tr>
 
@endforeach


<!-- pay section ends here -->


</tbody>

 </table>
 <br> 

</div>
     </div>

<!-- card section ends here -->



</div>
<!-- record display section ends here -->
</div>
</div>
@endsection

@section("script")


@endsection