@extends('layouts.admin_theme_hollow')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
 
?>

 

  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-7">
                  <h2 >
                    Daily Sales Report for <span class='badge badge-primary'>{{  $monthname }}</span>  
                  </h2> 
                </div>

<div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-3">  
        <form class="form-inline" method="get" action="{{ action('Admin\AccountsAndBillingController@monthlyTaxableSalesReport') }}"   > 
        <div class="form-row">
          <div class="col-md-12">
              <select name='month' class="form-control form-control-sm  ">
               
                <option value='6'>June</option>
                <option value='7'>July</option>
                <option value='8'>August</option>
                <option value='9'>September</option>
                <option value='10'>October</option>
                <option value='11'>November</option>
                <option value='12'>December</option> 
              </select>
             <select name='year' class="form-control form-control-sm  ">
                <?php 
                for($i=2021; $i >= 2020; $i--)
                {
                  ?> 
                  <option value='{{ $i }}'>{{ $i }}</option>
                <?php 
                }
                ?>

              </select>
         
              <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
          </div>

        </div>
  
      </form>
      </div>

       </div>
                </div>
            </div>
       <div class="card-body" style='overflow-x: scroll;'>    
    <table class="table" max-width='960px;'>
      <thead>
       <tr>
          <th>Date</th> 
          <th>Type | Source | Status | Mode | Target</th>  
           <td>Order Journal</td>
           <td>Voucher No.</td>
          <th class='text-right'>Sale Amount</th>  
          <th class='text-right'>Comission</th>
          <th class='text-right'>Delivery Fee</th>  
          <th class='text-right'>booTou Earning</th> 
          <th class='text-right'>Action</th> 
        </tr>  
      </thead>
      
      <tbody>
        @php  

          $orderCost = $packagingCost = $commission =   $deliveryFee =  $commissionPc = $dueOnMerchant = 0.00; 
          $totalOrderCost  = $totalPackagingCost = $totalDueOnMerchant  = $totalCommission = $totalDeliveryFee = 0.00;  
          $totalCashMerchant = $totalOnlineMerchant =     0.00; 
          $totalCashbookTou = $totalOnlinebookTou = 0.00;
          $totalCommCashbookTou =  $totalCommOnlinebookTou =   0.00;
          $totalDeliveryCashbookTou = $totalDeliveryOnlinebookTou =  0.00;  
          $totalCommCashMerchant = $totalDeliveryCashMerchant = 0.00;
          $totalCommOnlineMerchant= $totalDeliveryOnlineMerchant =     0.00;
          $totalPayable= 0.00;

        @endphp
          @foreach ($sales as $normal)
 
 

             @php 
              $business = null;
            @endphp
            @foreach($businesses as $bitem)

                @if( $normal->bin == $bitem->id )
                  @php 
                    $business = $bitem;
                  @endphp
                  @break
                @endif
 
          @endforeach

          @php
            $tempPackagingCost = 0.00;
          @endphp

          @if($normal->bookingStatus == "delivered")
            <tr>
              @if($normal->orderType == "normal")

                @foreach($cart_items as $cart)
                    @if($normal->id == $cart->order_no) 
                      @php 
                        $tempPackagingCost += $cart->qty * $cart->package_charge;
                      @endphp 
                    @endif
                @endforeach

                @php 
                  $normal->packingCost  = $tempPackagingCost;
                @endphp


                <td>{{ date('d-m-Y', strtotime($normal->book_date)) }}</td>  

                <!--
                <td class='text-center'>
                  <span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span> - 
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0 )
                      <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                      <span class='badge badge-primary'>CASH</span> - 
                    @endif
                    <span class='badge badge-success'>bookTou</span>
                </td> 
              -->

                <td class='text-left'>  
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0 )
                     CashFree India Pvt Ltd.
                    @else 
                      CASH 
                    @endif 
                </td>
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0 )
                      @php 
                        $commissionPc = ($business == null) ? 0 : $business->commission;
                        $orderCost = $normal->orderCost;
                        $deliveryFee = $normal->deliveryCharge; 
                        $packagingCost = $normal->packingCost; 
                        $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                        $totalOrderCost += $orderCost;
                        $totalPackagingCost += $packagingCost;   
                      @endphp
                    @else 
                      @php 
                        $commissionPc = ($business == null) ? 0 : $business->commission;
                        $orderCost = $normal->orderCost;
                        $deliveryFee = 30; 
                        $packagingCost = $normal->packingCost; 
                        $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                        $totalOrderCost += $orderCost;
                        $totalPackagingCost += $packagingCost;   
                      @endphp
                    @endif 
 

                   @if( strcasecmp($normal->paymentType , "online") == 0 || 
                        strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                    
                    @php 
                      $totalOnlinebookTou +=  $orderCost; //packing cost already included 
                      $totalDeliveryOnlinebookTou +=   $deliveryFee;
                      $totalCommOnlinebookTou +=  $commission;  
                      $dueOnMerchant = $deliveryFee  + $commission;   
                      $totalPayable +=  $orderCost;
                    @endphp  

                   @else 

                    @php 
                      $totalCashbookTou +=  $orderCost  ;  //packing cost already included
                      $totalDeliveryCashbookTou +=   $deliveryFee;
                      $totalCommCashbookTou +=  $commission;   
                      $dueOnMerchant =  0.00;  
                      $totalPayable +=  $orderCost;
                    @endphp

                  @endif
                  <td>Order Journal</td>
                  <td>{{ $normal->id }}</td>
                  <td class='text-right'>{{ number_format( $orderCost, 2 )   }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format( $deliveryFee, 2 )  }}</td>  
                  <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 
                  <td class='text-right'>
                    <a class='btn btn-primary' href="{{  URL::to('/admin/accounting/prepare-voucher-entry') }}?o={{  $normal->id }}" target='_blank'>
                      Prepare Voucher
                    </a>
                  </td> 

                @elseif($normal->orderType == "pnd")

                  <td>{{ date('d-m-Y', strtotime($normal->book_date)) }}</td>  
                <!--
                  <td class='text-center'><span class='badge badge-primary'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>
                      {{  ($normal->source=="business" || $normal->source=="merchant" ) ? "Merchant" : "Customer" }}
                    </span> - <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 

                  @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                    <span class='badge badge-info'>ONLINE</span> - 
                  @else 
                    <span class='badge badge-primary'>CASH</span> - 
                  @endif
                   
                  @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                     <span class='badge badge-danger'>Merchant</span>
                  @else 
                    <span class='badge badge-success'>bookTou</span>
                  @endif
                </td>  
                --> 

                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                      @php 
                        $type = "CASH";
                        $commissionPc = ($business == null) ? 0 : $business->commission;  
                      @endphp 
                      
                      @if( strcasecmp($normal->paymentType , "online") == 0  )   

                          @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                            strcasecmp($normal->paymentTarget , "business") == 0 )  

                            @php  
                              $type = "CASH";
                              $orderCost = 0;
                              $deliveryFee = 30; 
                              $packagingCost = 0; 
                              $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;    
                            @endphp

                            @php
                              $totalOnlineMerchant +=  $orderCost; 
                              $totalCommOnlineMerchant  +=  $commission; 
                              $totalDeliveryOnlineMerchant +=   $deliveryFee;  
                              $dueOnMerchant = $deliveryFee  + $commission;   
                            @endphp 
                          @else 
                            @php  
                              $type = "CASH";
                              $orderCost = $normal->orderCost;
                              $deliveryFee = $normal->deliveryCharge; 
                              $packagingCost = $normal->packingCost; 
                              $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;    
                            @endphp

                              @php
                                $totalOnlinebookTou +=  $orderCost; 
                                $totalDeliveryOnlinebookTou +=   $deliveryFee;
                                $totalCommOnlinebookTou += $commission;
                                $dueOnMerchant =  0.00; 
                                $totalPayable +=  $orderCost;
                              @endphp  
                          @endif   

                      @else

                          @php  
                            $type = "CASH";
                            $orderCost = 0;
                            $deliveryFee = 30; 
                            $packagingCost = 0; 
                            $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                            $totalOrderCost += $orderCost;
                            $totalPackagingCost += $packagingCost;    
                          
                            $totalCashbookTou +=  $orderCost; 
                            $totalDeliveryCashbookTou +=   $deliveryFee;
                            $totalCommCashbookTou +=  $commission;  
                            $dueOnMerchant =  0.00; 
                            $totalPayable +=  $orderCost;
                          @endphp 

                      @endif

                 
                  @else 
  
                      @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        

                          @if(  strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                                strcasecmp($normal->paymentTarget , "business") == 0 )   
                            @php
                              $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = 0 ;
                              $deliveryFee = 30; 
                              $packagingCost = 0; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;

                              $totalOnlineMerchant +=  $orderCost; 
                              $totalCommOnlineMerchant  +=  $commission; 
                              $totalDeliveryOnlineMerchant +=   $deliveryFee;  
                              $dueOnMerchant = $deliveryFee  + $commission;   
                            @endphp  
                          @else 
                            @php
                               $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = 0 ;
                              $deliveryFee = 30; 
                              $packagingCost = 0; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;   


                              $totalOnlinebookTou +=  $orderCost; 
                              $totalDeliveryOnlinebookTou +=   $deliveryFee;
                              $totalCommOnlinebookTou += $commission; 
                              $dueOnMerchant =  0.00; 
                              $totalPayable +=  $orderCost;
                            @endphp  
                          @endif  
                       @else

                            @php
                              $type = "CASH";
                              $commission = $commissionPc =   0  ;
                              $orderCost = 0;
                              $deliveryFee =  30; 
                              $packagingCost = 0; 
                              $totalOrderCost += $orderCost;
                              $totalPackagingCost += $packagingCost;   
                              $totalCashbookTou +=  $orderCost; 
                              $totalDeliveryCashbookTou +=   $deliveryFee;
                              $totalCommCashbookTou +=  $commission;
                              $dueOnMerchant =  0.00; 
                              $totalPayable +=  $orderCost;
                            @endphp     
                        @endif  

                  @endif

                  <td class='text-left'>{{ $type }}</td> 
                  <td>Order Journal</td>
                  <td>{{ $normal->id }}</td>
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td>  
                  <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 
                  <td class='text-right'>
                    <a class='btn btn-primary' href="{{  URL::to('/admin/accounting/prepare-voucher-entry') }}?o={{  $normal->id }}" target='_blank'>
                      Prepare Voucher
                    </a>
                  </td> 

                @elseif($normal->orderType == "assist")

                 <td>{{ date('d-m-Y', strtotime($normal->book_date)) }}</td>  
                 <!--
                   <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                      <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span> - <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> -  
                    
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                    <span class='badge badge-info'>ONLINE</span> - 
                    @else
                    <span class='badge badge-primary'>CASH</span> - 
                    @endif
                    
                    
                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                    <span class='badge badge-danger'>Merchant</span> 
                    @else 
                    <span class='badge badge-success'>bookTou</span>
                    @endif
                    </td> 
                  -->


                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0) 
                      
                    @if( strcasecmp($normal->paymentType , "online") == 0  )  
                        @php
                          $commission = $commissionPc =   0  ;
                          $orderCost = $normal->orderCost;
                          $deliveryFee = $normal->deliveryCharge; 
                          $packagingCost = $normal->packingCost;  
                          $totalOrderCost += $orderCost;
                          $totalPackagingCost += $packagingCost;    
                        @endphp

                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                        strcasecmp($normal->paymentTarget , "business") == 0 ) 
                          
                          @php
                            $totalOnlineMerchant +=  $orderCost; 
                            $totalCommOnlineMerchant  +=  $commission; 
                            $totalDeliveryOnlineMerchant +=   $deliveryFee; 
                            $dueOnMerchant = $deliveryFee  + $commission;  
                          @endphp

                        @else

                            @php
                              $totalOnlinebookTou +=  $orderCost; 
                              $totalDeliveryOnlinebookTou +=   $deliveryFee;
                              $totalCommOnlinebookTou += $commission;
                              $dueOnMerchant =  0.00;  
                              $totalPayable +=  $orderCost;
                            @endphp 

                        @endif   

                    @else
                          
                          @php
                            $commission = $commissionPc =   0  ;
                            $orderCost = 0;
                            $deliveryFee = 30; 
                            $packagingCost = 0;  
                            $totalOrderCost += $orderCost;
                            $totalPackagingCost += $packagingCost;     

                            $totalCashbookTou +=  $orderCost; 
                            $totalDeliveryCashbookTou +=   $deliveryFee;
                            $totalCommCashbookTou +=  $commission;  
                            $dueOnMerchant =  0.00; 
                            $totalPayable +=  $orderCost;
                          @endphp 
                        
                    @endif
 

                  @else 
                    @continue
                  @endif

                  <td>
                   @if( strcasecmp($normal->paymentType , "ONLINE") == 0)  
                      CASH Customer</span>
                    @else 
                      CASH
                    @endif 
                  </td> 
                  <td>Order Journal</td>
                  <td>{{ $normal->id }}</td>
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td>  
                  <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 
                  <td class='text-right'>
                    <a class='btn btn-primary' href="{{  URL::to('/admin/accounting/prepare-voucher-entry') }}?o={{  $normal->id }}" target='_blank'>
                      Prepare Voucher
                    </a>
                  </td> 

                @endif   

              </tr>
             @php
                  $totalDueOnMerchant += $dueOnMerchant;  
                  $totalCommission += $commission  ; 
                  $totalDeliveryFee += $deliveryFee  ; 
              @endphp
           @else

            <tr> 
              <td>{{ date('d-m-Y', strtotime($normal->book_date)) }}</td>  
              <td><span style='color:red'>No transaction</span></td> 
               <td>Order Journal</td>
               <td>{{ $normal->id }}</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td>  
              <td class='text-right'>0.00</td>  
              <td></td>
          </tr>

  

           @endif
  
        @endforeach
     
        <tr>
          <th>Date</th> 
          <th>Type | Source | Status | Mode | Target</th>  
           <td>Order Journal</td>
           <td>Voucher No.</td>
          <th class='text-right'>Sale Amount</th>  
          <th class='text-right'>Comission</th>
          <th class='text-right'>Delivery Fee</th>  
          <th class='text-right'>booTou Earning</th> 
        </tr>  
        <tr>
            <th colspan='4' class='text-right'>Sum Totals</th> 
            <th class='text-right'>A = {{ number_format( $totalOrderCost  ,2) }}</th> 
            <th class='text-right'>B = {{ number_format( $totalCommission,2) }}</th>
            <th class='text-right'>C = {{ number_format($totalDeliveryFee, 2) }}</th> 
            <th class='text-right'>E = {{ number_format($totalCommission + $totalDeliveryFee,2) }}</th>  
        </tr> 
<tfoot>
     
</tfoot>

      </tbody> 
    </table> 

      <hr/>
<form>
   

<div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Cash Collected:</label>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm is-valid" readonly 
      value="{{ number_format( $totalCashbookTou + $totalDeliveryCashbookTou  , 2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Online Collected:</label>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm is-valid" readonly 
      value="{{ number_format( $totalOnlinebookTou  + $totalDeliveryOnlinebookTou,2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Due towards Merchant:</label>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm" readonly 
      value="{{ number_format( $totalDueOnMerchant ,2, '.', '') }}"  >
    </div>


     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Total Collected:</label>
    <div class="col-sm-1">
      <input type="email" class="form-control form-control-sm is-valid" readonly 
      value="{{ number_format(  $totalCashbookTou + $totalDeliveryCashbookTou  + $totalOnlinebookTou  + $totalDeliveryOnlinebookTou  ,2, '.', '') }}"  >
    </div>
 
  </div> 
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Payable to Merchants:</label>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm is-valid" readonly 
      value="{{ number_format( $totalPayable  , 2, '.', '') }}"  >
    </div>

  </div>
</form>

 </div> 
  </div> 
  
  </div> 
</div>
 </div> 
 
 

 
@endsection



@section("script")

<script>
 
  

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

