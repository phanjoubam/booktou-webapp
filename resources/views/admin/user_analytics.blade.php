@extends('layouts.admin_theme_02')
@section('content')
 
 

   <div class="row">
     <div class="col-md-12">
 
     <div class="card panel-default">
           <div class="card-header">
              <div class="card-title">
            User Analytics
                </div>
            </div>
       <div class="card-body">   
      
              <div class=" " > 
                 <div class="table-responsive">
                  <table class="table"  >
                    <thead class=" text-primary">  
                  <tr class="text-center"  >  
                     <th scope="col">Customer</th>
                     <th scope="col">Address</th>
                      <th scope="col">City</th>
                      <th scope="col">Phone</th>
                     <th  style='width: 250px;'>Active Page</th>  
                  </tr>
                </thead> 
                     <tbody>
                <?php 

                  $i=1;
  
                foreach ($data['results'] as $item)
                {  
                    $categories = array();
                ?>
                  @foreach($data['visitlogs'] as $citem)
                    @if($citem->member_id == $item->id )
                      @php 
                        $categories[] = $citem->category;  
                      @endphp
                    @endif
                  @endforeach

                  @php
                    $categories=  array_unique($categories);
                  @endphp

                  <tr>
                      <td>{{  $item->fullname }}</td>
                      <td>{{  $item->locality }}</td>
                      <td>{{  $item->city }}</td>
                      <td>{{  $item->phone }}</td>
                      <td style='width: 250px;'><?php echo "<span class='badge badge-info'>" . implode("</span> <span class='badge badge-info'>", $categories ) . "</span>" ; ?></td>
                  </tr>
                    <?php
                          $i++; 
                       }

                          ?>
                </tbody>
                  </table>
             </div>
              </div>
 </div>
 
            </div>
          </div>
        </div>  
  
 
 
@endsection

@section("script")

<script>
 

</script> 

@endsection 