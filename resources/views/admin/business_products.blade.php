@extends('layouts.admin_theme_01')
@section('content')

<div class="panel-header panel-header-sm"> 

</div> 



<div class="cardbodyy margin-top-bg"> 

 

  <div class="col-md-12">

 <div class="card">

     <div class="card-header"> 
         <div class="row">
   <div class="col-md-6">
    <h3>Item Details</h3>
   </div>
    <div class="col-md-6 text-right">
      <form class="form-inline" method="post" action="{{  action('Admin\AdminDashboardController@searchBusinessProducts') }}">
            {{ csrf_field() }}
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Item Category:</label> 
            <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='search_key'>
              @foreach ($data['category'] as $item)
                    <option value='{{$item->category_name}}'>{{$item->category_name}}</option> 
              @endforeach
            </select>
              
                  <input type="hidden" value="{{ $data['bin'] }}" name="busi_id">
                <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
          </form> 

    </div>
  </div>    
     </div> 
        
   <div class="card-body"> 
 <table class="table"> 

  <thead class=" text-primary">
    <tr class="text-center">
    <th >Item Image</th>
    <th >Item Name</th>
    <th >Initial Stock</th>
    <th >Stock in Hand</th>
    <th >Max Order</th>
    <th >Unit Name</th>
    <th >Unit Price</th>
    <th >Action</th>
    </tr>
    </thead>

    <tbody>

    @foreach ($data['items'] as $itemP)

     <tr class="text-center">
     <td>
      <?php 


      $first_photo  = URL::to("/public/assets/image/no-image.jpg");

      $all_photos = array(); 
      $files =  explode(",",  $itemP->photos ); 

      if(count($files) > 0 )
      {
          $files=array_filter($files);
          $folder_path =  "/public/assets/image/store/bin_" .   $itemP->bin . "/pr_" . $itemP->id   .   "/";
          foreach($files as $file )
          {
            if(file_exists( base_path() .  $folder_path .   $file ))
            {
              $first_photo =  URL::to( $folder_path  ) . "/" .    $file  ;
              break;
            } 
          } 
      } 
           
      ?> 

      <img src="{{ $first_photo   }}" alt="..." height="80px" width="100px"></td>
     <td>{{$itemP->pr_name}}</td>
      <td>{{$itemP->initial_stock}}</td>
      <td>{{$itemP->stock_inhand}}</td>
      <td>{{$itemP->max_order}}</td>
      <td>{{$itemP->unit_name}}</td>
      <td>{{$itemP->unit_price}}</td>

      <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                           <a class="dropdown-item" href="">Edit/Update</a> 
                            <a class="dropdown-item" href="">Delete</a> 
                               
                        </div>
                      </div>
                  
        </td>
 
    </tr>
    @endforeach
   </tbody>

  </table>


  </div>

   
</div>    
  
   </div> 
 </div>


     

@endsection

 
  
 