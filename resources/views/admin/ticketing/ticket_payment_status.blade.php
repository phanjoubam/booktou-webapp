@extends('layouts.admin_theme_02')
@section('content')

<div class="col-md-12 "> 

  <div class="card">
    <div class="card-header">
      <div class="row">
       <div class="col-md-4"> <h5>Ticket Purchase Report</h5></div>
       <div class="col-md-2"> </div>
       <div class="col-md-6">
        <form method="get" action=""> 

          <div class="row">
           <div class="col-md-4">
            <input type="text" class="form-control" name="custphone"  placeholder="phone number or ticket no"  >
          </div>
          
          <div class="col-md-2">
            <button type="submit" class="btn btn-primary btn-sm" value='search' name="btn_search">Search</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>  
<div class="card-body">

  <div class="row">

   @if (session('err_msg')) <div class="col-12">
     <div class="alert alert-info">
      {{ session('err_msg') }}
    </div>
  </div>
  @endif 
  <div class="col-md-12 ">   
    <table class="table table-bordered  "> 
      <thead>
      <tr>
        <th>Order No.</th>
        <th>Customer</th> 
        <th>Amount</th>
        <th class='text-center'>Portal Status</th>
        <th class='text-center'>Action</th>
      </tr>
    </thead> 
    <tbody>
       @foreach($data['ticket_payment'] as $info)
     <tr>
       <td>
        {{$info->id}}
      </td>
      <td>{{$info->visitor_name}}<br>
       <strong></strong> <br/>
       <i class='fa fa-phone mt-2'></i>
       {{$info->phone}}
     </td>
     <td>
       {{$info->total_amount}}
     </td> 
     <td class='text-center'>
      <span class='badge badge-success'>{{$info->payment_status}}</span>
    </td>
    <td class='text-center'>
                            <a data-toggle="modal" href="#myModal"
                            data-key="{{$info->id}}" class="btn btn-outline-danger btn-sm showdetails" target='_blank' style="border-radius:8px;color: #e83e8c;border-color: #e83e8c;background: white;">More</a>
                         </td>
  </tr>
  @endforeach
</tbody>
</table>
<hr/>
@php
$params = array('status' => $data['status']);
@endphp
{{$data['ticket_payment']->appends($params)->links()}}
</div>
<div id="myModal" class="modal fade in modalrem">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="purchase-id">
                  
                  <div class="modal-footer">
                   <input type='hidden' name='orderno' id='key'/> 
                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 </div> 
            </div>
        </div>
    </div>

<div class="col-md-12"> 
@if(count($data['ticket_payment'])<=0)
  <div class="alert alert-info">

    <p>No purchase record found for the selected date!</p>
  </div>
  @endif

</div>

</div>
</div>

</div>

@endsection

@section("script")
<script> 
 
  $(document).on("click", ".showdetails", function()
{
  $("#key").val($(this).attr("data-key"));

  $(".modalrem").modal("show")


  var purchase_id = $(this).attr("data-key");

$("#purchase-id").html('');
                $.ajax({
                    url: "{{url('api/admin/ticket/ticket-by-purchase-id')}}",
                    type: "POST",
                    data: {
                        purchase_id: purchase_id,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {
                        $.each(result.tickets, function (key, value) {
                            $("#purchase-id").append('<tr><td>'+ value.purchase_id +'</td><td>'+ value.ticket_no +'<td class="text-center">'+ '<img src="'+ value.qrcode_url +'" style="height: 150px; width:150px; margin:15px">'  +'</td></tr><hr>');
                         });
                    }
                });
}); 



  
  
</script>
@endsection