<?php
if( strcasecmp( request()->query('theme'), 'new') == 0)
{

  $theme = "layouts.admin_theme_03";  
}
else 
{
  $theme = "layouts.admin_theme_02";   
}


?> 
@extends( $theme )
@section('content')



<?php 

$totalOrders=0;
$target = 20; 
$paa_orders = 0;
$paa_completed_orders =0 ;

$online_not_pass = 0;
$online_pass = 0;
$online_temp_exit = 0;
$online_exit = 0;
$ofl_pass=0;
$ofl_not_pass=0;
$ofl_tem_exit=0;
$ofl_exit=0;
$free_not_pass=0;
$free_pass=0;
$paid_ticket=$data['paid_ticket'];
$unpaid_ticket=$data['unpaid_ticket'];
foreach($data['sold_tickts'] as $item)
{
  if($item->current_status == 0 && $item->ticket_format == "ONL")
  {
    $online_not_pass++;
  }

  if ($item->current_status == 1 && $item->ticket_format == "ONL")
  {
    $online_pass++;
  }

  if ($item->current_status == 10 && $item->ticket_format == "ONL")
  {
   $online_temp_exit++;
 }

 if ($item->current_status == 100 && $item->ticket_format == "ONL")
 {
  $online_exit++;
}
if ($item->current_status == 0 && $item->ticket_format == "OFL")
{
  $ofl_not_pass++;
}
if ($item->current_status == 1 && $item->ticket_format == "OFL")
{
  $ofl_pass++;
}

if ($item->current_status == 10 && $item->ticket_format == "OFL")
{
  $ofl_tem_exit++;
}
if ($item->current_status == 100 && $item->ticket_format == "OFL")
{
  $ofl_exit++;
}
if ($item->current_status == 0 && $item->ticket_format == "FREE")
{
  $free_not_pass++;
}
if ($item->current_status == 1 && $item->ticket_format == "FREE")
{
  $free_pass++;
}


}
?>
<div class="row">
     <div class="col-md-12">  
       @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif
    </div>
  </div>


<div class="row my-flex-card mt-4">
  <div class="col-lg-3 col-sm-6 mb-2 col-md-4">
    <div class="card h-100">
      <div class="card-block">
        <div class="card-body easypiechart-panel">
          <h4>Online Paid Ticket</h4>
          <div class="easypiechart" id="easypiechart-orange" >
            <span class="percent h3">
              <a href="{{URL::to('/admin/ticket-count')}}?status=active&type=ONL">
                <span class='badge badge-primary'>{{$online_not_pass}} Active</span> </a>
                <a href="{{URL::to('/admin/ticket-count')}}?status=inside&type=ONL"> 
                  <span class='badge badge-success'>{{$online_pass}} Enter</span> 
                </a>
                <a href="{{URL::to('/admin/ticket-count')}}?status=tempexit&type=ONL">
                  <span class='badge badge-warning'>{{$online_temp_exit}} Temp Exit</span></a>
                  <a href="{{URL::to('/admin/ticket-count')}}?status=exit&type=ONL">
                    <span class='badge badge-danger'>{{$online_exit}} Exit</span></a>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 mb-2 col-md-4">
          <div class="card h-100">
            <div class="card-block">
              <div class="card-body easypiechart-panel">
                <h4>Offline Paid Ticket</h4>
                <div class="easypiechart" id="easypiechart-orange" >

                  <span class="percent h3">
                    <a href="{{URL::to('/admin/ticket-count')}}?status=active&type=OFL">

                      <span class='badge badge-primary'>{{$ofl_not_pass}} Active</span> </a>
                      <a href="{{URL::to('/admin/ticket-count')}}?status=inside&type=OFL">
                       <span class='badge badge-success'>{{$ofl_pass}} Enter</span>
                     </a>

                     <a href="{{URL::to('/admin/ticket-count')}}?status=tempexit&type=OFL">
                      <span class='badge badge-warning'>{{$ofl_tem_exit}} Temp Exit</span></a>
                      <a href="{{URL::to('/admin/ticket-count')}}?status=exit&type=OFL">
                        <span class='badge badge-danger'>{{$ofl_exit}} Exit</span>
                      </a>

                    </span>

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6 mb-2 col-md-4">
            <div class="card h-100">
              <div class="card-block">
                <div class="card-body easypiechart-panel">
                  <h4>Free Pass</h4>
                  <div class="easypiechart" id="easypiechart-orange">

                    <span class="percent h3">
                      <a href="{{URL::to('/admin/ticket-count')}}?status=active&type=FREE">

                        <span class='badge badge-primary'>{{$free_not_pass}} Active</span> </a> 
                        <a href="{{URL::to('/admin/ticket-count')}}?status=inside&type=FREE">
                          <span class='badge badge-success'>{{$free_pass}} Enter</span></a>
                        </span>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <div class="col-lg-3 col-sm-6 mb-2 col-md-4">
            <div class="card h-100">
              <div class="card-block">
                <div class="card-body easypiechart-panel">
                  <h4>Today's Sale</h4>
                  <div class="easypiechart" id="easypiechart-orange">

                    <span class="percent h3">
                      <a href="{{URL::to('/admin/ticket-payment-status')}}?status=paid">

                        <span class='badge badge-primary'>{{$paid_ticket}} Paid</span> </a> 
                        <a href="{{URL::to('/admin/ticket-payment-status')}}?status=unpaid">
                          <span class='badge badge-danger'>{{$unpaid_ticket}} Unpaid</span></a>
                          <a href="{{URL::to('/admin/ticket-scan-filterby-staff')}}">
                          <span class='badge badge-warning'>Scan log</span></a>
                        </span>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">

            </div>
      
            <div class="row mt-2">
              <div class="col-md-12">
                <div class='card'> 
                  <div class='card-body'>
                    @php
                    $slno=1;
                    @endphp
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Ticket Id</th>
                          <th>Ticket no.</th>
                          <th>Name</th>
                          <th>Phone</th>
                          <th style="text-align: center;">Price</th>
                          <th style="text-align: center;">Valid date</th>
                          <th style="text-align: center;">
                            <form class="form-inline"   method="get" action="" enctype="multipart/form-data">
                              {{  csrf_field() }}
                              <div class="form-row">
                                <div class="col-md-12"> 

                                  <input type="text" name ="search_key" class="form-control form-control-sm"  />
                                  <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
                                </div>

                              </div>
                            </form>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data['tickets_filter'] as $filter)
                        <tr>
                          <td>{{$filter->id}}</td>
                          <td>{{$filter->ticket_no}}</td>
                          <td>{{$filter->visitor_name}}</td>
                          <td>{{$filter->visitor_phone}}</td>
                          <td style="text-align: center;">{{$filter->price}}</td>
                          <td style="text-align: center;">{{$filter->valid_date}}</td>
                          <td style="text-align: center;">
                            <button type="button" class="btn btn-outline-danger btn-sm showdetails"  ata-toggle="modal" data-target="#modalepg" 
                            data-key="{{$filter->id}}"
                            data-ticket-no="{{$filter->ticket_no}}"
                            data-name="{{$filter->visitor_name}}"
                            data-phone="{{$filter->visitor_phone}}"
                            data-status="{{$filter->current_status}}"
                            data-qrcode="{{$filter->qrcode_url}}"
                            data-mode="{{$filter->ticket_format}}" style="border-radius:8px;color: #e83e8c;border-color: #e83e8c;background: white;">
                            <a  target='_blank' style="color: #e83e8c;">View details</a>
                          </button>
                        </td>
                      </tr>
                      @endforeach   

                    </tbody>
                  </table>
                  @php
                  $params = array('status' => $data['status'], 'type' => $data['ticket_type']);
                  @endphp
                  {{$data['tickets_filter']->appends($params)->links()}}
                </div>
              </div>
            </div>
          </div>
          <div class="modal modalrem" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                 <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="button" readonly type="text"  class="btn btn-warning btn-rounded"  id="ticket_no" name='ticket_no' >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group text-center">
                      <img src="" id="qr_code" style="height: 150px; width:150px; padding:2px">
                    </div>
                    <div class="form-group">
                      <div class="row mt-3" style="font-size: 1rem;">
                       Status: <span>
                         <input style="width:100;" readonly type="button"  class="btn btn-info btn-rounded"  id="status" name='status'/ >
                       </span>                  
                     </div>
                   </div>
                   <div class="form-group">
                    <div class="row mt-3" style="font-size: 1rem;">
                     Order type: <span>
                       <input style="width:100;" readonly type="button"  class="btn btn-success btn-rounded"  id="type" name='type'/ >
                     </span>                  
                   </div>

                 </div>

               </div>
             </div>
               <div class="form-group">
                <label style="color: black;">Customer Name:</label>
                <input readonly type="text"  class="form-control"  id="input_name" name='input_name' >
              </div>
              <div class="form-group">
                <label style="color: black;">Phone no.</label>
                <input readonly type="text"  class="form-control"  id="phone" name='phone' >
              </div>
            </div>
            <div class="modal-footer">
             <input type='hidden' name='orderno' id='key'/> 
             <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

           </div>
       </div>
     </div>
   </div>
   @endsection
   @section("script")
   <script>
    $(document).ready(function(){
      $(document).on("click", ".showdetails", function()
      {
        var name = $(this).attr("data-name");
        var phone = $(this).attr("data-phone");
        var ticket_no = $(this).attr("data-ticket-no");
        var qr_code = $(this).attr("data-qrcode");
        var status = $(this).attr("data-status");
        var ticket_mode= $(this).attr("data-mode");
        if(status==0)
        {
          status = "Not Enter";
        }
        else if(status==1)
        {
          status = "Inside";
        }
        else if(status==10)
        {
          status = "Temporary exit";
        }
        else
        {
          status = "Exit";
        }



    // code block

        $("#key").val($(this).attr("data-key"));
        $("#ticket_no").val(   ticket_no) ;
        $("#input_name").val(   name) ; 
        $("#phone").val(   phone)  ; 
        $("#status").val(   status) ;
        $("#type").val(   ticket_mode) ;
        $("#qr_code").attr("src",qr_code) ; 
        $(".modalrem").modal("show")

      }); 

    }); 

  </script> 
  @endsection