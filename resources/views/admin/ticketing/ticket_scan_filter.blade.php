@extends('layouts.admin_theme_02')
@section('content')

<div class="col-md-12 "> 

  <div class="card">
    <div class="card-header">
      <div class="row">
       <div class="col-md-4"> <h5>Ticket Scan Report</h5></div>
       <div class="col-md-2"> </div>
       <div class="col-md-6">
        <form method="get" action=""> 

          <div class="row">
           <div class="col-md-4">
            <div class="form-group mb-3">
                        <select  id="staffname-dropdown" class="form-control">
                            <option value=""> Select staff</option>
                            @foreach ($data['profile'] as $staffname)
                            <option value="{{$staffname->id}}">
                                {{$staffname->fullname}}
                            </option>
                            @endforeach
                        </select>
                    </div>
          </div>
          
          <!-- <div class="col-md-2">
            <button type="submit" class="btn btn-primary btn-sm" value='search' name="btn_search">Search</button>
          </div> -->
        </div>
      </form>
    </div>
  </div>
</div>
</div>  
<div  class="card-body">

  <div class="row">

   @if (session('err_msg')) <div class="col-12">
     <div class="alert alert-info">
      {{ session('err_msg') }}
    </div>
  </div>
  @endif 
  <div class="col-md-12 ">   
    <table class="table table-bordered"> 
      <thead>
      <tr>
        <th>Ticket No.</th>
        <th>Visitor Name</th> 
        <th class='text-center'>Purchase Id</th>
        <th class='text-center'>QR</th>
      </tr>
    </thead> 
    <tbody id="scan-detail-dropdown">

</tbody>
</table>
<hr/>
</div>
</div>
</div>

</div>

@endsection

@section("script")
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script> 
   $(document).ready(function () {
   	$('#staffname-dropdown').on('change', function () 
   	{
                var staffid = this.value;
                $("#scan-detail-dropdown").html('');
                $.ajax({
                    url: "{{url('api/admin/ticket/staff-selecting')}}",
                    type: "POST",
                    data: {
                        staff_id: staffid,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {
                        $.each(result.ticket_detail, function (key, value) {
                            $("#scan-detail-dropdown").append('<tr><td>'+ value.ticket_no +'</td><td>'+ value.visitor_name +'<td class="text-center">'+ value.purchase_id +'</td><td class="text-center">'+ '<img src="'+ value.qrcode_url +'" style="height: 150px; width:150px; padding:2px">'  +'</td></tr>');
                        });
                    }
                });
            });

   });
  

   
</script>
@endsection