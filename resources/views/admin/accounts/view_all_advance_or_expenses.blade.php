@extends('layouts.admin_theme_02')
@section('content')

<div class="row"> 

<div class="col-md-12">
 <div class="card">
 	@if(isset($advance)) 

 	<div class="card-header">  
		<h3>Total Advance Expenditure list for month of {{$month}}, {{$year}}</h3>
	</div>

	@else

	<div class="card-header">  
		<h3>Total Fuel Reimbursement list for month of {{$month}}, {{$year}}</h3>
	</div>

	@endif
	
 	<div class="card-body">
@if(isset($advance)) 
	<table class="table  table-bordered">
	<thead>
		<tr> 
			<th>Date</th>
			<th>Expend By</th>
			<th>Category</th>
			<th>Details</th>
			<th>Amount</th> 
		</tr>
	</thead>
	<tbody>
		@php 
			$i = 0;
			$totalAdvance=0.00;
			 
			$cdate = date('Y') . "-" . date('m') . "-01" ;

		@endphp 
		@foreach($advance as $item)
		@php 
			$i++; 
			$cdate = $item->use_date;
		@endphp

		@switch($item->category )
			@case("advance")
				@php 
					$totalAdvance += $item->amount;
				@endphp  
			@break
			  
		@endswitch  

		<tr> 
			<td>{{ date('d-m-Y', strtotime($item->use_date))}}</td>
			<td>
				@if($item->use_by == 0)
				<span class='badge badge-info badge-pill'>Office</span>
				@else
					@foreach($all_staffs as $staff)
						@if($staff->id == $item->use_by)
							<span class='badge badge-warning badge-pill'>{{ $staff->fullname }}</span>

							@break
						@endif
					@endforeach
				@endif</td>
			<td>{{$item->category}}</td>
			<td>{{$item->details}}</td> 
			<td>{{$item->amount}}</td> 
		</tr> 

		@endforeach

		<tr>
			<th colspan="4" class='text-right'>Total Advance</th>
			<th>{{ $totalAdvance }}</th> 
		</tr>	
		 

	</tbody>
</table>

 
@endif


@if(isset($fuelexpense)) 
	<table class="table  table-bordered">
	<thead>
		<tr> 
			<th>Date</th>
			<th>Expend By</th>
			<th>Category</th>
			<th>Details</th>
			<th>Amount</th> 
		</tr>
	</thead>
	<tbody>
		@php 
			$i = 0;
			$totalAdvance=0.00;
			 
			$cdate = date('Y') . "-" . date('m') . "-01" ;

		@endphp 
		@foreach($fuelexpense as $item)
		@php 
			$i++; 
			$cdate = $item->use_date;
			$totalAdvance += $item->amount;
		@endphp

		
					
				  

		<tr> 
			<td>{{ date('d-m-Y', strtotime($item->use_date))}}</td>
			<td>
				@if($item->use_by == 0)
				<span class='badge badge-info badge-pill'>Office</span>
				@else
					@foreach($all_staffs as $staff)
						@if($staff->id == $item->use_by)
							<span class='badge badge-warning badge-pill'>{{ $staff->fullname }}</span>

							@break
						@endif
					@endforeach
				@endif</td>
			<td>{{$item->category}}</td>
			<td>{{$item->details}}</td> 
			<td>{{$item->amount}}</td> 
		</tr> 

		@endforeach

		<tr>
			<th colspan="4" class='text-right'>Total Advance</th>
			<th>{{ $totalAdvance }}</th> 
		</tr>	
		 

	</tbody>
</table>
 
@endif




</div>

</div>

</div>
</div>

@endsection

@section("script")
<script>



</script>
@endsection