@extends('layouts.admin_theme_02')
@section('content')
 
 
 
 <div class="row"> 
  <div class="col-sm-12 col-md-6 ">
     <div class="card card-default">
       <div class="card-header">  
        <h3>Daily Deposit Entry</h3>       
       

    </div>
 <div class="card-body">
          @if (session('err_msg'))
           <div class="form-row">
        <div class="col-md-12"> 
        <div class="alert alert-danger">
      {{ session('err_msg') }}
      </div>
      </div>
    </div>
      @endif
          <form action="{{action('Admin\AdminDashboardController@saveCashDeposit') }}" method="post">
            {{csrf_field()}}
          <small id="alert" class="text-danger"></small>
          

<div class="form-row"> 

        <div class="col-md-6" id="expensesuser">
            <label>Associated Staff:</label>
            <select class="form-control category" name="depositby" id="depositby"> 
            @foreach($data['staffs'] as $staff)
              <option value="{{ $staff->id }}">{{ $staff->fullname }}</option>
            @endforeach 
          </select>   
        </div>  
    </div>



          <div class="form-row">
            
        <div class="col-md-6">

          <label>Select Deposit From:</label>
          
          <select class="form-control  " name="depositfrom" id="depositfrom"> 
            <option value="merchant">Merchant</option>
            <option value="staff">Staff</option>  
          </select> 
       
        </div>

        <div class="col-md-6" id="expensesuser">
            <label>Associated Merchant (if any):</label>
            <select class="form-control category" name="bin" id="bin"> 
              <option value="0">No Business Required</option>
            @foreach($data['businesses'] as $business)
              <option value="{{ $business->id }}">{{ $business->name }}</option>
            @endforeach 
          </select>   
        </div>  
    </div>


    <div class="form-row mt-3">
      <div class="col-md-4" id="expensesuser">
          <label>Date:</label>
            <input type="text" name="depodate" id="depodate" class="form-control calendar">

        </div>
        <div class="col-md-4">
          <label>Amount:</label>
          <input type="number" class="form-control" name="amount" id="amount">
          
</div>
        <div class="col-md-4">

          <label>Deposit Category:</label> 
          <select class="form-control category" name="category" id="category"> 
            <option value="agent tip">Tip for Agent</option> 
            <option value="merchant due payback">Merchant Due Payback</option>
            <option value="franchise due payback">Franchise Due Payback</option> 
          </select>  
        </div>
 

</div>
<div class="form-row">
            
        <div class="col-md-12">
          <label><small>Details:</small></label>
          <textarea rows="7" cols="6" class="form-control"name="details" id="details"></textarea>
           
        </div>
</div>


     

<div class="form-row mt-2">
<div class="col-md-2">
<button class="btn btn-primary btnsaveExpenses" name="btn_save" value="save">Save</button>
</div>
</div>
</form>
</div>






  </div>
</div>
</div>



@endsection

@section("script")
<script>
var siteurl =  "<?php echo config('app.url') ?>";

$('.selectUserExpenses').change(function(){

  
      
   $.ajax({
          
              
              url : siteurl +'/admin/staff/get-booktou-and-staff',
              data: { 
                    'expensesby': this.value 
              },
              type: 'get',
               
              success: function (response) {
                
                  if (response.success) {

                     
                   $('#expensesuser').empty().append(response.html);
                      
                    }
                  else {
                     
                $('#expensesuser').empty();
                  }
              }
            
      });




 });

$('.btnsaveExpenses').click(function()
{

if ($('#expensesby').val()=="") {
 $('#alert').text("please select booktou or staff option to proceed!");
 $('#expensesby').focus();
 return false; 
}

if ($('#use_by').val()=="") {
 $('#alert').text("staff name is mandatory!");
 $('#use_by').focus();
 return false; 
}
if ($('#expensedate').val()=="") {
 $('#alert').text("expense date is mandatory!");
 $('#expensedate').focus();
 return false; 
}

if ($('#details').val()=="") {
 $('#alert').text("expenses details mandatory!");
 $('#details').focus();
 return false; 
}

if ($('#amount').val()=="" || $('#amount').val()=="0") {
 $('#alert').text("please specify expenses amount!");
 $('#amount').focus();
 return false; 
}
});



$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});



</script>
@endsection
