@extends('layouts.admin_theme_hollow')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
 
?>

<style type="text/css">
   
    .h-scroll {
      overflow-y: auto;   
    }
    table {
      border-collapse: collapse;        
      width: 100%;
    }
    th,
    td {
      border: 1px solid #808080;
    }
    
  </style>

<div class="card">
 <div class="p-2"> 
  <div class="row">
    <div class="col-md-3">
      @if(isset($from_date) && isset($upto_date))
      <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
        <button type="button" class="btn btn-success">Date Range</a>
        <button type="button" class="btn btn-info">{{$from_date}}</a>
          <button type="button" class="btn btn-info">{{$upto_date}}</a>

      </div> 
      @else
      <div class="btn-group btn-group-sm"; role="group" aria-label="Basic example">
        <button type="button" class="btn btn-success">Please Select</button> 
        <button type="button" class="btn btn-info">Date Range</button>
         
      </div> 
      @endif
    </div>


    <div class="col-md-9">
      <form class="form-inline float-right" method="post" action="{{  action('Admin\AccountsAndBillingController@searchGstAndItrController') }}"> 
        {{ csrf_field() }}

        
        <label class="my-1 mr-2 btn btn-success" for="inlineFormCustomSelectPref"> From :</label>
        <input type="text" class='form-control form-control-sm border-primary custom-select my-1 mr-sm-2 calendar' value="{{ date('d-m-Y', strtotime($from_date))  }}" name='from_date' />

        <label class="my-1 mr-2 btn btn-success" for="inlineFormCustomSelectPref"> To :</label> 
        <input type="text" class='form-control form-control-sm border-primary custom-select my-1 mr-sm-2 calendar'  
        value="{{ date('d-m-Y', strtotime($upto_date))  }}" name='to_date' />

        <!-- <label class="my-1 mr-2 btn btn-success"  for="inlineFormCustomSelectPref">&nbsp Payment Mode:</label> 
        <select   class='form-control form-control-sm border-primary'   name='paymentMode' >
        <option class="form-control input-lg" value='allMode'>All</option>
        <option class="form-control input-lg" value='online'>Online</option> 
        <option class="form-control input-lg" value='cod'>Cash</option> -->
        
       <!--  </select> -->

        <!-- <label class="my-1 mr-2" for="inlineFormCustomSelectPref">&nbsp &nbsp Order Status:</label> 
        <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='status' >
        <option value='allOrders'>All</option> 
        <option value='new'>New</option> 
        <option value='confirmed'>Confirmed</option>
        <option value='cancelled'>cancelled</option>  
        </select> -->

        <button type="submit" class="btn btn-primary btn-sm my-1" style="margin-left: 25px;" value='search' name='btn_search'>Search  &nbsp<i class='fa fa-search'></i></button>
        <button type="submit" class="btn btn-primary btn-sm my-1" style="margin-left: 25px;" value='export_excel' name='download'>Download  &nbsp<i class='fa fa-download'></i></button>
      </form>
    </div>
  </div> 
 </div>         
</div>


<div class="h-scroll">
@if(isset($orders))
<div class="row mt-2">
    <div class="col-md-12">
    <table class="table">
  <thead>
    <tr  class="bg-primary">
      <th scope="col">Order No</th>
      <th scope="col">Date</th>
      <th scope="col">Order Type</th>
      <th scope="col">Source</th>
      <th scope="col">Status</th>
      <th scope="col">Mode</th>
      <th scope="col">Target</th>
      <th scope="col">Commentary Order( ₹ )</th>
      <th scope="col">Packaging</th>
      <th scope="col">Discount</th>
      <th scope="col">Refund </th>
      <th scope="col">Delivery</th>
      <th scope="col">Comission For</th>
      <th scope="col">Comission</th>
      <th scope="col">Comission Rate</th>
      <th scope="col">Order value</th>
      <th scope="col">Total Earnings</th>
      <th scope="col"> Merchant Pay-out</th>
      <th scope="col">Due on Merchant</th>
      <th scope="col">Date of payment</th>
      <th scope="col">Mode</th>
      <th scope="col">Remarks</th>
    </tr>
  </thead>






  <tbody>
  
  
  	
  	@foreach($orders as $item )

  	@php
  		$com_order = doubleval($item->totalCost) + doubleval($item->delivery_charge);
  		$sale_comission_for = doubleval($com_order) - doubleval($item->packingCharge) - doubleval($item->vendorDiscount) - doubleval($item->refunded);
  		$comission = doubleval($sale_comission_for) * (doubleval($item->comissionRate)/100);
  		$order_value = $com_order - doubleval($item->vendorDiscount) - doubleval($item->bookTouDiscount) - doubleval($item->refunded) + doubleval($item->delivery_charge);
  		$total_earning = doubleval($item->delivery_charge) + $comission - doubleval($item->bookTouDiscount);
  		$merchant_pay_out = $order_value - $total_earning;								

  	@endphp 

    <tr>
      <td scope="row">{{$item->orderNo}}</td>
      <td>{{date("d-m-Y",strtotime($item->orderDate))}}</td>
      <td>{{$item->order_type}}</td>
      <td>{{$item->source}}</td>
      <td>{{$item->status}}</td>
      <td>{{$item->paymentMode}}</td>
      <td>{{$item->target}}</td>
      <td>{{number_format($com_order,2)}}</td>
      @if(strcasecmp($item->status,'canceled') != 0 && strcasecmp($item->status,'cancelled') != 0)
        <td>{{number_format($item->packingCharge,2)}}</td>
        <td>{{number_format($item->bookTouDiscount,2)}}</td>
        <td>{{number_format($item->refunded,2)}}</td>
        <td>{{number_format($item->delivery_charge,2)}}</td>
        <td>{{number_format($sale_comission_for,2)}}</td>
        <td>{{number_format($comission,2)}}</td>
        <td>{{number_format($item->comissionRate,2)}} %</td>
        <td>{{number_format($order_value,2)}}</td>
        <td>{{number_format($total_earning,2)}}</td>
        
        @if(strcasecmp($item->order_type, "pnd" ) == 0 && strcasecmp($item->source, "business" ) == 0 
        && strcasecmp($item->paymentMode, "ONLINE" ) == 0  && strcasecmp($item->target, "merchant" ) == 0 )
        <td>0.00</td>
        @else
        <td>{{number_format($merchant_pay_out,2)}}</td>
        @endif

        
        @if(strcasecmp($item->order_type, "pnd" ) == 0 && strcasecmp($item->source, "business" ) == 0 
        && strcasecmp($item->paymentMode, "ONLINE" ) == 0  && strcasecmp($item->target, "merchant" ) == 0 )
        <td>{{number_format($merchant_pay_out,2)}}</td>
        @else
        <td>0.00</td>
        @endif
        <td>{{"NA"}}</td>
        <td>{{"NA"}}</td>
        <td>{{"NA"}}</td>
      @else
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      @endif
    </tr>
   @endforeach

	
  
  </tbody>
</table>
</div>
</div>
<div class="row">
  <div class="col-md-12 fixTableHead">
    <div class="mt-2">
      {{$orders->appends(Request::except('page'))->links('admin.template-01.pagination')}}
    </div>
  </div>
</div>
</div>
@else
	<div class="row mt-5 mb-5" >
		<h4 class="text-danger" style="margin: auto;" > Please Select a Date Range to to view Order List</43> 	
	</div>
	

   @endif
 
@endsection



@section("script")

<script>
 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY'
    });
});

</script>  

@endsection

