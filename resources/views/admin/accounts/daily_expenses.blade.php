@extends('layouts.admin_theme_02')
@section('content')

 
 
 
 <div class="row"> 
  <div class="col-sm-12 col-md-6 ">
     <div class="card card-default">
       <div class="card-header">  
        <h3>Daily Expenditure Entry</h3>       
       

    </div>
 <div class="card-body">
          @if (session('err_msg'))
           <div class="form-row">
        <div class="col-md-12"> 
        <div class="alert alert-danger">
      {{ session('err_msg') }}
      </div>
      </div>
    </div>
      @endif
          <form action="{{action('Admin\AdminDashboardController@saveDailyExpenses') }}" method="post">
            {{csrf_field()}}
          <small id="alert" class="text-danger"></small>
          <div class="form-row">
            
        <div class="col-md-6">

          <label>Select Expense Done By:</label>
          
          <select class="form-control  " name="expensesby" id="expensesby"> 
            <option value="booktou">bookTou</option>
            <option value="staff">Staff</option>
             
          </select> 
       
        </div>

        <div class="col-md-6" id="expensesuser">
            <label>Associated Staff:</label>
            <select class="form-control category" name="useby" id="useby"> 
              <option value="0">Not Relevant</option>
            @foreach($data['staffs'] as $staff)
              <option value="{{ $staff->id }}">{{ $staff->fullname }}</option>
            @endforeach 
          </select>  

            
        </div>


    </div>
    <div class="form-row">
      <div class="col-md-4" id="expensesuser">
          <label><small>Date:</small></label>
            <input type="text" name="expensedate" id="expensedate" class="form-control calendar">

        </div>
        <div class="col-md-4">
          <label><small>Amount:</small></label>
          <input type="number" class="form-control" name="amount" id="amount">
          
</div>
        <div class="col-md-4">

          <label>Expense Category:</label> 
          <select class="form-control category" name="category" id="category"> 
            <option value="advance">Advance Salary</option>
            <option value="fuel">Fuel Reimbursement</option>
             <option value="snacks">Snacks</option>
            <option value="office">Office Expense</option> 
            <option value="due-cleareance">Merchant Due Clearance</option>
            <option value="other">Others</option>
          </select> 
       
        </div>

   

</div>
<div class="form-row">
            
        <div class="col-md-12">
          <label><small>Details:</small></label>
          <textarea rows="7" cols="6" class="form-control"name="details" id="details"></textarea>
           
        </div>
</div>


     

<div class="form-row mt-2">
<div class="col-md-2">
<button class="btn btn-primary btnsaveExpenses" name="btn_save" value="save">Save</button>
</div>
</div>
</form>
</div>






  </div>
</div>
</div>



@endsection

@section("script")
<script>
var siteurl =  "<?php echo config('app.url') ?>"; 

$('.selectUserExpenses').change(function(){

  
      
   $.ajax({
          
              
              url : siteurl +'/admin/staff/get-booktou-and-staff',
              data: { 
                    'expensesby': this.value 
              },
              type: 'get',
               
              success: function (response) {
                
                  if (response.success) {

                     
                   $('#expensesuser').empty().append(response.html);
                      
                    }
                  else {
                     
                $('#expensesuser').empty();
                  }
              }
            
      });




 });

$('.btnsaveExpenses').click(function()
{

if ($('#expensesby').val()=="") {
 $('#alert').text("please select booktou or staff option to proceed!");
 $('#expensesby').focus();
 return false; 
}

if ($('#use_by').val()=="") {
 $('#alert').text("staff name is mandatory!");
 $('#use_by').focus();
 return false; 
}
if ($('#expensedate').val()=="") {
 $('#alert').text("expense date is mandatory!");
 $('#expensedate').focus();
 return false; 
}

if ($('#details').val()=="") {
 $('#alert').text("expenses details mandatory!");
 $('#details').focus();
 return false; 
}

if ($('#amount').val()=="" || $('#amount').val()=="0") {
 $('#alert').text("please specify expenses amount!");
 $('#amount').focus();
 return false; 
}
});



$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});



</script>
@endsection
