@extends('layouts.admin_theme_02')
@section('content')

<div class="row"> 

<div class="col-md-12">
@if(isset($cash_upi)) 
<div class="row mt-3"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header">   
       	<div class='row'>
       		<div class='col-md-8'>
       			<h3>Cash In UPI for month of {{$month}}, {{$year}}</h3>
       		</div>
       		<div class='col-md-4'>
       			  
       		</div>

       	</div>
 

    </div>
<div class="card-body">  
	
	<table class="table  table-bordered">
	<thead>
		<tr> 
			<th>Order No</th>
			<th>Date</th>  
			<th>Amount</th> 
		</tr>
	</thead>
	<tbody>
		@php 
			$i = 0;
			$totalVerifiedAmount=0.00;
		@endphp 
		@foreach($cash_upi as $item)
		
  

		<tr>
			<td>{{$item->order_no}}</td> 
			<td>{{ date('d-m-Y', strtotime($item->verified_date))}}</td> 
			<td>{{$item->verified_amount}}</td>
			 
		</tr> 
		<?php	$totalVerifiedAmount+=$item->verified_amount; ?>
		@endforeach

		<tr>
			<th colspan="2" class='text-right'>Total Amount in UPI</th>
			<th>{{ $totalVerifiedAmount }}</th> 
		</tr> 

	</tbody>
</table> 

</div>
</div>
</div>
</div> 
@endif

</div>
</div>

@endsection

@section("script")
<script>



</script>
@endsection