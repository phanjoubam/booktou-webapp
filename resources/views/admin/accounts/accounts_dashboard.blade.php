@extends('layouts.admin_theme_02')
@section('content')
 
    @php 
          $totalCash = $totalOnline = $totalPndCash = $totalPndOnline = 0.00; 
        @endphp
          @foreach ($all_orders as $normal)
           

           @if($normal->bookingStatus == "delivered" || $normal->bookingStatus == "completed") 

                  <!-- checking normal order -->

                            @if($normal->orderType == "normal")

                               
             

                              @if( strcasecmp($normal->paymentType , "online") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                                @php 
                                  $totalOnline += $normal->orderCost  ;  //packing cost already included
                                @endphp
                              @else 
                                @php
                                  $totalCash += $normal->orderCost  ;  //packing cost already included  
                                @endphp
                              @endif 
                    <!-- normal order check ends here -->
                    @elseif($normal->orderType=="pnd")

                    <!-- pnd section checks starts from here -->
                   

                    @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                                             


                                              @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                                                  @if(strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 ) 
                                                    @php 
                                                      $totalPndOnline += $normal->orderCost;
                                                    @endphp
                                                  @else
                                                      @php
                                                        $totalPndOnline += $normal->orderCost  ;
                                                      @endphp  
                                                  @endif  
                                                   @else
                                                        @php 
                                                          $totalPndCash += $normal->orderCost  ;   
                                                        @endphp     
                                              @endif  

                    @else 
  
                                              

                    @if( strcasecmp($normal->paymentType , "online") == 0  )   
                        @if( strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 )  
                          @php
                            
                          @endphp  
                        @else
                          @php
                            $dueOnMerchant  = 0.00;
                            $totalPndOnline += $normal->orderCost;
                          @endphp 
                        @endif 
                    @else
                        @php 
                          $dueOnMerchant  = 0.00;
                          $totalPndCash += $normal->orderCost  ;   
                        @endphp     
                    @endif 

                    @endif

                    <!-- pnd section ends here -->
                    @endif
 


           @endif
  
        @endforeach

  

<div class="row"> 
   <div class='col-md-12'>
    <div class="card card-body">
 
  <div class='row'>  
<div class='col-md-4 offset-md-8 text-right'> 
 <div class="row">

    <form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@viewAccountsDashboard') }}"> 
      <div class="form-row align-items-center"> 
        <div class="col-auto">
          <label class="sr-only" for="inlineFormInputGroup">Month</label>
           <select name='month' class="form-control form-control-sm  ">
              <option value='1'>January</option>
              <option value='2'>February</option>
              <option value='3'>March</option>
              <option value='4'>April</option>
              <option value='5'>May</option>
              <option value='6'>June</option>
              <option value='7'>July</option>
              <option value='8'>August</option>
              <option value='9'>September</option>
              <option value='10'>October</option>
              <option value='11'>November</option>
              <option value='12'>December</option> 
            </select>
        </div>
        <div class="col-auto">
          <label class="sr-only" for="inlineFormInputGroup">Year</label>
            <input readonly name='year' class="form-control form-control-sm  " value="{{ date('Y') }}">
          
        </div>
        <div class="col-auto">
           <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
        </div>
      </div>
  </form>
          </div>
  </div> 
 </div> 
</div> 
</div>

</div>

<div class="row mt-4" >   
  <div class="col-lg-3 col-md-6  ">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper">
                          	<h3>
                          	@if($deposits) 
                          	{{$deposits->depositAmount}}
                          	@else 
                          	0.00
                          	@endif &#x20b9; </h3>
                            <a target="_blank" href="{{ URL::to('admin/accounts/view-deposits-details')}}">
                            	<h5 class="mb-0 font-weight-medium text-primary">Total Deposits</h5>
                            </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>

<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          	<h3>
                          	@if($expense) 
                          	{{$expense->expensesAmount}}
                          	@else 
                          	0.00
                          	@endif &#x20b9; 
                            </h3>
                <a target="_blank" href="{{URL::to('admin/accounts/view-expenses-details')}}"> <h5 class="mb-0 font-weight-medium text-primary">Total Expenses</h5></a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          	<h3>
                          	@if($advance) 
                          	{{$advance->totalAdvance}}
                          	@else 
                          	0.00
                          	@endif &#x20b9;  
                          </h3>
                          <a target="_blank" href="{{URL::to('admin/accounts/view-advance-details')}}">
                            	<h5 class="mb-0 font-weight-medium text-primary">Total Advance</h5>
                            </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>


<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> 
                          <h3> 	@if($reimbursement) 
                          	{{$reimbursement->Totalreimbursement}}
                          	@else 
                          	0.00
                          	@endif &#x20b9; </h3>
                   <a target="_blank" href="{{URL::to('admin/accounts/view-reimbursement-details')}}"> <h5 class="mb-0 font-weight-medium text-primary">Total Fuel Reembursement</h5>
                           </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>




 

</div>

<div class="row mt-4">

	<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"><h3>
                            <?php
                              $totalAmount = $totalCash + $totalPndCash; 
                              $UPI = 0.00;
                            ?>
                            @if($upi) 
                            <?php  $UPI = $upi->upiAmount; ?>
                            @endif
                            {{$totalAmount-$UPI}}  &#x20b9; </h3>
                           <a href=""> <h5 class="mb-0 font-weight-medium text-primary">Cash in bookTou</h5>
                           </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>




<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> <h3>@if($upi) 
                            <?php  $UPI = $upi->upiAmount; ?>
                            {{$upi->upiAmount}}
                            @else 
                            0.00
                            @endif &#x20b9; </h3>
                          <a target="_blank" href="{{URL::to('admin/accounts/view_cash_upi_details')}}">  <h5 class="mb-0 font-weight-medium text-primary">Cash in UPI</h5>
                          </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>

<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> <h3>
                            @if($deposits) 
                            {{$deposits->extraAmount}}
                            @else 
                            0.00
                            @endif &#x20b9; </h3>
                          <a target="_blank" href="{{URL::to('admin/accounts/view_extra_deposit_details')}}">  <h5 class="mb-0 font-weight-medium text-primary">Total Extra</h5>
                          </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>

<div class="col-lg-3 col-md-6">
                        <div class="card">
                   <div class="card-body">

                        <div class="d-flex">
                          <div class="wrapper"> <h3>
                            @if($deposits) 
                            {{$deposits->dueAmount}}
                            @else 
                            0.00
                            @endif &#x20b9; </h3>
                          <a target="_blank" href="{{URL::to('admin/accounts/view_due_deposit_details')}}">  <h5 class="mb-0 font-weight-medium text-primary">Total Due</h5>
                          </a>
                            <p class="mb-0 text-muted"></p>
                          </div>
  

                          
                        </div>
                      </div>
                       </div>
</div>
</div>


@endsection

@section("script")
<script>



</script>
@endsection
