<p><b><span class="badge badge-primary">Expense Details</span></b></p> 
<table class="table table-striped table-bordered mt-2">
					 	<thead>
							<tr>
								<th><small>Expense Date</small></th>
								<th><small>Details</small></th>
								<th><small>Amount</small></th>
							</tr>
						</thead>
						<tbody>
							@foreach($expense as $staffexpense)
							<tr>
								<td>{{$staffexpense->use_date}}</td>
								<td>{{$staffexpense->details}}</td>
								<td>{{$staffexpense->amount}}</td>
							</tr>
							@endforeach

						</tbody>
					</table>