<table class="table">
  <thead>
    <tr>
      <th scope="col">Order No</th>
      <th scope="col">Date</th>
      <th scope="col">Order Type</th>
      <th scope="col">Source</th>
      <th scope="col">Status</th>
      <th scope="col">Mode</th>
      <th scope="col">Target</th>
      <th scope="col">Commentary Order( ₹ )</th>
      <th scope="col">Packaging</th>
      <th scope="col">Discount</th>
      <th scope="col">Refund </th>
      <th scope="col">Delivery</th>
      <th scope="col">Comission For</th>
      <th scope="col">Comission</th>
      <th scope="col">Comission Rate</th>
      <th scope="col">Order value</th>
      <th scope="col">Total Earnings</th>
      <th scope="col"> Merchant Pay-out</th>
      <th scope="col">Due on Merchant</th>
      <th scope="col">Date of payment</th>
      <th scope="col">Mode</th>
      <th scope="col">Remarks</th>
    </tr>
  </thead>

  <tbody>
  
  
    @foreach($orders as $item )

    @php
      $com_order = doubleval($item->totalCost) + doubleval($item->delivery_charge);
      $sale_comission_for = doubleval($com_order) - doubleval($item->packingCharge) - doubleval($item->vendorDiscount) - doubleval($item->refunded);
      $comission = doubleval($sale_comission_for) * (doubleval($item->comissionRate)/100);
      $order_value = $com_order - doubleval($item->vendorDiscount) - doubleval($item->bookTouDiscount) - doubleval($item->refunded) + doubleval($item->delivery_charge);
      $total_earning = doubleval($item->delivery_charge) + $comission - doubleval($item->bookTouDiscount);
      $merchant_pay_out = $order_value - $total_earning;                

    @endphp 

    <tr>
      <td scope="row">{{$item->orderNo}}</td>
      <td>{{date("d-m-Y",strtotime($item->orderDate))}}</td>
      <td>{{$item->order_type}}</td>
      <td>{{$item->source}}</td>
      <td>{{$item->status}}</td>
      <td>{{$item->paymentMode}}</td>
      <td>{{$item->target}}</td>
      <td>{{number_format($com_order,2)}}</td>
      @if(strcasecmp($item->status,'canceled') != 0 && strcasecmp($item->status,'cancelled') != 0)
        <td>{{number_format($item->packingCharge,2)}}</td>
        <td>{{number_format($item->bookTouDiscount,2)}}</td>
        <td>{{number_format($item->refunded,2)}}</td>
        <td>{{number_format($item->delivery_charge,2)}}</td>
        <td>{{number_format($sale_comission_for,2)}}</td>
        <td>{{number_format($comission,2)}}</td>
        <td>{{number_format($item->comissionRate,2)}} %</td>
        <td>{{number_format($order_value,2)}}</td>
        <td>{{number_format($total_earning,2)}}</td>
        
        @if(strcasecmp($item->order_type, "pnd" ) == 0 && strcasecmp($item->source, "business" ) == 0 
        && strcasecmp($item->paymentMode, "ONLINE" ) == 0  && strcasecmp($item->target, "merchant" ) == 0 )
        <td>0.00</td>
        @else
        <td>{{number_format($merchant_pay_out,2)}}</td>
        @endif

        
        @if(strcasecmp($item->order_type, "pnd" ) == 0 && strcasecmp($item->source, "business" ) == 0 
        && strcasecmp($item->paymentMode, "ONLINE" ) == 0  && strcasecmp($item->target, "merchant" ) == 0 )
        <td>{{number_format($merchant_pay_out,2)}}</td>
        @else
        <td>0.00</td>
        @endif
        <td>{{"NA"}}</td>
        <td>{{"NA"}}</td>
        <td>{{"NA"}}</td>
      @else
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      @endif
    </tr>
   @endforeach

  </tbody>
</table>