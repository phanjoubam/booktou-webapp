@extends('layouts.admin_theme_02')
@section('content')

<div class="row"> 

<div class="col-md-12">
@if(isset($deposits)) 
<div class="row mt-3"> 
  <div class="col-md-12">
     <div class="card card-default">
       <div class="card-header">   
       	<div class='row'>
       		<div class='col-md-8'>
       			<h3>Extra Amount for month of {{$month}}, {{$year}}</h3>
       		</div>
       		<div class='col-md-4'>
       			  
       		</div>

       	</div>
 

    </div>
<div class="card-body">  
	
	<table class="table  table-bordered">
	<thead>
		<tr> 
			<th>Date</th>
			<th>Deposit By</th> 
			<th>Due Amount</th> 
		</tr>
	</thead>
	<tbody>
		@php 
			$i = 0;
			$totalDueDeposit=0.00;
			 
		@endphp 
		@foreach($deposits as $item)
		

		<tr> 
			<td>{{ date('d-m-Y', strtotime($item->deposit_date))}}</td>
			<td>
				@if($item->bin == 0)
					@foreach($all_staffs as $staff)
						@if($staff->id == $item->deposit_by)
							<span class='badge badge-warning badge-pill'>{{ $staff->fullname }}</span>
							@break
						@endif
					@endforeach

				
				@else
					@foreach($all_businesses as $business)
						@if($business->id == $item->bin)
							<span class='badge badge-danger badge-pill'>{{ $business->name }}</span> 
							@break
						@endif
					@endforeach
				@endif</td>
			 
			<td>{{$item->due_amount}}</td> 
		</tr> 
		<?php $totalDueDeposit += $item->due_amount;?>
		@endforeach

		<tr>
			<th colspan="2" class='text-right'>Total Agent Deposit</th>
			<th>{{ $totalDueDeposit }}</th> 
		</tr>	
		 


	</tbody>
</table> 

</div>
</div>
</div>
</div> 
@endif

</div>
</div>

@endsection

@section("script")
<script>



</script>
@endsection