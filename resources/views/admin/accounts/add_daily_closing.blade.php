@extends('layouts.admin_theme_02')
@section('content')


   <div class="row">
     <div class="col-md-12"> 
     <div class="card  "> 
             <div class="card-header"> 
              <div class='row'>
                <div class='col-md-10'>
            <h1 class="text-center">{{ $title }} for {{ date('d-m-Y', strtotime($date)) }}</h1>
          </div>
          <div class='col-md-2 text-right'>
             <button class='btn btn-primary' id='btnAddLedger' ><i class='fa fa-plus'></i></button>
          </div>
        </div>
             </div>
                    <div class="card-body">   
   @if (session('msg'))
    <div class="col-md-3 alert alert-success">
        {{ session('msg') }}
    </div>
@endif
   
    <table class="table" max-width='960px;'>
      <thead>
         <tr>
          <th colspan="5" class='text-right'>
              
              <form action="{{ action('Admin\AccountsAndBillingController@dailyAccountLedgerEntry') }}" method="post">
                {{ csrf_field() }}
                <div class="form-row align-items-center">
                  <div class="col-auto">

                    <label class="sr-only" for="datefilter">Change Date</label>
                    <input type="text" class="form-control mb-2 calendar" id="datefilter" name="datefilter" value="{{ date('d-m-Y', strtotime($date)) }}">
                  </div>
                   
                   
                  <div class="col-auto">
                    <button type="submit" name='btnsearch' value='search' class="btn btn-primary mb-2">Search</button>
                  </div>
                </div>
              </form>


          </th> 
        </tr>

        <tr>
          <th>Sl. No.</th>
          <th>Account</th> 
          <th>Details</th>
          <th>Amount</th>
          <th class='text-center'>Is Verified?</th> 
        </tr>
      </thead>
      @php 
        $i=1;
      @endphp
      @foreach ($ledgers as $ledger)
        <tr>
          <td>{{ $i }}</td>
          <td>{{ $ledger->account_name }}</td>
          <td>{{ $ledger->details }}</td>
          <td>{{ $ledger->amount }}</td>
          <td>{{ $ledger->verified_by == null ? "No" : "Yes" }}</td>


        </tr>
        @php 
        $i++;
      @endphp
      @endforeach
      
      <tbody>
      </tbody>
    </table>


 

</div>
</div>
</div>

</div>
 

<form action="{{ action('Admin\AccountsAndBillingController@dailyAccountLedgerEntry') }}" method="post">
    {{ csrf_field() }} 
<div class="modal" tabindex="-1" id='wgtledger'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Ledger Entry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         


  <div class="form-row" >
    <div class="form-group col-md-4">
      <label for="type">Account Type</label>
      <select class="form-control" id="type" name='type'>
        <option>Commission</option>
        <option>Delivery Charge</option>
        <option>Merchant Amount</option>
        <option>Advance Salary</option>
        <option>Expenditure</option>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="amount">Amount</label>
      <input type="number" min='0' class="form-control" id="amount" name='amount' required>
    </div>

     <div class="form-group col-md-4">
      <label for="date">Account Date</label>
      <input type="text"  class="form-control calendar" id="date" name='date' required>
    </div>

  </div>
  <div class="form-group">
    <label for="details">Details (if any)</label>
    <textarea type="text" class="form-control" required id="details" name='details' placeholder="Write details of account eg expenditure on fuel reembursement etc."></textarea>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6"> 
      <label for="enteredby">Entered by</label>
      <input type="text" readonly class="form-control" id="enteredby" value="{{ Session::get('_full_name') }}" >
    </div>
    <div class="form-group col-md-6"> 
      <label for="entrydate">Entry Date</label>
      <input type="text" readonly class="form-control" id="entrydate" value="{{ date('d-m-Y') }}" >
    </div>

  </div>
 
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="hasverified" name='hasverified' required>
      <label class="form-check-label" for="hasverified">
        I have verified the amount before entry.
      </label>
    </div>
  </div>
  


</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" name='btnsave' value='save'>Save Ledger</button>
      </div>
    </div>
  </div>
</div>
</form>

@endsection

@section("script")


<script type="text/javascript">
  




$(document).on("click", "#btnAddLedger", function()
{
  $("#wgtledger").modal("show")

});


$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
  


</script>
@endsection