@extends('layouts.admin_theme_02')
@section('content')
 
<?php 
    $cdn_url =   env('APP_CDN') ;  
    $cdn_path =   env('APP_CDN_PATH') ; // cnd - /var/www/html/api/public

?>

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                <div class="row">
         <div class="col-md-7">
           <h5>Products Listed Under <span class='badge badge-primary'>{{  $data['business']->name }}</span></h5>
         </div>

       <!--   <div class="col-md-5 text-right">
           <form class="form-inline" method="post" action="{{  action('Admin\AdminDashboardController@searchBusinessProducts') }}">
            {{ csrf_field() }}
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Category:</label> 
            <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='search_key'>
              <option value='all'>-- ALL PRODUCTS --</option> 
              @foreach ($data['category'] as $item)
                <option value='{{$item->category_name}}'>{{$item->category_name}}</option> 
              @endforeach
            </select>
            <input type="hidden" value="{{ $data['bin'] }}" name="busi_id">
            <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search</button>
          </form>  
        </div>  -->
      </div> 


                </div>
            </div>
       <div class="card-body"> 
  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif
  <div class="table-responsive">
  <table class="table"> 
    <thead class=" text-primary">
      <tr >
        <th >Image</th>
        <th >Item </th> 
        <th >Category</th>
        <th >Price</th>
        <th >Discount</th>       
        <th >Edit</th>
        <th >Delete</th>
      </tr>
    </thead> 
    <tbody> 
    @foreach ($data['products'] as $itemP)  
     <tr >
     <td> 
      <img src="{{ $itemP->image  }}" alt="..." height="50px" width="50px"> 
    </td>
     <td style="white-space: normal;"><span class='badge badge-primary'>{{ $itemP->id }}</span><br/>
      {{ $itemP->pr_name }}<br/>
      {{  $itemP->description }}</td>
      <td>{{$itemP->category}}</td>        
      <td>{{$itemP->unit_price}}</td>
      <td>{{$itemP->discount}}</td>
      
      <td>
        <button class="btn btn-primary btnedit"
       data-key="{{ $itemP->prsubid }}"
       data-category="{{$itemP->category}}"
       data-price="{{$itemP->unit_price}}"
       data-discount="{{$itemP->discount}}"
        data-widget="edit">Edit</button>
        
      </td>
      <td>
        
        <button class="btn btn-danger btndel" 
        data-key="{{ $itemP->prsubid }}" 
        data-discount="{{$itemP->discount}}" 
        data-widget="del"
        >Delete</button>
      </td>
 
    </tr>
    @endforeach
   </tbody>

  </table>

  {{ $data['products']->links() }}

 </div>

  </div>

   
</div>    
  
   </div> 
 </div>

<form action="{{action('Admin\AdminDashboardController@updateDiscountProducts')}}" method="post" > 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-edit" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document" >
      <div class="modal-content ">
        <div class="modal-header">
          <h5 class="modal-title" >Edit Business Product</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="card mt-2 mb-2">
            <div class="row "> 
                 <div class="col-md-6">
                        <label  class="form-label">Category</label>
                       <input class="form-control" type="category" id="category" name="category">

                </div>
                                  
                <div class="col-md-6">
                    <label  class="form-label">Price</label>
                    <input class="form-control" type="price" id="price" name="price">

                </div>
                <div class="col-md-6">
                    <label  class="form-label">Discount</label>
                    <input class="form-control" type="discount" id="discount" name="discount">

                </div>
                
            </div>
             
        </div>
        <div class="modal-footer">
         <input type='hidden' name='key' id='key'/>
         <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

          <button type='submit' class="btn btn-danger" value='delete' id="btndel" name='btndel'>Update</button>
        </div>
      </div>
    </div>
  </div>
 </form>
<!--  -->

<!-- delete modal -->
<form action="{{action('Admin\AdminDashboardController@deleteDiscountProducts')}}" method="post" > 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-del" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Confirm Record Deletion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">You are about to delete this record. Please confirm your action?

        </div>
        <div class="modal-footer">
        <input  type="hidden" id="discount" name="discount">
         <input type='hidden' name='key' id='keydel'/>
         <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' id="btndel" name='btndel'>Delete</button>
        </div>
      </div>
    </div>
  </div>
 </form>
 <!-- delete ends here -->

@endsection

@section("script")

<script>

$(".btnedit").on('click', function(){ 
      var key  = $(this).attr("data-key"); 
      $("#key").val(key); 
      $widget  = $(this).attr("data-widget");  
      $("#category").val($(this).attr("data-category"));
      $("#price").val($(this).attr("data-price"));
       $("#discount").val($(this).attr("data-discount")); 
      $("#widget-"+$widget).modal('show'); 
}); 

$(".btndel").on('click', function(){ 
      var key  = $(this).attr("data-key"); 

      $("#keydel").val(key); 
      $widget  = $(this).attr("data-widget"); 
       $("#discount").val($(this).attr("data-discount"));
      $("#widget-"+$widget).modal('show'); 
});

  </script>

 @endsection