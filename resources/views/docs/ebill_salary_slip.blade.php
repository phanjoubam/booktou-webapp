<h3>
	Ezanvel Solutions (OPC) Pvt. Ltd.<br>
	CIN U72900MN2017OPC013529<br>
	Wangkhei Koijam Leikai, Imphal East – 795005<br>
	www.ezanvel.in 
</h3>


<h7 style="text-align: center;">Salary Slip</h7>

<br><br><br>

<table style="width: 700px;border: 1px solid #000;">
<tbody>
	<tr>
		<td>Name: </td>
		<td>{{$data->fullname}}</td>
	</tr>

	<tr>
		<td>Basic: </td>
		<td>{{$data->basic_pay}}</td>
	</tr>
	<tr>
		<td>DA: </td>
		<td>{{$data->da}}</td>
	</tr>
	<tr>
		<td>TA: </td>
		<td>{{$data->ta}}</td>
	</tr>
	<tr>
		<td>HRA: </td>
		<td>{{$data->hra}}</td>
	</tr>
	<tr>
		<td>Bonus: </td>
		<td>{{$data->bonus}}</td>
	</tr>
	<tr>
		<td>Gross Salary: </td>
		<td>{{$data->gross}}</td>
	</tr>
</tbody> 
</table>