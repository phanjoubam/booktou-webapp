<?php 

$total_service_fee=0;
$total_sum_amount=0;
$total_due_amount=0;
?>
<h3 style="font-weight: bolder;text-align: center;">
 Report for payment clearance no <span class='badge badge-primary'># {{$clearanceid}}</span>
</h3>

                <?php 

                    foreach ($paid as $clear) {
                    $paidAmount = $clear->amount;
                    }

                ?>
<h3 style="font-weight: bolder;text-align: center;">Paid amount:<small>{{$paidAmount}}</small></h3>


<table style="border: 1px dashed #000;margin-left: auto;margin-right: auto;">
        <thead >
              <tr >
          <th style="text-align: center;font-size: 13px;padding: 5px;">Order No</th>
          <th style="text-align: center;font-size: 13px;padding: 5px;">Source</th>
          <th style="text-align: center;font-size: 13px;padding: 5px;">Order type</th>
          <th style="text-align: center;font-size: 13px;padding: 5px;">Clearance</th>
          <th style="text-align: center;font-size: 13px;padding: 5px;">Payment Type</th>
          <th style="text-align: center;font-size: 13px;padding: 5px;">Service Fee</th>
          <th style="text-align: center;font-size: 13px;padding: 5px;">Book Date</th>
                     <th style="text-align: center;font-size: 13px;padding: 5px;">Commission</th>
                     <th style="text-align: center;font-size: 13px;padding: 5px;">Due by merchant</th>
          <th style="text-align: center;font-size: 13px;padding: 5px;">Amount</th>
              </tr>

          </thead>


          <tbody style="border: 1px dashed #000;">
                                   
                                   @php 
                                    $total_sales  = 0; 
                                    $total_commission =0;
                                   @endphp 

            @foreach($data as $clearance)

                            @php  
                            $actual = 0;
                            $commission = 0;
                            $payable = 0;
                            $total_due=0;
                            @endphp 

                            @php
                            $actual = $clearance->total_amount;     
                            @endphp

            <tr>
            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">{{$clearance->id}}</td>
            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">{{$clearance->source}}</td>

                            @if($clearance->orderType == "pnd")
                    @if($clearance->payment_target=="merchant" && $clearance->source == "booktou" || $clearance->source == "customer" ) 
                      @php 
                      $commission = 0 ;
                      @endphp
                    @elseif($clearance->source == "booktou" || $clearance->source == "customer" ) 
                      @php 
                         
                    $commission = ( $clearance->commission * 0.01 * $clearance->total_amount ); 

                      @endphp
                      @else
                       @php 
                        $commission = 0 ; 
                      @endphp
                    @endif 
                  @else 

                    @php
                    $commission = ( $clearance->commission * 0.01 * $clearance->total_amount ); 
                    @endphp 

              @endif

              @php
              $total_sales += $actual ;
              $total_commission += $commission ;      
              @endphp




            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">{{$clearance->orderType}}</td>
            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">{{$clearance->clerance_status}}</td>
            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">{{$clearance->pay_mode}}</td>
            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">{{$clearance->service_fee}}</td>
            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">
                            {{date('d-m-Y', strtotime($clearance->book_date))}}</td>

                            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">{{$commission}}</td>
                            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">
                              <?php


                              if ($clearance->payment_target=="merchant" && 
                                  $clearance->pay_mode == "ONLINE" || 
                                  $clearance->pay_mode == "Pay online (UPI)") {
                                 $total_due = $clearance->service_fee;
                              }else{
                                $total_due=0;
                              }

                              ?>

                              {{$total_due}}

                            </td>
            <td style="text-align: center;font-size: 13px;border-bottom: 1px dashed #000;">@if($clearance->orderType == "pnd")
                    @if( $clearance->payment_target == "merchant" 
                      && $clearance->pay_mode == "ONLINE" || 
                      $clearance->pay_mode == "Pay online (UPI)") 
                      @php 
                        $_amount = 0; 
                      @endphp
                    @else 
                      @php 
                        $_amount = $clearance->total_amount;
                      @endphp
                    @endif 
                  @else 

                   @php 
                        $_amount = $clearance->total_amount;
                   @endphp
              @endif


                 


                              {{number_format($_amount,2)}}

                            </td>
            </tr>
            <?php 

              $total_sum_amount+=$_amount;
              $total_service_fee+=$clearance->service_fee;
              $total_due_amount+=$total_due;
            ?>

            @endforeach

            <tr>
              <td></td>
                                   <td style="text-align: center;font-size: 13px; "></td>
              <td style="text-align: center;font-size: 13px; "></td>
              <td style="text-align: center;font-size: 13px; "></td>
               
              <td style="text-align: center;font-size: 12px;text-align: right;">
                                   <span style="font-weight: bold; margin: 5px;">Service fee:</span></td>
              <td style="font-weight: bold;text-align: center;font-size: 12px; ">Rs {{$total_service_fee}}</td>
                                   <td style="font-weight: bold;text-align: center;font-size: 12px; "> Total Commission:  
                                   </td>
                                    
                                   <td style="text-align: center;font-size: 12px;" >
                                    Rs {{$total_commission}}
                                   </td>
              <td style="font-weight: bold;text-align: center;font-size: 12px; 
              margin: 5px;">Total Amount</td> 
              <td style="font-weight: bold;text-align: center;font-size: 12px;">
                                   Rs {{number_format($total_sum_amount,2)}}</td>
            </tr>


                            <tr>
                                   <td></td>
                                   <td style="text-align: center;font-size: 13px; "></td>
                                   <td style="text-align: center;font-size: 13px; "></td>
                                   <td style="text-align: center;font-size: 13px; "></td>
                                   <td style="text-align: center;font-size: 13px; "></td>
                                   <td style="text-align: center;font-size: 13px; ">
                                    </td>
                                   <td style="font-weight: bold;text-align: center;font-size: 13px; ">Total due by merchant:  
                                   </td>
                                   <td style="font-weight: bold;text-align: center;font-size: 13px; ">  {{$total_due_amount}}
                                   </td>
                                   

                                   <td style="font-weight: bold;text-align: center;font-size: 12px; margin: 5px;"> 
                                   Payable Amount </td> 
                                   <td style="font-weight: bold;text-align: center;font-size: 12px;">
                                  Rs {{number_format($total_sum_amount - $total_commission-$total_due_amount,2)}}</td>
                            </tr>
             
          </tbody>

          
        </table>