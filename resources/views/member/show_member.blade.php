@extends('layouts.light_theme')
@section('pagebody')

<div class='container'>
<div class='row'>
<div class='col-md-12'>

		<table class='table table-bordered'>
		<tr>
		<td>Name</td>
		<td>Gender</td>
		<td>Date of Birth</td>
		<td>Locality</td>
		<td>Lanmark</td>
		<td>Phone</td>
		</tr>
		@foreach ($data['members'] as $item)
		 <tr>
		<td>{{ $item->fname}} {{ $item->mname}} {{ $item->lname}}</td>
		 <td>{{ $item->gender}}</td>
		 <td>{{ date('d-m-Y',strtotime($item->dob))}}</td>
		 <td>{{ $item->locality}}</td>
		 <td>{{ $item->landmark}}</td>
		 <td><?php echo $item->phone_1 ; ?></td>
		 <td><a href="" class='btn btn-primary btn-sm'>Edit</a>&nbsp;
		 <a href="" class='btn btn-danger btn-sm'>Delete</a></td>
		 </tr>
		@endforeach
		</table>
		</div>
		</div>
		</div>
 
@endsection