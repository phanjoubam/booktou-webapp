 <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Welcome to WaterApp </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="{{URL::to('/dashboard')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>

      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="{{URL::to('/profile-update1')}}" >
          <span>Profile Update</span>
        </a>
        <a class="nav-link collapsed" href="{{URL::to('/staff-update')}}" >
          <span>Update Staff</span>
        </a>
          <a class="nav-link collapsed"  href="{{URL::to('/retailer/profile/edit')}}" >
          <span>Business Details Update</span>
        </a>

        <a class="nav-link collapsed" href="{{URL::to('/dashboard')}}" >
          <span>View Orders</span>
        </a>
        <a class="nav-link collapsed" href="{{URL::to('/view-staff')}}" >
          <span>View Staffs</span>
        </a>
        
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      

      <!-- Divider -->
      <hr class="sidebar-divider">

      

      

    

      

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->