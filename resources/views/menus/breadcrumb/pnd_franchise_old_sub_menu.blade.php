<div class="card">
	 <div class="p-2">
	<div class='row'>
		
        <div class='col-md-6'> 
            
        <div class="btn-group btn-group-sm" role="group" aria-label="PnD Menu Item">
		<button type="button" class="btn btn-secondary btn-sm"><a   href="{{ URL::to('/franchise/pick-and-drop-orders/view-all') }}">Active Orders</a></button>

		<button type="button" class="btn btn-secondary btn-sm"><a   href="{{ URL::to('/franchise/pick-and-drop-orders/view-completed') }}">Completed Orders</a>
        </button>
        <button type="button" class="btn btn-secondary btn-sm">
		 <a class="addOrder"    name='btn_add'>Create New</a> 
        </button>
	   </div>

	</div> 

<div class='col-md-6'>
     <div class="row">
<form class="form-inline" method="post" action="{{  action('Franchise\FranchiseDashboardControllerV1@viewPickAndDropCompletedOrders') }}">
            {{ csrf_field() }} 
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 
            <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' 
            value="{{ date('d-m-Y',  strtotime($data['last_keyword_date']))  }}" name='filter_date' />

             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Status:</label> 
                <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='filter_status' >
                  <option value='-1'>All</option> 
                    <option value='new'>New</option> 
                    <option value='confirmed' <?php if($data['last_keyword']  == "confirmed") echo "selected";  ?>>Confirmed</option> 
                      <option value='delivery_scheduled' <?php if($data['last_keyword']  == "delivery_scheduled") echo "selected";  ?>>Wating Agent Pickup</option>  
                    <option value='order_packed' <?php if($data['last_keyword']  == "order_packed") echo "selected";  ?>>Packed and Ready for delivery</option>    
                    <option value='in_route' <?php if($data['last_keyword'] == "in_route") echo "selected";  ?>>In Route</option>   
                </select> 
                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
            </form>
        </div>
	</div> 
	</div> 
</div> 
</div> 