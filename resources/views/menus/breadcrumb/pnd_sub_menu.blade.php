<div class="card">
	 <div class="p-2">
	<div class='row'>
		<div class='col-md-6'>  
    
    <div class="btn-group btn-group-sm" role="group" aria-label="PnD Menu Item">
      <button type="button" class="btn btn-info btn-sm "><a  class="white"   href="{{ URL::to('/admin/customer-care/pick-and-drop-orders/view-all') }}">Active Orders</a></button>
      <button type="button" class="btn btn-success btn-sm "><a   class="white"  href="{{ URL::to('/admin/customer-care/pick-and-drop-orders/view-completed') }}">Completed Orders</a></button>
      <button type="button" class="btn btn-primary btn-sm "><a type="button" class="addOrder white"    name='btn_add'>Create New</a></button> 
    </div> 

    
	</div> 

<div class='col-md-6 text-right'>


 <div class="row">

<form class="form-inline" method="post" action="{{  action('Admin\AdminDashboardController@viewPickAndDropOrders') }}">
            {{ csrf_field() }} 
            
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Merchant:</label> 
                <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='filter_business' >
                  <option value='-1'>All</option> 
                  @foreach($data['todays_business'] as $item)
                    <option value='{{ $item->id }}'>{{ $item->name }}</option>
                  @endforeach
            </select> 


            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 
            <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' 
            value="{{ date('d-m-Y',  strtotime($data['last_keyword_date']))  }}" name='filter_date' />
 

                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>
                <i class='fa fa-search'></i>
                </button>


            </form>
          </div>
	</div> 
	</div> </div> 
</div> 