<div class="card">
	 <div class="p-2">
	<div class='row'>
		<div class='col-md-8'>  
    
    <div class="btn-group btn-group-sm" role="group" aria-label="PnD Menu Item">
      <button type="button" class="btn btn-info btn-sm "><a  class="white"   href="{{ URL::to('/admin/orders/assists/view-all') }}">Active Orders</a></button>
      <button type="button" class="btn btn-success btn-sm "><a   class="white"  href="{{ URL::to('/admin/orders/assists/view-all/view-completed') }}">Completed Orders</a></button>
    </div> 

    
	</div> 

<div class='col-md-4 text-right'>


 <div class="row">

<form class="form-inline" method="get" action="{{  action('Admin\AdminDashboardController@viewAllAssistOrdersCompleted') }}"> 
              
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 
            <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' 
            value="{{ date('d-m-Y',  strtotime($data['last_keyword_date']))  }}" name='filter_date' />
 

                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>
                <i class='fa fa-search'></i>
                </button>


            </form>
          </div>
	</div> 
	</div> </div> 
</div> 