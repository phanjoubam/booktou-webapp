<nav class="nav">
   <a  class="nav-link" href="{{ URL::to('/admin/cms') }}">CMS</a>

   <a  class="nav-link"  href="{{ URL::to('/admin/cms/menus') }}">Menus</a>

   <a  class="nav-link" href="{{ URL::to('/admin') }}">Add New Menu</a>

   <a  class="nav-link" href="{{ URL::to('/admin/systems/manage-app-banner') }}">Manage App Slider</a>
   
   <a  class="nav-link" href="{{ URL::to('/admin/systems/add-qr-menu') }}">Add QR Menu</a>
   <a  class="nav-link" href="{{ URL::to('/admin/systems/save-section-items') }}">Page Section</a>
   <a  class="nav-link" href="{{ URL::to('/admin/systems/edit-product-section_details') }}">Manage Page</a>
   <a  class="nav-link" href="{{ URL::to('/admin/systems/product-section_details') }}">Add Section Details</a>
</nav> 