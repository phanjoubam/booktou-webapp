<ol  class="nav nav-pills">
		  

	<li class="nav-item"><a  class="nav-link" href="{{ URL::to('/admin/products/category/manage-categories') }}">New Product Category</a></li>
	<li class="nav-item"><a class="nav-link" href="{{  URL::to('/admin/systems/manage-business-categories' )}}">Business Categories</a></li>  
	<li class="nav-item"><a class="nav-link" href="{{  URL::to('/admin/services/manage-category' )}}">Service Categories</a></li>  
	<li class="nav-item"><a class="nav-link" href="{{  URL::to('/admin/systems/edit-configuration' )}}">Platform Configuration</a></li>
	<li class="nav-item"><a class="nav-link" href="{{  URL::to('/admin/slide/enter-image-slide' )}}">Sliding Photo</a></li>
	<li class="nav-item"><a class="nav-link" href="{{  URL::to('/admin/system/arrange-icon' )}}">Arrange App Icons</a></li>
	<li class="nav-item"><a class="nav-link" href="{{  URL::to('/admin/system/water-booking/create-brand-name' )}}">Create Brand Name</a></li>
</ol> 