<nav class="navbar navbar-expand-lg navbar-light bg-light"> 
    <ul class="navbar-nav">
		<li class="nav-item active"><a class="nav-link btn btn-primary btn-compose" href="#">Compose New Message</a></li>  
	</ul>
	
</nav> 
<div class="row">
    <div class="col-md-12">
     @if (session('err_msg'))
    <div class="alert alert-info p-0">
    {{session('err_msg')}}
    </div>
    @endif 
 
   </div>
</div>