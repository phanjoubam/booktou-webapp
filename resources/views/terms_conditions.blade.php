@extends('layouts.store_front_no_heading')
@section('content') 

 <div class="breadcumb_area bg-img" style="background-image: url('{{ URL::to("/public/store/image/breadcrumb.jpg" ) }}') ;"  >
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Terms and Conditions</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> 
   
<section class="single_product_details_area d-flex align-items-center box-lg" >
<div class="container">
<div class="row">
  
<div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1"> 


          <ul class='list-unstyled txt'>
              <p class="x-biggest">
 
        These terms of use ("Terms of Use") mandate the terms on which users ("You" or "Your" or "Yourself" or "Users") interested in browsing or availing bookTou Services (defined below), and accessing the platform www.bootTou.com and the mobile application owned and operated by Ezanvel Solutions OPC Pvt. Ltd. collectively referred to as, the "Platform" connects with the merchants registered on the Platform ("Tied-up Merchants"), merchants not registered on the Platform ("Non-tied up Merchants") (together hereinafter referred to as "Merchants") and with delivery partners ("Delivery Partners") to avail the bookTou Services.
         
        Please read the Terms of Use carefully before using the Platform or registering on the Platform or accessing any material or information through the Platform. By clicking on the "I Accept" button, You accept this Terms of Use and agree to be legally bound by the same.
        Use of and access to the Platform is offered to You upon the condition of acceptance of all the terms, conditions and notices contained in this Terms of Use and Privacy Policy, along with any amendments made by bookTou at its sole discretion and posted on the Platform from time to time.
        For the purposes of these Terms of Use, the term “bookTou” or 'Us' or 'We' refers to Ezanvel Solutions OPC Pvt. Ltd. The term 'You' refers to the user or visitor of the Website and/or App. When You use our services, You will be subject to the terms, guidelines and policies applicable to such service and as set forth in these Terms of Use. As long as you comply with these Terms of Use, We grant You a personal, non-exclusive, non-transferable, limited privilege to enter and use our Platforms and services.
</p>
      
          <br/>
          <h3>1. Registration:</h3>

   <p class="x-biggest">PART A - GENERAL TERMS RELATING TO “bookTou” SERVICES 
    </p>
   <p class="x-biggest"> 
   1.1. You shall be permitted to access the Platform, avail the bookTou Services and connect with Merchants and Delivery Partners on the Platform only upon creating an Account (as defined below) and obtaining a registration on the Platform. Your ability to continue using the Platform, bookTou Services is subject to Your continued registration on the Platform. You will be required to enter Your personal information including your name, contact details, valid phone number while registering on the Platform.
    </p>
   <p class="x-biggest">  

        1.2. As a part of the registration, You may be required to undertake a verification process to verify Your personal information and setting up the Account.
  </p>
   <p class="x-biggest"> 

        1.3. bookTou shall have the right to display the information, feedback, ratings, reviews etc. provided by You on the Platform. You agree and accept that as on the date of Your registration on the Platform, the information provided by You is complete, accurate and up-to-date. In the event of any change to such information, You shall be required to promptly inform bookTou of the same, in writing, at least 1 (one) week prior to the date on which such change shall take effect. You acknowledge and accept that bookTou has not independently verified the information provided by You. bookTou shall in no way be responsible or liable for the accuracy or completeness of any information provided by You. If You provide any information that is untrue, inaccurate, not current or incomplete, or bookTou has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, bookTou reserves the right to suspend or terminate Your Account (defined below) and refuse any and all current or future use of the Platform (or any portion thereof) at any time.</p>
   
 
        <br/>
        <h3>2. bookTou Services:</h3>
        <p class="x-biggest">  2.1. The Platform provides You with the following services ("bookTou Services"):

 
</p>

 <ul>
            <li> It allows You to connect with Merchants and Delivery Partners;</li>
            <li>   It allows You to view the items/services ("Items") listed on the Platform by the Tied Up Merchants;</li>
            <li>It allows You to purchase Item(s) from the Tied Up Merchants listed on the Platform and allows You to get the Items delivered to You through Delivery Partners ("Purchase Services");
           </li>

            <li>It allows You to purchase Items from Non-Tied Up Merchants and get the same delivered to You by the Delivery Partners ("Delivery Services");</li>

            <li>  It allows you to pick up- and drop off packages from 1 (one) location to the other through the Delivery Partner ("Pick Up and Drop Off Services");</li>

            <li>  It allows You to give ratings, write comments and reviews about Delivery Partners and Merchants;</li>
            <li> It facilitates improvement in the quality of the services provided by bookTou on the Platform based on User ratings, reviews and feedbacks provided on the Platform.</li>
           
        </ul>

  
        <br/>

               <p class="x-biggest">
  2.2. Once the Delivery Services/ Purchase Services have been completed or delivered, as the case may be, You shall promptly notify the same on the Platform.
 
</p>

             <p class="x-biggest"> 
    2.3. bookTou may, at its absolute sole discretion, add, modify, upgrade, extend, withdraw or alienate any of the bookTou Services listed above from time to time. bookTou does not provide any guarantee to You that the bookTou Services will be made available to You at all times.
</p>

             <p class="x-biggest"> 
   2.4. You hereby agree and acknowledge that bookTou is only a facilitator between You, the Merchants and Delivery Partners (as the case maybe) and bookTou only provides You with access to the Platform to connect with Merchants and Delivery Partners for You to initiate transactions on the Platform. You hereby agree and acknowledge that bookTou will not be a party to any of the transactions that are initiated by You through the Platform and bookTou shall not be liable in any manner or incur any liability with respect to the services performed by the Merchants or the Delivery Partners, as the case may be. Further, You hereby agree and acknowledge that bookTou shall not be liable for the conduct, acts and omissions of the Merchants (including their employees and consultants) and Delivery Partners in the course of providing their services to You, or for any loss or damage to the Item or otherwise caused to You as a consequence of or in relation to the services being provided to You by the Merchants or the Delivery Partner, as the case may be.
</p>
 

              <p class="x-biggest"> 
   2.5. You shall not initiate any transaction for any Item on the Platform which is illegal, immoral, unethical, unlawful, unsafe, contains harmful substance and is in violation of this Terms of Use and applicable laws. You specifically agree that You shall not initiate any transaction on the Platform for the purchase or delivery of any alcoholic beverages, narcotic drug or psychotropic substance, etc. Further, You hereby acknowledge and agree that bookTou shall not be liable for any indirect, direct damage or loss, cost, expense incurred by You in relation to the transactions initiated by You on the Platform.
</p>

              <p class="x-biggest"> 
  2.6. bookTou does not check or verify the packages that are being picked up and dropped off on behalf of You or the Items that are being delivered to You by the Delivery Partner, and therefore bookTou shall have no liability with respect to the same. However, if it comes to the knowledge of bookTou that You have packaged any illegal or dangerous substance or availed the Pick- up and Drop Off Services using the Platform to deliver any illegal or dangerous substance, bookTou shall have the right to report You to the government authorities and take other appropriate legal actions against You.
</p>

              <p class="x-biggest"> 
 2.7. You hereby acknowledge that bookTou shall not be liable for any damages of any kind arising from Your use of the bookTou Services, including, but not limited to direct, indirect, incidental, punitive, and consequential damages.
</p>


              <p class="x-biggest"> 
     2.8. bookTou shall be entitled at any time without giving any reason terminate Your request for any bookTou Service.
</p>
             <p class="x-biggest"> 
      2.9. You hereby agree that bookTou shall not be liable for any conduct or misbehavior or actions of Delivery Partner with respect to any transactions initiated on the Platform. Further, You agree that bookTou has no control over the Items provided to You by the Merchants and therefore, bookTou shall not incur any liability with respect to such Items. However, keeping in mind the interests of the Users, We have informed our Merchants to ensure that Items are packaged properly to avoid any form of spillage or damage to the Item or any issues related to packaging
</p>

       <p class="x-biggest"> 
 2.10. You hereby agree that scheduling and rescheduling a transaction on the Platform depends upon the availability of Delivery Partners around Your area at the time of such scheduling and re-scheduling a transaction. Should You choose to reschedule a transaction on the Platform at a later point of time, You shall cancel the current transaction on the Platform (if initiated) and initiate a new transaction on the Platform, as per Your convenient time.
</p>

       <p class="x-biggest"> 
   2.11. If a transaction initiated by You on the Platform cannot be completed, bookTou shall notify You on the Platform.
</p>

      <p class="x-biggest"> 
   2.12. You agree to provide as much information as possible on the Platform with respect to the Items/services You wish to purchase/avail, using the Platform.
</p>

   <p class="x-biggest"> 
      2.13. bookTou shall use Your location based information that is captured by bookTou through global positioning system when You are using Your mobile device to request a bookTou Service on its m-app. Such location based information shall be used by bookTou only to facilitate and improve the bookTou Services being offered to You.
</p>

  <p class="x-biggest"> 
      2.14. We can’t fulfill any tasks which are immoral or unlawful in nature. bookTou reserves the right to refuse to perform any tasks on the grounds of such tasks being immoral/unethical/unlawful/banned either by bookTou internal policies or as per the independent discretion of bookTou. bookTou may also refuse to perform any task on the grounds that such task is prohibited under any contract to which we are party.
</p>

  <p class="x-biggest"> 
   2.15. You understand and acknowledge that bookTou by itself does not sell or provide any such Items. bookTou is not responsible for the quality, merchantability or fitness of such Items. Accordingly, in the event of any grievances arising from the transaction initiated by You on the Platform pertaining to purchase or sale of any product from any Merchant, You may contact bookTou support for routing. your grievances to the Merchant through the Platform.</p>

        <p class="x-biggest">    
           2.16. You hereby acknowledge that if You have any complaint with respect to the bookTou Services, You will first inform bookTou in writing within 24 (twenty four) hours of using such bookTou Services.
 
</p>
        
        
          <br/>
        <h3>3. User Information</h3>
        <p class="x-biggest">
3.1. You are solely responsible for and in control of the information You provide to us. Compilation of User Accounts and User Account bearing contact number and e-mail addresses are owned by bookTou. Further, You understand and agree that certain information will be case sensitive and must be handled with a prudent care.
3.2. In the case where the Platform is unable to establish unique identity of the User against a valid mobile number or e-mail address, the Account shall be indefinitely suspended. bookTou reserves the full discretion to suspend a User's Account in the above event and does not have the liability to share any Account information whatsoever.
</p>
 
 <h3>4. Payment Terms</h3>
          <p class="x-biggest"> 
     4.1. Purchase Services: While initiating a request for a Purchase Service, You shall pay for the price of the Items You require the Delivery Partners to deliver to You from the Tied Up Merchant. The transaction for the Purchase Service will be initiated on the Platform once You have completed the payment for the same on the Platform. In certain exceptional circumstances, if the purchase price of the Item is not available on the Platform, You shall be required to pay the purchase price of the Item, through the Platform, as may be communicated to You by the Delivery Partner, prior to the Delivery Partner undertaking Purchase Service. </p> 
           
                <p class="x-biggest"> 
      4.2. Delivery Services: While availing Delivery Service, You shall pay the purchase price of the Item through the Platform, as may be communicated to You by the Delivery Partner on behalf of the Non- Tied up Merchant. Only upon processing such agreed amount via Platform, shall the Delivery Partner purchase the Item on Your behalf.</p>
  <p class="x-biggest"> 
      4.3. Pick Up and Drop Off Services: While initiating a request for a Pick Up and Drop Off Service, You shall pay the service fees for availing the Pick Up and Drop Off Service, as may be displayed to You on the Platform. Only upon making such payment will the Delivery Partner initiate the Pick Up and Drop of Service.</p>
  <p class="x-biggest"> 
     4.4. Service Fees: With respect to Delivery Services and Purchase Services, You will be charged a separate service fees ("Service Fees"). The Service Fees shall be paid prior to availing any of the bookTou Services or whenever applicable.</p>
  <p class="x-biggest"> 
      4.5. You agree that bookTou may use certain third-party vendors and service providers, including payment gateways, to process the payments made by You on the Platform.</p>
                 
                
      
          <br/>
        <h3>5. Rating</h3>
        <p class="x-biggest">
      5.1. You agree that: 
      </p>  
       <ul>
            <li>after completion of a transaction on the Platform, the Platform will prompt the User with an option to provide a rating and comments about the Delivery Partner (with respect to the services performed by the Delivery Partner) and Merchants (with respect to the Items sold/provided by them); and</li>
            <li>the Delivery Partner and the Tied-Up Merchants may also be prompted to rate You on the Platform. Based upon such Delivery Partner and Merchant ratings, Your rating score on the Platform will be determined and displayed.</li> 
        </ul>
   <br/>

  <p class="x-biggest">
       5.2. bookTou and its affiliates reserve the right to use, share and display such ratings and comments in any manner in connection with the business of bookTou and its affiliates without attribution to or approval of Users and You hereby consent to the same. bookTou and its affiliates reserve the right to edit or remove comments in the event that such comments include obscenities or other objectionable content, include an individual’s name or other personal information, or violate any privacy laws, other applicable laws or bookTou’s or its affiliates’ content policies.
    </p>
    
     <p class="x-biggest"> 
      5.3. Location: You acknowledge and agree that Your geo-location information is required for You to avail the Dunzo Services and initiate transactions on the Platform. You acknowledge and hereby consent to the monitoring and tracking of Your geo-location information. In addition, Dunzo may share Your geo-location information with Delivery Partners and Merchants (as the case maybe). </p>    
   

          <br/>
        <h3>PART B - SPECIFIC TERMS FOR “bookTou” SERVICES</h3>
     
        <br/>
        <h3>6. Cancellation and Refund</h3>
               <p class="x-biggest">
       6.1. bookTou shall confirm and initiate the execution of the transaction initiated by You upon receiving confirmation from You for the same. If You wish to cancel a transaction on the Platform, You shall select the cancel option on the Platform. It is to be noted that You may have to pay a cancellation fee for transactions initiated on the Platform for which work has already been commenced by the Delivery Partner or the Merchant, as the case may be. With respect to work commenced by Merchants the cancellation fee will be charged to You which will be in accordance with the cancellation and refund policies of such Merchants.
      </p>  
       

          <p class="x-biggest">
      6.2. bookTou may cancel the transaction initiated by You on the Platform, if:
      </p>  
       <ul>
            <li>The designated address to avail the bookTou Services provided by You is outside the service zone of bookTou.</li>
            <li>Failure to get your response via phone or any other communication channel at the time of confirmation of the order booking.</li> 

            <li> The transaction involves supply/delivery/purchase of any material good that is illegal, offensive or volatile of the Terms of Use.</li> 

             <li>Information, instructions and authorizations provided by You is not complete or sufficient to execute the transaction initiated by You on the Platform.</li>

            <li>If in case of tied-up Merchants, the Tied-Up Merchant outlet is closed.</li>

             <li> If a Delivery Partner is not available to perform the services, as may be requested.</li>


            <li>If any Item for which You have initiated the transaction is not in stock with the Merchant.</li>

             <li> If the transaction cannot be completed for reasons not in control of bookTou.</li> 
        </ul>
</br>

    <p class="x-biggest">
      6.3. You shall only be able to claim refunds for transactions initiated by You only if You have already pre-paid the fees with respect to such transaction. Subject to relevant Merchant’s refund policy and in accordance therein, You shall be eligible to get the refund in the following circumstances:
      </p>  
       <ul>
            <li>Your package has been tampered or damaged at the time of delivery, as determined by bookTou basis the parameters established by bookTou in its sole discretion.</li>
            <li>If the wrong Item has been delivered to You, which does not match with the Item for which You had initiated a transaction on the Platform.</li> 

            <li> bookTou has cancelled the order because of any reason mentioned under Para 6 (2) above.</li> 

             <li> All decisions with respect to refunds will be at the sole discretion of bookTou and in accordance with bookTou’s internal refund policy (Refund Metrix) and the same shall be final and binding. All refunds initiated by bookTou shall be refunded to the financial source account from which, You have initiated the transaction on the Platform.</li>

             
        </ul>

 
        <br/>

  <p class="x-biggest">
      7. Pick Up and Drop Off Services
      </p>  
       <ul>
            <li>  As a part of the bookTou Services, bookTou also gives You an option to avail the Pick Up and Drop Off Services being provided by the Delivery Partners.</li>
            <li> You can initiate a transaction on the Platform by which You may (through the help of a Delivery Partner) send packages at a particular location. The Pick Up and Drop Off Services are provided to You directly by the Delivery Partner and bookTou merely acts as a technology platform to facilitate transactions initiated on the Platform and bookTou does not assume any responsibility or liability for any form of deficiency of services on part of the Delivery Partner.</li> 

            <li>  Upon initiation of a request for Pick Up and Drop Off Services on the Platform, depending upon the availability of Delivery Partner around Your area, bookTou will assign a Delivery Partner to You. The Delivery Partner shall pick up the Item from a location designated by You on the Platform and drop off the Items at a particular location designated by You. While performing the Pick Up and Drop off Services, the Delivery Partner shall act as an agent of You and shall act in accordance with Your instructions. You agree and acknowledge that the pick-up location and the drop off location has been added by You voluntarily and such information will be used for the bookTou Services and shall be handled by bookTou in accordance with the terms of its Privacy Policy.</li> 

             <li> You agree that You shall not request for a Pick Up and Drop Off Services for Items which are illegal, hazardous, dangerous, or otherwise restricted or constitute Items that are prohibited by any statute or law or regulation or the provisions of this Terms of Use.</li>

               <li> You are also aware that the Delivery Partner may choose to perform the Pick Up and Delivery Services requested by You.</li>
            <li>vi. You also agree that, upon becoming aware of the commission any offence by You or Your intention to commit any offence upon initiating a Pick-up and Drop-off Service or during a Pick-up and Drop-off service of any Item(s) restricted under applicable law, the Delivery Partner may report such information to bookTou or to the law enforcement authorities.</li> 
 
        </ul>



 <br/><p class="x-biggest">8. bookTou Cash, Google Pay Offer, Paytm Offer and Amazon Pay Offer shall hereinafter be referred to as "Offer".</p>

<br/><p class="x-biggest">
9. You hereby agree and acknowledge that the Offers are being extended by bookTou at its sole independent discretion and nothing shall entitle You to any of the Offers. You shall read the terms and conditions of the Offers carefully before availing them.</p>

   <br/>
        <h3>PART C: GENERAL TERMS OF USE</h3>

 
        <br/><p class="x-biggest"><strong>10. Eligibility to Use a.</strong></p> 
        <p class="x-biggest">The bookTou Services are not available to minors i.e. persons under the age of 18 (eighteen) years or to any Users suspended or removed by bookTou from accessing the Platform for any reason whatsoever. You represent that You are of legal age to form a binding contract and are not a person barred from receiving using or availing bookTou Services under the applicable laws.
        b. bookTou reserves the right to refuse access to the Platform, at any time to new Users or to terminate or suspend access granted to existing Users at any time without according any reasons for doing so.
        c. You shall not have more than 1 (one) active Account (as defined below) on the Platform. Additionally, You are prohibited from selling, trading, or otherwise transferring Your Account to another party or impersonating any other person for the purposing of creating an account with the Platform.
</p>

             <br/><p class="x-biggest"><strong>11. User Account, Password, and Security</strong></p>

 <ul>   <p class="x-biggest">
            <li>(a).In order to use the Platform and avail the bookTou Services, You will have to register on the Platform and create an account with a unique user identity and password ("Account"). If You are using the Platform on a compatible mobile or tablet, You will have to install the application and then proceed with registration.</li>
            <li>(b).You will be responsible for maintaining the confidentiality of the Account information, and are fully responsible for all activities that occur under Your Account. You agree to (i) immediately notify bookTou of any unauthorized use of Your Account information or any other breach of security, and (ii) [ensure that You exit from Your Account at the end of each session.] bookTou cannot and will not be liable for any loss or damage arising from Your failure to comply with this provision. You may be held liable for losses incurred by bookTou or any other User of or visitor to the Platform due to authorized or unauthorized use of Your Account as a result of Your failure in keeping Your Account information secure and confidential. Use of another User’s Account information for using the Platform is expressly prohibited.</li> 

          
        </ul>  
      </p>


                <br/><p class="x-biggest"><strong>12. Representations and Warranties</strong>  </p>
              </br>

<p class="x-biggest">12.1. Subject to compliance with the Terms of Use, bookTou grants You a non-exclusive, limited privilege to access and use this Platform and the bookTou Services.</p>

<p class="x-biggest"> 12.2. You agree to use the Platform only:   

 <ul>   <p class="x-biggest">
            <li> for purposes that are permitted by this Terms of Use; and</li>
            <li> in accordance with any applicable law, regulation or generally accepted practices or guidelines. You agree not to engage in activities that may adversely affect the use of the Platform by bookTou or Delivery Partner(s) or Merchants or other Users.</li> 
</p> 
          
        </ul>
</p>
 
                 <p class="x-biggest">     
    
       12.3. You represent and warrant that You have not received any notice from any third party or any governmental authority and no litigation is pending against You in any court of law, which prevents You from accessing the Platform and/or availing the bookTou Services.
     </p>

      <p class="x-biggest">  
      12.4. You represent and warrant that You are legally authorized to view and access the Platform and avail the bookTou Services.</p>

            <p class="x-biggest">  
        12.5. You agree not to access (or attempt to access) the Platform by any means other than through the interface that is provided by bookTou. You shall not use any deep-link, robot, spider or other automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any portion of the Platform, or in any way reproduce or circumvent the navigational structure or presentation of the Platform, materials or any bookTou Property, to obtain or attempt to obtain any materials, documents or information through any means not specifically made available through the Platform.</p>

              <p class="x-biggest">  
        12.6. You acknowledge and agree that by accessing or using the Platform, You may be exposed to content from others that You may consider offensive, indecent or otherwise objectionable. bookTou disclaims all liabilities arising in relation to such offensive content on the Platform.</p>

              <p class="x-biggest">  
      12.7. Further, You undertake not to:
      </p>

       <ul>   <p class="x-biggest">
            <li>defame, abuse, harass, threaten or otherwise violate the legal rights of others;</li>
            <li>publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, infringing, disparaging, ethnically objectionable, obscene, indecent or unlawful topic, name, material or information;</li> 

             <li> do any such thing that may harms minors in any way;Ø  copy, republish, post, display, translate, transmit, reproduce or distribute any bookTou Property through any medium without obtaining the necessary authorization from bookTou;</li>
             <li>conduct or forward surveys, contests, pyramid schemes or chain letters;</li>
              <li>upload or distribute files that contain software or other material protected by applicable intellectual property laws unless You own or control the rights thereto or have received all necessary consents;</li>

                <li>upload or distribute files or documents or videos (whether live or pre-recorded) that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of the Platform or another's computer;</li>
                 <li>engage in any activity that interferes with or disrupts access to the Platform (or the servers and networks which are connected to the Platform);</li>
                    <li>attempt to gain unauthorized access to any portion or feature of the Platform, any other systems or networks connected to the Platform, to any bookTou server, or through the Platform, by hacking, password mining or any other illegitimate means;</li>

                     <li>probe, scan or test the vulnerability of the Platform or any network connected to the Platform, nor breach the security or authentication measures on the Platform or any network connected to the Platform. You may not reverse look-up, trace or seek to trace any information on any other User, of or visitor to, the Platform, to its source, or exploit the Platform or information made available or offered by or through the Platform, in any way whether or not the purpose is to reveal any information, including but not limited to personal identification information, other than Your own information, as provided on the Platform;</li>

                      <li>disrupt or interfere with the security of, or otherwise cause harm to, the Platform, systems resources, accounts, passwords, servers or networks connected to or accessible through the Platform or any affiliated or linked sites;</li>
                      <li>collect or store data about other Users, Merchants, Delivery Partner in connection with the prohibited conduct and activities set forth herein;</li>

                      <li>use any device or software to interfere or attempt to interfere with the proper working of the Platform or any transaction being conducted on the Platform, or with any other person’s use of the Platform;</li>

                        <li>use the Platform or any material or bookTou Property for any purpose that is unlawful or prohibited by these Terms of Use, or to solicit the performance of any illegal activity or other activity which infringes the rights of the Company or other third parties;</li>

                         <li>falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded;</li>

                         <li>impersonate any other User, Merchant, Delivery Partner or person;</li>

                          <li> violate any applicable laws or regulations for the time being in force within or outside India or anyone’s right to privacy or personality;</li>

                           <li> violate the Terms of Use contained herein or elsewhere;</li>

                             <li>threatens the unity, integrity, defence, security or sovereignty of India, friendly relation with foreign states, or public order or causes incitement to the commission of any cognisable offence or prevents investigation of any offence or is insulting for any other nation; and Ø  reverse engineer, modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell any information or software obtained from the Platform.</li>
              </p> 
          
        </ul>

    <p class="x-biggest">
            12.8. You agree and acknowledge that the use of the bookTou Services offered by bookTou is at Your sole risk and that bookTou disclaims all representations and warranties of any kind, whether express or implied as to condition, suitability, quality, merchantability and fitness for any purposes are excluded to the fullest extent permitted by law.
</p> 


   <p class="x-biggest">
         12.9. Without prejudice to the above, bookTou makes no representation or warranty that the bookTou Services will meet Your requirements.
</p> 
  <p class="x-biggest">
       12.10. All materials/content on our Platform (except any third party content available on the Platform), including, without limitation, names, logos, trademarks, images, text, columns, graphics, videos, photographs, illustrations, artwork, software and other elements (collectively, "Material") are protected by copyrights, trademarks and/or other intellectual property rights owned and controlled by bookTou. You acknowledge and agree that the Material is made available for limited, non-commercial, personal use only. Except as specifically provided herein or elsewhere in our Platform, no Material may be copied, reproduced, republished, sold, downloaded, posted, transmitted, or distributed in any way, or otherwise used for any purpose other than the purposes stated under this Terms of Use, by any person or entity, without bookTou ‘s prior express written permission. You may not add, delete, distort, or otherwise modify the Material. Any unauthorized attempt to modify any Material, to defeat or circumvent any security features, or to utilize our Platform or any part of the Material for any purpose other than its intended purposes is strictly prohibited. Subject to the above restrictions under this Clause, bookTou hereby grants You a non-exclusive, freely revocable (upon notice from bookTou), non-transferable access to view the Material on the Platform.
</p> 
      
         
          <br/>
        <h3>8. Contact us</h3>
        <p class="x-biggest">If you have any inquiries or requests in respect of your Personal Data or our Privacy and Data Protection Policy, you may contact us at:</p>
        <p class="x-biggest">Ezanvel Office<br/>
        Ezanvel Solutions private Ltd.</br>
Minuthong-Khuyathong Road,</br>
Opposite Kekrupat</br>
Imphal West</br>
795001<br/>
Email: <a href='mailto:booktougi@gmail.com' target='_blank'>booktougi@gmail.com</a><br/>
Phone: +91-986-308-6093<br/>
</p>
 
     </ul>
       
  </div>

 
</div>
</div>
</section> 

 
 
@endsection 
 