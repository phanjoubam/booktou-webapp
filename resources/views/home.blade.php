@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>



<div data-kt-menu-trigger="click" data-kt-menu-placement="right-start" class="menu-item py-3">
						<span class="menu-link menu-center" title="Client Pages" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon me-0">
								<i class="bi bi-people fs-2"></i>
							</span>
						</span>
						
						<div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
						  

						 

							<div class="menu-item">
								<div class="menu-content">
									<span class="menu-section fs-5 fw-bolder ps-1 py-1">Accounts</span>
								</div>
							</div>
							<div class="menu-item">
								<a class="menu-link" href="<?php echo $host . "/list-clients"; ?>">
									<span class="menu-bullet">
										<span class="bullet bullet-dot"></span>
									</span>
									<span class="menu-title">Clients</span>
								</a>
							</div>
						</div>
					</div>

					
							
@endsection
