@extends('layouts.hollow_template_01')
@section('content')
 
<?php 
   $cdn_url =    config('app.app_cdn') ;  
   $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>

<section style="background-image: url(https://booktou.in/public/assets/image/event/event.jpg); background-size:cover ;"  class="p-5">

<div class='outer-top-tbm'> 
<div class="container mt-5">

<form method="get" action="{{action('EventTicketingController@phoneVerification')}}">
        {{csrf_field()}}
<div class="row">

     <div class="col-lg-12 mb-4 mb-lg-0">
            <h3 class="display-4 text-center white" style="padding-top:50px">Verify Your Identity</h3>
            </div>



   <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3" style="padding-top:50px">
                    <div class="card"> 
                            <div class="card-body">
                           
                            <div class="cart-page-heading mb-30">
                            <h5 class="">Please provide your phone number given at the time of buying ticket.</h5>
                            <hr/>
                                @if(isset($err_msg))
                                 <p class="alert-warning p-3">{{$err_msg}}</p>
                                @endif
                                    
                                
<div class="row g-3">
                                <div class="col-auto">
    <label for="staticEmail2" class="visually-hidden">Phone Number</label>
    <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Phone number:">
  </div>
  <div class="col-auto">
    <label for="inputPassword2" class="visually-hidden">Phone number</label>
    <input type="text" class="form-control" name="phoneno" id="inputPassword2" placeholder="Phone number">
  </div>
  <div class="col-auto">
    <button type="submit" class="btn btn-primary mb-3" id="btn_continue" value="verify" name="btn_continue">Confirm identity</button>
  </div>

  </div>


  

                        </div>

                         
                    </div>
                    </div>
                </div>
</div>
</form>


  

</div>
</div>

</section> 
 @endsection