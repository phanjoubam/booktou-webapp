@extends('layouts.admin_theme_02')
@section('content')

<div class="row" >
  <div class="col-md-12"> 


 <div class="card mb-6">
   <div class="p-2"> 
        <div class="row">
                <div class="col-md-6">
                     
                      <form   method="get" action="{{  action('EventTicketingController@sendWhatsappInvitationMessage') }}">
                        

                        Total message sent today - {{ $total_sent }} <button type="submit" name="submit" value="sent" class="btn btn-info btn-rounded" href="{{ URL::to('/admin/customer-care/orders/view-all') }}">Sent Next 100 Message</button>
                      </form> 
                </div>
           
 </div> 
      </div>         
 </div>


                   
                               @if( count($profiles ) > 0 ) 
                               <div class='card mt-4 '>
                               <div class="card-body">
                               <table class="table table-striped"  >
                                    <thead>
                                        <tr>  
                                            <th width='100px'>WMSG #</th>
                                            <th width='100px'>Phone</th>
                                            <th width='200px' class='text-center'>Status</th>
                                        </tr>
                                    </thead>
                                   
                     
                               @foreach($profiles as $profile) 
                       
                        
                                        <tr  > 
                                          <td class="text-left" width='100px'>{{$profile->id }}</td>
                                          <td width='100px' >{{ $profile->phone }}</td> 
                                            <td width='100px' ><span class='badge badge-success badge-pill'>Sent</span></td> 

                                        </tr>

                            @endforeach
                                        </tbody>
                                </table>  
                         {{ $profiles->links() }}

                            </div>
                                 </div>

                                 @else 
                                  <div class='card mt-4'>
                               <div class="card-body">
                                <p class='alert alert-info mt-6'>No whatsapp message sent yet.</p>
                              </div>
                            </div>
                                @endif 

                           
                   
     </div>
 
 </div>
  @endsection