@extends('layouts.hollow_template_01')
@section('content')

<style type="text/css">
 

        .app-button{

         
        border-radius: 10px;
       }
 
       .app-button span{

        font-size: 18px;
       } 

       .buttons{

        display: grid;
       }
       .logo-images
       {   
         
        border-radius: 10px;
        
       }
         
    .mobile
    {
      border-radius: 10px;
      width: 70%; 
    }
</style>

<div class="container mt-5">
        <div class="row pt-xl-2 pb-md-3 ">
          <div class="col-lg-12 mb-4 mb-lg-0 text-center white" style="padding-top: 60px">
            <h2 class="display-4 text-center white" style="padding-top:30px">Events and Tickets</h2>
            <p>Your event entry tickets are avaialble for download.</p>
          </div>
<div class="col-lg-12 mb-4 mb-lg-0">

<div class="bg-gray-2000 space-1 mt-4" >
 <div class="row  height d-flex justify-content-center align-items-center">
 
 @foreach($ticket_files as $ticket_file)


    <div class="col-md-2 text-center" >

      @if(strcasecmp($ticket_file->ticket_format, "free") == 0)
        <img class="" width="100%" src="{{URL::to('public/assets/image/event/free-pass-blank.jpg')}}" >

        <div class='mt-2'></div>

      <a  target="_blank" href="{{ URL::to('/events/ticketing/download-ticket') }}?purchase={{ Crypt::encrypt( $ticket_file->purchase_id ) }}&slno={{ $ticket_file->id }}" class='btn btn-info white btn-rounded btn-sm'>Download Free Pass Ticket</a>

  


      @else

      @if( date('Y-m-d', strtotime($ticket_file->valid_date)) ==   date('Y-m-d', strtotime( '2023-2-3' )) )
        <img class="" width="100%" src="{{URL::to('public/assets/image/event/first-day-blank.jpg')}}" >

        <div class='mt-2'></div>

      <a  target="_blank" href="{{ URL::to('/events/ticketing/download-ticket') }}?purchase={{ Crypt::encrypt( $ticket_file->purchase_id ) }}&slno={{ $ticket_file->id }}" class='btn btn-success btn-rounded btn-sm'>Download Day 1 Ticket</a>

    

      @else
        <img class="" width="100%" src="{{URL::to('public/assets/image/event/second-day-blank.jpg')}}" >

        <div class='mt-2'></div>

        <a  target="_blank" href="{{ URL::to('/events/ticketing/download-ticket') }}?purchase={{ Crypt::encrypt( $ticket_file->purchase_id ) }}&slno={{ $ticket_file->id }}" class='btn btn-success btn-rounded btn-sm'>Download Day 2 Ticket</a>

       

      @endif
     
     @endif

       </div>
     

  @endforeach

<div class="col-md-12 text-center mt-2 p-4" >

  <p class='white'>Looking to buy more tickets? Click <a target='_blank' href="{{ URL::to('/events/ticketing/ticket-sale-form') }}">here</a></p>

</div>
  </div>
  </div>
</div>
</div>
</div>
@endsection
@section("script")
 <script src="{{URL::to('public/portal/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>

  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper', {
      slidesPerView: 3,
      direction: getDirection(),
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      on: {
        resize: function () {
          swiper.changeDirection(getDirection());
        },
      },
    });

    function getDirection() {
      var windowWidth = window.innerWidth;
      var direction = window.innerWidth <= 360 ? 'vertical' : 'horizontal';

      return direction;
    }
  </script>
  @endsection