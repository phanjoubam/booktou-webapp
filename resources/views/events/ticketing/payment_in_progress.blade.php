@extends('layouts.hollow_template_01')
@section('content')
<div class="bg-gray-2000 space-1">
 <div class="row  height d-flex justify-content-center align-items-center">
 
  <div class="col-md-12 text-center" >
    <img class="" width="320px" src="{{URL::to('/mega-event/img/mobile.jpg')}}" >
  </div>  
  </div>

</div>
@endsection 
 
@section('script')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
 
<script>
  

var options = {
    "key": "<?php echo  $data['key'] ?>", 
    "amount":  "<?php echo $data['amount'] ?>", 
    "currency": "<?php echo $data['currency'] ?>", 
    "name": "<?php echo $data['name'] ?>", 
    "description":  "bookTouX Film Festival Ticket"   ,
    "image": "<?php echo $data['image'] ?>", 
    "order_id": "<?php echo $data['order_id'] ?>", 
    "callback_url": "<?php echo $data['callback_url'] ?>?rzid=<?php echo $data['order_id'] ?>" , 
    "redirect" :  true ,
    "prefill": {
        "name": "<?php echo $data['prefill']['name'] ?>", 
        "email":  "<?php echo $data['prefill']['email'] ?>", 
        "contact":  "<?php echo $data['prefill']['contact'] ?>", 
    },
    "notes": {
        "item": "<?php echo $data['notes']['item'] ?>", 
        "price": "<?php echo $data['notes']['price'] ?>", 
       
    }

    };
    
    var rzp1 = new Razorpay(options); 
    rzp1.open();
    e.preventDefault();
</script>


@endsection 