@extends('layouts.hollow_template_01')
@section('content')
<section style="background-image: url(https://booktou.in/public/assets/image/event/event.jpg); background-size:cover; min-height: 750px"  class="p-5">
 
<form action="{{ action('EventTicketingController@makeTicketPurchase') }}" method="post">
    {{csrf_field()}}
    <div class="container mt-5">
        <div class="row pt-xl-2 pb-md-3 ">
          <div class="col-lg-12 mb-4 mb-lg-0">
            <h2 class="display-3 text-center white" style="padding-top:30px">Events and Ticket Prices</h2>
            </div>

 
            
          <div class="col-lg-8 col-md-8 offset-md-2 position-relative">
            
           
<div class="card mt-3">
  <div class="card-header">
    <h4>All Your Recent Purchases</h4>
  </div>
  <div class="card-body">

 
<ol class="list-group list-group-numbered">

    @foreach($purchases as $purchase)

    <li class="list-group-item d-flex justify-content-between align-items-start">
    <div class="ms-2 me-auto">
      <div class="fw-bold">Order # {{ $purchase->id }}</div>
    </div>
    <span class="badge bg-primary rounded-pill">
        <a target='_blank' href="{{ URL::to('/events/ticketing/view-ticket') }}?purchase={{ Crypt::encrypt( $purchase->id  )  }}">View Tickets</a>
    </span>
  </li>  

    @endforeach

    </ol>
 


  </div>

 
</div>




<div class="card mt-3">

  <div class="card-body text-center">
     
     
<h4>Looking to buy new tickets?

        <a href="{{ URL::to('/events/ticketing/ticket-sale-form') }}" target="_blank">Click here.</a>
    </h4>
  </div>

  
</div>



 
          </div>
        </div>
    </div>  
</form>

</section> 
 @endsection


@section('script')
 
  <style>

.btn-group {
    margin-right: 10px !important;
}


    .btn span.fa-check {    			
	opacity: 0 !important;
}
.btn.active span.fa-check  {				
	opacity: 1 !important;
}

[data-toggle=buttons]>.btn>input[type=radio], [data-toggle=buttons]>.btn>input[type=checkbox] 
{
    position: absolute !important;
    z-index: -1 !important;
    filter: alpha(opacity=0) !important;
    opacity: 0 !important;
}
 
.btn-gray
{
    color: #000 !important;
    background-color: #d5d5d5 !important;
    border-color: #777878 !important;
}


.btn-gray.active
{
    color: #fff;
    background-color: #31a81e !important;
    border-color: #259820 !important;
} 
 
   

</style>

<script>

 


$(".checktickettype").on("change", function(){
    
    var totalCost = 0;
    var selectCount =   0;
    $(".checktickettype").each(function() 
    {
        if( $(this).is(":checked") === true)
        {
            totalCost = parseFloat( totalCost ) +   parseFloat( $(this).attr("data-cost")  );
            selectCount++;
            $(this).parent().addClass("active");
        }
        else
        {
            $(this).parent().removeClass("active");
        }

    });

    $("#subamount").html( totalCost );

    if (  selectCount  >= 1) 
    {
        $("#btnbooknow").removeAttr("disabled" );
    }
    else
    {
        $("#btnbooknow").attr("disabled", true);
    }

});
 

    
</script>

@endsection
