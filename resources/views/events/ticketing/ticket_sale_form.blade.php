@extends('layouts.hollow_template_01')
@section('content')
<section style="background-image: url(https://booktou.in/public/assets/image/event/event.jpg); background-size:cover"  class="p-5">
 
<form action="{{ action('EventTicketingController@makeTicketPurchase') }}" method="post">
    {{csrf_field()}}
    <div class="container mt-5">
        <div class="row pt-xl-2 pb-md-3 ">
          <div class="col-lg-12 mb-4 mb-lg-0">
            <h2 class="display-3 text-center white" style="padding-top:30px">Events and Ticket Prices</h2>
            </div>


        <div class="col-lg-5  ">  
            <!-- Pricing -->
            <ul class="list-group  mt-3">
              <li class="list-group-item d-flex flex-column flex-sm-row align-items-center justify-content-between p-4">
              <div class="col-sm-12">
                      <h5>Main Event Feb 3rd & 4th</h5>
                      <p class="mb-4">
                        Venue: Chandrakirti Auditorium & Art Gallery, Dept of Arts & Culture<br/>
                        Red Carpet, Photo session with Celebrities, Film Screening from all Northeast States, Prize Distribution, Master Class, Meet the Director and more
                      </p>
                      <div class="d-flex align-items-center" style="padding-left:20px">

                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-gray btn-sm active btntickettype">
                                <input type="checkbox" autocomplete="off" checked id="free" name="tickettype[]" value='free' data-cost="0" class="checktickettype" >
                                <span class="fa fa-check "></span>
                            </label>
                        </div>
 
                      <h4 class="fs-base fw-semibold text-nowrap ps-1 mb-0">Free Entry</h4>
                      </div>
            </div>
              </li>
              <li class="list-group-item d-flex flex-column flex-sm-row align-items-center justify-content-between p-4">
              <div class="col-sm-12">
                      <h5>Day 1 - Feb 3rd</h5>
                      <p class="mb-4">
                        Venue: Hapta Kangjeibung<br/>
                        Multi-specialty stalls, Extreme Sports, Music Concert with the best bands and more
                      </p>
                      <div class="d-flex align-items-center" style="padding-left:20px">

                      <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-gray btn-sm btntickettype">
                                <input type="checkbox" autocomplete="off" id="firstday" name="tickettype[]" value='first-day'  data-cost="100" class="checktickettype" >
                                <span class="fa fa-check "></span>
                            </label>
                        </div>
                      
                      <h5 class="fs-base fw-semibold text-nowrap ps-1 mb-0">₹100 per person for early birds  

                      <svg class="flex-shrink-0 me-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M23.794 1.71278C23.5195 1.43819 23.0743 1.43819 22.7997 1.71278L21.4284 3.08406C20.6078 2.1197 19.3859 1.50686 18.0235 1.50686C16.8653 1.50686 15.7764 1.95789 14.9577 2.77675L0.205961 17.5284C0.00486702 17.7295 -0.0552736 18.0319 0.0535233 18.2947C0.162367 18.5574 0.418726 18.7287 0.703117 18.7287H9.44934L11.8087 21.0881H11.2469C10.8585 21.0881 10.5437 21.4029 10.5437 21.7912C10.5437 22.1795 10.8585 22.4943 11.2469 22.4943H15.7656C16.1539 22.4943 16.4687 22.1795 16.4687 21.7912C16.4687 21.4029 16.1539 21.0881 15.7656 21.0881H13.7975L11.4381 18.7287H14.2594C18.7998 18.7287 22.4937 15.0348 22.4937 10.4943V5.97709C22.4937 5.39355 22.3812 4.83578 22.1769 4.32428L23.794 2.70714C24.0687 2.43255 24.0687 1.98737 23.794 1.71278ZM2.4006 17.3224L13.5562 6.1668V8.98806C13.5562 13.5836 9.81745 17.3224 5.22187 17.3224H2.4006ZM21.0875 10.4943C21.0875 14.2594 18.0244 17.3225 14.2594 17.3225H10.2592C13.0761 15.6134 14.9625 12.5168 14.9625 8.98811V6.15995C14.9625 5.25752 15.3139 4.40913 15.952 3.77116C16.5052 3.21784 17.2409 2.91316 18.0235 2.91316C19.713 2.91316 21.0875 4.28767 21.0875 5.97719L21.0875 10.4943Z" fill="currentColor"></path><path d="M18.7781 5.97526C19.1664 5.97526 19.4812 5.66046 19.4812 5.27213C19.4812 4.88381 19.1664 4.56901 18.7781 4.56901C18.3898 4.56901 18.075 4.88381 18.075 5.27213C18.075 5.66046 18.3898 5.97526 18.7781 5.97526Z" fill="currentColor"></path>
                  </svg></h5>

                      </div>
            </div> 
              </li>

              <li class="list-group-item d-flex flex-column flex-sm-row align-items-center justify-content-between p-4">

              <div class="col-sm-12">
                      <h5>Day 2 - Feb 4th</h5>
                      <p class="mb-4">
                      Venue: Hapta Kangjeibung<br/>
                      Multi-specialty stalls, Cosplay, Kite Festival, Music Concert with the best bands and more
                      </p>
                      <div class="d-flex align-items-center" style="padding-left:20px">

                      <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-gray btn-sm btntickettype">
                                <input type="checkbox" autocomplete="off" id="secondday" name="tickettype[]" value='second-day'  data-cost="100" class="checktickettype"  >
                                <span class="fa fa-check "></span>
                            </label>
                        </div> 
                      <h5 class="fs-base fw-semibold text-nowrap ps-1 mb-0">₹100 per person for early birds 
                      <svg class="flex-shrink-0 me-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M23.794 1.71278C23.5195 1.43819 23.0743 1.43819 22.7997 1.71278L21.4284 3.08406C20.6078 2.1197 19.3859 1.50686 18.0235 1.50686C16.8653 1.50686 15.7764 1.95789 14.9577 2.77675L0.205961 17.5284C0.00486702 17.7295 -0.0552736 18.0319 0.0535233 18.2947C0.162367 18.5574 0.418726 18.7287 0.703117 18.7287H9.44934L11.8087 21.0881H11.2469C10.8585 21.0881 10.5437 21.4029 10.5437 21.7912C10.5437 22.1795 10.8585 22.4943 11.2469 22.4943H15.7656C16.1539 22.4943 16.4687 22.1795 16.4687 21.7912C16.4687 21.4029 16.1539 21.0881 15.7656 21.0881H13.7975L11.4381 18.7287H14.2594C18.7998 18.7287 22.4937 15.0348 22.4937 10.4943V5.97709C22.4937 5.39355 22.3812 4.83578 22.1769 4.32428L23.794 2.70714C24.0687 2.43255 24.0687 1.98737 23.794 1.71278ZM2.4006 17.3224L13.5562 6.1668V8.98806C13.5562 13.5836 9.81745 17.3224 5.22187 17.3224H2.4006ZM21.0875 10.4943C21.0875 14.2594 18.0244 17.3225 14.2594 17.3225H10.2592C13.0761 15.6134 14.9625 12.5168 14.9625 8.98811V6.15995C14.9625 5.25752 15.3139 4.40913 15.952 3.77116C16.5052 3.21784 17.2409 2.91316 18.0235 2.91316C19.713 2.91316 21.0875 4.28767 21.0875 5.97719L21.0875 10.4943Z" fill="currentColor"></path><path d="M18.7781 5.97526C19.1664 5.97526 19.4812 5.66046 19.4812 5.27213C19.4812 4.88381 19.1664 4.56901 18.7781 4.56901C18.3898 4.56901 18.075 4.88381 18.075 5.27213C18.075 5.66046 18.3898 5.97526 18.7781 5.97526Z" fill="currentColor"></path>
                  </svg> </h5>
                      </div>
            </div> 
 
              </li>
            </ul>
          </div>
          <div class="col-xl-6 col-lg-7 offset-xl-1 position-relative">
            
          


<div class="card mt-3">
  <div class="card-header">
    <h4>Provide Visitor Information</h4>
  </div>
  <div class="card-body">

    <div class="form-group">
    <label for="cname">Visitor Full Name:</label>
    <input type="text" class="form-control" required id="cname" name="cname" value="">
  </div>
 

<div class="form-row">
  <div class="col-md-12 mt-2">
    <label for="cphone">Phone Number: </label>
    <input type="number" required class="form-control" id="cphone" name="cphone"   value=""  maxlength="12"
    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
    type = "number"
    maxlength = "6"
    >
    <small>Please provide a valid whatsapp number to receive your purchase tickets</small>
  </div>

   
 </div> 

  </div>

    <div class="card-footer text-right">
    
    <div class="d-flex mb-3">

          <div class="p-2 flex-fill"> 
            <strong>Pay total amount ₹ <label id="subamount">0.00</label></strong>
            <br/>
            <i class="fa fa-shield sgreen"></i> Payment is 100% safe and secure.

        </div>

            <div class="p-2 flex-fill-right">
        <button type="submit" id="btnbooknow" class="btn btn-primary btn-xss btn-rounded"   value="save" name="save">Book Ticket Now</button> 
        </div> 
      
    </div>

 

    </div>
</div>




<div class="card mt-3">

  <div class="card-body text-center">
     
     
<h4>Already purchased tickets?

        <a href="{{ URL::to('/events/ticketing/phone-verification') }}" target="_blank">Click to download.</a>
    </h4>
  </div>

  
</div>



 
          </div>
        </div>
    </div>  
</form>

</section> 
 @endsection


@section('script')
 
  <style>

.btn-group {
    margin-right: 10px !important;
}


    .btn span.fa-check {    			
	opacity: 0 !important;
}
.btn.active span.fa-check  {				
	opacity: 1 !important;
}

[data-toggle=buttons]>.btn>input[type=radio], [data-toggle=buttons]>.btn>input[type=checkbox] 
{
    position: absolute !important;
    z-index: -1 !important;
    filter: alpha(opacity=0) !important;
    opacity: 0 !important;
}
 
.btn-gray
{
    color: #000 !important;
    background-color: #d5d5d5 !important;
    border-color: #777878 !important;
}


.btn-gray.active
{
    color: #fff;
    background-color: #31a81e !important;
    border-color: #259820 !important;
} 
 
   

</style>

<script>

 


$(".checktickettype").on("change", function(){
    
    var totalCost = 0;
    var selectCount =   0;
    $(".checktickettype").each(function() 
    {
        if( $(this).is(":checked") === true)
        {
            totalCost = parseFloat( totalCost ) +   parseFloat( $(this).attr("data-cost")  );
            selectCount++;
            $(this).parent().addClass("active");
        }
        else
        {
            $(this).parent().removeClass("active");
        }

    });

    $("#subamount").html( totalCost );

    if (  selectCount  >= 1) 
    {
        $("#btnbooknow").removeAttr("disabled" );
    }
    else
    {
        $("#btnbooknow").attr("disabled", true);
    }

});
 

    
</script>

@endsection
