@extends('layouts.hollow_template_01')
@section('content')
 
<?php 
   $cdn_url =    config('app.app_cdn') ;  
   $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>

<section style="background-image: url(https://booktou.in/public/assets/image/event/event.jpg); background-size:cover ;"  class="p-5">

<div class='outer-top-tbm'> 
<div class="container">
    <form method="post" action=" {{action('EventTicketingController@otpVerification')}}">
    	{{csrf_field()}}
<div class="row">
       <div class="col-lg-12 mb-4 mb-lg-0">
            <h3 class="display-4 text-center white" style="padding-top:50px">Verify OTP &amp; Download Ticket</h3>
            </div>

   <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3" style="padding-top:50px">
                    <div class="card"> 
                            <div class="card-body">

                            <div class="cart-page-heading mb-30">
                            <h5 class="">Please provide the OTP received in your phone to verify your identity and start downloading tickets.</h5>
                            <hr/>
                            
							@if(isset($err_msg))
                                 <p class="alert-warning p-3">{{$err_msg}}</p>
                            @endif

<div class="row g-3">
                                <div class="col-auto">
    <label for="staticEmail2" class="visually-hidden">Phone Number</label>
    <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Phone number:">
  </div>
  <div class="col-auto">
    <label for="inputPassword2" class="visually-hidden">Phone number</label>
    <input type="text" readonly class="form-control" name="phoneno" id="inputPassword2" value="{{ Request::get('phoneno')  }}">
  </div>

  </div>


<div class="row g-3 mt-1">
                                <div class="col-auto">
    <label for="staticotp" class="visually-hidden">OTP</label>
    <input type="text" readonly class="form-control-plaintext" id="staticotp" value="OTP:">
  </div>
  <div class="col-auto">
    <label for="otp" class="visually-hidden">OTP</label>
    <input type="text" class="form-control" name="otp" id="otp" placeholder="OTP">
  </div>
  <div class="col-auto">
   
    <button type="submit" class="btn btn-primary mb-3" id="submit" value="verify" name="submit">Continue</button>
  </div>

  </div>


   

                        </div>

                         
                    </div>
                    </div>
                </div>
</div>
</form>

 
</div>
</div> 

</section> 
 @endsection