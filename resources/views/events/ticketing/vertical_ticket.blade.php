@php
  $bg_url  =  '/assets/image/event/free-pass.jpg';
@endphp

@if(strcasecmp($ticket_format, "free") == 0)
  @php
    $bg_url  =  '/assets/image/event/free-pass.jpg';
  @endphp
@else
  @if( date('Y-m-d', strtotime($valid_date)) ==   date('Y-m-d', strtotime( '2023-2-3' )) )
    @php
      $bg_url  =  '/assets/image/event/first-day.jpg';
    @endphp
  @else
    @php
      $bg_url  =  '/assets/image/event/second-day.jpg';
    @endphp
  @endif
@endif


<style>
@page {
   margin: 0 !important;
   padding:0 !important;
   size: 700px 1246px;
  }

  body
  {
    background-size:cover;
    background-image: url("data:image/png;base64, {{ base64_encode(file_get_contents( public_path( $bg_url ) )) }}");
  }

  thead {display: table-row-group;}


</style>
 

<img width="270px" height="270px" style="position: absolute; top: 226px; left: 232px" 
src="data:image/png;base64,{{ base64_encode(file_get_contents( '/var/www/html/stagingapi.booktou.in/public' . $qr_path  )) }}">




