@extends('layouts.store_front_no_heading')
@section('content') 

 <div class="breadcumb_area bg-img" style="background-image: url('{{ URL::to("/public/store/image/breadcrumb.jpg" ) }}') ;"  >
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Download POS Printer Driver</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> 



<section class="single_product_details_area d-flex align-items-center box-lg" >
<div class="container">
<div class="row">
  
  <div class="col-sm-12 col-md-4 col-lg-4 offset-md-1 offset-lg-1">
      
      <img src="{{ URL::to('/assets/images/thermal-printer.jpg') }}"   alt="Thermal Printer Download">


  </div>


<div class="col-sm-12 col-md-6 col-lg-6 ">  
 <h3 class="text-justify">XPrinter KIOSK/POS Printer</h3>  
 <div id="feature-bullets" class="a-section a-spacing-medium a-spacing-top-small">
<hr>
<p class="a-size-base-plus a-text-bold">
58mm Portable Rechargeable Thermal Printer Bluetooth + USB Interface
</p>
 
<ul class="a-unordered-list a-vertical a-spacing-mini">
 
<li><span class="a-list-item">
58mm Portable Rechargeable thermal Printer

</span></li>

<li><span class="a-list-item">
Supports mobile / computer printing

</span></li>

<li><span class="a-list-item">
Bluetooth + USB Interface

</span></li>

<li><span class="a-list-item">
Powerfull 2200 MAh battery for day long working on a single charge.

</span></li>

<li><span class="a-list-item">
6 Months Warranty

</span></li>

</ul>
 
<br/>
<a href="{{ URL::to('/public/driver/pos_printer_v7.zip') }}" target='_blank' style='display: inline-block;
    padding: 0 10px 0 0;
    border: 1px solid #09923d;'><img width='220px'  src="{{ URL::to('/assets/images/download.png') }}"   alt="Download"></a>

         
</div>
     

</div>
</div>
</div>
</section> 

 

<section class="single_product_details_area d-flex align-items-center box-lg" >
<div class="container">
<div class="row">
  
  <div class="col-sm-12 col-md-4 col-lg-4 offset-md-1 offset-lg-1">
      
      <img src="{{ URL::to('/assets/images/thermal-printer-hoin-58.jpg') }}"   alt="Thermal Printer Download">


  </div>


<div class="col-sm-12 col-md-6 col-lg-6 ">  
 <h3 class="text-justify">HOIN Kiosk Receipt/POS Bill Printer</h3>  
 <div id="feature-bullets" class="a-section a-spacing-medium a-spacing-top-small">
<hr>
<p class="a-size-base-plus a-text-bold">
HOIN BIS Certified 58MM Bluetooth + USB Thermal Printer
</p>
 
<ul class="a-unordered-list a-vertical a-spacing-mini">
 
<li><span class="a-list-item">
58mm Portable Rechargeable thermal Printer

</span></li>

<li><span class="a-list-item">Supports computer printing</span></li>
<li><span class="a-list-item">50 Km Print life Cycle</span></li>
<li><span class="a-list-item">The printer does not works with Mobile. Works only with computer/ laptop</span></li>
 
<li><span class="a-list-item">
Bluetooth + USB Interface

</span></li> 

<li><span class="a-list-item">
6 Months Warranty

</span></li>

</ul>
 
<br/>
<a href="{{ URL::to('/public/driver/h58-pos-driver.exe') }}" target='_blank' style='display: inline-block;
    padding: 0 10px 0 0;
    border: 1px solid #09923d;'><img width='220px'  src="{{ URL::to('/assets/images/download.png') }}"   alt="Download"></a>

         
</div>
     

</div>
</div>
</div>
</section> 


 
@endsection 
 