<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">  
<head>
    @include('admin.templates.head')
</head>

<body class="" style='background: #2f3e8f;'> 
   <div id="container"> 
   
    @yield('content')  
 
 </div> 

   @include('admin.templates.footer-scripts') 
   
   @yield('script') 
 </body> 
</html> 

