<!DOCTYPE html>
<html> 
	 <head>
		@include('store_front.template-02.head')

	  	
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-7TE5XV8H82"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-7TE5XV8H82');
		</script>

		@yield('style')

	 </head>
	 <body>

	 	

	 	<header class="section-header">
    <section class="border-bottom">
        <div class="container pt-2">
            <div class="d-flex flex-wrap">
                <div class="flex-fill p-2 brand-logo text-center float-start">
                   
                    <a href="{{ URL::to('/') }}">
                        <img width='150px' style='margin-top: 10px;' src="{{ URL::to('/public/store/image/logo.png') }}" alt="bookTou">
                    </a>
                </div>

               
<nav class="navbar navbar-expand-lg navbar-light bg-white d-none d-md-block mainnavbar"> 

                      <div id="navbarContent" class="collapse navbar-collapse ">
                        <ul class="navbar-nav nav-bubble mx-auto">
                         
                          <li class="nav-item parent "><a href="{{ URL::to('/') }}" class="nav-link font-weight-bold text-uppercase">Shopping</a></li>
                          <li class="nav-item parent "><a href="{{ URL::to('/booking') }}" class="nav-link font-weight-bold text-uppercase">Booking</a></li>
                          <li class="nav-item parent"><a href="{{ URL::to('/appointment') }}" class="nav-link font-weight-bold text-uppercase">Appointment</a></li>
                          

                      </ul>
                  </div>
              </nav>
  
 
    
</div>
</div> 
</section>

 
 
</header>

	@yield('content') 

	 	 @include('store_front.template-02.footer') 
 

	 	@include('store_front.template-02.footer-scripts') 
	 	@yield('script') 


 	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-623c32ddbb050c34"></script> 
 </body> 
</html> 