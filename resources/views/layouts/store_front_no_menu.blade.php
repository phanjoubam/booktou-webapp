<!DOCTYPE html>
<html> 
	 <head>
		@include('store_front.template-02.head_zoomable')

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-TNBF9WPMN6"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-TNBF9WPMN6');
		</script>
		
 
	 </head>
	 <body>
	 	@include('store_front.template-02.header_bar_hollow')
	 	
			 	@yield('content') 
		
	 	@include('store_front.template-02.footer') 

@if( session()->has('shopping_session') ) 
	<span style='display: none;height:0' id='gsid'>{{ Crypt::encrypt(   session()->get('shopping_session') ) }}</span>
@endif

@if( session()->has('__user_id_') ) 
	<span id='guid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__user_id_') )  }}</span>
@else
	<span style='display: none;height:0'  id='guid'>{{ Crypt::encrypt(  0 )  }}</span>
@endif
 

	 	@include('store_front.template-02.footer-scripts') 
	 	@yield('script')  
 
 
 </body> 
</html> 