<!DOCTYPE html>
<html> 
	 <head>
		@include('admin.templates.head')
	 </head>
	 <body>
 <div class="wrapper ">
    <div class="sidebar" data-color="orange">
    	@if( Session::get('_user_role_') == 100000  ) 
    		@include('admin.templates.side_bar_100000')
    	@elseif( Session::get('_user_role_') == 10000  ) 
    		@include('admin.templates.side_bar_10000')
    	@else 
    		@include('admin.templates.side_bar')
    	@endif    
	 </div>	
	  
	  <div class="main-panel" id="main-panel">
	  @include('admin.templates.nav_bar')  
		
		
		@yield('content') 
	  
	   
	
		</div>
	 
		
	  </div>
  </div>
  
   @include('admin.templates.footer') 
	 @include('admin.templates.footer-scripts') 
	 
	 @yield('script') 
 </body> 
</html> 