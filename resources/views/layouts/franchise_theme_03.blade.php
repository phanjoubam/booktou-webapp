<?php 

 
           $header = "franchise.template-02.head";
           $header_bar = "franchise.template-02.header_bar";
           $navbar = 'franchise.template-02.nav_bar';
           $topnavbar ='franchise.template-02.nav_top_bar';
           $footer='franchise.template-02.footer';
           $footer_scripts = 'franchise.template-02.footer-scripts';
         

?>
<!DOCTYPE html>
<html lang="en">
      <head>
            @include($header)
      </head> 
      <body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
                        
  @include($navbar)  
 
    <div id="content-wrapper" class="d-flex flex-column"> 
    <div id="content"> 
      @include($topnavbar)
      <div class="container-fluid">
        @include($header_bar)  
        @yield('content')  
      </div>

      </div>
      @include($footer) 
    </div> 
    </div>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
                         
 @include($footer_scripts)
 @yield('script')
</body>   
</html>
 

