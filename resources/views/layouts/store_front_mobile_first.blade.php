<!DOCTYPE html>
<html> 
	 <head>
		@include('store_front.templates.mobile_first_head')

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-TNBF9WPMN6"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-TNBF9WPMN6');
		</script>
		
	 </head>
	 <body>
	 	 <div class='container'>
	 	@yield('content') 

	 </div>
	 	@include('store_front.templates.footer-scripts') 
	 	@yield('script')
 

 </body> 
</html> 