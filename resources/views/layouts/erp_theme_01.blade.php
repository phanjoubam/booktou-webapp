<!DOCTYPE html>
<html lang="en">
  <head> 
    
		@include('erp.templates.head')
	 </head>
	 <body>
 
   	<div class="container-scroller">  
    	@include('erp.templates.nav_bar')  

    	<div class="container-fluid page-body-wrapper"> 

    	@include('erp.templates.side_bar')   
      <div class="main-panel">
          <div class="content-wrapper">

          	@include('erp.templates.header_bar')  

          @yield('content') 
      	</div>

      	@include('erp.templates.footer') 

  </div>
    </div>
 </div>


 	@include('erp.templates.footer-scripts') 
 	@yield('script') 



 </body> 
</html> 