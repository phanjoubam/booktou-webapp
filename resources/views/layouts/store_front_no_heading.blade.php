<!DOCTYPE html>
<html> 
	 <head>
		@include('store_front.templates.head')

		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-TNBF9WPMN6"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-TNBF9WPMN6');
		</script>

	 </head>
	 <body style='margin:0 !important; padding: 0 !important;'>
	 	 
	 	@yield('content')  
	 	@include('store_front.templates.footer-scripts') 
	  
 </body> 
</html> 