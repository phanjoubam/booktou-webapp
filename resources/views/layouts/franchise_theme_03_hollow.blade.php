<?php 

 
           $header = "franchise.template-02.head";
           $header_bar = "franchise.template-02.header_bar";
           $navbar = 'franchise.template-02.nav_bar'; 
           $footer='franchise.template-02.footer';
           $footer_scripts = 'franchise.template-02.footer-scripts';
         

?>
<!DOCTYPE html>
<html lang="en">
      <head>
            @include($header)
      </head> 
      <body id="page-top">

    <!-- Page Wrapper -->

    

        <!-- Sidebar --> 
 
    <div id="content-wrapper" class="d-flex flex-column">     
        
 
      
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow"> 
      <a class="navbar-brand " 
            href="{{  URL::to('/franchise') }}">
                <div class="sidebar-brand-icon ">
                    <img class="img-fluid"  src="{{ URL::to('/public/assets/image/logo.png') }}" style="width: 40px;" />
                </div>
               
            </a>


  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>


                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button> 
                    <div class="custom-control custom-switch">
                   <form style="margin-top: 10px;" action="{{ action('Franchise\FranchiseDashboardControllerV1@redirectToOrderDetailsView') }}">
                    {{ csrf_field() }}
                     <div class="form-group"> 
                     
                        <input type="search" class="form-control form-control-sm" placeholder="Specify order number" name='orderno'> 
                      </div>
                </form>
                    </div>
                    <ul class="navbar-nav ml-auto"> 
                        
                        <!-- Nav Item - Alerts --> 
                        <div class="topbar-divider d-none d-sm-block"></div> 
                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                                    Welcome , <?php echo session()->get('_full_name'); ?>
                                </span>
                    <img class="img-profile rounded-circle"  src="{{ URL::to('/public/assets/image/shop.png') }}">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown"> 
                                <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="{{ URL::to('/franchise/view-admin-profile') }}">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    My profile
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href='{{ URL::to("/logout") }}'>
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul> 
     </nav>



     <div class="container-fluid"> 


        @include($header_bar)  
        @yield('content')  
      </div>

      
      @include($footer) 
    </div> 
  
    

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
                         
 @include($footer_scripts)
 @yield('script')
</body>   
</html>
 

