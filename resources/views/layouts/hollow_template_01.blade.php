<!DOCTYPE html>
<html> 
	 <head>
	 	@include('store_front.template-02.head')
	 	<link rel="stylesheet" href="{{ URL::to('/public/store/theme-02') }}/css/booking.css">

		 

		<style>
body
{
	background-color: #000 !important;
}
.section-header
{
	position: fixed;
    width: 100%;
    background-color: #000 !important;
    opacity: .8 !important;
    border:none !important;
}
</style>


	 </head>
	 <body >

	 	@include('store_front.template-02.blank_header_bar')
	 	@yield('content')
	 
		@if( session()->has('shopping_session') ) 
			<span style='display: none;height:0' id='gsid'>{{ Crypt::encrypt(   session()->get('shopping_session') ) }}</span>
		@endif
		@if( session()->has('__user_id_') ) 
			<span id='guid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__user_id_') )  }}</span>
			<span id='gmid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__member_id_') )  }}</span>
		@else
			<span style='display: none;height:0'  id='guid'>{{ Crypt::encrypt( 0 )  }}</span>
			<span style='display: none;height:0'  id='gmid'>{{ Crypt::encrypt( -1 )  }}</span>
		@endif


	 	@include('store_front.template-02.footer-scripts')
	 	<script src="{{ URL::to('/public/store/js/services.js') }}?v={{ time() }}"></script>
	 	@yield('script')

	 	<style>

	 		.stickyfooter
	 		{
	 			border-top: 1px solid #efefef;
	 			position: fixed;
	 			right: 100px;
	 			bottom: 0;
	 			width: 520px;
	 			z-index: 16;
	 			border-top-right-radius: 25px;
	 			border-top-left-radius: 25px;
	 			padding-top: 10px;
	 		}

	 		.fixed-pane
	 		{
	 			position: fixed;
	 			top: 60px;
	 		}

	 		.filter-box {
	 			border: none;
	 			background-color: transparent;
	 			color: #595959;
	 			padding: 12px 16px;
	 			font-size: 16px;
	 			border-radius: 25px;
	 			cursor: pointer;
	 		}

	 		.filter-box img {
	 			width: 35px !important;
	 			display:inline-block !important;
	 			border-radius: 25px;
	 		}

	 		.display-8
	 		{
	 			font-size:1.45rem;
	 		}


.accordion-button:not(.collapsed) {
    color:  #343434 !important;
    background-color: transparent !important;
    box-shadow: inset 0 calc(var(--bs-accordion-border-width) * -1) 0 var(--bs-accordion-border-color);
}


</style>

 </body> 
</html> 