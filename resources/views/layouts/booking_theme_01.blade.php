<!DOCTYPE html>
<html> 
	 <head>
		@include('store_front.template-02.head')

	  	
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-7TE5XV8H82"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-7TE5XV8H82');
		</script>




	<style> 
	
	.mainnavbar 
	{ 
		margin-top: -5px ;
	}

	.mainnavbar div#navbarContent
	{
		margin-bottom: -15px;  
	}

	.bg-white
	{
		background-color: #fff !important;
	}

	

.megamenu {
  position: static;
}

.megamenu .dropdown-menu {
  background: none;
  border: none;
  width: 100%;
}

.subnav.green
	{
		background-color: #40cf33 !important;
		padding: 10px;

	}


	.subnav.yellow
	{
		background-color: #fff633 !important;
		padding: 10px;

	}
	.subnav.orange
	{
		background-color: #f83 !important;
		padding: 10px;

	}

	.navbar-nav li  
	{
		height: 50px !important;
    	padding-top: 10px !important;
	}


	.navbar-nav li a:active, .navbar-nav li a:visited
	{
		color: #454545 !important;  
	}

 
@media screen and (min-width: 960px) { 

	.navbar-nav li.parent.active.green
	{
		background-color: #40cf33 !important;
		color: #fff !important;
		border-top-right-radius: 6px !important;
		border-top-left-radius: 6px !important;
		margin-right:5px;  
	}

	.navbar-nav li.parent.yellow
	{
		background-color: #fff633 !important;
		color: #fff !important;
		border-top-right-radius: 6px !important;
		border-top-left-radius: 6px !important;
		margin-right:5px; 

	}
	.navbar-nav li.parent.orange
	{
		background-color: #f83 !important;
		color: #fff !important;
		border-top-right-radius: 6px !important;
		border-top-left-radius: 6px !important;
		margin-right:5px; 

	}
 
} 
@media screen and (max-width: 960px) { 
	.subnav
		{
			margin-top: 15px !important;
		}

}

	@media screen and (max-device-width: 480px) and (orientation: portrait) 
	{
		
		.navbar-nav li.parent.active.green, .navbar-nav li.parent.yellow, .navbar-nav li.parent.orange
		{
			background-color: #fff !important; 
		}

		
	}
	
	@media screen and (max-device-width: 640px) and (orientation: landscape) 
	{
		.navbar-nav li.parent.active.green, .navbar-nav li.parent.yellow, .navbar-nav li.parent.orange
		{
			background-color: #fff !important;  
		}
		 
	}


code {
  color: #745eb1;
  background: #fff;
  padding: 0.1rem 0.2rem;
  border-radius: 0.2rem;
}

.text-uppercase {
  letter-spacing: 0.08em;
}

.subnav .nav
{
	margin-top: 10px !important;
}

.subnav .nav li a:link, .subnav .nav li a:visited 
{
	color: #343434;
}

	</style>

		
 
	 </head>
	 <body>
	 	
	 	@include('store_front.template-02.header_bar')
	 	
			 	@yield('content') 
		
	 	@include('store_front.template-02.footer') 

@if( session()->has('shopping_session') ) 
	<span style='display: none;height:0' id='gsid'>{{ Crypt::encrypt(   session()->get('shopping_session') ) }}</span>
@endif

@if( session()->has('__user_id_') ) 
	<span id='guid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__user_id_') )  }}</span>
@else
	<span style='display: none;height:0'  id='guid'>{{ Crypt::encrypt(  0 )  }}</span>
@endif
 

	 	@include('store_front.template-02.footer-scripts') 
	 	@yield('script')  
 
 
 </body> 
</html> 