<!DOCTYPE html>
<html lang="en">
  	<head>
  		@include('erp.template-01.head')
    </head>
 <body>
 
    <div class="container-scroller">  
        @include('erp.template-01.nav_bar')   
        <div class="container-fluid page-body-wrapper"> 
        	@include('erp.template-01.side_bar')

      <div class="main-panel">
          <div class="content-wrapper">

            @include('erp.template-01.header_bar')  

          @yield('content') 
        </div>

        @include('erp.template-01.footer') 

  </div>
    </div>
 </div> 

    @include('erp.template-01.footer-scripts') 
    @yield('script') 
 

 </body> 
</html> 

 