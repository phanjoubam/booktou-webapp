<!DOCTYPE html>
<html> 
	 <head>
		@include('store_front.templates.mobile_first_head')

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-TNBF9WPMN6"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-TNBF9WPMN6');
		</script>
		
	 </head>
	 <body id='body-white'>
 <div style='background-color: #39b517 !important; '> 
<div class="row "> 
   <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 text-center" style=' padding: 20px  10px;'> 
   	<a href="{{ URL::to('/') }}">
<img src="{{ URL::to('/public/assets/image/logo_white.png') }}"  alt="bookTou">
</a>
</div>


  </div>

</div>
	<div class='container-fluid'>
	 	@yield('content') 

	 </div>
	 	@include('store_front.templates.footer-scripts') 

	 	
	 	@yield('script')
 

 </body> 
</html> 