<!DOCTYPE html>
<html lang="en">
  <head>
      @include('admin.template-02.head') 
         <meta name="csrf-token" content="{{ csrf_token() }}" />

         <link  rel="stylesheet" href="{{ URL::to('/public/assets/vendor/calendar/css/zabuto_calendar.min.css') }}" />
         <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
         <style>
          

          nav.navbar
          {
            background-color: #fff !important;
          }
          .bg-white
          {
            background-color: #fff !important;
          }

          .card .card-header
          {
            padding: .7rem .8rem !important;
          }
          

          .disablebtn{
              pointer-events: none;
              opacity: 0.4;
          }



          </style>
     </head>
    <body data-bin="{{ $data['business']->id}}">
 
      <nav class="navbar bg-white default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="{{ URL::to('/admin') }}">
            <img   src="{{ URL::to('/public/assets/image/logo_white.png') }}" /> </a>
          <a class="navbar-brand brand-logo-mini" href="{{ URL::to('/') }}">
            <img   src="{{ URL::to('/public/assets/image/logo_white.png') }}" /></a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
            
            <h2>{{ $data['business']->name }} <a href="{{ URL::to('/admin/services/business/package/search') }}?bin={{ $data['business']->id }}" class=><i class='fa fa-home'></i></a></h2>

  

          <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <strong>Amount ₹ <label class='badge badge-pill badge-info' id="cartamount">0.00</label></strong>
              </li>
              <li class="nav-item">
                <strong>Packages <label class='badge badge-pill badge-info' id="cartcount">0</label></strong>
              </li>
              <li class="nav-item">
                <strong>Timeslot # <label class='badge badge-pill badge-info' id="cartslot">--</label></strong>
              </li>

              <li class="nav-item">
                <button class="btn btn-xs btn-rounded btn-danger btnresetcart"   >
                  <i class='fa fa-refresh'></i>
                </button> 


              </li>

              @yield('cartbutton')



            <li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
              <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                {{ Session::get('_full_name') }}</a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                 
                  <p class="mb-1 mt-3 font-weight-semibold">Welcome, </p>
                  <p class="font-weight-light text-muted mb-0">User</p>
                </div>
                <a class="dropdown-item">My Profile <span class="badge badge-pill badge-danger">1</span><i class="dropdown-item-icon ti-dashboard"></i></a>
                
                <a class="dropdown-item" href='{{ URL::to("/logout") }}'>Sign Out<i class="dropdown-item-icon ti-power-off"></i></a>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>

      <div style="padding-top: 75px; min-height:680px">  
          @yield('content') 
       
 </div>
@include('admin.template-02.footer')
      <div class="modal" id="sysmsgbox" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">System Notification</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id='sysmsg'>System message ...</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>


  <form method="post" action="{{action('Admin\ServiceBookingAppointmentController@resetCart')}}" enctype="multipart/form-data">
    {{csrf_field()}} 
<div class="modal" id="wgtcartreset" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Cart Reset</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>All unsaved booking cart will be cleared. Please confirm your action.</p>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="bin"  value="{{ $data['business']->id}}" > 
        <button type="submit" name="submit" value='reset' class="btn btn-danger">Reset Now</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

    @if( session()->has('__user_id_') ) 
      <span id='guid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__user_id_') )  }}</span>
      <span id='gmid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__member_id_') )  }}</span>
    @else
      <span style='display: none;height:0'  id='guid'>{{ Crypt::encrypt(  0 )  }}</span>
      <span style='display: none;height:0'  id='gmid'>{{ Crypt::encrypt(  0 )  }}</span>
    @endif

@include('admin.template-02.footer-scripts')
<script src="{{ URL::to('/public/assets/vendor/calendar/js/zabuto_calendar.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script>

$(document).ready(function ()
{
  var bin = <?php echo $data['business']->id;?>;
  var gmid = $("#gmid").html();
  reloadCartInfo(bin, gmid);
});

$(".btnresetcart").on("click", function(){ 
  $("#wgtcartreset").modal('show');
});

</script>

@yield('script')


 </body> 
</html> 