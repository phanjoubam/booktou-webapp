<!DOCTYPE html>
<html lang="en">
  <head> 
    @include('admin.template-03.head')
  </head>


  <body>

    @if( Session::get('_user_role_') == 1000000  ) 
        @include('admin.template-03.side_bar_1000000')
    @elseif( Session::get('_user_role_') == 100000  )
        @include('franchise.template-03.side_bar_100000')
    @elseif( Session::get('_user_role_') == 10000  )
        @include('admin.template-03.side_bar_10000')
    @else
      @include('admin.template-03.side_bar')
    @endif


<section class="main_content dashboard_part large_header_bg">
      @include('admin.template-03.header_bar') 
     <div class="main_content_iner overly_inner ">
        <div class="container-fluid p-0 ">
            @yield('page_heading') 
            @yield('content')  
   
       </div>
    </div>
    @include('admin.template-03.footer') 
</section>


@include('admin.template-03.footer-scripts') 
@yield('script')  

 </body> 
</html> 