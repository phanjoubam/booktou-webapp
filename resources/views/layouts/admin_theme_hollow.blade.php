<!DOCTYPE html>
<html lang="en">
  <head>
      @include('admin.template-01.head') 
         <meta name="csrf-token" content="{{ csrf_token() }}" />
         <style>
          body {
            overflow-x: scroll; /* Show horizontal scrollbar */
          }
          </style>
     </head>
     <body>
 
    <div class="container-scroller">  
        @include('admin.template-01.nav_bar')   
        <div class="container-fluid page-body-wrapper">  
           
          <div class="content-wrapper">
 
            
            @include('admin.template-01.header_bar')  

          @yield('content') 
        </div>

        @include('admin.template-01.footer') 
 
    </div>
 </div> 

    @include('admin.template-01.footer-scripts') 
    @yield('script') 
 

 </body> 
</html> 