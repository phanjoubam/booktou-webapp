<!DOCTYPE html>
<html> 
	 <head>
		@include('store_front.template-02.head')

	  	
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-7TE5XV8H82"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-7TE5XV8H82');
		</script>

		@yield('style')

	 </head>
	 <body>
	 	
	 	@include('store_front.template-02.header_bar')
	 	
			 	@yield('content') 
	 
	 <div class='mt-4'></div>
	 	@include('store_front.template-02.footer') 

@if( session()->has('shopping_session') ) 


	<span style='display: none;height:0' id='gsid'>{{ Crypt::encrypt(   session()->get('shopping_session') ) }}</span>
@endif

@if( session()->has('__user_id_') ) 
	<span id='guid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__user_id_') )  }}</span>
@else
	<span style='display: none;height:0'  id='guid'>{{ Crypt::encrypt(  0 )  }}</span>
@endif
 

	 	@include('store_front.template-02.footer-scripts') 
	 	@yield('script') 


 	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-623c32ddbb050c34"></script> 
 </body> 
</html> 