<!DOCTYPE html>
<html lang="en">
  <head> 
    
        @include('admin.template-01.head')
     </head>
     <body>
 
    <div class="container-scroller">  
        @include('admin.template-01.nav_bar')   
        <div class="container-fluid page-body-wrapper">  
          
   
      @if( Session::get('_user_role_') == 100000  ) 
        @include('admin.template-01.side_bar_100000')
      @elseif( Session::get('_user_role_') == 10000  ) 
        @include('admin.template-01.side_bar_10000')
      @else 
         @include('admin.template-01.side_bar')
      @endif
 

      <div class="main-panel">
          <div class="content-wrapper">

            @include('admin.template-01.header_bar')  

          @yield('content') 
        </div>

        @include('admin.template-01.footer') 

  </div>
    </div>
 </div> 

    @include('admin.template-01.footer-scripts')  
    @yield('script') 

 </body> 
</html> 

 