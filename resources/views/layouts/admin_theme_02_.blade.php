<!DOCTYPE html>
<html> 
	 <head>
		@include('admin.templates.head')
	 </head>
	 <body>
 
    <div id="wrapper">

    	@include('admin.templates.nav_bar')  
    	@if( Session::get('_user_role_') == 100000  ) 
    		@include('admin.templates.side_bar_100000')
    	@elseif( Session::get('_user_role_') == 10000  ) 
    		@include('admin.templates.side_bar_10000')
    	@else 
    		@include('admin.templates.side_bar')
    	@endif
		<div id="page-wrapper">
		 
            <div id="page-inner"> 
           			@if(  isset( $data['breadcrumb_menu'] ) ) 
           				@include("menus.breadcrumb." . $data['breadcrumb_menu'] ) 
           			@endif 					
			  
				 @yield('content') 
               
            </div>
        </div>
</div>

 @include('admin.templates.footer')
 
	@include('admin.templates.footer-scripts')  
	@yield('script') 

</body> 
</html> 