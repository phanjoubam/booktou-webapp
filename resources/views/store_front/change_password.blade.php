<!DOCTYPE html>
<html> 
     <head>
        @include('store_front.template-02.head')
     </head>
     <body>

     	<div class='outer-top-tbm'> 
<div class="container">
<form method="post" action="{{action('StoreFront\StoreFrontController@changePassword')}}">
    {{csrf_field()}}
<div class="row">
   <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3">
                    <div class="card"> 
                            <div class="card-body">
                            <div class="card-title offset-md-5">
                            <a href="https://booktou.in">    <img src="{{URL::to('/public') }}/store/image/favicon-96x96.png"></a>
                            </div>
                            <div class="cart-page-heading mb-30">
                            <h5 class="offset-md-3">Change Password</h5>
                            <hr/>

                            @if(isset($userslist))
                            <input type="hidden" value="{{$userslist->id}}" name="userlistid" id="userlistid">
                            @endif

                            <div class="col-12 mb-3">
                            <label for="New Password">New Password: <span>*</span></label>
                            <input type="password" class="form-control" id="newpassword" name='newpassword' value="" placeholder='New password'>
                            </div>

                            <div class="col-12 mb-3">
                            <label for="Confirm Password">Confirm Password: <span>*</span></label>
                            <input type="password" class="form-control" id="cpassword" name='cpassword' value="" 
                            placeholder='Confirm password'>
                            </div>

                            

                            <div class="col-12 mb-3">
                            <button class="btn btn-primary btn-update-password" id="btn-update-password">Update Password</button>
                            </div>   

                        </div>

                         
                    </div>
                    </div>
                </div>
</div>


</form>

<div class="row mt-5">
                <div class="col-md-12 text-center">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved. <a href="https://booktou.in" target="_blank" style='color: '>bookTou</a>  
                    </p>
                </div>
            </div>



</div>
</div>



</body> 
</html> 

<script src="{{ URL::to('/public/store/js/jquery/jquery-2.2.4.min.js') }}/"></script>
<script src="{{ URL::to('/public/store/js/popper.min.js') }}/"></script>
<script src="{{ URL::to('/public/store/js/bootstrap.min.js') }}/"></script>  
<script src="{{ URL::to('/public/store/js/ecom.js') }}/"></script>  
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9KrtxONZFci3FxO0SPZJSejUXHpvgmHI&callback=initMap"></script>