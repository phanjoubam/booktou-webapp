 <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <title><?php if(isset($html_title) && $html_title != "" ) echo $html_title . " - bookTou" ; else echo "bookTou - Explore the Change";  ?></title>   

    <link rel="canonical" href="{{ Request::url() }}">


    <meta name="description" content="Best Food Delivery Imphal, Food Delivery Imphal, Essential Items Delivery Imphal, Online Shopping Imphal. Manipur Based 24X7 Support">

    <meta name="facebook-domain-verification" content="lo0m9lyv37z6aqlifngirc4ry4hfu4" />
    <meta property="og:title" content="Food Delivery, Essential Items Delivery & Assisted Delivery - bookTou" />
    <meta property="og:description" content="Best Food Delivery Imphal, Food Delivery Imphal, Essential Items Delivery Imphal, Online Shopping Imphal. Manipur Based 24X7 Support" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="bookTou" />
    <meta property="og:url" content="https://booktou.in" />
    <meta name="og:image" content="https://booktou.in/public/assets/image/logo.jpg" />
     



    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="https://booktou.in">
    <meta name="twitter:image" content="https://booktou.in/public/assets/image/logo.jpg" />
     
    <meta name="twitter:title" content="Food Delivery, Essential Items Delivery & Assisted Delivery - bookTou" />
    <meta name="twitter:description" content="Best Food Delivery Imphal, Food Delivery Imphal, Essential Items Delivery Imphal, Online Shopping Imphal. Manipur Based 24X7 Support">




    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="manifest" href="{{ URL::to('/public/store/image') }}/site.webmanifest">
    <link rel="mask-icon" href="{{ URL::to('/public/store/image') }}/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::to('/public/store/image') }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('/public/store/image') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::to('/public/store/image') }}/favicon-16x16.png"> 
    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Ubuntu:300,400,500,700' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" href="{{ URL::to('/public/store/theme-02') }}/css/bootstrap.css">  
      <link rel="stylesheet" href="{{ URL::to('/public/store/css/classy-nav.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/animate.css') }}"> 
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/jquery-ui.min.css') }}">  
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/font-awesome.min.css') }}"> 
    <link rel="stylesheet" href="{{ URL::to('/public/store/theme-02') }}/css/style.css"> 

    
    <link rel="stylesheet" href="{{ URL::to('/public/store')}}/css/zoom.css" />
    <link rel="stylesheet" href="{{ URL::to('/public/store')}}/vendor/fancybox/jquery.fancybox.css" />
    <link rel="stylesheet" href="{{ URL::to('/public/store')}}/vendor/fancybox/helpers/jquery.fancybox-thumbs.css" />
    <link href="{{ URL::to('/public/store') }}/vendor/pn/css/pignose.calendar.min.css" rel="stylesheet" />


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>
    <script src="{{ URL::to('/public/store/js/jquery/jquery-1.12.4.js') }}"></script>
    <!-- <script src="{{ URL::to('/public/store/js/jquery/jquery-ui.js') }}"></script> -->

    <link href="{{URL::to('/public/store')}}/theme-02/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{URL::to('/public/store/vendor/slick/css')}}/slick.css"/>
    <link rel="stylesheet" type="text/css" href="{{URL::to('/public/store/vendor/slick/css')}}/slick-theme.css"/>
      <style type="text/css">


            .mega_anchor{
                color: #fff;
                font-size: 12px;
            }
            .bg-white {
                background: #37b516e8 !important;
            }
            .megamenu  {
                    position: static
                   
            }
            .megamenu .dropdown-menu {
                background: none;
                border: none;
                width: 100%;
                color:#fff;
            }
 

            code {
                color: #745eb1;
                background: #efefef;
                padding: 0.1rem 0.2rem;
                border-radius: 0.2rem
            }

            .text-uppercase {
                letter-spacing: 0.08em
                color:#fff;
            }

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}


/*chat bot style starts here*/ 

.fa-star-o {
    color: #212529;
    cursor: pointer;
}
.fa-star {
    color: #FFD700;
    cursor: pointer;
}
/*chat bot style*/




 
  
/*----------bootstrap-navbar-css------------*/
.sticky
{
  position: fixed;

}
#booktou {
  vertical-align: middle;
  border-style: none;
  height: 50px;
  width: 110px;
 
}

.search {
  position: relative;
  color: gray;

}

.search input {
  height: 60px;
  width: 70%;
  margin-top: 27%;
  border-radius: 50px;
  text-indent: 25px;
  font-size: 20px;

  
}

.search .fa-search {
  position: absolute;
  top: 15px;
  font-size: 30px;
  right: 32%;
}
.stop
{
 position: relative;
/*margin-right: 124px;*/
right: 747px;
width: 90%;

} 
.stop .fa-search {
  position: absolute;
  left:90% ;
  bottom:10px;
  color: gray;
}
.cart
{
  margin-left:70px;
}
.login
{
  margin-left: 30px;
}

.searchpart
{
    margin-left: 21px;
    width:250px;
    height: 100%;
   
    
    padding-left: 2p;
   font-size: 14px;
   border:1px solid #e2e2e2;
  
}

.btn-primary {
  color: #fff;
  background-color: #007bff;
  border-color: #007bff;
}
.widget-header {
  display: inline-block;
  vertical-align: middle;
  position: relative;
}
.widget-header .widget-view {
  padding: 5px 8px;
  display: inline-block;
  position: relative;
  text-align: center;
}
.widget-header a {
  color: #212529;
}
.homenavbar {
  background-color:#36b416;
  border-bottom-color: #36b416;
 /* border-bottom:1px solid #36b416;*/
}

/*.tablink:hover{
  color: white;
 background-color: transparent;
}*/
.navbar-logo{
   color: #fff;
}
 
/*...*/
#navbarSupportedContent{
  overflow: hidden;
  position: fixed;
  margin-left:400px;
/*  background:red;*/
  width: 700px;
  height: 40px;
  top:8px;

}


/*.....*/
#navbarSupportedContent ul{
  padding: 0px;
  margin: 0px;
  margin-left:20% ;
  

}
/*#navbarSupportedContent ul li a i{
  margin-right: 20px;
}*/


#navbarSupportedContent li {
  list-style-type: none;
  float: left;
}
#navbarSupportedContent ul li a{
    color: #000;
    
    font-size: 15px;
    display: block;
    padding: 20px 30px;
    transition-duration:0.6s;
    transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
    position: relative;
}
#navbarSupportedContent>ul>li.active>a{
  color: #fff;
  background-color: transparent;
  transition: all 0.7s;
}
#navbarSupportedContent a:not(:only-child):after {
  content: "\f105";
  position: absolute;
  right: 20px;
  top: 10px;
  font-size: 14px;
  font-family: "Font Awesome 5 Free";
  display: inline-block;
  padding-right: 5px;
  vertical-align: middle;
  font-weight: 900;
  transition: 0.5s;
}
#navbarSupportedContent .active>a:not(:only-child):after {
  transform: rotate(90deg);
}
#element {
  top: 0px;
  left: 10%;
  height:63px;
  width: 164px;
  position: absolute;
}
.hori-selector{
  display:inline-block;
  position:absolute;
  height: 100%;
  top: 0px;
  left: 0px;
  transition-duration:0.6s;
  transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
  /*background-color: #36b416;*/
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
  margin-top: 12px;
  color: white;
  background-color: #36b416;
}
.hori-selector .right,
.hori-selector .left{
  position: absolute;
  width:40px;
  height:25px;
  /*background-color: #36b416;*/
  bottom:8px;
  /*background-color: #36b416;*/
  border-top-left-radius:100px;
  border-top-right-radius:100px;
}
.hori-selector .right{
  right: -25px;
}
.hori-selector .left{
  left: -25px;
}
.hori-selector .right:before,
.hori-selector .left:before{
  content: '';
    position: absolute;
    width: 50px;
    height:30px;
    border-radius:200%;
    margin-bottom:14px; 
   /* background-color: #fff;*/
}
.hori-selector .right:before{
  bottom: 0;
    right: -25px;
}
.hori-selector .left:before{
  bottom: 0;
    left: -25px;
}


@media(min-width: 768px){
  .navbar-mainbg{
  background-color: #fff;
  bottom:10px;
  padding:0px ;

  }
  .navbar-expand-custom {
      -ms-flex-flow: row nowrap;
      flex-flow: row nowrap;
      -ms-flex-pack: start;
      justify-content: flex-start;
  }
  .navbar-expand-custom .navbar-nav {
      -ms-flex-direction: row;
      flex-direction: row;
  }
  .navbar-expand-custom .navbar-toggler {
      display: none;
  }
  .navbar-expand-custom .navbar-collapse {
      display: -ms-flexbox!important;
      display: flex!important;
      -ms-flex-preferred-size: auto;
      flex-basis: auto;
  }
}

@media(min-width: 999px){
  .navbar-mainbg{
  background-color: #fff;
  bottom:10px;
  padding:0px ;

  }
  .navbar-expand-custom {
      -ms-flex-flow: row nowrap;
      flex-flow: row nowrap;
      -ms-flex-pack: start;
      justify-content: flex-start;
  }
  .navbar-expand-custom .navbar-nav {
      -ms-flex-direction: row;
      flex-direction: row;
  }
  .navbar-expand-custom .navbar-toggler {
      display: none;
  }
  .navbar-expand-custom .navbar-collapse {
      display: -ms-flexbox!important;
      display: flex!important;
      -ms-flex-preferred-size: auto;
      flex-basis: auto;
  }
}
.collapse{
  width: 60%;
}

@media (max-width: 1300px){

  .navbar-mainbg{
  background-color: #fff;
  bottom:10px;
  padding:13px ;

  }
  #navbarSupportedContent ul li a{
    padding: 12px 40px;
  }
  .hori-selector{
    margin-top: 0px;
    margin-left: 10px;
    border-radius: 0;
    border-top-left-radius: 35px;
    border-bottom-left-radius: 35px;

  }
  .hori-selector .left,
  .hori-selector .right{
    right: 10px;
  }
  .hori-selector .left{
    top: -25px;
    left: auto;
  }
  .hori-selector .right{
    bottom: -25px;
  }
  .hori-selector .left:before{
    left: -25px;
    top: -25px;
  }
  .hori-selector .right:before{
    bottom: -25px;
    left: -25px;
  }
}
/*nav end*/
.banner {
  width: 100%;
  float: left;
  background-color: #000000;
  height: 20%;
  background-size: 100%;
  background-repeat: no-repeat;
}
.bgcolor{
  background-color: #efefef;
}
.bg-footer{
  background-color: #000;
}
.Trend
{
    color:white;
    
     font-style: italic;
     /* margin-left:20px;*/
}


/*popular part*/
 #card
{
background-color:transparent;
height:190px;
max-width:326px;
}
#card3
{
  width:250px;
  height: 200px;
  border: none;
  left: 20px;
  border-radius: 7px;

}
.card1
{
    width:230px;
    height: 130px;

    border-radius: 7px;
}
.brow{
width:290px;
margin-right: 30px;
}

#popularcard
{
  width:250px;
  height: 200px;
  border: none;
  border-radius: 7px;
}
.card1name
{
   /* margin-left:60px;*/
    width:290px;
   /* height: 30px;*/

}

/*end popular*/

#bookrelated1
{
  height:510px;
  width:100%;
  /*margin-left:10px;*/
  border-right:1px solid black;
}
#bookrelated2
{
  
  height: 60px;
  margin-right:0; 
  bottom: 90px;

}
/*shopping part*/
.card2name

  {
   margin-left:40px;
  width: 240px;
    
} 
 
/*end shopping part*/

/*booking related part*/
 .botm-margn {
  margin-left:0px;
  margin-right: 190px;
  margin-left:px ;
  
}
.brelated{
  margin-top:10px;

}

.transperent_block {
  padding: 0px;
  position: relative;
  overflow: hidden;
  height: 250px; 
  border-radius: 2px;
}
.bg_black {
  background: #000;
  height: 40px;
  border-radius: 2px;
}


.transperent_block img {
width:250%;

height: 100%;
}
.transperent_block:hover .black_hover_block {
    opacity: 1;
    transform: translateY(-150px);
    -webkit-transform: translateY(-150px);
    -moz-transform: translateY(-150px);
    -ms-transform: translateY(-150px);
    -o-transform: translateY(-150px);

}
.transperent_block img {
    z-index: 4;
}
.transperent_block .black_hover_block {
    position: absolute;
    bottom: 3%;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    width:250%;

}
.transperent_block:hover .black_hover_block, .black_hover_block:hover {
    bottom: 30%;
    transition: all 0.3s ease-in-out;

}


.transperent_block .blur {
    background-color: rgba(0,0,0,0.7);
    height: 600px;
    z-index: 5;
    position: absolute;
    width: 100%;

}
.transperent_block .black_hover_block_text {
    z-index: 10;
    color: #fff;
    position: absolute;
    height: 800px;
    text-align: center;
    top: -40px;
    width: 100%;
   
    
}
    .block_header{
        margin-top:5px;}
        .black_hover_block_text i:hover {
    color: #77132f;
}
.black_hover_block_text i {
    padding: 10px 10px;
    font-size: 18px;
    color: #fff;
}
.black_hover_block_text i:hover {
color:#ffffffa6}

.black_hover_block_text:hover .titl-h5 {
    background-color: rgba(119, 19, 47, 0.60);
}
/*.titl-h5 {
    border-top: 2px solid #77132f;
    border-bottom: 2px solid #77132f;
    padding: 10px;
    margin-top: 0px;
    line-height: 20px;
    text-transform:uppercase;
}*/

.btn-sm {
    margin-top: 10px;
}
.black_hover_block_text li:hover {
    background-color: #77132f;
    cursor: pointer;
    color: #fff;
    width: 33.34%;
}
.black_hover_block_text li {
    list-style: none;
    border: none !important;
}

/*booking related part end*/

/*top part*/
.card2
{
    width:535px;
    height:310px;
    /*margin-left:30px;*/
    border-radius: 7px;
}

/*end top part*/

/*footer part*/

#appointment
{
    margin-left: 140px;
}
.appointdetails
{
    margin-left:155px;
}
.footerbg
{
    background-color:#333333;
}
.bookingdetails
{
    font-size:16px;
    margin-left: 10px;
}
.Shopping
{
    color: white;
    margin-left:50px;
}
.Shoppingdetails
{
    font-size:16px;
    margin-left: 10px;
}

.populardetails
{
    margin-left: 30px;
    font-size: 15px;
    width:99%;
}


.ftlastpartbg
{
    background-color: black;

}

#ftCompany
{
    color: white;
    margin-left:40px;
    font-size: 15px;
}
.details
{
    
    padding:2px ;
}
.Account
{
    color: white; 
    margin-left: 70px;
    font-size: 15px;
}
.Help
{
    color: white;
    margin-left:18px;
    font-size: 15px;
}

.Social
{
    color: white;
    margin-left:10px;
    letter-spacing: 2px;
    font-size: 15px;
}
  .item{
      left: 0;
      top: 0;
      position: relative;
      overflow: hidden;
      margin-top:50px;
      
    }
    .item img{
      -webkit-transition: .8s ease;
        transition: .8s ease;
        
    }
    .item img:hover{
      -webkit-transform: scale(1.1);
        transform: scale(1.1);

    }
    .img-thumbnail{
        border:0px;
        border-radius:0px;
    }

    .img-content {
            position: relative;
        }

        

        .overlay {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-image: linear-gradient( to right,
            hsl(344, 100%, 50%),
            hsl(31, 100%, 40%));
            transition: opacity .3s ease-in 0s,transform .3s ease-in 0s,-webkit-transform .3s ease-in 0s;
            opacity: 0;
            overflow: hidden;
        }

        .img-content:hover .overlay {
            opacity: 1;
        }

        .img-content:hover .overlay .text {
            -moz-transform: translateX(0);
            -webkit-transform: translateX(0);
            -ms-transform: translateX(0);
            transform: translateX(0);
        }

    /*footer*/

    .bg-voilet {background-color : #121012 !important}
    .text-voilet {color : #a300bf !important;}
    .bg-1 { 
     background-color: #a300bf;
    }


    /*footer ends here*/ 

     .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
     


    /*media queries for navbar*/



    /*media queries for navbar ends here*/
    </style>
</head>

