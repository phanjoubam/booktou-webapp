<!-- header top section start -->
<div class="container">
<!--2nd row  -->
<div class="row homenavbar navbar navbar-expand-md  fixed-top ">

<nav class="navbar navbar-expand-custom navbar-mainbg ">
    <div class="container" >
        <a class="" href="{{URL::to('/')}}">
   
          <img  class="img-responsive" src="{{URL::to('public/store/image/logo.png')}}" alt="bookTou" id="booktou" >
        </a>

       <button type="button" id="sidebarCollapse" class="btn btn-link d-block d-md-none">
                                <img src="{{ URL::to('/public/store/image/nav4.png') }}">
       </button>

        <div class="row col-md-4" >
           <div class="stop mt-2">
           
           <input type="text" class="form-control" placeholder="Search for booking" style="">
            <i class="fa fa-search " ></i>
           </div>
        </div>

     
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
            

            <ul class="navbar-nav ml-auto">
                
                <div class="hori-selector "id="element"><div class="left"></div><div class="right"></div></div>
                
                
                @foreach(request()->get('MAIN_MODULES') as $items)
                    
                    @if($items->main == 'SHOPPING')
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('https://alpha.booktou.in')}}">{{$items->main}}</a>
                    </li>
                    @elseif($items->main == Str::upper($name)) 

                    <li class="nav-item active">
                        <a class="nav-link " href="{{URL::to('/service/')}}/<?php echo strtolower($items->main);?>">{{$items->main}}</a>
                    </li> 
                    @else 
                      <li class="nav-item">
                        <a class="nav-link " href="{{URL::to('/service/')}}/<?php echo strtolower($items->main);?>">{{$items->main}}</a>
                    </li> 
                    @endif
                @endforeach         
            </ul>
              


              <div class="cart">
                  <a href="#" class="text-dark text-decoration-none"> Cart <i class="fa fa-shopping-cart"></i>
                <!--  <span class="badge badge-danger cart-counter">0</span> -->
              </a> 
               </div> 
               <div class="login">
                 <a  href="#" class="text-dark text-decoration-none">Login <i class="fa fa-user" aria-hidden="true"></i></a>  
               </div>             
              
        </div>
        </div>      
    </nav>

 </div>


<!-- menu navbar mobile responsive -->

<nav id="sidebar" class="d-block d-sm-none">
  <div class="sidebar-header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-7 pl-0">
          
          
          @auth
          <a class="" href="#"><i class="fa fa-user-circle mr-3"></i>Hello, 
            <small class="mt-1">{{session()->get('_full_name')}}</small></a>

          @endauth

          @guest

          <a class="btn btn-primary mr-3" href="{{ URL::to('/') }}/shopping/login">
            <i class="fa fa-user-circle mr-1"></i>Log In
          </a>
          @endguest

        </div>

        <div class="col-5 text-right">
        <a class=" btn btn-sm-info  active" href="{{URL::to('/')}}/shopping/checkout">
            <i class="fa fa-shopping-cart">

            </i> 
            <span class="badge badge-danger"> 
                @if(request()->get('CART_COUNT')>0)
                {{request()->get('CART_COUNT')}}
                @else
                0
                @endif
        </span>
        </a>
        <button type="button" id="sidebarCollapseX" class="btn btn badge badge-danger"> 
        <span class="badge badge-danger">
        <i class="fa fa-close"></i>
        </span>
        </button>
        </div>
      </div>
    </div>
  </div>

  <ul class="list-unstyled components links">
    <li class="text-center">
    <small>
        <a href="{{URL::to('/')}}">
        <img src="{{ URL::to('/public/store/image/logo.png') }}" class="img-fluid"/>
        </a>
    </small>
    </li>
    <li>
    <!-- <small><h5 class="text-uppercase text-center font-weight-bold">Shop By Category</h5></small> -->
    </li>
    <hr>

    <!--implementing category menu in mobile menu -->
     
    <li class="border-bottom"> 
    <small> 
    <a href="{{URL::to('/')}}/service/appointment"  
      style="color: #37b516;">APPOINTMENT</a> 
    </small>
    <small> 
    <a href="{{URL::to('/')}}/service/booking"  
        style="color: #37b516;">BOOKING</a> 
    </small> 
    <small> 
    <a href="{{URL::to('/')}}"   
          style="color: #37b516;">SHOPPING</a> 
    </small> 
    </li> 

    <!-- category menu ends here --> 
    @auth
        <li>
        <small> 
            <a class="btn btn-primary mr-3" href="{{ URL::to('/') }}/shopping/logout">Logout</a>
        </small>
        </li>
    @endauth
  </ul>
</nav>

<!-- menu navbar mobile responsive ends here -->
</div> 

