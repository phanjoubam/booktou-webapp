<section class="py-5 bg-voilet text-white">
    <div class="container">
      
          <div class="row col-md-10 offset-md-3 mt-3">
            <div class="col-md-3">
            <h4 >Appointment</h4>
            <div class="details">
            @foreach(request()->get('PRODUCT') as $items)
            <a href="{{URL::to('/appointment/appointment-details') }}/{{ strtolower( $items->name ) }}"class="text-decoration-none text-light">{{$items->name}}</a> <br>
            @endforeach
            </div>
           </div>

            <div class="col-md-3">
            <h4>Booking</h4>
            <div class="details">
            @foreach(request()->get('BROWSE_BOOKING') as $items)
            <a href="{{URL::to('/booking/service-details') }}/{{ strtolower( $items->name ) }}"class="text-decoration-none text-light">{{ $items->name  }}</a><br>
            @endforeach 
            </div>
            </div>
                 <div class="col-md-3">
            <h4>Shopping</h4>
            <div class="details">
            @foreach(request()->get('CATEGORIES') as $items)
            <a href="{{URL::to('/shop') }}/{{ strtolower( $items->name ) }}" class="text-decoration-none text-light"> {{$items->name}}</a><br>
            @endforeach 
          </div>
           </div>
          </div>
          <hr>
          <div class="row mt-5">
            <div class="container">
              <div class="col-md-12">
                <h4 class="text-md-center text-light">POPULAR CATEGORIES</h4>
                  
                <div class="text-md-center text-sm-left mt-4 populardetails">
                @foreach(request()->get('ALL_CATEGORIES') as $items)
                    
                  <a href=""class="text-decoration-none text-light">{{$items->category}} |</a>
                     
                @endforeach
              </div>
          </div>
        </div>
        </div>

          <div class="row mt-5">
            <div class="col-md-3">
                
                 <h4>Account</h4>
                  <ul class="list-unstyled mt-3">
                    <li><a href="{{URL::to('shopping/login')}}" class="text-white">User login</a></li>
                    <div class="mb-2"></div>
                    <li><a href="{{URL::to('join-and-refer')}}" class="text-white">Registration</a></li>
                    <div class="mb-2"></div> 
                  </ul>
            </div>
            <div class="col-md-3">
                <h4><b>Quick Links</b></h4>
                <ul class="list-unstyled mt-3">
                    <li><a href="#" class="text-white">How it works?</a></li>
                    <div class="mb-2"></div>
                    <li><a href="{{URL::to('careers')}}" class="text-white">Career</a></li>
                    <div class="mb-2"></div>
                    <li><a href="{{URL::to('privacy-policy')}}" class="text-white">Privacy Policy</a></li>
                    <div class="mb-2"></div>
                    <li><a href="{{URL::to('terms-and-conditions')}}" class="text-white">Terms & Conditions</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4><b>Support</b></h4>
                <ul class="list-unstyled mt-3">
                    <li><a href="{{URL::to('about-us')}}" class="text-white">About Us</a></li>
                    <div class="mb-2"></div>
                    <li><a href="{{URL::to('contact-us')}}" class="text-white">Contact Us</a></li>
                    <div class="mb-2"></div>
                     
                </ul>
            </div>
            <div class="col-md-3">
                <h4><b>Contact Us</b></h4>
                <ul class="list-unstyled mt-3">
                    <li><a href="#" class="text-white">How it works?</a></li>
                    <div class="mb-2"></div>
                    <li><a href="{{URL::to('careers')}}" class="text-white">Career</a></li>
                    <div class="mb-2"></div>
                    <li><a href="{{URL::to('privacy-policy')}}" class="text-white">Privacy Policy</a></li>
                    <div class="mb-2"></div>
                    <li><a href="{{URL::to('terms-and-conditions')}}" class="text-white">Terms & Conditions</a></li>
                </ul>
            </div> 

          </div>
        <hr>
        <div class="row mt-5" >
         <div class="col-md-10 offset-md-1 "id="lastpart">
            <p class="text-center">bookTou - ranked as the best food and essential items delivery platform is a product of ezanvel</p>
             <div class="col-md-6 offset-md-3 mt-3 text-center ">© 2022 bookTou, All rights are reserved.</div>



         </div>
        </div>
    </div>
</section>
      
    