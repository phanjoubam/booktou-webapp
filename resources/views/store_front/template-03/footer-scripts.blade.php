<script src="{{ URL::to('/public/store/js/jquery/jquery-2.2.4.min.js') }}/"></script>
<script src="{{ URL::to('/public/store/js/popper.min.js') }}/"></script>
<script src="{{ URL::to('/public/store/js/bootstrap.min.js') }}/"></script>  
<script src="{{ URL::to('/public/store/js/jquery-ui.js') }}/"></script>  
<script src="{{ URL::to('/public/store/js/ecom.js') }}?v=1.1"></script>
<script src="{{URL::to('/public/store/vendor/slick/js')}}/slick.min.js"></script>

<script type="text/javascript">
// section for navbar menu
function setNavBarMenu(){
    var tabsNewAnim = $('#navbarSupportedContent');
    var selectorNewAnim = $('#navbarSupportedContent').find('li').length;
    var activeItemNewAnim = tabsNewAnim.find('.active');
    var activeWidthNewAnimHeight = activeItemNewAnim.innerHeight();
    var activeWidthNewAnimWidth = activeItemNewAnim.innerWidth();
    var itemPosNewAnimTop = activeItemNewAnim.position();
    var itemPosNewAnimLeft = activeItemNewAnim.position();
    $(".hori-selector").css({
        "top":itemPosNewAnimTop.top + "px", 
        "left":itemPosNewAnimLeft.left + "px",
        "height": activeWidthNewAnimHeight + "px",
        "width": activeWidthNewAnimWidth + "px"
    });
    $("#navbarSupportedContent").on("click","li",function(e){
        $('#navbarSupportedContent ul li').removeClass("active");
        $(this).addClass('active');
        var activeWidthNewAnimHeight = $(this).innerHeight();
        var activeWidthNewAnimWidth = $(this).innerWidth();
        var itemPosNewAnimTop = $(this).position();
        var itemPosNewAnimLeft = $(this).position();
        $(".hori-selector").css({
            "top":itemPosNewAnimTop.top + "px", 
            "left":itemPosNewAnimLeft.left + "px",
            "height": activeWidthNewAnimHeight + "px",
            "width": activeWidthNewAnimWidth + "px"
        });
    });
}

$(document).ready(function(){
    setTimeout(function(){ setNavBarMenu(); });

    // mobile menu navigation
    $("#sidebarCollapse").on("click", function() {
    $("#sidebar").addClass("active");
    $('body').css('overflow', 'hidden');
  });

  $("#sidebarCollapseX").on("click", function() {
    $("#sidebar").removeClass("active");
    $('body').css('overflow-y','scroll');
  });

  $("#sidebarCollapse").on("click", function() {
    if ($("#sidebar").hasClass("active")) {
      $(".overlay").addClass("visible");
      $('body').css('overflow', 'hidden');
    }
  });

  $("#sidebarCollapseX").on("click", function() {
    $(".overlay").removeClass("visible");
    $('body').css('overflow-y','scroll');
  });


  $('.stop').hide("sticky");

  $(window).scroll(function(){                          
            if ($(this).scrollTop() > 200) {
                $('.stop').show("sticky");
            } else {
               
               $('.stop').hide("sticky");
            }
        });
});

$('.carousel').slick({
              rows: 1,
              infinite: false,
              slidesToShow: 1,
              slidesToScroll: 1
});

$(window).on('resize', function(){
    setTimeout(function(){ setNavBarMenu(); }, 500);
});
 

// --------------add active class-on another-page move----------
jQuery(document).ready(function($){
    // Get current path and find target link
    var path = window.location.pathname.split("/").pop();

    // Account for home page with empty path
    if ( path == '' ) {
        path = window.location;
    }

    var target = $('#navbarSupportedContent ul li a[href="'+path+'"]');
    // Add active class to target link
    target.parent().addClass('active');
}); 

// section for navbar menu ends here
 
</script>