@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public

    $total_cost = $delivery_cost = 0.0;

    $normal_delivery = 80; 
    $food_delivery = 50; 
    $grocery_delivery = 100.00;
    $cake_delivery =  100.00;
    
    $delivery_charge = 0.00;


    foreach($configs as $config)
    { 
        switch( $config->config_key )
        {
            case "delivery_charge": 
                $normal_delivery =  $config->config_value  ;
                break;
            case "food_delivery_charge":
                $food_delivery =  $config->config_value  ;
                break;
            case "grocery_delivery_charge":
                $grocery_delivery =  $config->config_value  ;
                break;
            case "cake_delivery_charge":
                $cake_delivery =  $config->config_value  ;
                break;
        }
    } 

?>


<div class='outer-top-tm'> 
<div class="container">
<div class="row">

     <div class="col-12">
                    <div class="page-title text-center">
                        <h1>Your Shopping Cart Details</h1>
                    </div>
                </div>


 <div class="col-md-8 offset-md-2">

    <div class='card'> 
      <div class='card-body'>
   

<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link active"  href="{{ URL::to('/shopping/my-orders') }}">My Orders</a>
  </li>
   
</ul>


                </div> 
           </div>
     </div>


  <div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 mt-2"> 
 
 

        @if (session('err_msg')) 
        <div class='card'> 
      <div class='card-body'>
            <div class="alert alert-info">
              {{ session('err_msg') }}
            </div>  </div> 
           </div> 
        @endif    
                   
  </div>  
<div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 mt-2"> 
  <div class='card'> 
      <div class='card-body'>


    @if( count($businesses) > 0 )

    @foreach($businesses as $business)

        @if( $business->category == "RESTAURANT")
            @php 
                $delivery_charge = $food_delivery;
            @endphp
        @elseif( $business->category == "CAKES & BAKERY") 
            @php 
                $delivery_charge = $cake_delivery;
            @endphp
        @elseif( $business->category == "VARIETY STORE")
             @php 
                $delivery_charge = $grocery_delivery;
            @endphp
        @elseif( $business->category == "GROCERY")
             @php 
                $delivery_charge = $grocery_delivery;
            @endphp
        @else 
             @php 
                $delivery_charge = $normal_delivery;
            @endphp
        @endif


        <div class="order-details-confirmation" style='margin-bottom: 20px;'>              
            <div class="cart-page-heading mb-30">
                <h5>{{ $business->name }}</h5>
            </div>

   
    @if( isset( $cart  ) )
        @php 
        $total_cost =0;
        $memid = 0;
       @endphp 
    <table class="table table-colored">
         <tr>
                <th width='90px'></th>
                <th style='width:250px; text-align:left' >Item</th>
                <th>Quantity</th> 
                <th>Unit Price</th>    
                <th>Packaging</th> 
                <th class='text-right'>Item + Packing = Sub-Total</th>
            </tr>  

        @foreach($cart as $cartitem)

            @if( $cartitem->bin ==$business->id  )
            <tr>
                <td width='90px'><img class='rounded ' width='40px' src="{{  $cartitem->photos  }}/" alt='{{ $cartitem->pr_code }}'/></td>
                <td style='width:250px; text-align:left' ><strong>{{ $cartitem->pr_name }}<strong></td>
                <td>{{ $cartitem->pqty }}</td>  
                <td>{{ $cartitem->actual_price }}</td>
                <td>{{ $cartitem->packaging * $cartitem->pqty}}</td>       
                <td class='text-right'>{{ ( $cartitem->actual_price *  $cartitem->pqty ) + 
                    ($cartitem->packaging * $cartitem->pqty)  }}</td>
            </tr>  

             @php 
                $total_cost += ( $cartitem->actual_price *  $cartitem->pqty ) + ($cartitem->packaging * $cartitem->pqty) ; 
                    $memid  =  $cartitem->member_id; 
             @endphp   

        @endif                   
        @endforeach  

            <tr>
                <td colspan='4'></td>    
                <td><strong>Item Total Cost:</strong></td>    
                <td class='text-right'>{{ $total_cost  }}</td>
            </tr>    
         
        


        <tr>
                <td colspan='4'></td>    
                <td><strong>Delivery Charge:</strong></td>    
                <td class='text-right'>{{ $delivery_charge }}</td>
        </tr>



<tr>
                <td colspan='4'></td>    
                <td><strong>Total Order Cost:</strong></td>    
                <td class='text-right'>{{ $total_cost + $delivery_charge }}</td>
        </tr>

<tr>
                <td colspan='6' class='text-right'> 
                    <form method="post" action="{{ action('StoreFront\StoreFrontController@cartSelectionForOrder') }}"> 
    {{ csrf_field() }}
    <input type='hidden' name='bin' value='{{ $business->id }}' /> 
    <input type='hidden' name='mid' value='{{ $memid }}' /> 
    <button type='button'   data-widget='w1' data-param='{ "w1-key":"{{ $business->id }}" }'  class="btn btn-danger btncancel">Remove</button> 
    <button type='submit' value='placeorder' name='btnorder'  class="btn btn-primary">Place Order</button> 
</form>
                </td>
        </tr>
 </table>
@endif  
   
    </div> 
    
    @endforeach 
     
    @else 
        <p class='alert alert-info'>Ooops! Your cart is empty. Please start shopping and enjoy great offers!</p>
    @endif  

 </div> 
           </div>
    </div> 
</div>
</div>
 </div>
 
 <div class='mt-4'></div>


 <form method="post" action="{{ action('StoreFront\StoreFrontController@removeCart') }}"> 
    {{ csrf_field() }}
<div class="modal wg-w1" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Shopping Cart Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <img src="{{ URL::to('/public/store/image/empty-cart.png') }}" width='120px' alt='Empty Now!' class="  float-left" />
        <p class='p-3' style='padding-left: 30px'>Selected items from the cart will be removed. Please confirm your action.<br/>
         You can still keep this items without deleting for future purchases though.
     </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='w1-key'/>    
        <button type="submit" value='submit' name='btndelete' class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form> 
 
@endsection 


@section('script')

<script>


$(document).on("click", ".btncancel", function()
{
   
    var widget = $(this).attr('data-widget');  
    var params  = $(this).attr("data-param");

    $.each( JSON.parse(params)  , function(selector, value) {
        $("#" + selector).val(value); 
    });
 
    $(".wg-" + widget).modal( "show" );



});

</script>  

@endsection 