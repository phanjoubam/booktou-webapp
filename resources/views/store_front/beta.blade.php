@extends('layouts.store_front_02')
@section('content')
 <?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<div class='outer-top-tm'> 
    <div class="container">  
    <div class='row'> 
        <div class='col-sm-3 col-xs-12   sidebar'>


<div class="sidebar-widget   widget_price_filter">
    <h3 class="section-title">Filter by price</h3>
  <div data-role="main" class="ui-content">
    <form method="post" action="/action_page_post.php">
      <div data-role="rangeslider">
        <label for="price-min">Price:</label>
        <input type="range" name="price-min" id="price-min" value="200" min="0" max="1000">
        <label for="price-max">Price:</label>
        <input type="range" name="price-max" id="price-max" value="800" min="0" max="1000">
      </div>
        <input type="submit" data-inline="true" value="Submit">
        <p>The range slider can be useful for allowing users to select a specific price range when browsing products.</p>
      </form>
  </div>
</div>

 

</div>


 
<div class='col-sm-9 col-xs-12 col-md-9 col-mlg-9 '>
    <div class='filters-container'>
        <div class="clearfix  ">
            <div class="row">
                <div class="col col-md-12 text-right">
                    <form class="woocommerce-ordering" method="get">
                        <select name="orderby" class="orderby" aria-label="Shop order">
                                                    <option value="menu_order" selected="selected">Default sorting</option>
                                                    <option value="popularity">Sort by popularity</option>
                                                    <option value="rating">Sort by average rating</option>
                                                    <option value="date">Sort by latest</option>
                                                    <option value="price">Sort by price: low to high</option>
                                                    <option value="price-desc">Sort by price: high to low</option>
                                            </select>
                        <input type="hidden" name="paged" value="1">
                                    </form>
                </div>
            </div>
        </div>


  <!-- filtered products -->
<div class="row row-sm  clearfix mt-2 cb-p2"> 

     @if( isset($products))
     @foreach ($products  as $product) 
        <?php   
                $all_photos = array(); 
                $files =  explode(",",  $product->photos ); 
                if(count($files) > 0 )
                {
                    $files=array_filter($files);
                    $folder_path =  $cdn_path .  "/assets/image/store/bin_" .   $product->bin .  "/";

                    foreach($files as $file )
                    {

                        $source =  $folder_path .   $file; 
                        if(file_exists( $source  ))
                        {

                            $pathinfo = pathinfo( $source  );
                            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 

                            $destination  = $cdn_path  .    $new_file_name ;
                            if( file_exists( $destination ) )  
                            {
                                $all_photos[]  = $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name;
                            }
                            else 
                            {
                                if(filesize(  $source ) > 307200)  
                                { 
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name ;  
                                }
                                else 
                                {
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" .   $product->bin   . "/" .    $file  ;
                                }
                            } 
                        }
                        else
                        {
                            $all_photos[]   =  $cdn_url .  "/assets/image/no-image.jpg";
                        }
                    }
                    
                    if( count($all_photos ) > 0 )
                    {
                        $image_url =  $all_photos[0];
                    } 
                    else 
                    {
                        $image_url = $cdn_url .  "/assets/image/no-image.jpg";
                    }
                    
                }
                else
                {
                    $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                } 

                $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

         ?> 
          <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 "> 
                <div class="card card-sm card-product-grid">
                <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->pr_code ) }}"  class="img-wrap">
                    <img src="{{ $image_url }}"> 
                </a>
                <figcaption class="info-wrap">
                    <a  href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->pr_code ) }}"  class="title">{{$product->pr_name}}</a>
                    <div class="price mt-1">
                    @if($product->unit_price == $product->actual_price)
                        <p class="card-text">₹ {{$product->actual_price }}</p>
                      @else     
                        <p class="card-text">₹ {{$product->actual_price }} <span class="old-price">₹ {{$product->unit_price}}</span></p>
                      @endif
                  </div>   
                </figcaption>
                
                <div class='text-center'>
            <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}" 
                class="btn btn-primary btn-sm btn-add-item">Add to Cart</button>
        </div>

             </div>                            
    </div> 
    @endforeach 
        <div class="col-md-12"> 
    {{      $products->appends(request()->input())->links()  }}
    </div> 
    @endif 
                            
                  
   </div> 
 <!-- filtered products --> 
</div>
</div> 
</div> 
</div> <!-- container -->
</div> <!-- spacer -->
  @endsection