
<header class="header_area">
    <div id='topbar-black' class="text-center white" > 
       Our super friendly Customer Care <i class='fa fa-phone'></i> <a href='tel:+919863086093' class='white'>9863086093</a> is ready to assist you on food &amp; essential delivery orders. 
    </div>


        <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">
            <!-- Classy Menu -->
            <nav class="classy-navbar" id="essenceNav">
                <!-- Logo -->
                <a class="nav-brand" href="{{ URL::to('https://booktou.in') }}">
                <img width='120px' src="{{ URL::to('/public/store/image/logo.png') }}" alt="bookTou"></a>
                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>
                <!-- Menu -->
                <div class="classy-menu">
                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>
                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul>
                            @if(Session::has('cms_main_menu'))  
                            @foreach(Session::get('cms_main_menu') as $item)
                          
                                @if($item->has_sub_menu == "yes")
                                <li><a href="{{ URL::to('/shop') }}/{{ strtolower( $item->menu_name ) }}">{{ ucfirst( $item->menu_name) }}</a>
                                    <div class="megamenu">
                                        <ul class="single-mega cn-col-4">
                                            @foreach(Session::get('cms_sub_menu') as $subitem)
                                                @if($subitem->menu_id == $item->id )
                                                    <li><a href="{{ URL::to('/shop') }}/{{strtolower(  $item->menu_name ) }}/all/{{ strtolower( $subitem->sub_menu_name) }}">{{ ucfirst( $subitem->product_category) }}</a></li> 
                                                @endif    
                                            @endforeach    

 

                                        </ul> 
                                    </div>
                                </li> 
                                @else
                                    <li><a href="{{ URL::to('/shop') }}/{{ strtolower($item->menu_name) }}">{{ ucfirst( $item->menu_name) }}</a></li> 
                                @endif  
                            @endforeach 
                            @endif 
                        </ul>
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>

            <!-- Header Meta Data -->
            <div class="header-meta d-flex clearfix justify-content-end">
                <!-- Search Area -->
                <div class="search-area">
                    <form action="{{  action('StoreFront\StoreFrontController@searchProducts') }}" method="get">
                        <input type="search" name="keyword" id="headerSearch" placeholder="Search anything here ...">
                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>


           @auth

                <div class="user-login-info">
                    <a href="{{ URL::to('/settings/view-my-profile') }}"> 
                        <img src="{{ URL::to('/public/store/image/user.svg') }}" alt="Profile"></a>
                </div>
                <div class="user-login-info">
                    <a href="{{ URL::to('/shopping/logout') }}"> 
                        <img src="{{ URL::to('/public/store/image/exit.svg') }}" alt="Logout"></a>
                </div>


           @endauth

           @guest
  
                <div class="user-login-info">
                    <a href="{{ URL::to('/shopping/login') }}"> 
                        <img src="{{ URL::to('/public/store/image/user.svg') }}" alt=""></a>
                </div> 

           @endguest
                 

                <!-- Cart Area -->
                <div class="cart-area">

                    <a href="#" id="essenceCartBtn"><img src="{{ URL::to('/public/store/image/bag.svg') }}" alt="">
                        <span class='badge badge-danger badge-cart'>
                        @if( Session::has("cart_count") )
                            {{ Session::get("cart_count") }}
                        @else 
                            0
                        @endif
                        </span></a>
                </div>
            </div>

        </div>
    </header>
    <div class="cart-bg-overlay"></div> 

     <div class="right-side-cart-area"> 
        <!-- Cart Button -->
        <div class="cart-button">
            <a href="#" id="rightSideCart"><img src="{{ URL::to('/public/store/image/bag.svg') }}" alt="">
                <span class='badge badge-danger badge-cart'>@if( Session::has("cart_count") )
                            {{ Session::get("cart_count") }}
                        @else 
                            0
                        @endif</span>
            </a>
        </div>

        <div class="cart-content d-flex">  
            <!-- Cart Summary -->
            <div class="cart-amount-summary"> 
                <h3>Shopping Cart</h3>
                @if( !Session::has("__user_id_")   )
                <p class='alert alert-info'>
                    The car items may change when you login in as it will show all your saved items.
                </p>
                @endif
                @php 
                if( Session::has('__cart_items' ) && Session::has('__businesses' ) )
                {
                    $__cart =  Session::get('__cart_items' );
                    $__businesses =  Session::get('__businesses' );
                @endphp
                <ul class="list-group list-group-flush" style='margin-top: 25px;'>
                    @foreach($__businesses as $business)

                    @foreach($__cart as $cartitem)

                        @if( $cartitem->bin == $business->id  )
                        <li class="list-group-item">
                            <a href="{{ URL::to('/shopping/checkout') }}">
                                <img class='rounded ' width='40px' src="{{  $cartitem->photos  }}/" alt='{{ $cartitem->pr_code }}'/>
                            <strong>{{ $cartitem->pr_name }}<strong> 
                            </a>
                        </li>   

                    @endif                   
                    @endforeach 


                    @endforeach
                </ul>

                @php     
                }
                @endphp 

                <div class="checkout-btn mt-4">
                    @if( !Session::has("__user_id_")   )
                        <a href="{{ URL::to('/shopping/login') }}" class="btn btn-info">Login</a>
                    @else
                        <a href="{{ URL::to('/shopping/checkout') }}" class="btn btn-info">check out</a>
                    @endif
                        
                 
                </div>
            </div>
        </div>
    </div>


 
    

    