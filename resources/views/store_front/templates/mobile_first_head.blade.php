<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <title>
    <?php 
    if( isset($htmlTitle) && $htmlTitle != "")
    	echo $htmlTitle;
    else 
    echo "Callback Enquiry Form"; 
 	?> - bookTou</title>  
 
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /> 

<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/css/vendor.bundle.addons.css">    
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/css/shared/style.css">  
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/font-awesome.css" rel="stylesheet" />  
<link href="{{ URL::to('/public/assets') }}/vendor/pn/css/pignose.calendar.min.css" rel="stylesheet" />   
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/style.css" rel="stylesheet" />  

<style>

body 
{
    background-color: #616161 !important;
}

body#body-white
{
    background-color: #fff !important;
}


</style>
</head>

