<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <title>bookTou - Evolve the change with your own Assistant</title>  

 

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="manifest" href="{{ URL::to('/public/store/image') }}/site.webmanifest">
    <link rel="mask-icon" href="{{ URL::to('/public/store/image') }}/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::to('/public/store/image') }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('/public/store/image') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::to('/public/store/image') }}/favicon-16x16.png">
    
    




    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Ubuntu:300,400,500,700' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" href="{{ URL::to('/public/store/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/classy-nav.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/font-awesome.min.css') }}"> 
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/core-style.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store') }}/style.css"> 


</head>

