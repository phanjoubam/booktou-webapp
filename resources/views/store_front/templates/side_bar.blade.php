
 <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                     <li class="active ">
            <a href="{{  URL::to('/' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Dashboard
            </a>
          </li> 
          
          <li  >
            <a href="{{  URL::to('/admin/customer-care/orders/view-all' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> View All Orders
            </a>
          </li>
         
          <li>
            <a href="{{  URL::to('/admin/customer-care/orders/view-completed' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Completed Orders
            </a>
          </li>


          <li>
            <a href="{{  URL::to('/admin/products/category/manage-categories' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Add Product Category
            </a>
          </li>
  


  <li>
            <a href="{{  URL::to('/admin/customer-care/business/view-all' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> View Businesses
            </a>
          </li>

        <li>
            <a href="{{  URL::to('/admin/customer-care/delivery-agents' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> View Delivery Agents
            </a>
          </li>
          <li>
            <a href="{{  URL::to('/admin/customer-care/business/view-promotion-businesses' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> Premium Businesses
            </a>
          </li>
       
    <li><a href="#"><i class="fa fa-sitemap"></i> Customer<span class="fa arrow"></span></a>
      <ul class="nav nav-second-level">
        <li>
            <a href="{{  URL::to('/admin/systems/manage-customers' )}}">
              <i class="now-ui-icons ui-1_bell-53"></i> View All
            </a>
          </li>
        </ul>
     </li>

                    <li>
                        <a href="#"><i class="fa fa-cog"></i> Systems<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level"> 
<li>
                <a href="{{  URL::to('/admin/systems/manage-business-categories' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Business Categories
                </a>
              </li>

              



                             
                        </ul>
                    </li>
                   <li>
                        <a href="#"><i class="fa fa-cog"></i> Marketing<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                <li>
                <a href="{{  URL::to('/admin/systems/manage-cloud-messages' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Cloud Message
                </a>
              </li>

              <li>
                <a href="{{  URL::to('/admin/systems/manage-voucher-codes' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Voucher Codes
                </a>
              </li>

<li>
                <a href="{{  URL::to('/admin/customer-care/business/products/view-all-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> Manage Featured Products
                </a>
              </li>

              
<li>
                <a href="{{  URL::to('/admin/customer-care/business/products/view-featured-products' )}}">
                  <i class="now-ui-icons ui-1_bell-53"></i> View Featured Products
                </a>
              </li>

 

                             
                        </ul>
                    </li>
                </ul>

<p class='text-center white'>All right reserved. Owned by <a href="https://booktou.in">bookTou.in</a></p> 
            </div>

        </nav>


          
        
     