@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public

    $total_cost = $delivery_cost = 0.0;

?>


<div class='outer-top-tm'> 
<div class="container">
   <div class="row mt-2">
        <div class="col-12">
      <div class="page-title text-center">
        <h1>My Wishlist</h1>
      </div>
    </div>


             <div class="col-md-8 offset-md-2 mt-2">
               <div class='card'> 
                  <div class='card-body'>
                    <ul class="nav nav-pills">
                      <li class="nav-item pr-2">
                        <a class="nav-link "  href="{{ URL::to('/shopping/my-orders') }}">My Orders</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="nav-link btn-primary"  href="{{ URL::to('/shopping/my-bookings') }}">My Bookings</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="nav-link  btn-danger active"  href="{{URL::to('/shopping/show-wishlist-product') }}">Your Wishlist</a>
                    </li>
                </ul>
            </div> 
        </div>
    </div>


</div>


  <div class="row mt-2">

  <div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1"> 

        @if (session('err_msg')) 
            <div class="alert alert-info">
              {{ session('err_msg') }}
            </div> 
        @endif                
  </div> 

<div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1"> 
<div class="row mt-1">
<div class="card col-12">
  <div class="card-body">
  <div class="row row-sm   clearfix mt-2 cb-p2" id="loading"> 
 {{csrf_field()}}

        @if( isset($products))
        @foreach ($products  as $product) 
        <?php 
        $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-', trim($product->pr_name) ) ); 
        ?>
                  
            <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 ">
                <div class="card card-sm card-product-grid">
                        
                        <span class="text-right mr-4"> @if($product->food_type=="Veg")
                        <i style="color: green;" class="fa fa-circle"></i>
                        @elseif($product->food_type=="Non-veg")
                        <i style="color: brown;" class="fa fa-circle"></i> </i> 
                        @else
                        @endif
                        </span> 

        <a href="{{  URL::to('/shop/view-product-details')}}/{{$formatted_name}}/{{ strtolower($product->pr_code)}}"  class="img-wrap">
           @foreach($photos as $image)

           @if($product->prid == $image->prid)
           <img src="{{$image->image_url}}" class="img-fluid"> 
           @break
           @endif
           @endforeach           
            </a> 
            <figcaption class="info-wrap">
                   
        <div class="col text-left">
        <a class="text-center" href="{{URL::to('/shop/view-product-details')}}/{{$formatted_name}}/{{ strtolower($product->pr_code)}}"  class="title">{{$product->pr_name}}</a>
                       


                    <div class="price mt-1 text-left">
                    @if($product->unit_price == $product->actual_price)
                        <p class="card-text">₹ {{$product->actual_price }}

                       

                        </p>
                      @else     
                        <p class="card-text">₹ {{$product->actual_price }} <span class="old-price">₹ {{$product->unit_price}}
                        </span>


                        </p>
                      @endif
                  </div>
                       </div> 
 

                </figcaption>
       

        <div class='info-wrap  '>
            
         <div class="row">
        <div class="col">  

        <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}" 
                class="btn btn-primary btn-sm btn-add-item"> Add to Cart </button> 

        </div>

    </div>
                
         
    </div>
             </div>                            
    </div>

    @endforeach

    <div class="col-md-12"> 
    
    </div> 
    @endif 
        
    </div> 

  </div>
</div>
</div>
</div> 


</div>
</div>
</div>









@endsection 


@section('script')

<script>


$(document).on("click", ".btncancel", function()
{
   
    var widget = $(this).attr('data-widget');  
    var params  = $(this).attr("data-param");

    $.each( JSON.parse(params)  , function(selector, value) {
        $("#" + selector).val(value); 
    });
 
    $(".wg-" + widget).modal( "show" );

});

</script>  

@endsection 