<!DOCTYPE html>
<html> 
     <head>
        @include('store_front.template-02.head')
     </head>
     <body>

     	<div class='outer-top-tbm'> 
<div class="container">
    <form method="post" action=" {{action('StoreFront\StoreFrontController@passwordOtpcheck')}}">
    	{{csrf_field()}}
<div class="row">
   <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3">
                    <div class="card"> 
                            <div class="card-body">
                            <div class="card-title offset-md-5">
                            <a href="https://booktou.in">    <img src="{{URL::to('/public') }}/store/image/favicon-96x96.png"></a>
                            </div>
                            <div class="cart-page-heading mb-30">
                            <h5 class="offset-md-3"> OTP Verification Process</h5>
                            <hr/>
							@if(isset($err_msg))
                                 <p class="alert-warning p-3">{{$err_msg}}</p>
                            @endif
                            <div class="col-12 mb-3">
                            <label for="phone">Enter OTP: <span>*</span></label>
                            <input type="number" class="form-control" id="otp" name='otp' value="" placeholder='Enter 6 Digit Number'>
                            </div>

                            <div class="col-12 mb-3">
                                    <button class="btn btn-primary" id="btn-submit-otp">Continue</button>
                            </div>   

                        </div>

                         
                    </div>
                    </div>
                </div>
</div>
</form>

<div class="row mt-5">
                <div class="col-md-12 text-center">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved. <a href="https://booktou.in" target="_blank" style='color: '>bookTou</a>  
                    </p>
                </div>
            </div>



</div>
</div>



</body> 
</html> 
<script src="{{ URL::to('/public/store/js/jquery/jquery-2.2.4.min.js') }}/"></script>
<script src="{{ URL::to('/public/store/js/popper.min.js') }}/"></script>
<script src="{{ URL::to('/public/store/js/bootstrap.min.js') }}/"></script>  
<script src="{{ URL::to('/public/store/js/ecom.js') }}/"></script>  
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9KrtxONZFci3FxO0SPZJSejUXHpvgmHI&callback=initMap"></script>