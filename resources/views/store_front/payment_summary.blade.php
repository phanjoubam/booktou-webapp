@extends('layouts.store_front_mobile_first_white')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>




@if(isset($order_info))

  @if( $order_info->book_status == "delivered" || $order_info->book_status == "canceled"  )


   <div class="row "> 
    <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-2"  > 
        <p>Thank you for choosing bookTou. Your order is closed.</p>

     </div> 
    </div> 
  </div> 
 

  @else 
 
<div class="row "> 
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-2"  > 
        <strong>Hello {{ ucwords( $order_info->customer_name) }},</strong><br/>
        <p>We received payment for your order. Your payment summary is listed below:</p>

     </div>
    </div>
  </div>
  <div class="mt-2" style='border-bottom:1px dashed #efefef;'></div> 

  <div class="row ">
     <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
     <table class='table table-striped'  style='width:90%; margin-right:auto; margin-left:auto' >
        <thead class=" text-primary">
          <th colspan='2'>Order # {{ $order_info->id }} Status <span class='badge badge-{{ strcasecmp( $order_info->book_status, "cancelled" ) == 0 ? "danger" : "success" }} badge-pill'>{{ $order_info->book_status }}</span></th>

        </thead>
        <thead class=" text-primary">
          <th colspan='2'>Booking Date: {{  date('Y-m-d', strtotime( $order_info->book_date ) )  }} Service Date: {{  date('Y-m-d', strtotime( $order_info->service_date ) )  }}</th>
        </thead>


        <thead class=" text-primary">
          <th>Service Name</th> 
          <th class='text-right'>Sub Total</th> 
        </thead>
        <tbody>

       @php
       $slno=1;
       $subtotal = 0.00;
       @endphp
       @foreach($order_items as $item)

       @php
        $subtotal += $item->service_charge ;
       @endphp

       <tr>
         <td class='text-right'>{{ $item->service_name}}</td> 
         <td class="text-right">₹ {{number_format( $subtotal , 2, ".", "")  }}</td>
       </tr>

       @php
        $slno++;
       @endphp
       @endforeach
       <tr> 
         <td  class='text-right'><b>Applicable GST:</b></td>
         <td class="text-right">₹ {{number_format(  $order_info->gst , 2, ".", "")  }}</td>
       </tr>

       <tr> 
         <td   class='text-right'><b>Total Payable After Discount :</b></td>
         <td class="text-right">₹ {{number_format( $order_info->total_cost + $order_info->gst   - $order_info->discount , 2, ".", "")  }}</td>
       </tr>

       <tr> 
         <td   class='text-right'><b>Payment Status:</b></td>
         <td class="text-right"><span class='badge badge-success badge-pill'>{{ $order_info->payment_status }}</span></td>
       </tr>


</tbody>
</table>
</div>
  
  </div> 

 

  <div class="mt-2" style='border-bottom:1px dashed #efefef;'></div>
   

 
 @endif
 
   
@endif
  
 
   

<div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 text-center mt-2 ">  
 
  <h4>Have no account yet?<br/>
  Seems like we can have great experience together.</h4>
  <a href="https://booktou.in/join-and-refer" class="btn btn-info btn-rounded">Signup Here</a><br/>

  <h4>Already have an account?</h4>
  <a href="https://booktou.in/shopping/login" class="btn btn-success btn-rounded">Login Instead</a>

</div> 
</div>  

<div class="row mb-4"> 
     <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-5 text-center"  > 
         <a href='https://bit.ly/2Ln8C1Y' target='_blank'>
           <img src="{{ URL::to('/public/assets/image/download-booktou.png') }}" width='200px' alt='Download bookTou Android App'/>
    </a>
     </div> 
    </div> 
  </div> 

  

@endsection 

@section('script')

  

<script>
 
 
</script>

@endsection 