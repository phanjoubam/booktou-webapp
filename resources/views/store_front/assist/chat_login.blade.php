<div class='chat-bubble you scrollview'>

    <form method="post">
    {{ csrf_field() }}
    <input type="hidden" id="srvukey" name="refererlogin" value="<?php echo $nos = bin2hex(random_bytes(24));?>">
         <h5 class="modal-title" id="staticBackdropLabel">Customer Login</h5>
         <div class=" col-sm-12  col-md-12 col-lg-12  text-center">           
            <div class=" col-12 mb-3">
                <label for="phone">Phone Number or Email: <span>*</span></label>
                <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email' required>
            </div>                               
            <div class=" col-12 mb-4">
                <label for="password">Password: <span>*</span></label>
                <input type="password" class="form-control" id="password" value="" name='password' placeholder='Specify password' required>
            </div>
          </div>
                           
          <div class="col-sm-12 col-md-12 col-lg-12  text-center">
            <div class=" col-sm-12 col-md-12 col-lg-12">
                <button type="button" class="btn btn-primary btn-block btncustomerchatlogin" value='login' name='login'>Login</button>  
            </div>
                   
            <div class="row col-sm-12 col-md-12 col-lg-12 text-center signup">
                <div class=" col-12 mb-3">
               <small class="text col-12  text-center" style="margin-left:3PX">New Member? Please signup here</small>
            </div>               
              <div class="col-sm-12 col-md-12 col-lg-12  text-center">
                  <a target="_blank" class="btn btn-info btn-sm " href="{{URL::to('join-and-refer')}}">Sign up</a>
              </div> 
            </div>  
          </div>
 
      <div class="mt-2"></div>

      </div>
    </form>
 </div>