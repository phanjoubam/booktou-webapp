<form action="{{ action('Auth\LoginController@customerLogin') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" id="srvukey" name="assistlogin">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Customer Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
         <div class="col-12 mb-3">
                                    <label for="phone">Phone Number or Email: <span>*</span></label>
                                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email'>
                                </div>
                               
                               
                                <div class="col-12 mb-4">
                                    <label for="email_address">Password: <span>*</span></label>
                                    <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password'>
                                </div>
                            </div>
    </div>
    <div class="modal-footer">
    <button type="submit" class="btn btn-primary btn-block" value='login' name='login'>Login</button>
        
        
    </div>
    </div>
  </form>