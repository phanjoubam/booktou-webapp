<div class="stepwizard col-sm-12 col-md-12 col-lg-12">
    <div class="stepwizard-row setup-panel col-sm-12  col-md-12 col-lg-12  ">
        <div class="stepwizard-step ">
            <a href="javascript::void();" type="button" class="btn btn-primary btn-circle stepswizard-1">1</a>
            <p style="font-size:10px;"><b>Pick up</b></p>
        </div>
        <div class="stepwizard-step ">
            <a href="javascript::void();" type="button" class="btn btn-default btn-circle stepswizard-2" disabled="disabled">2</a>
            <p style="font-size:10px;"><b>Destination</b></p>
        </div>
        <div class="stepwizard-step ">
            <a href="javascript::void();" type="button" class="btn btn-default btn-circle stepswizard-3" disabled="disabled">3</a>
            <p style="font-size:10px;"><b>Task Description</b></p>
        </div>
    </div>
</div>
 
    <div class=" setup-content col-sm-12  col-md-12 col-lg-12" id="step-1" style="display:block;">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <input type="hidden" value="{{$taskid}}" class="taskid" name="taskid">

            <div class="form-row">
                <div class="col-sm-12  col-md-12 col-lg-12">
                    <label class="control-label"> </label>
                    <input type="text" name="pickupName" class="form-control pickupname" placeholder="Pick up name" required>
                </div>
                <div class="col-sm-12  col-md-12 col-lg-12">
                    <label class="control-label"></label>
                    <input maxlength="10" type="number" name="pickupPhone"  class="form-control pickupphone" placeholder="Pick up phone no" required/>
                </div>
            <div class="col-sm-12  col-md-12 col-lg-12">
            <label class="control-label"> </label>
            <input type="text" name="pickupAddress"  class="form-control pickupaddress" placeholder="Pick up address" required />
            </div>
                <div class="col-sm-12  col-md-12 col-lg-12 mt-3">
                    <button class="btn btn-primary btn-sm nextBtn btn-block" style="height: 30px;" type="button" >Next</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12  col-md-12 col-lg-12 setup-content" id="step-2" style="display:none;">
        <div class="col-sm-12 col-md-12 col-lg-12">
             
                 <div class="form-row">
                <div class="col-sm-12  col-md-12 col-lg-12">
                    <label class="control-label"></label>
                     <input type="text" name="destinationName" id="destinationname"  
                class="form-control destinationname" placeholder="destination name" required> 
                </div>
                <div class="col-sm-12  col-md-12 col-lg-12">
                    <label class="control-label"></label>
                   <input type="text" name="destinationPhone" id="destinationphone"  
                class="form-control destinationphone" placeholder="destination phone no"   required>
                </div>
                <div class="col-sm-12  col-md-12 col-lg-12">
                <label class="control-label"></label>
                <input type="text" name="destinationAddress" id="destinationaddress"  
                class="form-control destinationaddress" placeholder="destination address"   required>
                </div>
                <div class="col-sm-12  col-md-12 col-lg-12 mt-3">
                    <button class="btn btn-primary btn-sm nextBtn2 btn-lg btn-block" style="height: 30px;" type="button" >Next</button>
                </div>
             
                
            </div>
        </div>
    </div>
    <div class="col-sm-12  col-md-12 col-lg-12 setup-content" id="step-3" style="display:none;">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="form-row"> 
            <div class="col-sm-12  col-md-12 col-lg-12">
            <label class="control-label"></label>
            <input type="text" name="itemservicedate" id="item_servicedate" class="form-control calendar" placeholder="Preffered date" required>
            </div>
            <div class="col-sm-12  col-md-12 col-lg-12">
              <label class="control-label"></label>
              <input type="text" name="servicefee" id="task_charges" class="form-control servicefee" placeholder="service fee" min="60" required> 
            </div>

            <div class="col-sm-12  col-md-12 col-lg-12">
             <label class="control-label"></label> 
            <input type="file"  id="photo" name='photo' class="form-control"> 
            </div>

            <div class="col-sm-12  col-md-12 col-lg-12">
            <label class="control-label"></label> 
            <textarea rows="2" class="form-control" name="remarks" id="task_details" placeholder="Items description" required></textarea>
            </div> 

            <div class="col-sm-12  col-md-12 col-lg-12 mt-3 assist_steps"> 

                <button class="btn btn-primary btn-sm btn-block complete-your-request" type="button"  value="button">Complete Your Request
                </button>
                
            </div>
            <div class="col-sm-12  col-md-12 col-lg-12 mt-3 assist_userlog">
                 <button class="btn btn-primary btn-sm btn-block " value="btnsave" name="btnTaskWizard" type="submit">
                    Save Your Request
                </button>
            </div>


            </div>
        </div>
    </div>
 
 



<script>
    
    $('.calendar').pignoseCalendar({

});
</script>