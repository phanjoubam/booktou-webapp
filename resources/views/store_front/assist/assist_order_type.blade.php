@extends('layouts.store_front_mobile_first')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; 


$itemname; 

?>
@if(Session::get('stage2'))
@foreach(Session::get('stage2') as $val) 

@php $itemname = $val;  @endphp

@endforeach
@endif
<div class=" ">

<div class=" ">
<div class="card-body">

<div class="row ">
  <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3 ">
      
<div class="page-content" style="background-image: url('images/wizard-v3.jpg')">
<div class="wizard-v3-content">
<div class="wizard-form">
<div class="wizard-header">
<h3 class="heading">Call Back Request Form</h3>
<p>Fill all form field to go next step</p>
</div>
<form class="form-register" action="{{url('shopping/booktou-assist-wizard-post')}}" method="post"enctype="multipart/form-data">
	{{csrf_field()}}
<div id="form-total">
<div class="steps clearfix">

<ul role="tablist"><li role="tab" aria-disabled="false" class="first done" aria-selected="false">
<a id="form-total-t-0" class="modal-verification" href="javascript::void();" aria-controls="form-total-p-0"><div class="title">
<span class="step-icon"><i class="fa fa-user"></i></span>
<span class="step-text"> Verification</span>

</div>
</a>
</li>

<li role="tab" aria-disabled="false" class="done" aria-selected="false">

@if(Session::get('stage2'))
<a id="form-total-t-1" class="" href="{{URL::to('/shopping/booktou-assist-wizard?stage=2')}}" aria-controls="form-total-p-1"><div class="title">
<span class="step-icon"><i class="fa fa-clipboard"></i></span>
<span class="step-text">Order Type</span></div></a>
@else
<a id="form-total-t-1" class=" disabled"   aria-controls="form-total-p-1"><div class="title">
<span class="step-icon"><i class="fa fa-clipboard"></i></span>
<span class="step-text"> </span></div></a>
@endif
</li>

<li role="tab" aria-disabled="false" class="done" aria-selected="false">
@if(Session::get('stage2'))
<a id="form-total-t-2" href="{{URL::to('/shopping/booktou-assist-wizard?stage=3')}}" aria-controls="form-total-p-2">
<div class="title"><span class="step-icon"><i class="fa fa-tag"></i></span><span class="step-text">Request</span>
</div>
</a>
@else
<a id="form-total-t-2"  aria-controls="form-total-p-2">
<div class="title"><span class="step-icon"><i class="fa fa-tag"></i></span><span class="step-text"></span>
</div>
</a>

@endif

</li>

<li role="tab" aria-disabled="false" class="last current" aria-selected="true">
 

<a id="form-total-t-3" aria-controls="form-total-p-3"><span class="current-info audible"> 
</span><div class="title"><span class="step-icon"><i class="fa fa-check"></i></span>
<span class="step-text"></span></div></a>

 

</li>
</ul>

</div>
 

<section>
 

 <input type="hidden" value="2" name="stage">




 
 	<div class="card">
 		<div class="card-body">
 			<div class="form-group">
 			<div class="form-row">
			
			<div class="col">
				 <select  class="form-control" id="order-items-list" name="orderitemslist">
				  <option value="">Select Order Type</option>
				  <option value="cake"
				  @if(isset($itemname))
					  {{ $itemname == "cake" ? 'selected' : '' }}
					  @endif >Cake</option>
									  <option value="grocery" 
									  @if(isset($itemname))
					  {{ $itemname == "grocery" ? 'selected' : '' }}
					  @endif>Grocery</option>
									  <option value="medicine"@if(isset($itemname))
					  {{ $itemname == "medicine" ? 'selected' : '' }}
					  @endif>Medicine</option>
									  <option value="gifts" 
									  @if(isset($itemname))
					  {{ $itemname == "gifts" ? 'selected' : '' }}
					  @endif>Gift Items</option>
									  <option value="others"
									  @if(isset($itemname))
					  {{ $itemname == "others" ? 'selected' : '' }}
					  @endif>Others</option>
				  </select>
			       

			</div>
			
			<div class="col">

			    
			 
		
			    </div>
			  </div>
			 

			   <div class="showHtml">
			   </div>

			<div class="form-group">
			  <div class="form-row">
			
			<div class="col">
			      <button class="btn btn-primary">Next</button>
			    </div>
			    <div class="col">
			      
			    </div>
			</div>

			</div>

 		</div>
 	</div>
  
     
</section>

 

 

 
</div>
</form>
</div>
</div>
</div>               
  </div>
 </div>







</div>
</div>
</div>
 

<!-- modal box -->

 

<!-- Modal -->
<div class="modal fade" id="verification" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<form method="post" action="{{URL::to('/shopping/booktou-assist-wizard-post')}}">
    		{{csrf_field()}}
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Identity Verification Process</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <p class="text-center">Data will be lost, if you press ok!</p>
         <p class="text-center">Are you sure!</p>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" name="confirm_deletion" value="OK">OK</button>
      </div>
  </form>
    </div>
  </div>
</div>






@endsection


@section('script')
 
<script>
$('.modal-verification').click(function(){
 	 
 	 $("#verification").modal("show");

 });
 
  
</script>

<style type='text/css'> 

  body{margin:0}.page-content{width:100%;margin:0 auto;display:flex;display:-webkit-flex;justify-content:center;-o-justify-content:center;-ms-justify-content:center;-moz-justify-content:center;-webkit-justify-content:center;align-items:center;-o-align-items:center;-ms-align-items:center;-moz-align-items:center;-webkit-align-items:center;background-repeat:no-repeat;background-position:center center;background-size:cover;-o-background-size:cover;-ms-background-size:cover;-moz-background-size:cover;-webkit-background-size:cover}.wizard-v3-content{background:#fff;width:780px;box-shadow:0 8px 20px 0 rgba(0,0,0,.15);-o-box-shadow:0 8px 20px 0 rgba(0,0,0,.15);-ms-box-shadow:0 8px 20px 0 rgba(0,0,0,.15);-moz-box-shadow:0 8px 20px 0 rgba(0,0,0,.15);-webkit-box-shadow:0 8px 20px 0 rgba(0,0,0,.15);border-radius:10px;-o-border-radius:10px;-ms-border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px;margin:110px 0;font-family:roboto,sans-serif;position:relative;display:flex;display:-webkit-flex}.wizard-v3-content .wizard-form{width:100%}.wizard-form .wizard-header{text-align:center;padding:40px 0 20px}.wizard-form .wizard-header .heading{color:#333;font-size:32px;font-weight:700;margin:0;padding:13px 0 10px}.wizard-form .wizard-header p{color:#666;font-size:18px;font-weight:400;margin:0}.form-register .steps{margin-bottom:33px}.form-register .steps ul{display:flex;display:-webkit-flex;list-style:none;padding-left:108px}.form-register .steps li,.form-register .steps li.current{outline:none;-o-outline:none;-ms-outline:none;-moz-outline:none;-webkit-outline:none;position:relative}.form-register .steps li .current-info{display:none}.form-register .steps li a{text-decoration:none;outline:none;-o-outline:none;-ms-outline:none;-moz-outline:none;-webkit-outline:none}.form-register .steps li a .title span{display:block}.form-register .steps li a .title .step-icon{width:60px;height:60px;border-radius:50%;-o-border-radius:50%;-ms-border-radius:50%;-moz-border-radius:50%;-webkit-border-radius:50%;background:#ccc;margin:0 auto;position:relative;outline:none;-o-outline:none;-ms-outline:none;-moz-outline:none;-webkit-outline:none;color:#fff;font-size:25.6px;margin-right:108px}.form-register .steps li a .step-icon i{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-webkit-transform:translate(-50%,-50%)}.form-register .steps li .step-icon::before,.form-register .steps li:last-child .step-icon::after{position:absolute;content:"";background:#e5e5e5;width:108px;height:2px;top:50%;transform:translateY(-50%);-o-transform:translateY(-50%);-ms-transform:translateY(-50%);-moz-transform:translateY(-50%);-webkit-transform:translateY(-50%)}.form-register .steps li .step-icon::before{right:100%}.form-register .steps li:last-child .step-icon::after{left:100%}.form-register .steps li.current a .step-icon,.form-register .steps li.current a:active .step-icon,.form-register .steps li.done a .step-icon,.form-register .steps li.done a:active .step-icon{background:#24c1e8}.form-register .steps .current .step-icon::before,.form-register .steps .current:last-child .step-icon::after,.form-register .steps .done .step-icon::before{background:#24c1e8}.form-register .steps li a .step-text{color:#999;font-weight:400;font-size:18px;padding:14px 0 8px}.form-register .steps .current .step-text,.form-register .steps .done .step-text{color:#333}.form-register .content{margin:0 20px;box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-o-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-ms-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-moz-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-webkit-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;padding:35px 55px 55px}.form-register .content h2{display:none}.form-register .content .inner h3{font-size:22px;color:#333;font-weight:700;margin:0;padding-bottom:18px;padding-top:5px}.form-register .content .form-total-p-1 .inner h3{padding-bottom:13px;padding-top:0}.form-register .content #form-total-p-3 .inner h3{padding-bottom:5px}.form-register .content .inner h4{font-size:18px;font-weight:700;color:#333;margin:0}.inner .form-row{display:flex;margin:0 -12px;position:relative}.form-row.form-row-date.form-row-date-1{margin-top:50px;margin-bottom:-18px}.inner .form-row .form-holder{width:50%;padding:0 12px;margin-bottom:15px;position:relative}.inner .form-row .form-holder.form-holder-1{width:84.1%}.inner .form-row .form-holder.form-holder-2{width:100%;position:relative}.inner .form-row .form-row-inner{position:relative}.inner .form-row .form-holder .label{position:absolute;top:-3px;left:10px;font-size:16px;font-weight:400;color:#666;transform-origin:0 0;transition:all .2s ease;-webkit-transition:all .2s ease;-moz-transition:all .2s ease;-o-transition:all .2s ease;-ms-transition:all .2s ease}.inner .form-row .form-holder .border{position:absolute;bottom:31px;left:0;height:2px;width:100%;background:#6bc734;transform:scaleX(0);-webkit-transform:scaleX(0);-moz-transform:scaleX(0);-o-transform:scaleX(0);-ms-transform:scaleX(0);transform-origin:0 0;transition:all .15s ease;-webkit-transition:all .15s ease;-moz-transition:all .15s ease;-o-transition:all .15s ease;-ms-transition:all .15s ease}.inner .form-row #radio{color:#666;font-weight:400;font-size:16px;margin:8px 12px}.inner .form-row #checkbox{margin:10px 0 28px 11px;font-size:15px;color:#666;font-weight:400}.inner .form-row .form-holder label.special-label{display:inline-block;float:left;margin-top:25px;padding-right:20px;color:#666;font-size:16px;font-weight:400}.inner .form-row .form-holder label.pay-1-label,.inner .form-row .form-holder label.pay-2-label{width:190px;height:95px;border:1px solid #e5e5e5;display:block;cursor:pointer;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-o-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box;float:left;margin-right:15px;text-align:center;color:#666;font-size:16px;font-weight:400;margin-bottom:5px}.inner .form-row .form-holder label.pay-1-label img,.inner .form-row .form-holder label.pay-2-label img{padding-top:15px;padding-bottom:8px}.inner .form-row.form-row-date .form-holder select{float:left;width:19%;margin-right:20px}.inner .form-row .form-holder input,.inner .form-row .form-holder select{width:100%;padding:13px 10px 8px;border:none;border-bottom:2px solid #e5e5e5;appearance:unset;-moz-appearance:unset;-webkit-appearance:unset;-o-appearance:unset;-ms-appearance:unset;outline:none;-moz-outline:none;-webkit-outline:none;-o-outline:none;-ms-outline:none;font-family:roboto,sans-serif;font-weight:400;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;-o-box-sizing:border-box;-ms-box-sizing:border-box}.inner .form-row .form-holder input{font-size:18px;color:#333;font-weight:700}.inner .form-row .form-holder input#pay-1,.inner .form-row .form-holder input#pay-2{display:none}.inner .form-row .form-holder select{font-size:15px;color:#666;background:#fff url(../images/wizard_v4_icon.png) no-repeat scroll;background-position:right 0 center;z-index:1;cursor:pointer;position:relative}.inner .form-row .form-holder select:focus{border-bottom:2px solid #24c1e8}.inner .form-row .form-holder .form-control:focus,.inner .form-row .form-holder .form-control:valid{border-bottom:2px solid #24c1e8;margin-top:21px}.inner .form-row .form-holder .form-control:focus+.label,.inner .form-row .form-holder .form-control:valid+.label{transform:translateY(-23px) scale(1);-o-transform:translateY(-23px) scale(1);-ms-transform:translateY(-23px) scale(1);-moz-transform:translateY(-23px) scale(1);-webkit-transform:translateY(-23px) scale(1);color:#24c1e8}.inner .form-row .form-holder .form-control:focus+.border,.inner .form-row .form-holder .form-control:valid+.border{transform:scaleX(1);-o-transform:scaleX(1);-ms-transform:scaleX(1);-moz-transform:scaleX(1);-webkit-transform:scaleX(1)}.inner .form-row .form-holder input#pay-1:checked+label,.inner .form-row .form-holder input#pay-2:checked+label{border:none;box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-o-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-ms-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-moz-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-webkit-box-shadow:0 3px 10px 0 rgba(0,0,0,.15)}.inner .form-row.table-responsive{border:none;border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;margin:27px 0 -25px}.inner .table-responsive .table{width:100%}.inner .table-responsive tbody{text-align:left}.inner .table-responsive tr.space-row>td,.inner .table-responsive tr.space-row>th{padding:17px 20px 14px;border-top:1px solid #e5e5e5}.inner .table-responsive tr.space-row:last-child>td,.inner .table-responsive tr.space-row:last-child>th{border-bottom:none}.inner .table-responsive tbody th{color:#666;font-size:16px;font-weight:400;width:30%}.inner .table-responsive tbody td{color:#333;font-size:16px;font-weight:400}.actions ul{list-style:none;padding-left:0;padding:0 20px;margin:30px 0;display:flex;display:-webkit-flex;justify-content:space-between;-o-justify-content:space-between;-ms-justify-content:space-between;-moz-justify-content:space-between;-webkit-justify-content:space-between}.actions ul li.disabled{opacity:0}.actions ul li{padding:0;border:none;border-radius:3px;-o-border-radius:3px;-ms-border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;display:inline-flex;height:45px;width:140px;justify-content:center;-o-justify-content:center;-ms-justify-content:center;-moz-justify-content:center;-webkit-justify-content:center;-o-align-items:center;-ms-align-items:center;-moz-align-items:center;-webkit-align-items:center;align-items:center;background:#24c1e8;font-family:roboto,sans-serif;font-size:16px;font-weight:400;cursor:pointer}.actions ul li:hover{background:#1d97b5}.actions ul li:first-child{background:#999;margin-left:60.9%}.actions ul li:first-child:hover{background:#666}.actions ul li a{color:#fff;text-decoration:none;padding:13px 34px}@media screen and (max-width:1199px){.wizard-v3-content{margin:180px 20px}}@media screen and (max-width:991px){.form-register .steps ul{padding-left:0;justify-content:space-around;-o-justify-content:space-around;-ms-justify-content:space-around;-moz-justify-content:space-around;-webkit-justify-content:space-around}.form-register .steps li a .title{text-align:center}.form-register .steps li a .title .step-icon,.form-register .steps li a .title .step-icon{margin:0 auto}.form-register .steps li a .title .step-icon::before,.form-register .steps li:last-child a .title .step-icon::after{content:none}.actions ul li:first-child{margin-left:0}}@media screen and (max-width:767px){.inner .form-row.form-row-date .form-holder select{width:17.5%;margin-right:20px}.inner .form-row .form-holder label.pay-1-label,.inner .form-row .form-holder label.pay-2-label{margin-bottom:15px}}@media screen and (max-width:575px){.wizard-v3-content{width:90%}.wizard-form .wizard-header{padding:40px 20px 20px}.inner .form-row{flex-direction:column;-o-flex-direction:column;-ms-flex-direction:column;-moz-flex-direction:column;-webkit-flex-direction:column;margin:0}.inner .form-row .form-holder{width:100%}.inner .form-row .form-holder.form-holder-1{width:100%}.inner .form-row .form-holder{padding:0}.inner .form-row .form-holder label.special-label{float:none}.inner .form-row.form-row-date .form-holder select{float:none;width:100%;margin-bottom:20px;margin-right:0;display:block}.form-register .steps li a .title .step-icon{width:40px;height:40px;font-size:20px}.form-register .steps li a .step-text{font-size:12px}.form-register .content{padding:35px 30px 60px}.inner .form-row #radio{margin-left:-2px}.inner .form-row #checkbox{margin-left:0}.actions ul li{width:120px}.actions ul li a{padding:15px 24px}}

  .btn1 {
    display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
     
    font-size: 0.875rem;
    line-height: 1;
    border-radius: 0.1875rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}

  </style> 
@endsection 