
<?php 

$item;$title;$kg;$flavour;$description;$photo;

// if (session()->has("stage2")) {
// 	  foreach (session()->get("stage2") as $key => $value) {
 			
//  			$item=$value['itemname'];
//  			$title=$value['_title'];
//  			$kg=$value['_size'];
//  			$flavour=$value['_flavour'];
//  			$description= $value['description'];
//  			$photo= $value['photo_path'];
// 	  }
// }


?>


<div class="form-group">
 			<div class="form-row">
			
			<div class="col">
				<label>What to print</label><input type="hidden" value="what_to_print" name="cake_title_id">
				<input type="text" name="cake_title" placeholder="print of the cake" class="form-control" 
				value="@if(isset($item)) {{$item}} @endif">
			</div>
			
			<div class="col">
				<label>Weight</label>
				 <input type="hidden" value="weight" name="cake_weight_id">
			   <input type="number" name="size" min="1" class="form-control" value="@if(isset($kg)){{$kg}}@endif" >    
			</div>
			<div class="col">
				<label>Flavour</label>
				<input type="hidden" value="flavour" name="cake_flavour_id">
				<input type="text" name="cake_flavour" placeholder="Flavour of the cake" class="form-control" value="@if(isset($flavour)) {{$flavour}}@else @endif">   
			</div>
			</div>
</div>

<div class="form-group">
 			<div class="form-row">
			
			<div class="col">
				 <label>Description</label><input type="hidden" value="description" name="cake_description_id">
				<textarea class="form-control" name="cake_description">@if(isset($description)) {{$description}}@else @endif</textarea> 
			</div>
			
			<div class="col">
				 <label>Upload a sample photo <small>[Photo is mandatory for cake]</small></label>
				 <input type="hidden" value="photo" name="cake_photo_id">
				   <input type="file"  id="photo" name='photo'>
				   <input type="hidden" name="photo_path" value="@if(isset($photo)) {{$photo}}@else @endif">
			</div>
			
			</div>
</div>


 