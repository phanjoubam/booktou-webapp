 
     <!--  <h7 class="modal-title text-center col" id="staticBackdropLabel"><img src="{{asset('/public/store/image/favicon-96x96.png')}}"> How may i assist you?</h7>
      <hr> -->
      <!-- <div class="scrollChat"> -->

      <div class='chat-bubble you view  col-sm-12  col-md-12 col-lg-12'>
      
      <p class="col-sm-12  col-md-12 col-lg-12 text-center">
                <small>Pick up, purchase or send an item from anywhere inside the Imphal Town</small>
      </p>

    <!-- <p class="sys-alert"></p> -->
    <form method="post" 
    action="{{action('StoreFront\StoreFrontController@assistCompleteYourRequest')}}" enctype="multipart/form-data" class="col-sm-12 col-md-12 col-lg-12 mb-4">
        {{csrf_field()}} 

    <div class="wizardsteps">
        
        <div class="col-sm-12 col-md-12 col-lg-12  text-center mb-3">
                    @if(isset($assists))
                        <select class="form-control" name="assistcategory" required id="assistcategory">
                            <option value="">Select assist category</option>
                            @foreach($assists as $items)
                            <option value="{{$items->id}}">{{$items->assist_cat_name}}</option>
                            @endforeach
                        </select>
                    @endif 
                <div class="col-sm-4 offset-sm-4 col-md-8 col-lg-8 offset-md-2 offset-lg-2 mt-2 mb-2">
                        <button type="button" class="btn btn-warning btn-block btn-sm btn-assist-proceed" style="height: 30px;">Proceed</button>
                </div>
        </div>
        
    </div>
    </form>

     
    <div class="col-md-12">
        <div class="mt-4 text-center"> 
            <img src="{{asset('/public/store/image/smoking.png')}}"> 
            <img src="{{asset('/public/store/image/drinks.png')}}">
        </div>
        <p class="text-center" style="color:red;font-size: 10px;"><small>We do not deliver cigarettes, alcohols, drugs, guns and other illegal items which are prohibited by the law.
         (Fragile/value items is not recommended)</small>
        </p>
    </div> 
</div>

                    </div>