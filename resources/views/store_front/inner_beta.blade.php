@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>
<div class='outer-top-tm'>

 <div class="container"> 

<div class='row'>
	<div class='col-sm-3 col-xs-12   sidebar'>

		<div class="sidebar-widget widget_product_categories">
			<h3 class="section-title">{{ $business->name }}</h3>
                            <div class='fanbox'>
                                <div class='fanbox-header'>
                                 <img src='{{ $cdn_url  }}{{ $business->banner }}' width='100%'  />
                                </div>
                                <div class='fanbox-body'>
                                    <p><span class="widget-title mb-30"></span><br/>

                                    {{ $business->locality }}<br/>
                                     {{ $business->landmark }}  {{ $business->city }},  {{ $business->state }}
                                    </p>  
                                </div>   
                            </div>

                        </div>

<div class="sidebar-widget   widget_price_filter">
	<h3 class="section-title">Filter by price</h3>
<form method="get" action="">
	<div class="price_slider_wrapper">
		<div class="price_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" style=""><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 100%;"></span></div>
		<div class="price_slider_amount" data-step="10">
			<input type="text" id="min_price" name="min_price" value="40" data-min="40" placeholder="Min price" style="display: none;">
			<input type="text" id="max_price" name="max_price" value="990" data-max="990" placeholder="Max price" style="display: none;">
						<button type="submit" class="button">Filter</button>
			<div class="price_label" style="">
				Price: <span class="from">₹10</span> — <span class="to">₹1000</span>
			</div>
						<div class="clear"></div>
		</div>
	</div>
</form>

</div>
<div class="sidebar-widget widget_product_categories">
	<h3 class="section-title">Product categories</h3>

	<ul class="product-categories"> 
	@foreach ($product_categories as $cat)
                                    @php 
                                     $category = $cat->category;
                                     $parts = explode(" ", $category);
                                     $category_hyphenated  = implode("-", $parts); 
                                    @endphp 
                                   <li class="cat-item">
                                        <a href="{{ URL::to('/shopping') . '/' . strtolower($business->merchant_code) . '/' . strtolower($category_hyphenated)  }}">{{$cat->category}}</a>  
                                    </li>
                                    @endforeach 

</ul>

</div>

	</div>
 
<div class='col-sm-9 col-xs-12 col-md-9 col-mlg-9'>
	<div class='filters-container'>
<div class="clearfix  ">
	<div class="row">
		<div class="col col-md-12 text-right">
			<form class="woocommerce-ordering" method="get">
				<select name="orderby" class="orderby" aria-label="Shop order">
											<option value="menu_order" selected="selected">Default sorting</option>
											<option value="popularity">Sort by popularity</option>
											<option value="rating">Sort by average rating</option>
											<option value="date">Sort by latest</option>
											<option value="price">Sort by price: low to high</option>
											<option value="price-desc">Sort by price: high to low</option>
									</select>
				<input type="hidden" name="paged" value="1">
							</form>
		</div>
	</div>
</div>


  <!-- filtered products -->
<div class="row row-sm  clearfix mt-2"> 

		@if( isset($products))
		@foreach ($products  as $product)  
            <?php  
  
                $all_photos = array(); 
                $files =  explode(",",  $product->photos ); 
                if(count($files) > 0 )
                {
                    $files=array_filter($files);
                    $folder_path =  $cdn_path .  "/assets/image/store/bin_" .   $product->bin .  "/";

                    foreach($files as $file )
                    {

                        $source =  $folder_path .   $file; 
                        if(file_exists( $source  ))
                        {

                            $pathinfo = pathinfo( $source  );
                            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 

                            $destination  = $cdn_path  .    $new_file_name ;
                            if( file_exists( $destination ) )  
                            {
                                $all_photos[]  = $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name;
                            }
                            else 
                            {
                                if(filesize(  $source ) > 307200)  
                                { 
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name ;  
                                }
                                else 
                                {
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" .   $product->bin   . "/" .    $file  ;
                                }
                            } 
                        }
                        else
                        {
                            $all_photos[]   =  $cdn_url .  "/assets/image/no-image.jpg";
                        }
                    }
                    
                    if( count($all_photos ) > 0 )
                    {
                        $image_url =  $all_photos[0];
                    } 
                    else 
                    {
                        $image_url = $cdn_url .  "/assets/image/no-image.jpg";
                    }
                    
                }
                else
                {
                    $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                }  

                $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 
         ?> 

         <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 ">
         	<a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->pr_code ) }}">
         		<div class="card card-sm card-product-grid">
				<a href="#" class="img-wrap">
					<img src="{{ $image_url }}"> 
				</a>
				<figcaption class="info-wrap">
					<a href="#" class="title">{{$product->pr_name}}</a>
					<div class="price mt-1">
					@if($product->unit_price == $product->actual_price)
				      	<p class="card-text">₹ {{$product->actual_price }}</p>
				      @else     
				      	<p class="card-text">₹ {{$product->actual_price }} <span class="old-price">₹ {{$product->unit_price}}</span></p>
				      @endif
				  </div>  

				  <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" 
                                                    data-prid="{{  $product->id }}"  data-subid="{{  $product->id }}" 
                                                    class="btn essence-btn btn-add-item">Add to Cart</button>

				</figcaption>
		</div>
	</div>
 


		   
		  @endforeach
		  {{      $products->appends(request()->input())->links()  }}

@endif 

</div>

  <!-- filtered products -->

</div>
</div>

</div>


</div> <!-- container -->
</div> <!-- spacer -->

@endsection 

@section('script')

 <style>
      
      #embedmap {
        height: 250px;   
        width: 100%;   
       }
    </style>



<script>

 
  
    </script>

@endsection 
