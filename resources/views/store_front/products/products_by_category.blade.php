@extends('layouts.store_front_02')
@section('content')
<?php 
$cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
    ?>
    <div class='breadcrumb-box'>  
        <div class='container'>  
            <div class="row mt-2"> 
                <div class="col-md-12">
                    <div class="row mt-3">
                        <div class="col-md-12 col-sm-12" >
                            <nav aria-label="breadcrumb">
                              <div class="breadcrumb col-md-3" >
                                <li class="breadcrumb-item text-uppercase" >
                                    <a class="text-weight-bold" href="{{URL::to('/')}}" style="color:green;">
                                     <b> SHOPPING</b>
                                 </a>
                             </li> 
                             <li class="breadcrumb-item active text-uppercase" aria-current="page">
                                {{$menuname}}
                            </li>
                        </div>
                        <div class="breadcrumb col-md-9" >                           
                              <!-- <li class='btn btn-outline-primary'><a href="#">Everywhere</a></li>
                              <li class='badge badge-warning badge-pill'><a href="#">Everywhere</a></li>
                              <li class='badge badge-warning badge-pill'><a href="#">Everywhere</a></li>
                           
                          -->
                      </div>
                  </nav>
              </div>
          </div>
      </div>
  </div>
</div>
</div>
<div class='outer-top-tm'> 
    <div class="container">  
        <div class='row'> 

            <!-- sidebar area -->


            <div class='col-sm-3 col-xs-12   sidebar'>

                <div class="sidebar-widget   widget_price_filter">
                    <h3 class="section-title">Sharing is Caring</h3> 
                    <div class="row"> 
                        <div class="col"> 
                            <p class=''><i class='fa fa-share-alt-square fa-3x light-green' onclick="copyUrl();"></i></p>
                        </div>
                        <div class="col mt-3 d-block d-sm-none"> 
                            <button class="btn btn-warning float-right btn-sm showMerchantList"> 
                               <span class="fa fa-filter"></span> View Merchant 
                           </button>
                           
                       </div> 
                   </div> 
               </div>



               <div class="accordion" id="accordionExample">
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                      <button class="accordion-button " data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <span class='heading-4 text-dark'>View Merchant</span>
                    </button>
                </h2>

                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="addto-search">                                 
                        <div class="col-md-10 offset-md-1 mt-3" style="border-bottom: 1px solid #ced4da;color: gray;">
                          <i class="fa fa-search " ></i>                        
                          <input  class="keyword " id="mersearch" type="text" placeholder="Search ....">    
                      </div>
                  </div>
                  <div class="accordion-body addto-playlists">
                   <ul>

                    @foreach ($businesses as $business)
                    <li class="cat-item">
                       <label for="random-1" class="playlist-name">                
                        <a href="{{ URL::to('/business') }}/{{ strtolower(  $business->merchant_code ) }}">{{$business->name}}</a>
                    </label>
                </li>
                @endforeach 

                <li class="cat-item">
                   <label for="random-1" class="playlist-name">                
                    <a href="#">More ...</a>
                </label>
            </li>

        </ul>
    </div>
</div>
</div>
<div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed text-dark" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
         Category
     </button>
 </h2>
 <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
  <div class="accordion-body">
   <form class="addto-playlists" action="{{URL::to('/shop') }}/{{$menuname}}" method='get' >  
    @foreach ($product_category as $cat)
    @php 
    $category = $cat->category; 
    $checked = ""
    @endphp


    @foreach ($filters as $filter_item)

    @if( strcasecmp($filter_item, $category) == 0)
    @php 
    $checked = "checked"
    @endphp
    @endif

    @endforeach


    <div class="form-check checkbox-container">
        <input class="form-check-input category-check" {{ $checked }}  onChange='submit();'
        type="checkbox"  name="categories"  
        value="{{ $category }}" 
        data-check="{{strtolower( $category )}}" 
        data-key="{{$menuname}}">
        <label class="form-check-label" for="{{strtolower( $category )}}">{{  $category }}</label>
    </div>
    @endforeach  
</form>     
</div>
</div>
</div>
<div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed text-dark"  data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
         Price
     </button>
 </h2>
 <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
  <div class="accordion-body">
      
    <?php
    
    $minval = $min_amount;
    $full_range = $max_amount -  $minval; 
    
    $range_size= 100;
    $no_of_ranges = $full_range / $range_size;
    

    for($i = 0 ; $i <= $no_of_ranges; $i++)
    {
        ?>
        <a class="text-color" href="{{ Request::fullUrl() }}&minprice=<?php echo (number_format($minval)) ; ?>&maxprice=<?php echo (number_format($minval + $range_size)); ?>">
         

            <label>₹ <?php echo (number_format($minval)) ; ?> - ₹ <?php echo (number_format($minval + $range_size)); ?></label>
        </a>  
        <br/>
        <?php 
        $minval+=$range_size;
    }
    
    ?>

</div>
</div>
</div>
</div>
<!-- sidebar area ends -->
</div>
<div class='col-sm-9 col-xs-12 col-md-9 col-mlg-9'>
    <div class='filters-container'>
        <div class="clearfix  ">
            <div class="row" style="background: #efefef; padding:10px;">
                <div class="col-md-5 text-right">
                  <input  class="form-control form-control-sm" type="text" id="search-product" placeholder="Filter product by keyword" style="height: calc(1.2em + .7rem + 2px);
                                                    padding: .35rem .5rem;
                                                    font-size: .82rem;
                                                    line-height: 1.2;
                                                    border-radius: .2rem;">


                </div>
                <div class="col-md-3 text-right">
                    <form class="woocommerce-ordering" method="get">
                        <select name="orderby" class="orderby" aria-label="Shop order">
                            <option value="menu_order" selected="selected">Default sorting</option>
                            <option value="popularity">Sort by popularity</option>
                            <option value="rating">Sort by average rating</option>
                            <option value="date">Sort by latest</option>
                            <option value="price">Sort by price: low to high</option>
                            <option value="price-desc">Sort by price: high to low</option>
                        </select>
                        <input type="hidden" name="paged" value="1">
                    </form>
                </div>
            </div>
        </div>
        <!-- filtered products -->
        <div class="row row-sm  clearfix mt-2 cb-p2 sort-list" id="loading"> 
           {{csrf_field()}}
            @if($maincategoryMenu=="Shopping")
            @if( isset($products))
            @foreach ($products  as $product) 
            
            <?php  
            if ($product->photos=="") {
              $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
          }else{
           $image_url = $product->photos;
       }  
       $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

       
       ?>     
       <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 "> 
        <div class="card card-sm card-product-grid">
            <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="img-wrap">
                <img src="{{ $image_url }}"> 
            </a>
            <figcaption class="info-wrap">
                <a  href="{{  URL::to('/shop/view-product-details')}}/{{$formatted_name}}/{{ strtolower(  $product->prsubid ) }}"  class="title">{{$product->pr_name}}</a>
                <div class="price mt-1">
                    @if($product->unit_price == $product->actual_price)
                    <p class="card-text">₹ {{$product->actual_price }}</p>
                    @else     
                    <p class="card-text">₹ {{$product->actual_price }} <span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}</span></p>
                    @endif
                </div>  
                

            </figcaption>
            

            <div class='text-center'>
                @if($product->allow_enquiry == "yes")
                <a role="button" target='_blank' data-phone="8787306375" 
                href="https://web.whatsapp.com/send?text={{ Request::url() }}&phone=+918787306375"   class="btn btn-info btn-pill  btn-sm " data-action="share/whatsapp/share" >Enquire Now</a> 
                
                <button type="button" data-action="wish" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}"  class="btn btn-warning btn-pill btn-sm btn-wish-item   "> Add to Wishlist </button> 

                @else
                
                @if(  $product->stock_inhand == 0  )
                <button type="button"   class="btn btn-outline-primary btn-sm" disabled>Out of stock</button>
                
                @else  
                <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}" 
                    class="btn btn-primary btn-sm btn-add-item">Add to Cart</button>
                    @endif 
                    @endif     

                </div>
            </div>                            
        </div>

        @endforeach

        <div class="col-md-12"> 
            {{      $products->appends(request()->input())->links()  }}
        </div> 
        @endif 
        @endif  
    </div> 
    <!-- filtered products --> 
</div>
</div> 
</div> 
</div> <!-- container -->
</div> <!-- spacer --> 

<!-- Sidebar Overlay -->
<div class="sidenav" id="mySidenav" style="">
    <div class="row ">
        <div class="col-12 "> 
            <div class="card-title">
                <h5 class="pl-3 mt-3 title-merchant">All Merchants</h5>
                <a href="javascript:void(0)" class="closebtn showMerchantListX">×</a>
            </div>
            
        </div> 
    </div>
    <hr>
    
    <ul class="list-unstyled components links"> 
        
        @foreach ($businesses as $business)
        <li class="cat-item">
            <a href="{{ URL::to('/business') }}/{{ strtolower(  $business->merchant_code ) }}"><small>
                {{$business->name}}
            </small></a>  
        </li>
        @endforeach
        
    </ul>       
    <!--   -->
    <hr>
</div>
<!--  -->

@endsection

@section('script')
<style>

   .accordion-item:first-of-type
   {
    border-radius:  0!important;
}

.accordion-button:not(.collapsed)
{
    background-color: #fff !important;

}


.search-button{

   border: #ced4da;

}
#mersearch{
  border: #ced4da;
  outline: none;

}
</style>  


<script type="text/javascript">

 $(document).ready(function(){
  $("#search-product").on("keyup", function() {
    var value = $(this).val().toLowerCase();
      $("#loading div").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });
    (function($) {

        "use strict";

        var fullHeight = function() {

            $('.js-fullheight').css('height', $(window).height());
            $(window).resize(function(){
                $('.js-fullheight').css('height', $(window).height());
            });

        };
        fullHeight();

    })(jQuery);

    (function($){
      $(".keyword").on('keyup', function(e) {
        var $this = $(this);
        var exp = new RegExp($this.val(), 'i');
        $(".addto-playlists li label").each(function() {
          var $self = $(this);
          if(!exp.test($self.text())) {
            $self.parent().hide();
        } else {
            $self.parent().show();
        }
    });
    });
  })(jQuery);
</script>
@endsection