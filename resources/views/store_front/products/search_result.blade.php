@extends('layouts.store_front_02')
@section('content')
 <?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>



<div class='outer-top-tm'> 
    <div class="container">  
    <div class='row'> 
        
<div class='col-sm-3 col-xs-12   sidebar  float-sm-none float-sm-right'>

 <div class="sidebar-widget   widget_price_filter">
    <h3 class="section-title">Sharing is Caring</h3> 
    <p class=''><i class='fa fa-share-alt-square fa-3x light-green' onclick="copyUrl();"></i></p>
</div>

 

</div>

 
<div class='col-sm-9 col-xs-12 col-md-9 col-mlg-9 '>
    <div class='filters-container'>
        <div class="clearfix  ">
            <div class="row">
                <div class="col col-md-12 text-right">
                    <form class="woocommerce-ordering" method="get">
                        <select name="orderby" class="orderby" aria-label="Shop order">
                                                    <option value="menu_order" selected="selected">Default sorting</option>
                                                    <option value="popularity">Sort by popularity</option>
                                                    <option value="rating">Sort by average rating</option>
                                                    <option value="date">Sort by latest</option>
                                                    <option value="price">Sort by price: low to high</option>
                                                    <option value="price-desc">Sort by price: high to low</option>
                                            </select>
                        <input type="hidden" name="paged" value="1">
                                    </form>
                </div>
            </div>
        </div>


  <!-- filtered products -->
<div class="row row-sm  clearfix mt-2 cb-p2"> 
    <?php 

    if (strcasecmp($category,'shopping')==0) 
    {

    ?>
     @if( isset($products))
     @foreach ($products  as $product) 
        <?php   
                if ($product->photos=="") {
                      $image_url =  $cdn_url."/assets/image/no-image.jpg"; 
                  } else{
                      $image_url =  $product->photos;  
                  } 

                $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

         ?> 
          <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 "> 
                <div class="card card-sm card-product-grid">
                <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="img-wrap">
                    <img src="{{ $image_url }}"> 
                </a>
                <figcaption class="info-wrap">
                    <a  href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="title">{{$product->pr_name}}</a>
                    <div class="price mt-1">
                    @if($product->unit_price == $product->actual_price)
                        <p class="card-text">₹ {{$product->actual_price }}</p>
                      @else     
                        <p class="card-text">₹ {{$product->actual_price }} <span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}</span></p>
                      @endif
                  </div>   
                </figcaption>
            
            <div class='text-center' style='padding:10px'>
                @if($product->allow_enquiry == "yes")
                <a role="button" target='_blank' data-phone="8787306375" 
                 href="https://web.whatsapp.com/send?text={{ Request::url() }}&phone=+918787306375"   class="btn btn-info btn-pill  btn-sm " data-action="share/whatsapp/share" >Enquire Now</a> 
 
                 <button type="button" data-action="wish" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}"  class="btn btn-warning btn-pill btn-sm btn-wish-item   "> Add to Wishlist </button> 

                @else
                    @if(  $product->stock_inhand == 0  )
                    <button type="button"   class="btn btn-outline-primary btn-sm btn-block" disabled>Out of stock</button> 
                    @else  
                    <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}" 
                        class="btn btn-primary btn-sm btn-add-item btn-block">Add to Cart</button>
                    @endif
                    
                @endif
            
        </div>


           
            
      

             </div>                            
    </div> 
    @endforeach 
        <div class="col-md-12"> 
    {{      $products->appends(request()->input())->links()  }}
    </div> 
    @endif    
    
    <?php 
    }
    else
        {
    ?>
    
    @if( isset($products))
     @foreach ($products  as $product) 
        <?php   
                if ($product->banner=="") {
                      $image_url =  $cdn_url."/assets/image/no-image.jpg"; 
                  } else{
                      $image_url = $cdn_url.$product->banner;  
                  } 

                $formatted_name = strtolower(preg_replace('/[^A-Za-z0-9\-]/','-',trim($product->name))); 

         ?> 
          <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 "> 
            <div class="card card-product-grid">
                   <div class="wg-box-content">
                    <a href="{{URL::to('/business')}}/{{$product->merchant_code}}">
                      <div class="wg-box-content-overlay"></div>
                      <img class="img-fluid wg-box-content-image " src="{{$image_url}}">
                      <div class="wg-box-content-details wg-box-fadeIn-bottom">
                        <h4 class="wg-box-content-title text-uppercase">{{$product->name}}</h4>
                        <p class="wg-box-content-text">{{$product->locality}}</p>
                      </div>
                    </a>
                    </div>
            </div>                            
         </div> 
    @endforeach 
    <div class="col-md-12"> 
    {{$products->appends(request()->input())->links()}}
    </div> 
    @endif


    <?php
        }
    ?>               
   </div> 
 <!-- filtered products --> 
</div>
</div> 



</div> 
</div> <!-- container -->
</div> <!-- spacer -->
@endsection

@section('script')

<style>
 .wg-box-content {
  position: relative;
  width: 90%;
  max-width: 400px;
  margin: auto;
  overflow: hidden;
} 
.wg-box-content .wg-box-content-overlay {
  background: rgba(0,0,0,0.7);
  position: absolute;
  height: 54%;
  width: 100%;
  left: 0;
  bottom: 0;
  right: 0;
  opacity: 0;
  -webkit-transition: all 0.4s ease-in-out 0s;
  -moz-transition: all 0.4s ease-in-out 0s;
  transition: all 0.4s ease-in-out 0s; 
}
.wg-box-content:hover .wg-box-content-overlay{
  opacity: 1;
}
.wg-box-content-image{
  width: 100%;
}
.wg-box-content-details {
  position: absolute;
  text-align: center;
  padding-left: 1em;
  padding-right: 1em;
  width: 100%;
  top: 50%;
  left: 50%;
  opacity: 0;
  margin-top: 20px;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  -webkit-transition: all 0.3s ease-in-out 0s;
  -moz-transition: all 0.3s ease-in-out 0s;
  transition: all 0.3s ease-in-out 0s;
}
.wg-box-content:hover .wg-box-content-details{
  top: 50%;
  left: 50%;
  opacity: 1;
}
.wg-box-content-details h4{
  color: #fff;
  margin-top: 0.5em;
  font-size: 1em;
  margin-bottom: 0.5em;
  text-transform: uppercase;
}
.wg-box-content-details p{
  color: #fff;
  font-size: 0.8em;
} 
.wg-box-fadeIn-bottom{
  top: 80%;
}

</style>
@endsection