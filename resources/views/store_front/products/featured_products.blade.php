@extends('layouts.store_front_02')
@section('content')
 <?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<div class='outer-top-tm'> 
    <div class="container">  
    <div class='row'> 
        <div class='col-sm-3 col-xs-12   sidebar'>

 

<div class="sidebar-widget   widget_price_filter">
    <h3 class="section-title">Sharing is Caring</h3> 
    <div class="row"> 
    <div class="col"> 
        <p class=''><i class='fa fa-share-alt-square fa-3x light-green' onclick="copyUrl();"></i></p>
    </div>
    <div class="col mt-3 d-block d-sm-none"> 
        <button class="btn btn-warning float-right btn-sm showMerchantList"> 
         <span class="fa fa-filter"></span> View Merchant 
        </button>
 
    </div> 

    </div> 

       
</div>



<div class="sidebar-widget widget_product_categories d-none d-lg-block d-xl-block">
    <h3 class="section-title">All Merchants</h3>
    <ul class="product-categories">  
@foreach ($businesses as $business)
         <li class="cat-item">
                    <a href="{{ URL::to('/business') }}/{{ strtolower(  $business->merchant_code ) }}">{{$business->name}}</a>  
                </li>
            @endforeach 
</ul> 
</div> 

</div>

 
 
<div class='col-sm-9 col-xs-12 col-md-9 col-mlg-9'>
    <div class='filters-container'>
        <div class="clearfix  ">
            <div class="row">
                <div class="col col-md-12 text-right">
                    <form class="woocommerce-ordering" method="get">
                        <select name="orderby" class="orderby" aria-label="Shop order">
                                                    <option value="menu_order" selected="selected">Default sorting</option>
                                                    <option value="popularity">Sort by popularity</option>
                                                    <option value="rating">Sort by average rating</option>
                                                    <option value="date">Sort by latest</option>
                                                    <option value="price">Sort by price: low to high</option>
                                                    <option value="price-desc">Sort by price: high to low</option>
                                            </select>
                        <input type="hidden" name="paged" value="1">
                                    </form>
                </div>
            </div>
        </div>


  <!-- filtered products -->
<div class="row row-sm  clearfix mt-2 cb-p2"> 
 

                @if( isset($products))
                    @foreach ($products  as $product) 
               
            <?php   
 
                if ($product->photos == "") {
                    $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                }else{
                    $image_url =  $product->photos;
                }

                $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

            ?>     
         <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 "> 
                <div class="card card-sm card-product-grid">
                <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="img-wrap">
                    <img src="{{ $image_url }}"> 
                </a>
                <figcaption class="info-wrap">
                    <a  href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="title">{{$product->pr_name}}</a>
                    <div class="price mt-1">
                    @if($product->unit_price == $product->actual_price)
                        <p class="card-text">₹ {{$product->actual_price }}</p>
                      @else     
                        <p class="card-text">₹ {{$product->actual_price }} <span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}</span></p>
                      @endif
                  </div>  
 

                </figcaption>
       

        <div class='text-center' style='padding:10px'>
            @if($product->allow_enquiry == "yes")
                <a role="button" target='_blank' data-phone="8787306375" 
                 href="https://web.whatsapp.com/send?text={{ Request::url() }}&phone=+918787306375"   class="btn btn-info btn-pill  btn-sm " data-action="share/whatsapp/share" >Enquire Now</a> 
 
                 <button type="button" data-action="wish" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}"  class="btn btn-warning btn-pill btn-sm btn-wish-item   "> Add to Wishlist </button> 

                @else

                        @if(  $product->stock_inhand == 0  )
                        <button type="button"   class="btn btn-outline-primary btn-sm btn-block" disabled>Out of stock</button> 
                        @else  
                        <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}" 
                            class="btn btn-primary btn-sm btn-add-item btn-block">Add to Cart</button>
                        @endif
                @endif
            
        </div>
             </div>                            
    </div>

    @endforeach

    <div class="col-md-12"> 
    {{      $products->appends(request()->input())->links()  }}
    </div> 
    @endif 
        
    </div> 
 <!-- filtered products --> 
</div>
</div> 
</div> 
</div> <!-- container -->
</div> <!-- spacer -->

    <!-- Sidebar Overlay -->
<div class="sidenav" id="mySidenav" style="">
    <div class="row ">
        <div class="col-12 "> 
            <div class="card-title">
            <h5 class="pl-3 mt-3 title-merchant">All Merchants</h5>
            <a href="javascript:void(0)" class="closebtn showMerchantListX">×</a>
        </div>
         
    </div> 
    </div>
    <hr>
    
    <ul class="list-unstyled components links"> 
            
            @foreach ($businesses as $business)
                <li class="cat-item">
                    <a href="{{ URL::to('/business') }}/{{ strtolower(  $business->merchant_code ) }}"><small>
                    {{$business->name}}
            </small></a>  
                </li>
            @endforeach

            
             
          </ul>       
         <!--   -->
         <hr>
</div>
     <!--  -->

@endsection

@section('script')
<style type="text/css"> 
  
</style>  


<script type="text/javascript"> 

</script>
@endsection