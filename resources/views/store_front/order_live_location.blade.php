@extends('layouts.store_front_mobile_first_white')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>




@if(isset($order_info))

  @if( $order_info->book_status == "delivered" || $order_info->book_status == "canceled"  )


   <div class="row "> 
    <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-2"  > 
        <p>Thank you for choosing bookTou. Your order is closed.</p>

     </div> 
    </div> 
  </div> 
 

  @else 
 
<div class="row "> 
    <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-2"  > 
        <strong>Hello {{ ucwords( $order_info->fullname) }},</strong><br/>
        <p>We received an order from {{ $order_info->business }} to deliver a package.</p>

     </div>
    </div>
  </div>
  <div class="mt-2" style='border-bottom:1px dashed #efefef;'></div> 

  <div class="row "> 
    <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-2"  > 
        <strong>Order #: {{ $order_info->id }}</strong>
     </div> 
    </div> 
  </div> 

   
  <div class="mt-2" style='border-bottom:1px dashed #efefef;'></div>

  <div class="row "> 
    <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-2"  > 
        <strong>Delivery Location:</strong><br/>
         {{  ucwords(  $order_info->address ) }} 
     </div> 
    </div> 
  </div> 
  <div class="mt-2" style='border-bottom:1px dashed #efefef;'></div>
 

<div class="row "> 
     <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-2"  > 
        <strong>Delivery From: </strong><br/>
        {{  $order_info->business }}<br/>
        {{ $order_info->locality }}
     </div> 
    </div> 
  </div> 
 <div class="mt-2" style='border-bottom:1px dashed #efefef;'></div> 

  

<div class="row "> 
     <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-2"  > 
        <strong>Current Status: </strong> 
        @switch($order_info->book_status)

                      @case("new")
                        <span class='badge badge-primary'>Order Placed</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery scheduled for your package.</span>
                      @break 

                        @case("canceled")
                        <span class='badge badge-danger'>Cancelled</span>
                      @break 

                      @endswitch

     </div> 
    </div> 
  </div> 
 <div class="mt-2" style='border-bottom:1px dashed #efefef;'></div> 

  @if(isset($agent_info))
   <div class="row "> 
       <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
        <div class="mt-2"  > 
            <strong>Delivery Agent: </strong><br/>{{  $agent_info->fullname }}<br/>
            <strong>Phone #: </strong> <a href="tel:{{  $agent_info->phone }}">{{  $agent_info->phone }}</a>
       </div> 
      </div> 
    </div>  
   
   <div class="mt-2" style='border-bottom:1px dashed #efefef;'></div>

   @endif
 @endif
 
   
@endif
  
  @php 
    $slideimages = array();
  @endphp

  @foreach($slides as $item)
    @php 
      $slideimages[] = $item->path; 
    @endphp
  @endforeach
 
  @php 
    shuffle($slideimages); 
  @endphp

<div class="row "> 
     <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4">  
        <a href="{{ URL::to('/') }}">
          <img class="d-block w-100" src="{{  $slideimages[0] }}" > 
        </a>
    </div> 

<div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 text-center mt-2 ">  
  <hr/>
  <h4>Have no account yet?<br/>
  Seems like we can have great experience together.</h4>
  <a href="https://booktou.in/join-and-refer" class="btn btn-info btn-rounded">Signup Here</a><br/>

  <h4>Already have an account?</h4>
  <a href="https://booktou.in/shopping/login" class="btn btn-success btn-rounded">Login Instead</a>

</div> 
</div>  

<div class="row mb-4"> 
     <div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4 ">  
      <div class="mt-5 text-center"  > 
         <a href='https://bit.ly/2Ln8C1Y' target='_blank'>
           <img src="{{ URL::to('/public/assets/image/download-booktou.png') }}" width='200px' alt='Download bookTou Android App'/>
    </a>
     </div> 
    </div> 
  </div> 

  

@endsection 

@section('script')

  

<script>
 
 
</script>

@endsection 