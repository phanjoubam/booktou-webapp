@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<div class='outer-top-tbm'> 
<div class="container">
<div class="row">
   <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3">
                    <div class="card"> 
                         <div class="card-body">
                        <div class="cart-page-heading mb-30">
                            <h1>Customer Login</h1>
                            <hr/>
                        </div>

                        <form action="{{ action('Auth\LoginController@customerLogin') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                
                                <div class="col-12 mb-3">
                                    <label for="phone">Phone Number or Email: <span>*</span></label>
                                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email'>
                                </div>
                               
                               
                                <div class="col-12 mb-4">
                                    <label for="email_address">Password: <span>*</span></label>
                                    <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password'>
                                </div>
                                <div class="col-12 mb-4">
                                <button type="submit" value='login' name='login' class="btn btn-primary btn-rounded">Login</button>

                                <a  class="btn btn-danger btn-rounded" href="{{ URL::to('/') }}">Cancel</a>
                                <hr/>

                                <h5 class='text-center'>Don't have an account yet? Signup <a href="{{ URL::to('/join-and-refer') }}" class="btn-link">here</a></h5>



 </div>

                            </div>
                        </form>
                    </div>
                    <div class='card-footer text-center'>
<h5>Forgot your password or having trouble signin? 
    <a href="{{ URL::to('/forgot-password') }}" class="btn-link">Click here to get help.</a></h5>
                    </div>
                    </div>
                </div>
</div>


</div>
</div>

@endsection 