@extends('layouts.store_front_no_menu')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>

<div class='outer-top-tbm'>  
<div class="container">
<div class="row">
   <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3"> 
        <div id="menuslider" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                @php $i=0; @endphp
                @foreach($menu_images as $item)
                    @if($i == 0)
                        <div class="carousel-item   active">
                    @else 
                        <div class="carousel-item ">
                    @endif 
                  <img src="{{ $item->url }}" class="d-block" alt="Menu Page {{ $item->page_no }}">
                </div>

                @php $i++; @endphp
                @endforeach
               
              </div>
              <a class="carousel-control-prev" href="#menuslider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#menuslider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>

   </div>
</div>
</div>
</div>
</div>
@endsection 