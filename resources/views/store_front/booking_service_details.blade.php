@extends('layouts.store_front_02')
@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ;

?>
<!-- calculation for booking -->
<?php

$taxes = 0;
$price = 0;
$total= 0;

$actualamount = 0;
$offer_amount =0;

$offer_total = 0;

$price = $services->actual_price;
$total = $price+$taxes;


$actualamount = $services->actual_price;
$offer_amount = $services->pricing;

$offer_total = $actualamount - $services->discount;
?>

 @auth


 <!-- if service category is offer -->
@if($services->service_category=="OFFER")

 
    <div class="container">
    <div class='row'>
            <?php
                $mainmenu;
                  foreach(request()->route()->parameters as $val)
                  {
                       $mainmenu =  $val;
                  }


            ?>
  <div class='col-sm-8 col-xs-12 '>

    <div class="card">
        <div class="card-header">
          <h5>{{$services->service_category}} Valid from {{date('Y-m-d',strtotime($services->valid_start_date))}} to  {{date('Y-m-d',strtotime($services->valid_end_date))}} </h5>
        </div>
    <div class="card-body">
    <table class="table">

        <tr>
            <td><img src="{{$services->photos}}" width="70px" height="70px"></td>
            <td>{{$services->srv_name}}</td>
            <td>{{$services->srv_details}}</td>
            <td> ₹ {{$services->pricing}}</td>
        </tr>
    </table>
    </div>
    </div>

  </div>

    <div class='col-sm-4 col-xs-12'>

    <div class="card">
        <div class="card-header text-uppercase b" style="background: #37b516; color: #fff;">
        Order Summarys
        </div>
        <div class="card-body">
                <form method="post" action="{{action('StoreFront\StoreFrontController@purchaseServiceOffers')}}">
                  {{csrf_field()}}
                    <input type="hidden"  id="subModule" name="subModule" required="" value="{{$business->sub_module}}">
                    <input type="hidden"  id="totalCost" name="totalCost" required="" value="{{$offer_amount}}">
                    <input type="hidden" value="{{$services->id}}" id="srvkey" name="serviceProductIds[]" required="">
                    <input type="hidden" value="1" id="qty" name="productQty[]" required="">
                   <!--  <dl class="dlist-align">
                        <span class="badge badge-success mr-3">Shop Name</span>
                        <br>
                        <span class=""> {{$business->name}}</span>

                    </dl>
                    <hr> -->

                    <dl class="dlist-align">
                        <dt>Price:</dt>
                        <dd class="text-right text-dark ml-3 text-underline"> ₹ {{$actualamount}}</dd>
                    </dl>
                    <dl class="dlist-align">
                        <dt>Discount:</dt>
                        <dd class="text-right text-dark ml-3"> ₹ {{$services->discount}}</dd>
                    </dl>
                    <hr>
                    <dl class="dlist-align">
                        <dt>Total Amount:</dt>
                        <dd class="text-right text-dark ml-3"> ₹ {{$offer_amount}}</dd>
                    </dl>
                    <dl class="dlist-align">
                        <dt>Taxes:</dt>
                        <dd class="text-right text-dark ml-3"><strong>₹ {{$taxes}}</strong></dd>
                    </dl>

                    <hr>

                    <dl class="dlist-align">
                        <dt>To pay:</dt>
                        <dd class="text-right text-dark ml-3"><strong>₹ {{$offer_amount + $taxes}}</strong></dd>
                        <input type="hidden" name="serviceDate" id="offerServiceDate" required="">
                        <input type="hidden" name="serviceTimeSlots" id="serviceTimeSlots" required="">
                        <input type="hidden" name="bin" id="bin" value="{{$business->id}}" required="">
                    </dl>
                    <hr>
                    <dl class="dlist-align">
                        <input type="text" name="custRemarks" placeholder="customer Remarks"
                        class="form-control">
                    </dl>

                    <hr>
                    <input type='hidden' value="{{ $business->id }}"  name="key" />
                    <button type="submit" class="btn btn-primary btn-block btn_bookcheckout" value="btn_placeOrder" name="btnorder">Book Now</button>
                </form>
        </div>
    </div>


    </div>
    </div>

    </div>
    </div>



<!-- if service is other than offer -->
@else
 
    <div class="container mt-3">
    <div class='row'>

  <div class='col-xs-12 col-sm-12 col-md-4  col-lg-4'>

    <div class="card">
    <div class="card-header text-uppercase b" style="color: #343434;">
      <span class="badge badge-info badge-step">1</span> Available Services
    </div>
    <div class="card-body">
      @if( isset($service_products))  
        @foreach($service_products as $item) 
 
        <div class='row'>
            <div class='col-md-10'>

          <div class="d-flex mb-3">
              <div class="flex-shrink-0">
                @if( $item->photos != "") 
                    @php
                    $imageurl = $item->photos;
                    @endphp
                    <img src="{{ $item->photos }}" class="align-self-end mr-3" width="60px" alt="{{ $item->srv_name }}">  
                @else 
                    @if( count($service_profiles) > 0) 
                        @foreach($service_profiles as $service_profile)
                            @if(  $service_profile->service_product_id  == $item->id  )
                                <img src="{{ $service_profile->photos }}" class="align-self-end mr-3" width="60px" alt="{{ $item->srv_name }}">
                                @break
                            @endif 
                        @endforeach  
                    @endif 
                @endif
                </div>
                <div class="flex-grow-1 ms-3">
                    <h6 class="mt-0 mb-1">{{ $item->srv_name }}</h6> 
                </div>
             </div>
         </div>
<div class='col-md-1 text-right'>
                <div class="flex-grow-1 ms-3 ">
                    <button   class='btn btn-sm btn-outline-dark btnselectitems' 
                        data-key="{{ $item->id  }}" 
                        data-srvname="{{ $item->srv_name }}" data-price="{{ $item->actual_price }}" 
                        data-details="{{ $item->srv_details }}"><i class='fa fa-plus'></i></button>
                </div> 
            </div>
  </div>  
        @endforeach    

      @else

      @endif

</div>
</div>


    </div>
     <div class='col-xs-12 col-sm-12 col-md-3 col-lg-3'>
            <?php
                $mainmenu;
                  foreach(request()->route()->parameters as $val)
                  {
                       $mainmenu =  $val;
                  }

            ?>

            <div class="card">
            <div class="card-header text-uppercase b" style="color: #343434;"> 
              <span class="badge badge-info badge-step">2</span> Booking Date 
            </div>
            <div class="">
            <div class="calendar dateselection btn_select_slots"></div>
            </div>
            </div>

  </div> 

  <div class='col-sm-5 col-xs-12 col-md-5 mb-4'>

    <div class="card">

      <div class="card-header text-uppercase b" style="color: #343434;">
          <span class="badge badge-info badge-step">3</span> Booking Summary
      </div>
        <div class="card-body">
      @if (session('err_msg'))

       <div class="alert alert-info">
        {{ session('err_msg') }}
       </div>

       
      @endif
      <form method="post" action="{{action('StoreFront\StoreFrontController@makeServiceBooking')}}" enctype="multipart/form-data">

            {{csrf_field()}} 

              <div class="row">
            @if(Session::get('module_active')=="booking") 
                <div class="col-md-12">  
                <div id='de_service_hours'> 
                </div> 
                </div> 
 
                <div class="form-group col-md-6">  
                <p for="appt"><strong>Your preferred time of visit:</strong></p>
                <input type="text" id="preferredTime" class="form-control form-control-sm flex" name="preferredTime" placeholder="08:00:AM" required>  

                </div>

            @elseif(Session::get('module_active')=="appointment")
            <div class="err_msg"></div>
            <div class="card showSlots col-md-12 col-sm-12"> 

            </div> 
            @endif
              </div> 
              
                <hr> 

                <h5>Services Selected</h5>
                <hr>
                   @if(Session::get('module_active')=="booking")
                    <!-- <input type="hidden"  id="preferredTime" name="preferredTime"  required=""> -->
                   @endif
                    <input type="hidden"  id="endtimeslot" name="endTime" required="">
                    <input type="hidden"  id="staffId" name="staffId" required="">
                    <input type="hidden"  id="subModule" name="subModule" required="" value="{{$business->sub_module}}">
                   <!--  <dl class="dlist-align">
                        <span class="badge badge-success mr-3">Shop Name</span>
                        <br>
                        <span class=""> {{$business->name}}</span>

                    </dl>
                    <hr> -->
                    <div class="addservicelist">
                    <dl class="dlist-align pb-3">
                        <dt>{{$services->srv_name}}</dt>
                        <dd class="text-right text-dark ml-3">₹ {{ number_format($price,2, ".", "")}}</dd>
                         <input class="serviceproducts srvkey{{$services->id}}" type="hidden" value="{{$services->id}}" id="srvkey" name="serviceProductIds[]" required="">
                    </dl>
                    </div>
                    <div>
                    <dl class="dlist-align">
                        <dt>Total:</dt>
                        <input type="hidden" id="total" value="{{$total}}">

                        <dd class="text-right text-dark ml-3"> ₹ <label id="totalamount">{{number_format($total,2,".","")}}</label></dd>
                    </dl>
                    </div>
                    <div>
                    <dl class="dlist-align">
                        <dt>Taxes:</dt>
                        <dd class="text-right text-dark ml-3">
                            <strong>₹ <label id="tax">{{number_format($taxes,2,".","")}}</label></strong></dd>
                    </dl>
                    </div>
                    <hr>
                    <div>
                    <dl class="dlist-align">
                        <dt>To pay:</dt>
                        <dd class="text-right text-dark ml-3">
                            <strong>₹ <label id="subamount">{{number_format($total,2,".","")}}</label></strong></dd>
                            <input type="hidden" id="subtotal" value="{{$total}}">
                        <input type="hidden" name="serviceDate" id="bookingServiceDate" required="">
                        <input type="hidden" name="serviceTimeSlot" id="serviceTimeSlots" required="">
                        <input type="hidden" name="bin" id="bin" value="{{$business->id}}" required="">
                    </dl>
                    </div>
                    <hr>
                    <div>
                    <dl class="dlist-align">
                        <input type="text" name="bookingRemark" placeholder="Booking Remarks"
                        class="form-control">
                    </dl>
                    </div>
                    <hr>
                    <div>
                        <dl>
                        <input type="file" name="attachment"   class="form-control" placeholder="add an attachment" >
                        </dl>
                    </div>
                    <hr>
                    <!-- <dl class="col text-center">
                     <input class="form-check-input pod" disabled="true" type="radio"  data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" name="paymode" value="online">
                     <label class="form-check-label m5">Pay Online</label>
                   </dl>
                   <hr> -->
                   <input type='hidden'  value="{{ $business->id }}"  name="key" />
                    <button type="submit" class="btn btn-primary btn-block btn_bookcheckout" value="btn_placeOrder" name="btnorder" disabled>Book Now</button>
                </form>
        </div>
    </div>


    </div>
    </div>

    </div> 

@endif
<!-- service checking ends here -->

@endauth

@guest
<div class='outer-top-tm'>
    <div class="container">
      <div class="col-md-6 offset-md-3">
      <form action="{{action('Auth\LoginController@customerLogin')}}" method="post">
        {{ csrf_field() }}
     <input type="hidden" id="srvukey" name="referer" value="{{ $services->id }}">
     <input type="hidden"  name="key" value="{{ $business->id }}">

     <input type="hidden" id="module_name" name="module_name"
     value="<?php echo session()->get('module_active'); ?>" required>
     <div class="card">
       <div class="card-header">
         <h5 class="modal-title" id="staticBackdropLabel">Customer Login</h5>
       </div>
       <div class="card-body">
         <div class="row">
          <div class="col-12 mb-3">
                                     <label for="phone">Phone Number or Email: <span>*</span></label>
                                     <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email'>
                                 </div>


                                 <div class="col-12 mb-4">
                                     <label for="email_address">Password: <span>*</span></label>
                                     <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password'>
                                 </div>
                             </div>
       </div>
       <div class="card-footer">

         <div class="row col-12">
        <div class=" col-4">
         <button type="submit" class="btn btn-primary btn-block" value="login" name="login">Login</button>
        </div>

        <div class=" col-8">
             <p class="mt-1">
            <small>New Member? Please signup here</small>
            <a target="_blank" class="btn btn-info btn-sm" href="{{URL::to('join-and-refer')}}">Sign up</a>
         </p>

         </div>
         </div>

       </div>
     </div>
     </form>
</div>
    </div>
</div>
@endguest


<!-- spinner -->
<div id="cover-spin"></div>



<!-- modal for service category -->


<div class="modal fade serviceModal" id="serviceModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">

    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
            <span class="badge badge-primary badge-pill">Service Provided by {{$business->name}}</span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-md-6">
                  <select name="serviceCategory" class="form-control serviceCategory" required>
                      <option value="">Select Service Category</option>
                      @foreach($service_products as $item)
                        <option value="{{$item->service_category}}">{{ $item->service_category}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-md-6">
                    <input type="text" name="searchFilter" class="form-control filterservice" placeholder="Filter service name">
                </div>



             <div class="col-md-12 serviceNameResult" id="loading">


             </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- modal service category -->

@endsection

@section('script')
<style>


</style>

<script>
  

$(function(){
    $('[data-toggle="tooltip"]').tooltip();
})

var d = '/api';

var siteurl = "<?php echo config('app.api_url');  ?>"  ;
 
var today = new Date();
var yesterday = new Date(today);
yesterday.setDate(today.getDate() -1);
var current_date = yesterday.toISOString().split('T')[0];

$('.dateselection').pignoseCalendar({
   theme: 'light',
    disabledRanges: [
        ['2000-01-01', current_date]
    ],
    select: function(date) {
    startloading();
    var selectedDate = date[0].format('YYYY-MM-DD');

    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var d = new Date(selectedDate);
    var day = days[d.getDay()];

    var dayName = day.toUpperCase();
    var guid = $("#guid").text();
    $("#bookingServiceDate").val(selectedDate);
    <?php
    $business = $business->id;
    ?>

    var json = {};
    json['dayName'] =  dayName;
    json['guid'] =  guid;
    json['date'] = selectedDate;
    json['bin'] = <?php echo $business;?>;
    json['month'] = <?php echo date('m');?>;
    json['year'] = <?php echo date('Y');?>;

    var productlistid = [];
    $(document).find("input[id=srvkey]").each(function() {
        productlistid.push($(this).val());
    });

    json["serviceProductIds"] = productlistid ;  

    <?php

        

      if(session()->get('module_active')=="booking"):
        ?>
 

        $.ajax({
          type: 'post',
          url: siteurl + "/v3/web/booking/check-date-availability" ,
          data: json,
          success: function(data)
          {
            if(data.status_code == 3021)
            {
              $("#de_service_hours").html(""); 
              $("#de_service_hours").append("<p><strong>Service Day:</strong> " + data.day_name + "</p>" );
              $("#de_service_hours").append("<hr/>"); 
              $("#de_service_hours").append("<p><strong>Service Hours:</strong><br/>");  
              $.each(data.business_hours, function (i, v) {
                $("#de_service_hours").append("<span class='tsbox-gray-xs service_hour'>" + v.shifts+ "</span>"); 
              });
              $("#de_service_hours").append("</p>");  

              $(".btn_bookcheckout").prop("disabled", true);
              exitloading();
            }
            else
            {
              //$(".err_msg").empty().append('<p style="color:red;">'+data.detailed_msg +'</p>');
              //$(".staffList").empty();
              //$(".showslotstiming").empty();
              exitloading();
            }
          },
          error: function()
          {
            alert(  'Something went wrong, please try again')
          }
        });

      <?php
      endif;
    ?>
     

     // appointment section

    <?php 
    if(session()->get('module_active')=="appointment"):
    ?>
    $.ajax({
         type: 'post',
         url: siteurl + "/v3/web/appointment/business/get-day-slots" ,
         data: json,
         success: function(data)
         {
             if(data.status_code == 7007)
                     {   
                         $(".showSlots").empty().append(data.html);
                         $(".staffList").empty();
                         $(".btn_bookcheckout").prop("disabled", true);
                         $(".showslotstiming").empty();
                         exitloading();
                     }
                     else
                     {
                         $(".showSlots").empty();
                         $(".err_msg").empty().append('<p style="color:red;">'+data.message+'</p>');
                         $(".staffList").empty();
                         // $(".showslotstiming").empty();
                         exitloading();
             }

         },
         error: function( )
         {
             alert(  'Something went wrong, please try again')
         }
        });
    <?php
      endif;
    ?>
    }
});


$(document).on('click','.btn-showservice',function(){
  $('.serviceModal').modal('show');
}); 


$('#preferredTime').timepicker({ 'timeFormat': 'h:i A' });


$('#preferredTime').on('change', function() { 
  var pref_time = $(this).val();  
  var ts_preftime = new Date("<?php echo date('F d, Y') ?> " + pref_time);
  ts_preftime = ts_preftime.getTime();
 

  var sh = $(".service_hour").html();
  var time_parts = sh.split("-");
  if(time_parts.length >= 2 )
  {
    var start_time =  time_parts[0];
    var end_time =  time_parts[1];
    //convert both time into timestamp
    var ts_start = new Date("<?php echo date('F d, Y') ?> " + start_time);
    ts_start = ts_start.getTime();
    var ts_end = new Date("<?php echo date('F d, Y') ?> " + end_time);
    ts_end = ts_end.getTime();

    if(ts_start <= ts_preftime &&  ts_preftime <= ts_end) {
      $(".btn_bookcheckout").prop("disabled", false);
    }
    else
    {
      $(".btn_bookcheckout").prop("disabled", true);
    }

  }  
});


$(document).on("click", ".btnselectitems", function()
 {

    var key = $(this).attr('data-key');
    var srvname = $(this).attr('data-srvname');
    var details = $(this).attr('data-details');
    var price = $(this).attr('data-price');
    var total = $('#total').val();

    if($('.srvkey'+key).val() == key ){
        alert("service already in the list! please add another service.");
        return;
    }

    var subamount = parseFloat(price) + parseFloat(total) ;

    $('#total').val(subamount);
    $('#subtotal').val(subamount);
    $('#totalamount').text(subamount);
    $('#subamount').text(subamount);
    $('.addservicelist').last().append('<dl class="dlist-align"><dt>'+srvname+'</dt>'+
                        '<dd class="text-right  ml-3"><a href="javascript::void();" class="badge badge-danger btnremove mr-3" data-price="'+price+'">X</a>₹ '+price+'</dd><input type="hidden"'+
                        'value="'+key+'" class="srvkey'+key+'" id="srvkey" name="serviceProductIds[]" required=""></dl>'
                         );
});

$(document).on("click", ".btnremove", function(){

   var price = $(this).attr('data-price');
   var total = $('#subtotal').val();

    var subamount = total - price ;
    $('#total').val(subamount);
    $('#subtotal').val(subamount);
    $('#totalamount').text(subamount);
    $('#subamount').text(subamount);

   $(this).closest('dl').remove();

});
</script>
<style type="text/css">
.btn-primary.disabled,
.btn-primary:disabled {
 color:#000000 !important;
 background-color:#efefef !important;
 border-color:#fff !important;
}

.how-it-work {

    position: relative;
}

.how-it-work .panel {
    color:#fff;
    font-size: 16px;
    background: #37b516;
   /* box-shadow: 0px 1px 7px 1px rgba(0,0,0,0.4);
   */
}
.how-it-work .panel:nth-child(even) {
    background: #78c043;
}
.how-it-work .panel-body {
    overflow-y: auto;
    overflow: visible;
    height: 70px;
    padding-left: 70px;
    position: relative;
}

.badge.badge-info.badge-step
{
  width: 35px;
  height: 35px;
  color: #008931;
  background: #f6f6f6;
  text-align: center;
  font-size: 1.3em;
  font-weight: 800;
  border-radius: 50%;
  box-shadow: 0px 2px 10px rgb(0 0 0 / 40%);
}


.how-it-work .step-heading {
    font-size: 20px;
    margin-top: 0px;
    color: #fff;
    padding-top:5px;
}

.dateselection .pignose-calendar
{
    max-width: 100% !important;
    border: none !important;
    box-shadow: none !important;
}

.dateselection .pignose-calendar .pignose-calendar-top
{
  padding: 1em 0 !important;
}

.dateselection .pignose-calendar .pignose-calendar-top .pignose-calendar-top-month
{
  font-weight: normal;
}

.dateselection span.pignose-calendar-top-month
{
  display:inline !important;
  font-size: 110% !important;
}

.dateselection span.pignose-calendar-top-year {
    display: inline !important;
}

.dateselection .pignose-calendar-top-date {
    padding: 1em 0 !important;
}

.list-service li
{
  border-bottom:1px solid #efefef;
  margin-bottom: 10px;
}
.list-service img
{
  border-radius: 10px;
}
.addservicelist dl
{
  border-bottom:1px solid #efefef;
  margin-bottom:15px; 
}

span.tsbox-gray-xs {
    display: inline-block;
    border: 1px solid #cec9c9;
    padding: 5px;
    border-radius: 3px;
    font-size: 12px;
    margin-right: 10px;
}

</style>
@endsection
