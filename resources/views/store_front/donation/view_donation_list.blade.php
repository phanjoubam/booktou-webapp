@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
   


?> 

 
 <div class='outer-top-tm mb-3'>
 <div class="container">
<section class="section-main  ">
<div class="card">
<div class="card-body">
 
<div class="col">
	<h3 class=" badge badge-primary">Donor List</h3>
<table class="table ">

	<thead>
		<th>Name</th>
		<th>Address</th> 
	</thead>

	@foreach($data['donor'] as $list)	
		<tbody>
			<tr>
				<td>{{$list->name}}</td>
				<td>{{$list->address}}</td>
			</tr>
		</tbody>
	@endforeach
</table>	
</div>



  
</div>
</div>
</section>

</div>

</div>
@endsection 

