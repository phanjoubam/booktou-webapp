<div class="form-group">
 			<div class="form-row">
			<div class="col-6">
				<label>Name</label><input type="hidden" value="person name" name="person_name_id">
				<input type="text" class="form-control" name="person_name" required="" id="personName" />
			</div>
			<div class="col-6">
				<label>Phone No</label><input type="hidden" value="person phone" name="person_phone_id">
				 <input type="number" class="form-control" name="person_phone" required="" id="personPhone" 
				 maxlength="10" 
				 oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/> 
			</div>
			</div>
</div>

<div class="form-group">
 			<div class="form-row">
 			<div class="col-6 col-xs-12">
				<label>Address</label><input type="hidden" value="person address" name="person_address_id">
				 <input type="text" class="form-control" name="person_address" required="" id="personAddress"> 
			</div> 
			<div class="col-6 col-xs-12">
				 <label>Category</label>
				<input type="hidden" value="category" name="category_id">
			    <select class="form-control" name="person_category" id="personCategory">
			    	<option value="">--Select Category--</option> 
			    	<option value="donor">Donor</option> 
			    	<option value="source">Source</option> 

			    </select>
			</div> 
			 
</div>
</div>

<!-- <div class="form-group">
<div class="form-row">
<div class="col">
	<label>Items list</label>
	<textarea class="form-control" name="person_items" id="personItems"></textarea>   
					 
</div> 
</div>
</div> -->

<div class="form-group">
<div class="form-row">
<div class="col">
	<input type="checkbox"   name="person_toinclude" value="yes">
	<label>Please check the box if you want to display your name in the donor list</label>				 
</div> 
</div>
</div>