


<div class="form-group">
 			<div class="form-row">
			<div class="col">
				<label>Donor Name</label><input type="hidden" value="donor name" name="donor_name_id">
				<input type="text" name="donorname" class="form-control" id="donorName">
			</div>

			<div class="col">
				<label>Donor Phone</label><input type="hidden" value="donor phone" name="donor_phone_id">
				<input type="number" name="donorphone" class="form-control" maxlength="10"
			      oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="donorPhone">
			</div>
			
			</div>
</div>

<div class="form-group">
	<div class="form-row">
		<div class="col">
				<label>Donee Name</label><input type="hidden" value="donee name" name="donee_name_id">
				<input type="text" name="doneename" class="form-control" id="doneeName">
			</div>

			<div class="col">
				<label>Donee Phone</label><input type="hidden" value="donee phone" name="donee_phone_id">
				<input type="number" name="doneephone" class="form-control" maxlength="10"
			      oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="doneePhone">
			</div>
	</div>

</div>


<div class="form-group">
	<div class="form-row">
		<div class="col">
				<label>Donee Address</label><input type="hidden" value="donee address" name="donee_address_id">
				<input type="text" name="doneeaddress" class="form-control" id="doneeAddress">
			</div>
		<div class="col">
				<label>Donation Type</label><input type="hidden" value="type" name="type_id">
				 <select name="donation_type" class="form-control donation_type"  onchange="showtextbox(this);" >
					
					<option value="">-Select type of donation-</option>
					<option value="cash">cash</option>
					<option value="item">item</option>
					 
				</select>
			</div>

			
	</div>

</div>


<div class="form-group">
	<div class="form-row" id="type">
		
	</div>
</div>

 