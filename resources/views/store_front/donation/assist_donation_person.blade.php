<div class="form-group">
 			<div class="form-row">
			<div class="col">
				<label>Name</label><input type="hidden" value="person name" name="person_name_id">
				<input type="text" name="person_name" class="form-control" id="personName">
			</div>

			<div class="col">
				<label>Phone No</label><input type="hidden" value="person no" name="person_phone_id">
				<input type="number" name="person_phone" class="form-control" maxlength="10"
			      oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="personPhone">
			</div>
			
			</div>
</div>

<div class="form-group">
	<div class="form-row">
		
<div class="col">
				<label>Address</label><input type="hidden" value="address" name="person_address_id">
				 <input type="text" class="form-control" name="person_address" id="personAddress"/> 
			</div>

			<div class="col">
				 <label>Category</label>
				<input type="hidden" value="category" name="category_id">
			    <select class="form-control" name="person_category" id="personCategory">
			    	<option value="">--Select Category--</option> 
			    	<option value="donor">Donor</option> 
			    	<option value="source">Source</option> 

			    </select>
			</div>

	</div>

</div>

 