@extends('layouts.store_front_02')
@section('content')
 <?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<div class='outer-top-tm'> 
    <div class="container">  
    <div class='row'> 
        <div class='col-sm-3 col-xs-12 sidebar col-md-3 order-lg-1 order-sm-2'>

 

<div class="sidebar-widget   widget_price_filter">
    <h3 class="section-title">Sharing is Caring</h3> 
    <div class="row"> 
    <div class="col"> 
       <div class="addthis_inline_share_toolbox"></div>
    </div>
    <div class="col mt-3 d-block d-sm-none"> 
        <button class="btn btn-warning float-right btn-sm showMerchantList"> 
         <span class="fa fa-filter"></span> View Merchant 
        </button>
 
    </div> 

    </div> 

       
</div>



@if( count($businesses) > 0 )
<div class="sidebar-widget widget_product_categories d-none d-lg-block d-xl-block">
    <h3 class="section-title">Verified Merchants</h3>
    <ul class="product-categories">  
@foreach ($businesses as $business)
        <li class="cat-item">
            <a href="{{ URL::to('/business') }}/{{ strtolower(  $business->merchant_code ) }}">
               <img src="{{ URL::to('/public/store/image/bluetick.png') }}" width="15px" alt='verified' /> 
                {{$business->name}}</a>  
        </li>
            @endforeach 
</ul> 
</div> 

@endif

@php 
  $selected_categories = array();
  $i=0;
@endphp

@foreach ($product_categories_for_menu as $product_category) 
    @if( $i > 10 )
        @break
    @endif 
    @php 
      $selected_categories[] = $product_category->category_name ;  
      $i++;
    @endphp 

@endforeach 


@if( count($selected_categories) > 0 )
<div class="sidebar-widget widget_product_categories d-none d-lg-block d-xl-block">
    <h3 class="section-title">Product Categories</h3>
    <ul class="product-categories">  
    @foreach ($selected_categories as $product_category)
        <li class="cat-item">
            <a href="{{ URL::to('/shop-by-menulist') }}/{{ strtolower(  $bizcategory) }}/{{ strtolower($product_category) }}">
                {{ ucwords( $product_category)  }}</a>  
        </li>
    @endforeach 
</ul> 
</div>  
@endif
 


</div>

 
 
<div class='col-sm-9 col-xs-12 col-md-9 col-mlg-9 col-sm-pull-3 order-lg-2 order-sm-1'>
    <div class='filters-container'>
        <div class="clearfix  ">
            <div class="row">
                <div class="col col-md-12 text-right">
                    <form class="woocommerce-ordering" method="get">
                        <select name="orderby" class="orderby" aria-label="Shop order">
                                                    <option value="menu_order" selected="selected">Default sorting</option>
                                                    <option value="popularity">Sort by popularity</option>
                                                    <option value="rating">Sort by average rating</option>
                                                    <option value="date">Sort by latest</option>
                                                    <option value="price">Sort by price: low to high</option>
                                                    <option value="price-desc">Sort by price: high to low</option>
                                            </select>
                        <input type="hidden" name="paged" value="1">
                                    </form>
                </div>
            </div>
        </div>


  <!-- filtered products -->
<div class="row row-sm  clearfix mt-2 cb-p2"> 
 

                @if( isset($products))
                    @foreach ($products  as $product) 
               
            <?php   
                    if ($product->photos=="") {
                      $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                    }else{
                         $image_url = $product->photos;
                    }  
                    $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 
            ?>
                 
         <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 "> 
                <div class="card card-sm card-product-grid">
                <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="img-wrap">
                    <img src="{{ $image_url }}"> 
                </a>
                <figcaption class="info-wrap">
                    <a  href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="title">{{$product->pr_name}}</a>
                    <div class="price mt-1">
                    @if($product->unit_price == $product->actual_price)
                        <p class="card-text">₹ {{$product->actual_price }}</p>
                      @else     
                        <p class="card-text">₹ {{$product->actual_price }} <span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}</span></p>
                      @endif
                  </div>  
 

                </figcaption>
       

        <div class='text-center'>

            @if(  $product->stock_inhand == 0  )
                <button type="button"   class="btn btn-outline-primary btn-sm" disabled>Out of stock</button>
             
            @else  
            <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" 
                data-prid="{{ $product->id }}" data-subid="{{  $product->id }}" 
                class="btn btn-primary btn-sm btn-add-item">Add to Cart
            </button>
            @endif 
        </div>
             </div>                            
    </div>

    @endforeach

    <div class="col-md-12"> 
    {{      $products->appends(request()->input())->links()  }}
    </div> 
    @endif 
        
    </div> 
 <!-- filtered products --> 
</div>
</div> 
</div> 
</div> <!-- container -->
</div> <!-- spacer -->


   <!-- Sidebar Overlay -->
<div class="sidenav" id="mySidenav" style="">
    <div class="row ">
        <div class="col-12 "> 
            <div class="card-title">
            <h5 class="pl-3 mt-3 title-merchant">All Merchants</h5>
            <a href="javascript:void(0)" class="closebtn showMerchantListX">×</a>
        </div>
         
    </div> 
    </div>
    <hr>
          <ul class="list-unstyled components links"> 
            
            @foreach ($businesses as $business)
                <li class="cat-item">
                    <a href="{{ URL::to('/business') }}/{{ strtolower(  $business->merchant_code ) }}"><small>
                    {{$business->name}}
            </small></a>  
                </li>
            @endforeach
             
          </ul>
         <hr>
</div>
     <!--  -->


@endsection