@extends('layouts.store_front_02')
@section('content')
 <?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;

 
?>

<div class='breadcrumb-box'>  
    <div class='container'>  
        <div class="row mt-2"> 
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/booking')}}" style="color:green;">
                                   <b>SHOPPING</b>
                                </a>
                            </li> 
                            <li class="breadcrumb-item active text-uppercase" aria-current="page">
                                 
                                   <b>{{$bizcategory}}</b> 
                            </li>
                        </ol>
              </nav>
          </div>
          <div class="col-md-6 col-sm-12">

          </div>
      </div>
  </div>
  <div class=" col-md-3 d-none d-sm-block">   
</div>
</div>
</div>
 </div>

 
<div class="container">  
<div class='row'> 
  
 

@if( isset($businesses))

       @foreach($businesses as $business)
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
         <figure class="card-product-grid card-sm">
          <a href="{{URL::to('/')}}/business/btm-{{$business->id}}"  class="img-wrap"> 
           <img src="{{$cdn_url}}{{$business->banner}}" class="img-fluid"> 
          </a>
          <div class="text-wrap p-3">
            <a href="{{URL::to('/')}}/business/btm-{{$business->id}}" class="title"><strong>{{$business->name }}</strong>
            	@if( $business->is_verified == "yes")
            		<i class='fa fa-check-circle blue'></i>
            	@endif
            </a> 
          </div>
       </figure>
    </div>
    @endforeach

{{ $businesses->links() }}

 
    @endif 
         
</div> 
</div>


@endsection

@section('script')
<style type="text/css"> 
  
</style>  


<script type="text/javascript"> 

</script>
@endsection