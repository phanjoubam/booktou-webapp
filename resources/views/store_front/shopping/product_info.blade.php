@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>



<div class='breadcrumb-box'>  
    <div class='container'>  
        <div class="row mt-2"> 
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/booking')}}" style="color:green;">
                                   <b>SHOPPING</b>
                                </a>
                            </li> 
                            <li class="breadcrumb-item text-uppercase" aria-current="page">
                                 
                                   <b>{{ strtoupper($product->category) }}</b> 
                            </li>

                        <li class="breadcrumb-item active text-uppercase" aria-current="page">
                          {{ $product->pr_name }}
                        </li>

                        </ol>
              </nav>
          </div>
          <div class="col-md-6 col-sm-12">

          </div>
      </div>
  </div>
  <div class=" col-md-3 d-none d-sm-block">   
</div>
</div>
</div>
 </div>


<div class="container">  
 

    <div class='product-detail'> 
        <div class='row product-block'>
            <div class='col-xs-12 col-sm-5 col-md-5'>
            <div class="row pdp-image-gallery-block ">
                <div id="gallery_pdp" class="col-md-2 grid_mobile">
                     
                        @foreach($photos as $items)         
                        <a class="zoomanchor zoomanchor-product-grid  " href="#" data-image="{{ $items->image_url }}" data-zoom-image="{{ $items->image_url }}" >
                        <img id="" class="shadow" src="{{ $items->image_url }}" />
                        </a>
                         @endforeach
                     
            </div>



            <!-- Up and down button for vertical carousel -->
            <a href="#" id="ui-carousel-next" style="display: inline;"></a>
            <a href="#" id="ui-carousel-prev" style="display: inline;"></a>
            <!-- arrow key carousel  d-none d-lg-block-->

            <div class="col-md-8 gallery-viewer mt-2"> 
                @php $i=0 @endphp
                @foreach($photos  as $items)
                @if($i=1)         
                <img class="img-fluid" id="zoom_10" src="{{$items->image_url}}" data-zoom-image="{{$items->image_url}}" />
                @break
                @endif
                @endforeach  
              <p class="hint-pdp-img">Roll over image to zoom in</p>
            </div>
         

            <div id="enlarge_gallery_pdp">
                <div class="enl_butt">
                    <a class="enl_but enl_fclose" title="Close"></a>
                    <a class="enl_but enl_fright" title="Next"></a>
                    <a class="enl_but enl_fleft" title="Prev"></a>
                </div>
                <div class="mega_enl"></div>
            </div>

            </div>

              
 </div> 
  
<div class="col-md-7"> 
<div class="single_product_desc clearfix">
    <h1 class="name">{{ $product->pr_name }}</h1> 
    <span>{{ $business->name }}</span> 
     <p class="product-desc">{{ $product->description }}</p>

    @if($product->allow_enquiry == "yes")


     <div class="price-container info-container m-t-20">
        <div class="row"> 
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                

                 <a role="button" target='_blank' data-phone="8787306375" 
                 href="https://web.whatsapp.com/send?text={{ Request::url() }}&phone=+918787306375"   class="btn btn-rounded btn-info btn-pill  btn-sm " data-action="share/whatsapp/share" >Enquire Now</a> 
 
                 <button type="button" data-action="wish" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}"  class="btn btn-rounded btn-warning btn-pill btn-sm btn-wish-item   "> Add to Wishlist </button> 

            </div>
 
        </div>
    </div>

 

    @else 


     <div class="price-container info-container m-t-20">
        <div class="row"> 
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                <div class="price-box">


                    <p class="price">
                        @if($product->actual_price > $product->unit_price)
                        <del><span class="amount"><span class="">₹</span>{{ $product->unit_price }}</span></del> 
                        @else 
                        <span class="amount"><span class="">₹</span>{{ $product->actual_price }}</span> 
                        @endif
                    </p> 
                </div>
            </div>

            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                <div class="favorite-button"> </div>
            </div> 
        </div>
    </div>
 
    <div class="quantity-container info-container">
        <div class="row"> 

            @if($business->is_block == "no" &&  $business->is_open == "open"  ) 
                @if(  $product->stock_inhand == 0  )
                <div class='col-md-3 mt-2'>
                    <button type="button"   class="btn btn-rounded btn-outline-primary " disabled>Out of stock</button>
                </div> 
                @else

                    <div class='col-md-3 mt-2'>

                         <div class="input-group">
  <input type="number" class="form-control qty" aria-label="Quantity"  placeholder="Quantity" step="1" min="1" max="" id='tbqty' value="1" title="Qty" >
  <button class="btn btn-outline-secondary btnplusminus" type="button" data-step='-1'>-</button>
  <button class="btn btn-outline-secondary btnplusminus" type="button" data-step='+1'>+</button>
</div>

 

                    </div>
                    <div class='col-md-4 mt-2'>  
                             <button type="button" 
                                data-action="add"
                                data-qty="1" 
                                data-bin="{{  $product->bin }}"
                                data-prid="{{  $product->id }}" 
                                data-subid="{{  $product->id }}" 
                                class="btn btn-rounded btn-primary btn-add-item">Add to Cart
                            </button> 
                    </div>
                 
                @endif
            @else 
            <button type="button" data-action="wish" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}"  class="btn btn-rounded btn-warning btn-sm btn-wish-item   "> Add to Wishlist </button>  
            @endif 
        </div>   
    </div>
@endif

</div>

</div>
</div> <!-- row -->
</div>

 
<section>
 
<header class="section-heading heading-line">
        <h7 class="title-section text-uppercase">Similar products</h7>
</header>

 
<div class='filters-container'> 
  <!-- filtered products -->
<div class="row  "> 
 

@if( isset($similarProducts))
@foreach ($similarProducts  as $product) 
               
<?php  
            if ($product->photos=="") {
                      $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
            }else{
                 $image_url = $product->photos;
            }  
            $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

             
?>     
         <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 col-xl-3"> 
                <div class="card card-sm card-product-grid">
                <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="img-wrap">
                    <img src="{{ $image_url }}"> 
                </a>
                <figcaption class="info-wrap">
                    <a  href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="title">{{$product->pr_name}}</a>
                    <div class="price mt-1">
                    @if($product->unit_price == $product->actual_price)
                        <p class="card-text">₹ {{$product->actual_price }}</p>
                      @else     
                        <p class="card-text">₹ {{$product->actual_price }} <span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}</span></p>
                      @endif
                  </div>  
 

                </figcaption>
       

        <div class='text-center'>
            @if($product->allow_enquiry == "yes")
                <a role="button" target='_blank' data-phone="8787306375" 
                 href="https://web.whatsapp.com/send?text={{ Request::url() }}&phone=+918787306375"   class="btn btn-rounded btn-info btn-pill  btn-sm " data-action="share/whatsapp/share" >Enquire Now</a> 
 
                 <button type="button" data-action="wish" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}"  class="btn btn-rounded btn-warning btn-pill btn-sm btn-wish-item   "> Add to Wishlist </button> 

                @else
                        @if(  $product->stock_inhand == 0  )
                            <button type="button"   class="btn btn-rounded btn-outline-primary btn-sm" disabled>Out of stock</button>
                         
                        @else  
                        <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}" 
                            class="btn btn-rounded btn-primary btn-sm btn-add-item">Add to Cart</button>
                        @endif 
                @endif
        </div>
             </div>                            
    </div>

    @endforeach

    <div class="col-md-12  mb-2"> 
    {{$similarProducts->appends(request()->input())->links()}}
    </div> 
    @endif 
        
    </div> 
 <!-- filtered products --> 
</div>

 
</section> 
<!-- similar products section ends here -->
 
</div> <!-- container -->
 

@endsection 

@section('script')
<script> 
$(document).on("click", ".btnplusminus", function()
{
    var step = $(this).attr("data-step");  
    var curval = $("#tbqty").val();
    
    if(curval == "")
    {
        $("#tbqty").val( 1 );
    }
    else if( parseInt(curval) == 0)
    {
        if(parseInt(step) > 0)
        {
            $("#tbqty").val( parseInt(curval) + parseInt(step)  );
        }
    }
    else  if( parseInt(curval) > 0)
    {
        $("#tbqty").val( parseInt(curval) + parseInt(step)  );
    }
})

</script>

@endsection 




  