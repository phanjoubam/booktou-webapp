@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>
<div class='outer-top-tm'> 
    <div class="container">  
    <div class='row'> 
        <div class='col-sm-3 col-xs-12   sidebar'>
 
 
 

<div class="sidebar-widget   widget_price_filter">
    <h3 class="section-title">Filter based on price</h3> 
    <div class="addthis_inline_share_toolbox"></div>

    <ul>
    <?php 

        if($max_amount < 500)
        {
            ?>
            <li><a href="" >Under 500 ₹</a></li>
            <?php
        }
        else
        {
            ?>
                <li><a href="" >Under 500 ₹</a></li>
            <?php

            $price_band = 500;
            $starting_price = 500;
            $current_price = $max_amount; 

            while( $current_price > 0 )
            {
                ?>
                <li><a href="" >{{ $starting_price }} - {{  $starting_price + $price_band }} ₹</a></li>
                <?php
                
                $current_price  -=  $price_band ;
                $starting_price += $price_band; 

            }
        }
        
    ?>
</ul>
 
</div>
 

    </div>
 
<div class='col-sm-9 col-xs-12 col-md-9 col-mlg-9  '>
    
 
<div class="row row-sm  clearfix mt-2 cb-p2 sort-list" id="loading"> 
 
  

</div> 
 <!-- filtered products --> 
</div>
</div> 
</div> 
</div> <!-- container -->
</div> <!-- spacer -->

 

@endsection 

@section('script')
 
 
@endsection 
