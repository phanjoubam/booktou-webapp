@extends('layouts.store_front_02')
@section('content')
<?php 
$cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public

    $total_cost = $delivery_cost = 0.0;
    $normal_delivery = 80; 
    $food_delivery = 50; 
    $grocery_delivery = 100.00;
    $cake_delivery =  100.00; 
    $delivery_charge = 0.00; 
    $taxes = 2;

    foreach($configs as $config)
    { 
        switch( $config->config_key )
        {
            case "delivery_charge": 
            $normal_delivery =  $config->config_value  ;
            break;
            case "food_delivery_charge":
            $food_delivery =  $config->config_value  ;
            break;
            case "grocery_delivery_charge":
            $grocery_delivery =  $config->config_value  ;
            break;
            case "cake_delivery_charge":
            $cake_delivery =  $config->config_value  ;
            break;
        }
    } 

    ?>

    <div class='outer-top-tm'> 
        <div class="container">  
            <div class='row'> 
  
            <div class="col-sm-12 col-xs-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1  mb-2">
                <form action="{{ action('StoreFront\StoreFrontController@placeOrder')  }}" method="post">
                    {{ csrf_field() }} 

                    <input  type='hidden' name='addressSelected' id='addressSelected' /> 

                    <div class="card   mb-4">
                        <div class="card-body">

                            @if(strcasecmp($business->category,"RESTAURANT")  == 0)
                            @php 
                            $delivery_charge = $food_delivery;
                            @endphp
                            @elseif( strcasecmp($business->category,"CAKES & BAKERY") == 0) 
                            @php 
                            $delivery_charge = $cake_delivery;
                            @endphp
                            @elseif( strcasecmp($business->category,"VARIETY STORE") == 0)
                            @php 
                            $delivery_charge = $grocery_delivery;
                            @endphp
                            @elseif(strcasecmp ($business->category,"GROCERY") == 0)
                            @php 
                            $delivery_charge = $grocery_delivery;
                            @endphp
                            @else 
                            @php 
                            $delivery_charge = $normal_delivery;
                            @endphp
                            @endif
                            <input type="hidden" name="bin"  value="{{$business->id}}">

                            <div class="cart-page-heading">
                                <p class="h3 text-center">Order will be place to {{ $business->name }}</p>
                                <hr/> 
                            </div>
 
                                @php 
                                $total_cost = $total = $total_packaging_cost = $total_to_pay =  0;
                                $memid = 0;
                                $tax = $gst= $cgst = $sgst = 0;
                                $gst_pc = 0;
                                @endphp 

                                <table class="table table-colored">
                <tr>
                    <th width='90px'></th>
                    <th style='width:250px; text-align:left' >Item</th>
                    <th style='width: 250px'>Quantity</th>
                    <th style='width: 250px'>Unit Price</th>
                    <th style='width: 250px'>Packaging</th>
                    <th class='text-end'>Sub-Total</th>
                </tr>  
                @foreach($cart as $cartitem)
                 
                <?php
                if($cartitem->photos=="") {
                  $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
              }else{
               $image_url = $cartitem->photos;
           }
           ?>
           <tr>
            <td width='90px'><img class='rounded ' width='40px' src="{{$image_url}}/" 
                alt='{{ $cartitem->pr_code }}'/>
            </td>
            <td style='width:250px; text-align:left' >
                <strong>{{ $cartitem->pr_name }}<strong>
                </td>
                <td style='width: 250px'>{{ $cartitem->pqty }}</td>
                <td style='width: 250px'>{{ $cartitem->actual_price }}</td>
                <td style='width: 250px'>{{ $cartitem->packaging }}</td>

         <td class='text-end'>{{ ( $cartitem->actual_price *  $cartitem->pqty ) + 
            ($cartitem->packaging * $cartitem->pqty)  }}</td>
        </tr>  
            @php 
            $total_cost += ( $cartitem->actual_price *  $cartitem->pqty )  ;
            $total_packaging_cost  +=  ($cartitem->packaging * $cartitem->pqty) ; 
            $memid  =  $cartitem->member_id;  
            
            $gst += ( ( $cartitem->actual_price + $cartitem->packaging)  * $cartitem->pqty *  $cartitem->gst_pc )/100 ;
            $gst_pc = $cartitem->gst_pc;
            $total_to_pay = $total_cost + $total_packaging_cost + $delivery_charge + $gst;
            @endphp
                        
        @endforeach  
        <tr>
            <td colspan='4' class='text-end'><strong>Item Total Cost:</strong></td>    
            <td colspan='2' class='text-end'>₹ {{ number_format($total_cost,2,".","") }}</td>
        </tr>
        <tr>
            <td colspan='4' class='text-end'><strong>Packaging Cost:</strong></td>    
            <td colspan='2' class='text-end'>₹ {{ number_format($total_packaging_cost,2,".","") }}</td>
        </tr>  

        <tr>
            <td colspan='4' class='text-end'><strong>Delivery Charge:</strong></td>    
            <td colspan='2' class='text-end'>₹ {{ number_format($delivery_charge,2,".","") }}</td>
        </tr>
        <tr>
            <td colspan='4' class='text-end'><strong>GST ( {{ number_format($gst_pc,2,".","") }}% ):</strong></td>                   
            <td colspan='2' class='text-end'>₹ {{ number_format($gst,2,".","") }}</td>
        </tr> 
        <tr>
            <td colspan='4' class='text-end'><strong>Total Order Cost:</strong></td>  
            <td colspan='2' class='text-end'>₹ {{ number_format( ($total_cost + $total_packaging_cost + $delivery_charge + $gst) ,2,".","") }}</td>
        </tr>

        <tr>
            <td colspan='6' class='text-center'>
<strong>Choose delivery address</strong>   

        </td>
    </tr>
    <tr>
            <td colspan='6'  >


              @php 
              $i = 0;  
              @endphp

              @if(isset($member)) 
              <div  role="tab" id="heading{{ $i }}">
                <input class="form-check-input cb_select_address" checked type="radio" value="0" name='cbAddress' id='addressSelected{{ $i }}' data-toggle="collapse" href="#collapse{{ $i }}" aria-expanded="false" aria-controls="collapse{{ $i }}">
                <label class="form-check-label" for="addressSelected{{ $i }}">{{ $member->locality }}</label>  
            </div>
            <div id="collapse{{ $i }}" class="collapse" role="tabpanel" aria-labelledby="heading{{ $i }}" data-parent="#accordion">
                <p>{{ $member->landmark }}</p>
            </div> 
            @php 
            $i++;  
            @endphp 
            @endif 



            @foreach($addresses as $address)
            @php 
            $parts = explode(",", $address->address ); 
            @endphp 

            @if(count($parts) >  0)

            <div role="tab" id="headingOne" data-toggle="collapse" href="#collapse{{ $i }}"  aria-expanded="false" aria-controls="collapse{{ $i }}" > 
                <input class="form-check-input cb_select_address" type="radio"  name='cbAddress' value="{{ $address->id }}"  id="addressSelected{{ $i }}">
                <label class="form-check-label" for="addressSelected{{ $i }}">{{ $parts[0] }}</label> 
            </div>

            <div id="collapse{{ $i }}" class="collapse" role="tabpanel" aria-labelledby="heading{{ $i }}" data-parent="#accordion">
                <div class="card-body">
                    <p>{{ $address->landmark }}</p>
                </div>
            </div>


            @else  
            <div  role="tab" id="heading{{ $i }}" data-toggle="collapse" href="#collapseOne"  aria-expanded="false" aria-controls="collapse{{ $i }}" > 
                <input class="form-check-input cb_select_address"  type="radio" name='cbAddress' value="{{ $address->id }}"   id="addressSelected{{ $i }}">
                <label class="form-check-label" for="addressSelected{{ $i }}">{{ $address->address }}</label> 
            </div>

            <div id="collapse{{ $i }}" class="collapse" role="tabpanel" aria-labelledby="heading{{ $i }}" data-parent="#accordion">
                <div class="card-body">
                    <p>{{ $address->landmark }}</p>
                </div>
            </div> 



            @endif 

            @php
            $i++;  
            @endphp 

            @endforeach
        </div>
    </div>
    <hr/>
    <button type='button' name='btnorder'  class="btn btn-primary btn-rounded btnShowWidget">Add New Address</button>
</td>

        </tr>




        <tr>
            <td colspan='6' class='text-center'>
<strong>Choose your payment mode to place this order</strong>   

        </td>
        </tr>
  
   
    </tr>
</table>

<div class="row row-cols-1 row-cols-md-2 g-4">
  <div class="col">

    <div class="card">
  <div class="card-body">
    Pay online to get your order confirmed from the merchant instantly. 
    It is also the safest and the most secure way of doing transaction. This is the most preferred payment mode for most buyers.
    
  </div>
   <div class="card-footer  ">


    <div class="d-flex mb-3">
          <div class="p-2 flex-fill"> 
            <strong>Your order amount is ₹ <label id="subamount">{{ number_format( $total_to_pay ,2 , "." ,"" ) }}</label></strong>
            <br>
            <i class="fa fa-shield sgreen"></i> 100% safe and secure payment.

        </div>
        <div class="p-2 flex-fill-right text-end">
          <button type='submit' name='btnorder' value='online' id="rzp-button1" class="btn btn-rounded btn-success">Place Order</button>

        </div> 
    </div>


    

  </div>

</div>

  </div> 

   <div class="col">

    <div class="card">
  <div class="card-body">
    We  support cash on delivery (COD). However, COD order may take some time to get merchant confirmation. We recommend to place order online for your and merchant's safety.
    
  </div>
   <div class="card-footer ">

     <div class="d-flex mb-3">
          <div class="p-2 flex-fill"> 
            <strong>Your order amount is ₹ <label id="subamount">{{ number_format( $total_to_pay ,2 , "." ,"" ) }}</label></strong>
            <br>
            Pay cash at the time of delivery.
        </div>
        <div class="p-2 flex-fill-right text-end">
          <button type='submit' name='btnorder' value="cash"  class="btn btn-rounded btn-primary">Place Order</button>

        </div> 
    </div>


    
  </div>

</div>

  </div> 


</div>
 
                            </div>
                        </div>
                    </form>  
                </div> 
            </div>
        </div>
    </div>
</div>


<form action="{{ action('StoreFront\StoreFrontController@addDeliveryAddress')  }}" method="post">
    {{ csrf_field() }}
    <div class="modal" id='wgt01' tabindex="-1">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">New Shipping &amp; Billing Address</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">

        <div class="row">
            <div class="col-12 mb-3">
                <label for="street_address">Address <span>*</span></label>
                <input type="text" class="form-control mb-3" name="address" value=""> 
            </div>

            <div class="col-12 mb-3">
                <label for="street_address">Landmark <span>*</span></label> 
                <input type="text" class="form-control" name="landmark" value="">
            </div>
            <div class="col-12 mb-3">
                <label for="city">Town/City <span>*</span></label>
                <input type="text" class="form-control" name="city" value="">
            </div>

            <div class="col-12 mb-3">
                <label for="state">State <span>*</span></label>
                <input type="text" class="form-control" name="state" value="">
            </div>

            <div class="col-12 mb-3">
                <label for="postcode">Pin <span>*</span></label>
                <input type="text" class="form-control" name="pin" value="">
            </div> 

            <div class="col-12">

            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type='submit' name='btnadd' value='addaddress'  class="btn btn-primary ">Add Address</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> 
    </div>
</div>
</div>
</div>
</form>




@endsection 

@section('script')

<script>


    $(document).on("change", ".cb_select_address", function()
    {
        $('#addressSelected').val($(this).val());        
    });




    $(document).on("click", ".btnShowWidget", function()
    {
        $("#wgt01").modal('show');
    })

</script>

@endsection 