 <div class="card"> 
                         <div class="card-body">
                        <div class="cart-page-heading mb-30">
                            <h1>Customer Login</h1>
                            <hr/>
                        </div>

                        <form action="{{ action('Auth\LoginController@customerLogin') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                
                                <div class="col-12 mb-3">
                                    <label for="phone">Phone Number or Email: <span>*</span></label>
                                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email'>
                                </div>
                               
                               
                                <div class="col-12 mb-4">
                                    <label for="email_address">Password: <span>*</span></label>
                                    <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password'>
                                </div>
                                <div class="col-12 mb-4">
                                
                                <button type="submit" value='login' name='login' class="btn btn-primary">Login</button>
                        
                                </div>

                            </div>
                        </form>
</div>
</div>
                