@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?> 

<section> 

    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{URL::to('public/assets/image/slider_sns.jpg')}}" class="d-block w-100">
            </div> 
        </div>

        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <form action="{{action('Appointment\AppointmentControllerWeb@searchAppointmentServices')}}" method="get"> 
        <div class="search-box-outer"> 
            <div class="search-box-inner"> 
                <div class="search-inpage"> 
                    <div class="row"> 
                        <div class="col-md-12">
                            <h2>Search beauty or makeup services</h2>
                            <hr/>
                        </div>
                        <div class="col-md-12">

                            <div class="input-group">
                                <input type="text" name="keyword"  class='form-control' placeholder="Hair dying, bridal makeup">

                                <select class="form-control search-slt" id="city" name="city" placeholder="Imphal"> 
                                    @foreach($city as $citylist)
                                    <option value="{{$citylist->cityList}}">{{$citylist->cityList}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" value='search' class='btn inpgsearch'>
                                    <i class='fa fa-search'></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
    </form>

    
</section>

 

<div class="clearfix"></div> 

<section class="mt-2 bgcolor p-3">
    <h4 class="text-center text-uppercase">Popular Services</h4>
    <div class="container">
        <div class="row carousel2 mt-5">
             @foreach($product as $items) 
            <div class="col-md-1 col-6 col-sm-6 col-xs-6 mb-2">
                <a href="{{URL::to('/appointment/appointment/view-service')}}/{{$items->id}}"> 
                    <div class="card">  
                     @if($items->photos=="")
                       <?php 
                        $image_url = 'https://cdn.booktou.in/assets/image/no-image.jpg';
                       ?>  
                     @else
                       <?php 
                       $image_url = $items->photos;
                       ?> 
                     @endif
                     <img src="{{$image_url}}" class="img-fluid  p-2">
                      <h7 class="card-title text-uppercase text-center"><small> {{$items->srv_name}}</small></h7>
                    </div> 
                </a>
            </div> 
            @endforeach

        </div>
    </div>
</section>

<section class="mt-2 bgcolor  ">
<h4 class="text-center">TOP RATED PARTNERS</h4>
    <div class="container mt-4">
       <div class="container mt-40 d-none d-sm-block"> 

            <div class="row mt-30">
                 @foreach($business as $items) 

                  @if($items->banner=="") 
                    <?php 
                        $image_url = "https://cdn.booktou.in/assets/image/no-image.jpg"; 
                    ?>
                    @else
                        <?php 
                        $image_url = $cdn_url.$items->banner; 
                        ?>
                    @endif
                  
                <div class="col-md-3 col-sm-6 mb-4">
                    <div class="partner-box">
                        <img src="{{$image_url}}" class="img-fluid">
                        <div class="box-content">
                            <h3 class="title">{{$items->name}}</h3>
                            <span class="post">{{$items->tag_line}}</span>
                            <ul class="social">
                                <li>
                                    <a href="{{URL::to('/appointment/business/btm-')}}{{$items->id}}"><i class="fa fa-home"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> 
                 @endforeach
            </div>
        </div>

        <div class="container mt-40 d-block d-md-none"> 

            <div class="row mt-30">
                 @foreach($business as $items) 

                  @if($items->banner=="")
                    <!-- <img src="https://cdn.booktou.in/assets/image/no-image.jpg" style="height: 250px;border-radius: 7px;"> -->
                    <?php 
                        $image_url = "https://cdn.booktou.in/assets/image/no-image.jpg"; 
                    ?>
                    @else
                        <?php 
                        $image_url = $cdn_url.$items->banner; 
                        ?>
                    @endif
                  
                <div class="col-6 col-sm-6 mb-1">
                    <a href="{{URL::to('/appointment/business/btm-')}}{{$items->id}}">
                    <div class="partner-box">
                        <img src="{{$image_url}}" class="img-fluid">
                        <p class="post-title  my-1" style="color:#000;">{{$items->name}}</p>
                    </div>
                    </a>
                </div> 
                 @endforeach
            </div>
        </div>
    </div>
</section> 

@endsection 

@section('script')

<script type="text/javascript">
     
     if (screen.width<500) { 
                  $('.carousel2').slick({
                  rows: 1,
                  infinite: false,
                  slidesToShow: 1,
                  slidesToScroll: 1
                });

        }else{ 
                  $('.carousel2').slick({
                  rows: 1,
                  infinite: false,
                  slidesToShow: 4,
                  slidesToScroll: 3
                  }); 
        }

</script>
@endsection 
