@extends('layouts.booking_theme_01')
@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<style>

.box
{
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.19);
    border-radius:7px;
    height:250px;
background-color: transparent;
   /* width:195px;*/
}

.det {
    height: 250px;
    padding-top: 5px;
    padding-left:20px;
    }

.det .detImg
{
    /*height: 130px;   */
    position: relative;
   /* top:13px; */

    border-radius:10px ;
}
.det .detImg img
{


    width: 95%;
    height: 95%;
    border-radius:10px ;

}


.bnow
{
    text-align: center;
    color: #fff;
   background-color: #2e9c11;
   border-color: #2e9c11;
 margin-right:8px;
}

.bnow:hover {
  border: 1px solid orange;
  color: #fff;
  background-color:#2e9c11;
  background-color: #2e9c11;
}
.bnow:focus {
  color: #fff;
  border: 1px solid orange;
  background-color: #2e9c11;
  border-color: #2e9c11;

}
.btn-sml {

    font-size:13px;
    border-radius: 8px;
}
.text-color
{
    color: black;
    text-decoration: none;
}
.text-color:hover
{
    color: red;
    text-decoration: none;
}


.bnow
{
 text-align: center;

}
.tname
{
    font-size:13px;
}

</style>

<div class='breadcrumb-box'>  
    <div class='container'>  
        <div class="row mt-2"> 
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/service/booking')}}" style="color:green;">
                                   <b> Booking</b>
                                </a>
                            </li>
                            @if(isset($categoryname))
                            <li class="breadcrumb-item active text-uppercase" aria-current="page">
                                <a class="text-weight-bold" href="{{URL::to('/booking/service-details')}}/{{$categoryname}}" style="color:green;">
                                   <b> {{$categoryname}} </b>

                                 </a>
                            </li>
                            @endif
                            <li class="breadcrumb-item text-uppercase">

                                   {{$business->name}}

                            </li> 
                        </ol>
              </nav>
          </div>
          <div class="col-md-6 col-sm-12">

          </div>
      </div>
  </div>
  <div class=" col-md-3 d-none d-sm-block">   
</div>
</div>
</div>
 </div>
 
 <div class='container'>  
     <div class="row"> 
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >

            <div class="sidebar-widget widget_product_categories  d-lg-block d-xl-block" >
               <h5 class="section-title font-weight-bold">{{$business->name}}</h5>
               <div class="fanbox" >
                <div class="fanbox-header d-none d-lg-block d-xl-block">

                    <?php

                    if($business->banner==""){

                        $image_url =   $cdn_url."/assets/image/no-image.jpg";

                    } else {

                        $image_url =  $cdn_url. $business->banner;

                    }

                    ?>  
                    <img src="{{URL::to($image_url)}}" width="100%"> 
                </div>
                <div class="fanbox-body">
                    <p><span class="widget-title mb-30"></span><br>

                     {{$business->locality}}<br>
                     {{$business->landmark}} {{$business->city}},{{$business->state}}
                 </p>
             </div>
         </div>
     </div>

     <div class='card mb-3 sidebar-widget widget_product_categories'>
        <div class="card-body">
            <h5 class="font-weight-bold"> Categories</h5><hr>
            <div class="">
                @foreach($categories as $category) 

                <div class="media"> 
                  <div class="media-body">
                    <a class="text-dark" href="{{URL::to('/booking/btm-')}}{{$business->id}}?category={{ $category->service_category }}">
                        {{ ucwords(  $category->service_category ) }}
                    </a>
                </div>
            </div>
 
            @endforeach
        </div>
    </div>
</div>

</div>
 

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >  
        <div class="row">

            @foreach($services as $item) 

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"> 
                <div class="card mb-3  rca-2">  
                    <div class='card-body'>
                        <div class="row align-items-start"> 
                            <div class="col-md-12"> 
                                <p><span class='h5'>{{$item->srv_name}}</span></p>
                                <hr/>
                            </div> 
                            


                            <div class="col-md-5 float-sm-start float-md-end float-lg-end">
                            @if( $item->photos != "") 
                                @php
                                    $imageurl = $item->photos;
                                @endphp
                                <img src="{{ $imageurl }}" class="img-fluid rounded">

                            @else
                                
                                @if( count($service_profiles) > 0)
                                    @php 
                                        $active_slider ="active";
                                    @endphp
                                    <div id="service_slider{{$item->id}}" class="carousel slide" data-bs-ride="true" >
                                      <div class="carousel-inner">
                                        @foreach($service_profiles as $service_profile)
                                            @if(  $service_profile->service_product_id  == $item->id  )
                                                <div class="carousel-item {{ $active_slider }}">
                                                  <img src="{{ $service_profile->photos }}" class="d-block w-100" alt="...">
                                                </div>
                                                @php 
                                                    $active_slider ="";
                                                @endphp
                                            @endif 
                                        @endforeach  
                                    </div>
                                  
                                    <button class="carousel-control-prev" type="button" data-bs-target="#service_slider{{$item->id}}" data-bs-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Previous</span>
                                        </button>
                                    <button class="carousel-control-next" type="button" data-bs-target="#service_slider{{$item->id}}" data-bs-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Next</span>
                                    </button>
                                    </div> 
                                @endif 
                            @endif
                </div>

                <div class="col-md-7 float-sm-end float-md-start float-lg-start">
                            <div class='box-content-sm'> 
                                <p>
                                @php 
                                    $len = strlen ( $item->srv_details ); 
                                    if($len > 250) 
                                    {
                                         
                                        echo substr($item->srv_details, 0, 250 ) . ' [+]';
                                    }
                                    else
                                    {
                                          echo $item->srv_details;
                                    } 
                                @endphp  
                             </p>
                         </div> 
                        </div>

            </div>
             <div class="row align-items-start"> 
                <div class="col-md-8"> 
                        <div class="price mt-1 text-right">
                            <span class="h3">Starts from <span class='orange'>{{$item->actual_price}}₹</span></span>
                        </div>
                </div>
                <div class="col-md-4 text-right"> 
                    <a  href="{{URL::to('booking/slot-selector')}}/{{ $item->bin }}/{{$item->id}}" class="btn btn-primary btn-rounded">
                    Book Now</a>
                </div>
            </div>


 
                    <div class="text-center">
                        
                    </div>
                </div></div>
            </div>


            @endforeach
        </div> 
 <!-- filtered products -->

</div>
<!--  -->
</div>
</div> 
@endsection
