
<div class="row ">
@if(count($result)>0)
<div class="col-md-10 offset-md-1">
@foreach($result as $items)
<div class="card mt-2">
<div class="card-body">
	<div class="card-title">
		<h5>{{$items->heading}}</h5>
	</div> 
	<div class="row">
<div class="col-md-2">
	@if($items->photos=="")
	<img src="{{URL::to('public/asset/theme01/images/no-image.jpg')}}" class="img-fluid">
	@else
	<img src="{{$items->photos}}" class="img-fluid">
	@endif
</div>

<div class="col-md-8">
<p> 
	{{$items->description}}
</p>

</div>
</div>

</div>
</div>
@endforeach

@else

<p class="alert alert-info text-center">No Service details to show</p>

@endif
</div>
</div>
 