@extends('layouts.store_front_02')
@section('content')

<style>
 
.rating-block{
    background-color:#FAFAFA;
    border:1px solid #EFEFEF;
    padding:15px 15px 20px 15px;
    border-radius:3px;
}
.review-block{
    background-color:#FAFAFA;
    border:1px solid #EFEFEF;
    padding:15px;
    border-radius:3px;
    margin-bottom:15px;
}
.review-block-name{
    font-size:12px;
    margin:10px 0;
}
.review-block-date{
    font-size:12px;
}
.review-block-rate{
    font-size:13px;
    margin-bottom:15px;
}
.review-block-title{
    font-size:15px;
    font-weight:700;
    margin-bottom:10px;
}
.review-block-description{
    font-size:13px;
}


  .box
{
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.19);
    border-radius:7px;
    height:250px;
background-color: transparent;
   /* width:195px;*/
}



.ban
{
  /*height: 400px;*/
 /*
   padding-left:10px;
   padding-right:10px;*/
   
}
.ban.detban
{
   height: 10px;   
    position: absolute;
   
    border-radius:10px ;
}
.ban .detban img
{
    width:100%;
    height:200px;
    /*margin-left:15px;*/
    border-radius:10px ;

}

.bnow
{
    text-align: center;
    color: #fff;
   background-color: #2e9c11;
   border-color: #2e9c11;

}

.bnow:hover {
  border: 1px solid orange;
  color: #fff;
  background-color:#2e9c11;
  background-color: #2e9c11;
}
.bnow:focus {
  color: #fff;
  border: 1px solid orange;
  background-color: #2e9c11;
  border-color: #2e9c11;
}
.btn-sml {
    
    font-size:13px;
    border-radius:15px;
}
.text-color
{
    color: black;
    text-decoration: none;
}
.text-color:hover
{
    color: red;
    text-decoration: none;
}


.bnow
{
 text-align: center;
      
}
.tname
{
    font-size:14px;
}  

#ads .card:hover {
            background: #fff;
           /* box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);*/
            border-radius: 4px;
           
            /*transition: all 0.3s ease;*/

        }


.filters-container {
  padding: 15px 20px;
    padding-bottom: 15px;
  background-color: #fff;
  padding-bottom: 0px;
  margin-bottom: 20px;
  font-size: 14px;
  border-radius: 4px 4px 0px 0px;
  box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
} 
.image-grid-cover {
width: 100%;
background-size: cover;
min-height: 100px;
position: relative;
margin-bottom: 30px;
text-shadow: rgba(0,0,0,.8) 0 1px 0;
border-radius: 4px;
}
.image-grid-clickbox {
position: absolute;
top: 0;
right: 0;
bottom: 0;
left: 0;
display: block;
width: 100%;
height:100%;
z-index: 20;
border-radius: 40px;

}



.image-grid-cover {
position: relative;
width: 100%;
max-width: 300px;
}



.image {
display: block;
width:100%;
border-radius: 40px;
height:100px;
}



.overlay {
position: absolute;
bottom:0;
/*top: 20px;
*/background-color: black;
background: rgb(0, 0, 0);
background: rgba(0, 0, 0, 0.5); /* Black see-through */
/*color: #f1f1f1; */
width: 100%;
transition: .5s ease;
opacity:0;
height: 100%;
color: white;
font-size: 20px;
padding:12px;
border-radius: 40px;
text-align: center;
z-index: 100;
}



.image-grid-cover:hover .overlay {
opacity: 1;
}
   
#moredetails
{
    font-size: 13px;
}



</style>
<div class="outer-top-tm" > 
    <div class="container " style="">  
     <div class="row mt-3"> 

        <div class="col-sm-12 col-md-3 sidebar mt-5">
             

        <div class='card mt-4'>
                <div class="card-body">

            <div class="py-2  border-bottom">

              <h5 class="font-weight-bold mt-2 Merhants"> Categories</h5>       
               <form >
                @foreach($category as $items)         
                <div > 
                    <a class="text-dark" href="{{URL::to('/booking/service-details?book[]=')}}{{$items->service_category}}">
                    <input type="checkbox" > <label class="text-color">{{$items->service_category}}</label></a>
                </div>
                @endforeach
               </form>
            </div>
        </div>
    </div>


 <div class='card mt-4'>
                <div class="card-body">
    <div class="py-2 border-bottom ">
     <h4 class="font-weight-bold mt-2">Price</h4>
       <form>
         
                          
        <?php
        $min_amount = $min_amount;

        if($max_amount < $min_amount)
        {
        ?>
        <a href="" >Under $min_amount ₹</a> 
        <?php
        }

        else

        {
        ?>

        <a class="text-color" href="" >
            <input type="checkbox">
       <label> Under ₹ {{$min_amount}} </label>
       </a>
       <br>     
            <?php
            $price_band = $min_amount;
            $starting_price = $min_amount;
            $current_price = $max_amount;
            while( $current_price > 0 )
            {
            ?>
              
        <a class="text-color" href="" >
        <input type="checkbox">
        <label  >₹ {{number_format($starting_price)}} - ₹ {{number_format($starting_price + $price_band) }}</label>
       </a>
       <br>
        <?php        
        $current_price -= $price_band ;
        $starting_price += $price_band;
        }
        }

        ?>
         
         
          </form>
          </div>
        </div>
        </div>
       </div>


 
 
 
    <div class="col-sm-12 col-xs-12 col-md-9   mt-5" >
            <div class="row filters-container  mt-4 cb-p2" style="height: 60px;">
                 
                <div class="col-md-8">
                    Merchant List

                </div>   
                <div class=" col-md-4">
                            <div class="  text-right" >
                                <form class="woocommerce-ordering" method="get" >
                                    <select name="orderby" class="orderby" aria-label="Shop order">
                                        <option value="menu_order" selected="selected" >Default sorting</option>
                                        <option value="popularity">Sort by popularity</option>
                                        <option value="rating">Sort by average rating</option>
                                        <option value="date">Sort by latest</option>
                                        <option value="price">Sort by price: low to high</option>
                                        <option value="price-desc">Sort by price: high to low</option>
                                    </select>
                                    <input type="hidden" name="paged" value="1">
                                </form>
                            </div>
                        </div>
                    </div>


<div class="row   mt-3 cb-p2 mt-4">
                @if(isset($merchants))
                @foreach($merchants as $item)
                 
                <div class="card mt-2">
                    <div class="card-body">
                        <div class="row">
                    
                        <div class="col-md-8">
                              
                        @if($item->banner=="")
                               <img src="{{URL::to('public/asset/theme01/images/no-image.jpg')}}" class="" width="" height="200" width="30%">
                                @else 
                                <img src="{{URL::to('public/')}}{{$item->banner}}" class=" " height="200" width="30%">
                        @endif
                         
                    </div>

                        <div class="col-md-4">
                           
                        <div class="card-title">
                            <h5>
                                 
                                 <img src="https://booktou.in/public/store/image/bluetick.png" width="20px">  {{$item->name}}
                                
                            </h5>
                            <p>
                                {{$item->locality}}<br>
                                {{$item->landmark}}<br>
                                {{$item->pin}}<br> 
                                {{$item->city}}<br>
                                {{$item->phone_pri}}
                            </p>
                    
                      @if($item->module==null)
                        <a href="" class="btn btn-primary btn-xs " disable>No service</a>
                        @else 
                        <a href="{{URL::to('booking/business-details/btm-')}}{{$item->id}}" class="btn btn-primary btn-xs">View all service</a>
                     @endif  
                      </div>
                         
                        </div> 

                    
                     
                        <!-- section for displaying items for particular business -->
                            <?php 
                            if (count($item->products)>0) {
                                            foreach ($item->products as $sitems) { 
                            ?>
                                   
                                <div class="col-6 col-sm-6 col-md-2 image-grid-item card m-1 ">

                                    <div class="image-grid-cover ">
                                    <a href="#" class="image-grid-clickbox mt-2">
                                    @if($sitems->photos=="")
                                    <img src="{{URL::to('public/asset/theme01/images/no-image.jpg')}}" class="image" height="100px" width="100px">
                                    @else
                                    <img src="{{$sitems->photos}}" class="image">
                                    @endif
                                    <div class="overlay">
                                    <p class="text-weight-bold" style="font-size:13px;">{{$sitems->srv_name}}</p>
                                    <button class="btn btn-info btn-sm btn-quickview"
                                    data-serviceProductId="{{$sitems->id}}"
                                    data-bin="{{$item->id}}"
                                    >more details</button> 
                                    </div>
                                    </a> 
                                    </div>
                                    <a class="btn btn-success btn-sm mb-2" href="{{URL::to('booking/slot-selector')}}/{{$sitems->id}}">Book</a>
                                </div> 

                              <?php      
                                    }
                                }

                            ?>
                        <!-- displaying ends here --> 
                        </div>
                    </div>
                </div> 

                @endforeach                
                @endif

            </div>
                 
    </div>
    </div> 
    </div>
</div>

    
<!-- modal section goes from here -->
<div class="modal fade" id="widgetquickview" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-fullscreen ">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><span class="badge badge-primary">More Service Details</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="servicedetails">
        
      </div>
       
    </div>
  </div>
</div> 

<!-- modal section ends here -->
                      


@endsection 

@section('script')

<script type="text/javascript">

 //var url =  "//alpha.booktou.in";
 var url =  "//localhost/booktou-s";   
 $(document).on('click','.btn-quickview',function(){ 
    
    var json = {};
    json['serviceProductId'] =  $(this).attr("data-serviceProductId");
    json['bin'] = $(this).attr("data-bin");
    json['_token'] = "{{csrf_token()}}";
     

    $.ajax({
         type: 'post',
         url: url+"/appointment/get-service-details" ,
         data: json,
         success: function(response)
         {      
             if(response.status_code == 7003)
             {    
                 $('#servicedetails').empty().append(response.html);
                 $('#widgetquickview').modal('show');
             }
             else 
             { 
                  
             }

         },
         error: function( ) 
         {
             alert(  'Something went wrong, please try again')
         }
        });  
 });


</script>
@endsection