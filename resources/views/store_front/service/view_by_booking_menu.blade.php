@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>

<div class="outer-top-tm" > 
    <div class="container" style="">  
     <div class="row"> 
        <div class="col-sm-3 col-xs-12 sidebar" >
             
            <div class="sidebar-widget widget_product_categories  d-lg-block d-xl-block" >
             <h5 class="section-title font-weight-bold">Business Names</h5>        
               <div class="fanbox" >
                <div class="fanbox-header d-none d-lg-block d-xl-block">
                    @foreach($business as $biz)
                    <?php  
                              
                                if($biz->banner==""){

                                    $image_url =  $cdn_url."/assets/image/no-image.jpg";

                                } else {

                                    $image_url =  $cdn_url.$biz->banner;

                                } 

                        ?>   
                    <!-- <div> 
                         <a href="{{URL::to('booking/business-details/btm-')}}{{$biz->id}}">{{$biz->name}}</a>
                    </div> -->
                    <a class="text-dark text-decoration-none" 
                    href="{{URL::to('booking/business-details/btm-')}}{{$biz->id}}">              
                        <div class="media p-2 flex-column flex-md-row">
                       
                         <img src="{{$image_url}}" class="mr-3 img-fluid" style="width:70px;">
                          <div class="media-body align-self-center">
                            <small><b> {{$biz->name}}</b></small>
                            <br>
                            <small>
                                {{$biz->locality}}<br>
                                      
                                            {{$biz->pin}}
                            </small>
                          </div>
                      
                        </div>
                    </a>
                    <hr>

                    @endforeach
                </div> 
                </div>
            </div> 
    </div>
        
  

<div class="col-sm-12 col-md-9" > 
  <div class="card mt-3">
        <div class="row card-body"> 
            
        @foreach($products as $items)
            <?php  
                 if($items->photos=="")
                 {
                    $image_url =  $cdn_url."/assets/image/no-image.jpg"; 
                 } else { 
                    $image_url =  $items->photos; 
                 } 
            ?>
            <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 ">
                <div class="card card-sm card-product-grid">
                <a href="{{URL::to('booking/slot-selector')}}/{{$items->bin}}/{{$items->id}}" class="img-wrap">
                    <img src="{{$image_url}}" class="img-fluid"> 
                </a>
                <figcaption class="info-wrap">
                    <a href="{{URL::to('booking/slot-selector')}}/{{$items->id}}" class="title">{{$items->srv_name}}</a>
                    <div class="price mt-1">
                        <p class="card-text">₹ {{$items->actual_price}} </p>
                    </div>
                </figcaption> 
                <div class="text-center"><a  href="{{URL::to('booking/slot-selector')}}/{{$items->bin}}/{{$items->id}}" class="btn btn-primary btn-sm">Book</a></div>
                </div>                            
            </div> 
        @endforeach
           
        </div> 
    </div>
     
 <!-- filtered products --> 
    
</div>
<!--  -->
</div> 
</div>  
</div> 

@endsection 

@section('script')
<script type="text/javascript"> 

</script>
@endsection 