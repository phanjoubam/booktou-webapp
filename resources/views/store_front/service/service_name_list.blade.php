<div>
<table class="table mt-2 ">
<th>Service Name</th>
<th>Price</th>
<th>Action</th>

<tbody>
@foreach($results as $item) 
   
     <tr>
       <td>{{$item->srv_name}}</td>
       <td>{{$item->pricing}}</td>
       <td><button type="button" class="btn btn-primary btnselectitems" 
        data-key="{{$item->id}}"
        data-srvname="{{$item->srv_name}}"
        data-price="{{$item->pricing}}"
        data-details="{{$item->srv_details}}"
        >+</button></td>
     </tr>
   
@endforeach
</tbody>     
</table> </div>