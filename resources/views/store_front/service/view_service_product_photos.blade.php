<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  

?>

<div class="card-header how-it-work"> 
			    <div class="panel panel-default col-sm-12 col-sm-offset-2">
				  <div class="panel-body">
				  	<span class="badge badge-info">1</span>  <h3 class="step-heading text-uppercase">Spot Availability</h3> 
				  	Steps 1 will show you the spot is available or not
				  </div>
				</div> 

</div>
<div class="card-body row">  
	@foreach($days as $listdays)

	<?php 
		if ($listdays['photos']=="") {
			 
			 $image_url = 'https://cdn.booktou.in/assets/image/no-image.jpg';

		}else{

			 $image_url =  $cdn_url.$listdays['photos'];
		}
	?>
		<div class="col-md-3 col-12 col-sm-12 col-xs-4 mb-2 text-center">
			<div class="card"> 
			@if($listdays['status']=="close") 

			   <img src="{{URL::to('public/store/image/booked.png')}}"  class="disabled img-fluid"  /> 
			   
			   <h7 class="text-uppercase">{{$listdays['productName']}}</h7>
			   
			   <br>
			   <span class="badge badge-danger">Already booked</span> 

			  @else
			    
			  <!-- <button class="btn btn-light showbookinghour"> -->
			   <img src="{{$image_url}}" class="img-fluid" />
			   <h7 class="text-uppercase">{{$listdays['productName']}}</h7>
			  <!--  </button> -->
			   
			   @endif
			   
			</div>
		</div>
	@endforeach 

 
</div>

<div class="card-header how-it-work">
	<!-- <h7 class="text-uppercase"></h7> -->
			    <div class="panel panel-default col-sm-12 col-sm-offset-2">
				  <div class="panel-body">
				  	<span>2</span>  <h3 class="step-heading text-uppercase">Time Selection</h3> 
				  	Click the button to continue booking process
				  </div>
				</div> 

</div>
<div class="row card-body">
	<?php $i=0; ?>
	@foreach($days as $listdays)
		@if($listdays['status']=='open')

		@foreach($servicehours as $items) 

			@if($i==0)

						 	@php 
						 	$day = strtoupper($items['dayName']);

						 	@endphp 
						 
							@foreach($items['shiftBreakUps'] as $list)  
							
							<?php 	
							
							if( $day == $dayName)  { 

							?>

							<div class="col-md-4 col-12 col-sm-12 col-xs-6 "> 
							  
							  <button class="btn btn-secondary enabledcheckout" data-serviceDate="{{$serviceDate}}">{{$list}}</button>
							</div>
							
						 	<?php 
						 		}
						 	 ?> 
						 	
		  				    @endforeach  
			@endif

		@endforeach 
		@endif
		<?php	$i++; ?>
	@endforeach
</div>