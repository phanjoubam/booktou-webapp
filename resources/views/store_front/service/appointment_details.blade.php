@extends('layouts.store_front_02')
@section('content')
<style>

 .card1
{
    margin-left: 50px; 
}

  .box
{
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 4px 8px 0 rgba(0, 0, 0, 0.19);
    border-radius: 10px;

}
.card-body {
    /*padding: 8px*/
}



.det {

    width: 220px;
    height: 330px;
    padding-left:10px;
    padding-right:10px;
     margin-left:25px;
 
    }

.det .detImg
{
    height: 130px;
    width: 100%;
    position: relative;
    top:13px;
   
    border-radius:20px ;
}
.det .detImg img
{
    width: 100%;
    height: 105%;
    border-radius:20px ;
}

.det .info
{
    text-align: center;
    margin-top:25px;
    color: black;

}

.det .info p
{
    color: black;
    font-size: 18px;
    
}


#sidebar
{
/*padding: 20px;
*/float: left;
 width:253px  
}

#products {
        width: 70%;
       /* padding: 10px;*/
        margin: 0;
        float: right;
        margin-right: 151px;
        padding-right: 5px;
        padding-left: 10px;
        text-decoration: none;
    }
.text-color
{
    color: black;
    text-decoration: none;
}
.text-color:hover
{
    color: red;
    text-decoration: none;
}


.flex-row
{
    margin-right: 50px;
}
/*paginate*/
.btn
{
    background-color: green;
}

.btn:hover {
  border: 1px solid orange;
  color: #fff;
  background-color:#28a745;
}
.Merhants
{
  width:240px;
}
/*.det
{
   margin-left:30%; 
}*/
.default
{
    margin-left: 30px;
}

</style>
<!-- Navbar section -->
    <div class="container" >
        <div class="row">
        <div class="row mt-5 default">
             <div class="d-flex flex-row mt-4">
            
            <div class="ml-auto mr-lg-4" >
                <div id="sorting" class="border rounded p-1 m-1 mt-3"> <span class="text-muted">default sorting</span> <select name="sort" id="sort">
                        <option value="popularity"><b>Popularity</b></option>
                        <option value="prcie"><b>Price</b></option>
                        <option value="rating"><b>Rating</b></option>
                    </select> </div>
            </div>
        </div> 
        </div>


     <div class="row">
      <div class="col-lg-4">
          <div id="sidebar" >

    
     <div class="py-2  ml-4 Merhants " >
        <div class="sidebar-widget widget_product_categories">
          

            @if(isset($Amerchants))

           <div class="row  border-bottom "> 
             
              <h4 class="font-weight-bold Merhants">Verified Merchants</h4>
        
               <form>
                 @foreach($Amerchants as $items)
                   <div class="text-color"> 
               
                <a class="text-dark" href="{{URL::to('/appointment/business/btm-')}}{{$items->id}}"><label class="text-color">{{$items->name}}</label ></a> 
               </div>
                @endforeach
             </form>
           
           </div>
           @endif



         <div class="py-2  border-bottom">
        <h4 class="font-weight-bold mt-2 Merhants"> Categories</h4>
        
        <form>
          @foreach($acategory as $items)
            <div > 
                <a class="text-dark" href="{{URL::to('/appointment/appointment-details?book[]=')}}{{$items->service_category}}">
                <input type="checkbox" > <label class="text-color">{{$items->service_category}}</label></a>
            </div>
            @endforeach
            

        </form>
    </div>

    </div>
    </div>

     <div class="py-2 border-bottom ml-4">
     <h4 class="font-weight-bold mt-2">Price</h4>
       <form>
         
                          
        <?php
        

        if($Amax_amount < 500)
        {
        ?>
        <a href="" >Under 500 ₹</a> 
        <?php
        }

        else

        {
        ?>

        <a class="text-color" href="" >
            <input type="checkbox">
       <label> Under ₹ 500 </label>
       </a>
       <br>
       
      
            <?php



            $price_band = 500;
            $starting_price = 500;
            $current_price = $Amax_amount;



            while( $current_price > 0 )
            {
            ?>
              
        <a class="text-color" href="" >
        <input type="checkbox">
        <label  >₹ {{number_format($starting_price)}} - ₹ {{number_format($starting_price + $price_band) }}</label>
       </a>
       <br>
        <?php
        
        $current_price -= $price_band ;
        $starting_price += $price_band;



        }
        }

        ?>
         
         
      </form>
  </div>
    

  
</div>

      </div>

    
<div class="col-md-8  row  container" style="height:0;">
  
    @foreach($Aproduct as $items)
        <div class="col-md-4 "> 
            <div class="col-md-12 ">
                <div class="box det mt-4" >
                    <div class="box detImg ">
                       
                       @if($items->photos=="")
                       <img src="{{URL::to('public/asset/theme01/images/no-image.jpg')}}">
                       @else
                       <img src="{{URL::to($items->photos)}}">
                       @endif  
                         
                    </div>

                    <div class="info">
                          <h3>{{$items->srv_name}}</h3>
                        <p>Rs {{$items->actual_price}}</p>
                        <button class="btn btn-success">Book Now</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
 </div>

 </div>

</div>
    </div>
<!-- Sidebar filter section -->

<!-- products section -->





@endsection 
