@extends('layouts.booking_theme_01')
@section('content')
<?php
    $cdn_url = config('app.app_cdn') ;
    $cdn_path = config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<style>

 .wg-box-content {
  position: relative;
  width: 100%;
  height: 20vh;
  margin: auto;
  overflow: hidden;
}
.wg-box-content .wg-box-content-overlay {
  background: rgba(0,0,0,0.7);
  position: absolute;
  height: 100%;
  width: 100%;
  left: 0;
  bottom: 0;
  right: 0;
  opacity: 0;
  -webkit-transition: all 0.4s ease-in-out 0s;
  -moz-transition: all 0.4s ease-in-out 0s;
  transition: all 0.4s ease-in-out 0s;
}
.wg-box-content:hover .wg-box-content-overlay{
  opacity: 1;
}
.wg-box-content-image{
  width: 100%;
}
.wg-box-content-details {
  position: absolute;
  text-align: center;
  padding-left: 1em;
  padding-right: 1em;
  width: 100%;
  top: 50%;
  left: 50%;
  opacity: 0;
  margin-top: 20px;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  -webkit-transition: all 0.3s ease-in-out 0s;
  -moz-transition: all 0.3s ease-in-out 0s;
  transition: all 0.3s ease-in-out 0s;
}
.wg-box-content:hover .wg-box-content-details{
  top: 50%;
  left: 50%;
  opacity: 1;
}
.wg-box-content-details h4{
  color: #fff;
  margin-top: 0.5em;
  font-size: 1em;
  margin-bottom: 0;
  text-transform: uppercase;
}
.wg-box-content-details p{
  color: #fff;
  font-size: 0.8em;
}
.wg-box-fadeIn-bottom{
  top: 80%;
}



 .text-block {
    position: absolute;
    right: 0px;
    background-color: rgba(15, 15, 15, 0.69);
    color: #fbfdf8;
    padding: 20px;
    width: 30%;
    height: 35vh;
}

</style>

<div class='breadcrumb-box'>  
    <div class='container'>
        <div class="row card-body"> 
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/service/booking')}}" style="color:green;">
                                 <b> Booking</b>
                             </a>
                         </li>
                         <li class="breadcrumb-item active text-uppercase" aria-current="page">
                          {{$category}}
                      </li>
                  </ol>
              </nav>
          </div>
          <div class="col-md-6 col-sm-12">

          </div>
      </div>
  </div>
  <div class=" col-md-3 d-none d-sm-block">  
    <input class="form-control form-control-sm" type="text" id="search-business" placeholder="Filter business by keyword" style="height: calc(1.2em + .7rem + 2px);
    padding: .35rem .5rem;
    font-size: .82rem;
    line-height: 1.2;
    border-radius: .2rem;">
</div>
</div>
</div>
</div>

 
<div class="container">
   <div class="row">  
        
                 @if(isset($businesses)) 
                     @foreach($businesses as $item)  
                     <div class="col-md-8 offset-md-2" >
                     <div class="card mt-3 rounded">
                        <div class="card-body"> 
                           <div class="row"> 
                            <div class="col-sm-12" >
                                <p><span class='h2'>{{$item->name}}</span><br/><span style='font-style:italic;'>{{$item->locality}}</span></p> 
                               <hr/> 
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-5" > 
                                <?php

                                if($item->banner==""){

                                    $image_url =  $cdn_url."/assets/image/no-image.jpg";

                                } else {

                                    $image_url =  $cdn_url.$item->banner;

                                }

                                ?>

                                <img src="{{$image_url}}" class='rounded' style="width:100%; ">
                            </div>

                         <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" > 
                            <div class='box-content-sm'> 
                                <h4>Services Offered</h4>
                               <p> 
                                    @php 
                                        $service_counter = 0; 
                                    @endphp
                                    @foreach($services as $service)

                                       @if ( $service->bin ==  $item->id  )
                                        <span><i class='fa fa-check blue'></i> {{ $service->srv_name }}</span><br/> 


                                        @php
                                            $service_counter++;
                                        @endphp

                                       @endif 

                                       @if( $service_counter > 7)
                                        @break
                                       @endif  
                                   @endforeach
                                   <span><i class='fa fa-check blue'></i>  etc ....</span><br/>   
                                </p>
                            </div>
                            <div class='box-bottom text-right'>
                             <a target="_blank" href="{{URL::to('booking/business-details/btm-')}}{{$item->id}}" class="btn btn-primary btn-rounded btn-xs"  >Check Availability</a>
                            </div>

                           </div>
                       </div>
                   </div>
               </div>  
                </div> 
           @endforeach
       @endif 
   
    </div>
</div>





<!-- modal section goes from here -->
<div class="modal fade" id="widgetquickview" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-fullscreen ">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><span class="badge badge-primary">More Service Details</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body" id="servicedetails">

  </div>

</div>
</div>
</div>

<!-- modal section ends here -->

<!-- Sidebar Overlay -->
<div class="sidenav" id="mySidenav" style="">
    <div class="row ">
        <div class="col-12 ">
            <div class="card-title">
                <h5 class="pl-3 mt-3 title-merchant">Business List</h5>
                <a href="javascript:void(0)" class="closebtn showMerchantListX">x</a>
            </div>

        </div>
    </div>
    <hr>

    <ul class="list-unstyled components links">

            @foreach ($businesses as $items)
                <li class="text-dark text-decoration-none" >
                    <a href="{{URL::to('booking/'.$mainmenu.'/business-details/btm-')}}{{$items->id}}">
                    <small>
                   {{$items->name}}
                    </small></a>
                </li>
            @endforeach

          </ul>
         <!--   -->
         <hr>
    </div>
     <!--  -->

@endsection

@section('script')

<script type="text/javascript">

 //var url =  "//window.location";
 var url =  "//localhost/booktou-s";
 $(document).on('click','.btn-quickview',function(){

    var json = {};
    json['serviceProductId'] =  $(this).attr("data-serviceProductId");
    json['bin'] = $(this).attr("data-bin");
    json['_token'] = "{{csrf_token()}}";


    $.ajax({
         type: 'post',
         url: url+"/appointment/get-service-details" ,
         data: json,
         success: function(response)
         {
             if(response.status_code == 7003)
             {
                 $('#servicedetails').empty().append(response.html);
                 $('#widgetquickview').modal('show');
             }
             else
             {

             }

         },
         error: function( )
         {
             alert(  'Something went wrong, please try again')
         }
        });
 });


</script>
@endsection
