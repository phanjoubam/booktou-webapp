@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<style>
.wg-box-content {
  position: relative;
  width: 100%;
  height: 20vh;
  margin: auto;
  overflow: hidden;
} 
.wg-box-content .wg-box-content-overlay {
  background: rgba(0,0,0,0.7);
  position: absolute;
  height: 100%;
  width: 100%;
  left: 0;
  bottom: 0;
  right: 0;
  opacity: 0;
  -webkit-transition: all 0.4s ease-in-out 0s;
  -moz-transition: all 0.4s ease-in-out 0s;
  transition: all 0.4s ease-in-out 0s; 
}
.wg-box-content:hover .wg-box-content-overlay{
  opacity: 1;
}
.wg-box-content-image{
  width: 100%;
}
.wg-box-content-details {
  position: absolute;
  text-align: center;
  padding-left: 1em;
  padding-right: 1em;
  width: 100%;
  top: 50%;
  left: 50%;
  opacity: 0;
  margin-top: 20px;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  -webkit-transition: all 0.3s ease-in-out 0s;
  -moz-transition: all 0.3s ease-in-out 0s;
  transition: all 0.3s ease-in-out 0s;
}
.wg-box-content:hover .wg-box-content-details{
  top: 50%;
  left: 50%;
  opacity: 1;
}
.wg-box-content-details h4{
  color: #fff;
  margin-top: 0.5em;
  font-size: 1em;
  margin-bottom: 0;
  text-transform: uppercase;
}
.wg-box-content-details p{
  color: #fff;
  font-size: 0.8em;
} 
.wg-box-fadeIn-bottom{
  top: 80%;
}



 .text-block {
    position: absolute;
    right: 0px;
    background-color: rgba(15, 15, 15, 0.69);
    color: #fbfdf8;
    padding: 20px;
    width: 30%; 
    height: 35vh;
}

.rating-block{
    background-color:#FAFAFA;
    border:1px solid #EFEFEF;
    padding:15px 15px 20px 15px;
    border-radius:3px;
}
.review-block{
    background-color:#FAFAFA;
    border:1px solid #EFEFEF;
    padding:15px;
    border-radius:3px;
    margin-bottom:15px;
}
.review-block-name{
    font-size:12px;
    margin:10px 0;
}
.review-block-date{
    font-size:12px;
}
.review-block-rate{
    font-size:13px;
    margin-bottom:15px;
}
.review-block-title{
    font-size:15px;
    font-weight:700;
    margin-bottom:10px;
}
.review-block-description{
    font-size:13px;
}


  .box
{
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.19);
    border-radius:7px;
    height:250px;
background-color: transparent;
   /* width:195px;*/
}



.ban
{
  /*height: 400px;*/
 /*
   padding-left:10px;
   padding-right:10px;*/
   
}
.ban.detban
{
   height: 10px;   
    position: absolute;
   
    border-radius:10px ;
}
.ban .detban img
{
    width:100%;
    height:200px;
    /*margin-left:15px;*/
    border-radius:10px ;

}

.bnow
{
    text-align: center;
    color: #fff;
   background-color: #2e9c11;
   border-color: #2e9c11;

}

.bnow:hover {
  border: 1px solid orange;
  color: #fff;
  background-color:#2e9c11;
  background-color: #2e9c11;
}
.bnow:focus {
  color: #fff;
  border: 1px solid orange;
  background-color: #2e9c11;
  border-color: #2e9c11;
}
.btn-sml {
    
    font-size:13px;
    border-radius:15px;
}
.text-color
{
    color: black;
    text-decoration: none;
}
.text-color:hover
{
    color: red;
    text-decoration: none;
}


.bnow
{
 text-align: center;
      
}
.tname
{
    font-size:14px;
}  

#ads .card:hover {
            background: #fff;
           /* box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);*/
            border-radius: 4px;
           
            /*transition: all 0.3s ease;*/

        }


.filters-container {
  padding: 15px 20px;
    padding-bottom: 15px;
  background-color: #fff;
  padding-bottom: 0px;
  margin-bottom: 20px;
  font-size: 14px;
  border-radius: 4px 4px 0px 0px;
  box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
} 
.image-grid-cover {
width: 100%;
background-size: cover;
min-height: 100px;
position: relative;
margin-bottom: 30px;
text-shadow: rgba(0,0,0,.8) 0 1px 0;
border-radius: 4px;
}
.image-grid-clickbox {
position: absolute;
top: 0;
right: 0;
bottom: 0;
left: 0;
display: block;
width: 100%;
height:100%;
z-index: 20;
border-radius: 40px;

}



.image-grid-cover {
position: relative;
width: 100%;
max-width: 300px;
}



.image {
display: block;
width:100%;
border-radius: 40px;
height:100px;
}



.overlay {
position: absolute;
bottom:0;
/*top: 20px;
*/background-color: black;
background: rgb(0, 0, 0);
background: rgba(0, 0, 0, 0.5); /* Black see-through */
/*color: #f1f1f1; */
width: 100%;
transition: .5s ease;
opacity:0;
height: 100%;
color: white;
font-size: 20px;
padding:12px;
border-radius: 40px;
text-align: center;
z-index: 100;
}



.image-grid-cover:hover .overlay {
opacity: 1;
}
   
#moredetails
{
    font-size: 13px;
}



</style>
<div class="outer-top-tm" > 
    <div class="container " style="">  
     <div class="row"> 

        <div class="col-sm-12 col-md-3 d-none d-sm-block">
             
         <div class='card mb-3 sidebar-widget widget_product_categories'>

            <div class="card-body"> 
                <h3 class="font-weight-bold" style="font-size: 20px;">Business Names</h3><hr>  
            <div class="py-2  border-bottom"> 
              
                @foreach($Amerchants as $items)   

                        <?php  
                              
                                if($items->banner==""){

                                    $image_url =  $cdn_url."/assets/image/no-image.jpg";

                                } else {

                                    $image_url =  $cdn_url.$items->banner;

                                } 

                        ?>        
                <!-- <div> 
                <a class="text-dark text-decoration-none" 
                href="{{URL::to('appointment/business/btm-')}}{{$items->id}}">{{$items->name}}
                </a>
                </div> -->

                 <a class="text-dark text-decoration-none" 
                    href="{{URL::to('appointment/business/btm-')}}{{$items->id}}">              
            <div class="media p-2 flex-column flex-md-row">
           
             <img src="{{$image_url}}" class="mr-3 img-fluid" style="width:70px;">
              <div class="media-body align-self-center">
                <small><b> {{$items->name}}</b></small>
                <br>
                <small>
                    {{$items->locality}}<br>
                          
                                {{$items->pin}}
                </small>
              </div>
          
            </div>
              </a>
              <hr>
                @endforeach 
            </div>
            </div> 
        </div> 
       </div>

       <!-- <div class="col mb-2 d-block d-sm-none"> 
        <button class="btn btn-warning float-right btn-sm showMerchantList"> 
         <span class="fa fa-filter"></span> View Business 
        </button>
        </div>  -->

       <div class="col-md-9 col-sm-12">
        <div class="card">
            <div class="row card-body">
            
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item text-uppercase">
                            <a class="text-weight-bold" href="{{URL::to('/service/appointment')}}" style="color:green;">
                               <b> Appointment</b>
                            </a>
                        </li> 
                        <li class="breadcrumb-item active text-uppercase" aria-current="page">
                          {{$category}}
                        </li>
                      </ol>
                    </nav> 
                </div>
                <div class="col-md-6 col-sm-12">
                    <input class="form-control form-control-sm" type="text" id="search-business" placeholder="Filter business by keyword" style="height: calc(1.2em + .7rem + 2px);
                                                    padding: .35rem .5rem;
                                                    font-size: .82rem;
                                                    line-height: 1.2;
                                                    border-radius: .2rem;">
                    </div>
                </div>

                   <!-- <input class="form-control form-control-sm" type="text" id="search-business" placeholder="Filter business by keyword" style="height: calc(1.2em + .7rem + 2px);
                                                    padding: .35rem .5rem;
                                                    font-size: .82rem;
                                                    line-height: 1.2;
                                                    border-radius: .2rem;"> -->

                </div>   
                <div class=" col-md-3 d-none d-sm-block">
                            <div class="  text-right" >
                                <form class="woocommerce-ordering" method="get" >
                                    <select name="orderby" class="orderby" aria-label="Shop order">
                                        <option value="menu_order" selected="selected" >Default sorting</option>
                                        <option value="popularity">Sort by popularity</option>
                                        <option value="rating">Sort by average rating</option>
                                        <option value="date">Sort by latest</option>
                                        <option value="price">Sort by price: low to high</option>
                                        <option value="price-desc">Sort by price: high to low</option>
                                    </select>
                                    <input type="hidden" name="paged" value="1">
                                </form>
                            </div>
                        </div>
            </div>
        </div>

         <div class="loading">
            <div class="d-none d-sm-block">
             @if(isset($Amerchants))
                @foreach($Amerchants as $item)
                 
                <div class="card mt-3">
                    <?php  
                              
                                if($item->banner==""){

                                    $image_url =  $cdn_url."/assets/image/no-image.jpg";

                                } else {

                                    $image_url =  $cdn_url.$item->banner;

                                } 

                        ?>
                    
                    <img src="{{$image_url}}" style="width:100%; height: 60vh;">
                      <div class="text-block">
                        <h4>{{$item->name}}</h4>
                        <p>
                           {{$item->locality}}<br>
                           {{$item->landmark}}<br>
                                {{$item->pin}}<br> 
                                {{$item->city}}<br>
                                {{$item->phone_pri}}
                            
                        </p>

                         @if (count($item->products)>0) 
                            <a href="{{URL::to('appointment/business/btm-')}}{{$item->id}}" class="btn btn-success btn-xs" style="background-color:#36b416;">View all service</a> 
                             @endif
                            
                      </div>


                    <div class="card-body prservice" id="loading-next" style="margin-top: -200px;"> 
                           <div class="row d-flex justify-content-center mt-3">

                        <!-- section for displaying items for particular business -->
                            <?php 
                            
                            if (count($item->products)>0) {
                                
                                $i=0;        
                                foreach ($item->products as $sitems) { 
                                                if($i<=3){
                                ?>
                                
                                <!-- <div class="col-12 col-sm-12 col-md-2 image-grid-item m-2 card ">

                                    <div class="image-grid-cover ">
                                    <a href="#" class="image-grid-clickbox mt-2">
                                    @if($sitems->photos=="")
                                    <img src="{{URL::to('public/asset/theme01/images/no-image.jpg')}}" class="image" height="100px" width="100px">
                                    @else
                                    <img src="{{$sitems->photos}}" class="image">
                                    @endif
                                    <div class="overlay">
                                    <p class="text-weight-bold" style="font-size:13px;">{{$sitems->srv_name}}</p>
                                    <button class="btn btn-info btn-sm btn-quickview"
                                    data-serviceProductId="{{$sitems->id}}"
                                    data-bin="{{$item->id}}"
                                    >more details</button> 
                                    </div>
                                    </a> 
                                    </div>
                                     
                                    <a class="btn btn-success btn-sm mb-2" href="{{URL::to('booking/slot-selector')}}/{{$sitems->id}}" 
                                    style="background-color:#36b416;">Book</a>
                                  
                                 
                            </div> -->
                            <?php  
                              
                                if($sitems->photos==""){

                                    $photos =  $cdn_url."/assets/image/no-image.jpg";

                                } else {

                                    $photos =  $sitems->photos;

                                } 

                        ?>
                            <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 "> 
                                <div class="card" style="border-radius: 30px;border:1px solid #ccc;">
                                       <div class="wg-box-content" style="border-radius: 30px;">
                                         
                                          <div class="wg-box-content-overlay"></div>
                                          <img class="img-fluid wg-box-content-image " src="{{$photos}}">
                                          <div class="wg-box-content-details wg-box-fadeIn-bottom">
                                            <h4 class="wg-box-content-title text-uppercase">
                                                {{$sitems->srv_name}}
                                            </h4>


                                            <a class="wg-box-content-text btn-quickview badge badge-info" href="javascript::void();" data-serviceProductId="{{$sitems->id}}"
                                                    data-bin="{{$item->id}}">
                                            View more details
                                            </a>

                                            <br>
                                            <a class="btn btn-success btn-sm mt-2" href="{{URL::to('booking/slot-selector')}}/{{$sitems->bin}}/{{$sitems->id}}" 
                                                style="background-color:#36b416;">Book
                                            </a>
                                            <!-- <p class="wg-box-content-text"> 
                                                <button class="btn btn-secondary btn-sm btn-quickview"
                                                    data-serviceProductId="{{$sitems->id}}"
                                                    data-bin="{{$item->id}}"
                                                    >more details</button> 
                                            </p>
                                            --> 
                                          </div>
                                        
                                        </div>
                                </div>                            
                            </div> 
                           
                              <?php      
                                    
                                 } 

                                 $i++;
                                }
                               
                                }
                            ?>
                        <!-- displaying ends here --> 
                         </div> 
                    </div>
                </div> 

                @endforeach                
                @endif
                </div>


            <div class="d-block d-md-none">
               <div class="row"> 
                 @if(isset($Amerchants))
                @foreach($Amerchants as $item)
                <?php  
                              
                                if($item->banner==""){

                                    $image_url =  $cdn_url."/assets/image/no-image.jpg";

                                } else {

                                    $image_url =  $cdn_url.$item->banner;

                                } 

                        ?>
                
                <div class="col-6 col-sm-6 col-xs-6  mt-2">
                    <div class="card">
                        <a href="{{URL::to('booking/business-details/btm-')}}{{$item->id}}">
                           
                            <img src="{{$image_url}}" class="img-fluid"> 
                            <p class="text-center post-title  my-1" style="color:#000;">{{$item->name}}</p>
                            
                        </a>
                    </div>
                </div>
                
                @endforeach
                @endif
            </div>
            </div>




            </div>
       </div> 
    </div> 
    </div>
</div>

 







<!-- modal section goes from here -->
<div class="modal fade" id="widgetquickview" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-fullscreen ">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><span class="badge badge-primary">More Service Details</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="servicedetails">
        
      </div>
       
    </div>
  </div>
</div> 

<!-- modal section ends here -->
                      
<!-- Sidebar Overlay -->
    <div class="sidenav" id="mySidenav" style="">
    <div class="row ">
        <div class="col-12 "> 
            <div class="card-title">
            <h5 class="pl-3 mt-3 title-merchant">Business List</h5>
            <a href="javascript:void(0)" class="closebtn showMerchantListX">x</a>
        </div>
         
    </div> 
    </div>
    <hr>
    
    <ul class="list-unstyled components links"> 
            
            @foreach ($Amerchants as $items)
                <li class="text-dark text-decoration-none" >
                    <a href="{{URL::to('booking/business-details/btm-')}}{{$items->id}}"> 
                    <small>
                   {{$items->name}}
                    </small></a> 
                </li> 
            @endforeach
             
          </ul>       
         <!--   -->
         <hr>
    </div>
     <!--  -->

@endsection 

@section('script')

<script type="text/javascript">

 //var url =  "//alpha.booktou.in";
 var url =  "//localhost/booktou-s";   
 $(document).on('click','.btn-quickview',function(){ 
    
    var json = {};
    json['serviceProductId'] =  $(this).attr("data-serviceProductId");
    json['bin'] = $(this).attr("data-bin");
    json['_token'] = "{{csrf_token()}}";
     

    $.ajax({
         type: 'post',
         url: url+"/appointment/get-service-details" ,
         data: json,
         success: function(response)
         {      
             if(response.status_code == 7003)
             {    
                 $('#servicedetails').empty().append(response.html);
                 $('#widgetquickview').modal('show');
             }
             else 
             { 
                  
             }

         },
         error: function( ) 
         {
             alert(  'Something went wrong, please try again')
         }
        });  
 });

$(document).ready(function(){
  $("#search-business").on("keyup", function() {
    var value = $(this).val().toLowerCase();
      $(".loading div").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        $('#loading-next').addClass('show');
      });
    }); 
});

</script>
@endsection