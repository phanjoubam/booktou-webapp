@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>
<div class='breadcrumb-box'>  
    <div class='container'>  
        <div class="row mt-2">   
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/')}}" style="color:green;">
                                   <b> SHOPPING</b>
                                </a>
                            </li> 
                            <li class="breadcrumb-item active text-uppercase" aria-current="page">
                           {{$business->name}}
                            </li>
                          </ol>
                        </nav>
                    </div>
<div class="col-md-6 col-sm-12 text-end">

     <form  method="get">
                        <select name="ps" id="shopOrder" class="orderby" aria-label="Shop order">
                                                    <option value="menu_order" selected="selected">Default sorting</option>

                                                    <option value="date">Sort by latest</option>
                                                    <option value="lh">Sort by price: low to high</option>
                                                    <option value="hl">Sort by price: high to low</option>
                                            </select>
                        <input type="hidden" name="p" value="1">
                                    </form>


</div>
 
            
        </div>
    </div>
</div>
<div class='outer-top-tm'> 
    <div class="container">  
    <div class='row'> 
        <div class='col-sm-3 col-xs-12   sidebar'>

            <?php 
                $mainmenu;
                  foreach(request()->route()->parameters as $val)
                  {
                       $mainmenu =  $val;
                  } 

                   
        ?>

        <div class="sidebar-widget widget_product_categories">
           <div class="row"> 
            <div class="col-md-10">
            <h3 class="section-title">{{ $business->name }}</h3>
          </div>
          <div class="col-md-2">
           <h3 class="section-title"> @if($business->is_verified=="yes") 
            <img src="{{URL::to('/public/store/image/bluetick.png')}}" style="width: 20px;" />
            @endif
          </h3>
          </div>
          </div>
                            <div class='fanbox'>
                                <div class='fanbox-header'>
                                 <img src='{{ $cdn_url  }}{{ $business->banner }}' width='100%'  />
                                </div>
                                <div class='fanbox-body'>
                                    <p><span class="widget-title mb-30"></span><br/>

                                    {{ $business->locality }}<br/>
                                     {{ $business->landmark }}  {{ $business->city }},  {{ $business->state }}
                                    </p>  
                                </div>   
                            </div>

                        </div>



<div class="sidebar-widget   widget_price_filter">
    <h3 class="section-title">Business QR Code</h3> 
    <img src="{{ $business->qrcode_url }}" alt="Business QR Code" />
</div>


<div class="accordion" id="accordionFilter">
  <div class="accordion-item">
    <h2 class="accordion-header" id="filterheadingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#filterOne" aria-expanded="true" aria-controls="filterOne">
        Categories
      </button>
    </h2>
    <div id="filterOne" class="accordion-collapse collapse show" aria-labelledby="filterheadingOne" data-bs-parent="#accordionFilter">
      <div class="accordion-body">
  <form id="cat-check" action="{{ URL::to('/business') }}/{{ strtolower($business->merchant_code) }}" method='get' >
    
        @php 
        $all_filters = array_unique(array_filter(   $filters   )); 
    @endphp 

    @foreach ($product_categories as $cat)
        @php 
            $category = $cat->category; 
            $checked = ""
        @endphp
        
        @foreach ($all_filters as $filter_item)
            @if($filter_item == $category)
                @php 
                    $checked = "checked"
                @endphp
            @endif
        @endforeach

        <div class="form-check checkbox-container">

<input class="form-check-input category-check" {{ $checked }}  onChange='submit();'
  type="checkbox"  name="sf[]"  
  value="{{ $category }}" 
  data-check="{{strtolower( $category )}}" 
  data-key="{{strtolower($business->merchant_code)}}">
  <label class="form-check-label" for="{{strtolower( $category )}}">{{  $category }}</label>

</div>


    
 
@endforeach  
   </form>




      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="filterheadingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#filterTwo" aria-expanded="false" aria-controls="filterTwo">
        Price Range
      </button>
    </h2>
    <div id="filterTwo" class="accordion-collapse collapse" aria-labelledby="filterheadingTwo" data-bs-parent="#accordionFilter">
      <div class="accordion-body">
       <?php
    
    $minval = $min_amount;
    $full_range = $max_amount -  $minval; 
    
    $range_size = 200;
    $no_of_ranges = $full_range / $range_size;
    

    for($i = 0 ; $i <= $no_of_ranges; $i++)
    {
        ?>
        <a class="text-color" href="{{ Request::fullUrl() }}&minprice=<?php echo (number_format($minval)) ; ?>&maxprice=<?php echo (number_format($minval + $range_size)); ?>">
         

            <label>₹ <?php echo (number_format($minval)) ; ?> - ₹ <?php echo (number_format($minval + $range_size)); ?></label>
        </a>  
        <br/>
        <?php 
        $minval+=$range_size;
    }
    
    ?>
      </div>
    </div>
  </div>
 
</div>


 

    </div>
 
<div class='col-sm-9 col-xs-12 col-md-9 col-mlg-9  '>
   
<div class="row mt-2 cb-p2 sort-list" id="loading"> 
{{csrf_field()}}
  
 @if($maincategoryMenu=="Shopping")
        @if( isset($products))
        @foreach ($products  as $product)  
            <?php

                if ($product->photos=="") 
                {
                    $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                }
                else
                {
                     $image_url = $product->photos;
                }
                $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 
            ?> 
 
         <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 mb-3 ">
            
            <div class="card"  >
                <a class="text-center" href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="title">
                <div style='background-image:url("{{ $image_url }}"); min-height: 250px; background-size: cover;
                background-position: center;background-repeat: no-repeat; background-position:fixed' 
                class="card-img-top" ></div>

                  
                        @if($product->food_type=="Veg")
                        <span class="foodtype"><i style="color: green;" class="fa fa-circle"></i></span>
                        @elseif($product->food_type=="Non-veg")
                        <span class="foodtype"><i style="color: brown;" class="fa fa-circle"></i></i></span>
                        @endif
                    


            </a>

                <div class="card-body">
                    <a class="text-center" href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="title">
                    <h5 class="card-title">{{$product->pr_name}}</h5>
                    <p class="card-text">
                        <div class="price mt-1 text-left">
                            @if($product->unit_price == $product->actual_price)
                            ₹ {{$product->actual_price }}
                            @else     
                            ₹ {{$product->actual_price }} <span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}
                            </span>
                            @endif
                        </div>
                    </p>
 
                    </a>

        <div>            
             @if($business->is_block == "no" &&  $business->is_open == "open"  )     
                @if(  $product->stock_inhand == 0  )
                    <button type="button"  class="btn btn-outline-primary btn-sm btn-block btn-rounded" disabled>Out of stock</button> 
                @else
                    <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->prsubid }}"  class="btn btn-primary btn-sm btn-add-item btn-block btn-rounded">Add to Cart</button> 
                @endif 
            @endif

                    <button type="button" data-action="wish" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->prsubid }}" 
        class="btn btn-warning btn-sm btn-wish-item btn-block btn-rounded"> Add to Wishlist </button> 


        </div>


 
                </div>
            </div>
            
  </div>
 

           
   @endforeach
  <div class='col-md-12 mt-3 text-center'>
   {{      $products->appends(request()->input())->links()  }} 
</div>
  @endif
  <!-- shopping section ends here -->
  @else

  <!-- appointment section starts here -->
  @if( isset($products))
        @foreach ($products  as $product)  
            <?php  
  
                 
                 if ($product->photos=="") {
                      $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                }else{
                     $image_url = $cdn_url.$product->photos;
                }

                $formatted_name = strtolower(preg_replace('/[^A-Za-z0-9\-]/','-',trim($product->srv_name))); 
            ?> 
          
            <div class=" col-sm-12 col-md-3 col-lg-3 col-xl-3 "> 
                <div class="card card-sm card-product-grid"> 
                <a href=""  class="img-wrap">
                    <img src="{{ $image_url }}"> 
                </a>
                 <figcaption class="info-wrap">
                    
                   
                    <div class="col text-left">
                    <a class="text-center" href=""  class="title">
                    {{$product->srv_name}}</a>
                    <br>
                    <span>{{$product->srv_details}}</span>   


                    <div class="price mt-1 text-left">
                    <p class="card-text">₹ {{$product->actual_price}} </p> 
                  </div>
                  </div> 
 

                </figcaption>

         <div class='info-wrap  '>
            
         <div class="row">
        <div class="col">  
 
        <a href="{{URL::to('booking/slot-selector')}}/{{$product->id}}" 
        class="btn btn-primary btn-sm btn-block">Book</a> 

        </div>

    </div>
                
         
    </div>
        </div>
    </div>
           
           @endforeach
          <div class='col-md-12'>
           {{      $products->appends(request()->input())->links()  }} 
          </div>
  @endif


  <!-- appointment section ends here -->
  @endif

</div> 
 <!-- filtered products --> 
</div>
</div> 
</div> 
</div> <!-- container -->
</div> <!-- spacer -->



<div class="modal wg-wish" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Item Added to Cart</h5>
        <button type="button" class="btn-close"  data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="sys-alert"></div>
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div> 
    </div>
  </div>
</div>

@endsection 

@section('script')

 <style>
      
span.foodtype {
    margin-top: -25px;
    margin-left: 5px;
    margin-right: 10px;
    width: 20px;
    height: 20px;
    background-color: #fff;
    display: block;
}
    </style>



<script>

 $(document).ready(function(){
  $("#search-product").on("keyup", function() {
    var value = $(this).val().toLowerCase();
      $("#loading div").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });
  
    </script>

@endsection 
