@extends('layouts.service_template_01')
@section('style')

<style type="text/css">

.owl-dots,  .owl-prev, button.owl-dot{

   display: none !important;
}
.disabled {
   display: none !important;
}
</style>
@endsection

@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ;
?>


<section>
  <div class="container">
    <div class='row'>

        <?php

        if( $business->banner == ""  || $business->banner == null )
        {
            $image_url =   URL::to('/public/assets/image/slider_paa.jpg');
        }
        else
        {
            $image_url =  $cdn_url. $business->banner;

        }

        ?>  

        
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mt-4 float-sm-start" > 
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ $image_url }}" class="d-block w-100 rounded">
            </div> 
        </div>

        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    </div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mt-4 float-sm-end" > 
            <div class="p-5" >
                <h1 class="display-6">{{$business->name}}</h1>
                <h5 class="section-title font-weight-bold"></h5> 
                <p>
                     {{$business->landmark}} {{$business->city}},{{$business->state}}
                </p> 
                <button role='link' type='button' class='btn btn-outline-secondary btn-rounded'>
                   <i class='fa fa-check-circle'></i> Verified Merchant
                </button>
     </div> 
</div>

    </div>
    </div>
 
</section>

<div class='hr-md mt-3'></div> 
<div class="container " >
    <div class='row'>
        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 p-3' id="contentpackages"> 
            <div class="owl-carousel owl-theme">
                @foreach( $package_categories as $package_category)
                <a class="filter-box" 
                href="{{ URL::to('/booking/business/prepare-service-booking') }}?bin={{ $business->id }}&category={{ strtolower($package_category->category ) }}#contentpackages">
                 <img width="20px" src="{{ $package_category->icon_url}}"   > {{ ucwords( strtolower($package_category->category ) ) }}</a>
                 @endforeach
             </div>
         </div>
     </div>
 </div> 
 <div class='hr-md '></div>
 
<div class="container mt-3">
    <div class="row">    

            @foreach($packages as $item) 

            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3"> 
                <div class="card mb-4">  
                    <div class='card-body'>
                        <div class="row align-items-start"> 
                            
                            <div class="col-md-12 float-sm-start float-md-end float-lg-end">
                            @if( $item->photos != "") 
                                @php
                                    $imageurl = $item->photos;
                                @endphp
                                <img src="{{ $imageurl }}" class="img-fluid rounded">

                            @else
                                
                                @if( count($service_profiles) > 0)
                                    @php 
                                        $active_slider ="active";
                                    @endphp
                                    <div id="service_slider{{$item->id}}" class="carousel slide" data-bs-ride="true" >
                                      <div class="carousel-inner">
                                        @foreach($service_profiles as $service_profile)
                                            @if(  $service_profile->service_product_id  == $item->id  )
                                                <div class="carousel-item {{ $active_slider }}">
                                                  <img src="{{ $service_profile->photos }}" class="d-block w-100" alt="...">
                                                </div>
                                                @php 
                                                    $active_slider ="";
                                                @endphp
                                            @endif 
                                        @endforeach  
                                    </div>
                                  
                                    <button class="carousel-control-prev" type="button" data-bs-target="#service_slider{{$item->id}}" data-bs-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Previous</span>
                                        </button>
                                    <button class="carousel-control-next" type="button" data-bs-target="#service_slider{{$item->id}}" data-bs-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Next</span>
                                    </button>
                                    </div> 
                                @endif 
                            @endif
                </div>

                <div class="col-sm-12 col-md-12">
                            <div class='box-content-sm p-3'> 
                                <p><span class='h5'>{{$item->srv_name}}</span></p>
                                <p>
                                @php 
                                    $len = strlen ( $item->srv_details ); 
                                    if($len > 250) 
                                    {
                                         
                                        echo substr($item->srv_details, 0, 250 ) . ' [+]';
                                    }
                                    else
                                    {
                                          echo $item->srv_details;
                                    } 
                                @endphp  
                             </p>
                         </div> 
                        </div>

            </div>
             <div class="row align-items-start"> 
                <div class="col-md-8"> 
                        <div class=" mt-1 text-right">
                            <span class="h5">Starts from <span class='orange'>{{$item->actual_price}}₹</span></span>
                        </div>
                </div>
                <div class="col-md-4 text-right"> 
                    <a  href="{{URL::to('/booking/business/booking-calendar')}}?bin={{ $item->bin }}&package={{ $item->id }}" 
                        class="btn btn-primary btn-sm btn-rounded">
                    Book Now</a>
                </div>
            </div>


 
                    <div class="text-center">
                        
                    </div>
                </div></div>
            </div>


            @endforeach
    

</div> 
</div> 
@endsection



@section('script')

<script>
$(document).ready(function ()
{ 
    $(".owl-carousel").owlCarousel({
        margin:10,
        center: false,
        items:2,
        loop:false,
        navigation : false,
        autoWidth:true,
    }); 


});

</script>

@endsection