@extends('layouts.store_front_02')
@section('content')  
 <?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<style type="text/css">

.owl-dots,  .owl-prev, button.owl-dot{

   display: none !important;
}
.disabled {
   display: none !important;
}

  

/*carousel*/

  .carousel-inner img {
    width: 80%;
    height: 80%;
  }

 
/*carousel ends here*/
</style>

<section>
 
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{URL::to('/public/assets/image/slider_paa.jpg')}}" class="d-block w-100">
            </div> 
        </div>

        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


    <form action="{{action('Booking\BookingController@searchBookingServices')}}" method="get">
    <div class="search-box-outer"> 
        <div class="search-box-inner"> 
        <div class="search-inpage"> 
            <div class="row"> 
                <div class="col-md-12">
                    <h2>Search picnic location or homestay</h2>
                    <hr/>
                </div>
 
                    <div class="input-group">
                      <input type="text" name="keyword" class='form-control' placeholder="Picnic, park">

                    <select class="form-control search-slt" id="city" name="city" placeholder="Imphal">
                        @foreach($city as $citylist)
                        <option value="{{$citylist->cityList}}">{{$citylist->cityList}}</option>
                        @endforeach
                    </select>
                    <button type="submit" value='search' class='btn inpgsearch'>
                        <i class='fa fa-search'></i>
                    </button>

                </div> 
                
            </div>
        </div>
        </div>
    </div>
    </form> 
</section>



<section  class="bg-ltgray icon-block-lg">
   <div class="container"> 
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center text-uppercase">Package Categories</h4>
        </div>

        <div class="col-md-12 mt-5">
        <div class="owl-carousel owl-theme">

        @foreach( $package_categories as $package_category)

        @php
        $encoded_url = implode("-", explode(" ", $package_category->category) );
        @endphp

        <div class="item" style="width:110px; text-align:center">
            <a href="{{  URL::to('/booking/browse-packages')}}/{{ strtolower(  $encoded_url ) }}">
                <img  src="{{ $package_category->icon_url}}" class="img-fluid rounded-circle  p-2">
                <span class='text-sm'>{{ ucwords($package_category->category) }}</span>
            </a>
        </div> 
        @endforeach
</div> 
        </div> 
    </div>
</div>
</section> 
 
              

<section class="mt-2 bgcolor p-3">
    <h4 class="text-center text-uppercase">Packages from Popular Destinations</h4>
    <div class="container">
        <div class="row carousel1 mt-5">
            @foreach($bookingrelated as $items) 
            <div class="col-md-2 col-6 col-sm-6 col-xs-6 mb-2" style="margin-right:20px;">
                <a href="{{ URL::to('/booking/business/prepare-service-booking') }}?bin={{ $items->id }}">
                    @if($items->banner=="")
                       <?php 
                        $image_url = 'https://cdn.booktou.in/assets/image/no-image.jpg';
                       ?>   
                    @else
                       <?php 
                       $image_url = $cdn_url.$items->banner;
                       ?>
                    @endif
                    <img src="{{$image_url}}" class="img-fluid  p-2">
                    <h7 class="card-title text-uppercase text-center"><small> {{$items->name}}</small></h7> 
                </a>
            </div> 
            @endforeach

        </div>
    </div>
</section>
 

<section class="mt-2 bgcolor p-3 ">
<h4 class="text-center">TOP RATED PARTNERS</h4>
<div class="container mt-3"> 
  <div class="row col-md-12"> 
  <section class="row">
                 
                <div class="col-12 col-md-6 pt-2 pl-md-1 mb-3 mb-lg-4">
                    
                    @php 
                    $i=0;
                    @endphp

                    @foreach($bookingrelated as $items)
                    
                        @if($items->banner=="")
                            <?php 
                                $image_url = 'https://cdn.booktou.in/assets/image/no-image.jpg';
                            ?>  
                        @else
                           <?php 
                                $image_url = $cdn_url.$items->banner;
                           ?>    
                        @endif

                    @if($i==0)
                                <div class="card border-0 rounded-0 text-light overflow zoom">
                                    <div class="position-relative">
                                        <!--thumbnail img-->
                                        <div class="ratio_left-cover-1 image-wrapper">
                                            <a href="{{ URL::to('/booking/business/prepare-service-booking') }}?bin={{ $items->id }}">
                                                <img class="img-fluid w-100" src="{{$image_url}}">
                                            </a>
                                        </div>
                                        <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                            <!--title-->
                                            <a href="{{ URL::to('/booking/business/prepare-service-booking') }}?bin={{ $items->id }}">
                                                <h2 class="h3 post-title text-white my-1">
                                                    {{$items->name}}
                                                </h2>
                                            </a> 
                                        </div>
                                    </div>
                                </div>
                                        @endif
                                        @php 
                                            $i++;
                                        @endphp

                                        @endforeach  
                                </div>
                
                
               
                <div class="col-12 col-md-6 pt-2 pl-md-1 mb-3 mb-lg-4">
                    <div class="row">
                        @php 
                            $j=0;
                        @endphp
                        @foreach($bookingrelated as $items)
                        
                        @if($j>0)


                        @if($items->banner=="")
                            <?php 
                                $image_url = 'https://cdn.booktou.in/assets/image/no-image.jpg';
                            ?>  
                        @else
                           <?php 
                                $image_url = $cdn_url.$items->banner;
                           ?>    
                        @endif
                       
                        <div class="col-6 pb-1 pt-0 pr-1">
                            <div class="card border-0 rounded-0 text-white overflow zoom">
                                <div class="position-relative">
                                    <!--thumbnail img-->
                                    <div class="ratio_right-cover-2 image-wrapper">
                                        <a href="{{ URL::to('/booking/business/prepare-service-booking') }}?bin={{ $items->id }}">
                                             <img src="{{$image_url}}" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                         
                                        <a href="{{ URL::to('/booking/business/prepare-service-booking') }}?bin={{ $items->id }}">
                                            <h2 class="h5 text-white my-1">{{$items->name}}</h2>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                          @endif
                                @php 
                                    $j++;
                                @endphp
                        @endforeach 
                         
                    </div>
                </div>
            </section>

  </div>

</div>
</section>

<div class="clearfix"></div>


 
@endsection 
@section('script')

<script type="text/javascript">


  $(document).ready(function(){
      $(".owl-carousel").owlCarousel({
        margin:10,
        loop:true,
        navigation : false,
        autoWidth:true,
        items:8
    });
  });


     if (screen.width<500) { 
                  $('.carousel1').slick({
                  rows: 1,
                  infinite: false,
                  slidesToShow: 1,
                  slidesToScroll: 1
                });

        }else{ 
                  $('.carousel1').slick({
                  rows: 1,
                  infinite: false,
                  slidesToShow: 4,
                  slidesToScroll: 3
                  }); 
        }

</script>
@endsection 