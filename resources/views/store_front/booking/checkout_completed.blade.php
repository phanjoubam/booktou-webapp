@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
 

?>

 
 

<div class='outer-top-tm'> 
<div class="container">
<div class="row">

     <div class="col-12">
                    <div class="page-title text-center">
                        <h1>Your Booking List</h1>
                    </div>
                </div>


  <div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1"> 

        @if (session('err_msg')) 
            <div class="alert alert-info">
              {{ session('err_msg') }}
            </div> 
        @endif                
  </div>  
<div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1"> 

  
  
        <p class='alert alert-info'>Your booking checkout is completed!</p>
   


    </div> 
</div>
</div>
 </div>
 


 
@endsection 


@section('script')

<script>

 

</script>  

@endsection 