<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<! JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<div class="row">
  <div class="col-sm-6">
      <div class="form-group">
          <div class="input-group date" id="id_startTime" data-target-input="nearest">
              <input type="text" class="form-control datetimepicker-input" data-target="#id_startTime"/>
              <div class="input-group-append" data-target="#id_startTime" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
              </div>
          </div>
      </div>
  </div>
  <div class="col-sm-6">
      <div class="form-group">
          <div class="input-group date" id="id_endTime" data-target-input="nearest">
              <input type="text" class="form-control datetimepicker-input" data-target="#id_endTime"/>
              <div class="input-group-append" data-target="#id_endTime" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
              </div>
          </div>
      </div>
  </div>
  <div class="form-group pmd-textfield pmd-textfield-floating-label">
	<label class="control-label" for="timepicker">Select Time</label>
	<input type="text" class="form-control" id="timepicker">
</div>
</div>

<script type="text/javascript">
	 $('#id_startTime').datetimepicker({
      format: 'LT'
  });
$('#id_endTime').datetimepicker({
      format: 'LT'
  });


//::Time Condition
$('#id_startTime').on("change.datetimepicker", function (e) {
    // if(e.date){
    //     $('#id_endTime').datetimepicker(e.date.add(15, 'm'));
    // }
    // $('#id_endTime').datetimepicker('minDate', e.date)
});
</script>
 

