@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public

    $total_cost = $delivery_cost = 0.0;

?>

<div class='outer-top-tm'> 
    <div class="container">  
    <div class='row'> 
  
  <div class="col-12 col-md-6 offset-md-3  mb-2">
        <div class="card">
            <div class="card-body">
                    <div class="checkout_details_area mt-50 clearfix"> 
                        <div class="cart-page-heading mb-30">
                            <h5>Book An Expert Doctor</h5>
                        </div> 
                        <form id="redirectForm" method="post" action="{{ action('StoreFront\StoreFrontController@confirmAndCheckout') }}">
                            {{ csrf_field() }} 

                            <div class="form-group">
                                <label for="profession">Select Medication Necessary:</label>
                                <select name="category" class="form-control" id="profession" aria-describedby="profession">
                                     <option>Select specialty type</option>
                                    @foreach($professions as $item)
                                        <option>{{ $item->profession }}</option>
                                    @endforeach
                                </select>
                                <small id="profession" class="form-text text-muted">Select the type of speciality based on your requirement</small>
                              </div>
                               <div class="form-group">
                                <label for="professional">Select Doctor/Nurse/Physiotherapist:</label>
                                <select name="professional" class="form-control" id="professional" aria-describedby="professional">
                                    @foreach($professionals as $item)
                                        <option value='{{ $item->id}}'>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                               
                              </div>

                              <div class="form-row">
                                <div class="form-group col-md-4"> 
                                <label for="orderAmount">Online Consulation Fee</label>
                                <input type="number" readonly class="form-control" id="orderAmount" name='orderAmount' value='1.00'>
                              </div>
                          </div>

                            <div class="form-group">
                                <label for="customerName">Patient Fullname:</label>
                                <input type="text" class="form-control" id="customerName" name="customerName" >
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6"> 
                                    <label for="customerEmail">Email</label>
                                    <input type="text" class="form-control" id="customerEmail" name="customerEmail" >
                                </div>
                                 <div class="form-group col-md-6"> 
                                    <label for="customerPhone">Contact Phone No.:</label>
                                    <input type="text" class="form-control" id="customerPhone" name="customerPhone" >
                                </div>
                            </div>

                             <div class="form-group">
                                <label for="orderNote">Specify reason for consulation:</label>
                                <textarea type="text" rows='5' class="form-control" id="orderNote" name="orderNote" ></textarea>
                            </div>


                            <button type="submit" class="btn btn-primary">Submit</button>
                            <input type="hidden" name="orderId" value="{{ time()  }}"/> 
                            <input type="hidden" name="orderCurrency" value="INR"/>   


                          </form>
                    </div>
 </div>
                  </div>
                    
                </div>
                <div class="col-12 col-md-6 col-lg-5 ml-lg-auto mb-2">

               

   </div>

 


  </div>
</div>
</div>
</div>

 
@endsection 
 
@section('script')

 
@endsection 