@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;

    $total_pay = 0;
    $taxes = 2;
    $tax = $cgst = $sgst = 0;
   ?>
 
<div class='outer-top-tm'> 
<div class="container">
 
  <div class="row">

  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif



   <div class="col-md-8"> 
 
     <div class="card panel-default">
           <div class="card-header">
              <div class="card-title"> 
         <div class="title" style="font-size: 1rem;">Book # <span class='badge rounded-pill text-bg-warning'>{{ $order->id}}</span> 
          Order Status:
          @switch( $order->book_status)

          @case("new")
          <span class='badge rounded-pill text-bg-primary'>New</span>
          @break

          @case("confirmed")
          <span class='badge text-bg-info rounded-pill'>Confirmed</span>
          @break

          @case("in_route")
          <span class='badge text-bg-success rounded-pill'>In Route</span>
          @break

          @case("completed")
          <span class='badge text-bg-success rounded-pill'>Completed</span>
          @break 
          @case("cancelled")
          <span class='badge text-bg-danger rounded-pill'>Order Cancelled</span>
          @break

          @endswitch

          <span class="" style="font-weight: bold; font-size:1rem">Payment:</span> 
       @if(  strcasecmp($order->payment_type,"ONLINE") == 0   ) 
            <span class='badge text-bg-success rounded-pill'>ONLINE</span>
            @else
            <span class='badge text-bg-info rounded-pill'>CASH</span>
            @endif

            @if(   strcasecmp($order->payment_status,"pending") == 0   )  
            <span class='badge text-bg-warning rounded-pill'>PENDING</span>
            @else
            <span class='badge text-bg-success rounded-pill'>PAID</span>
            @endif

          <div class="float-end">
            @if( strcasecmp($order->book_status, "cancelled") != 0)
              <button type="button" id="confirmcancel" data-key="{{ $order->id }}" data-target="wgtconfirmcancel" class="btn btn-danger btn-xss">Cancel Order</button>
            @endif
          </div> 
        <hr/> 
          Booking Date:
          <span class='badge text-bg-success rounded-pill' >{{ date('d-m-Y', strtotime( $order->book_date )) }}</span>


          <div class="float-end">
          Service Date:
          <span class='badge text-bg-info rounded-pill' >{{ date('d-m-Y', strtotime( $order->service_date )) }}</span>

          @if( $order->preferred_time != null )
          Time:
          <span class='badge text-bg-info rounded-pill' >{{ date('h:i A', strtotime( $order->preferred_time )) }}</span>
          @endif
          </div>
        </div>

      </div> 
 </div>
       <div class="card-body">  

         <?php
         $image_url = $cdn_url.'/assets/image/no-image.jpg';
         ?>

                     <div class="row">
                        <div class="col-md-12">
                        <table class="table">
                         <thead  >
                            <th></th> 
                            <th>Package</th> 
                            <th>Details</th>
                            <th>Amount</th>  
                        </thead>
                        @foreach($booking_details as $items)
                        <tbody>
                          <tr>
                          <td><img src="{{$items->photos}}" class="img-fluid" width="100"></td>
                          <td>{{$items->service_name}}</td> 
                          <td>{{$items->serviceDetails}}</td>
                          <td>₹ {{$items->service_charge}}</td>
                          </tr> 
                          <?php 
                              $total_pay += $items->service_charge - $order->discount; 
                          ?>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            
                            <td><b>GST:</b></td>
                            <td>₹ {{number_format($order->gst, 2, ".", "")}}</td>
                          </tr>


                        <tr>
                            <td></td>
                            <td></td>
                            
                            <td><b>Total Amount:</b></td>
                            <td>₹ {{number_format( $order->total_cost + $order->gst, 2, ".", "")}}</td>
                          </tr>
                        
                          <tr>
                            <th colspan='2'>Remarks:</th> 
                            <td colspan='2' class='text-right'>
                              </td>
                </tr>

                <tr>
                  <td colspan='4'>{{$order->cust_remarks}} <button class="btn btn-link btn-rounded text-right btn_remarks" data-key="{{$order->id}}">
                  Add/Edit Additional Remarks
                  </button></td> 
                </tr>
                        </tbody>
 

                        </table>  
                         
                      </div>
 
                      
                     </div>
                      
              </div>
            </div> 

 </div>


 <div class="col-md-4">   
  
   @if( $ordertype=="booking" || $ordertype=="appointment" || $ordertype=="normal" || $ordertype=="offers")

<!-- section for side view -->
            <div class="card">
              <div class="card-header">
                <h5  >Shop/Business</h5><hr>
                <h4 class="card-title">{{$business->name}}</h4>
                 <p>{{$business->locality}}<br/>
                 {{$business->landmark}}, {{$business->city}}<br/>
                <i class='fa fa-phone'></i> {{$business->phone_display}}</p> 

                </div> 
            </div> 

             <div class="card panel-default mt-2">
              <div class="card-header">
                <h5  >Customer</h5><hr>
     <h4 class="card-title">{{$order->customer_name}}</h4>
                <p>{{$order->address}}<br/> 
                  <i class='fa fa-phone'></i> {{$order->customer_phone}}</p> 
               </div>  
            </div>
  

              <div class="card mt-2 mb-2">
                <div class="card-header">
                <h4>Order Rating</h4>
              </div>
             <form method="post" action="{{action('StoreFront\StoreFrontController@updateOrderReview')}}"> 
              {{csrf_field()}}
              <div class="row card-body">
               @foreach($ratingType as $items) 
                    
                    

                    <div class="col-md-12"> 
                    <h7><small><b>{{$items->rating_type}}</b></small></h7> 
                  
                        <?php 
                        if($items->rating>0) {
                           switch($items->rating) {
                            case '1':
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break; 
                          case '2':
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break;
                          case '3':
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break;
                          case '4':
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break;
                          default :
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break;
                        ?>
 

                        <?php  
                          }
                        ?>  
             
                     <?php }else {?>

                    

                    <div class="star-area">
                    <h4>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o"  aria-hidden="true" 
                        data-key="{{$items->id}}" data-number="1"></i>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                    </h4>
                   </div>

                  <?php }?> 
                     
                    @if($items->rating>0)
                    <label>{{$items->review}}</label> 
                    @else
                      <input type="text" class="form-control" name="review[]" placeholder="<?php echo strtolower($items->rating_type)." ".'review';?>" value="{{$items->review}}" @if($items->status=="verified")disabled @endif>
                    @endif

                    <input type="hidden" name="ratingType[]" value="{{$items->rating_type}}" @if($items->status=="verified")disabled @endif>
                    @if($items->rating>0)
                    <input type="hidden" name="rating[]" class="ratingno{{$items->id}}" value="{{$items->rating}}" @if($items->status=="verified")disabled @endif>
                    @else
                      <input type="hidden" name="rating[]" class="ratingno{{$items->id}}" value="1" @if($items->status=="verified")disabled @endif>
                    @endif
                    <input type="hidden" name="orderno" class="orderno" value="{{request()->get('key')}}"@if($items->status=="verified")disabled @endif>
                    <hr>
                    </div>
                    
                    
              @endforeach

              @if($items->rating>0)
              
              @else
              <div class="col-md-12 mt-2">
               <button type="submit" class="btn btn-primary btn-block">Submit</button>
              </div>
              @endif
              
              
            </div>
            </form>
              </div>

              @else

              <div class="card panel-default">
              <div class="card-header">
                <h5 class=" ">Pick up from</h5>
                <h4 class="card-title"></h4>
                </div>
              <div class="card-body">

                <div class="timeline timeline-xs">
                  <div class="timeline timeline-xs"> 
                    <div class="timeline-item mb-2">
                      <div class="timeline-item-marker">
                        
                        <div class="timeline-item-marker-indicator bg-red"></div>
                      </div>
                      <div class="timeline-item-content">
                        {{$order->pickup_name}} <br>
                        {{$order->pickup_address}}<br>
                         
                        <i class="fa fa-phone"></i> {{$order->pickup_phone}}
                      </div>
                    </div> 
                  </div>
                </div> 
              </div> 
              
            </div>

            <div class="card mt-2">
                <div class="card-header">
                <h5>Drop at</h5>
              </div>
              <div class="card-body">
                <div class="timeline-item">
                      <div class="timeline-item-marker"> 
                        <div class="timeline-item-marker-indicator bg-green"></div>
                      </div>
                      <div class="timeline-item-content">
                        {{$order->drop_name}}<br>
                        {{$order->drop_address}}<br> 
                        <i class="fa fa-phone"></i> {{$order->drop_phone}}
                      </div>
                    </div>

              </div>
              </div> 
              @endif
           
          </div>  

       </div>  
 </div>  
 </div> 
 

 
<div class="modal fade" id="widget{{$order->id}}" tabindex="-1" aria-labelledby="addremark" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <form method="post" action="{{action('StoreFront\StoreFrontController@updateOrderRemarks')}}">
        {{csrf_field()}}
      <div class="modal-header">
        <h5 class="modal-title" id="remarks">Customer Remarks</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="card">
        <input type="hidden" name="key" id="widgetkey">
        <input type="text" name="customer_remarks" class="form-control" placeholder="Add your remarks">
       </div>
      </div>
      <div class="modal-footer"> 
          <button type="submit" class="btn btn-primary">Save</button>
         <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </form>
    </div>
  </div>
</div>



<div class="modal fade" id="assistwidget{{$order->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <form method="post" action="{{action('StoreFront\StoreFrontController@updateAssistOrderRemarks')}}">
        {{csrf_field()}}
      <div class="modal-header">
        <h5 class="modal-title" id="remarks">Add your order feedback</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="card">
        <input type="hidden" name="assistkey" id="assistwidgetkey">
        <textarea name="customer_remarks" placeholder="Add your feedback" class="form-control"></textarea>
       </div>
      </div>
      <div class="modal-footer"> 
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>


<div class="modal fade" id="wgtconfirmcancel" tabindex="-1" aria-labelledby="cancelorder" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <form method="post" action="{{action('Booking\BookingController@cancelBookingOrder')}}">
        {{csrf_field()}}
        <input type="hidden" name="key" id="wgtconfirmcancelkey">
      <div class="modal-header">
        <h5 class="modal-title" id="remarks">Cancel Booking Order</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
         <p>You are about to cancel your prebooked order. Though the cancellation process is fast and your order amount refund may depend on our terms of use and refund policy.</p>
         <p>Please read the refund policy and guides before proceeding. You can talk with one of our customer support staff prior to your cancellation process in case you want to clarify any doubt.<br/>
          Read cancellation policy <a href="{{ URL::to('/terms/cancellation-and-refund') }}" target="_blank">here</a>

        </p>. 
      </div>
      <div class="modal-footer">
          <button type="submit" name="save" value="cancel" class="btn btn-danger rounded-pill">Confirm Cancellation</button>
         <button type="button" class="btn btn-secondary rounded-pill" data-bs-dismiss="modal">I've Change My Mind</button>
      </div>
    </form>
    </div>
  </div>
</div>


@endsection


@section('script')
 

<script> 

document.querySelectorAll('.star_review').forEach(function(element) {
          element.addEventListener("mouseenter", function(event) {
            var rid = $(this).attr('data-key');
             
            var stars = event.target.dataset.number; 
            var count = 0;
            document.querySelectorAll('.ratings'+rid).forEach(function(el) {
              if (count < stars) {
                $(el).removeClass('fa-star-o');
                $(el).addClass('fa-star');
                $('.ratingno'+rid).val(stars);
               
              } else {
                $(el).removeClass('fa-star');
                $(el).addClass('fa-star-o');
              }
              count = count+1; 
            });
          });
          element.addEventListener("click", function(event) {
            console.log('clicked')
             $(event).removeClass('fa-star');
             $(event).addClass('fa-star-o');
          });
          element.addEventListener("mouseleave", function(event) {
            document.querySelectorAll('.star_review').forEach(function(el) {
              
            });
          });
        });


$(document).on("click", ".btn_remarks", function()
{
    var widget = $(this).attr('data-key'); 
    $("#widgetkey").val(widget); 
    $("#widget"+widget).modal("show");
});

$(document).on("click", "#confirmcancel", function()
{
  var widget = $(this).attr('data-target');
  var key = $(this).attr('data-key');
  $("#" +  widget + "key").val(key);
  $("#" + widget ).modal("show");
});


$(document).on("click", ".btnassistremarks", function()
{
   
    var widget = $(this).attr('data-key'); 
    $("#assistwidgetkey").val(widget); 
    $("#assistwidget"+widget).modal("show"); 

});
</script>


@endsection

   