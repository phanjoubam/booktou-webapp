@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  

?>

<div class='outer-top-tm'> 
    <div class="container">  
    <div class='row'> 
  
  <div class="col-12 col-md-6 offset-md-3  mb-2">
        <div class="card">
            <div class="card-body">
                    <div class="checkout_details_area mt-50 clearfix"> 
                        <div class="cart-page-heading mb-30">
                            <h5>Book An Expert Doctor</h5>
                        </div> 
                        <form id="redirectForm" method="post" action="https://www.cashfree.com/checkout/post/submit">
                           
                             

                            <div class="form-group">
                                <label for="profession">Selected Professional Category:</label>
                                <input type="text" readonly  class="form-control" id="category"   value='{{ $category }}'> 
                              </div>
                               <div class="form-group">
                                <label for="professional">Select Doctor/Nurse/Physiotherapist:</label>
                                <input type="text" readonly  class="form-control" id="professional"   value='{{ $professional }}'> 
                               
                              </div>

                              <div class="form-row">
                                <div class="form-group col-md-4"> 
                                <label for="orderAmount">Online Consulation Fee</label>
                                <input type="number" readonly class="form-control" id="orderAmount" name='orderAmount' value='{{ $orderAmount }}'>
                              </div>
                          </div>

                            <div class="form-group">
                                <label for="customerName">Patient Fullname:</label> 
                                 <input type="text" readonly class="form-control" id="customerName" name='customerName' value='{{ $customerName }}'>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6"> 
                                    <label for="customerEmail">Email</label>
                 <input type="text" readonly class="form-control" id="customerEmail" name='customerEmail' value='{{ $customerEmail }}'>
 
                                </div>
                                 <div class="form-group col-md-6"> 
                                    <label for="customerPhone">Contact Phone No.:</label>
                     <input type="text" readonly class="form-control" id="customerPhone" name='customerPhone' value='{{ $customerPhone }}'> 
                                </div>
                            </div>

                             <div class="form-group">
                                <label for="orderNote">Specify reason for consulation:</label>
                                <textarea type="text" readonly rows='5' class="form-control" id="orderNote" name="orderNote" >{{ $orderNote }}</textarea>
                            </div>


                              <button type="submit" class="btn btn-primary">Submit</button>
                              <input type="hidden" name="appId" value="92213d92ddd6010b563914a5031229"/>
                              <input type="hidden" name="orderId" value="{{ $orderId  }}"/> 
                              <input type="hidden" name="orderCurrency" value="INR"/> 
                              <input type="hidden" name="returnUrl" value="https://booktou.in/appointment/checkout-completed"/> 
                              <input type="hidden" name="notifyUrl" value="https://booktou.in/appointment/notify-checkout-status"/>  
                              <input type="hidden" name="signature" value="{{ $signature }}"/>


                     
   

                          </form>
                    </div>
 </div>
                  </div>
                    
                </div>
                <div class="col-12 col-md-6 col-lg-5 ml-lg-auto mb-2">

               

   </div>

 


  </div>
</div>
</div>
</div>

 
@endsection 
 
@section('script')

 
@endsection 