<div class="card-header how-it-work">
	<!-- <h7 class="text-uppercase">Select to book</h7> -->
	<div class="panel panel-default col-sm-12 col-sm-offset-2">
				  <div class="panel-body">
				  	<span>2</span>  <h3 class="step-heading text-uppercase">Staff Selection</h3>
				    Select Staff to continue booking process!
				  </div>
				</div> 
</div>
<div class="card-body row"> 
@foreach($results as $items)

<div class="col-md-3 col-3 col-sm-4 col-xs-4 mb-2 text-center">
	<div class="card"> 
		@if($items->staffId==0)
			<?php $items->staffName="Any Staff"; ?>
		@endif

	   @if($items->available=="yes") 

	   <button class="btn btn-light showcheckout" id="btnShowCheckOut{{$items->staffId}}"  
	   	data-ukey="{{$items->staffId}}" 
	   	data-time="{{$time}}" 
	   	data-endtime="{{$endtime}}"
	   	data-selected-date="{{$selectedDate}}"
	   	data-slots-number="">
	   	<img src="{{$items->profilePicture}}" class="rounded-circle" style="height:100px;width: 100px;" />
	   	<label>{{$items->staffName}}</label>
	   </button>

	   @else
	    
	   <img src="{{$items->profilePicture}}"  class="disabled rounded-circle" data-ukey="" data-time="{{$time}}" style="height:100px;width: 100px;"/>
	   <label>{{$items->staffName}}</label>
	   
	   @endif
	   
	</div>
</div>

@endforeach
</div>


