@extends('layouts.booking_theme_01')
@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ;
?>

<section class='inpage-subhead'>
    <div class='container'>  
        <div class="row mt-2">
            <div class='col-md-8 p-5'>
                 <h1 class='display-4 text-white'>{{ ucwords( implode(" ", explode("-", $categoryname) )) }}</h1>
                 <p >
                     Browse packages, buy offers or start booking your preferred package.
                 </p>
            </div>
            <div class='col-md-4 d-none d-md-block'>
                 <div 
                 style="background-image: url('{{ URL::to("/public/assets/image/gift-box.png" )  }}'); background-size: cover; 
                 height: 150px; width: 150px;margin: 30px auto;" ></div>
            </div>
        </div>
    </div>
</section>

<div class='breadcrumb-box'>  
    <div class='container'>  
        <div class="row mt-2"> 
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/booking')}}" style="color:green;">
                                   <b> Booking</b>
                                </a>
                            </li>
                            @if(isset($categoryname))
                            <li class="breadcrumb-item active text-uppercase" aria-current="page">
                                <a class="text-weight-bold" href="{{URL::to('/booking/service-details')}}/{{$categoryname}}" style="color:green;">
                                   <b> {{$categoryname}} </b>

                                 </a>
                            </li>
                            @endif 
                        </ol>
              </nav>
          </div>
          <div class="col-md-6 col-sm-12">

          </div>
      </div>
  </div>
  <div class=" col-md-3 d-none d-sm-block">   
</div>
</div>
</div>
 </div>
 
 <div class='container'>  
     <div class="row"> 
       

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >  
        <div class="row">

            @foreach($services as $item) 

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> 
                <div class="card mb-3  rca-2">  
                    <div class='card-body'>
                        <div class="row align-items-start"> 
                            <div class="col-md-12"> 
                                 @foreach($businesses as $business)
                                @if( $item->bin  == $business->id )
                                <p><span class='h5'>{{$business->name}}</span></p>
                                @break
                                @endif 
                                @endforeach
                                <hr/> 
                            </div>  

                            <div class="col-md-5 float-sm-start float-md-end float-lg-end">
                            @if( $item->photos != "") 
                                @php
                                    $imageurl = $item->photos;
                                @endphp
                                <img src="{{ $imageurl }}" class="img-fluid rounded">

                            @else
                                
                                @if( count($service_profiles) > 0)
                                    @php 
                                        $active_slider ="active";
                                    @endphp
                                    <div id="service_slider{{$item->id}}" class="carousel slide" data-bs-ride="true" >
                                      <div class="carousel-inner">
                                        @foreach($service_profiles as $service_profile)
                                            @if(  $service_profile->service_product_id  == $item->id  )
                                                <div class="carousel-item {{ $active_slider }}">
                                                  <img src="{{ $service_profile->photos }}" class="d-block w-100" alt="...">
                                                </div>
                                                @php 
                                                    $active_slider ="";
                                                @endphp
                                            @endif 
                                        @endforeach  
                                    </div>
                                  
                                    <button class="carousel-control-prev" type="button" data-bs-target="#service_slider{{$item->id}}" data-bs-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Previous</span>
                                        </button>
                                    <button class="carousel-control-next" type="button" data-bs-target="#service_slider{{$item->id}}" data-bs-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Next</span>
                                    </button>
                                    </div> 
                                @endif 
                            @endif
                </div>

                <div class="col-md-7 float-sm-end float-md-start float-lg-start">
                            <div class='box-content-sm'> <p><span class='h5'>{{$item->srv_name}}</span></p> 
                                <p>
                                @php 
                                    $len = strlen ( $item->srv_details ); 
                                    if($len > 250) 
                                    {
                                         
                                        echo substr($item->srv_details, 0, 250 ) . ' [+]';
                                    }
                                    else
                                    {
                                          echo $item->srv_details;
                                    } 
                                @endphp  
                             </p>
                         </div> 
                        </div>

            </div>
             <div class="row align-items-start"> 
                <div class="col-md-8"> 
                        <div class="price mt-1 text-right">
                            <span class="h3">Starts from <span class='orange'>{{$item->actual_price}}₹</span></span>
                        </div>
                </div>
                <div class="col-md-4 text-right"> 
                    <a  href="{{URL::to('booking/business/prepare-booking')}}/{{ $item->bin }}/{{$item->id}}" class="btn btn-primary btn-rounded">
                    Book Now</a>
                </div>
            </div>


 
                    <div class="text-center">
                        
                    </div>
                </div></div>
            </div>


            @endforeach
        </div> 
 <!-- filtered products -->

</div>
<!--  -->
</div>
</div> 
@endsection
