<!-- <div class="card-header how-it-work"> -->
	<!-- <h7 class="text-uppercase"></h7> -->
			   <!--  <div class="panel panel-default col-sm-12 col-sm-offset-2">
				  <div class="panel-body">
				  	<span>1</span>  <h3 class="step-heading text-uppercase">  Time Slots </h3> -->
				  <!-- <h5>Select Time Slots</h5> -->
				 <!--  </div>
				</div>  -->

<!-- </div> --> 
<h5 class="p-2">Select Time Slots</h5>
	<br>
<div class="row">



 			@foreach($results as $slots)
				<div class="col-md-3 col-3 col-sm-6 col-xs-6 mb-2 text-center">
		        @if($selectedDate == date('Y-m-d'))
				@if( date('H:i') > date('H:i', strtotime($slots->endTime)) )
			        <button type="button" class="btn btn-secondary showStaffs btn-sm" disabled data-toggle="tooltip" 
			        data-placement="bottom" title="" 
			        data-selected-date = "{{$selectedDate}}" 
			        data-bin="{{$slots->bin}}"
			        data-time="{{$slots->startTime}}"
			        data-endtime="{{$slots->endTime}}"
			        data-slots="{{$slots->slotNo}}">
					 <small> 
					 <b> {{date('h:i A', strtotime($slots->startTime))}} 
					   -{{date('h:i A', strtotime($slots->endTime))}}
					 </b> 
					 </small>
					</button>
				@else
					<button type="button" class="btn btn-secondary showStaffs btn-sm"  data-toggle="tooltip" 
			        data-placement="bottom" title="" 
			        data-selected-date = "{{$selectedDate}}" 
			        data-bin="{{$slots->bin}}"
			        data-time="{{$slots->startTime}}"
			        data-endtime="{{$slots->endTime}}"
			        data-slots="{{$slots->slotNo}}">
					 <small>    
					 	<b>{{date('h:i A', strtotime($slots->startTime))}} 
					   		-{{date('h:i A', strtotime($slots->endTime))}}
					   	</b> 
					</small>
					</button>

				@endif
				@else
					<button type="button" class="btn btn-secondary showStaffs btn-sm"  data-toggle="tooltip" 
			        data-placement="bottom" title="" 
			        data-selected-date = "{{$selectedDate}}" 
			        data-bin="{{$slots->bin}}"
			        data-time="{{$slots->startTime}}"
			        data-endtime="{{$slots->endTime}}"
			        data-slots="{{$slots->slotNo}}">
					  <small> 
					  <b>
					  	{{date('h:i A', strtotime($slots->startTime))}}
					  	-{{date('h:i A', strtotime($slots->endTime))}}
					  </b>
					  </small> 
					</button> 
				@endif

				</div>
			@endforeach  
</div>
<hr>
<div class="card-title"><h5>Staff List</h5></div>
<div class="row mt-2 p-2" style="border-top:1px solid #efefef;">

@foreach($staffs as $items)

<div class="col-md-3 col-3 col-sm-4 col-xs-4 mb-2 text-center">
	<div> 
		@if($items->staffId==0)
			<?php $items->staffName="Any Staff"; ?>
		@endif 

	   <button class="btn btn-info showcheckout" type="button" disabled id="btnShowCheckOut{{$items->staffId}}"  
	   	data-ukey="{{$items->staffId}}" 
	   	data-selected-date="{{$selectedDate}}"
	   	data-slots-number="">
	   	<img src="{{$items->profilePicture}}"  class=" disabled rounded-circle" style="" />
	   	<label><small>{{$items->staffName}}</small></label>
	   </button> 

	</div>
</div>

@endforeach
</div>


