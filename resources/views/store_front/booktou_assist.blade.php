@extends('layouts.store_front_02')
@section('content')
<style>
   .text
   {
    margin-left: 40PX;
   }
    .btnchatbotlogins {
  color: #fff;
  background-color: #37b516;
  border-color: #37b516;
  width: 300px;
  height: 37px;
}

.btnchatbotlogins {
  color: #fff;
  background-color: #37b516;
  border-color: #37b516;
  width: 100px;
  height: 37px;
}
</style>
<?php 
    $cdn_url =  config('app.app_cdn');  
    $cdn_path = config('app.app_cdn_path');
?> 

<div class='outer-top-tbm'> 
<div class="container">
<div class="row">

   <div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 " >
    <div class="card " style=" box-shadow: 2px 2px 2px 0 rgba(2, 0, 0, 0.2), 0 2px 2px 0 rgba(2, 0, 0, 0.19); "> 
        <div >
           
            
          
              <div class="row chat-mail chat-box overflow-auto ">                                
                <div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 mb-3 text-center">
                                    <?php  
                       if (session()->get('__user_id_')!="") { 
                    ?>
                    @include('store_front.assist.chat_assist')
                    
                    <?php  
                        }else{
                    ?>
                    
                    @foreach($bot as $items)
                        <ul class="col-12 mt-3">
                            <li class="list-group-item font-weight-bold"><a class="btnchat" data-chatquestion="{{$items->question}}" href="javascript::void();">{{$items->question}}</a></li>
                        </ul>
                    @endforeach
                    
                    <?php }?>
                     <div class="  chat-body mt-2 showchatresult block" >
                    <div class="scrollChat">

                    </div>
                    </div>
                </div> 
            </div>
       
        </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- chat bot ends here -->
<!-- section for booktou assists --> 
 
   <!-- login successfull! -->
    @auth
    <div class="modal fade widgetassist"   data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center col" id="staticBackdropLabel">How may i assist you?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      
      <p class="col-sm-12  col-md-12 col-lg-12 text-center" >
                <small >Pick up, purchase or send an item from anywhere inside the Imphal Town</small>
      </p>

      <div class="modal-body">
        <div class="row">
            @if(isset($assists))
            @foreach($assists as $items)
            <div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 text-center p-1">
                <input type="hidden" value="{{$items->assist_cat_name}}" name="task_{{$items->id}}" required>
                <a href="{{URL::to('shopping/assist-pickup/'.$items->id)}}">
                  
                    <div class="card">
                       
                      <img src="{{$items->icon_url}}" class="img-fluid rounded-circle  p-2">
                      
                      <h7 class="card-title"><small>{{$items->assist_cat_name}}</small></h7>
                    </div>
                  
                </a>
            </div> 
            @endforeach
            @endif
              
        </div>
      </div>
      <div class="modal-footer">
        <p class="text-center" style="color:red;"><small>We do not deliver cigarettes, alcohols, drugs, guns and other illegal items which are prohibited by the law.
         (Fragile/value items is not recommended)</small>
        </p>
      </div>
    </div>
   </div>
   </div> 

   <!-- new wizard template for booktou assists -->
   <div class="modal fade booktouwidgetassist"   data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">  
    <div class="modal-content p-5 m-3"> 
     <div class="modal-header">
        <h4 class="modal-title text-center col" id="staticBackdropLabel"><img src="{{asset('/public/store/image/favicon-96x96.png')}}"> How may i assist you?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      
      <p class="col-sm-12  col-md-12 col-lg-12 text-center mb-3">
                <small>Pick up, purchase or send an item from anywhere inside the Imphal Town</small>
      </p>

    <p class="sys-alert"></p>
    <form method="post" action="{{action('StoreFront\StoreFrontController@assistCompleteYourRequest')}}" enctype="multipart/form-data">
        {{csrf_field()}}


    <div class="modal-body wizardsteps">
        
        <div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1 mt-3">
                    @if(isset($assists))
                        <select class="form-control" name="assistcategory" required id="assistcategory">
                            <option value="">Select assist category</option>
                            @foreach($assists as $items)
                            <option value="{{$items->id}}">{{$items->assist_cat_name}}</option>
                            @endforeach
                        </select>
                    @endif 
                    <div class="col-md-10 offset-md-1 mt-5">
                        <button type="button" class="btn btn-warning btn-block btn-assist-proceed">Proceed</button>
                    </div>
        </div>
        
    </div>
    </form>
    <div class="modal-footer">
       
    </div>
    <div class="col-md-6 offset-md-3">
        <div class="offset-md-4"> 
            <img src="{{asset('/public/store/image/stop.png')}}"> 
            <img src="{{asset('/public/store/image/smoking.png')}}"> 
            <img src="{{asset('/public/store/image/drinks.png')}}">
        </div>
        <p class="text-center" style="color:red;"><small>We do not deliver cigarettes, alcohols, drugs, guns and other illegal items which are prohibited by the law.
         (Fragile/value items is not recommended)</small>
        </p>
    </div>
    </div>
    </div>
    </div>

   <!-- booktou assist wizard ends here -->
    @endauth
    <!-- successfull login ends here -->
    
    @guest
    


    <!-- spinner --> 
    <!-- <div id="cover-spin"></div>-->

<!-- login dialogue box -->
<div class="modal fade assitswidgetModal" id="assitswidgetModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered ">
     <form action="{{ action('Auth\LoginController@customerLogin') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" id="srvukey" name="refererlogin" value="<?php echo $nos = bin2hex(random_bytes(24));?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Customer Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
         <div class="col-12 mb-3">
                                    <label for="phone">Phone Number or Email: <span>*</span></label>
                                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email' required>
                                </div>
                               
                               
                                <div class="col-12 mb-4">
                                    <label for="email_address">Password: <span>*</span></label>
                                    <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password' required>
                                </div>
                            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-block" value='login' name='login'>Login</button>
        
        
      </div>
    </div>
    </form>
  </div>
</div> 
<!-- login dialog box ends here -->

@endguest  

<!-- booktou assists ends here -->
@endsection 
 @section('content')

<style type='text/css'>
 /*search box css start here*/
.search-sec{
    padding: 2rem;
}
.search-slt{
    display: block;
    width: 100%;
    font-size: 0.875rem;
    line-height: 1.5;
    color: #55595c;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
.wrn-btn{
    width: 100%;
    font-size: 16px;
    font-weight: 400;
    text-transform: capitalize;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
@media (min-width: 992px){
    .search-sec{
        position: relative;
        top: -114px;
        background: rgba(26, 70, 104, 0.51);
    }
}

@media (max-width: 992px){
    .search-sec{
        background: #1A4668;
    }
}

.modal-dialog1 {
    max-width: 90% !important;
    margin: 1.75rem auto;
}
.modal-content1 {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    width: 100% !important;
    height: 90% !important;
    pointer-events: auto;
    background-color: #fff !important;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,0.2);
    border-radius: .55rem;
    outline: 0;
}

/*hover effect*/

</style>

 <script>


$(document).ready(function() {
 
   

   $('.product_thumbnail_slides').owlCarousel({
            items: 1,
            margin: 0,
            loop: true,
            nav: true,
            navText: ["<img src='img/core-img/long-arrow-left.svg' alt=''>", "<img src='img/core-img/long-arrow-right.svg' alt=''>"],
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 1000
        });
 
     
});


 
        
      

</script>


@endsection