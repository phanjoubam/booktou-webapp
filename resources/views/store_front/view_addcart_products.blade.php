 @if( isset($product))
	<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
	@foreach($product as $product)

 <div class="card mb-3" >
  <div class="row no-gutters">
    <div class="col-md-4">
       
				 
				<?php   
 

                $all_photos = array(); 
                $files =  explode(",",  $product->photos ); 

         		if(count($files) > 0 )
                {
                    $files=array_filter($files);
                    $folder_path =  $cdn_path .  "/assets/image/store/bin_" .   $product->bin .  "/";

                    foreach($files as $file )
                    {

                        $source =  $folder_path .   $file; 
                        if(file_exists( $source  ))
                        {

                            $pathinfo = pathinfo( $source  );
                            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 

                            $destination  = $cdn_path  .    $new_file_name ;
                            if( file_exists( $destination ) )  
                            {
                                $all_photos[]  = $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name;
                            }
                            else 
                            {
                                if(filesize(  $source ) > 307200)  
                                { 
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" . $product->bin  .  "/"  . $new_file_name ;  
                                }
                                else 
                                {
                                    $all_photos[]  =   $cdn_url .  "/assets/image/store/bin_" .   $product->bin   . "/" .    $file  ;
                                }
                            } 
                        }
                        else
                        {
                            $all_photos[]   =  $cdn_url .  "/assets/image/no-image.jpg";
                        }
                    }
                    
                    if( count($all_photos ) > 0 )
                    {
                        $image_url =  $all_photos[0];
                    } 
                    else 
                    {
                        $image_url = $cdn_url .  "/assets/image/no-image.jpg";
                    }
                    
                }
                else
                {
                    $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
                }
 

                $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name) ) ); 

         ?>

	<img src="{{ $image_url }}"> 


    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title">{{$product->pr_name}}</h5>
        <p class="card-text">
        	@if($product->unit_price == $product->actual_price)
             ₹ {{$product->actual_price }} 
                  @else     
                         ₹ {{$product->actual_price }} <span class="old-price">₹ {{$product->unit_price}}
                        </span> 
                      @endif 
                  </p>

        <p class="card-text"> {{$product->description}}</p>
        <p class="card-text">  
        	<div class="input-group  col-8">
              <div class="input-group-prepend">
                <button type="button" class="input-group-text btnplusminus" data-step="-1">-</button> 
              </div>
              <input type="number" class="form-control qty" aria-label="Quantity" step="1" min="1" max="" name="quantity" id="tbqty" value="1" title="Qty">
              <div class="input-group-append">
               <button type="button" class="input-group-text btnplusminus" data-step="+1">+</button>  
              </div>
        </div></p>





      </div>
    </div>
    
  </div>
  <div class="modal-footer">
        
      
    <input type='hidden' name='key' id='w1-key'/> 
        <button type="button" data-action="add" data-qty="1" data-bin="{{  $product->bin }}" data-prid="{{  $product->id }}" data-subid="{{  $product->id }}" 
                class="btn btn-primary btn-add-item"> Cart </button>    
        <button type="button"   class="btn btn-danger gocart">Go to Cart</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Continue Shopping</button>
  </div>
</div>

@endforeach

@endif 