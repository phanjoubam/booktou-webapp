@extends('layouts.store_front_02')
@section('content')
<?php 
$cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
    $total_cost = $delivery_cost = 0.0;
    $normal_delivery = 80; 
    $food_delivery = 50; 
    $grocery_delivery = 100.00;
    $cake_delivery =  100.00; 
    $delivery_charge = 0.00;
    $taxes = 2;
    if( Auth::user() ):

        foreach($configs as $config)
        { 
            switch( $config->config_key )
            {
                case "delivery_charge": 
                $normal_delivery =  $config->config_value  ;
                break;
                case "food_delivery_charge":
                $food_delivery =  $config->config_value  ;
                break;
                case "grocery_delivery_charge":
                $grocery_delivery =  $config->config_value  ;
                break;
                case "cake_delivery_charge":
                $cake_delivery =  $config->config_value  ;
                break;
            }
        } 
    endif;
    ?>
    <section>
      <div class="container">
        <div class="row mt-2">
            @auth
            <div class="col-12">
                <div class="page-title text-center">
                    <h1>Your Shopping Cart Details</h1>
                </div>
            </div>
            <div class="col-md-8 offset-md-2 mt-2">
               <div class='card'> 
                  <div class='card-body'>
                    <ul class="nav nav-pills">
                      <li class="nav-item pr-2">
                        <a class="nav-link active"  href="{{ URL::to('/shopping/my-orders') }}">My Orders</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="nav-link btn-primary"  href="{{ URL::to('/shopping/my-bookings') }}">My Bookings</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="nav-link  btn-danger"  href="{{URL::to('/shopping/show-wishlist-product') }}">Your Wishlist</a>
                    </li>
                </ul>
            </div> 
        </div>
    </div>

    <div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 mt-2"> 
        @if (session('err_msg')) 
        <div class='card'> 
          <div class='card-body'>
            <div class="alert alert-info">
              {{ session('err_msg') }}
          </div>  </div> 
      </div> 
      @endif    
  </div>  
  <div class="col-sm-12 col-md-8 col-lg-8 offset-md-2 offset-lg-2 mt-2"> 
    @if( count($businesses) > 0 )
    @foreach($businesses as $business)
        @if( strcasecmp($business->category,"RESTAURANT")  == 0)
            @php 
            $delivery_charge = $food_delivery;
            @endphp

        @elseif( strcasecmp($business->category,"CAKES & BAKERY") == 0) 
            @php 
            $delivery_charge = $cake_delivery;
            @endphp

        @elseif( strcasecmp($business->category,"VARIETY STORE") == 0)
             @php 
            $delivery_charge = $grocery_delivery;
            @endphp

        @elseif( strcasecmp ($business->category,"GROCERY") == 0)
        @php 
            $delivery_charge = $grocery_delivery;
        @endphp

        @else 
        @php 
            $delivery_charge = $normal_delivery;
         @endphp
    @endif

    <div class='card mt-3'> 
      <div class='card-body'>
        <div class="order-details-confirmation" style='margin-bottom: 20px;'>              
            <div class="cart-page-heading mb-30">
                <p class="display-6">{{ $business->name }}</p>
                <hr/>
            </div>
            @if( isset( $cart  ) )
            @php 
            $total_cost = $total = 0;
            $memid = 0;
            $tax = $gst= $cgst = $sgst = 0;
            $gst_pc = 0;
            @endphp 
            <table class="table table-colored">
                <tr>
                    <th width='90px'></th>
                    <th style='width:250px; text-align:left' >Item</th>
                    <th style='width: 250px'>Quantity</th>  
                    <th class='text-end'>Item + Packing = Sub-Total</th>
                </tr>  
                @foreach($cart as $cartitem)
                @if( $cartitem->bin == $business->id  )
                <?php
                if($cartitem->photos=="") {
                  $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
              }else{
               $image_url = $cartitem->photos;
           }
           ?>
           <tr>
            <td width='90px'><img class='rounded ' width='40px' src="{{$image_url}}/" 
                alt='{{ $cartitem->pr_code }}'/>
            </td>
            <td style='width:250px; text-align:left' >
                <strong>{{ $cartitem->pr_name }}<strong>
                </td>
                <td style='width: 250px'>
                    @if(  $cartitem->stock_inhand == 0  )
                    <button type="button"   class="btn btn-outline-primary btn-sm btn-block" disabled>Out of stock</button> 
                    @else 


                    <div class="input-group">
  <input type="number" class="form-control qty" aria-label="Quantity"  placeholder="Quantity" step="1" min="1" max="" name="quantity" id='tbqty{{ $cartitem->key }}' value="{{ $cartitem->pqty }}" title="Qty" >
  <button class="btn btn-outline-secondary btnupdatecart" type="button" data-prid="{{  $cartitem->prid }}" 
                        data-subid="{{  $cartitem->prid }}"  
                        data-bin="{{  $cartitem->bin }}"   
                        data-step='-1' 
                        data-key="{{ $cartitem->key }}">-</button>
  <button class="btn btn-outline-secondary btnupdatecart" type="button" data-prid="{{  $cartitem->prid }}" 
                     data-subid="{{  $cartitem->prid }}"  
                     data-bin="{{  $cartitem->bin }}"  
                     data-step='+1' data-key="{{ $cartitem->key }}">+</button>
</div>
 
             @endif
         </td>  
         <td class='text-end'>{{ ( $cartitem->actual_price *  $cartitem->pqty ) + 
            ($cartitem->packaging * $cartitem->pqty)  }}</td>
        </tr>  
        @php 
        $total_cost += ( $cartitem->actual_price *  $cartitem->pqty ) + ($cartitem->packaging * $cartitem->pqty) ; 
        $memid  =  $cartitem->member_id; 
        @endphp  
       <?php 
        $gst += ( ( $cartitem->actual_price + $cartitem->packaging)  * $cartitem->pqty *  $cartitem->gst_pc )/100 ;
        $gst_pc = $cartitem->gst_pc;
        ?>  
        @endif                   
        @endforeach  
        <tr>
            <td colspan='2'></td>    
            <td class='text-end'><strong>Item Total Cost:</strong></td>    
            <td class='text-end'>{{ $total_cost  }}</td>
        </tr>    
        <tr>
            <td colspan='2'></td>    
            <td class='text-end'><strong>Delivery Charge:</strong></td>    
            <td class='text-end'>{{ $delivery_charge }}</td>
        </tr>
        <tr>
            <td colspan='2'></td>    
            <td class='text-end'><strong>GST :</strong> &nbsp {{$gst_pc}}%</td>                   
            <td class='text-end'>₹ {{ $gst}}</td>
        </tr> 
        <tr>
            <td colspan='2'></td>    
            <td class='text-end'><strong>Total Order Cost:</strong></td>  
            <td class='text-end'>₹ {{ $total_cost + $delivery_charge + $gst}}</td>
        </tr>

        <td colspan='4' class='text-end'> 
            <form method="post" action="{{ action('StoreFront\StoreFrontController@cartSelectionForOrder') }}"> 
                {{ csrf_field() }}
                <input type='hidden' name='bin' value='{{ $business->id }}' /> 
                <input type='hidden' name='mid' value='{{ $memid }}' /> 
                <button type='button'   data-widget='w1' data-param='{ "w1-key":"{{ $business->id }}" }'  class="btn btn-danger btn-rounded btncancel">Remove</button> 
                @if(  $cartitem->stock_inhand == 0  )
                <button type='button'  class="btn btn-primary btn-rounded" disabled>Place Order</button> 
                @else  
                <button type='submit' value='placeorder' name='btnorder'  class="btn btn-primary btn-rounded">Place Order</button> 
                @endif
            </form>
        </td>
    </tr>
</table>
@endif  
</div>
</div>
</div> 
@endforeach 
@else 
<p class='alert alert-info'>Ooops! Your cart is empty. Please start shopping and enjoy great offers!</p>
@endif  
</div> 
@endauth
@guest
<div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3 mt-4 mb-4">
    <div class="card"> 
       <div class="card-body">
        <div class="cart-page-heading mb-30">
            <h1>Customer Login</h1>
            <hr/>
        </div>
        <form action="{{ action('Auth\LoginController@customerLogin') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-12 mb-3">
                    <label for="phone">Phone Number or Email: <span>*</span></label>
                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email'>
                </div>
                <div class="col-12 mb-4">
                    <label for="email_address">Password: <span>*</span></label>
                    <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password'>
                </div>
                <div class="col-12 mb-4">

                    <button type="submit" value='login' name='login' class="btn btn-primary">Login</button>
                    <br/> 
                    <br/> <br/>
                    <h5>New Member? Please signup here</h5>

                    <a href="{{ URL::to('/join-and-refer') }}" class="btn btn-info">Signup Here</a>

                    <h5>Lost password? Recover here</h5>
                    <a href="{{ URL::to('/forgot-password') }}" class="btn btn-danger">Forgot Password?</a> 
                </div>
            </div>
        </form>
    </div>
</div>
</div>
@endguest
</div>
</div> 
</section>

<form method="post" action="{{ action('StoreFront\StoreFrontController@removeCart') }}"> 
    {{ csrf_field() }}
    <div class="modal wg-w1" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Confirm Shopping Cart Removal</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">

        <img src="{{ URL::to('/public/store/image/empty-cart.png') }}" width='120px' alt='Empty Now!' class="  float-left" />
        <p class='p-3' style='padding-left: 30px'>Selected items from the cart will be removed. Please confirm your action.<br/>
           You can still keep this items without deleting for future purchases though.
       </p>
   </div>
   <div class="modal-footer">
    <input type='hidden' name='key' id='w1-key'/>    
    <button type="submit" value='submit' name='btndelete' class="btn btn-danger">Delete</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
</div>
</div>
</div>
</div>
</form> 

@endsection 


@section('script')

<script>


    $(document).on("click", ".btncancel", function()
    {

        var widget = $(this).attr('data-widget');  
        var params  = $(this).attr("data-param");

        $.each( JSON.parse(params)  , function(selector, value) {
            $("#" + selector).val(value); 
        });

        $(".wg-" + widget).modal( "show" );



    });


    $(document).on("click", ".btnupdatecart", function()
    {
        var step = $(this).attr("data-step");  
        var key = $(this).attr("data-key");

        var curval = $("#tbqty" + key).val(); 

        if(curval == "")
        {
            $("#tbqty" + key).val( 1 );
        }
        else if( parseInt(curval) == 0)
        {
            if(parseInt(step) > 0)
            {
                $("#tbqty" + key).val( parseInt(curval) + parseInt(step)  );  
            }

        }
        else if( parseInt(curval) > 0)
        {
            $("#tbqty" + key).val( parseInt(curval) + parseInt(step)  );
        }


    //update cart
    var prid = $(this).attr("data-prid");
    var subid =  $(this).attr("data-subid");
    var bin  = $(this).attr("data-bin");
    var qty = $("#tbqty" + key).val() ;
    var action = $(this).attr("data-action"); 
    var gsid = $("#gsid").html();
    var guid = $("#guid").html();


    var json = {};
    json['guid'] =  guid  ;
    json['gsid'] =  gsid ;
    json['prid'] =   prid;
    json['subid'] = subid;
    json['bin'] =   bin ;
    json['qty'] =  qty ; 


    $.ajax({

        type: 'post',
        url: api + "/v3/web/shopping/add-item-to-cart" ,
        data: json,
        success: function(data)
        {
            data = $.parseJSON(data); 
            if(data.status_code == 6001)
            {
               $(location).attr('href', siteurl + '/shopping/checkout');

           }  
       },
       error: function( ) 
       {
        alert(  'Something went wrong, please try again')
    }
});

}) 


</script>  

@endsection 