@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public

    $total_pay = 0;
    $taxes = 2;
    $tax = $cgst = $sgst = 0;
   ?>
 
<div class='outer-top-tm'> 
<div class="container">
 
  <div class="row">

  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif



   <div class="col-md-8"> 
 
     <div class="card panel-default">
           <div class="card-header">
              <div class="card-title"> 

                Order # <span class='badge rounded-pill text-bg-warning'>{{ $order->id  }}</span> 
                  Order Status:
          @switch( $order->book_status)

          @case("new")
          <span class='badge rounded-pill text-bg-primary'>New</span>
          @break

          @case("confirmed")
          <span class='badge text-bg-info rounded-pill'>Confirmed</span>
          @break

          @case("in_route")
          <span class='badge text-bg-success rounded-pill'>In Route</span>
          @break

          @case("completed")
          <span class='badge text-bg-success rounded-pill'>Completed</span>
          @break 
          @case("cancelled")
          <span class='badge text-bg-danger rounded-pill'>Order Cancelled</span>
          @break

          @endswitch

          <span class="" style="font-weight: bold; font-size:1rem">Payment:</span> 
       @if(  strcasecmp($order->payment_type,"ONLINE") == 0   ) 
            <span class='badge text-bg-success rounded-pill'>ONLINE</span>
            @else
            <span class='badge text-bg-info rounded-pill'>CASH</span>
            @endif

            @if(   strcasecmp($order->payment_status,"pending") == 0   )  
            <span class='badge text-bg-warning rounded-pill'>PENDING</span>
            @else
            <span class='badge text-bg-success rounded-pill'>PAID</span>
            @endif

            OTP: <span class='badge text-bg-info rounded-pill'>{{ $order->otp }}</span> 


          <div class="float-end">

          </div> 
        <hr/>
 
          Order Date:
          <span class='badge text-bg-success rounded-pill' >{{ date('d-m-Y', strtotime( $order->book_date )) }}</span>

          <div class="float-end">
          Service Date:
          <span class='badge text-bg-info rounded-pill' >{{ date('d-m-Y', strtotime( $order->service_date )) }}</span>

          @if( $order->preferred_time != null )
          Time:
          <span class='badge text-bg-info rounded-pill' >{{ date('h:i A', strtotime( $order->preferred_time )) }}</span>
          @endif
          </div>
  
                </div>
            </div>
       <div class="card-body">  
        @if($ordertype=="normal")  
           <div class="table-responsive">
                  <table class="table">
                    <thead  >
                      <th>Sl.No</th>
                        <th style='width: 210px'>Item</th>
                        <th>Qty</th>
                        <th>Unit Price</th> 
                        <th>Packing</th> 
                        <th class='text-right'>Item + Packing = Sub-Total</th> 
                    </thead>
                    <tbody>
                      <?php $i = 0 ;
                      $item_total  =0;
                      $pack_total = 0;
                      $sub_total = 0;
                      $cgst_total =0.00;
                      $sgst_total =0.00;
                      ?>
               

               @foreach ( $cart_items   as $item)

               <?php $i++;

               $item_total += ( $item->price * $item->qty ) ;
               $sub_total += ( $item->price * $item->qty )  +  ( $item->package_charge * $item->qty );
               $pack_total += ( $item->package_charge * $item->qty );
               $total_cost = ( $item->price *  $item->qty ) + ($item->package_charge * $item->qty);

               $tax += $taxes * $total_cost / 100 ;
               $cgst +=  (  ( $item->price + $item->package_charge ) * $item->qty * $item->cgst ) / 100;
               $sgst +=  ( ( $item->price + $item->package_charge ) * $item->qty * $item->sgst ) / 100;


               ?>

                    <tr >
                      <td>{{$i}}</td>
                      <td style='width: 210px'>
                      <span> {{$item->pr_name}}</span><br/>
                      <small>{{ implode(' ', array_slice(str_word_count( $item->description ,1), 0,10)) }}</small>
                      </span>
                    </td>
                      <td>
                      {{$item->qty}} {{$item->unit}}  
                    </td>   

                    <td>
                       {{ $item->price }}  
                    </td>  
                     <td>
                       {{ $item->package_charge * $item->qty  }}  
                    </td>  

                     <td class='text-right'>
                         ( {{ $item->price }} X {{ number_format( $item->qty   , 0 , ".", "" )   }} ) +  ( {{ $item->package_charge }} X {{ number_format( $item->qty   , 0 , ".", "" )  }} )  =     {{  number_format(  ( $item->price * $item->qty )  +  ( $item->package_charge * $item->qty )  , 2, ".", "" ) }}  
                    </td> 


                    </tr>
                   @endforeach
                    <tr>

                    <td colspan="5">
                      <strong>Total Item Cost:</strong>
                    </td>
                    <td class='text-right'>
                    ₹ {{ number_format(  $item_total , 2, ".", "" )   }}
                    </td>

                   </tr>

                       <tr> 
                    <td colspan="5">
                      <strong>Packaging Cost:</strong>
                    </td>
                    <td class='text-right'>
                     ₹ {{ number_format( $pack_total   , 2, ".", "" )   }}
                    </td>

                   </tr>
 

                   <tr> 
                    <td colspan="5">
                      <strong>Total Cost: (Item Cost + Packaging Cost)</strong>
                    </td>
                   
                    <td class='text-right'>
                     ₹ {{ number_format( $sub_total, 2, ".", "" )   }}
                    </td>

                   </tr> 

 @if( Session::get('_user_role_') >= 10000  )

                   <tr> 
                    <td colspan="5">
                      <strong>Delivery Agent Commission:</strong>
                    </td>
                    <td class='text-right'>
                     ₹ {{ number_format( $order->agent_payable , 2, ".", "" )   }}
                    </td>

                   </tr>


                   <tr>

                    <td colspan="5">
                      <strong>bookTou Service Fee:</strong>
                    </td>
                    <td class='text-right'>
                     ₹ {{ number_format( $order->service_fee , 2, ".", "" )   }}
                    </td>

                   </tr>
 @endif


                   <tr>

                    <td colspan="5">
                      <strong>Actual Delivery Charge:</strong>
                    </td>
                    <td class='text-right'>
                     ₹ {{ number_format( $order->delivery_charge , 2, ".", "" )   }}
                    </td>

                   </tr>

                   <tr>
                    <tr>

                    <td colspan="5">
                      <strong>GST:</strong>
                    </td>
                    <td class='text-right'>
                     ₹ {{ number_format(   $cgst + $sgst , 2 , ".", "" )   }} 
                    </td> 
                   </tr>
                  
                  

                   @if($order->coupon)
                    <tr>

                    <td colspan="5">
                      <strong>Discount Coupon:</strong> <button data-widget="rem-coupon" data-key='{{ $order->id }}' class='btn btn-sm btn-danger showmodal'>Remove</button>
                    </td>
                    <td class='text-right'>
                        <span class='badge badge-info'>{{ $order->coupon }}</span> 
                    </td> 
                   </tr>
                   @endif 

                    <tr>

                    <td colspan="5">
                      <strong>Discount Applied:</strong>
                    </td>
                    <td class='text-right'>
                     ₹ {{ number_format( $order->discount , 2, ".", "" )   }} 
                    </td> 
                   </tr>
                   <tr>
                    <td colspan="5">
                      <strong>Total Order Amount:</strong>
                    </td>
                    <td class='text-right'>
                    <label  > ₹ {{ number_format(  ( $sub_total + $order->delivery_charge + $cgst + $sgst) - $order->discount  , 2, ".", "" )   }}</label>
                  &nbsp;&nbsp; 
                  @if( strcasecmp($order->payment_type, "online") == 0 && strcasecmp($order->book_status, "cancelled") != 0 && strcasecmp($order->payment_status, "pending") == 0  )
                      <a target='_blank' class="btn btn-primary btn-rounded btn-xss" href="{{ URL::to('/shopping/payment-in-progress') }}?o={{ $order->id }}" style="color:white;">Pay Now</a> 
                    @endif
                    </td>
                   </tr>
               
                     
                    </tbody>
                    </table>




                  </div>
                    @elseif($ordertype=="assist") 

                    <?php 
                      $image_url = $cdn_url.'/assets/image/no-image.jpg';
                    ?>
                    <div class="row">

                      <div class="col-md-12"> 
                        <table class="table">
                         <thead class=" text-primary">
                            <th class="col-md-2">Pickup Image:</th> 
                            <th class="col-md-4">Order Details:</th> 
                            <th>Service Date</th>
                            <th>Service Fee:</th> 
                            <th>Amount</th> 
                        </thead> 
                        <tbody>
                          <tr>
                          <td>@if($order->pickup_image=="") 
                              <img src="{{$image_url}}" style="width:150px;"> 
                              @else
                                <img src="{{$order->pickup_image}}" style="width:150px;"> 
                              @endif
                          </td>
                          <td>{{$order->pickup_details}}</td>
                          <td>{{date('Y-m-d',strtotime($order->service_date))}}</td>  
                          <td> 
                             {{$order->service_fee}}
                          </td> 
                          <td>₹  {{$order->service_fee}} </td>
                          </tr> 
                           
                        
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>To Pay:</b></td>
                            <td>₹ {{$order->service_fee}} </td>
                          </tr>
                        </tbody>
                        </table> 

                         
                      </div>
                     </div>

                    @elseif($ordertype=="booking") 
                     <!-- booking section -->
                     <div class="row">
                      
                        @switch($business->main_module)

                        @case ('Booking')
                        <div class="col-md-12"> 
                        <table class="table">
                         <thead class=" text-primary">
                            <th>Photos</th> 
                            <th>Service Booked Name</th> 
                            <th>Details</th>
                            <th>Amount</th>  
                        </thead>
                        @foreach($booking_details as $items)
                        <tbody>
                          <tr>
                          <td><img src="{{$items->photos}}" class="img-fluid" width="100"></td>
                          <td>{{$items->service_name}}</td> 
                          <td>{{$items->serviceDetails}}</td>
                          <td>₹ {{$items->service_charge}}</td>
                          </tr> 
                          <?php 
                              $total_pay += $items->service_charge - $order->discount; 
                          ?>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            
                            <td><b>Total Amount:</b></td>
                            <td>₹ {{number_format($total_pay, 2, ".", "")}}</td>
                          </tr>
                        </tbody>
                        </table>  
                         
                      </div>

                        @break

                        @case ('Appointment')
                        
                        <div class="col-md-3 text-center"> 
                        <?php 
                          if($staff->profilePicture=="" || $staff->profilePicture==null) {
                              $image_url = $cdn_url.'/assets/image/no-image.jpg';
                            }else {
                                     $image_url = $staff->profilePicture;
                            } 
                        ?>
                        <img src="{{$image_url}}" class="rounded-circle" style="height:100px;width:100px;">
                        <hr>
                        <span class="text-center text-uppercase"><h7>
                          @if($staff->staffName=="Admin bookTou")
                              Any Staff
                          @else
                              {{$staff->staffName}} 
                          @endif
                        </h7></span>
                        </div>

                        <div class="col-md-9"> 
                        <table class="table">
                         <thead class=" text-primary">
                            <th>Service Booked</th>
                            <th>Coupon Code</th> 
                            <th>Payment Mode</th> 
                            <th>Discount</th> 
                            <th>Amount</th> 
                           
                        </thead>
                        @foreach($booking_details as $items)
                        <tbody>
                          <tr>
                          <td>{{$items->service_name}}</td>  
                          <td>@if($order->coupon=="")
                              
                             <span class="text-uppercase text-center"> None </span>
                              @else
                              {{$items->coupon}}
                              @endif
                          </td>
                          <td>{{$order->payment_type}}</td> 
                          <td>₹ {{$order->discount}}</td>
                          <td>₹ {{$items->service_charge}}</td>
                          </tr> 
                          <?php 
                              $total_pay += $items->service_charge - $order->discount; 
                          ?>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Total Amount:</b></td>
                            <td>₹ {{number_format($total_pay, 2, ".", "")}}</td>
                          </tr>
                        </tbody>
                        </table>  
                         
                      </div>
                        @break 
                        @endswitch 
                      

                      
                     </div>
                    
                     <!-- booking section ends here -->

                     <!-- offers orders -->
                     @elseif($ordertype=="offers")


                     <div class="col-md-12"> 
                        <table class="table">
                         <thead class=" text-primary">
                            <th>Service Booked Name</th> 
                            <th>Details</th>
                            <th>Amount</th>  
                        </thead>
                        @foreach($booking_details as $items)
                        <tbody>
                          <tr> 
                          <td>{{$items->srv_name}}</td> 
                          <td>{{$items->srv_details}}</td>
                          <td>₹ {{$items->price}}</td>
                          </tr> 
                          <?php 
                              $total_pay += $items->price; 
                          ?>
                        @endforeach
                        <tr>  
                            <td></td> 
                            <td><b>Total Amount:</b></td>
                            <td>₹ {{number_format($total_pay, 2, ".", "")}}</td>
                          </tr>
                        </tbody>
                        </table>  
                         
                      </div>
                      <!-- offer orders ends here -->

                    @endif 
              </div>
              
    @if( strcasecmp($order->book_status, "cancelled") != 0 )
        <div class='card-footer text-end'>
              <button type="button" id="confirmcancel" data-key="{{ $order->id }}" data-target="wgtconfirmcancel" 
                class="btn btn-danger btn-rounded btn-xss">Cancel Order</button>
        </div>
    @else
       <div class='card-footer text-end'>
              <p>Your order has been cancelled.</p>
        </div>
    @endif
              
            </div>
 


 </div>


 <div class="col-md-4">   
  
   @if($ordertype=="booking" || $ordertype=="normal" || $ordertype=="offers")

<!-- section for side view -->
            <div class="card mt-2">
              <div class="card-header">
                <h5  >Shop/Business</h5><hr>
                <h4 class="card-title">{{$business->name}}</h4>
                 <p>{{$business->locality}}<br/>
                 {{$business->landmark}}, {{$business->city}}<br/>
                <i class='fa fa-phone'></i> {{$business->phone_display}}</p> 

                </div> 
            </div> 

             <div class="card panel-default mt-2">
              <div class="card-header">
                <h5  >Customer</h5><hr>
     <h4 class="card-title">{{$order->customer_name}}</h4>
                <p>{{$order->address}}<br/>
                  {{$order->landmark}}<br/>
                  {{$order->city}}, {{$order->state}} - {{ $order->pin_code}} <br/>
                  <i class='fa fa-phone'></i> {{$order->customer_phone}}</p> 
               </div>  
            </div>
  
              <!-- booking -->
              @if($ordertype=="booking")  
              <div class="card panel-default mt-2 mb-2">
              <div class="card-header">
                <div class="row">
                <div class="col-md-8">  <h5>Remarks</h5> </div>
                <div class="col-md-4">
                  @if($order->cust_remarks=="")
                  <button class="btn btn-primary btn-sm text-right btn_remarks" data-key="{{$order->id}}">
                  Add Remarks
                  </button>
                  @endif   
                </div>
                </div> 
                  <hr> 
                  <p>{{$order->cust_remarks}}</p>
                  
              </div>  
              </div>
              @endif      
              <!-- booking ends here -->
              <div class="card mt-2 mb-2">
                <div class="card-header">
                <h4>Order Rating</h4>
              </div>
             <form method="post" action="{{action('StoreFront\StoreFrontController@updateOrderReview')}}"> 
              {{csrf_field()}}
              <div class="row card-body">
               @foreach($ratingType as $items) 
                    
                    

                    <div class="col-md-12"> 
                    <h7><small><b>{{$items->rating_type}}</b></small></h7> 
                  
                        <?php 
                        if($items->rating>0) {
                           switch($items->rating) {
                            case '1':
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break; 
                          case '2':
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break;
                          case '3':
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break;
                          case '4':
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break;
                          default :
                            ?>
                            <div class="star-area">   
                            <h4>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true" 
                              data-key="{{$items->id}}" data-number="1"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                            <i class="star_review ratings{{$items->id}} fa fa-star" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                          </h4>
                          </div>

                        <?php 
                          break;
                        ?>
 

                        <?php  
                          }
                        ?>  
             
                     <?php }else {?>

                    

                    <div class="star-area">
                    <h4>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o"  aria-hidden="true" 
                        data-key="{{$items->id}}" data-number="1"></i>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="2"></i>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="3"></i>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="4"></i>
                      <i class="star_review ratings{{$items->id}} fa fa-star-o" aria-hidden="true"  data-key="{{$items->id}}" data-number="5"></i>
                    </h4>
                   </div>

                  <?php }?> 
                     
                    @if($items->rating>0)
                    <label>{{$items->review}}</label> 
                    @else
                      <input type="text" class="form-control" name="review[]" placeholder="<?php echo strtolower($items->rating_type)." ".'review';?>" value="{{$items->review}}" @if($items->status=="verified")disabled @endif>
                    @endif

                    <input type="hidden" name="ratingType[]" value="{{$items->rating_type}}" @if($items->status=="verified")disabled @endif>
                    @if($items->rating>0)
                    <input type="hidden" name="rating[]" class="ratingno{{$items->id}}" value="{{$items->rating}}" @if($items->status=="verified")disabled @endif>
                    @else
                      <input type="hidden" name="rating[]" class="ratingno{{$items->id}}" value="1" @if($items->status=="verified")disabled @endif>
                    @endif
                    <input type="hidden" name="orderno" class="orderno" value="{{request()->get('key')}}"@if($items->status=="verified")disabled @endif>
                    <hr>
                    </div>
                    
                    
              @endforeach

              @if($items->rating>0)
              
              @else
              <div class="col-md-12 mt-2">
               <button type="submit" class="btn btn-primary btn-block">Submit</button>
              </div>
              @endif
              
              
            </div>
            </form>
              </div>

              @else

              <div class="card panel-default">
              <div class="card-header">
                <h5 class=" ">Pick up from</h5>
                <h4 class="card-title"></h4>
                </div>
              <div class="card-body">

                <div class="timeline timeline-xs">
                  <div class="timeline timeline-xs"> 
                    <div class="timeline-item mb-2">
                      <div class="timeline-item-marker">
                        
                        <div class="timeline-item-marker-indicator bg-red"></div>
                      </div>
                      <div class="timeline-item-content">
                        {{$order->pickup_name}} <br>
                        {{$order->pickup_address}}<br>
                         
                        <i class="fa fa-phone"></i> {{$order->pickup_phone}}
                      </div>
                    </div> 
                  </div>
                </div> 
              </div> 
              
            </div>

            <div class="card mt-2">
                <div class="card-header">
                <h5>Drop at</h5>
              </div>
              <div class="card-body">
                <div class="timeline-item">
                      <div class="timeline-item-marker"> 
                        <div class="timeline-item-marker-indicator bg-green"></div>
                      </div>
                      <div class="timeline-item-content">
                        {{$order->drop_name}}<br>
                        {{$order->drop_address}}<br> 
                        <i class="fa fa-phone"></i> {{$order->drop_phone}}
                      </div>
                    </div>

              </div>
              </div> 
              @endif
          
              <div class="card panel-default mt-2 mb-2">
              <div class="card-header">
                <div class="row">
                <div class="col-md-8">  <h5>Remarks</h5> </div>
                <div class="col-md-4">
                  @if($order->cust_remarks=="")
                  <button class="btn btn-primary btn-sm text-right btnassistremarks" data-key="{{$order->id}}">
                  Add Remarks
                  </button>
                  @endif   
                </div>
                </div>   
                  
                  <hr> 
                  <p>{{$order->cust_remarks}}</p>
                  
              </div>  
              </div>

              <!-- all section ends here -->
          </div>  

       </div>  
 </div>  
 </div> 
 

 
<!-- remarks modal --> 
<!-- Modal -->
<div class="modal fade" id="widget{{$order->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <form method="post" action="{{action('StoreFront\StoreFrontController@updateOrderRemarks')}}">
        {{csrf_field()}}
      <div class="modal-header">
        <h5 class="modal-title" id="remarks">Customer Remarks</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="card">
        <input type="hidden" name="key" id="widgetkey">
        <input type="text" name="customer_remarks" class="form-control" placeholder="Add your remarks">
       </div>
      </div>
      <div class="modal-footer"> 
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
</div> 
<!-- remarks modal ends here -->



<!-- remarks modal --> 
<!-- Modal -->
<div class="modal fade" id="assistwidget{{$order->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <form method="post" action="{{action('StoreFront\StoreFrontController@updateAssistOrderRemarks')}}">
        {{csrf_field()}}
      <div class="modal-header">
        <h5 class="modal-title" id="remarks">Add your order feedback</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="card">
        <input type="hidden" name="assistkey" id="assistwidgetkey">
        <textarea name="customer_remarks" placeholder="Add your feedback" class="form-control"></textarea>
       </div>
      </div>
      <div class="modal-footer"> 
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
</div> 
<!-- remarks modal ends here -->


<div class="modal fade" id="wgtconfirmcancel" tabindex="-1" aria-labelledby="cancelorder" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <form method="post" action="{{action('StoreFront\StoreFrontController@cancelShoppingOrder')}}">
        {{csrf_field()}}
        <input type="hidden" name="key" id="wgtconfirmcancelkey">
      <div class="modal-header">
        <h5 class="modal-title" id="remarks">Cancel Booking Order</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
         <p>You are about to cancel your prebooked order. Though the cancellation process is fast and your order amount refund may depend on our terms of use and refund policy.</p>
         <p>Please read the refund policy and guides before proceeding. You can talk with one of our customer support staff prior to your cancellation process in case you want to clarify any doubt.<br/>
          Read cancellation policy <a href="{{ URL::to('/terms/cancellation-and-refund') }}" target="_blank">here</a>

        </p>. 
      </div>
      <div class="modal-footer">
          <button type="submit" name="save" value="cancel" class="btn btn-danger rounded-pill">Confirm Cancellation</button>
         <button type="button" class="btn btn-secondary rounded-pill" data-bs-dismiss="modal">I've Change My Mind</button>
      </div>
    </form>
    </div>
  </div>
</div>



@endsection


@section('script')
 

<script> 

document.querySelectorAll('.star_review').forEach(function(element) {
          element.addEventListener("mouseenter", function(event) {
            var rid = $(this).attr('data-key');
             
            var stars = event.target.dataset.number; 
            var count = 0;
            document.querySelectorAll('.ratings'+rid).forEach(function(el) {
              if (count < stars) {
                $(el).removeClass('fa-star-o');
                $(el).addClass('fa-star');
                $('.ratingno'+rid).val(stars);
               
              } else {
                $(el).removeClass('fa-star');
                $(el).addClass('fa-star-o');
              }
              count = count+1; 
            });
          });
          element.addEventListener("click", function(event) {
            console.log('clicked')
             $(event).removeClass('fa-star');
             $(event).addClass('fa-star-o');
          });
          element.addEventListener("mouseleave", function(event) {
            document.querySelectorAll('.star_review').forEach(function(el) {
              
            });
          });
        });


$(document).on("click", ".btn_remarks", function()
{
   
    var widget = $(this).attr('data-key'); 
    $("#widgetkey").val(widget); 
    $("#widget"+widget).modal("show"); 

});

$(document).on("click", ".btnassistremarks", function()
{
   
    var widget = $(this).attr('data-key'); 
    $("#assistwidgetkey").val(widget); 
    $("#assistwidget"+widget).modal("show"); 

});



$(document).on("click", "#confirmcancel", function()
{
  var widget = $(this).attr('data-target');
  var key = $(this).attr('data-key');
  $("#" +  widget + "key").val(key);
  $("#" + widget ).modal("show");
});



</script>


@endsection

   