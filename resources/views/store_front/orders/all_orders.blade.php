@extends('layouts.store_front_02')
@section('content')
 
 
<div class='outer-top-tm'> 
<div class="container">
 

  <div class="row mt-2">

    <div class="col-12">
      <div class="page-title text-center">
        <h1>My Shopping Orders</h1>
      </div>
    </div>

            <div class="col-md-8 offset-md-2 mt-2">
               <div class='card'> 
                  <div class='card-body'>
                    <ul class="nav nav-pills">
                      <li class="nav-item pr-2">
                        <a class="nav-link active"  href="{{ URL::to('/shopping/my-orders') }}">My Orders</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="nav-link btn-primary"  href="{{ URL::to('/shopping/my-bookings') }}">My Bookings</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="nav-link  btn-danger"  href="{{URL::to('/shopping/show-wishlist-product') }}">Your Wishlist</a>
                    </li>
                </ul>
            </div> 
        </div>
    </div>


 
 </div>
 <div class="row">
 <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($orders as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');  

                ?>
               
              <div class="col-md-8 offset-md-2">

                <div class="card mt-1"><div class="card-body ">

                  <div class="dropdown pull-right"> 
                      </div>
                
                  

                  <div class="row">   
                 <div class='col-md-2' id="tr{{$item->id }}" > 
                  <div class="pa-2 " >      
                    <strong>Order #</strong><br/> 
                     
                    <a class='h1' href="{{ URL::to('/shopping/my-order-details') }}?key={{ $item->id }}"  >
                    {{$item->id}}
                     </a> 
                     
              </div>
 
            </div>
               
                <div class='col-md-3'  > 
                 <div class="pa-2">  
                  <strong>Order Summary</strong><br/>
                
                <strong>Order Date:</strong> {{ date('d-m-Y', strtotime( $item->book_date )) }}<br/>   
                    <strong>Order Status:</strong> 
                      @switch($item->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                        @break
                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                        @break

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                        @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                        @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                        @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                        @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                        @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                        @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                        @break 

                        @case("canceled")
                        <span class='badge badge-danger'>Cancelled</span>
                        @break 
                      @case("cancelled")
                        <span class='badge badge-danger'>Cancelled</span>
                        @break
                      @case("pending-payment")
                        <span class='badge badge-danger'>Pending</span>
                        @break  
                        

                      @endswitch

                      <br/>
                      <strong>Payment Type:</strong> 
                      @if( strcasecmp($item->payment_type,"Cash on delivery") == 0 || strcasecmp($item->payment_type,"cash") == 0 )
                        CASH
                      @else 
                        ONLN
                      @endif
                      <br/>
 
                  </div>   
                </div>

                 <div class='col-md-3'  > 
                  <div class="pa-2"> 
                   @if($item->type=='assist')
                    <strong>Pickup Details</strong><br/>
                    
                    {{$item->orderdescription}}

                      @else 
                      <strong>Merchant Details</strong><br/>
                        @foreach($all_businesses  as $business)
                          @if($business->id == $item->bin)
                            {{  $business->name  }}
                            {{  $business->locality  }}
                            @break
                          @endif
                        @endforeach
                      @endif
                  
</div> 
                </div>


                  
  
              </div>
           </div>
                   </div>
 </div> 
                 
          <?php
                          $i++; 
                       }

                 ?>
              
             </div>
        </div>
    </div>  
   
<div class="mt-3"></div>
   
 
@endsection

@section("script")

<style type='text/css'>
  .pa-2
  {
    padding: 10px;
  }
</style>
 

@endsection 