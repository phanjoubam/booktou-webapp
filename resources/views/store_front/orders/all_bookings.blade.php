@extends('layouts.store_front_02')
@section('content')
 <style>
 .checked {
  color: orange;
  }
</style> 
<div class='outer-top-tm'> 
<div class="container">
 
  <div class="row mt-2">
        <div class="col-12">
      <div class="page-title text-center">
        <h1>My Booking and Appointment Orders</h1>
      </div>
    </div>

             <div class="col-md-8 offset-md-2 mt-2">
               <div class='card'> 
                  <div class='card-body'>
                    <ul class="nav nav-pills">
                      <li class="nav-item pr-2">
                        <a class="nav-link "  href="{{ URL::to('/shopping/my-orders') }}">My Orders</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="nav-link btn-primary active"  href="{{ URL::to('/shopping/my-bookings') }}">My Bookings</a>
                    </li>
                    <li class="nav-item pr-2">
                        <a class="nav-link  btn-danger"  href="{{URL::to('/shopping/show-wishlist-product') }}">Your Wishlist</a>
                    </li>
                </ul>
            </div> 
        </div>
    </div>
 </div>
 <div class="row">
 <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($orders as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');  

                ?>
               
              <div class="col-md-8 offset-md-2">

                <div class="card mt-1"><div class="card-body ">

                  <div class="dropdown pull-right"> 
                      </div>
                
                  

                  <div class="row">   
                 <div class='col-md-2' id="tr{{$item->orderid }}" > 
                  <div class="pa-2 text-center" >      
                    <strong>Order #</strong><br/> 
                     
                    <a class='h1' href="{{ URL::to('/booking/view-order-details') }}?key={{ $item->orderid }}"  >
                    {{$item->orderid}}
                     </a>
                    <br/>
                    <small>{{ date('d-m-Y', strtotime( $item->book_date )) }}</small>
                     
              </div>
 
            </div>
               
                <div class='col-md-3'  > 
                 <div class="pa-2">  
                  <strong>Order Summary</strong><br/>
                  <strong>Booking Date:</strong> <br/>
                  <strong>Service Date:</strong> {{ date('d-m-Y', strtotime( $item->service_date )) }}<br/>   

                    <strong>Order Status:</strong> 
                      @switch($item->book_status)

                      @case("new")
                        <span class='badge text-bg-primary rounded-pill'>New</span>
                        @break
                      @case("confirmed")
                        <span class='badge text-bg-info rounded-pill'>Confirmed</span>
                        @break

                      @case("order_packed")
                        <span class='badge text-bg-info rounded-pill'>Order Packed</span>
                        @break

                      @case("package_picked_up")
                        <span class='badge text-bg-info rounded-pill'>Package Picked Up</span>
                        @break

                       @case("pickup_did_not_come")
                        <span class='badge text-bg-warning rounded-pill'>Pickup Didn't Come</span>
                        @break

                       @case("in_route")
                        <span class='badge text-bg-success rounded-pill'>In Route</span>
                        @break

                       @case("completed")
                        <span class='badge text-bg-success rounded-pill'>Completed</span>
                        @break

                      @case("delivered")
                        <span class='badge text-bg-success rounded-pill'>Delivered</span>
                        @break

                       @case("delivery_scheduled")
                        <span class='badge text-bg-info-bg-success rounded-pill'>Delivery Scheduled</span>
                        @break 

                        @case("canceled")
                        <span class='badge text-bg-danger rounded-pill'>Cancelled</span>
                      @break 

                      @endswitch

                      <br/>
                      <strong>Payment Type:</strong> 
                      @if( strcasecmp($item->payment_type,"Cash on delivery") == 0 || strcasecmp($item->payment_type,"cash") == 0 )
                        CASH
                      @else 
                        ONLINE
                      @endif
                      <br/>
 
                  </div>   
                </div>

                <div class='col-md-3'> 
                  <div class="pa-2"> 
                      <strong>Merchant Name</strong><br/>
                        {{$item->name}}
                      <br>
                      {{$item->phone_pri}}
                      <br>
                      {{$item->locality}}

                  </div> 
                </div>

                <div class="col-md-3">
                    <div class="pa-2"> 
                      
                       
                  </div> 
                </div>
                  
  
              </div>
           </div>
                   </div>
 </div> 
                 
          <?php
                          $i++; 
                       }

                 ?>
              
             </div>
        </div>
    </div>  
   

   
 
@endsection

@section("script")

<style type='text/css'>
  .pa-2
  {
    padding: 10px;
  }
</style>
 

@endsection 