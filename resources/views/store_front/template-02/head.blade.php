<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <title><?php if(isset($html_title) && $html_title != "" ) echo $html_title . " - bookTou" ; else echo "bookTou - Explore the Change";  ?></title>

    <link rel="canonical" href="{{ Request::url() }}">

    @php
     $description ="bookTou is your one-stop destination to book beauty and health services, book picnic spots, book holiday homestays, book makeup services, book any service nearby";
    @endphp

    <meta name="description" content="{{ $description }}"> 
    <meta name="facebook-domain-verification" content="lo0m9lyv37z6aqlifngirc4ry4hfu4" />
    <meta property="og:title" content="{{ $description }}" />
    <meta property="og:description" content="{{ $description }}" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="bookTou" />
    <meta property="og:url" content="https://booktou.in" />
    <meta name="og:image" content="https://booktou.in/public/assets/image/logo.jpg" />


    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="https://booktou.in">
    <meta name="twitter:image" content="https://booktou.in/public/assets/image/logo.jpg" />

    <meta name="twitter:title" content="{{ $description }}" />
    <meta name="twitter:description" content="{{ $description }}">
 
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="mask-icon" href="{{ URL::to('/public/store/image') }}/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::to('/public/store/image') }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('/public/store/image') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::to('/public/store/image') }}/favicon-16x16.png">
    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Ubuntu:300,400,500,700' rel='stylesheet' type='text/css' />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::to('/public/store/vendor/bootstrap/5.2.0/css/bootstrap.css') }}">
      <link rel="stylesheet" href="{{ URL::to('/public/store/css/classy-nav.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::to('/public/store/css/jquery-ui.min.css') }}"> 
    <link rel="stylesheet" href="{{ URL::to('/public/assets/vendor/fa/6.1.1/css/all.css') }}">
    <link href="{{ URL::to('/public/assets/vendor/fa/6.1.1/css/v4-shims.css') }}" rel="stylesheet"> 
    
    <link rel="stylesheet" href="{{ URL::to('/public/store')}}/css/zoom.css" />
    <link rel="stylesheet" href="{{ URL::to('/public/store')}}/vendor/fancybox/jquery.fancybox.css" />
    <link rel="stylesheet" href="{{ URL::to('/public/store')}}/vendor/fancybox/helpers/jquery.fancybox-thumbs.css" />
    <link href="{{ URL::to('/public/store') }}/vendor/pn/css/pignose.calendar.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{URL::to('/public/store/vendor/slick/css')}}/slick.css"/>
    <link rel="stylesheet" type="text/css" href="{{URL::to('/public/store/vendor/slick/css')}}/slick-theme.css"/> 
    <link rel="stylesheet" href="{{ URL::to('/public/store/theme-02') }}/css/style.css">
    <link rel="stylesheet" href="{{URL::to('/public/store/css/jquery.timepicker.css')}}"> 
    <link  rel="stylesheet" href="{{ URL::to('/public/store/vendor/swiper/css/swiper-bundle.min.css') }}" />
    <link  rel="stylesheet" href="{{ URL::to('/public/assets/vendor/calendar/css/zabuto_calendar.min.css') }}" />

    <style type="text/css"> 

.megamenu {
  position: static;
}

.megamenu .dropdown-menu {
  background: none;
  border: none;
  width: 100%;
}

.subnav.green
    {
        background-color: #40cf33 !important;
        padding: 10px;

    }


    .subnav.yellow
    {
        background-color: #fff633 !important;
        padding: 10px;

    }
    .subnav.orange
    {
        background-color: #f83 !important;
        padding: 10px;

    }

    .navbar-nav li
    {
        height: 50px !important;
        padding-top: 10px !important;
    }


    .navbar-nav li a:active, .navbar-nav li a:visited
    {
        color: #454545 !important;
    }


@media  screen and (min-width: 960px) {

    .navbar-nav li.parent.active.green
    {
        background-color: #40cf33 !important;
        color: #fff !important;
        border-top-right-radius: 6px !important;
        border-top-left-radius: 6px !important;
        margin-right:5px;
    }

    .navbar-nav li.parent.yellow
    {
        background-color: #fff633 !important;
        color: #fff !important;
        border-top-right-radius: 6px !important;
        border-top-left-radius: 6px !important;
        margin-right:5px;

    }
    .navbar-nav li.parent.orange
    {
        background-color: #f83 !important;
        color: #fff !important;
        border-top-right-radius: 6px !important;
        border-top-left-radius: 6px !important;
        margin-right:5px;

    }

}
@media  screen and (max-width: 960px) {
    .subnav
        {
            margin-top: 15px !important;
        }

}

    @media  screen and (max-device-width: 480px) and (orientation: portrait)
    {

        .navbar-nav li.parent.active.green, .navbar-nav li.parent.yellow, .navbar-nav li.parent.orange
        {
            background-color: #fff !important;
        }


    }

    @media  screen and (max-device-width: 640px) and (orientation: landscape)
    {
        .navbar-nav li.parent.active.green, .navbar-nav li.parent.yellow, .navbar-nav li.parent.orange
        {
            background-color: #fff !important;
        }

    }


code {
  color: #745eb1;
  background: #fff;
  padding: 0.1rem 0.2rem;
  border-radius: 0.2rem;
}

.text-uppercase {
  letter-spacing: 0.08em;
}

.subnav .nav
{
    margin-top: 10px !important;
}

.subnav .nav li a:link, .subnav .nav li a:visited
{
    color: #343434;
}



            .mega_anchor{
                color: #fff;
                font-size: 12px;
            }
            .bg-white {
                /*background: #37b516e8 !important;*/
            }
            .megamenu  {
                    position: static

            }
            .megamenu .dropdown-menu {
                background: none;
                border: none;
                width: 100%;
                color:#fff;
            }


            code {
                color: #745eb1;
                background: #efefef;
                padding: 0.1rem 0.2rem;
                border-radius: 0.2rem
            }

            .text-uppercase {
                letter-spacing: 0.08em
                color:#fff;
            }

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}


/*chat bot style starts here*/
.chat-screen {
    position: fixed;
    bottom: 68px;
    right: 20px;
    z-index: 9999;
    width: 350px;
    background: #fff;
    box-sizing: border-box;
    border-radius: 15px;
    box-shadow: 0px 15px 20px rgba(0, 0, 0, 0.1);
    visibility: hidden;
}
.chat-screen.show-chat {
    -moz-transition: bottom 0.5s linear;
    -webkit-transition: bottom 0.5s linear;
    transition: bottom 0.5s linear;
    visibility: visible;
    bottom: 82px;
}
.chat-screen .chat-header {
    background-image: linear-gradient(to right, #ff6a00, #ff6a00, #ff6a00, #ff6a00, #ff6a00);
    border-radius: 15px 15px 0 0;
    padding: 15px;
    display: block;
}
.chat-screen .chat-header .chat-header-title {
    display: inline-block;
    width: calc(100% - 50px);
    color: #fff;
    font-size: 16px;
}
.chat-screen .chat-header .chat-header-option {
    display: inline-block;
    width: 44px;
    color: #fff;
    font-size: 14px;
    text-align: right;
}
.chat-screen .chat-header .chat-header-option .dropdown .dropdown-toggle svg {
    color: #fff;
}
.chat-screen .chat-mail {
    padding: 30px;
    display: block;
}
.chat-screen .chat-mail input.form-control {
    border-radius: 30px;
    border: 1px solid #e1e1e1;
    color: #3b3f5c;
    font-size: 14px;
    padding: 0.55rem 1.25rem;
}
.chat-screen .chat-mail input.form-control:focus {
    box-shadow: none;
    border: 1px solid #add5fc;
}
.chat-screen .chat-mail .select2 .selection .select2-selection .select2-selection__rendered {
    border-radius: 30px;
    border: 1px solid #e1e1e1;
    height: calc(1.28em + 1.28rem + 2px);
    padding: 9px 20px;
    font-size: 14px;
}
.chat-screen .chat-mail .select2.select2-container--open .selection .select2-selection {
    box-shadow: none;
    border-radius: 30px;
}
.chat-screen .chat-mail button {
    background-image: linear-gradient(to right, #673ab7, #813bcb, #9e38de, #bc32ef, #dc22ff);
    border: none;
    padding: 0.58rem 1.25rem;
    transition: transform 0.5s ease;
}
.chat-screen .chat-mail .form-group {
    margin-bottom: 1.5rem;
}
.chat-screen .chat-body {
    /*padding: 25px;*/
    display: inline-block;
    min-height: 382px;
    max-height: 382px;
    background: #fbfbfb;
}
.chat-screen .chat-body .chat-start {
    border: 1px solid #f8d4ff;
    width: 150px;
    border-radius: 50px;
    padding: 6px 10px;
    font-size: 12px;
    margin: 0 auto;
    text-align: center;
    margin-bottom: 15px;
    background: #fff;
}
.chat-screen .chat-body .chat-bubble {
    font-size: 12px;
    padding: 10px 15px;
    box-shadow: none;
    display: inline-block;
    clear: both;
    margin-bottom: 10px;
    box-shadow: 0 5px 5px rgba(0, 0, 0, 0.02);
}
.chat-screen .chat-body .chat-bubble.you {
    background: #efefef;
    color: #000;
    border-radius: 15px 15px 15px 15px;
    align-self: flex-start;
    display: table;
}
.chat-screen .chat-body .chat-bubble.me {
    background-image: linear-gradient(to right, #FFFFFF, #FFFFFF, #FFFFFF, #FFFFFF, #FFFFFF);
    color: #888ea8;
    border-radius: 15px 0px 15px 15px;
    float: right;
    align-self: flex-end;
    display: table;
}
.chat-screen .chat-input {
    width: 100%;
    position: relative;
    margin-bottom: -5px;
}
.chat-screen .chat-input input {
    width: 100%;
    background: #ffffff;
    padding: 15px 70px 15px 15px;
    border-radius: 0 0 15px 15px;
    resize: none;
    border-width: 1px 0 0 0;
    border-style: solid;
    border-color: #f8f8f8;
    color: #7a7a7a;
    font-weight: normal;
    font-size: 13px;
    transition: border-color 0.5s ease;
}
.chat-screen .chat-input input:focus {
    border-color: #f9dcff;
}
.chat-screen .chat-input input:focus + .input-action-icon a svg.feather-send {
    color: #bc32ef;
}
.chat-screen .chat-input .input-action-icon {
    width: 61px;
    white-space: nowrap;
    position: absolute;
    z-index: 1;
    top: 15px;
    right: 15px;
    text-align: right;
}
.chat-screen .chat-input .input-action-icon a {
    display: inline-block;
    margin-left: 5px;
    cursor: pointer;
}
.chat-screen .chat-input .input-action-icon a svg {
    height: 17px;
    width: 17px;
    color: #a9a9a9;
}
.chat-screen .chat-session-end {
    display: block;
    width: 100%;
    padding: 25px;
}
.chat-screen .chat-session-end h5 {
    font-size: 17px;
    text-align: center;
    font-weight: bold;
    margin-top: 20px;
}
.chat-screen .chat-session-end p {
    font-size: 14px;
    text-align: center;
    margin: 20px 0;
}
.chat-screen .chat-session-end .rate-me {
    width: 120px;
    margin: 40px auto;
}
.chat-screen .chat-session-end .rate-me .rate-bubble {
    display: inline-block;
    text-align: center;
    width: 50px;
}
.chat-screen .chat-session-end .rate-me .rate-bubble span {
    height: 50px;
    width: 50px;
    text-align: center;
    display: block;
    line-height: 46px;
    cursor: pointer;
    transition: transform 0.5s ease;
    margin-bottom: 7px;
}
.chat-screen .chat-session-end .rate-me .rate-bubble span:hover {
    transform: scale(1.1);
    transition: transform 0.5s ease;
}
.chat-screen .chat-session-end .rate-me .rate-bubble.great {
    margin-right: 12px;
    color: #43cc6c;
}
.chat-screen .chat-session-end .rate-me .rate-bubble.great span {
    background: #43cc6c;
    border-radius: 50px 50px 0 50px;
}
.chat-screen .chat-session-end .rate-me .rate-bubble.bad {
    color: #ef4252;
}
.chat-screen .chat-session-end .rate-me .rate-bubble.bad span {
    background: #ef4252;
    border-radius: 50px 50px 50px 0;
}
.chat-screen .chat-session-end .transcript-chat {
    display: block;
    text-align: center;
    margin-top: 80px;
    color: #0768f8;
    text-decoration: underline;
    line-height: 20px;
}
.chat-screen .powered-by {
    margin-top: 40px;
    text-align: center;
    font-size: 12px;
}
.chat-bot-icon {
    position: fixed;
    bottom: 20px;
    right: 20px;
    height: 50px;
    width: 50px;
    background-image: linear-gradient(to right, #ff6a00, #ff6a00, #ff6a00, #ff6a00, #ff6a00);
    z-index: 9999;
    border-radius: 30px;
    box-shadow: 0px 10px 15px rgba(0, 0, 0, 0.1);
    text-align: center;
    line-height: 50px;
    cursor: pointer;
    transition: all 0.5s ease;
}
.chat-bot-icon img {
    height: 90px;
    width: 90px;
    position: absolute;
    right: -13px;
    top: -33px;
}
.chat-bot-icon svg {
    color: #fff;
    -moz-transition: all 0.5s linear;
    -webkit-transition: all 0.5s linear;
    transition: transform 0.5s linear;
    position: absolute;
    left: 13px;
    top: 13px;
    opacity: 0;
    z-index: -1;
}
.chat-bot-icon svg.animate {
    -moz-transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
    opacity: 1;
    z-index: 1;
}

/*main*/

.chat-box
{
    min-height: 450px;
}
.modal, .modal-backdrop {
    position: absolute !important;
}
.after_modal_appended
{
  position:relative;
}
.block
{
  width:100%;
  height:200px;
}
.star-area {

}

.fa-star-o {
    color: #212529;
    cursor: pointer;
}
.fa-star {
    color: #FFD700;
    cursor: pointer;
}
/*chat bot style*/


.swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }

.box1 img,.box1:after,.box1:before{width:100%;transition:all .3s ease 0s}
.box1 .icon,.box2,.box3,.box4,.box5 .icon li a{text-align:center}
.box10:after,.box10:before,.box1:after,.box1:before,.box2 .inner-content:after,.box3:after,.box3:before,.box4:before,.box5:after,.box5:before,.box6:after,.box7:after,.box7:before{content:""}
.box1,.box11,.box12,.box13,.box14,.partner-box,.box17,.box18,.box2,.box20,.box21,.box3,.box4,.box5,.box5 .icon li a,.box6,.box7,.box8{overflow:hidden}
.box1 .title,.box10 .title,.box4 .title,.box7 .title{letter-spacing:1px}
.box3 .post,.box4 .post,.box5 .post,.box7 .post{font-style:italic}

.partner-box{text-align:center;color:#fff;position:relative}
.partner-box .box-content,.partner-box:after{width:100%;position:absolute;left:0}
.partner-box:after{content:"";height:100%;background:linear-gradient(to bottom,rgba(0,0,0,0) 0,rgba(0,0,0,.08) 69%,rgba(0,0,0,.76) 100%);top:0;transition:all .5s ease 0s}
.partner-box .post,.partner-box .title{transform:translateY(145px);transition:all .4s cubic-bezier(.13,.62,.81,.91) 0s}
.partner-box:hover:after{background:linear-gradient(to bottom,rgba(0,0,0,.01) 0,rgba(0,0,0,.09) 11%,rgba(0,0,0,.12) 13%,rgba(0,0,0,.19) 20%,rgba(0,0,0,.29) 28%,rgba(0,0,0,.29) 29%,rgba(0,0,0,.42) 38%,rgba(0,0,0,.46) 43%,rgba(0,0,0,.53) 47%,rgba(0,0,0,.75) 69%,rgba(0,0,0,.87) 84%,rgba(0,0,0,.98) 99%,rgba(0,0,0,.94) 100%)}
.partner-box img{width:100%;height:auto}
.partner-box .box-content{padding:20px;margin-bottom:20px;bottom:0;z-index:1}
.partner-box .title{font-size:22px;font-weight:700;text-transform:uppercase;margin:0 0 10px}
.partner-box .post{display:block;padding:8px 0;font-size:15px}
.partner-box .social li a,.box17 .icon li a{border-radius:50%;font-size:20px;color:#fff}
.partner-box:hover .post,.partner-box:hover .title{transform:translateY(0)}
.partner-box .social{list-style:none;padding:0 0 5px;margin:40px 0 25px;opacity:0;position:relative;transform:perspective(500px) rotateX(-90deg) rotateY(0) rotateZ(0);transition:all .6s cubic-bezier(0,0,.58,1) 0s}
.partner-box:hover .social{opacity:1;transform:perspective(500px) rotateX(0) rotateY(0) rotateZ(0)}
.partner-box .social:before{content:"";width:50px;height:2px;background:#fff;margin:0 auto;position:absolute;top:-23px;left:0;right:0}
.partner-box .social li{display:inline-block}
.partner-box .social li a{display:block;width:40px;height:40px;line-height:40px;background:#6d3795;margin-right:10px;transition:all .3s ease 0s}
.box17 .icon li,.box17 .icon li a{display:inline-block}
.partner-box .social li a:hover{background:#bea041}
.partner-box .social li:last-child a{margin-right:0}
@media only screen and (max-width:990px){.partner-box{margin-bottom:30px}
}



.b-0 {
    bottom: 0;
}
.bg-shadow {
    background: rgba(76, 76, 76, 0);
    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(179, 171, 171, 0)), color-stop(49%, rgba(48, 48, 48, 0.37)), color-stop(100%, rgba(19, 19, 19, 0.8)));
    background: linear-gradient(to bottom, rgba(179, 171, 171, 0) 0%, rgba(48, 48, 48, 0.71) 49%, rgba(19, 19, 19, 0.8) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c4c4c', endColorstr='#131313', GradientType=0 );
}
.top-indicator {
    right: 0;
    top: 1rem;
    bottom: inherit;
    left: inherit;
    margin-right: 1rem;
}
.overflow {
    position: relative;
    overflow: hidden;
}
.zoom img {
    transition: all 0.2s linear;
}
.zoom:hover img {
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
}



    </style>
</head>
