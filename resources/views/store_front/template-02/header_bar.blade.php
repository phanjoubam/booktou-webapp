<?php

    $main_menu_name = request()->get('ACTIVE_MAIN_MODULE');

    if($main_menu_name == "")
    {
        $main_menu_name = Request::segment(1);
    }
 
?>

<header class="section-header">
    <section class="border-bottom">
        <div class="container pt-2">
            <div class="d-flex flex-wrap">
                <div class="flex-fill p-2 brand-logo text-center float-start">
                   
                    <a href="{{ URL::to('/') }}">
                        <img width='150px' style='margin-top: 10px;' src="{{ URL::to('/public/store/image/logo.png') }}" alt="bookTou">
                    </a>
                </div>

               

                <div class="flex-fill m-2 float-start">
 
                      <ul class="nav nav-mainmenu justify-content-center   d-md-none d-lg-none d-xlg-none"> 
                        <li class="nav-item">
                            <a href="{{ URL::to('/') }}" class="nav-link text-uppercase">Shopping</a>
                        </li>
                        <li class="nav-item ">
                            <a href="{{ URL::to('/booking') }}" class="nav-link text-uppercase">Booking</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ URL::to('/appointment') }}" class="nav-link text-uppercase">Appointment</a>
                        </li> 
                    </ul> 
 
                    <nav class="navbar navbar-expand-lg navbar-light bg-white d-none d-md-block mainnavbar"> 

                      <div id="navbarContent" class="collapse navbar-collapse ">
                        <ul class="navbar-nav nav-bubble mx-auto">
                          <!-- Megamenu-->

                          @switch($main_menu_name)
                            @case("shopping")
                            @php 
                                $keyword = "shopping";
                            @endphp
                            <li class="nav-item parent active green"><a href="{{ URL::to('/') }}" class="nav-link font-weight-bold text-uppercase">Shopping</a></li>
                          <li class="nav-item parent  "><a href="{{ URL::to('/booking') }}" class="nav-link font-weight-bold text-uppercase">Booking</a></li>
                          <li class="nav-item parent  "><a href="{{ URL::to('/appointment') }}" class="nav-link font-weight-bold text-uppercase">Appointment</a></li>
 
                            @break
                            @case("booking")
                            @php 
                                $keyword = "booking";
                            @endphp
                          <li class="nav-item parent "><a href="{{ URL::to('/') }}" class="nav-link font-weight-bold text-uppercase">Shopping</a></li>
                          <li class="nav-item parent active green"><a href="{{ URL::to('/booking/') }}" class="nav-link font-weight-bold text-uppercase">Booking</a></li>
                          <li class="nav-item parent "><a href="{{ URL::to('/appointment') }}" class="nav-link font-weight-bold text-uppercase">Appointment</a></li>
                            @break
                            @case("appointment")
                            @php 
                                $keyword = "appointment";
                            @endphp
                              <li class="nav-item parent "><a href="{{ URL::to('/') }}" class="nav-link font-weight-bold text-uppercase">Shopping</a></li>
                              <li class="nav-item parent "><a href="{{ URL::to('/booking') }}" class="nav-link font-weight-bold text-uppercase">Booking</a></li>
                              <li class="nav-item parent active green"><a href="{{ URL::to('/appointment') }}" class="nav-link font-weight-bold text-uppercase">Appointment</a></li>
                            @break

                            @default
                            @php 
                                $keyword = "shopping";
                            @endphp
                            <li class="nav-item parent active green"><a href="{{ URL::to('/') }}" class="nav-link font-weight-bold text-uppercase">Shopping</a></li>
                          <li class="nav-item parent"><a href="{{ URL::to('/booking') }}" class="nav-link font-weight-bold text-uppercase">Booking</a></li>
                          <li class="nav-item parent"><a href="{{ URL::to('/appointment') }}" class="nav-link font-weight-bold text-uppercase">Appointment</a></li>

                          @endswitch
  
                          @php
                          $subnav_class = "green";
                          @endphp 
                      </ul>
                  </div>
              </nav>
          </div>
  
<!-- section ends here -->
 <div class="flex-fill p-2 gsearch float-end">
                    <form action="{{ action('StoreFront\StoreFrontController@searchProducts') }}" class="search-header">
                        <div class="input-group w-100">
                            <input type="text" class="form-control" placeholder="Search" name='keyword'>
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                            <input type='hidden' name="key_category" value=" " />
                        </div>
                    </div>
                </form>
            </div>
    
</div>
</div>

<div class='main-menu subnav {{ $subnav_class }} d-none d-sm-block d-xs-block '>
    <div class='container'>
        <div class="row">
            <div class="col-9"> 
                <ul class="nav justify-content-center"> 
                @if(Session::has('module_active'))
                     @switch(Session::get('module_active'))
                     @case("shopping")

                        @foreach( request()->get('MENU_ITEMS') as $item )
                        <li class="nav-item">
                            <a class="nav-link" href="{{ URL::to( $item->navigate_to )  }}">{{ ucwords(strtolower($item->display_name )) }}</a>
                        </li>
                        @endforeach

                     @break

                     @case("appointment")
                          <?php
                            if (request()->get('WEB_MAIN_MENU_ITEMS')!="") {
                                foreach (request()->get('WEB_MAIN_MENU_ITEMS')  as $appointname) { 

                                    if ( strcasecmp($appointname->main_module,  "appointment")  == 0 ) {
                                 ?>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{$appointname->menulink}}">{{$appointname->mainMenu }}</a>
                                </li>

                                <?php
                                    }
                                }
                            }
                         ?>
                     @break

                     @case("booking")
                         <?php
                            if (request()->get('WEB_MAIN_MENU_ITEMS')!="") {
                                foreach (request()->get('WEB_MAIN_MENU_ITEMS')  as $appointname) {
                                    if ( strcasecmp($appointname->main_module,  "booking")  == 0 ) {
                                 ?>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{$appointname->menulink}}">{{$appointname->mainMenu }}</a>
                                </li>

                                <?php
                                  }
                                }
                            }
                         ?>
                     @break

                     @endswitch
                @endif
            </ul>
            </div>

            <div class="col-3">

            <div class="widgets-wrap float-md-right text-center ">
                    <div class="widget-header ">
                       <!--  <a class="widget-view" href="https://bit.ly/2Ln8C1Y">
                            <div class="icon-area">
                                <i class="fa fa-android"></i>
                            </div>
                            <small class="text">Get the app</small>
                        </a>


                            <a class="widget-view" href="http://bit.ly/bookTouBizApp">
                            <div class="icon-area">
                                <i class="fa fa-briefcase"></i>
                            </div>
                                <small class="text">Sell with us</small>
                            </a> -->

                            <a href="{{ URL::to('/') }}/shopping/checkout" class="widget-view">
                            <span class="badge badge-danger cart-counter">{{ request()->get('CART_COUNT') }}</span>
                            <div class="icon-area">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <small class="text"> Cart</small>
                        </a>
                        @auth

                        <a href="{{ URL::to('/') }}/settings/view-my-profile" class="widget-view">
                            <div class="icon-area">
                                <i class="fa fa-user-circle"></i>
                            </div>
                            <small class="text"> Account</small>
                        </a>

                         <a href="{{ URL::to('/') }}/shopping/my-orders" class="widget-view">
                            <div class="icon-area">
                                <i class="fa fa-file-text"></i>
                            </div>
                            <small class="text"> Orders</small>
                        </a>


                        <a href="{{ URL::to('/') }}/shopping/logout" class="widget-view">
                            <div class="icon-area">
                                <i class="fa fa-sign-out"></i>
                            </div>
                            <small class="text"> Logout</small>
                        </a>
                        @endauth
                        @guest
                        <a href="{{ URL::to('/') }}/shopping/login" class="widget-view">
                            <div class="icon-area">
                                <i class="fa fa-sign-in"></i>
                            </div>
                            <small class="text"> Login</small>
                        </a>
                        @endguest

                    </div>
                </div>
        </div>


        </div>
    </div>
</div>
</section>


<!-- flyout menu -->
<div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
  <div class="offcanvas-header">
    <h5 class="offcanvas-title" id="offcanvasExampleLabel">Main Menu</h5>
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
</div>
<div class="offcanvas-body"> 

    <ul class="nav flex-column">
      <li class="nav-item">
        <a href="{{ URL::to('/') }}" class="nav-link text-uppercase">Shopping</a>
    </li>
    <li class="nav-item">
        <a href="{{ URL::to('/booking') }}" class="nav-link text-uppercase">Booking</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
    </li>
    <li class="nav-item">
        <a href="{{ URL::to('/appointment') }}" class="nav-link text-uppercase">Appointment</a>
    </li>

</ul>


</div> 
</div>
</div>
<!-- flyout menu ends-->


<?php
    $module = session()->get('module_active');
?> 

<nav id="sidebar" class="sidebar">
  <div class="sidebar-header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-7 pl-0">


          @auth
          <a class="" href="#"><i class="fa fa-user-circle mr-3"></i>Hello,
            <small class="mt-1">{{session()->get('_full_name')}}</small></a>

          @endauth

          @guest

          <a class="btn btn-primary mr-3" href="{{ URL::to('/') }}/shopping/login">
            <i class="fa fa-user-circle mr-1"></i>Log In
          </a>
          @endguest

        </div>

        <div class="col-5 text-right">
        <a class=" btn btn-sm-info  active" href="{{URL::to('/')}}/shopping/checkout">
            <i class="fa fa-shopping-cart">

            </i>
            <span class="badge badge-danger">
                @if(request()->get('CART_COUNT')>0)
                {{request()->get('CART_COUNT')}}
                @else
                0
                @endif
        </span>
        </a>
        <button type="button" id="sidebarCollapseX" class="btn btn badge badge-danger">
            <!-- btn-link -->
        <i class="fa fa-close"></i>
        </button>
        </div>
      </div>
    </div>
  </div>

  <ul class="list-unstyled components links">

                     <div class="nav-shopping" style="display: none;">
                     <li>
                     <small><h5 class="text-uppercase text-center font-weight-bold">Shopping</h5></small>
                    </li>
                    <hr>
                        <li class="nav-item">
                            <a class="nav-link btn btn-success text-left text-white" href="{{URL::to('/')}}">
                                <i class="fa fa-home"> </i> <small>Shopping Home</small>
                            </a>
                        </li>
                        @foreach( request()->get('MENU_ITEMS') as $item )
                        <li class="nav-item">
                            <a class="nav-link" href="{{  $item->navigate_to  }}">
                                <small>{{ $item->item_name }}</small>
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <a href="https://bit.ly/2Ln8C1Y">
                                <small>Get the app</small></a>
                        </li>
                        <li>
                            <a href="http://bit.ly/bookTouBizApp">
                                <small>Sell with us</small></a>
                        </li>
                    </div>

                     <div class="nav-booking" style="display: none;">
                     <li>
                     <small><h5 class="text-uppercase text-center font-weight-bold">Booking</h5></small>
                    </li>
                    <hr>
                            <li class="nav-item">
                            <a class="nav-link btn text-left" style="background-color: yellow;" href="{{URL::to('/service/booking')}}">
                                <i class="fa fa-home"> </i> <small>All Bookable Services</small>
                            </a>
                            </li>

                          <?php
                            if (request()->get('BOOKING_MODULE_CATEGORY')!="") {
                                foreach (request()->get('BOOKING_MODULE_CATEGORY')  as $bookingname) {
                                 ?>

                                    <li class="nav-item">
                                    <a class="nav-link" href="{{$bookingname->menulink}}">
                                        <small>{{$bookingname->mainMenu }}</small></a>
                                    </li>

                                <?php
                                }
                            }
                         ?>
                         <li>
                            <a href="https://bit.ly/2Ln8C1Y">
                                <small>Get the app</small></a>
                        </li>
                        <li>
                            <a href="http://bit.ly/bookTouBizApp">
                                <small>Sell with us</small></a>
                        </li>
                     </div>


                        <div class="nav-appointment" style="display: none;">
                        <li>
                     <small><h5 class="text-uppercase text-center font-weight-bold">Appointment</h5></small>
                    </li>
                    <hr>

                            <li class="nav-item">
                            <a class="nav-link btn btn-warning text-left text-white" href="{{URL::to('/service/appointment')}}">
                                <i class="fa fa-home"> </i> <small>All Bookable Appointment Services</small>
                            </a>
                            </li>

                         <?php
                            if (request()->get('APPOINTMENT_MODULE_CATEGORY')!="") {
                                foreach (request()->get('APPOINTMENT_MODULE_CATEGORY')  as $appointname) {
                                 ?>

                           
                                    <li class="border-bottom">
                                       <small>   <a href="{{  $appointname->menulink  }}" style="color: #37b516;">
                                      <?php echo strtoupper($appointname->mainMenu); ?>
                                    </a>
                                       </small>
                                    </li>
                           

                                <?php
                                }
                            }
                         ?>
                            <li>
                                <a href="https://bit.ly/2Ln8C1Y">
                                    <small>Get the app</small></a>
                            </li>
                            <li>
                                <a href="http://bit.ly/bookTouBizApp">
                                    <small>Sell with us</small></a>
                            </li>
                     </div>

    <!-- category menu ends here -->

    @auth
    <li>
        <a href="https://bit.ly/2Ln8C1Y">
            <small>Get the app</small></a>
    </li>
    <li>
        <a href="http://bit.ly/bookTouBizApp">
            <small>Sell with us</small></a>
    </li>
    <li class="nav-item">

            <a  href="{{ URL::to('/') }}/settings/view-my-profile" class="widget-view">
            <small> Account </small>
            </a>

    </li>
    <li class="nav-item">
        <a  href="{{ URL::to('/') }}/shopping/my-orders" class="widget-view">
              <small> Orders </small>
        </a>
    </li>
    <li >

            <a class="btn btn-primary btn-sm mr-3" href="{{ URL::to('/') }}/shopping/logout">Logout</a>

    </li>

    @endauth


  </ul>
</nav>
</header>
