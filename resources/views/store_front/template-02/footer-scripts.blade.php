<script src="{{URL::to('/public/store/js/jquery-3.4.0.min.js')}}"></script>  
<script src="{{ URL::to('/public/store/js/popper.min.js') }}/"></script>
<script src="{{ URL::to('/public/store/vendor/bootstrap/5.2.0/js/bootstrap.min.js') }}/"></script> 
<script src="{{ URL::to('/public/store/js/ecom.js') }}?v=1.2"></script>
<script src="{{URL::to('/public/store')}}/vendor/jquery/jquery-ui.min.js"></script>
<script src="{{URL::to('/public/store')}}/vendor/fancybox/jquery.fancybox.js"></script>
<script src="{{URL::to('/public/store')}}/vendor/elevatezoom/jquery.elevatezoom.js"></script>
<script src="{{URL::to('/public/store/') }}/vendor/panZoom/panZoom.js"></script>
<script src="{{URL::to('/public/store')}}/vendor/ui-carousel/ui-carousel.js"></script>
<script src="{{URL::to('/public/store/js/zoom.js') }}"></script> 
<script src="{{ URL::to('/public/store') }}/vendor/pn/js/pignose.calendar.full.min.js" type="text/javascript"></script>
<script src="{{URL::to('/public/store/vendor/slick/js')}}/slick.min.js"></script>
<script src="{{URL::to('/public/store/js/jquery.timepicker.js')}}"></script> 
<script src="{{ URL::to('/public/store/vendor/swiper/js/swiper-bundle.min.js') }}"></script>
<script src="{{ URL::to('/public/assets/vendor/calendar/js/zabuto_calendar.min.js') }}"></script>
<script src="{{URL::to('/public/store/vendor/owl/js/owl.carousel.min.js')}}"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
$(document).ready(function() {


  $(".sidebarCollapse").on("click", function() {

    var name = $(this).attr("data-name");
    switch(name)
    {
      case 'shopping':
        $(".sidebar").addClass("active");
        $(".nav-"+name).css("display","block");
        $(".nav-booking").css("display","none");
        $(".nav-appointment").css("display","none");
        $('body').css('overflow', 'hidden');

      break;

      case 'booking':
        $(".sidebar").addClass("active");
        $(".nav-shopping").css("display","none");
        $(".nav-"+name).css("display","block");
        $(".nav-appointment").css("display","none");
        $('body').css('overflow', 'hidden');
      break;

      case 'appointment':
       $(".sidebar").addClass("active");
        $(".nav-shopping").css("display","none");
        $(".nav-booking").css("display","none");
        $(".nav-"+name).css("display","block");
       $('body').css('overflow', 'hidden');
      break;
    }
    });



    $("#sidebarCollapseX").on("click", function() {
    $("#sidebar").removeClass("active");
    $('body').css('overflow-y','scroll');
    });

    $(".sidebarCollapse").on("click", function() {
    var name = $(this).attr('data-name');
    if ($(".sidebar").hasClass("active")) {
      $(".overlay").addClass("visible");
      $('body').css('overflow', 'hidden');
    }
    });

    $("#sidebarCollapseX").on("click", function() {
    $(".overlay").removeClass("visible");
    $('body').css('overflow-y','scroll');
    });

<?php
// if (session()->get('__user_id_')!="") {
//       if (request()->get('assist-wizard') == session()->get('__random_wizard_') &&
//         request()->get('shopping') == session()->get('__random_shopping_')
//       )
//           {
?>

//           $('.booktouwidgetassist').modal('show');

<?php
//           }
//       }
?>


// chat
        //Toggle fullscreen
        $(".chat-bot-icon").click(function (e) {
            $(this).children('img').toggleClass('hide');
            $(this).children('svg').toggleClass('animate');
            $('.chat-screen').toggleClass('show-chat');
        });
        $('.end-chat').click(function () {
            $('.chat-body').addClass('hide');
            $('.chat-input').addClass('hide');
            $('.chat-session-end').removeClass('hide');
            $('.chat-header-option').addClass('hide');
        });


//         $('.carousel').slick({
//               rows: 1,
//               infinite: false,
//               slidesToShow: 4,
//               slidesToScroll: 3
// });
// chat ends here

       // var $chatbox = $('.chatbox'),
       // $chatboxTitle = $('.chatbox__title'),
       // $chatboxTitleClose = $('.chatbox__title__close'),
       // $chatboxCredentials = $('.chatbox__credentials');

       // $chatboxTitle.on('click', function() {
       // // $chatbox.toggleClass('buttom-btn');

       // $chatbox.toggleClass('chatbox--tray');


       // });

       //star review

       //star review ends here

       // $('.preferredtime').val(''); 
       // $('.preferredtime').wickedpicker(); 
      
       

});

//merchant list

$(document).on("click", ".showMerchantList", function()
{

 $("#mySidenav").css('width','100%');
 $('body').css('overflow', 'hidden');

});

$(document).on("click", ".showMerchantListX", function()
{

 $("#mySidenav").css('width','0');
 $('body').css('overflow-y', 'visible');

});

// list

$(".hbot-header").on("click", function() {

	if($(".hbot").hasClass("hbot-collapse"))
	{
		$(".hbot").removeClass("hbot-collapse");
	}
	else
	{
		$(".hbot").addClass("hbot-collapse");
	}

});

// $(document).on('click', '.showwizard', function() {

    <?php
//         if (session()->get('__user_id_')!="") {
    ?>

//     $('.booktouwidgetassist').modal('show');

    <?php
//     } else
//     {
    ?>

//     $('.assitswidgetModal').modal('show');

    <?php
//     }
    ?>


// });

$('.calendar').pignoseCalendar({

});

var url =  "<?php echo config('api_url') ?>";
//var url =  "//" + window.location.hostname ;

$(document).on("click", ".complete-your-request", function(){
  

  var pickupname =  $('.pickupname').val();
  var pickupphone = $('.pickupphone').val();
  var pickupaddress = $('.pickupaddress').val();
  var dest_name = $('.destinationname').val();
  var dest_phone = $('.destinationphone').val();
  var dest_address = $('.destinationaddress').val();
  var preferred_time = $('#item_servicedate').val(); 
  var fee = $('.servicefee').val();
  var desc = $('#task_details').val();
  var taskid = $('.taskid').val();
  var login = "assistRequest"; 
  var file_data = $('#photo').prop('files')[0];   
  var form_data = new FormData();                  
  form_data.append('photo', file_data);
  form_data.append('_token', "{{csrf_token()}}");
  form_data.append('pickupName',pickupname);
  form_data.append('pickupPhone',pickupphone);
  form_data.append('pickupAddress',pickupaddress);
  form_data.append('destName',dest_name);
  form_data.append('destPhone',dest_phone);
  form_data.append('destAddress',dest_address);
  form_data.append('preferredTime',preferred_time);
  form_data.append('serviceFee',fee);
  form_data.append('taskDetails',desc);
  form_data.append('taskId',taskid);
  form_data.append('login',login); 

  // var json ={};

  // json['_token'] = "{{csrf_token()}}";
  // json['pickupName'] = pickupname;
  // json['pickupPhone'] = pickupphone;
  // json['pickupAddress'] = pickupaddress;
  // json['destName'] = dest_name;
  // json['destPhone'] = dest_phone;
  // json['destAddress'] = dest_address;
  // json['preferredTime'] = preferred_time;
  // json['photo'] =  $('#photo').prop("files")[0];
  // json['serviceFee'] = fee;
  // json['taskDetails'] = desc;
  // json['taskId'] = taskid;
  // json['login'] = login; 

    $.ajax({
                        
            url : url +'/shopping/show-chat-login',
            type: 'post',
            data:  form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data)  
            {
        data = $.parseJSON(data); 
        if(data.status_code == 7000)
        { 

           // $('#photo').text(myResultText);        
          $('.view').html(data.html);
          $(".chat-box").stop().animate({ scrollTop: $(".chat-box")[0].scrollHeight}, "slow");
        } 
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again')
      }
      });
});

$(document).on("click", ".btncustomerchatlogin", function(){

  var cphone = $('#phone').val();
  var cpass = $('#password').val();

  if (cphone == "") {
        alert("phone no missing");
        $('#phone').focus();
        return false;
  }

  if (cpass=="") {
        alert("password missing");
        $('#password').focus();
        return false;
  }

  var json = {};
  json['phone'] = cphone;
  json['password'] = cpass;
  json['_token'] = "{{csrf_token()}}";

    $.ajax({
              url : url +'/shopping/customer/chat-login',
              type: 'post',
              data:  json,
              success: function (data)
              {
 
          data = $.parseJSON(data); 
          
          if(data.status_code == 1059)
          {
            
            $('.chat-box').empty();
            $('.chat-box').append('<div class="col-sm-12 col-md-12 col-lg-12 ">'+
              '<div class="row justify-content-center mt-5">'+
              '<img class="img-fluid mb-3" style="width:100px;" src="{{URL::to("public/assets/image/task.png")}}">'+
              '<p class="col-10">Your assist task request order no. <span class="text-danger">'+data.orderId+'</span> is completed. For more information please click the below link</p>'+
              '<a href="'+window.location+'shopping/my-order-details?key='+data.orderId+'" target="_blank"><span class="badge badge-success badge-pills"> View your order details</span></a>'+
              '</div></div>');
            
          }else  if (data.status_code==902) 
          { 
            alert('missing data to perform action');
          }
        },
        error: function()
        {
            alert(  'Something went wrong, please try again')
        }
      });

});

// ajax calling
</script>
