<footer class="section-footer bg-secondary">
    <div class="container">
        <section class="footer-top padding-y-lg text-dark">
            <div class="row"> 
                <aside class="col-md col-6">
                    <h6 class="title">Company</h6>
                    <ul class="list-unstyled">
                        <li> <a href="{{ url('/terms/about-us') }}">About Us</a></li>  
                        <li> <a href="{{ url('terms/terms-and-conditions') }}">Terms and Conditions</a></li>
                        <li> <a href="{{ url('/terms/privacy-policy') }}">Privacy Policy</a></li>
                        <li> <a href="{{ url('/terms/cancellation-and-refund') }}">Cancellation and Refund</a></li>
                        <li> <a href="{{ url('/terms/shipping-and-delivery') }}">Shipping And Delivery</a></li> 
                         

                    </ul>
                </aside>
                <aside class="col-md col-6">
                    <h6 class="title">Services</h6>
                    <ul class="list-unstyled">
                    <li>
                      <a target='_blank' href="http://bit.ly/bookTouBizApp">Sell with us</a>
                    </li> 
                <li>
                  <a  target='_blank' href="https://bit.ly/2Ln8C1Y">Get the app</a>
                </li> 

                        <li> <a href="https://booktou.in/careers">Career</a></li>
                        <li> <a href="{{ URL::to('/shopping/my-orders') }}">Order status</a></li>
                        <li> <a target='_blank' href="{{ URL::to('/downloads/drivers/pos/windows') }}">Downloads</a></li> 
                    </ul>
                </aside>
                <aside class="col-md col-6">
                    <h6 class="title">Account</h6>
                    <ul class="list-unstyled">
                        @guest
                        <li> <a href="https://booktou.in/shopping/login"> User Login</a></li>
                        <li> <a href="https://booktou.in/join-and-refer"> User register</a></li> 
                         @endguest
                        <li> <a href="/shopping/checkout"> My Shopping Cart</a></li>
                    </ul>
                </aside>
                <aside class="col-md">
                    <h6 class="title">Social</h6>
                    <ul class="list-unstyled">
                        <li><a href="https://www.facebook.com/bookTou.in" target='_blank'> <i class="fa fa-facebook"></i> Facebook </a></li> 
                        <li><a href="https://www.instagram.com/booktouu/"  target='_blank'> <i class="fa fa-instagram"></i> Instagram </a></li>
                        <li> <a href="https://booktou.in/contact-us">Contact Us</a></li> 
                    </ul>
                </aside>
            </div> <!-- row.// -->
        </section>  <!-- footer-top.// -->

        <section class=" text-center">
        
                <p class="text-dark">

                    bookTou - book a professional nearby, pick-and-drop an item, order food from restaurants on the go!

                </p>
                <p class="text-muted"> &copy {{ date('Y')}} bookTou, All rights are reserved. A product of <a target='_blank' href='https://ezanvel.in' style='color: #454545'>ezanvel</a></p>
                <br>
        </section>
    </div><!-- //container -->
</footer>


<div class="modal wg-cart" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Item Added to Cart</h5>
        <button type="button" class="close" data-bs-dismiss="modal"  data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row"> 
            <div class="col-md-12 sys-alert"> 
            </div>
            <div class="col-md-12 mt-2  col-sm-12"> 
                <div class='ecommsg'></div> 
            </div>
            <div class="col-md-4 mt-2  ecomimg"></div>
            
        </div>
 
  
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='w1-key'/>    
        <a role="button"  href="{{ URL::to('/shopping/checkout') }}"  class="btn btn-danger">Go to Cart</a>
        <a href="" role="button" class="btn btn-primary">Continue Shopping</a>
      </div>
    </div>
  </div>
</div> 


 <div class="modal wg-alert" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">bookTou Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row"> 
            <div class="col-md-12">
                <span class=''>Copy below URL and share to your friends via facebook, whatsapp, email etc.</span>
                <br/>
                <input type='text' id='shareurl' class='form-control' value="{{ Request::url() }}" />
            </div> 
        </div> 
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='w1-key'/>     
        <button type="button" class="btn btn-primary btncopyurl"  >Copy</button>
      </div>
    </div>
  </div>
</div> 

 