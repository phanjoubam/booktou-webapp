<header class="section-header">
<section class="header-main border-bottom">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-3 col-md-12">
                <a  class="brand-wrap" href="{{ URL::to('https://booktou.in') }}">
                <img width='120px' src="{{ URL::to('/public/store/image/logo.png') }}" alt="bookTou"></a> 
                </a> 
            </div>
            <div class="col-xl-6 col-lg-5 col-md-6">
                <form action="{{ action('StoreFront\StoreFrontController@searchProducts') }}" class="search-header">
                    <div class="input-group w-100">
                        <select class="custom-select border-right"  name="category_name">
                            <option value="SHOPPING">SHOPPING</option>
                            <!--
                            @foreach(request()->get('BUSINESS_CATEGORIES') as $item)
                              <option value="{{ $item->name }}">{{ $item->name }}</option>
                            @endforeach 
                            -->
                        </select>
                        <input type="text" class="form-control" placeholder="Search" name='keyword'>
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i> Search
                          </button>
                        </div>
                    </div>
                </form> <!-- search-wrap .end// -->
            </div> <!-- col.// -->
            <div class="col-xl-4 col-lg-4 col-md-6 d-none d-sm-block">
                <div class="widgets-wrap float-md-right">  
                    <div class="widget-header">
                         <a href="{{ URL::to('/') }}/shopping/checkout" class="widget-view">
                            <div class="icon-area">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <small class="text"> My Cart</small>
                        </a>
                        @auth
                        <a href="{{ URL::to('/') }}/shopping/logout" class="widget-view">
                            <div class="icon-area">
                                <i class="fa fa-sign-out"></i>
                            </div>
                            <small class="text"> Logout</small>
                        </a>
                        @endauth
                        @guest
                        <a href="{{ URL::to('/') }}/shopping/login" class="widget-view">
                            <div class="icon-area">
                                <i class="fa fa-sign-in"></i>
                            </div>
                            <small class="text"> Login</small>
                        </a>
                        @endguest


                    </div>
                </div> <!-- widgets-wrap.// -->
            </div> <!-- col.// -->
        </div> <!-- row.// -->
    </div> <!-- container.// -->
</section> <!-- header-main .// -->

 
</header> <!-- section-header.// -->