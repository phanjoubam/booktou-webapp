@extends('layouts.store_front_02')
@section('content')
<?php 
    $cdn_url =  config('app.app_cdn');  
    $cdn_path = config('app.app_cdn_path');
?> 

  

@if( count($slides) > 0 ) 
    @php 
    $pos=0;
    @endphp 
    <div class="swiper indexSlider">
      <div class="swiper-wrapper">

        @foreach( $slides  as $slide)     
        <div class="swiper-slide"> 
                <img src="{{  URL::asset($slide->image_url) }}"  alt="{{ $slide->remarks }}"> 
        </div> 
            @php 
            $pos++;
            @endphp 
        @endforeach  
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
    </div>
@endif 


<div class='mt-2'>
    <div class="container">
   
<section >
 
<div class="row mt-2"> 

    @php $i=0; @endphp  
    @foreach(request()->get('BUSINESS_CATEGORIES') as $biz_category)

        @if( strcasecmp($biz_category->main_module, "shopping" ) == 0 )
            <div class="col-md-1 col-4 col-sm-4 col-xs-4 mb-2 text-center">
                <a href="{{URL::to('/shopping/browse-by-business-category') }}/{{ strtolower( $biz_category->name ) }}">
                    <img src="{{$biz_category->icon_url}}" class="img-fluid rounded-circle  p-2">
                    <span class='text-sm'>{{$biz_category->name}}</span>
                </a>
            </div> 
        @else
            <div class="col-md-1 col-4 col-sm-4 col-xs-4 mb-2 text-center">
                <a href="{{URL::to('/booking/browse-by-business-category') }}/{{ strtolower( $biz_category->name ) }}">
                    <img src="{{$biz_category->icon_url}}" class="img-fluid rounded-circle  p-2">
                    <span class='text-sm'>{{$biz_category->name}}</span>
                </a>
            </div> 
        @endif
    @endforeach

       
      
</div>
</section> 
 

<!-- =============== SECTION DEAL =============== -->
<section class="padding-bottom mt-3">

 <header class="section-heading heading-line">
        <h4 class="title-section text-uppercase">Hot Deals of the Day</h4>
 </header>
 <div class="card card-deal">
   
   <div class="row no-gutters items-wrap"> 

    @php 
            $i=0; 
            @endphp
            @foreach($deals as $product)  
            <?php 

            if ($product->photos=="") {
                $image_url = $cdn_url .  "/assets/image/no-image.jpg";
            }else{
                $image_url = $product->photos;
            }

            $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name))); 
            ?>             
          <div class="col-md-2 col-6 col-sm-6 col-xs-6 mb-2">
               <figure class="card-product-grid card-sm ">
                 <a href="{{URL::to('/shop/view-product-details')}}/{{$formatted_name}}/{{strtolower(  $product->prsubid)}}"  class="img-wrap">  
                  <img src="{{$image_url}}" class="img-fluid"> 
                </a>
                <div class="text-wrap p-3">
                  <a href="{{URL::to('/shop/view-product-details')}}/{{$formatted_name}}/{{strtolower(  $product->prsubid)}}" class="title">{{ $product->pr_name }}</a>
                  <span class="badge badge-danger">{{ $product->unit_price }}</span>
                </div>
             </figure>
           </div>
      @php 
        $i++; 
      @endphp
  @endforeach

</div>
</div>

</section>
 

<section class="padding-bottom mt-3">
 <header class="section-heading heading-line">
        <h4 class="title-section text-uppercase">Premium Business</h4>
 </header>
 <div class="card card-deal">
   
   <div class="row no-gutters items-wrap">

    @foreach($permium_business as $premium)
    <div class="col-md-2 col-6 col-sm-6 col-xs-6">
         <figure class="card-product-grid card-sm">
          <a href="{{URL::to('/')}}/business/btm-{{$premium->id}}"  class="img-wrap"> 
           <img src="{{$cdn_url}}{{$premium->banner}}" class="img-fluid"> 
          </a>
          <div class="text-wrap p-3">
            <a href="{{URL::to('/')}}/business/btm-{{$premium->id}}" class="title">{{$premium->name }}</a> 
          </div>
       </figure>
    </div>
    @endforeach

</div>
</div>
</section>


<!-- section details -->
@foreach($section as $items)
 <section class="padding-bottom">
<header class="section-heading heading-line">
    <h4 class="title-section text-uppercase">{{$items->title}}</h4>
</header>

<!-- <div class="card card-home-category"> -->
<div class="row row-sm">
    
 
<!-- <div class="col-md-12"> -->
 
 
  @foreach($sectionDetails as $details)
   <?php if ($details->photos=="") {
            $image_url =  $cdn_url .  "/assets/image/no-image.jpg"; 
    }else{
            $image_url =  $details->photos ; 
    } 
    $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($details->pr_name))); 
    ?>
  @if($details->section_id==$items->id)
  <!-- <li class="col-6 col-lg-3 col-md-4">
    <a href="{{  URL::to('/shop/view-product-details')}}/{{$formatted_name}}/{{strtolower(  $details->prsubid) }}" class="item"> 
        <div class="card-body">
            <h6 class="title">{{ $details->pr_name }}</h6>
            <img class="img-sm float-right" src="{{ $image_url }}"> 
            <p class="text-muted"><i class="fa fa-map-cutlery"></i>{{$details->biz_name}}</p>
        </div>
    </a>
  </li> -->
  <div class="col-xl-2 col-lg-3 col-md-4 col-6">
        <div class="card card-sm card-sectionproduct-grid">
             <a href="{{URL::to('/shop/view-product-details')}}/{{$formatted_name}}/{{strtolower(  $details->prsubid)}}"  class="img-wrap"> <img src="{{ $image_url }}"> </a>
            <div class="card-footer">
              <a href="{{URL::to('/shop/view-product-details')}}/{{$formatted_name}}/{{strtolower(  $details->prsubid)}}" class="title">{{ $details->pr_name }}</a>
              <div class="price mt-1">{{$details->actual_price}}</div>  
            </div> 
        </div>
    </div> 

  @endif
  @endforeach
 
    <!-- </div> --> <!-- col.// -->
</div> <!-- row.// -->
<!-- </div> --> <!-- card.// -->
</section>
@endforeach
<!-- section details ends here -->


 
 
 


 
<section  class="padding-bottom-sm"> 
<header class="section-heading heading-line">
    <h4 class="title-section text-uppercase">Recommended Products</h4>
</header>

<div class="row row-sm">

  @foreach($recommended_list as $product)
  <?php 
  if ($product->photos=="") {
     $image_url =  $cdn_url .  "/assets/image/no-image.jpg";
  }else{
    $image_url =  $product->photos;
  }
  
  $formatted_name = strtolower(  preg_replace('/[^A-Za-z0-9\-]/', '-',  trim($product->pr_name))); 

  ?> 
    <div class="col-xl-2 col-lg-3 col-md-4 col-6">
        <div class="card card-sm card-product-grid">
             <a href="{{  URL::to('/shop/view-product-details') }}/{{  $formatted_name  }}/{{ strtolower(  $product->prsubid ) }}"  class="img-wrap"> <img src="{{ $image_url }}"> </a>
            <figcaption class="info-wrap">
                <a href="#" class="title">{{ $product->pr_name }}</a>
                <div class="price mt-1">{{ $product->actual_price }} </div>  
            </figcaption>
        </div>
    </div> 
  @endforeach 
</div>  
</section>
 
 
 

<article class="my-4">
    <img src="{{ URL::to('/public/store/image/ad-horizontal.jpg') }}" class="w-100">
</article>
</div>  
 
 

</div> <!-- container -->
</div> <!-- spacer -->

<!-- chat bot system -->
<div class="chat-screen">
    <div class="chat-header">
        <div class="chat-header-title text-center">
          <b>bookTou assist</b>
        </div>
    </div>
    <div class="chat-mail chat-box overflow-auto block"  style="height: 450px;">
        <div class="row">
            <div class="col-12 text-center mb-2">
               <!-- <div class="chat-body"> -->
                <!-- <p>What do you want to explore?</p> -->
                    <!-- <div class="chat-bubble you"></div> -->
                     <!-- <a href="#" class="btn btn-primary btn-sm">Shopping</a>
                     <a href="#" class="btn btn-primary btn-sm">Booking</a>
                     <a href="#" class="btn btn-primary btn-sm">Appointment</a>
 -->            

                    <?php  
                       if (session()->get('__user_id_')!="") { 
                    ?>
                    @include('store_front.assist.chat_assist')
                    
                    <?php  
                        }else{
                    ?>
                    
                    @foreach($bot as $items)
                        <ul class="mt-2">
                            <li class="list-group-item"><a class="btnchat" data-chatquestion="{{$items->question}}" href="javascript::void();">{{$items->question}}</a></li>
                        </ul>
                    @endforeach
                    
                    <?php }?>

                    <div class="chat-body mt-2 showchatresult block">
                    <div class="scrollChat">

                    </div>
                    </div>
                <!-- </div> -->
            </div> 
        </div>
    </div>
     
 </div>

<div class="chat-bot-icon">
    
 <!-- <svg  width="24" height="24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="bi bi-chat-left-text animate" viewBox="0 0 16 16">
  <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H4.414A2 2 0 0 0 3 11.586l-2 2V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12.793a.5.5 0 0 0 .854.353l2.853-2.853A1 1 0 0 1 4.414 12H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
  <path d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
</svg> -->
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="bi bi-chat-dots animate" viewBox="0 0 16 16">
  <path d="M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
  <path d="m2.165 15.803.02-.004c1.83-.363 2.948-.842 3.468-1.105A9.06 9.06 0 0 0 8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6a10.437 10.437 0 0 1-.524 2.318l-.003.011a10.722 10.722 0 0 1-.244.637c-.079.186.074.394.273.362a21.673 21.673 0 0 0 .693-.125zm.8-3.108a1 1 0 0 0-.287-.801C1.618 10.83 1 9.468 1 8c0-3.192 3.004-6 7-6s7 2.808 7 6c0 3.193-3.004 6-7 6a8.06 8.06 0 0 1-2.088-.272 1 1 0 0 0-.711.074c-.387.196-1.24.57-2.634.893a10.97 10.97 0 0 0 .398-2z"/>
</svg>
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x "><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
</div>





<!-- chat bot ends here -->



<!-- section for booktou assists --> 
 
   <!-- login successfull! -->
    @auth
    <div class="modal fade widgetassist"   data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center col" id="staticBackdropLabel">How may i assist you?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      
      <p class="text-center">
                <small>Pick up, purchase or send an item from anywhere inside the Imphal Town</small>
      </p>

      <div class="modal-body">
        <div class="row">
            @if(isset($assists))
            @foreach($assists as $items)
            <div class="col-md-2 col-4 col-sm-4 col-xs-4 mb-2 text-center p-1">
                <input type="hidden" value="{{$items->assist_cat_name}}" name="task_{{$items->id}}" required>
                <a href="{{URL::to('shopping/assist-pickup/'.$items->id)}}">
                  
                    <div class="card">
                       
                      <img src="{{$items->icon_url}}" class="img-fluid rounded-circle  p-2">
                      
                      <h7 class="card-title"><small>{{$items->assist_cat_name}}</small></h7>
                    </div>
                  
                </a>
            </div> 
            @endforeach
            @endif
              
        </div>
      </div>
      <div class="modal-footer">
        <p class="text-center" style="color:red;"><small>We do not deliver cigarettes, alcohols, drugs, guns and other illegal items which are prohibited by the law.
         (Fragile/value items is not recommended)</small>
        </p>
      </div>
    </div>
   </div>
   </div> 

   <!-- new wizard template for booktou assists -->
   <div class="modal fade booktouwidgetassist"   data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">  
    <div class="modal-content p-5 m-3"> 
     <div class="modal-header">
        <h4 class="modal-title text-center col" id="staticBackdropLabel"><img src="{{asset('/public/store/image/favicon-96x96.png')}}"> How may i assist you?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      
      <p class="text-center mb-3">
                <small>Pick up, purchase or send an item from anywhere inside the Imphal Town</small>
      </p>

    <p class="sys-alert"></p>
    <form method="post" action="{{action('StoreFront\StoreFrontController@assistCompleteYourRequest')}}" enctype="multipart/form-data">
        {{csrf_field()}}


    <div class="modal-body wizardsteps">
        
        <div class="col-md-6 offset-md-3 mt-3">
                    @if(isset($assists))
                        <select class="form-control" name="assistcategory" required id="assistcategory">
                            <option value="">Select assist category</option>
                            @foreach($assists as $items)
                            <option value="{{$items->id}}">{{$items->assist_cat_name}}</option>
                            @endforeach
                        </select>
                    @endif

                    <div class="col-md-6 offset-md-3 mt-5">
                        <button type="button" class="btn btn-warning btn-block btn-assist-proceed">Proceed</button>
                    </div>
        </div>
        
    </div>
    </form>
    <div class="modal-footer">
       
    </div>
    <div class="col-md-6 offset-md-3">
        <div class="offset-md-4"> 
            <img src="{{asset('/public/store/image/stop.png')}}"> 
            <img src="{{asset('/public/store/image/smoking.png')}}"> 
            <img src="{{asset('/public/store/image/drinks.png')}}">
        </div>
        <p class="text-center" style="color:red;"><small>We do not deliver cigarettes, alcohols, drugs, guns and other illegal items which are prohibited by the law.
         (Fragile/value items is not recommended)</small>
        </p>
    </div>
    </div>
    </div>
    </div>

   <!-- booktou assist wizard ends here -->
    @endauth
    <!-- successfull login ends here -->
    
    @guest
    


    <!-- spinner --> 
    <!-- <div id="cover-spin"></div>-->

<!-- login dialogue box -->
<div class="modal fade assitswidgetModal" id="assitswidgetModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered ">
     <form action="{{ action('Auth\LoginController@customerLogin') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" id="srvukey" name="refererlogin" value="<?php echo $nos = bin2hex(random_bytes(24));?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Customer Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
         <div class="col-12 mb-3">
                                    <label for="phone">Phone Number or Email: <span>*</span></label>
                                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email' required>
                                </div>
                               
                               
                                <div class="col-12 mb-4">
                                    <label for="email_address">Password: <span>*</span></label>
                                    <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password' required>
                                </div>
                            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-block" value='login' name='login'>Login</button>
        
        
      </div>
    </div>
    </form>
  </div>
</div> 
<!-- login dialog box ends here -->

@endguest  

<!-- booktou assists ends here -->
@endsection 




@section('script')

<style type='text/css'>
 /*search box css start here*/
.search-sec{
    padding: 2rem;
}
.search-slt{
    display: block;
    width: 100%;
    font-size: 0.875rem;
    line-height: 1.5;
    color: #55595c;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
.wrn-btn{
    width: 100%;
    font-size: 16px;
    font-weight: 400;
    text-transform: capitalize;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
@media (min-width: 992px){
    .search-sec{
        position: relative;
        top: -114px;
        background: rgba(26, 70, 104, 0.51);
    }
}

@media (max-width: 992px){
    .search-sec{
        background: #1A4668;
    }
}

.modal-dialog1 {
    max-width: 90% !important;
    margin: 1.75rem auto;
}
.modal-content1 {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    width: 100% !important;
    height: 90% !important;
    pointer-events: auto;
    background-color: #fff !important;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,0.2);
    border-radius: .55rem;
    outline: 0;
}

/*hover effect*/

</style>

 <script>


 var swiper = new Swiper(".indexSlider", {
        cssMode: true,
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
        },
        mousewheel: true,
        keyboard: true,
      });


$(document).ready(function() {
 
   

   $('.product_thumbnail_slides').owlCarousel({
            items: 1,
            margin: 0,
            loop: true,
            nav: true,
            navText: ["<img src='img/core-img/long-arrow-left.svg' alt=''>", "<img src='img/core-img/long-arrow-right.svg' alt=''>"],
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 1000
        });
 
     
});


 
        
      

</script>


@endsection 