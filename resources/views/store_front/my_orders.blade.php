@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public

    $total_cost = $delivery_cost = 0.0;

?>

 
 

<div class='outer-top-tm'> 
<div class="container">
<div class="row">

     <div class="col-12">
                    <div class="page-title text-center">
                        <h1>Your Order Details</h1>
                    </div>
                </div>


  <div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1"> 

        @if (session('err_msg')) 
            <div class="alert alert-info">
              {{ session('err_msg') }}
            </div> 
        @endif                
  </div>  
<div class="col-sm-12 col-md-12 col-lg-10 offset-md-1 offset-lg-1"> 
        
        

    @foreach($data['bookings'] as $item) 
<div class="card mb-3">
<div class="row">
  <div class="col-9 ">
    <div class="card m-3">
      <div class="card-body">
         <div class="order-details-confirmation" style='margin-bottom: 20px;'>              
            <div class="cart-page-heading mb-30">
                <h5>Order No: {{$item->id}} <span class="badge badge-danger">{{$item->type}} </span></h5>
            </div>

        @if( $item->type == "normal")  

            
        @php 
        $total_cost =0;
        $delivery = 0;
        @endphp
            
        
            <table class="table table-colored">
            <tr>
                     
                    <th style='width:250px; text-align:left' >Item</th>
                    <th>Unit Price</th>    
                    <th>Quantity</th>    
                    <th class='text-right'>Sub-Total</th>
            </tr>  

            
            
                 
                 

           @foreach ($item->shoppingItems as $product)  
                 
              <tr>  <td style='width:250px; text-align:left' ><strong>{{$product->pr_name}}<strong></td>
                <td>{{$product->price}}</td>    
                <td>{{$product->qty}}</td>    
                <td class='text-right'> {{ $product->price *  $product->qty }} </td>
              </tr>  
                @php 
                $total_cost += ($product->price *  $product->qty ); 
                $delivery += $item->delivery_charge;  
                @endphp

            @endforeach
               
           
            
               
                
 
       
            <tr>
                <td colspan='2'></td>    
                <td><strong>Item Total Cost:</strong></td>    
                <td class='text-right'> {{$total_cost}}</td>
            </tr>    
        <tr>
                <td colspan='2'></td>    
                <td><strong>Delivery Charge:</strong></td>    
                <td class='text-right'> {{$delivery}}</td>
        </tr>
<tr>
                <td colspan='2'></td>    
                <td><strong>Total Order Cost:</strong></td>    
                <td class='text-right'> {{$total_cost + $delivery}}</td>
        </tr>

 
</table>

@elseif($item->type == "booking")
        
        @php 
        $total_cost =0;
        $delivery = 0;
        @endphp
     <table class="table table-colored">
            <tr>
                    <th style='width:250px; text-align:left' >Item</th>
                    <th>Amount</th>    
                    <th>Service Date</th>    
                    <th class='text-right'>Sub-Total</th>
            </tr> 
            
            @foreach ($item->shoppingItems as $product)  
               <tr> 
                <td style='width:250px; text-align:left' ><strong>{{$product->service_name}}<strong></td>
                <td> {{$item->total_cost}}</td>    
                <td> {{$item->service_date}}</td>    
                <td class='text-right'> {{$item->total_cost}}</td>
                </tr>  

                @php 
                $total_cost += $item->total_cost; 
                 $delivery += $item->delivery_charge;   
                @endphp
            @endforeach
               
            
            
        
 
       
            <tr>
                <td colspan='2'></td>    
                <td><strong>Item Total Cost:</strong></td>    
                <td class='text-right'> {{$total_cost}}</td>
            </tr>    
        <tr>
                <td colspan='2'></td>    
                <td><strong>Delivery Charge:</strong></td>    
                <td class='text-right'>{{$delivery}}</td>
        </tr>
<tr>
                <td colspan='2'></td>    
                <td><strong>Total Order Cost:</strong></td>    
                <td class='text-right'> {{$total_cost + $delivery}}</td>
        </tr>

 
 </table>

                 

@endif


        </div>
      </div>
    </div>




  </div>




  <div class="col m-3">
    <div class="card">
      <div class="card-body">
         
         @if($item->book_status=="canceled" || $item->book_status=="completed"||  $item->book_status=="delivered")
             
            <label class="badge badge-success"><small>Order Date: {{$item->book_date}}</small></label> 

            <label class="badge badge-warning"><small>Order Status: {{$item->book_status}}</small></label>    
             
            @elseif($item->book_status=="in_route")

            @else

            <label class="badge badge-success"><small>Order Date: {{$item->book_date}}</small></label> 

            <label class="badge badge-warning"><small>Order Status: {{$item->book_status}}</small></label>
            <br>

            <a class="badge mb-1" href="#"><i class="fa fa-shopping-cart"></i> <small>View Order</small></a>
            <br>
             <a class="badge" href="#"><small><i class="fa fa-car"></i> Track Order</small></a> 
            

         @endif   
      </div>
    </div>
  </div>
</div>  

    </div>
    @endforeach
    </div> 
</div>
</div>
 </div>
 


  
 
@endsection 


@section('script')

<script>


$(document).on("click", ".btncancel", function()
{
   
    var widget = $(this).attr('data-widget');  
    var params  = $(this).attr("data-param");

    $.each( JSON.parse(params)  , function(selector, value) {
        $("#" + selector).val(value); 
    });
 
    $(".wg-" + widget).modal( "show" );



});

</script>  

@endsection 