@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>

 


 
<div class="container mt-5">
<div class="row">
  

                <div class="jumbotron">
  <h1 class="display-4">Hey {{  $data['profile']->fullname }}!</h1>
  <p class="lead">Your referral code is ready. Invite your friends and family to join bookTou by using your referral code and earn cashback on purchases.</p>
  <hr class="my-4">
  
 <div class="form-row">
    <h3 class="display-5">Referral Code:</h3>
    <div class="col-md-4">
     <input class="form-control form-control-lg rcode" readonly type="text" value="{{  $data['profile']->referral_code }}">
    </div>
   
  </div>
               

 <hr class="my-4">

 <div class="form-row">
  <h3 class="display-5">Referral URL:</h3>
    <div class="col-md-8">
     <input class="form-control form-control-lg" readonly type="text" value="https://booktou.in/join-and-refer/{{  $data['profile']->referral_code }}">
    </div>
 
  </div>




  
</div>


</div>
</div> 

@endsection 