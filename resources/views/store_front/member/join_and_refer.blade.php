@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<section class="single_product_details_area d-flex align-items-center box-lg full-form mt-5" >
<div class="container">
<div class="row">
   <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3">


    <div class="card" style='margin-bottom: 40px;' >
  <div class="card-body">

                    <div class="checkout_details_area mt-50 clearfix ">
 

                        <div class="cart-page-heading mb-30">
                            <h5>Join - Refer - Earn</h5>
                        </div>

                        <form action="{{ action('StoreFront\StoreFrontController@joinAndGenerateReferralLink') }}" method="post">
                            {{ csrf_field() }} 


 @if (session('err_msg'))
  <div class="row"> <div class="col-md-12">
  <div class="alert alert-info">
    {{ session('err_msg') }}
    </div></div></div>
@endif

                           

                               <div class="form-row mt-4">
                                <div class="col-md-6">
                                  <label for="firstname">First Name: <span>*</span></label>
                                    <input type="text" class="form-control" id="firstname" value="" name='firstname' placeholder='First Name'>
                                </div>
                                <div class="col-md-6">
                                  <label for="lastname">Last Name: <span>*</span></label>
                                    <input type="text" class="form-control" id="lastname" value="" name='lastname' placeholder='Last Name'>
                                </div>
                              </div>

                              <div class="form-row mt-4">
                                <div class="col-md-6">
                                   <label for="phone">Phone Number: <span>*</span></label>
                                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number'>
                                </div> 
                              </div>

                              <div class="form-row mt-4">
                                <div class="col-md-6">
                                  <label for="password">Password: <span>*</span></label>
                                    <input type="password" class="form-control" id="password" value="" name='password' placeholder='Password'>
                                </div>
                                <div class="col-md-6">
                                  <label for="confpassword">Confirmation Passwod: <span>*</span> </label>
                                    <input type="password" class="form-control" id="confpassword" value="" name='confpassword' placeholder='Confirm your password'>
                                    <small id='err_pwd'  style='visibility:  hidden;color: red'>Mismatch confirmation password</small>
                                </div>
                              </div>


                               <div class="form-row mt-4">
                                <div class="col-md-12">
                                   <label for="address">Address: <span>*</span></label>
                                    <input type="text" class="form-control" id="address" name='address' value="" placeholder='Your address'>
                                </div>
                                
                              </div>

                               <div class="form-row mt-4">
                                <div class="col-md-12">
                                   <label for="landmark">Landmark: <span>*</span></label>
                                    <input type="text" class="form-control" id="landmark" name='landmark' value="" placeholder="Nearest popular place such as school, club, bazaar etc">
                                </div>
                                
                              </div>

                               <div class="form-row mt-4">
                                 <div class="col-md-5">
                                   <label for="city">City: <span>*</span></label><br/>
                                    <select  class="form-control" id="city" name='city' >
                                      <option>Imphal East</option>
                                      <option>Imphal West</option>
                                      <option>Thoubal</option>
                                      <option>Bishnupur</option>
                                      <option>Kakching</option>
                                      <option>Chandel</option>
                                    </select>
                                </div>
<div class="col-md-4">
                                   <label for="state">State: <span>*</span></label>
                                    <input type="text" class="form-control" id="state" name='state' readonly value="Manipur" >
                                </div>
                                <div class="col-md-3">
                                   <label for="pin">Pin Code: <span>*</span></label>
                                    <input type="text" class="form-control" id="pin" name='pin' value="" >
                                </div> 
                              
                                
                                
                              </div>

                              
                              @if(isset($data['referrer']))
                               <div class="form-row mt-4">
                                <div class="col-md-12">
                                   <label for="referred">Referred By: <span>*</span></label>
                                    <input type="text" readonly class="form-control" id="referred" name='referred' value="{{  $data['referrer']->fullname }}"  >
                                </div> 
                              </div>
                              @endif



                               <div class="form-row mt-4">
  <div class="col-12 mb-4">
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="terms">
  <label class="form-check-label" for="terms"> By clicking this checkbox, I confirm, I read and also agree to the <a href='{{ URL::to("/terms-and-conditions") }}' target='_blank'>terms of use</a> and <a target="_blank" href='{{ URL::to("/privacy-policy") }}'>privacy policty</a> of bookTou service.
  </label>
</div>
</div>
</div>
 <div class="form-row mt-4">
                                <div class="col-12 mb-4">
                                
                                <button type="submit" value='save' name='save' class="btn btn-success btnjoin">Join Now</button>
 

                            </div> </div>
                        </form>
                    </div>
                    </div>


                    </div>
                </div>
</div>
</div>
</section> 

@endsection 
@section('script')
 

<script>

$("#confpassword").on('change keyup paste', function() {
    
    var password = $("#password").val();
    var confpassword = $("#confpassword").val();

    if(password != confpassword){

        $("#err_pwd").css("visibility", "visible");
        $(".btnjoin").attr("disabled", true);
    }
    else 
    {
        $("#err_pwd").css("visibility", "hidden");
        $(".btnjoin").removeAttr("disabled");
    }


});

</script>


@endsection 