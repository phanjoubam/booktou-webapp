@extends('layouts.store_front_02')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public

    $total_cost = $delivery_cost = 0.0;

?>


<div class='outer-top-tm'> 
    <div class="container">  
    
   
<div class="row">

            <div class="col-12 col-md-8 mt-3">
                 <div class="card">
 <div class="card-body">
   

                   
 <?php 

$item = $data['profiles'];
 
?>
          <div class="text-center"> 
        
        <?php 

            if($item->cusImage=="")
             { 
                $image_url =  $cdn_url  .  "/assets/image/no-image.jpg"; 
             } 
          else
             {
              
                $image_url =  $cdn_url  . '/assets/image/member/'  . $item->cusImage;
             }     
         ?>
      <img class="rounded" src='{{ $image_url }}' alt="..."   width="140px">
              
       </div>
       <br/>

       <div class="text-center"><h3 class="text-danger">{{$item->cusFullName}}</h3></div>

<br/> 
      <hr class="my-4" />
<div class="row">
    <div class="col-12 col-md-6">
        <div><strong>Customer ID # :</strong> {{$item->cusProfileId}}</div><br/>
        <div><strong>Date of Birth :</strong> {{date('d-m-Y', strtotime($item->cusDateOfBirth))}}</div>
    </div>

    <div class="col-12 col-md-6">
        <div><strong>Contact No. :</strong> {{$item->cusPhone}}</div><br/>
        <div><strong>Email : </strong><span class="text-primary"> <?php echo $item->cusEmail ; ?></span></div>
    </div>
</div> 

       <hr class="my-4" /> 
   <div class="row">
    <div class="col-12  ">     
        <h5>Primary Address</h5>  
        <hr/>
    </div>
    <div class="col-12 col-md-6">    
                   
   
                 <div><strong>Locality :</strong> {{$item->cusLocality}}</div><br/>
                 
                 <div><strong>City :</strong> {{$item->cusCity}}</div><br/>          
                  <div><strong>Pin Code :</strong> {{$item->cusPin}}</div>
       </div>

    <div class="col-12 col-md-6">
                 <div><strong>Landmark :</strong> {{$item->cusLandmark}}</div><br/>
         
                  <div><strong>State :</strong> {{$item->cusState}}</div>
    </div>
  </div>
       

 </div>

      </div>
   </div>


                <div class="col-12 col-md-4 ml-lg-auto mt-3"> 
                    <div class="card">
 <div class="card-body">
                        <div class="cart-page-heading">
                            <h5>Reviews</h5> 
                        </div>
                       
         <hr class="my-4" /> 

<div class="row">
<div class="col-md-12">
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<a class="btn card-link">  {{$data['totalRating']}} rating </a>
</div>
</div>

<div class="row">
<div class="col-md-12">

<div class="row">
<div class="col-md-3">
  5 star
</div>

<div class="col-md-7">
  <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingFiveCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

</div>
</div>
<div class="col-md-2">
  {{$data['ratingFiveCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-3">
  4 star
</div>

<div class="col-md-7">
 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingFourCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingFourCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-3">
  3 star
</div>

<div class="col-md-7">

 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingThreeCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingThreeCount']}}%
</div>
</div>

<div class="row">
<div class="col-md-3">
  2 star
</div>

<div class="col-md-7">

 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingTwoCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
<div class="col-md-2">
  {{$data['ratingTwoCount']}}%
</div>
</div>


<div class="row">
<div class="col-md-3">
  1 star
</div>

<div class="col-md-7">
 <div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: {{$data['ratingOneCount']}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div> 
</div>
</div>
<div class="col-md-2">
  {{$data['ratingOneCount']}}%
</div>



     </div>
   </div>  
</div>
</div>
</div>
</div>
</div>
</div>
<div class="mt-3"></div>
 
@endsection 
 
@section('script')

 
@endsection 