@extends('layouts.service_template_01')
<?php 
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ;
    $total_amount =0.00;
    $total_gst =0.00;
    $total_discount =0.00;
    $actual_amount =0.00;
    $total_gst_pc =0.00;
?>

@section('content')


    <div class="container"> 

  <form method="post" action="{{action('Appointment\AppointmentControllerWeb@makeNewAppointment')}}" enctype="multipart/form-data">
    {{csrf_field()}} 
  
<div class="row">
  @if (session('err_msg'))
  <div class="col-md-12 mt-4">
    <div class="alert alert-info">
      {{ session('err_msg') }}
    </div>
  </div>
  @endif

  <div class="col-md-8">
    <div class="card mt-4">
  <div class="card-header">
    <h4>Booking Summary</h4>
  </div>
  <div class="card-body">
 
  <table class="table"> 
      <thead class=" text-primary">
        <tr >
          <th scope="col">Image</th>
          <th scope="col">Package</th> 
          <th scope="col">Pricing</th>
          <th scope="col">Discount</th>
          <th scope="col">Actual Pricing</th>
          <th></th>
        </tr>
      </thead>
    <tbody>

    @foreach ($data['packages'] as $package)

      @php
        $image_found = false;
        $sub_total = ($package->pricing -  $package->discount);
        $total_amount += $package->pricing;
        $total_discount += $package->discount;
        $sub_gst = ( $sub_total * 0.01 * ( $package->cgst_pc + $package->sgst_pc )  );
        $total_gst +=$sub_gst;
        $actual_amount += $sub_total + $sub_gst;

        $total_gst_pc = $package->cgst_pc + $package->sgst_pc ;
      @endphp
     <tr >
     <td>

      @foreach($data['package_specifications'] as $package_specification)
          @if(  $package_specification->service_product_id  == $package->id  )
            <img src="{{ $package_specification->photos }}" class="align-self-start mr-3 img-rounded" height="50px" width="50px" alt="{{ $package->srv_name }}">
            @break
          @endif 
      @endforeach 
    </td>
      <td>
        {{ $package->srv_name }}<input type="hidden" name="packageNos[]" value='{{ $package->id  }}'>
    </td>
      <td>{{$package->pricing}}</td>
      <td>{{$package->discount}}</td>
      <td>{{$package->actual_price}}</td>
      <td><a   class='btn btn-link red' href="{{ URL::to('/appointment/remove-package-from-cart') }}?bin={{ $data['business']->id }}&package={{ $package->id  }}&return=checkout" ><i class='fa fa-trash red'></i></a></td>
    </tr>
    @endforeach 
        <tr >
          <th colspan='6' scope="col">
            <a  href="{{ URL::to('appointment/business/prepare-service-appointment') }}?bin={{ $data['business']->id }}#contentpackages" class='btn btn-link' >Add Additional Package</a></th>
        </tr>
     

   </tbody>
  </table>
 
 

 <table class="table">
    <tbody>
      <tr >
        <th scope="col">Service Date</th>
        <th scope="col" class='text-right'>
          {{ date('Y-m-d', strtotime($data['service_date'])) }}
          <input type="hidden" name="serviceDate" id="bookingServiceDate" value="{{ date('Y-m-d', strtotime($data['service_date'])) }}"> 
          <a type="button"  class='btn btn-sm btn-link' 
          href="{{ URL::to('/booking/business/booking-calendar') }}?bin={{ $data['bin'] }}" >Change Date</a>
        </th>
      </tr>

      <tr >
        <th scope="col">Service Serial No.</th>
        <th scope="col" class='text-right'>{{ $data['timeslot']->slotNo }}
        <input type="hidden" name="serviceTimeSlot" id="serviceTimeSlot" value="{{ $data['timeslot']->slotNo }}">
      </th>
      </tr>
      <tr >
        <th scope="col">Service Time</th>
        <th scope="col" class='text-right'>{{ date('h:i A', strtotime($data['timeslot']->startTime )) }} - {{ date('h:i A', strtotime($data['timeslot']->endTime)) }}</th>
      </tr>
      <tr >
        <th scope="col">Selected Staff</th>
        <th scope="col" class='text-right'>{{ $data['staff']->staffName }}
        <input type="hidden"  id="staffId" name="staffId" value="{{ $data['staff']->staffId }}" ></th>
      </tr>

      <tr >
        <th scope="col">Total Amount</th>
        <th scope="col" class='text-right'>{{ $total_amount  }}</th>
      </tr>
      <tr >
        <th scope="col">Discount</th>
        <th scope="col" class='text-right'>{{ $total_discount  }}</th>
      </tr>


      <tr >
        <th scope="col">GST ({{ $total_gst_pc }}% of the total amount)</th>
        <th scope="col" class='text-right'>{{ $total_gst  }}</th>
      </tr>
  <tr >
        <th scope="col">Total amount to pay </th>
        <th scope="col" class='text-right'>{{ $actual_amount   }}
          <input type="hidden" id="subtotal" value="{{ $actual_amount   }}"> </th>
      </tr>

    </tbody>
  </table> 
 
  </div>

  <div class="card-footer text-left"> 
    bookTou booking process is the fastest and easiest way to reserve your picnic or vacation spot.
    Your booking will be confirmed instantly. Our super-friendly support staff will also update you regarding the booking from time to time.
  </div>
</div>

  </div> 
    <div class="col-md-4">
    <div class="card mt-4">
  <div class="card-header">
    <h4>Customer &amp; Additional Details</h4>
  </div>
  <div class="card-body">

    <div class="form-group">
    <label for="cname">Customer Name:</label>
    <input type="text" class="form-control" required id="cname" name="cname" value="{{ $data['cust_info']->fullname }}">
  </div>
 

<div class="form-row">
  <div class="col-md-6 mt-2">
    <label for="cphone">Customer Phone:</label>
    <input type="number"  required class="form-control" id="cphone" name="cphone"  maxlength="10" value="{{ $data['cust_info']->phone }}">
  </div>
  <div class="form-group mt-2">
    <label for="caddress">Customer Address:</label>
    <textarea  class="form-control" id="caddress" name="caddress">{{ $data['cust_info']->locality }}</textarea>
  </div>
 
 </div>



  <div class="form-group mt-2">
    <label for="caddress">Additional Requests/Remarks:</label>
    <textarea  class="form-control" id="bookingRemark" name="bookingRemark"></textarea>
  </div>

  <div class="form-group mt-3">
  <input class="form-check-input"  type="radio"  data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" name="paymode" value="online">
  <label class="form-check-label m5">Pay Online</label>  
  </div>

  </div>

    <div class="card-footer text-right">
      <input type="hidden" name="bin" id="bin" value="{{ $data['business']->id}}" > 


              <div class="d-flex mb-3">
          <div class="p-2 flex-fill"> 
            <strong>Pay total amount ₹ <label id="subamount">{{number_format($actual_amount,2,".","")}}</label></strong>
            <br/>
            <i class="fa fa-shield sgreen"></i> Payment is 100% safe and secure.

        </div>
        <div class="p-2 flex-fill-right">
          <button type="submit" class="btn btn-primary btn-xss btn-rounded btn_bookcheckout" value="save" name="save">Pay Now</button> 

        </div> 
    </div>


      
    </div>
</div>
</div>
</div>
</form>


 </div>

@endsection

@section("script")
 
 
<script>


$('.showtime').timepicker({
    timeFormat: 'h:mm p',
    interval: 30,
    minTime: '10',
    maxTime: '7:00pm',
    defaultTime: '11',
    startTime: '9:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});



</script>

 @endsection