@extends('layouts.service_template_01')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path');
?>
<style type="text/css">
    
 
.search-sec{
    background: rgba(0, 11, 9, 0.82);
    padding: 2rem;
}
.search-slt{
    display: block;
    width: 100%;
    font-size: 0.875rem;
    line-height: 1.5;
    color: #55595c;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
.wrn-btn{
    width: 100%;
    font-size: 16px;
    font-weight: 400;
    text-transform: capitalize;
     height: calc(3rem + 2px) !important;
     border-radius:0;
}
</style> 

<section class="search-sec"> 
    <div class="container">

        <div class="row">
            <div class="col-12">
        <form action="{{action('Appointment\AppointmentControllerWeb@searchAppointmentServices')}}" method="get"> 
        <div class="search-box-outer" style="margin-top:0!important; position:relative !important;"> 
            <div class="search-box-inner"> 
                <div class="search-inpage"> 
                    <div class="row"> 
                        <div class="col-md-12">
                            <h2>Search beauty or makeup services</h2>
                            <hr/>
                        </div>
                        <div class="col-md-12">

                            <div class="input-group">
                                <input type="text" name="keyword"  class='form-control' placeholder="Hair dying, bridal makeup">

                                <select class="form-control search-slt" id="city" name="city" placeholder="Imphal"> 
                                    @foreach($city as $citylist)
                                        @if($citylist->cityList != "")
                                            <option value="{{$citylist->cityList}}">{{$citylist->cityList}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <button type="submit" value='search' class='btn inpgsearch'>
                                    <i class='fa fa-search'></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
    </form>
    </div>
    </div>
    </div>
</section>

 
 <div class="container" > 
        <div class="row mt-4" > 

            @foreach($packages as $item)
             @foreach($package_specifications as $package_specification)
                @if(  $package_specification->service_product_id  == $item->id  )
                    <div class="col-sm-6 col-lg-3">
            
                        <div class="card no-border card-img-scale overflow-hidden bg-transparent mb-3">    
                          
                            <div class="position-absolute top-0 end-0 p-3 z-index-9">
                                <a tabindex="0" class="mb-0 btn btn-white btn-round" data-bs-toggle="popover" data-bs-trigger="focus" data-bs-placement="left" data-bs-content="Appointment Packages">
                                    <i class="bi bi-info-circle"></i>
                                </a>
                            </div>

                       
                            <div class="card-img-scale-wrapper rounded-3">
                                <img src="{{ $package_specification->photos }}" class="card-img" alt="{{ $item->srv_name }}">
                            </div>

                          
                            <div class="card-body px-2">
                                <!-- Title -->
                                <div class="d-flex justify-content-between align-items-center">
                                    <h5 class="card-title">
                                        <a href="{{ URL::to('/appointment/business/appointment-calendar') }}?bin={{ $item->bin }}&package={{ $item->id  }}" class="stretched-link">{{ $item->srv_name }}</a>
                                    </h5>
                                    <h6 class="mb-0">0.0<i class="fas fa-star text-warning ms-1"></i></h6>
                                </div>
                                <!-- Content -->
                                <span class="mb-0">Starts from ₹ {{ $item->actual_price }}</span>
                            </div>
                        </div>
                    </div>

                @endif
            @endforeach
        @endforeach 
    </div>  
</div> 


@endsection  
@section('script')
<script>

</script>
@endsection