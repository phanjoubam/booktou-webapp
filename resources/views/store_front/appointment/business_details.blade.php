@extends('layouts.booking_theme_01')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?>
<style>
 
   

</style>

<div class='breadcrumb-box'>  
    <div class='container'>  
        <div class="row mt-2"> 
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/booking')}}" style="color:green;">
                                   <b> Appointment</b>
                                </a>
                            </li>
                            <li class="breadcrumb-item active text-uppercase" aria-current="page"> 
                                   <b> {{$business->name}}</b> 
                            </li>
                        </ol>
              </nav>
          </div>
          <div class="col-md-6 col-sm-12">

          </div>
      </div>
  </div>
  <div class=" col-md-3 d-none d-sm-block">   
</div>
</div>
</div>
 </div>


<div class="outer-top-tm" > 
    <div class="container" style="">  
     <div class="row"> 
        <div class="col-sm-3 col-xs-12 sidebar " >
             
            <div class="sidebar-widget widget_product_categories" >
            <h5 class="section-title font-weight-bold">{{$business->name}}</h5>        
            <div class="fanbox" >
            <div class="fanbox-header   d-none d-lg-block d-xl-block"> 
                <?php
                if($business->banner==""){

                                    $image_url =  $cdn_url."/assets/image/no-image.jpg";

                                } else {

                                    $image_url =  'public'.$business->banner;

                                } 

                        ?>


             <img src="{{URL::to($image_url)}}" width="100%">
            </div>
            <div class="fanbox-body">
                <p><span class="widget-title mb-30"></span><br>

               {{$business->locality}}<br>
                 {{$business->landmark}} {{$business->city}},{{$business->state}}
                </p>  
            </div>   
            </div>
            </div>


           
            <div class="py-2  border-bottom filters-container">

              <h5 class="font-weight-bold mt-2 Merhants">Categories</h5>       
               <form >
                <?php 
                    if(isset($serviceid))
                    {
                        $service = $serviceid;
                    }else{
                        $service = "";
                    }
                ?>
                @if($service == "")
                <a class="text-dark text-decoration-none" href="{{URL::to('/appointment/business/btm-')}}{{$business->id}}/all">
                        All Categories
                </a>
                @else
                <a class="text-dark text-decoration-none" href="{{URL::to('/appointment/appointment/view-service')}}/<?php echo $service;?>/all">
                         All Categories
                </a>
                @endif


                
               </form>
            </div>
            
    </div>
        
  

<div class="col-sm-12 col-md-9  col-sm-12-order-2" >
    <div class="card">
    <div class="row card-body"> 
  <!-- filtered products -->
    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/service/booking')}}" style="color:green;">
                                   <b> Booking</b>
                                </a>
                            </li> 
                            <li class="breadcrumb-item text-uppercase">
                                
                                   {{$business->name}}
                               
                            </li> 
                            
                        </ol>
    </nav>
    </div>
    </div>

  <div class="card mt-2 load">
        <div class="row card-body" > 

            @if( count($services) > 0 )




        @foreach($services as $item) 
            @if($item->photos=="")
                @php 
                $imageurl = "https://cdn.booktou.in/assets/image/no-image.jpg";
                @endphp     
            @else
                @php 
                $imageurl = $item->photos;
                @endphp             
            @endif     
        <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3  order-1"> 
                
                <div class="card card-sm card-product-grid"> 
                <a href="{{URL::to('appointment/prepare-service-booking')}}/{{ $item->bin }}/{{$item->id}}" class="img-wrap">
                    <img src="{{$imageurl}}" class="img-fluid"> 
                </a>
                <figcaption class="info-wrap">
                    <a href="{{URL::to('appointment/prepare-service-booking')}}/{{ $item->bin }}/{{$item->id}}" class="title">{{$item->srv_name}}   </a>
 
                    <div class="price mt-1">
                                            <p class="card-text">₹ {{$item->actual_price}}  </p>
                                        </div>  
 

                </figcaption> 
                <div class="text-center">      
                        <a  href="{{URL::to('appointment/prepare-service-booking')}}/{{ $item->bin }}/{{$item->id}}" class="btn btn-primary btn-sm">Book</a>
 
                         </div>
                </div>                            
        </div>

            @endforeach 

            @else

            <p>No service or package is listed!</p>

            @endif



        </div> 
    </div>
 <!-- filtered products --> 
    
</div>
<!--  -->
</div> 
</div> <!-- container -->
</div>

@endsection 

@section('script')

<script type="text/javascript">

 

</script>
@endsection