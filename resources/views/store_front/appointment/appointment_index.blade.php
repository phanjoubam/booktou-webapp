@extends('layouts.booking_theme_01')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public
?> 



<section> 

    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{URL::to('public/assets/image/slider_sns.jpg')}}" class="d-block w-100">
            </div> 
        </div>

        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <form action="{{action('Appointment\AppointmentControllerWeb@searchAppointmentServices')}}" method="get"> 
        <div class="search-box-outer"> 
            <div class="search-box-inner"> 
                <div class="search-inpage"> 
                    <div class="row"> 
                        <div class="col-md-12">
                            <h2>Search beauty or makeup services</h2>
                            <hr/>
                        </div>
                        <div class="col-md-12">

                            <div class="input-group">
                                <input type="text" name="keyword"  class='form-control' placeholder="Hair dying, bridal makeup">

                                <select class="form-control search-slt" id="city" name="city" placeholder="Imphal"> 
                                    @foreach($city as $citylist)
                                        @if($citylist->cityList != "")
                                            <option value="{{$citylist->cityList}}">{{$citylist->cityList}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <button type="submit" value='search' class='btn inpgsearch'>
                                    <i class='fa fa-search'></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
    </form>

    
</section>
 
<section  class="bg-ltgray icon-block-lg">
   <div class="container"> 
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center text-uppercase">Package Categories</h4>
        </div>

        <div class="col-md-12 mt-5">
        <div class="owl-carousel owl-theme">

        @foreach( $package_categories as $package_category)

        @php
        $encoded_url = implode("-", explode(" ", $package_category->category) );
        @endphp


        <div class="item" style="width:110px; text-align:center">
            <a href="{{  URL::to('/appointment/browse-packages')}}/{{ strtolower(  $encoded_url ) }}">
                <img  src="{{ $package_category->icon_url}}" class="img-fluid rounded-circle  p-2">
                <span class='text-sm'>{{ strtoupper($package_category->category) }}</span>
            </a>
        </div> 
        @endforeach
</div> 
        </div> 
    </div>
</div>
</section> 

 





<section class="mt-5 bgcolor m-4 ">

    <div class="container"> 
        <div class="row">
            <div class='col-md-12'>
                <h4 class="text-center">TOP RATED PARTNERS</h4>
            </div>

        </div>

        <div class="row mt-3"> 

            @foreach($business as $items) 
            <div class='col-md-3 mb-3'>
                <div style="border:1px solid #efefef; border-radius:10px; "> 
                        @if($items->banner=="") 
                        @php
                        $image_url = "https://cdn.booktou.in/assets/image/no-image.jpg"; 
                        @endphp
                        @else
                        @php
                        $image_url = $cdn_url.$items->banner; 
                        @endphp
                        @endif

                        <div style="background-image: url('{{$image_url}}'); background-size:cover; height: 250px; border-top-right-radius:8px;  border-top-left-radius:8px; "></div>
                        <div style="padding: 20px;color: #343434;">
                            <p class='h5'>{{$items->name}}</p>
                            <p>
                                Starts from 250 ₹
                            </p>
                            <p>
                                <a role='link' type='button' class='btn btn-primary btn-rounded' href="{{URL::to('/appointment/business/prepare-service-appointment')}}?bin={{$items->id}}">
                                    Browse Packages
                                </a></p>

                            </div> 
                    </div>
                </div> 

                @endforeach

            </div>

 
 
    </div>
</section> 

@endsection 

@section('script')

<style>

    .owl-dots,  .owl-prev, button.owl-dot{
   display: none !important;
}
.disabled {
   display: none !important;
}
</style>


<script type="text/javascript">
     
   $(document).ready(function(){
      $(".owl-carousel").owlCarousel({
        margin:10,
        loop:true,
        navigation : false,
        autoWidth:true,
        items:8
    });
  });


     if (screen.width<500) { 
                  $('.carousel2').slick({
                  rows: 1,
                  infinite: false,
                  slidesToShow: 1,
                  slidesToScroll: 1
                });

        }else{ 
                  $('.carousel2').slick({
                  rows: 1,
                  infinite: false,
                  slidesToShow: 4,
                  slidesToScroll: 3
                  }); 
        }

</script>
@endsection 
