@extends('layouts.booking_theme_01')
@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ;
?>

<section class='inpage-subhead'>
    <div class='container'>  
        <div class="row mt-2">
            <div class='col-md-8 p-5'>
                 <h1 class='display-4 text-white'>{{ ucwords( implode(" ", explode("-", $categoryname) )) }}</h1>
                 <p >
                     Browse packages, buy offers or start booking your preferred package.
                 </p>
            </div>
            <div class='col-md-4 d-none d-md-block'>
                 <div 
                 style="background-image: url('{{ URL::to("/public/assets/image/gift-box.png" )  }}'); background-size: cover; 
                 height: 150px; width: 150px;margin: 30px auto;" ></div>
            </div>
        </div>
    </div>
</section>

<div class='breadcrumb-box'>  
    <div class='container'>  
        <div class="row mt-2"> 
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/appointment')}}" style="color:green;">
                                   <b>Appointment</b>
                                </a>
                            </li>
                            @if(isset($categoryname))
                            <li class="breadcrumb-item active text-uppercase" aria-current="page">
                                 
                                   <b> {{$categoryname}} </b> 
                            </li>
                            @endif 
                        </ol>
              </nav>
          </div>
          <div class="col-md-6 col-sm-12">

          </div>
      </div>
  </div>
  <div class=" col-md-3 d-none d-sm-block">   
</div>
</div>
</div>
 </div>
 
 <div class='container'>  
     <div class="row"> 


<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" >  

<div class='card '>
    <div class='card-header'>
<p class='text-md'><i class='fa fa-question'></i> Why book with bookTou?</p>
    </div>
    <div class='card-body bg-ltgray'> 
<ul>
<li><i class='fa fa-check'></i> Experienced professionals.</li>
<li><i class='fa fa-check'></i> Realtime confirmation.</li>
<li><i class='fa fa-check'></i> Fast and secure.</li>
</ul> </div>
 </div>

</div>


<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" >  
        <div class="row">

            @foreach($services as $item) 

            <div class="col-xs-12 col-sm-12 col-md-12"> 
                <div class="package-card">

 
                    <?php 
                    /* 
                        if( $item->photos != "")
                        @php
                            $imageurl = $item->photos;
                        @endphp
                        <img src="{{ $imageurl }}" class="img-fluid "> 

                    */
                    ?>  

                    



                    <div class='package-card-body'>
                        <div class="row "> 

                            @if( isset($service_profiles) && count($service_profiles) > 0)
                                    @php 
                                        $active_slider ="active";
                                    @endphp
                                <div class='col-md-2'>
                                    <div id="service_slider{{$item->id}}" class="carousel slide" data-bs-ride="true" >
                                      <div class="carousel-inner">
                                        @foreach($service_profiles as $service_profile)
                                            @if(  $service_profile->service_product_id  == $item->id  )
                                                <div class="carousel-item {{ $active_slider }}">
                                                  <img src="{{ $service_profile->photos }}" class="d-block w-100 ar-image mr-1" alt="...">
                                                </div>
                                                @php 
                                                    $active_slider ="";
                                                @endphp
                                            @endif 
                                        @endforeach  
                                    </div>
                                  
                                    <button class="carousel-control-prev" type="button" data-bs-target="#service_slider{{$item->id}}" data-bs-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Previous</span>
                                        </button>
                                    <button class="carousel-control-next" type="button" data-bs-target="#service_slider{{$item->id}}" data-bs-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="visually-hidden">Next</span>
                                    </button>
                                    </div>
                                </div>

                            <div class="col-md-10 p-2"> 
                        @else

                            <div class="col-md-12 p-2">  

                        @endif

                            <p class='display-6'>{{$item->srv_name}}</p> 
                            <p>
                                @php 
                                    $len = strlen ( $item->srv_details ); 
                                    if($len > 250) 
                                    {
                                         
                                        echo substr($item->srv_details, 0, 250 ) . ' [+]';
                                    }
                                    else
                                    {
                                          echo $item->srv_details;
                                    } 
                                @endphp  
                             </p> 
                        </div>

 

                        <div class="col-md-12"> 
                                @foreach($businesses as $business)
                                    @if( $item->bin  == $business->id )
                                        <p style='font-size: 1.2em;'>Offered by<br/>{{$business->name}}</p>
                                        @break
                                    @endif 
                                @endforeach
                                <hr/> 
                        </div> 

                    </div>


             <div class="row align-items-start"> 
                <div class="col-md-10"> 
                        <div class="price mt-1 text-right">
                            <span class="h3">Starts from <span class='orange'>{{$item->actual_price}} ₹</span></span>
                        </div>
                </div>
                <div class="col-md-2 text-right"> 
                    <a  href="{{URL::to('/appointment/business/appointment-calendar')}}?bin={{ $item->bin }}&package={{$item->id}}" class="btn btn-primary btn-rounded">
                    Book Now</a>
                </div>
            </div>


 
                    <div class="text-center">
                        
                    </div>
                </div></div>
            </div>


            @endforeach
        </div> 
 <!-- filtered products -->

</div>


</div>
</div> 
@endsection
