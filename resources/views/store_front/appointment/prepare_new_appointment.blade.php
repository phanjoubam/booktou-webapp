@extends('layouts.service_template_01')

@section('style')

<style type="text/css">

.owl-dots,  .owl-prev, button.owl-dot{

   display: none !important;
}
.disabled {
   display: none !important;
}
</style>
@endsection


@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ; 
    $taxes = 0;
    $price = 0;
    $total= 0;
    $actualamount =  $offer_amount = $offer_total =  $price = $total = 0;
?>


@auth
 
<section>
  <div class="container">
    <div class='row'>

        <?php

        if( $business->banner == ""  || $business->banner == null )
        {
            $image_url =   URL::to('/public/assets/image/slider_paa.jpg');
        }
        else
        {
            $image_url =  $cdn_url. $business->banner;

        }

        ?>  

        
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4 " >
    <div style="width:100%; height: 350px; border-radius: 10px; background-image: url('{{ $image_url }}'); background-size:cover" > 
    </div> 
</div> 
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4 float-sm-end" > 
            <div class="p-5" >
                <h1 class="display-6">{{$business->name}}</h1>
                <h5 class="section-title font-weight-bold"></h5> 
                <p>
                     {{$business->landmark}} {{$business->city}},{{$business->state}}
                </p> 
                <button role='link' type='button' class='btn btn-outline-secondary btn-rounded'>
                   <i class='fa fa-check-circle'></i> Verified Merchant
                </button>
     </div> 
</div>

    </div>
    </div>
</section>


<div class='hr-md mt-3'></div>
<div class="nav_bar_categories" id="navbar_top" >
<div class="container " >
    <div class='row'>
        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 p-1'> 
                <div class="owl-carousel owl-theme">

                    @foreach( $package_categories as $package_category)
                        <button class="filter-box">
               <img width="20px" src="{{ $package_category->icon_url}}"   > {{ ucwords( strtolower($package_category->category ) ) }}</button>
                    @endforeach
                </div>
        </div>
    </div>
</div>
</div>
<div class='hr-md '></div>

<div class="container">
    <div class="row">
        
        <div class='col-xs-12 col-sm-12 col-md-7 col-lg-7 br-right-md p-4'> 
               
            @if( isset($packages))  
            @foreach($packages as $item)  
            <div class="d-flex mb-2 card-package">
              <div class="flex-shrink-0">
                @if( $item->photos != "") 
                @php
                $imageurl = $item->photos;
                @endphp
                <img src="{{ $item->photos }}" class="align-self-end mr-3 rounded" width="90px" alt="{{ $item->srv_name }}">  
                @else 
                    @if( count($package_specifications) > 0) 
                        @foreach($package_specifications as $package_specification)
                            @if(  $package_specification->service_product_id  == $item->id  )
                            <img src="{{ $package_specification->photos }}" class="align-self-start mr-3 img-rounded" width="90px" alt="{{ $item->srv_name }}">
                            @break
                            @endif 
                        @endforeach  
                    @endif 
                @endif
            </div>
            <div class="flex-grow-1 ms-3">
                <p  class="lead">
                    {{ $item->srv_name }}<br/> 

                    ₹ {{ $item->actual_price }}<br/>

                </p>
            </div>
            <div class="p-2  ">
                @if( isset($selected_service) && $selected_service->id == $item->id  )
                <button type="button"  class='btn btn-sm btn-rounded btn-mw btn-outline-danger btnselectitems' 
                data-status="rem" 
                data-key="{{ $item->id  }}" 
                data-srvname="{{ $item->srv_name }}" data-price="{{ $item->actual_price }}" 
                data-details="{{ $item->srv_details }}">Remove</button>
                @else 
                <button type="button"  class='btn btn-sm btn-rounded btn-mw btn-outline-dark btnselectitems' 
                data-status="add" 
                data-key="{{ $item->id  }}" 
                data-srvname="{{ $item->srv_name }}" data-price="{{ $item->actual_price }}" 
                data-details="{{ $item->srv_details }}">Add Service</button>
                @endif

            </div>
        </div>
        @endforeach    

        @else

        @endif 
    </div>

<?php
    $mainmenu;
    foreach(request()->route()->parameters as $val)
    {
       $mainmenu =  $val;
    }
?>


<div class='col-xs-12 col-sm-12 col-md-5 col-lg-5 p-1'>
        <div class="mb-2" id="pane_cart_summary" >
        <form method="post" action="{{action('Appointment\AppointmentControllerWeb@makeNewAppointment')}}" enctype="multipart/form-data">

            {{csrf_field()}} 
            <div class="accordion accordion-flush" id="accordionBookingPane">
                      <div class="accordion-item">
                        <h2 class="accordion-header" id="accordionBookingheadingOne">
                          <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#accordionBookingcollapseOne" aria-expanded="true" aria-controls="accordionBookingcollapseOne">
                            <span class='display-8'>Select Your Preferred Date</span>
                        </button>
                    </h2>
                    <div id="accordionBookingcollapseOne" class="accordion-collapse collapse show" aria-labelledby="accordionBookingheadingOne" data-bs-parent="#accordionBookingPane">
                      <div class="accordion-body" style="padding: 0 0 10px 0;">
                        <div id="slotcalendar"></div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="accordionBookingheadingTwo">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordionBookingcollapseTwo" aria-expanded="false" aria-controls="accordionBookingcollapseTwo">
                    <span class='display-8'>Select Your Preferred Time</span>
                </button>
            </h2>
            <div id="accordionBookingcollapseTwo" class="accordion-collapse collapse" aria-labelledby="accordionBookingheadingTwo" data-bs-parent="#accordionBookingPane">
              <div class="accordion-body">
                            <div class="row">  
                <div class="col-md-12"> 
                <div id='de_available_slots'>
                    @foreach($available_slots as $v)
                    <label class="lblslot rb-slot" data-slot="{{ $v->slot_number }}"> 
                        <span class="outside" ><span class="inside"></span></span>{{ $v->start_time }} - {{ $v->end_time }}
                    </label>
                    @endforeach
                </div>
                </div>
                </div>

            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="accordionBookingheadingThree">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordionBookingcollapseThree" aria-expanded="false" aria-controls="accordionBookingcollapseThree">
            <span class='display-8'>Select Available Staff</span>
        </button>
    </h2>
    <div id="accordionBookingcollapseThree" class="accordion-collapse collapse" aria-labelledby="accordionBookingheadingThree" data-bs-parent="#accordionBookingPane">
      <div class="accordion-body">
        <div id='de_available_staffs'>

          @foreach($staff_available as $v)

          <div class='card-image'>
            <div style='min-height: 100px'>
                @if($v->profilePicture === null)
                <img src="{{ URL::to('/public/assets/image/no-image.jpg') }}" width='90px' class='rounded mx-auto d-block'/>
                @else
                <img src="{{ $v->profilePicture }}" width='90px' class='rounded mx-auto d-block'/>
                @endif

            </div>
            <p class='h5'>{{ $v->staffName }}</p>
        </div>

        @endforeach

    </div> 
</div>
</div>
</div>

<div class="accordion-item">
    <div class="accordion-header" id="accordionBookingheadingFour">
         <div class="d-flex mb-3">
                      <div class="p-2 flex-fill"> 
                        <strong>Cart Amount ₹ <label id="subamount">{{number_format($total,2,".","")}}</label></strong>
                        <br/>
                        <i class="fa fa-shield blue"></i> Secure Payment

                    </div>
                    <div class="p-2 flex-fill" style="text-align:right">
                        <button type="submit" class="btn btn-primary btn-rounded btn_bookcheckout" value="btn_placeOrder" name="btnorder" disabled>Book Now</button>

                    </div> 
                </div>

    </div>
<div id="accordionBookingcollapseFour" class="accordion-collapse collapse" aria-labelledby="accordionBookingheadingFour" data-bs-parent="#accordionBookingPane">
  <div class="accordion-body">


  </div>
</div>
</div> 

</div>

<input type="hidden"  id="preferredTime" name="preferredTime"  required=""> 
<input type="hidden"  id="endtimeslot" name="endTime" required="">
<input type="hidden"  id="subModule" name="subModule" required="" value="{{$business->sub_module}}">
<input type="hidden" id="total" value="{{$total}}">
<input type="hidden" id="subtotal" value="{{$total}}">
<input type="hidden" name="serviceDate" id="bookingServiceDate" required="">
<input type="hidden" name="serviceTimeSlot" id="serviceTimeSlot" required="">
<input type="hidden" name="bin" id="bin" value="{{$business->id}}" required=""> 
<input type="hidden"  id="staffId" name="staffId"  >
<input type='hidden'  value="{{ $business->id }}"  name="key" />
<div class="addservicelist"></div> 


 </form>
  
    </div>

    </div> 
</div> 
 
</div> 

@endauth

@guest

 

 <section>
  <div class="container">
    <div class="row">
 <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3 mt-4 mb-4">
                    <div class="card"> 
                         <div class="card-body">
                        <div class="cart-page-heading mb-30">
                            <h1>Customer Login</h1>
                            <hr/>
                        </div>

                        <form action="{{ action('Auth\LoginController@customerLogin') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                
                                <div class="col-12 mb-3">
                                    <label for="phone">Phone Number or Email: <span>*</span></label>
                                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email'>
                                </div>
                               
                               
                                <div class="col-12 mb-4">
                                    <label for="email_address">Password: <span>*</span></label>
                                    <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password'>
                                </div>
                                <div class="col-12 mb-4">
                                
                                <button type="submit" value='login' name='login' class="btn btn-primary">Login</button>
                                <br/> 
 <br/> <br/>
                                 <h5>New Member? Please signup here</h5>

<a href="{{ URL::to('/join-and-refer') }}" class="btn btn-info">Signup Here</a>

<h5>Lost password? Recover here</h5>
<a href="{{ URL::to('/forgot-password') }}" class="btn btn-danger">Forgot Password?</a> 



 </div>

                            </div>
                        </form>
                    </div>
                    </div>
                </div>
           </div>
    </div>

</section>
@endguest


<!-- spinner -->
<div id="cover-spin"></div>
<!-- modal for service category -->


<div class="modal fade serviceModal" id="serviceModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">

    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
            <span class="badge badge-primary badge-pill">Service Provided by {{$business->name}}</span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-md-6">
                  <select name="serviceCategory" class="form-control serviceCategory" required>
                      <option value="">Select Service Category</option>
                      @foreach($packages as $item)
                        <option value="{{$item->service_category}}">{{ $item->service_category}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-md-6">
                    <input type="text" name="searchFilter" class="form-control filterservice" placeholder="Filter service name">
                </div> 
             <div class="col-md-12 serviceNameResult" id="loading"> 
             </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- modal service category -->

@endsection

@section('script')


<script>


var lastScrollY = 0;
$(window).scroll(function(event)
{
   var st = $(this).scrollTop(); 
   lastScrollY = st;
   if (lastScrollY > 440)
   {
    $("#navbar_top").addClass("fixed-top bg-gray");
    catbar_height = document.querySelector('.nav_bar_categories').offsetHeight;
    document.body.style.paddingTop = catbar_height + 'px';
    fixedpane_width = $("#pane_cart_summary").width();
    $("#pane_cart_summary").addClass("fixed-pane");
    $('#pane_cart_summary').css('width',  fixedpane_width + 'px');

   }
   else
   {
    $("#navbar_top").removeClass('fixed-top bg-gray');
    $("#pane_cart_summary").removeClass("fixed-pane");
    document.body.style.paddingTop = '0';
   }
 


});
 

$(function(){
    $('[data-toggle="tooltip"]').tooltip();
})
 

var today = new Date();
var yesterday = new Date(today);
yesterday.setDate(today.getDate() -1);
var current_date = yesterday.toISOString().split('T')[0];

$(document).on('click','.btn-showservice',function(){
  $('.serviceModal').modal('show');
}); 


$('#preferredTime').timepicker({ 'timeFormat': 'h:i A' });


$('#preferredTime').on('change', function() {

  var pref_time = $(this).val();  
  var ts_preftime = new Date("<?php echo date('F d, Y') ?> " + pref_time);
  ts_preftime = ts_preftime.getTime();
 

  var sh = $(".service_hour").html();
  var time_parts = sh.split("-");
  if(time_parts.length >= 2 )
  {
    var start_time =  time_parts[0];
    var end_time =  time_parts[1];
    //convert both time into timestamp
    var ts_start = new Date("<?php echo date('F d, Y') ?> " + start_time);
    ts_start = ts_start.getTime();
    var ts_end = new Date("<?php echo date('F d, Y') ?> " + end_time);
    ts_end = ts_end.getTime();

    if(ts_start <= ts_preftime &&  ts_preftime <= ts_end) {
      $(".btn_bookcheckout").prop("disabled", false);
    }
    else
    {
      $(".btn_bookcheckout").prop("disabled", true);
    }

  }  
});


$(document).on("click", ".btnselectitems", function()
{
    var key = $(this).attr('data-key');
    var srvname = $(this).attr('data-srvname');
    var details = $(this).attr('data-details');
    var price = $(this).attr('data-price');
    var total = $('#total').val();

    var cStatus = $(this).attr("data-status" ).toLocaleLowerCase();
     
    if(cStatus === "rem")
    {
        var subamount =  parseFloat(total) - parseFloat(price) ; 
        $('#total').val(subamount);
        $('#subtotal').val(subamount);
        $('#totalamount').text(subamount);
        $('#subamount').text(subamount);

        $(this).html("Add Service");
        $(this).attr("data-status", "add");
        $(this).removeClass("btn-outline-danger");
        $(this).addClass("btn-outline-dark "); 
        $('#dgkey' + key ).remove();

    }
    else 
    {
        var subamount = parseFloat(price) + parseFloat(total) ;
        $('#total').val(subamount);
        $('#subtotal').val(subamount);
        $('#totalamount').text(subamount);
        $('#subamount').text(subamount);
        $(this).html("Remove");
        $(this).attr("data-status", "rem");
        $(this).addClass("btn-outline-danger");
        $(this).removeClass("btn-outline-dark ");
        $('.addservicelist').last().
        append('<input type="hidden" value="'+key+'" id="dgkey'+key+'" name="packageNos[]" />');
    }

    if(subamount == 0)
    {
        $('#accordionBookingcollapseOne').collapse('toggle');
    }

    
});



$(document).ready(function ()
{ 

    $("#slotcalendar").zabuto_calendar({
        data: [
              {"date":"2022-12-11", "badge":false, "classname": "bg-red", "title":"Today"}
            ],
        cell_border: true,
        show_days: true,
        weekstartson: 0,
        legend: [
            {type: "block", label: "Booking Available", classname: "bg-green"},
            {type: "block", label: "All Booked",  classname: "bg-red"},
            {type: "block", label: "Some Free Slots",  classname: "bg-light-yellow"}
          ],
        nav_icon: {
          prev: '<i class="fa fa-chevron-left"></i>',
          next: '<i class="fa fa-chevron-right"></i>'
        },
        action: function () {
            return calendarDateChange(this.id, false);
        } 
    });
 

    $(".owl-carousel").owlCarousel({
        margin:10,
        center: false,
        items:2,
        loop:false,
        navigation : false,
        autoWidth:true,
    }); 

});


function calendarDateChange(id, fromModal) 
{
    <?php
    $bin = $business->id;
    ?>
    selectedDate = $("#" + id).data("date");    
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var d = new Date(selectedDate);
    var day = days[d.getDay()];
    var dayName = day.toUpperCase();
    var guid = $("#guid").text();
    $("#bookingServiceDate").val(selectedDate);

    startloading(); 
    var json = {};
    var productlistid = []; 
    $(document).find("input[name='packageNos[]']").each(function() {
        productlistid.push($(this).val());
    }); 
    json["serviceProductIds"] = productlistid ;
    json['dayName'] =  dayName;
    json['guid'] =  guid;
    json['date'] = selectedDate;
    json['bin'] = <?php echo $bin;?>;
    json['month'] = <?php echo date('m');?>;
    json['year'] = <?php echo date('Y');?>;


    $.ajax({
      type: 'post',
      url: api + "/v3/web/appointment/check-date-availability" ,
      data: json,
      success: function(data)
      {
        if(data.status_code == 3021)
        {
            $.each(data.business_hours, function (i, v) {
            $("#de_service_hours").append("<span class='badge rounded-pill text-bg-dark service_hour'>" + v.shifts+ "</span>"); 
            });
          $("#de_service_hours").append("<hr/>");  

          $(".btn_bookcheckout").prop("disabled", true);


        $("#de_available_staffs").html("");
        $.each(data.staffs, function (i, v) {

            if(v.profilePicture === null)
            {
                var imgUrl = "https://cdn.booktou.in/assets/image/no-image.jpg";
            }
            else
            {
                var imgUrl = v.profilePicture;
            }
            
            var profile_car = "<div class='card-image'>"; 
            profile_car += "<div style='min-height: 100px'><img src='" + imgUrl  +"' width='90px' class='rounded mx-auto d-block'/></div>";
            profile_car += "<p class='h5'>" + v.staffName  +"</p>"; 
            profile_car += "<button type='button' data-staffkey='" + v.staffId  + "' class='btn btn-primary btn-xss btn-rounded btnselectstaff'>Select Staff</button>"; 
            profile_car += "</div>";
            $("#de_available_staffs").append( profile_car ); 
            
        });


        $("#de_available_slots").html("");
        $.each(data.available_slots, function (i, v) {

            var slot_elem = `<label class="lblslot rb-slot" data-slot="` + v.slot_number + `"><input type="radio" class="radio-inline"  name="rbslots" value="` + v.slot_number + `"><span class="outside"><span class="inside"></span></span>` 
            +  v.start_time + " - " + v.end_time + `</label>`;
            $("#de_available_slots").append( slot_elem ); 
            
        });

        $('#accordionBookingcollapseTwo').collapse('toggle');

        exitloading();
      }
      else
      {
        exitloading();
        }
      },
      error: function()
      {
        alert(  'Something went wrong, please try again')
    }
});

}




$('body').on('click', 'label.lblslot', function() {

    var s = $(this).attr("data-slot"); 
    $(":radio[value=" + s + "]").prop('checked',true); 
    $("#serviceTimeSlot").val(s);

    $('#accordionBookingcollapseThree').collapse('toggle');

 });



$('body').on('click', 'button.btnselectstaff', function() {
 
    var d = $("#bookingServiceDate").val();
    var guid = $("#guid").text();
    var s = $('input[name="rbslots"]:checked').val();
    var u = $(this).attr("data-staffkey");

    $("#staffId").val(u);

    startloading(); 
    var json = {};
    json['guid'] =  guid;
    json['date'] = selectedDate;
    json['bin'] = <?php echo $bin;?>;
    json['staffId'] = u;
    json['slotNo'] = s;
    console.log(json);

    $.ajax({
      type: 'post',
      url: api + "/v3/web/appointment/check-staff-availability" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON( data ); 
        if(data.status_code == 1091)
        {

            if(data.is_available === "open")
          {
              $(".btn_bookcheckout").prop("disabled", false);
          }
          else
          {
              $(".btn_bookcheckout").prop("disabled", true);
          }
        exitloading();
      }
      else
      {
        exitloading();
        }
      },
      error: function()
      {
        alert(  'We could not process your request. Please retry after checking your internet connection.')
    }
});

});
      

</script>

 
@endsection
