@extends('layouts.service_template_01')
<?php

    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ; 
    $taxes = 0;
    $price = 0;
    $total= 0;
    $actualamount = 0;
    $offer_amount =0;
    $offer_total = 0;
    $price = 0;
    $total = $price+$taxes; 
    $actualamount = 0;
    $offer_amount = 0;
    $offer_total = $actualamount - 0;

    $total_amount =0.00;
    $total_gst =0.00;
    $total_discount =0.00;
    $actual_amount =0.00;


?>


@section('style')

<style type="text/css">

.owl-dots,  .owl-prev, button.owl-dot{

   display: none !important;
}
.disabled {
   display: none !important;
}
 

</style>
@endsection


@section('content')



<div class='breadcrumb-box'>  
    <div class='container'>  
        <div class="row mt-2"> 
           
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item text-uppercase">
                                <a class="text-weight-bold" href="{{URL::to('/appointment')}}" style="color:green;">
                                   <b>Appointment</b>
                                </a>
                            </li>
                            @if(isset($categoryname))
                            <li class="breadcrumb-item active text-uppercase" aria-current="page">
                                <a class="text-weight-bold" href="{{URL::to('/service-details')}}/{{$categoryname}}" style="color:green;">
                                   <b> {{$categoryname}} </b>

                                 </a>
                            </li>
                            @endif
                            <li class="breadcrumb-item text-uppercase">
                                <a href="{{ URL::to('/appointment/business/prepare-service-appointment') }}?bin={{ $business->id }}#contentpackages">
                                   {{ strtoupper($business->name) }}
                                </a>

                            </li> 
                            <li class="breadcrumb-item text-uppercase">
                                Calendar
                            </li> 

                        </ol>
              </nav>
          </div> 


</div>
</div>
 </div>
 
    
    <div class="container">
        <div class="row"> 
 
  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif

    <div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 br-right-md ' > 
        
        
         
    <div class=''>  

         <div class='text-left'>
            <span class='display-8'>{{ strtoupper(date('F')) }}</span>
             <button class='btn btn-xs btn-rounded btn-outline-primary'>
             <i class="fa fa-calendar" aria-hidden="true"></i> full calendar
         </button>
          </div> 

          <div class="pt-3" style='display : none;'>
          <div   id="slotcalendar"></div>
          </div> 
          <div class="mt-4"></div> 
        @php
            
            $day = date('d');
            $month = date('m');
            $year = date('Y');
            $last_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            if($day <= 15)
            {
                $enddate = $day + 10;
            }
            else
            {
                $enddate =  $last_day ;
            }

        @endphp

        @for($i = $day ; $i < $enddate ; $i++)
            @php
                $date_string = $year . '-'. $month . '-' . $i ;
                $day_name = date('D',  strtotime( $date_string ));
            @endphp

            <div class="acalday @php if($i == date('d') ) echo 'selected_day'; @endphp" data-date="{{   $date_string  }}">
                <p>
                    <span class="dayname">{{ strtoupper($day_name) }}</span><br/>
                    {{ $i }}
                </p>

            </div>
        @endfor
    </div>
    <div class='clearfix'></div>
    <hr/>

 <span class='display-8'>Packages Selected</span> <hr/>
    <table class="table"> 
      <thead  >
        <tr >
          <th scope="col">Image</th>
          <th scope="col">Package</th>
          <th scope="col">Actual Pricing</th>
          <th></th>
        </tr>
      </thead>
    <tbody>
    @php
        $slno = 1;
    @endphp
    @foreach ($packages as $package)

      @php
         
        $sub_total = ($package->pricing -  $package->discount);
        $total_amount += $package->pricing;
        $total_discount += $package->discount;
        $sub_gst = ( $sub_total * 0.01 * ( $package->cgst_pc + $package->sgst_pc )  );
        $total_gst +=$sub_gst;
        $actual_amount += $sub_total + $sub_gst;
      @endphp
     <tr >
     <td>{{ $slno }}</td>
      <td>{{ $package->srv_name }}</td> 
      <td>{{$package->actual_price}}</td>
      <td><a   class='btn btn-link red' href="{{ URL::to('/appointment/remove-package-from-cart') }}?bin={{ $business->id }}&package={{ $package->id  }}" ><i class='fa fa-trash red'></i></a></td>
    </tr>

    @php
        $slno++;
    @endphp
    @endforeach 
        

   </tbody>
  </table> 
 
        <a  href="{{ URL::to('appointment/business/prepare-service-appointment') }}?bin={{ $business->id }}#contentpackages" class='btn btn-link' >Add Additional Package</a>
 
 
    </div>

<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6 '>
    <div  id="pane_cart_summary" >  
  
                <p class='display-8 text-center'><span class=''>Choose Time Slot</span></p>
                <div class="text-center"><div id="slotloading"></div></div>

                <div id="slotarea" >
                    @if($timeslots->count() > 0 )
                        @foreach( $timeslots as $slot)
                            @if($slot->status  == "open" )
                            <div id="slbox-{{ $slot->slotNo }}" class="aptslotbox" data-slot="{{ $slot->slotNo }}" role="button">
                                {{ date('h:i', strtotime( $slot->startTime ) )   }} - {{ date('h:i A', strtotime( $slot->endTime) )  }}
                            </div>
                            @else
                            <div id="slbox-{{ $slot->slotNo }}" class="aptslotbox-disabled" data-msg="This time slot is already booked!" data-slot="{{ $slot->slotNo }}" role="button">
                              {{ date('h:i', strtotime( $slot->startTime ) )   }} - {{ date('h:i A', strtotime( $slot->endTime) )  }}
                          </div>
                          @endif
                      @endforeach
                  @endif
              </div>

              

            <hr/>
              <span class='display-8'>Available Service Staff</span> 
            <hr/>

            @foreach($staffs as $staff)

          <div class='card-image'>
            <div style='min-height: 100px'>
                @if($staff->profilePicture === null)
                <img src="https://cdn.booktou.in/assets/image/no-image.jpg" width='90px' class='rounded mx-auto d-block'/>
                @else
                <img src="{{ $staff->profilePicture }}" width='90px' class='rounded mx-auto d-block'/>
                @endif

            </div>
            <p class='h5'>{{ $staff->staffName }}</p>
            <button type='button' data-key='{{ $staff->staffId }}' class='btn btn-primary btn-xss btn-rounded btnselectstaff'>Select Staff</button>
        </div>

        @endforeach

        @foreach($selected_packages as $package)
            @php
                $total += $package->amount + $package->gst;
            @endphp
        @endforeach
        <hr>
        <input type="hidden" name="bin" id="gbin" value="{{ $business->id }}" >
        <div class="d-flex mb-3">
          <div class="p-2 flex-fill"> 
            <strong>Cart Amount (inclusive of GST) ₹ <label id="subamount">{{number_format($total,2,".","")}}</label></strong>
            <br/>
            <i class="fa fa-shield sgreen"></i> Secure Payment

        </div>
        <div class="p-2 flex-fill-right">
            <a href="{{ URL::to('/appointment/review-appointment-summary') }}?bin={{ $business->id }}" class="btn btn-primary btn-xs btn-rounded btn_bookcheckout"  >Continue</a>

        </div> 
    </div>

                     
                     

    </div>
    </div>

    </div> 

  
 
</div> 

@endsection

@section('script')
<style>


</style>

<script>
 
 
 

$(document).ready(function ()
{

    var bin = $("#gbin").val();
    var gmid = $("#gmid").html();
    var gsid = $("#gsid").html();


    var today = new Date();
    var d = String(today.getDate()).padStart(2, '0');
    var m = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var y = today.getFullYear();
    const dates = [];
      
    for(var i = 0; i < d-1; i++)
    {
        day = i+1;
        if(day<10)
          day = "0" + day;
        dates[i] = {"date": y + "-" + m + "-" + day , "badge":false, "classname": "bg-dimred disablebtn", "title":"Not available"}; 
    }

    dates[i] = {"date": y + "-" + m + "-" + d  , "badge":false, "classname": "bg-sgreen", "title":"Today"}; 




    $("#slotcalendar").zabuto_calendar({
        data: dates,
        cell_border: true,
        show_days: true,
        weekstartson: 0,
        legend: [
            {type: "block", label: "Passdays", classname: "bg-dimred"},
            {type: "block", label: "Selection", classname: "bg-light-blue"},
            {type: "block", label: "All Booked",  classname: "bg-red"}            


          ],
        nav_icon: {
          prev: '<i class="fa fa-chevron-left"></i>',
          next: '<i class="fa fa-chevron-right"></i>'
        },
        action: function () 
        {

          //prep selection
          $('.dow-clickable').removeClass("bg-light-blue");
          $('.day').removeClass("bg-light-blue");
          $("#" + this.id + "_day").addClass("bg-light-blue");
          var selectedDate = $("#" + this.id).data("date");

          return appointmentCalendarDateChanged(selectedDate, bin, gmid , gsid, false);
        } 
    });


    $(".owl-carousel").owlCarousel({
        margin:10,
        center: false,
        items:2,
        loop:false,
        navigation : false,
        autoWidth:true,
    });

});



$(document).on("click", ".aptslotbox", function(){

      $(".aptslotbox").each(function() {
        if($(this).hasClass("selected"))
        {
          $(this).removeClass("selected");
        }
      });


    var bin = $("#gbin").val();
    var gmid = $("#gmid").html();
    var slot = $(this).attr('data-slot');
    
    if($(this).hasClass("selected"))
    {
        aptTimeslotSelected(this, slot, bin, gmid );
        $(this).removeClass("selected");
    }
    else
    {
        aptTimeslotSelected(this, slot, bin, gmid );
        $(this).addClass("selected");
    }
});


$(document).on("click", ".btnselectstaff", function()
{
    var staffid = $(this).attr('data-key'); 
    var bin = $("#gbin").val();
    var gmid = $("#gmid").html();
    var json = {};
    json['bin'] = bin;
    json['staffid'] = staffid;
    var gmid = $("#gmid").html();
    staffSlotSelected(staffid, bin, gmid );
});


$(document).on("click", ".acalday", function()
{
      $(".acalday").each(function() {
        $(this).removeClass("selected_day");
      });

      $(this).addClass("selected_day");


    var bin = $("#gbin").val();
    var gmid = $("#gmid").html();
    var gsid = $("#gsid").html();
    var selectedDate = $(this).attr("data-date");
    appointmentCalendarDateChanged(selectedDate, bin, gmid , gsid, false);

});


 

</script>
 
@endsection
