@extends('layouts.store_front_02')
@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ; 
    $taxes = 0;
    $price = 0;
    $total= 0;
    $actualamount = 0;
    $offer_amount =0;
    $offer_total = 0;
    $price = $selected_service->actual_price;
    $total = $price+$taxes; 
    $actualamount = $selected_service->actual_price;
    $offer_amount = $selected_service->pricing;

    $offer_total = $actualamount - $selected_service->discount;
?>

 @auth


<section>
  <div class="container">
    <div class='row'>

        <?php

        if( $business->banner == ""  || $business->banner == null )
        {
            $image_url =   URL::to('/public/assets/image/slider_paa.jpg');
        }
        else
        {
            $image_url =  $cdn_url. $business->banner;

        }

        ?>  

        
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mt-4 float-sm-start" > 
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ $image_url }}" class="d-block w-100 rounded">
            </div> 
        </div>

        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    </div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mt-4 float-sm-end" > 
            <div class="p-5" >
                <h1 class="display-6">{{$business->name}}</h1>
                <h5 class="section-title font-weight-bold"></h5> 
                <p>
                     {{$business->landmark}} {{$business->city}},{{$business->state}}
                </p> 
                <button role='link' type='button' class='btn btn-outline-secondary btn-rounded'>
                   <i class='fa fa-check-circle'></i> Verified Merchant
                </button>
     </div> 
</div>

    </div>
    </div>
 
</section>


        



@if($selected_service->service_category=="OFFER")

 
    <div class="container">
    <div class='row'>
            <?php
                $mainmenu;
                  foreach(request()->route()->parameters as $val)
                  {
                       $mainmenu =  $val;
                  }


            ?>
  <div class='col-sm-8 col-xs-12 '>

    <div class="card">
        <div class="card-header">
          <h5>{{$selected_service->service_category}} Valid from {{date('Y-m-d',strtotime($selected_service->valid_start_date))}} to  {{date('Y-m-d',strtotime($selected_service->valid_end_date))}} </h5>
        </div>
    <div class="card-body">
    <table class="table">

        <tr>
            <td><img src="{{$selected_service->photos}}" width="70px" height="70px"></td>
            <td>{{$selected_service->srv_name}}</td>
            <td>{{$selected_service->srv_details}}</td>
            <td> ₹ {{$selected_service->pricing}}</td>
        </tr>
    </table>
    </div>
    </div>

  </div>

    <div class='col-sm-4 col-xs-12'>

    <div class="card">
        <div class="card-header text-uppercase b" style="background: #37b516; color: #fff;">
        Order Summary
        </div>
        <div class="card-body">
                <form method="post" action="{{action('StoreFront\StoreFrontController@purchaseServiceOffers')}}">
                  {{csrf_field()}}
                    <input type="hidden"  id="subModule" name="subModule" required="" value="{{$business->sub_module}}">
                    <input type="hidden"  id="totalCost" name="totalCost" required="" value="{{$offer_amount}}">
                    <input type="hidden" value="{{$selected_service->id}}" id="srvkey" name="serviceProductIds[]" required="">
                    <input type="hidden" value="1" id="qty" name="productQty[]" required="">
                   <!--  <dl class="dlist-align">
                        <span class="badge badge-success mr-3">Shop Name</span>
                        <br>
                        <span class=""> {{$business->name}}</span>

                    </dl>
                    <hr> -->

                    <dl class="dlist-align">
                        <dt>Price:</dt>
                        <dd class="text-right text-dark ml-3 text-underline"> ₹ {{$actualamount}}</dd>
                    </dl>
                    <dl class="dlist-align">
                        <dt>Discount:</dt>
                        <dd class="text-right text-dark ml-3"> ₹ {{$selected_service->discount}}</dd>
                    </dl>
                    <hr>
                    <dl class="dlist-align">
                        <dt>Total Amount:</dt>
                        <dd class="text-right text-dark ml-3"> ₹ {{$offer_amount}}</dd>
                    </dl>
                    <dl class="dlist-align">
                        <dt>Taxes:</dt>
                        <dd class="text-right text-dark ml-3"><strong>₹ {{$taxes}}</strong></dd>
                    </dl>

                    <hr>

                    <dl class="dlist-align">
                        <dt>To pay:</dt>
                        <dd class="text-right text-dark ml-3"><strong>₹ {{$offer_amount + $taxes}}</strong></dd>
                        <input type="hidden" name="serviceDate" id="offerServiceDate" required="">
                        <input type="hidden" name="serviceTimeSlots" id="serviceTimeSlots" required="">
                        <input type="hidden" name="bin" id="bin" value="{{$business->id}}" required="">
                    </dl>
                    <hr>
                    <dl class="dlist-align">
                        <input type="text" name="custRemarks" placeholder="customer Remarks"
                        class="form-control">
                    </dl>

                    <hr>
                    <input type='hidden' value="{{ $business->id }}"  name="key" />
                    <button type="submit" class="btn btn-primary btn-block btn_bookcheckout" value="btn_placeOrder" name="btnorder">Book Now</button>
                </form>
        </div>
    </div>


    </div>
    </div> 
    </div> 
 
@else

<div class='hr-md mt-3'></div>
<div class="container">
    <div class='row'>  
        
    <div class='col-xs-12 col-sm-12 col-md-7 col-lg-7 br-right-md p-4'>
        <p class="display-5">Services</p>  
      @if( isset($service_products))  
        @foreach($service_products as $item)  
        <div class="d-flex mb-2">
          <div class="flex-shrink-0">
            @if( $item->photos != "") 
                    @php
                    $imageurl = $item->photos;
                    @endphp
                    <img src="{{ $item->photos }}" class="align-self-end mr-3 rounded" width="90px" alt="{{ $item->srv_name }}">  
                @else 
                    @if( count($service_profiles) > 0) 
                        @foreach($service_profiles as $service_profile)
                            @if(  $service_profile->service_product_id  == $item->id  )
                                <img src="{{ $service_profile->photos }}" class="align-self-start mr-3" width="90px" alt="{{ $item->srv_name }}">
                                @break
                            @endif 
                        @endforeach  
                    @endif 
            @endif
          </div>
          <div class="flex-grow-1 ms-3">
            <p  class="lead">
                {{ $item->srv_name }}<br/> 

                ₹ {{ $item->actual_price }}<br/>

            </p>
          </div>
          <div class="p-2  ">
            @if( $selected_service->id == $item->id  )
                <button type="button"  class='btn btn-sm btn-rounded btn-mw btn-outline-danger btnselectitems' 
                data-status="rem" 
                data-key="{{ $item->id  }}" 
                data-srvname="{{ $item->srv_name }}" data-price="{{ $item->actual_price }}" 
                data-details="{{ $item->srv_details }}">Remove</button>
            @else 
                <button type="button"  class='btn btn-sm btn-rounded btn-mw btn-outline-dark btnselectitems' 
                data-status="add" 
                data-key="{{ $item->id  }}" 
                data-srvname="{{ $item->srv_name }}" data-price="{{ $item->actual_price }}" 
                data-details="{{ $item->srv_details }}">Add Service</button>
            @endif
            
        </div>

        </div>

 
 
  
        @endforeach    

      @else

      @endif
 
</div>

<?php
    $mainmenu;
    foreach(request()->route()->parameters as $val)
    {
       $mainmenu =  $val;
    }
?>


  <div class='col-sm-5 col-xs-12 col-md-5 p-4'> 
    <div class="mb-2">
        <div class="">
            <div id="slotcalendar"></div> 

      @if (session('err_msg'))

       <div class="alert alert-info">
        {{ session('err_msg') }}
       </div>

       
      @endif
      <form method="post" action="{{action('StoreFront\StoreFrontController@makeServiceBooking')}}" enctype="multipart/form-data">

            {{csrf_field()}} 

              <div class="row"> 
                <div class="col-md-12">  
                <div id='de_service_hours'> 
                </div> 
                </div> 


                <div class="mb-3 row">
                    <label for="preferredTime" class="col-sm-8 col-form-label">Your preferred time of visit:</label>
                    <div class="col-sm-4">
                      <input type="text" id="preferredTime" class="form-control form-control-xs flex" name="preferredTime" placeholder="10:00:AM" required>  
                    </div>
                  </div> 
                </div>  
                <hr>
                    <input type="hidden"  id="preferredTime" name="preferredTime"  required=""> 
                    <input type="hidden"  id="endtimeslot" name="endTime" required="">
                    <input type="hidden"  id="staffId" name="staffId" required="">
                    <input type="hidden"  id="subModule" name="subModule" required="" value="{{$business->sub_module}}">
                  
                    <div class="addservicelist">
                        <input class="serviceproducts" id="dgkey{{$selected_service->id}}" type="hidden" value="{{$selected_service->id}}"  name="serviceProductIds[]" > 
                    </div>  
                    <div>
                    <dl class="dlist-align">
                        <input type="text" name="bookingRemark" placeholder="Booking Remarks"
                        class="form-control">
                    </dl>
                    </div>
                    <hr>
                    <div>
                        <dl>
                        <input type="file" name="attachment"   class="form-control" placeholder="add an attachment" >
                        </dl>
                        <small>Upload file is optional</small>
                    </div>
                    <hr>
                    <input type="hidden" id="total" value="{{$total}}">
                    <input type="hidden" id="subtotal" value="{{$total}}">
                    <input type="hidden" name="serviceDate" id="bookingServiceDate" required="">
                    <input type="hidden" name="serviceTimeSlot" id="serviceTimeSlots" required="">
                    <input type="hidden" name="bin" id="bin" value="{{$business->id}}" required="">
 
                    <div class="d-flex mb-3">
                      <div class="p-2 flex-fill"> 
                        <strong>Cart Amount ₹ <label id="subamount">{{number_format($total,2,".","")}}</label></strong>
                    <br/>
                    <i class="fa fa-shield blue"></i> Secure Payment
                          
                      </div>
                      <div class="p-2 flex-fill">
                        <button type="submit" class="btn btn-primary btn-rounded btn_bookcheckout" value="btn_placeOrder" name="btnorder" disabled>Book Now</button>

                    </div> 
                  </div>

                    
                   <input type='hidden'  value="{{ $business->id }}"  name="key" />
                    
                </form>
        </div>
    </div>


    </div>
    </div>

    </div> 

@endif
<!-- service checking ends here -->

@endauth

@guest
<div class='outer-top-tm'>
    <div class="container">
      <div class="col-md-6 offset-md-3">
      <form action="{{action('Auth\LoginController@customerLogin')}}" method="post">
        {{ csrf_field() }}
     <input type="hidden" id="srvukey" name="referer" value="{{ $selected_service->id }}">
     <input type="hidden"  name="key" value="{{ $business->id }}">

     <input type="hidden" id="module_name" name="module_name"
     value="<?php echo session()->get('module_active'); ?>" required>
     <div class="card">
       <div class="card-header">
         <h5 class="modal-title" id="staticBackdropLabel">Customer Login</h5>
       </div>
       <div class="card-body">
         <div class="row">
          <div class="col-12 mb-3">
                                     <label for="phone">Phone Number or Email: <span>*</span></label>
                                     <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number or email'>
                                 </div>


                                 <div class="col-12 mb-4">
                                     <label for="email_address">Password: <span>*</span></label>
                                     <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password'>
                                 </div>
                             </div>
       </div>
       <div class="card-footer">

         <div class="row col-12">
        <div class=" col-4">
         <button type="submit" class="btn btn-primary btn-block" value="login" name="login">Login</button>
        </div>

        <div class=" col-8">
             <p class="mt-1">
            <small>New Member? Please signup here</small>
            <a target="_blank" class="btn btn-info btn-sm" href="{{URL::to('join-and-refer')}}">Sign up</a>
         </p>

         </div>
         </div>

       </div>
     </div>
     </form>
</div>
    </div>
</div>
@endguest


<!-- spinner -->
<div id="cover-spin"></div>



<!-- modal for service category -->


<div class="modal fade serviceModal" id="serviceModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">

    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
            <span class="badge badge-primary badge-pill">Service Provided by {{$business->name}}</span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-md-6">
                  <select name="serviceCategory" class="form-control serviceCategory" required>
                      <option value="">Select Service Category</option>
                      @foreach($service_products as $item)
                        <option value="{{$item->service_category}}">{{ $item->service_category}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-md-6">
                    <input type="text" name="searchFilter" class="form-control filterservice" placeholder="Filter service name">
                </div>



             <div class="col-md-12 serviceNameResult" id="loading">


             </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- modal service category -->

@endsection

@section('script')
<style>


</style>

<script>
  

$(function(){
    $('[data-toggle="tooltip"]').tooltip();
})

var d = '/api'; 
var siteurl = "<?php echo config('app.api_url');  ?>"  ; 
var today = new Date();
var yesterday = new Date(today);
yesterday.setDate(today.getDate() -1);
var current_date = yesterday.toISOString().split('T')[0];

$(document).on('click','.btn-showservice',function(){
  $('.serviceModal').modal('show');
}); 


$('#preferredTime').timepicker({ 'timeFormat': 'h:i A' });


$('#preferredTime').on('change', function() { 
  var pref_time = $(this).val();  
  var ts_preftime = new Date("<?php echo date('F d, Y') ?> " + pref_time);
  ts_preftime = ts_preftime.getTime();
 

  var sh = $(".service_hour").html();
  var time_parts = sh.split("-");
  if(time_parts.length >= 2 )
  {
    var start_time =  time_parts[0];
    var end_time =  time_parts[1];
    //convert both time into timestamp
    var ts_start = new Date("<?php echo date('F d, Y') ?> " + start_time);
    ts_start = ts_start.getTime();
    var ts_end = new Date("<?php echo date('F d, Y') ?> " + end_time);
    ts_end = ts_end.getTime();

    if(ts_start <= ts_preftime &&  ts_preftime <= ts_end) {
      $(".btn_bookcheckout").prop("disabled", false);
    }
    else
    {
      $(".btn_bookcheckout").prop("disabled", true);
    }

  }  
});


$(document).on("click", ".btnselectitems", function()
{
    var key = $(this).attr('data-key');
    var srvname = $(this).attr('data-srvname');
    var details = $(this).attr('data-details');
    var price = $(this).attr('data-price');
    var total = $('#total').val();

    var cStatus = $(this).attr("data-status" ).toLocaleLowerCase();
    

    if(cStatus === "rem")
    {
        var subamount =  parseFloat(total) - parseFloat(price)   ; 
        //var subtotal = $('#subtotal').val(); 
        //var subamount = total - price ;
        $('#total').val(subamount);
        $('#subtotal').val(subamount);
        $('#totalamount').text(subamount);
        $('#subamount').text(subamount);

        $(this).html("Add Service");
        $(this).attr("data-status", "add");
        $(this).removeClass("btn-outline-danger");
        $(this).addClass("btn-outline-dark "); 
        $('#dgkey' + key ).remove();

    }
    else 
    {
        var subamount = parseFloat(price) + parseFloat(total) ; 

        $('#total').val(subamount);
        $('#subtotal').val(subamount);
        $('#totalamount').text(subamount);
        $('#subamount').text(subamount);

        $(this).html("Remove");
        $(this).attr("data-status", "rem");
        $(this).addClass("btn-outline-danger");
        $(this).removeClass("btn-outline-dark ");
        $('.addservicelist').last().
        append('<input type="hidden" value="'+key+'" id="dgkey'+key+'" name="serviceProductIds[]" />');
    }
    
});



 



$(document).ready(function ()
{
    $("#slotcalendar").zabuto_calendar({
        action: function () {
            return calendarDateChange(this.id, false);
        } 
    });
});


function calendarDateChange(id, fromModal) 
{
    <?php
    $bin = $business->id;
    ?>
    selectedDate = $("#" + id).data("date");    
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var d = new Date(selectedDate);
    var day = days[d.getDay()];
    var dayName = day.toUpperCase();
    var guid = $("#guid").text();
    $("#bookingServiceDate").val(selectedDate);

    startloading(); 
    var json = {};
    var productlistid = []; 
    $(document).find("input[name='serviceProductIds[]']").each(function() {
        productlistid.push($(this).val());
    }); 
    json["serviceProductIds"] = productlistid ;
    json['dayName'] =  dayName;
    json['guid'] =  guid;
    json['date'] = selectedDate;
    json['bin'] = <?php echo $bin;?>;
    json['month'] = <?php echo date('m');?>;
    json['year'] = <?php echo date('Y');?>;
    console.log(json);

    $.ajax({
      type: 'post',
      url: siteurl + "/v3/web/booking/check-date-availability" ,
      data: json,
      success: function(data)
      {
        if(data.status_code == 3021)
        {
          $("#de_service_hours").html(""); 
          $("#de_service_hours").append("<strong>Service Day:</strong> " + data.day_name  ); 
          $("#de_service_hours").append(" <strong>Service Hours:</strong> ");  
          $.each(data.business_hours, function (i, v) {
            $("#de_service_hours").append("<span class='tsbox-gray-xs service_hour'>" + v.shifts+ "</span>"); 
        });
          $("#de_service_hours").append("<hr/>");  

          $(".btn_bookcheckout").prop("disabled", true);
          exitloading();
      }
      else
      {
        exitloading();
        }
      },
      error: function()
      {
        alert(  'Something went wrong, please try again')
    }
});

}


 
      

</script>


<style type="text/css">
.btn-primary.disabled,
.btn-primary:disabled {
 color:#000000 !important;
 background-color:#efefef !important;
 border-color:#fff !important;
}

.how-it-work {

    position: relative;
}

.how-it-work .panel {
    color:#fff;
    font-size: 16px;
    background: #37b516;
   /* box-shadow: 0px 1px 7px 1px rgba(0,0,0,0.4);
   */
}
.how-it-work .panel:nth-child(even) {
    background: #78c043;
}
.how-it-work .panel-body {
    overflow-y: auto;
    overflow: visible;
    height: 70px;
    padding-left: 70px;
    position: relative;
}

.badge.badge-info.badge-step
{
  width: 35px;
  height: 35px;
  color: #008931;
  background: #f6f6f6;
  text-align: center;
  font-size: 1.3em;
  font-weight: 800;
  border-radius: 50%;
  box-shadow: 0px 2px 10px rgb(0 0 0 / 40%);
}


.how-it-work .step-heading {
    font-size: 20px;
    margin-top: 0px;
    color: #fff;
    padding-top:5px;
}

.dateselection .pignose-calendar
{
    max-width: 100% !important;
    border: none !important;
    box-shadow: none !important;
}

.dateselection .pignose-calendar .pignose-calendar-top
{
  padding: 1em 0 !important;
}

.dateselection .pignose-calendar .pignose-calendar-top .pignose-calendar-top-month
{
  font-weight: normal;
}

.dateselection span.pignose-calendar-top-month
{
  display:inline !important;
  font-size: 110% !important;
}

.dateselection span.pignose-calendar-top-year {
    display: inline !important;
}

.dateselection .pignose-calendar-top-date {
    padding: 1em 0 !important;
}

.list-service li
{
  border-bottom:1px solid #efefef;
  margin-bottom: 10px;
}
.list-service img
{
  border-radius: 10px;
}
.addservicelist dl
{
  border-bottom:1px solid #efefef;
  margin-bottom:15px; 
}

span.tsbox-gray-xs {
    display: inline-block;
    border: 1px solid #cec9c9;
    padding: 5px;
    border-radius: 3px;
    font-size: 12px;
    margin-right: 10px;
}




</style>
@endsection
