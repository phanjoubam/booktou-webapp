@extends('layouts.service_template_01')

@section('style')

<style type="text/css">

.owl-dots,  .owl-prev, button.owl-dot{

   display: none !important;
}
.disabled {
   display: none !important;
}
</style>
@endsection


@section('content')
<?php
    $cdn_url =    config('app.app_cdn') ;
    $cdn_path =     config('app.app_cdn_path') ; 
    $taxes = 0;
    $price = 0;
    $total= 0;
    $actualamount =  $offer_amount = $offer_total =  $price = $total = 0;
?>

 
 
<section>
  <div class="container">
    <div class='row'>

        <?php

        if( $business->banner == ""  || $business->banner == null )
        {
            $image_url =   URL::to('/public/assets/image/slider_paa.jpg');
        }
        else
        {
            $image_url =  $cdn_url. $business->banner;

        }

        ?>  

        
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4 " >
    <div style="width:100%; height: 350px; border-radius: 10px; background-image: url('{{ $image_url }}'); background-size:cover" > 
    </div> 
</div> 
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4 float-sm-end" > 
            <div class="p-5" >
                <h1 class="display-6">{{$business->name}}</h1>
                <h5 class="section-title font-weight-bold"></h5> 
                <p>
                     {{$business->landmark}} {{$business->city}},{{$business->state}}
                </p> 
                <button role='link' type='button' class='btn btn-outline-secondary btn-rounded'>
                   <i class='fa fa-check-circle'></i> Verified Merchant
                </button>
     </div> 
</div>

    </div>
    </div>
</section>


<div class='hr-md mt-3'></div>
<div class="nav_bar_categories" id="navbar_top" >
<div class="container " >
    <div class='row'>
        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 p-3' id="contentpackages"> 
                <div class="owl-carousel owl-theme">

                    @foreach( $package_categories as $package_category)
                     <a class="filter-box" 
                href="{{ URL::to('/appointment/business/prepare-service-appointment') }}?bin={{ $business->id }}&category={{ strtolower($package_category->category ) }}#contentpackages">
                 <img width="20px" src="{{ $package_category->icon_url}}"   > {{ ucwords( strtolower($package_category->category ) ) }}</a>
                    @endforeach
                </div>
        </div>
    </div>
</div>
</div>
<div class='hr-md '></div>
 <div class="mt-4"></div>
<div class="container">
    <div class="row">
        
         
            @if( isset($packages))  
            @foreach($packages as $item)
       

                @if( $item->photos != "") 
                    @php
                    $imageurl = $item->photos;
                    @endphp
                 
                @else 
                    @if( count($package_specifications) > 0) 
                        @foreach($package_specifications as $package_specification)
                            @if(  $package_specification->service_product_id  == $item->id  )
                            <div class="col-sm-6 col-lg-3">
    
                <div class="card no-border card-img-scale overflow-hidden bg-transparent mb-3">    
                  
                    <div class="position-absolute top-0 end-0 p-3 z-index-9">
                        <a tabindex="0" class="mb-0 btn btn-white btn-round" data-bs-toggle="popover" data-bs-trigger="focus" data-bs-placement="left" data-bs-content="Appointment Packages">
                            <i class="bi bi-info-circle"></i>
                        </a>
                    </div>

               
                    <div class="card-img-scale-wrapper rounded-3">
                        <img src="{{ $package_specification->photos }}" class="card-img" alt="{{ $item->srv_name }}">
                    </div>

                  
                    <div class="card-body px-2">
                        <!-- Title -->
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">
                                <a href="{{ URL::to('/appointment/business/appointment-calendar') }}?bin={{ $item->bin }}&package={{ $item->id  }}" class="stretched-link">{{ $item->srv_name }}</a>
                            </h5>
                            <h6 class="mb-0">0.0<i class="fas fa-star text-warning ms-1"></i></h6>
                        </div>
                        <!-- Content -->
                        <span class="mb-0">Starts from ₹ {{ $item->actual_price }}</span>
                    </div>
                </div>
            </div>
                            @break
                            @endif 
                        @endforeach  
                    @endif 
                @endif

 
 
        @endforeach    
 
        @endif  
 
 
</div> 
 
</div> 
 
 


<!-- spinner -->
<div id="cover-spin"></div>
<!-- modal for service category -->


<div class="modal fade serviceModal" id="serviceModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">

    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
            <span class="badge badge-primary badge-pill">Service Provided by {{$business->name}}</span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-md-6">
                  <select name="serviceCategory" class="form-control serviceCategory" required>
                      <option value="">Select Service Category</option>
                      @foreach($packages as $item)
                        <option value="{{$item->service_category}}">{{ $item->service_category}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-md-6">
                    <input type="text" name="searchFilter" class="form-control filterservice" placeholder="Filter service name">
                </div> 
             <div class="col-md-12 serviceNameResult" id="loading"> 
             </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- modal service category -->

@endsection

@section('script')


<script>


var lastScrollY = 0;
$(window).scroll(function(event)
{
   var st = $(this).scrollTop(); 
   lastScrollY = st;
   if (lastScrollY > 440)
   {
    $("#navbar_top").addClass("fixed-top bg-gray");
    catbar_height = document.querySelector('.nav_bar_categories').offsetHeight;
    document.body.style.paddingTop = catbar_height + 'px';
    fixedpane_width = $("#pane_cart_summary").width();
    $("#pane_cart_summary").addClass("fixed-pane");
    $('#pane_cart_summary').css('width',  fixedpane_width + 'px');

   }
   else
   {
    $("#navbar_top").removeClass('fixed-top bg-gray');
    $("#pane_cart_summary").removeClass("fixed-pane");
    document.body.style.paddingTop = '0';
   }
 


});
 

$(function(){
    $('[data-toggle="tooltip"]').tooltip();
})
 

var today = new Date();
var yesterday = new Date(today);
yesterday.setDate(today.getDate() -1);
var current_date = yesterday.toISOString().split('T')[0];

$(document).on('click','.btn-showservice',function(){
  $('.serviceModal').modal('show');
}); 


$('#preferredTime').timepicker({ 'timeFormat': 'h:i A' });


$('#preferredTime').on('change', function() {

  var pref_time = $(this).val();  
  var ts_preftime = new Date("<?php echo date('F d, Y') ?> " + pref_time);
  ts_preftime = ts_preftime.getTime();
 

  var sh = $(".service_hour").html();
  var time_parts = sh.split("-");
  if(time_parts.length >= 2 )
  {
    var start_time =  time_parts[0];
    var end_time =  time_parts[1];
    //convert both time into timestamp
    var ts_start = new Date("<?php echo date('F d, Y') ?> " + start_time);
    ts_start = ts_start.getTime();
    var ts_end = new Date("<?php echo date('F d, Y') ?> " + end_time);
    ts_end = ts_end.getTime();

    if(ts_start <= ts_preftime &&  ts_preftime <= ts_end) {
      $(".btn_bookcheckout").prop("disabled", false);
    }
    else
    {
      $(".btn_bookcheckout").prop("disabled", true);
    }

  }  
});


$(document).on("click", ".btnselectitems", function()
{
    var key = $(this).attr('data-key');
    var srvname = $(this).attr('data-srvname');
    var details = $(this).attr('data-details');
    var price = $(this).attr('data-price');
    var total = $('#total').val();

    var cStatus = $(this).attr("data-status" ).toLocaleLowerCase();
     
    if(cStatus === "rem")
    {
        var subamount =  parseFloat(total) - parseFloat(price) ; 
        $('#total').val(subamount);
        $('#subtotal').val(subamount);
        $('#totalamount').text(subamount);
        $('#subamount').text(subamount);

        $(this).html("Add Service");
        $(this).attr("data-status", "add");
        $(this).removeClass("btn-outline-danger");
        $(this).addClass("btn-outline-dark "); 
        $('#dgkey' + key ).remove();

    }
    else 
    {
        var subamount = parseFloat(price) + parseFloat(total) ;
        $('#total').val(subamount);
        $('#subtotal').val(subamount);
        $('#totalamount').text(subamount);
        $('#subamount').text(subamount);
        $(this).html("Remove");
        $(this).attr("data-status", "rem");
        $(this).addClass("btn-outline-danger");
        $(this).removeClass("btn-outline-dark ");
        $('.addservicelist').last().
        append('<input type="hidden" value="'+key+'" id="dgkey'+key+'" name="packageNos[]" />');
    }

    if(subamount == 0)
    {
        $('#accordionBookingcollapseOne').collapse('toggle');
    }

    
});



$(document).ready(function ()
{ 

    $("#slotcalendar").zabuto_calendar({
        data: [
              {"date":"2022-12-11", "badge":false, "classname": "bg-red", "title":"Today"}
            ],
        cell_border: true,
        show_days: true,
        weekstartson: 0,
        legend: [
            {type: "block", label: "Booking Available", classname: "bg-green"},
            {type: "block", label: "All Booked",  classname: "bg-red"},
            {type: "block", label: "Some Free Slots",  classname: "bg-light-yellow"}
          ],
        nav_icon: {
          prev: '<i class="fa fa-chevron-left"></i>',
          next: '<i class="fa fa-chevron-right"></i>'
        },
        action: function () {
            return calendarDateChange(this.id, false);
        } 
    });
 

    $(".owl-carousel").owlCarousel({
        margin:10,
        center: false,
        items:2,
        loop:false,
        navigation : false,
        autoWidth:true,
    }); 

});


function calendarDateChange(id, fromModal) 
{
    <?php
    $bin = $business->id;
    ?>
    selectedDate = $("#" + id).data("date");    
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var d = new Date(selectedDate);
    var day = days[d.getDay()];
    var dayName = day.toUpperCase();
    var guid = $("#guid").text();
    $("#bookingServiceDate").val(selectedDate);

    startloading(); 
    var json = {};
    var productlistid = []; 
    $(document).find("input[name='packageNos[]']").each(function() {
        productlistid.push($(this).val());
    }); 
    json["serviceProductIds"] = productlistid ;
    json['dayName'] =  dayName;
    json['guid'] =  guid;
    json['date'] = selectedDate;
    json['bin'] = <?php echo $bin;?>;
    json['month'] = <?php echo date('m');?>;
    json['year'] = <?php echo date('Y');?>;


    $.ajax({
      type: 'post',
      url: api + "/v3/web/appointment/check-date-availability" ,
      data: json,
      success: function(data)
      {
        if(data.status_code == 3021)
        {
            $.each(data.business_hours, function (i, v) {
            $("#de_service_hours").append("<span class='badge rounded-pill text-bg-dark service_hour'>" + v.shifts+ "</span>"); 
            });
          $("#de_service_hours").append("<hr/>");  

          $(".btn_bookcheckout").prop("disabled", true);


        $("#de_available_staffs").html("");
        $.each(data.staffs, function (i, v) {

            if(v.profilePicture === null)
            {
                var imgUrl = "https://cdn.booktou.in/assets/image/no-image.jpg";
            }
            else
            {
                var imgUrl = v.profilePicture;
            }
            
            var profile_car = "<div class='card-image'>"; 
            profile_car += "<div style='min-height: 100px'><img src='" + imgUrl  +"' width='90px' class='rounded mx-auto d-block'/></div>";
            profile_car += "<p class='h5'>" + v.staffName  +"</p>"; 
            profile_car += "<button type='button' data-staffkey='" + v.staffId  + "' class='btn btn-primary btn-xss btn-rounded btnselectstaff'>Select Staff</button>"; 
            profile_car += "</div>";
            $("#de_available_staffs").append( profile_car ); 
            
        });


        $("#de_available_slots").html("");
        $.each(data.available_slots, function (i, v) {

            var slot_elem = `<label class="lblslot rb-slot" data-slot="` + v.slot_number + `"><input type="radio" class="radio-inline"  name="rbslots" value="` + v.slot_number + `"><span class="outside"><span class="inside"></span></span>` 
            +  v.start_time + " - " + v.end_time + `</label>`;
            $("#de_available_slots").append( slot_elem ); 
            
        });

        $('#accordionBookingcollapseTwo').collapse('toggle');

        exitloading();
      }
      else
      {
        exitloading();
        }
      },
      error: function()
      {
        alert(  'Something went wrong, please try again')
    }
});

}




$('body').on('click', 'label.lblslot', function() {

    var s = $(this).attr("data-slot"); 
    $(":radio[value=" + s + "]").prop('checked',true); 
    $("#serviceTimeSlot").val(s);

    $('#accordionBookingcollapseThree').collapse('toggle');

 });



$('body').on('click', 'button.btnselectstaff', function() {
 
    var d = $("#bookingServiceDate").val();
    var guid = $("#guid").text();
    var s = $('input[name="rbslots"]:checked').val();
    var u = $(this).attr("data-staffkey");

    $("#staffId").val(u);

    startloading(); 
    var json = {};
    json['guid'] =  guid;
    json['date'] = selectedDate;
    json['bin'] = <?php echo $bin;?>;
    json['staffId'] = u;
    json['slotNo'] = s;
    console.log(json);

    $.ajax({
      type: 'post',
      url: api + "/v3/web/appointment/check-staff-availability" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON( data ); 
        if(data.status_code == 1091)
        {

            if(data.is_available === "open")
          {
              $(".btn_bookcheckout").prop("disabled", false);
          }
          else
          {
              $(".btn_bookcheckout").prop("disabled", true);
          }
        exitloading();
      }
      else
      {
        exitloading();
        }
      },
      error: function()
      {
        alert(  'We could not process your request. Please retry after checking your internet connection.')
    }
});

});
      

</script>

 
@endsection
