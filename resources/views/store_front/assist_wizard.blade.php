@extends('layouts.store_front_mobile_first')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
?>

<div class="row ">
  <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3 ">
      
<div class="page-content" style="background-image: url('images/wizard-v3.jpg')">
<div class="wizard-v3-content">
<div class="wizard-form">
<div class="wizard-header">
<h3 class="heading">Sign Up Your User Account</h3>
<p>Fill all form field to go next step</p>
</div>
<form class="form-register" action="#" method="post">
<div id="form-total">
<div class="steps clearfix"><ul role="tablist"><li role="tab" aria-disabled="false" class="first done" aria-selected="false"><a id="form-total-t-0" href="#form-total-h-0" aria-controls="form-total-p-0"><div class="title">
<span class="step-icon"><i class="zmdi zmdi-account"></i></span>
<span class="step-text">About</span>
</div></a></li><li role="tab" aria-disabled="false" class="done" aria-selected="false"><a id="form-total-t-1" href="#form-total-h-1" aria-controls="form-total-p-1"><div class="title">
<span class="step-icon"><i class="zmdi zmdi-lock"></i></span>
<span class="step-text">Personal</span>
</div></a></li><li role="tab" aria-disabled="false" class="done" aria-selected="false"><a id="form-total-t-2" href="#form-total-h-2" aria-controls="form-total-p-2"><div class="title">
<span class="step-icon"><i class="zmdi zmdi-card"></i></span>
<span class="step-text">Payment</span>
</div></a></li><li role="tab" aria-disabled="false" class="last current" aria-selected="true"><a id="form-total-t-3" href="#form-total-h-3" aria-controls="form-total-p-3"><span class="current-info audible"> </span><div class="title">
<span class="step-icon"><i class="zmdi zmdi-receipt"></i></span>
<span class="step-text">Confirm</span>
</div></a></li></ul></div>
<h2>
<span class="step-icon"><i class="zmdi zmdi-account"></i></span>
<span class="step-text">About</span>
</h2>
<section>
<div class="inner">
<h3>Account Information:</h3>
<div class="form-row">
<div class="form-holder form-holder-2">
<label class="form-row-inner">
<input type="text" name="email" id="email" class="form-control" required>
<span class="label">Email Address</span>
<span class="border"></span>
</label>
</div>
</div>
<div class="form-row">
<div class="form-holder form-holder-2">
<label class="form-row-inner">
<input type="text" class="form-control" id="username" name="username" required>
<span class="label">Username</span>
<span class="border"></span>
</label>
</div>
</div>
<div class="form-row">
<div class="form-holder form-holder-2">
<label class="form-row-inner">
<input type="password" name="password_1" id="password_1" class="form-control" required>
<span class="label">Password</span>
<span class="border"></span>
</label>
</div>
</div>
<div class="form-row">
<div class="form-holder form-holder-2">
<label class="form-row-inner">
<input type="password" name="comfirm_password_1" id="comfirm_password_1" class="form-control" required>
<span class="label">Comfirm Password</span>
<span class="border"></span>
</label>
</div>
</div>
</div>
</section>

<h2>
<span class="step-icon"><i class="zmdi zmdi-lock"></i></span>
<span class="step-text">Personal</span>
</h2>
<section>
<div class="inner">
<h3>Personal Information:</h3>
<div class="form-row">
<div class="form-holder">
<label class="form-row-inner">
<input type="text" class="form-control" id="first_name" name="first_name" required>
<span class="label">First Name*</span>
<span class="border"></span>
</label>
</div>
<div class="form-holder">
<label class="form-row-inner">
<input type="text" class="form-control" id="last_name" name="last_name" required>
<span class="label">Last Name*</span>
<span class="border"></span>
</label>
</div>
</div>
<div class="form-row">
<div id="radio">
<label>Gender*:</label>
<input type="radio" name="gender" value="male" checked class="radio-1"> Male
<input type="radio" name="gender" value="female"> Female
</div>
</div>
<div class="form-row form-row-date">
<div class="form-holder form-holder-2">
<label for="date" class="special-label">Date of Birth*:</label>
<select name="date" id="date">
<option value="Day" disabled selected>Day</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
</select>
<select name="month" id="month">
<option value="Month" disabled selected>Month</option>
<option value="Feb">Feb</option>
<option value="Mar">Mar</option>
<option value="Apr">Apr</option>
<option value="May">May</option>
</select>
<select name="year" id="year">
<option value="Year" disabled selected>Year</option>
<option value="2017">2017</option>
<option value="2016">2016</option>
<option value="2015">2015</option>
<option value="2014">2014</option>
<option value="2013">2013</option>
</select>
</div>
</div>
<div class="form-row">
<div class="form-holder form-holder-2">
<label class="form-row-inner">
<input type="text" class="form-control" id="phone" name="phone" required>
<span class="label">Phone Number*</span>
<span class="border"></span>
</label>
</div>
</div>
<div class="form-row">
<div class="form-holder form-holder-1">
<label class="form-row-inner">
<input type="text" class="form-control" id="address" name="address" required>
<span class="label">Address*</span>
<span class="border"></span>
</label>
</div>
</div>
</div>
</section>

<h2>
<span class="step-icon"><i class="zmdi zmdi-card"></i></span>
<span class="step-text">Payment</span>
</h2>
<section>
<div class="inner">
<h3>Payment Information:</h3>
<div class="form-row">
<div class="form-holder form-holder-2">
<input type="radio" name="radio1" id="pay-1" value="pay-1" checked>
<label class="pay-1-label" for="pay-1"><img src="images/wizard_v3_icon_1.png" alt="pay-1">Credit Card</label>
<input type="radio" name="radio1" id="pay-2" value="pay-2">
<label class="pay-2-label" for="pay-2"><img src="images/wizard_v3_icon_2.png" alt="pay-2">Paypal</label>
</div>
</div>
<div class="form-row">
<div class="form-holder form-holder-2">
<label class="form-row-inner">
<input type="text" class="form-control" id="holder" name="holder" required>
<span class="label">Holder Name*</span>
<span class="border"></span>
</label>
</div>
</div>
<div class="form-row">
<div class="form-holder">
<label class="form-row-inner">
<input type="text" class="form-control" id="card" name="card" required>
<span class="label">Card Number*</span>
<span class="border"></span>
</label>
</div>
<div class="form-holder">
<label class="form-row-inner">
<input type="text" class="form-control" id="cvc" name="cvc" required>
<span class="label">CVC*</span>
<span class="border"></span>
</label>
</div>
</div>
<div class="form-row form-row-date form-row-date-1">
<div class="form-holder form-holder-2">
<label for="date" class="special-label">Expiry Date*:</label>
<select name="month_1" id="month_1">
<option value="Month" disabled selected>Month</option>
<option value="Feb">Feb</option>
<option value="Mar">Mar</option>
<option value="Apr">Apr</option>
<option value="May">May</option>
</select>
<select name="year_1" id="year_1">
<option value="Year" disabled selected>Year</option>
<option value="2017">2017</option>
<option value="2016">2016</option>
<option value="2015">2015</option>
<option value="2014">2014</option>
<option value="2013">2013</option>
</select>
</div>
</div>
</div>
</section>

<h2>
<span class="step-icon"><i class="zmdi zmdi-receipt"></i></span>
<span class="step-text">Confirm</span>
</h2>
<section>
<div class="inner">
<h3>Confirm Details:</h3>
<div class="form-row table-responsive">
<table class="table">
<tbody>
<tr class="space-row">
<th>Full Name:</th>
<td id="fullname-val"></td>
</tr>
<tr class="space-row">
<th>Email Address:</th>
<td id="email-val"></td>
</tr>
<tr class="space-row">
<th>Phone Number:</th>
<td id="phone-val"></td>
</tr>
<tr class="space-row">
<th>User:</th>
<td id="username-val"></td>
</tr>
<tr class="space-row">
<th>Gender:</th>
<td id="gender-val"></td>
</tr>
<tr class="space-row">
<th>Address:</th>
<td id="address-val"></td>
</tr>
<tr class="space-row">
<th>Card Type:</th>
<td id="pay-val">Credit Card</td>
</tr>
</tbody>
</table>
</div>
</div>
</section>
</div>
</form>
</div>
</div>
</div>               
  </div>
 </div>

@endsection 

@section('script')

  

<script>
 
 $(function(){$("#form-total").steps({headerTag:"h2",bodyTag:"section",transitionEffect:"fade",enableAllSteps:true,autoFocus:true,transitionEffectSpeed:500,titleTemplate:'<div class="title">#title#</div>',labels:{previous:'Previous',next:'Next Step',finish:'Submit',current:''},onStepChanging:function(event,currentIndex,newIndex){var fullname=$('#first_name').val()+' '+$('#last_name').val();var email=$('#email').val();var phone=$('#phone').val();var username=$('#username').val();var gender=$('form input[type=radio]:checked').val();var address=$('#address').val();$('#fullname-val').text(fullname);$('#email-val').text(email);$('#phone-val').text(phone);$('#username-val').text(username);$('#address-val').text(address);$('#gender-val').text(gender);return true;}});$("#date").datepicker({dateFormat:"MM - DD - yy",showOn:"both",buttonText:'<i class="zmdi zmdi-chevron-down"></i>',});});
 
</script>


<style type='text/css'> 

  body{margin:0}.page-content{width:100%;margin:0 auto;display:flex;display:-webkit-flex;justify-content:center;-o-justify-content:center;-ms-justify-content:center;-moz-justify-content:center;-webkit-justify-content:center;align-items:center;-o-align-items:center;-ms-align-items:center;-moz-align-items:center;-webkit-align-items:center;background-repeat:no-repeat;background-position:center center;background-size:cover;-o-background-size:cover;-ms-background-size:cover;-moz-background-size:cover;-webkit-background-size:cover}.wizard-v3-content{background:#fff;width:780px;box-shadow:0 8px 20px 0 rgba(0,0,0,.15);-o-box-shadow:0 8px 20px 0 rgba(0,0,0,.15);-ms-box-shadow:0 8px 20px 0 rgba(0,0,0,.15);-moz-box-shadow:0 8px 20px 0 rgba(0,0,0,.15);-webkit-box-shadow:0 8px 20px 0 rgba(0,0,0,.15);border-radius:10px;-o-border-radius:10px;-ms-border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px;margin:110px 0;font-family:roboto,sans-serif;position:relative;display:flex;display:-webkit-flex}.wizard-v3-content .wizard-form{width:100%}.wizard-form .wizard-header{text-align:center;padding:40px 0 20px}.wizard-form .wizard-header .heading{color:#333;font-size:32px;font-weight:700;margin:0;padding:13px 0 10px}.wizard-form .wizard-header p{color:#666;font-size:18px;font-weight:400;margin:0}.form-register .steps{margin-bottom:33px}.form-register .steps ul{display:flex;display:-webkit-flex;list-style:none;padding-left:108px}.form-register .steps li,.form-register .steps li.current{outline:none;-o-outline:none;-ms-outline:none;-moz-outline:none;-webkit-outline:none;position:relative}.form-register .steps li .current-info{display:none}.form-register .steps li a{text-decoration:none;outline:none;-o-outline:none;-ms-outline:none;-moz-outline:none;-webkit-outline:none}.form-register .steps li a .title span{display:block}.form-register .steps li a .title .step-icon{width:60px;height:60px;border-radius:50%;-o-border-radius:50%;-ms-border-radius:50%;-moz-border-radius:50%;-webkit-border-radius:50%;background:#ccc;margin:0 auto;position:relative;outline:none;-o-outline:none;-ms-outline:none;-moz-outline:none;-webkit-outline:none;color:#fff;font-size:25.6px;margin-right:108px}.form-register .steps li a .step-icon i{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-webkit-transform:translate(-50%,-50%)}.form-register .steps li .step-icon::before,.form-register .steps li:last-child .step-icon::after{position:absolute;content:"";background:#e5e5e5;width:108px;height:2px;top:50%;transform:translateY(-50%);-o-transform:translateY(-50%);-ms-transform:translateY(-50%);-moz-transform:translateY(-50%);-webkit-transform:translateY(-50%)}.form-register .steps li .step-icon::before{right:100%}.form-register .steps li:last-child .step-icon::after{left:100%}.form-register .steps li.current a .step-icon,.form-register .steps li.current a:active .step-icon,.form-register .steps li.done a .step-icon,.form-register .steps li.done a:active .step-icon{background:#24c1e8}.form-register .steps .current .step-icon::before,.form-register .steps .current:last-child .step-icon::after,.form-register .steps .done .step-icon::before{background:#24c1e8}.form-register .steps li a .step-text{color:#999;font-weight:400;font-size:18px;padding:14px 0 8px}.form-register .steps .current .step-text,.form-register .steps .done .step-text{color:#333}.form-register .content{margin:0 20px;box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-o-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-ms-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-moz-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-webkit-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;padding:35px 55px 55px}.form-register .content h2{display:none}.form-register .content .inner h3{font-size:22px;color:#333;font-weight:700;margin:0;padding-bottom:18px;padding-top:5px}.form-register .content .form-total-p-1 .inner h3{padding-bottom:13px;padding-top:0}.form-register .content #form-total-p-3 .inner h3{padding-bottom:5px}.form-register .content .inner h4{font-size:18px;font-weight:700;color:#333;margin:0}.inner .form-row{display:flex;margin:0 -12px;position:relative}.form-row.form-row-date.form-row-date-1{margin-top:50px;margin-bottom:-18px}.inner .form-row .form-holder{width:50%;padding:0 12px;margin-bottom:15px;position:relative}.inner .form-row .form-holder.form-holder-1{width:84.1%}.inner .form-row .form-holder.form-holder-2{width:100%;position:relative}.inner .form-row .form-row-inner{position:relative}.inner .form-row .form-holder .label{position:absolute;top:-3px;left:10px;font-size:16px;font-weight:400;color:#666;transform-origin:0 0;transition:all .2s ease;-webkit-transition:all .2s ease;-moz-transition:all .2s ease;-o-transition:all .2s ease;-ms-transition:all .2s ease}.inner .form-row .form-holder .border{position:absolute;bottom:31px;left:0;height:2px;width:100%;background:#6bc734;transform:scaleX(0);-webkit-transform:scaleX(0);-moz-transform:scaleX(0);-o-transform:scaleX(0);-ms-transform:scaleX(0);transform-origin:0 0;transition:all .15s ease;-webkit-transition:all .15s ease;-moz-transition:all .15s ease;-o-transition:all .15s ease;-ms-transition:all .15s ease}.inner .form-row #radio{color:#666;font-weight:400;font-size:16px;margin:8px 12px}.inner .form-row #checkbox{margin:10px 0 28px 11px;font-size:15px;color:#666;font-weight:400}.inner .form-row .form-holder label.special-label{display:inline-block;float:left;margin-top:25px;padding-right:20px;color:#666;font-size:16px;font-weight:400}.inner .form-row .form-holder label.pay-1-label,.inner .form-row .form-holder label.pay-2-label{width:190px;height:95px;border:1px solid #e5e5e5;display:block;cursor:pointer;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-o-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box;float:left;margin-right:15px;text-align:center;color:#666;font-size:16px;font-weight:400;margin-bottom:5px}.inner .form-row .form-holder label.pay-1-label img,.inner .form-row .form-holder label.pay-2-label img{padding-top:15px;padding-bottom:8px}.inner .form-row.form-row-date .form-holder select{float:left;width:19%;margin-right:20px}.inner .form-row .form-holder input,.inner .form-row .form-holder select{width:100%;padding:13px 10px 8px;border:none;border-bottom:2px solid #e5e5e5;appearance:unset;-moz-appearance:unset;-webkit-appearance:unset;-o-appearance:unset;-ms-appearance:unset;outline:none;-moz-outline:none;-webkit-outline:none;-o-outline:none;-ms-outline:none;font-family:roboto,sans-serif;font-weight:400;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;-o-box-sizing:border-box;-ms-box-sizing:border-box}.inner .form-row .form-holder input{font-size:18px;color:#333;font-weight:700}.inner .form-row .form-holder input#pay-1,.inner .form-row .form-holder input#pay-2{display:none}.inner .form-row .form-holder select{font-size:15px;color:#666;background:#fff url(../images/wizard_v4_icon.png) no-repeat scroll;background-position:right 0 center;z-index:1;cursor:pointer;position:relative}.inner .form-row .form-holder select:focus{border-bottom:2px solid #24c1e8}.inner .form-row .form-holder .form-control:focus,.inner .form-row .form-holder .form-control:valid{border-bottom:2px solid #24c1e8;margin-top:21px}.inner .form-row .form-holder .form-control:focus+.label,.inner .form-row .form-holder .form-control:valid+.label{transform:translateY(-23px) scale(1);-o-transform:translateY(-23px) scale(1);-ms-transform:translateY(-23px) scale(1);-moz-transform:translateY(-23px) scale(1);-webkit-transform:translateY(-23px) scale(1);color:#24c1e8}.inner .form-row .form-holder .form-control:focus+.border,.inner .form-row .form-holder .form-control:valid+.border{transform:scaleX(1);-o-transform:scaleX(1);-ms-transform:scaleX(1);-moz-transform:scaleX(1);-webkit-transform:scaleX(1)}.inner .form-row .form-holder input#pay-1:checked+label,.inner .form-row .form-holder input#pay-2:checked+label{border:none;box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-o-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-ms-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-moz-box-shadow:0 3px 10px 0 rgba(0,0,0,.15);-webkit-box-shadow:0 3px 10px 0 rgba(0,0,0,.15)}.inner .form-row.table-responsive{border:none;border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;margin:27px 0 -25px}.inner .table-responsive .table{width:100%}.inner .table-responsive tbody{text-align:left}.inner .table-responsive tr.space-row>td,.inner .table-responsive tr.space-row>th{padding:17px 20px 14px;border-top:1px solid #e5e5e5}.inner .table-responsive tr.space-row:last-child>td,.inner .table-responsive tr.space-row:last-child>th{border-bottom:none}.inner .table-responsive tbody th{color:#666;font-size:16px;font-weight:400;width:30%}.inner .table-responsive tbody td{color:#333;font-size:16px;font-weight:400}.actions ul{list-style:none;padding-left:0;padding:0 20px;margin:30px 0;display:flex;display:-webkit-flex;justify-content:space-between;-o-justify-content:space-between;-ms-justify-content:space-between;-moz-justify-content:space-between;-webkit-justify-content:space-between}.actions ul li.disabled{opacity:0}.actions ul li{padding:0;border:none;border-radius:3px;-o-border-radius:3px;-ms-border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;display:inline-flex;height:45px;width:140px;justify-content:center;-o-justify-content:center;-ms-justify-content:center;-moz-justify-content:center;-webkit-justify-content:center;-o-align-items:center;-ms-align-items:center;-moz-align-items:center;-webkit-align-items:center;align-items:center;background:#24c1e8;font-family:roboto,sans-serif;font-size:16px;font-weight:400;cursor:pointer}.actions ul li:hover{background:#1d97b5}.actions ul li:first-child{background:#999;margin-left:60.9%}.actions ul li:first-child:hover{background:#666}.actions ul li a{color:#fff;text-decoration:none;padding:13px 34px}@media screen and (max-width:1199px){.wizard-v3-content{margin:180px 20px}}@media screen and (max-width:991px){.form-register .steps ul{padding-left:0;justify-content:space-around;-o-justify-content:space-around;-ms-justify-content:space-around;-moz-justify-content:space-around;-webkit-justify-content:space-around}.form-register .steps li a .title{text-align:center}.form-register .steps li a .title .step-icon,.form-register .steps li a .title .step-icon{margin:0 auto}.form-register .steps li a .title .step-icon::before,.form-register .steps li:last-child a .title .step-icon::after{content:none}.actions ul li:first-child{margin-left:0}}@media screen and (max-width:767px){.inner .form-row.form-row-date .form-holder select{width:17.5%;margin-right:20px}.inner .form-row .form-holder label.pay-1-label,.inner .form-row .form-holder label.pay-2-label{margin-bottom:15px}}@media screen and (max-width:575px){.wizard-v3-content{width:90%}.wizard-form .wizard-header{padding:40px 20px 20px}.inner .form-row{flex-direction:column;-o-flex-direction:column;-ms-flex-direction:column;-moz-flex-direction:column;-webkit-flex-direction:column;margin:0}.inner .form-row .form-holder{width:100%}.inner .form-row .form-holder.form-holder-1{width:100%}.inner .form-row .form-holder{padding:0}.inner .form-row .form-holder label.special-label{float:none}.inner .form-row.form-row-date .form-holder select{float:none;width:100%;margin-bottom:20px;margin-right:0;display:block}.form-register .steps li a .title .step-icon{width:40px;height:40px;font-size:20px}.form-register .steps li a .step-text{font-size:12px}.form-register .content{padding:35px 30px 60px}.inner .form-row #radio{margin-left:-2px}.inner .form-row #checkbox{margin-left:0}.actions ul li{width:120px}.actions ul li a{padding:15px 24px}}

  </style> 
@endsection 