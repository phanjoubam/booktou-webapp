@extends('layouts.store_front_02')
@section('content')

<style type="text/css">
 .status{
  position: absolute;
  top:50%;
  left: 50%;
  transform: translate(-50%,-50%);

}
.bar-status{
  width: 250px;
  height: 50px;
  background: #dcdde1;
  border-radius: 20px; 
  box-shadow: 2px 2px 3px #000 inset;
  overflow: hidden; 
}

.bar{ 
  height: 100%;
  background: #4cd137;
  animation:set 10s ease;
  animation-iteration-count: 1; 
}

@keyframes set{
  0%{
    width: 0;
  }
  50%{
    width: 250px;
  } 
}
</style>

<?php
  $appId = Session::get('appId');
  $orderId = Session::get('orderId');
  $orderAmount = Session::get('orderAmount');
  $customerName = Session::get('customerName');
  $customerPhone = Session::get('customerPhone');
  $customerEmail = Session::get('customerEmail');
  $returnUrl = Session::get('returnUrl');
  $notifyUrl = Session::get('notifyUrl');
  $signature = Session::get('signature');
  $rzpapi = Session::get('rzpapi'); 
  
?>
<div class="row">

<div class="card col-md-12" style="height:200px;">
      <div class="card-body"> 
      <div class="status">

        <h7>Your Payment is processing! please do not close the page.</h7>

        <div class="bar-status mt-5">
          <div class="bar"> 
          </div>
        </div>
      </div> 
    
    </div>
</div>

<!-- <form id="paymentForm" action="https://www.cashfree.com/checkout/post/submit" method="post"> -->

<form id="paymentForm" action="https://test.cashfree.com/billpay/checkout/post/submit" method="post">
<input type="hidden" class="form-control" name="appId" value="{{$appId}}"/>
<input type="hidden" class="form-control" name="orderId" placeholder="" value="{{$orderId}}" />
<input type="hidden" class="form-control" name="orderAmount" placeholder="" value="{{$orderAmount}}" />
<input  type="hidden" class="form-control" name="customerName" placeholder="" value="{{$customerName}}"/>
<input type="hidden" class="form-control" name="customerEmail" placeholder="" value="{{$customerEmail}}" />
<input type="hidden" class="form-control" name="customerPhone" placeholder="" value="{{$customerPhone}}" />
<input type="hidden" class="form-control" name="returnUrl" placeholder="" value="{{$returnUrl}}"/>
<input type="hidden" class="form-control" name="notifyUrl" value="{{$notifyUrl}}" />
<input type="hidden" class="form-control" name="signature" value="{{$signature}}" />
<input type="hidden" class="form-control" name="rzpapi" value="{{$rzpapi}}" />
        
</div>

         

</form>
@endsection 
 
@section('script')
<script>
 
$(document).ready(function(){
     $("#paymentForm").submit();
});

</script>

@endsection 