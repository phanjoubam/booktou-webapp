@extends('layouts.store_front_02')
@section('content') 

<style type="text/css">
p.x-biggest{
    font-family: sans-serif;
    font-size: 1.2em;
    text-align: justify;
    line-height: 1.8;
}
li{
    font-family: sans-serif;
    font-size: 1.1em;
    text-align: justify;
    line-height: 1.8;
}
h2{
    text-decoration-line: underline;
    font-size: 2.5em;
}
h3{
    font-size: 2em;
}
}

</style> 

<div class="breadcumb_area bg-img mt-5 mb-5" style="background: white;">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Cancellation and Refund Policies</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> 
   
<section class="single_product_details_area d-flex align-items-center box-lg mb-5" >
<div class="container">
<div class="row">
  
<div class="col-sm-12 col-md-10 col-lg-10 offset-md-1 offset-lg-1"> 

     
        <br/>
               <p class="x-biggest">
       1. bookTou shall confirm and initiate the execution of the transaction initiated by You upon receiving confirmation from You for the same. If You wish to cancel a transaction on the Platform, You shall select the cancel option on the Platform. It is to be noted that You may have to pay a cancellation fee for transactions initiated on the Platform for which work has already been commenced by the Delivery Partner or the Merchant, as the case may be. With respect to work commenced by Merchants the cancellation fee will be charged to You which will be in accordance with the cancellation and refund policies of such Merchants.
      </p>  
       

          <p class="x-biggest">
      2. bookTou may cancel the transaction initiated by You on the Platform, if:
      </p>  
       <ul>
            <li>The designated address to avail the bookTou Services provided by You is outside the service zone of bookTou.</li>
            <li>Failure to get your response via phone or any other communication channel at the time of confirmation of the order booking.</li> 

            <li> The transaction involves supply/delivery/purchase of any material good that is illegal, offensive or volatile of the Terms of Use.</li> 

             <li>Information, instructions and authorizations provided by You is not complete or sufficient to execute the transaction initiated by You on the Platform.</li>

            <li>If in case of tied-up Merchants, the Tied-Up Merchant outlet is closed.</li>

             <li> If a Delivery Partner is not available to perform the services, as may be requested.</li>


            <li>If any Item for which You have initiated the transaction is not in stock with the Merchant.</li>

             <li> If the transaction cannot be completed for reasons not in control of bookTou.</li> 
        </ul>
</br>

    <p class="x-biggest">
      3. You shall only be able to claim refunds for transactions initiated by You only if You have already pre-paid the fees with respect to such transaction. Subject to relevant Merchant’s refund policy and in accordance therein, You shall be eligible to get the refund in the following circumstances:
      </p>  
       <ul>
            <li>Your package has been tampered or damaged at the time of delivery, as determined by bookTou basis the parameters established by bookTou in its sole discretion.</li>
            <li>If the wrong Item has been delivered to You, which does not match with the Item for which You had initiated a transaction on the Platform.</li> 

            <li> bookTou has cancelled the order because of any reason mentioned under Para 6 (2) above.</li> 

             <li> All decisions with respect to refunds will be at the sole discretion of bookTou and in accordance with bookTou’s internal refund policy (Refund Metrix) and the same shall be final and binding. All refunds initiated by bookTou shall be refunded to the financial source account from which, You have initiated the transaction on the Platform.</li>

             
        </ul>

 
        <br/>

  <p class="x-biggest">
      4. Pick Up and Drop Off Services
      </p>  
       <ul>
            <li>  As a part of the bookTou Services, bookTou also gives You an option to avail the Pick Up and Drop Off Services being provided by the Delivery Partners.</li>
            <li> You can initiate a transaction on the Platform by which You may (through the help of a Delivery Partner) send packages at a particular location. The Pick Up and Drop Off Services are provided to You directly by the Delivery Partner and bookTou merely acts as a technology platform to facilitate transactions initiated on the Platform and bookTou does not assume any responsibility or liability for any form of deficiency of services on part of the Delivery Partner.</li> 

            <li>  Upon initiation of a request for Pick Up and Drop Off Services on the Platform, depending upon the availability of Delivery Partner around Your area, bookTou will assign a Delivery Partner to You. The Delivery Partner shall pick up the Item from a location designated by You on the Platform and drop off the Items at a particular location designated by You. While performing the Pick Up and Drop off Services, the Delivery Partner shall act as an agent of You and shall act in accordance with Your instructions. You agree and acknowledge that the pick-up location and the drop off location has been added by You voluntarily and such information will be used for the bookTou Services and shall be handled by bookTou in accordance with the terms of its Privacy Policy.</li> 

             <li> You agree that You shall not request for a Pick Up and Drop Off Services for Items which are illegal, hazardous, dangerous, or otherwise restricted or constitute Items that are prohibited by any statute or law or regulation or the provisions of this Terms of Use.</li>

               <li> You are also aware that the Delivery Partner may choose to perform the Pick Up and Delivery Services requested by You.</li>
            <li>vi. You also agree that, upon becoming aware of the commission any offence by You or Your intention to commit any offence upon initiating a Pick-up and Drop-off Service or during a Pick-up and Drop-off service of any Item(s) restricted under applicable law, the Delivery Partner may report such information to bookTou or to the law enforcement authorities.</li> 
 
        </ul>



 <br/><p class="x-biggest">5. bookTou Cash, Google Pay Offer, Paytm Offer and Amazon Pay Offer shall hereinafter be referred to as "Offer".</p>

<br/><p class="x-biggest">
6. You hereby agree and acknowledge that the Offers are being extended by bookTou at its sole independent discretion and nothing shall entitle You to any of the Offers. You shall read the terms and conditions of the Offers carefully before availing them.</p>
      
         
          <br/>
        <h3> Contact us</h3>
        <p class="x-biggest">If you have any inquiries or requests in respect of your Personal Data or our Privacy and Data Protection Policy, you may contact us at:</p>
        <p class="x-biggest">Ezanvel Office<br/>
        Ezanvel Solutions private Ltd.</br>
Minuthong-Khuyathong Road,</br>
Opposite Kekrupat</br>
Imphal West</br>
795001<br/>
Email: <a href='mailto:booktougi@gmail.com' target='_blank'>booktougi@gmail.com</a><br/>
Phone: +91-986-308-6093<br/>
</p>
 
     </ul>
       
  </div>

 
</div>
</div>
</section> 

 
 
@endsection 
 