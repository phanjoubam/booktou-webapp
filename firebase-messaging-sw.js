// Scripts for firebase and firebase messaging
 importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
 importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

 // Initialize the Firebase app in the service worker by passing the generated config
 const firebaseConfig = {
 	apiKey: "AIzaSyC-FH-ZY6Uv-r07slY11O0BYH1twtcQJA8",
 	authDomain: "aptclassfcm.firebaseapp.com",
 	projectId: "aptclassfcm",
 	storageBucket: "aptclassfcm.appspot.com",
 	messagingSenderId: "341747945802",
 	appId: "1:341747945802:web:048c9584f03555d6e9b1b3",
 	measurementId: "G-0BGEYE8G4Q"

 };

 firebase.initializeApp(firebaseConfig);

 // Retrieve firebase messaging
 const messaging = firebase.messaging();

 messaging.onBackgroundMessage(function(payload) {
   console.log("Received background message ", payload);

   const notificationTitle = payload.notification.title;
   const notificationOptions = {
     body: payload.notification.body,
   };

   self.registration.showNotification(notificationTitle, notificationOptions);
 });